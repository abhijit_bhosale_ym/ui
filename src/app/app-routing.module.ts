import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './_helpers';
import { LoginComponent } from './components';

import { AppLayoutComponent } from './components/layouts/app-layout/app-layout.component';
import { DefaultappComponent } from './components/defaultapp/defaultapp.component';
import { ConnectorListComponent } from './modules/compaign-management/connector-list/connector-list.component';

const routes: Routes = [
  {
    path: 'auth/set-password/:email/:token',
    component: LoginComponent
  },
  {
    path: 'login/forgot-password/:email/:token',
    component: LoginComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    component: AppLayoutComponent,
    children: [
      {
        path: '',
        component: DefaultappComponent
      },
      {
        path: 'adops360',
        loadChildren: () => import('./modules/rev360-app/rev360-app.module').then(m => m.Rev360AppModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Revenue Management 360'
        }
      },
      {
        path: 'payout-publisher',
        loadChildren:
          () => import('./modules/payout-publisher/payout-publisher-app.module').then(m => m.PayoutPublisherAppModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Payout Publisher'
        }
      },
      {
        path: 'ott-analytics',
        loadChildren:
          () => import('./modules/ott-analytics/ott-analytics-app.module').then(m => m.OTTAnalyticsAppModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'OTT Analytics'
        }
      },
      {
        path: 'waypoint-cpr',
        loadChildren:
          () => import('./modules/compaign-progress-dashboard/compaign-progress-dashboard-app.module').then(m => m.CompaignProgressDashboardAppModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Campaign Progress Dashboard'
        }
      },
      {
        path: 'rpmTrends',
        loadChildren: () => import('./modules/rpm-app/rpm-app.module').then(m => m.RpmAppModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'RPM Trends'
        }
      },
      {
        path: 'revmgmt',
        loadChildren:
          () => import('./modules/revmgmt-app/revmgmt-app.module').then(m => m.RevMgmtAppModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Yield Management'
        }
      },
      {
        path: 'unfilled-inventory',
        loadChildren:
          () => import('./modules/unfilled-app/unfilled-app.module').then(m => m.UnfilledAppModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Unfilled Inventory'
        }
      },
      {
        path: 'vast-errors',
        loadChildren: () => import('./modules/vast-app/vast-app.module').then(m => m.VastAppModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Vast Errors'
        }
      },
      {
        path: 'payout-internal',
        loadChildren:
          () => import('./modules/frankly-payout-app/franklypayout-app.module').then(m => m.FranklyPayoutAppModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Internal Payout'
        }
      },
      {
        path: 'finance-audit',
        loadChildren:
          () => import('./modules/finance-audit-app/financeaudit-app.module').then(m => m.FinanceAuditAppModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Finance Audit'
        }
      },
      {
        path: 'data-pipeline',
        loadChildren:
          () => import('./modules/data-pipeline/data-pipeline-app.module').then(m => m.DataPipelineAppRoutesAppModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Data Pipeline'
        }
      },
      {
        path: 'users',
        loadChildren: () => import('./modules/users/users.module').then(m => m.UsersModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Users'
        }
      },
      {
        path: 'roles',
        loadChildren: () => import('./modules/roles/roles.module').then(m => m.RoleModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Users'
        }
      },
      {
        path: 'teams',
        loadChildren: () => import('./modules/team/team.module').then(m => m.TeamModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Teams'
        }
      },
      {
        path: 'upr',
        loadChildren: () => import('./modules/upr/upr-app.module').then(m => m.UPRAppModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Unified Pricing Rules'
        }
      },
      {
        path: '3p-statements',
        loadChildren:
          () => import('./modules/threep-statements/threep-statements.module').then(m => m.ThreePStatementsAppRoutesAppModule),
        data: {
          breadcrumb: '3p-statements'
        }
      },
      {
        path: 'analysis-report',
        loadChildren:
          () => import('./modules/analysis-report/analysis-report.module').then(m => m.AnalysisReportAppRoutesAppModule),
        data: {
          breadcrumb: 'Analysis Report'
        }
      },
      {
        path: 'dashboard',
        loadChildren:
          () => import('./modules/dashboard-app/dashboard-app.module').then(m => m.DashboardAppRoutesAppModule),
        data: {
          breadcrumb: 'Dashboard'
        }
      },
      {
        path: 'usefull-resources',
        loadChildren: () => import('./modules/usefull-resources-app/usefull-resources-app.module').then(
          m => m.UsefullResourcesAppRoutesAppModule
        ),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Usefull Resources'
        }
      },

      {
        path: 'user-profile',
        loadChildren:
          () => import('./modules/user-profile/user-profile.module').then(m => m.UserProfileModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'User Profile'
        }
      },
      // {
      //   path: 'campaign-progress-report',
      //   loadChildren: () => import('./modules/cpr/cpr.module').then(m => m.CprModule),
      //   data: {
      //     breadcrumb: 'Campaign Progress Report'
      //   }
      // },
      {
        path: 'roles',
        loadChildren: () => import('./modules/roles/roles.module').then(m => m.RoleModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Roles'
        }
      },
      {
        path: 'platform-publisher',
        loadChildren:
          () => import('./modules/platform-publisher/platform-publisher-app.module').then(m => m.PlatformPublisherAppModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Payout Publisher'
        }
      },
      {
        path: 'dsp-connection',
        loadChildren: () => import('./modules/dsp-connection-app/dsp-connection-app.module').then(m => m.DSPConnectionAppModule),
        data: {
          breadcrumb: 'DSP Connection'
        }
      },
      {
        path: 'audience-segment-360',
        loadChildren: () => import('./modules/audience-segement360/rev360-app.module').then(m => m.AudienceSegement360AppModule),
        data: {
          breadcrumb: 'Audience Segement 360'
        }
      },
      {
        path: 'builder',
        loadChildren: () => import('./modules/builder-app/builder-app.module').then(m => m.BuilderAppModule),
        data: {
          breadcrumb: 'DSP Connection'
        }
      },
      {
        path: 'rio',
        loadChildren: () => import('./modules/rio-app/rio-app.module').then(m => m.RIOAppModule),
        data: {
          breadcrumb: 'Media Planning'
        }
      },
      {
        path: 'supply-360',
        loadChildren: () => import('./modules/supply360-app/supply360.module').then(m => m.Supply360Module),
        data: {
          breadcrumb: 'Supply 360'
        }
      },
      {
        path: 'trend-compare',
        loadChildren: () => import('./modules/date-compare-app/date-compare-app.module').then(m => m.DateCompareAppRoutesAppModule),
        data: {
          breadcrumb: 'Date Compare'
        }
      },
      {
        path: 'revenue-compare',
        loadChildren: () => import('./modules/revenue-compare-app/revenue-compare-app.module').then(m => m.RevenueCompareAppModule),
        data: {
          breadcrumb: 'Revenue Compare'
        }
      },
      {
        path: 'campaign-dashboard',
        loadChildren: () => import('./modules/compaign-dashboard/compaign-dashboard-app.module').then(m => m.CompaignDashboardAppModule),
        data: {
          breadcrumb: 'Campaign Dashboard'
        }
      },
      {
        path: 'prebid',
        loadChildren: () => import('./modules/prebid-app/prebid-app.module').then(m => m.PrebidAppModule),
        data: {
          breadcrumb: 'Prebid Analytics'
        }
      },
      {
        path: 'daas-support',
        loadChildren: () => import('./modules/support-app/support-app.module').then(m => m.SupportAppModule),
        data: {
          breadcrumb: 'Support'
        }
      },
      {
        path: 'pit-issues-tracker',
        loadChildren: () => import('./modules/pit-issues/pit-issues-app.module').then(m => m.PitIssuesAppModule),
        data: {
          breadcrumb: 'PIT Issues Tracker'
        }
      },
      {
        path: 'payment-summary',
        loadChildren: () => import('./modules/payment-summary/payment-summary-app.module').then(m => m.PaymentSummaryAppModule),
        data: {
          breadcrumb: 'Payment Summary'
        }
      },
      {
        path: 'payment-contact-info',
        loadChildren: () => import('./modules/payment-contact-info-app/payment-contact-info-app.module').then(m => m.PaymentContactInfoAppModule),
        data: {
          breadcrumb: 'Payment & Contact Info'
        }
      },
      {
        path: 'social-media-campaign',
        loadChildren: () => import('./modules/social-media-campaign/social-media-campaign.module').then(m => m.SocialMediaCampaignModule),
        data: {
          breadcrumb: 'Social Media Campaign'
        }
      },
      {
        path: 'visualize',
        loadChildren:
          () => import('./modules/visualize/visualize-app.module').then(m => m.VisualizeAppRoutesAppModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Visualize'
        }
      },
      {
        path: 'post360',
        loadChildren: () => import('./modules/post360-app/post360-app.module').then(m => m.Post360AppModule),
        data: {
          breadcrumb: 'Post 360'
        }
      },
      {
        path: 'ad-hoc-dashboard',
        loadChildren:
          () => import('./modules/ad-hoc-dashboard/ad-hoc-dashboard-app.module').then(m => m.AdHocDashboardAppModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Visualize'
        }
      },
      // {
      //   path: 'alert-dashboard',
      //   loadChildren:
      //     './modules/alert-dashboard-app/alert-dashboard-app.module#AlertDashboardAppRoutesAppModule',
      //   canActivate: [AuthGuard],
      //   data: {
      //     breadcrumb: 'Alert Dashboard'
      //   }
      // },
      {
        path: 'alert-dashboard',
        loadChildren:
          () => import('./modules/alert-dashboard-app/alert-dashboard-app.module').then(m => m.AlertDashboardAppRoutesAppModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Alert Dashboard'
        }
      },
      {
        path: 'alert-mgmt',
        loadChildren:
          () => import('./modules/alert-create-app/alert-create-app.module').then(m => m.AlertCreateAppRoutesAppModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Alert Management'
        }
      },
      {
        path: 'daily-kpi-lite-app',
        loadChildren:
          () => import('./modules/daily-kpi-lite-app/daily-kpi-lite-app.module').then(m => m.DailyKpiLiteAppModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Daily KPI Lite App'
        }
      },
      {
        path: 'daily-kpi',
        loadChildren:
          () => import('./modules/daily-kpi-app/daily-kpi-app.module').then(m => m.DailyKpiAppModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Daily KPI App'
        }
      },
      {
        path: 'digital-property-analytics',
        loadChildren:
          () => import('./modules/analytics/analytics.module').then(m => m.AnalyticsModule),
        // loadChildren: './modules/analytics/analytics.module#AnalyticsModule',
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Digital Property Analytics'
        }
      },
      {
        path: 'active-campaigns',
        loadChildren: () => import('./modules/active-campaigns/active-campaign-app.module').then(m => m.ActiveCampaignAppModule),
        data: {
          breadcrumb: 'Active Campaigns'
        }
      },
      {
        path: 'new-websites',
        loadChildren: () => import('./modules/new-sites-app/new-sites-app.module').then(m => m.NewSitesAppModule),
        data: {
          breadcrumb: 'New Sites'
        }
      },
      {
        path: 'campaign-mapping',
        loadChildren: () => import('./modules/campaign-mapping/campaign-mapping.module').then(m => m.CampaignMappingAppModule),
        data: {
          breadcrumb: 'Campaign Mapping'
        }
      },
      {
        path: 'campaign-delivery-dashboard',    //campaign-management
        loadChildren: () => import('./modules/compaign-management/compaign-management.module').then(m => m.ComapaignManagementModule),
        data: {
          breadcrumb: 'Campaign Management'
        }
      },
      {
        path: 'campaign-connector',
        component: ConnectorListComponent
      },
      {
        path: 'campaign-enable',
        loadChildren:
          () => import('./modules/campaign-enable/campaign-enable.module').then(m => m.CampaignEnableModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Campaign Enable'
        }
      },
      {
        path: 'revmgmt-fc',
        loadChildren:
          () => import('./modules/revmgmt-freecycle/revmgmt-freecycle.module').then(m => m.RevmgmtFreecycleAppModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Revenue Management'
        }
      },
      {
        path: 'unfilled-inventory-fc',
        loadChildren:
          () => import('./modules/unfilled-freecycle/unfilled-freecycle.module').then(m => m.UnfilledFreeCycleAppModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Unfilled Inventory'
        }
      },
      {
        path: 'analysis-report-fc',
        loadChildren:
          () => import('./modules/analysis-report-freecycle/analysis-report-freecycle.module').then(m => m.AnalysisReportAppRoutesAppModule),
        data: {
          breadcrumb: 'Analysis Report'
        }
      },
      {
        path: 'revenue-dashboard-fc',
        loadChildren:
          () => import('./modules/dashboard-freecycle/dashboard-freecycle.module').then(m => m.DashboardAppRoutesAppModule),
        data: {
          breadcrumb: 'Dashboard'
        }
      },
      {
        path: 'data-pipeline-fc',
        loadChildren:
          () => import('./modules/data-pipeline-freecycle/data-pipeline-freecycle.module').then(m => m.DataPipelineAppRoutesAppModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Data Pipeline'
        }
      },
      {
        path: 'supply-performance',
        loadChildren:
          () => import('./modules/supply-performance-app/supply-performance-app.module').then(m => m.SupplyPerformanceAppModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Supply Performance'
        }
      },
      {
        path: 'campaign-progress-report',
        loadChildren:
          () => import('./modules/goal-setting-app/goal-setting-app.module').then(m => m.GoalSettingAppModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Campaigns Fulfillment'
        }
      },
      {
        path: 'corporate-finance',
        loadChildren: () => import('./modules/corporate-finance/corporate-finance-app.module').then(m => m.CorporateFinanceAppModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Corporate Finance'
        }
      },
      {
        path: 'recommendations',
        loadChildren:
          () => import('./modules/all-recommandations-app/all-recommandations-app.module').then(m => m.AllRecommandationsAppRoutesAppModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Campaign Optimization'
        }
      },
      {
        path: 'all-campaigns',
        loadChildren: () => import('./modules/all-campaigns/all-campaigns-app.module').then(m => m.AllCampaignsAppModule),
        data: {
          breadcrumb: 'All Campaigns'
        }
      },
      {
        path: 'pixel-pinger',
        loadChildren: () => import('./modules/pixel-pinger/pixel-pinger-app.module').then(m => m.PixelPingerModule ),
        data: {
          breadcrumb: 'Pixel Pinger'
        }
      },
      {
        path: 'derived-metrics',
        loadChildren: () => import('./modules/derived-metrics/derived-metrics.module').then(m => m.DerivedMetricsModule),
        data: {
          breadcrumb: 'Derived Metrics'
        }
      },
      {
        path: 'social_analytics',
        loadChildren: () => import('./modules/campaign-performance/campaign-performance-app.module').then(m => m.CampaignPerformanceAppAppModule),
        data: {
          breadcrumb: 'Derived Metrics'
        }
      },
      {
        path: 'asbn-revmgmt',
        loadChildren:
          () => import('./modules/revmgmt-app-asbn/revmgmt-app-asbn.module').then(m => m.RevMgmtAppAsbnModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Programmatic Revenue Trends'
        }
      },
      {
        path: 'asbn-rev360',
        loadChildren: () => import('./modules/rev360-app-asbn/rev360-asbn-app.module').then(m => m.Rev360AsbnAppModule),
        canActivate: [AuthGuard],
        data: {
          breadcrumb: 'Revenue Management 360'
        }
      }
    ]
  },
  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      onSameUrlNavigation: 'reload'
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
