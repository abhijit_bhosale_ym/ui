export interface ExportConfig {
  exportLogo: string;
  exportTeamName: string;
  exportMailId: string;
}
