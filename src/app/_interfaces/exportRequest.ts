import { SheetDetails } from './sheetDetails';
import { ExportConfig } from './exportConfig';

export interface ExportRequest {
  sheetDetails: SheetDetails[];
  appName: string;
  sendEmail: string[];
  fileName: string;
  exportFormat: string;
  exportConfig: ExportConfig;
}
