import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaymentSummaryExportPopupComponent } from './payment-summary-export-popup.component';

describe('PaymentSummaryExportPopupComponent', () => {
  let component: PaymentSummaryExportPopupComponent;
  let fixture: ComponentFixture<PaymentSummaryExportPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PaymentSummaryExportPopupComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaymentSummaryExportPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
