import { Component, OnInit } from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { FetchApiDataService } from '../fetch-api-data.service';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import * as moment from 'moment';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-payment-summary-export-popup',
  templateUrl: './payment-summary-export-popup.component.html',
  styleUrls: ['./payment-summary-export-popup.component.scss']
})
export class PaymentSummaryExportPopupComponent implements OnInit {
  configData: object;
  exportRequest: ExportRequest = <ExportRequest>{};
  reportFormat = 'Excel';
  reportType = 'monthly';
  downloadType = 'media_group';
  rowData: any[];
  constructor(
    private libServ: CommonLibService,
    private dataFetchServ: FetchApiDataService,
    private exportService: ExportdataService,
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private toastService: ToastService
  ) {
    this.configData = this.config.data;
  }

  ngOnInit() {
    this.exportRequest = this.configData['exportRequest'];
    this.rowData = this.configData['rowData'];
    console.log('rowData', this.rowData)
  }

  downloadData() {
    this.toastService.displayToast({
      severity: 'info',
      summary: 'Export Report',
      detail:
        'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
      life: 10000
    });
    let colDef = [
      {
        field: 'accounting_key',
        displayName: 'Month-Year',
        format: 'date',
        width: '150',
        value: '',
        isExpanded: 'false',
        exportConfig: {
          format: 'date<<MMMM-YYYY<<-',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: ['MMMM YYYY'],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'station_group',
        displayName: 'Media Group',
        format: '',
        width: '250',
        isExpanded: 'false',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'derived_station_rpt',
        displayName: 'Property',
        format: '',
        width: '150',
        isExpanded: 'false',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'dp_impressions',
        displayName: 'Billable Imps.',
        format: 'number',
        width: '170',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'gross_revenue',
        displayName: 'Gross Revenue',
        width: '155',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'gross_ecpm',
        displayName: 'Gross eCPM',
        width: '155',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      }
    ];
    if (this.reportType == 'daily') {
      colDef[0].field = 'time_key';
      colDef[0].displayName = 'date';
      colDef[0].exportConfig.format = 'date<<DD-MM-YYYY<<-';
    }
    const sheetDetailsarray = [];
    this.rowData.forEach(element => {
      const tableReq = {
        dimensions: this.reportType == 'daily' ? ['station_group', 'derived_station_rpt', 'time_key'] : ['station_group', 'derived_station_rpt', 'accounting_key'], // [this.filtersApplied['groupby'][0]['key']],
        metrics: [
          'dp_impressions',
          'gross_revenue'
        ],
        derived_metrics: ['gross_ecpm'],
        timeKeyFilter: {
          time_key1: element['accounting_period'], time_key2: moment(element['accounting_period'], 'YYYYMMDD')
            .endOf('month')
            .format('YYYYMMDD')
        },
        filters: { dimensions: [{ key: "derived_station_rpt", values: [element['station']] }], metrics: [] },
        groupByTimeKey: {
          key: ['accounting_key', 'time_key'],
          interval: 'daily'
        },
        gidGroupBy: ['station_group', 'derived_station_rpt'], // grpBys,
        orderBy: this.reportType == 'daily' ? [{ key: 'time_key', opcode: 'desc' }] : [{ key: 'accounting_key', opcode: 'desc' }],
        limit: '',
        offset: ''
      };
      const sheetDetails = {};
      sheetDetails['columnDef'] = colDef;
      sheetDetails['data'] = [];
      sheetDetails['sheetName'] = 'Daily Distribution';
      sheetDetails['isRequest'] = true;
      sheetDetails['request'] = {
        url: '/api/flask/payout/cassandradata',
        method: 'POST',
        param: tableReq
      };
      sheetDetails['disclaimer'] = [
        {
          position: 'top',
          label:
            'Data persent in below table is not final and may varies over period of time',
          color: '#3A37CF'
        },
        {
          position: 'bottom',
          label: 'Thank You',
          color: '#EC6A15'
        }
      ];
      sheetDetails['totalFooter'] = {
        available: true,
        custom: true
      };
      sheetDetails['tableTitle'] = {
        available: true,
        label: 'Daily Distribution'
      };
      sheetDetails['image'] = [
        {
          available: true,
          path: environment.exportConfig.exportLogo,
          position: 'top'
        }
      ];

      sheetDetailsarray.push(sheetDetails);
    });




    this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
    this.exportRequest['fileName'] =
      'Daily Distribution ' + moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
    this.exportRequest['exportFormat'] = this.reportFormat;
    this.exportRequest['exportConfig'] = environment.exportConfig;

    // return false;
    this.dataFetchServ
      .getExportReportData(this.exportRequest)
      .subscribe(response => {
        console.log(response);
      });

  }

  cancel() {
    this.ref.close(null);
  }
}
