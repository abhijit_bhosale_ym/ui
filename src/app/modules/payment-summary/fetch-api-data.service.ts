import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { TreeNode } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class FetchApiDataService {
  private BASE_URL: string = environment.baseUrl;

  constructor(private http: HttpClient) {}

  getPayoutData(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/payment-summary/getData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
  }

  getExportReportData(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/common/exportData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
    // return <TreeNode[]> json.data;
  }

  getLastUpdatedData(appId) {
    const url = `${this.BASE_URL}/frankly/v1/common/getLastUpdatedData/${appId}`;
    return this.http.get(url);
  }

  updatePaymentStatus(req) {
    const url = `${this.BASE_URL}/frankly/v1/payment-summary/update-payment-status`;
    return this.http.post(url, req);
  }

  sendNotification(req) {
    const url = `${this.BASE_URL}/frankly/v1/payment-summary/send-notification`;
    return this.http.post(url, req);
  }
}
