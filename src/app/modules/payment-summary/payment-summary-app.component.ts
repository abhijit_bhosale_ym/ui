import { Component, OnInit, OnDestroy } from '@angular/core';
import { TreeNode,  ConfirmationService } from 'primeng/api';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { CommonLibService } from 'src/app/_services';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';
import { FetchApiDataService } from './fetch-api-data.service';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { Subscription } from 'rxjs';
import { SendEmailComponent } from '../send-email/send-email.component';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { PaymentSummaryExportPopupComponent } from './payment-summary-export-popup/payment-summary-export-popup.component';
import { DialogService } from 'primeng/dynamicdialog';

@Component({
  selector: 'ym-payment-summary-app',
  templateUrl: './payment-summary-app.component.html',
  styleUrls: ['./payment-summary-app.component.scss']
})
export class PaymentSummaryAppComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;

  appConfig: object = {};
  lastUpdatedOn: Date;
  nextUpdated: Date;
  dataUpdatedThrough: Date;
  cardsJson = [];
  showCards = true;
  isPopupGroupbyView = false;
  isPopupChartView = false;
  aggTableData: TreeNode[];
  noTableData = false;
  dimColDef: any[];
  //  metricColDef: any[];
  aggTableColumnDef: any[];
  aggTableJson: Object;
  displayAggTable = true;
  selectedNodesData: TreeNode[] = [];
  filtersApplied: object = {};
  selectedTypes: object = {};
  selectedPaymentStatus: object = {};
  exportRequest: ExportRequest = <ExportRequest>{};
  selectedData: any[];

  grpbyData = [{ key: 0, label: 'Payment Status', value: 'payment_status' },
  { key: 1, label: 'Send Notification', value: 'send_notification' },
  { key: 2, label: 'Multiple Export', value: 'multiple_export' }];
  paymetStatusData = [
    { key: 0, label: 'Not Initiated', value: 'not_initiated' },
    { key: 1, label: 'In Progress', value: 'in_progress' },
    { key: 2, label: 'Paid', value: 'paid' }];

  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private htmltoimage: HtmltoimageService,
    private exportService: ExportdataService,
    private pptExport: ExportPptService,
    private confirmationService: ConfirmationService
  ) { }

  ngOnInit() {


    this.dimColDef = [
      {
        field: 'accounting_period',
        displayName: 'Month-Year',
        format: 'date',
        width: '130',
        value: '',
        isExpanded: 'false',
        exportConfig: {
          format: 'date<<MMMM-YYYY<<-',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: ['MMMM YYYY'],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'station_group',
        displayName: 'Media Group',
        format: '',
        width: '250',
        isExpanded: 'false',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'station',
        displayName: 'Property',
        format: '',
        width: '150',
        isExpanded: 'false',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'gross_revenue',
        displayName: 'Gross Revenue',
        width: '155',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'payment_status',
        displayName: 'Payment Status',
        width: '155',
        exportConfig: {
          format: '',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'advice_check_date',
        displayName: 'Advice/Check Date',
        width: '155',
        exportConfig: {
          format: '',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'method_type',
        displayName: 'Payment Method',
        width: '155',
        exportConfig: {
          format: '',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'Export',
        displayName: 'Export',
        width: '110',
        exportConfig: {
          format: '',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      }
    ];

    // this.metricColDef = [

    // ];
    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: false,
      reload: false,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,

      columns: this.libServ.deepCopy(this.dimColDef.slice(3)),
      selectedColumns: this.libServ.deepCopy(this.dimColDef.slice(3)),
      frozenCols: this.libServ.deepCopy([...this.dimColDef.slice(0, 3)]),
      frozenWidth:
        this.dimColDef
          .slice(0, 3)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'single',
      selectedColsModal: this.selectedNodesData,
      selectionDataKey: 'name',
      metaKeySelection: false,
      showHideCols: false,
      overallSearch: false,
      columnSearch: false
    };
    console.log('this..', this.selectedNodesData);
    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        console.log('appConfig', this.appConfig);
        this.isPopupGroupbyView = this.appConfig['permissions'].some(
          o => o.name == 'frank-payout-grpby-popup'
        );

        this.isPopupChartView = this.appConfig['permissions'].some(
          o => o.name == 'frank-payout-view-charts-popup'
        );
        this._titleService.setTitle(this.appConfig['displayName']);
        let startDate;
        let endDate;
        const date_config = this.appConfig['filter']['filterConfig']['filters'][
          'datePeriod'
        ][0];
        if ((date_config['defaultDate'][0]['value'] === 0 && date_config['defaultDate'][0]['period'] === 'month') && (moment().format('DD') === '01' || moment().format('DD') === '02')) {
          startDate = moment().subtract(1, 'months').startOf('month');
          endDate = moment().subtract(1, 'months').endOf('month');
        } else {
          if (
            date_config[
            'defaultDate'
            ][0]['startOf']
          ) {
            startDate = moment()
              .subtract(
                date_config['defaultDate'][0]['value'],
                date_config['defaultDate'][0]['period']
              )
              .startOf(
                date_config['defaultDate'][0]['period']
              );
          } else {
            startDate = moment().subtract(
              date_config['defaultDate'][0]['value'],
              date_config['defaultDate'][0]['period']
            );
          }
          endDate = moment().subtract(
            date_config[
            'defaultDate'
            ][1]['value'],
            date_config[
            'defaultDate'
            ][1]['period']
          );
        }

        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: startDate.format('YYYYMMDD'),
            time_key2: endDate.format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: [{ key: 'station_group' }] // this.appConfig["filter"]["filterConfig"]["groupBy"].filter(
          // v => v.selected
          // )
        };
        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        this.initialLoading();
      }
    });
  }
  onCheckboxclick() {
    console.log('this..1', this.selectedNodesData);
  }
  initialLoading() {
    this.dataFetchServ
      .getLastUpdatedData(this.appConfig['id'])
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        this.lastUpdatedOn = moment(data[0]['updated_at']).toDate();
        this.nextUpdated = moment(data[0]['next_run_at']).toDate();
        this.dataUpdatedThrough = moment(
          data[0]['source_updated_through'],
          'YYYYMMDD'
        ).toDate();
      });

    const tableReq = {
      dimensions: ['station_group', 'accounting_period', 'station'], // [this.filtersApplied['groupby'][0]['key']],
      metrics: [
        'gross_revenue'
      ],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['accounting_period', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: ['accounting_period', 'station_group', 'station'], // grpBys,
      orderBy: [{ key: 'accounting_period', opcode: 'desc' }],
      limit: '',
      offset: ''
    };

    this.loadTableData(tableReq);
  }

  resetPagination(tt1) {
    if (typeof tt1 !== 'undefined') {
      tt1.reset();
    }
  }
  changeOperation() {
    this.selectedData = [];
    console.log('dataselected', this.selectedTypes)
    this.selectedNodesData.forEach(element => {
      if (!(this.libServ.isEmptyObj(element['data']))) {
        //   if (element['data']['isExpanded'] == '0') {
        //     console.log('element', element);
        //     selectedData['station_group'].push(element['data']['station_group']);
        //   } else {
        this.selectedData.push(element['data']);
        //   }

      }

    });
    if (this.selectedTypes['value'] == 'payment_status') {


    } else if (this.selectedTypes['value'] == 'send_notification') {
      this.confirmationService.confirm({
        message: 'Are you sure you want to send notification?',
        accept: () => {
          const req = {
            data: this.selectedData.map(function (x) {
              return { station: x.station, accounting_period: moment(x.accounting_period, 'YYYYMMDD').format('MMMM-YYYY'), gross_revenue: x.gross_revenue.toString() };
            }),
          };

          this.dataFetchServ.sendNotification(req).subscribe(data => {
            if (data['status']) {
              this.toastService.displayToast({
                severity: 'success',
                summary: 'Payment Status Update',
                detail: 'Payment status updated successfully'
              });
              const tableReq = {
                dimensions: ['station_group', 'accounting_period', 'station'], // [this.filtersApplied['groupby'][0]['key']],
                metrics: [
                  'gross_revenue'
                ],
                derived_metrics: [],
                timeKeyFilter: this.libServ.deepCopy(
                  this.filtersApplied['timeKeyFilter']
                ),
                filters: this.libServ.deepCopy(this.filtersApplied['filters']),
                groupByTimeKey: {
                  key: ['accounting_period', 'time_key'],
                  interval: 'daily'
                },
                gidGroupBy: ['accounting_period', 'station_group', 'station'], // grpBys,
                orderBy: [{ key: 'accounting_period', opcode: 'desc' }],
                limit: '',
                offset: ''
              };
              this.selectedNodesData = [];
              this.selectedTypes = {};
              // this.loadTableData(tableReq);
            } else {
              this.toastService.displayToast({
                severity: 'error',
                summary: 'Server Error',
                detail: 'Please refresh the page'
              });
            }
          });
        },
        reject: () => {
          this.selectedTypes = {};
          this.selectedPaymentStatus = {};
        }
      });

    } else if (this.selectedTypes['value'] == 'multiple_export') {

      if (this.exportRequest['sendEmail'].length > 0) {
        const data = {
          rowData: this.selectedData,
          colDef: this.aggTableColumnDef,
          exportRequest: this.exportRequest,
        };

        const ref = this.dialogService.open(PaymentSummaryExportPopupComponent, {
          header: 'Download Report',
          contentStyle: { 'max-height': '80vh', overflow: 'auto' },
          data: data
        });
        ref.onClose.subscribe((data: string) => { });
      } else {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Export Report',
          detail:
            'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
          life: 10000
        });
      }

    }
    console.log('selectedData', this.selectedData)
  }

  changePaymentStatus() {
    console.log('dataselected', this.selectedPaymentStatus)
    this.confirmationService.confirm({
      message: 'Are you sure you want to change update payment status?',
      accept: () => {
        const req = {
          payment_status: this.selectedPaymentStatus['label'],
          station: this.selectedData.map(x => x.station),
          user_id: this.appConfig['user']['id']
        };

        this.dataFetchServ.updatePaymentStatus(req).subscribe(data => {
          if (data['status']) {
            this.toastService.displayToast({
              severity: 'success',
              summary: 'Payment Status Update',
              detail: 'Payment status updated successfully'
            });
            const tableReq = {
              dimensions: ['station_group', 'accounting_period', 'station'], // [this.filtersApplied['groupby'][0]['key']],
              metrics: [
                'gross_revenue'
              ],
              derived_metrics: [],
              timeKeyFilter: this.libServ.deepCopy(
                this.filtersApplied['timeKeyFilter']
              ),
              filters: this.libServ.deepCopy(this.filtersApplied['filters']),
              groupByTimeKey: {
                key: ['accounting_period', 'time_key'],
                interval: 'daily'
              },
              gidGroupBy: ['accounting_period', 'station_group', 'station'], // grpBys,
              orderBy: [{ key: 'accounting_period', opcode: 'desc' }],
              limit: '',
              offset: ''
            };
            this.selectedNodesData = [];
            this.selectedTypes = {};
            this.loadTableData(tableReq);
          } else {
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Server Error',
              detail: 'Please refresh the page'
            });
          }
        });
      },
      reject: () => {
        this.selectedTypes = {};
        this.selectedPaymentStatus = {};
      }
    });
  }
  loadTableData(params) {
    // this.reloadAggTable();
    this.aggTableJson['loading'] = true;

    // params['dimensions'].push('accounting_period');

    this.dataFetchServ.getPayoutData(params).subscribe(data => {
      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      } else {
        this.noTableData = false;
      }
      const grpBysFilter = ['accounting_period', 'station_group', 'station'];

      const tableData = data['data'];
      const arr = [];
      tableData.forEach((row: object) => {
        // for (let i = 0; i < grpBysFilter.length; i++) {
        //   if (i !== grpBysFilter.length - 1) {
        //     row['station'] = 'All';
        //   }
        // }
        const tk = row['advice_check_date'];
        delete row['advice_check_date'];
        row['advice_check_date'] = moment(tk).format('MM-DD-YYYY');
        let obj = {};

        obj = {
          data: row
        };
        arr.push(obj);
      });

      this.aggTableData = <TreeNode[]>arr;
      this.aggTableJson['totalRecords'] = data['totalItems'];
      this.aggTableJson['loading'] = false;
    });
  }

  reloadAggTable() {
    this.displayAggTable = false;
    setTimeout(() => {
      this.displayAggTable = true;
    }, 0);
  }

  reset(table) {
    table.reset();
    const tableReq = {
      dimensions: this.getGrpBys(),
      metrics: [
        'dp_impressions',
        'gross_revenue',
        'total_expenses',
        'station_revenue_share_distribution',
        'total_recoupable_expenses'
      ],
      derived_metrics: ['client_net_share', 'gross_ecpm', 'client_net_ecpm'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['accounting_period', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys(), // this.filtersApplied['filters']['dimensions'].map(e => e.key), // grpBys,
      orderBy: [{ key: 'accounting_period', opcode: 'desc' }],
      limit: '',
      offset: ''
    };
    (this.aggTableJson['columns'] = this.libServ.deepCopy(
      this.dimColDef.slice(3)
    )),
      (this.aggTableJson['selectedColumns'] = this.libServ.deepCopy(
        this.dimColDef.slice(3)
      )),
      (this.aggTableJson['frozenCols'] = this.libServ.deepCopy([
        ...this.dimColDef.slice(0, 3)
      ])),
      this.loadTableData(tableReq);
  }

  onTableDrill(e: Event) {
    console.log('Drilled', e);
    if (!e['node']['childLoaded']) {
      this.aggTableJson['loading'] = true;
      const tableReq = {
        dimensions: ['station_group', 'accounting_period', 'station'],
        metrics: [
          'gross_revenue'],
        derived_metrics: [],
        timeKeyFilter: {},
        filters: this.libServ.deepCopy(this.filtersApplied['filters']),
        groupByTimeKey: {
          key: ['accounting_period', 'time_key'],
          interval: 'daily'
        },
        gidGroupBy: ['station', 'station_group'], // this.getGrpBys(),
        orderBy: [],
        limit: '',
        offset: ''
      };

      const timeKey1 = e['node']['data']['accounting_period'];
      const timeKey2 = moment(timeKey1, 'YYYYMMDD')
        .endOf('month')
        .format('YYYYMMDD');

      tableReq['timeKeyFilter'] = { time_key1: timeKey1, time_key2: timeKey2 };

      const grpBys = ['station', 'station_group']; // this.filtersApplied['groupby'].map(e => e.key);
      for (const g of grpBys) {
        if (e['node']['data'][g] !== 'All') {
          // tableReq['dimensions'].push(g);
          if (!tableReq['gidGroupBy'].includes(g)) {
            tableReq['gidGroupBy'].push(g);
          }
          if (
            tableReq['filters']['dimensions'].findIndex(e => e.key === g) === -1
          ) {
            tableReq['filters']['dimensions'].push({
              key: g,
              values: [e['node']['data'][g]]
            });
          } else {
            tableReq['filters']['dimensions'].splice(
              tableReq['filters']['dimensions'].findIndex(e => e.key === g),
              1
            );
            tableReq['filters']['dimensions'].push({
              key: g,
              values: [e['node']['data'][g]]
            });
            if (
              tableReq['filters']['dimensions']
                .find(e => e.key === g)
              ['values'].findIndex(v => v === e['node']['data'][g]) === -1
            ) {
              tableReq['filters']['dimensions'].push(e['node']['data'][g]);
            }
          }
        }
      }

      this.dataFetchServ.getPayoutData(tableReq).subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        const childData = data['data'];
        const arr = [];

        childData.forEach((row: object) => {
          this.aggTableJson['frozenCols'].forEach(r => {
            if (typeof row[r['field']] === 'undefined') {
              row[r['field']] = 'All';
            }
          });
          let obj = {};
          if (row[grpBys[grpBys.length - 1]] === 'All') {
            obj = {
              data: row,
              children: [{ data: {} }]
            };
          } else {
            obj = {
              data: row
            };
          }
          obj['data']['isExpanded'] = '1';
          arr.push(obj);
        });

        this.aggTableJson['loading'] = false;
        e['node']['children'] = <TreeNode[]>arr;
        this.aggTableData = [...this.aggTableData];
        e['node']['childLoaded'] = true;
      });
    }
  }

  onLazyLoadAggTable(e: Event) {
    console.log('Lazy Agg', e);
  }


  openExportPopup(row) {
    if (this.exportRequest['sendEmail'].length > 0) {
      const data = {
        rowData: [row],
        colDef: this.aggTableColumnDef,
        exportRequest: this.exportRequest,
      };

      const ref = this.dialogService.open(PaymentSummaryExportPopupComponent, {
        header: 'Download Report',
        contentStyle: { 'max-height': '80vh', overflow: 'auto' },
        data: data
      });
      ref.onClose.subscribe((data: string) => { });
    } else {
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Export Report',
        detail:
          'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
        life: 10000
      });
    }
    return false;
  }

  isHiddenColumn(col: Object) {
    return (
      this.aggTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }

  /* --------------------- On Filter Apply Button Clicked --------------------- */

  onFiltersApplied(filterData: object) {
    this.filtersApplied['filters']['dimensions'] = [];

    // tslint:disable-next-line: forin
    for (const k in filterData['filter']['dimensions']) {
      if (filterData['filter']['dimensions'][k].length !== 0) {
        this.filtersApplied['filters']['dimensions'].push({
          key: k,
          values: filterData['filter']['dimensions'][k]
        });
      }
    }

    // this.filtersApplied['groupby'] = filterData['groupby'];
    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];

    const tableReq = {
      dimensions: ['station_group', 'accounting_period', 'station'],
      metrics: ['gross_revenue'],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['accounting_period', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: ['accounting_period', 'station_group', 'station'], // this.filtersApplied['filters']['dimensions'].map(e => e.key), // grpBys,
      orderBy: [{ key: 'accounting_period', opcode: 'desc' }],
      limit: '',
      offset: ''
    };

    this.loadTableData(tableReq);
  }

  getGrpBys() {
    let grpBys = this.filtersApplied['groupby'].map(e => e.key);
    // let grpBys = [this.filtersApplied['groupby'][0]['key']];
    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          grpBys.concat(
            this.filtersApplied['filters']['dimensions']
              .filter(f => f.values.length)
              .map(m => m.key)
          )
        )
      );
    }
    return grpBys;
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['name']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    } else if (type === 'ppt') {
      const chartElements = document.getElementsByTagName('ym-charts');
      const slides = [];
      [].forEach.call(chartElements, (el, index) => {
        const c = el.getElementsByTagName('canvas')[0];

        slides.push({
          title: `${this.appConfig['name']}`,
          base64data: this.libServ.canvasToImage('white', c),
          width: 8,
          height: 3
        });

        if (index === chartElements.length - 1) {
          const pptConfig = {
            filename: 'Charts',
            logo: {
              visible: true,
              path: '/assets/images/ym.png',
              width: 0.53125,
              height: 0.458333
            },
            slides: slides
          };
          this.pptExport.exportPPT(pptConfig);
        }
      });
    }
    return false;
  }

  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }

  sendEmail() {
    const imageConfig = {
      imgName: `${this.appConfig['name']}.png`,
      type: 'image/png'
    };

    this.htmltoimage
      .canvasImage(document.getElementById('capture'), imageConfig)
      .then(canvas => {
        this.dialogService.open(SendEmailComponent, {
          header: 'Email Screenshot',
          contentStyle: {
            'max-height': '80vh',
            width: '30vw',
            overflow: 'auto'
          },
          data: canvas.toDataURL('image/png')
        });
      });
  }
}
