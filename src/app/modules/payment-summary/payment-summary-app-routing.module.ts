import { Routes } from '@angular/router';
import { PaymentSummaryAppComponent } from './payment-summary-app.component';

export const PaymentSummaryAppRoutes: Routes = [
  {
    path: '',
    component: PaymentSummaryAppComponent
  }
];
