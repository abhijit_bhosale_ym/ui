import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { TreeNode } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class FetchApiDataService {
  private BASE_URL: string = environment.baseUrl;

  constructor(private http: HttpClient) { }

  // getTableData(params: object) {
  //   const url = `${this.BASE_URL}/frankly/v1/revmgmt/getData`;
  //   if (!('isTable' in params)) {
  //     params = Object.assign(params, { isTable: true });
  //   }
  //   return this.http.post(url, params);
  //   // return <TreeNode[]> json.data;
  // }

  getCampaignStatus(params) {
    return {
      data : [
        {
          total: 66,
          active: 42
        }
      ]
    };
  }

  getTableData(params) {
    return {
      data: [{
        id: 1858,
        c_status: 'Active',
        campaign: 'Tremor-OMD-Showtime-Desus&Mero_S2',
        CID: '2525',
        start_date: '2020-02-03',
        end_date: '2020-04-09'
      },
      {
        id: 1859,
        c_status: 'Active',
        campaign: 'Tremor-OMD-Showtime-Desus&Mero_S2',
        CID: '2526',
        start_date: '2020-02-03',
        end_date: '2020-04-09'
      },
      {
        id: 1858,
        c_status: 'Active',
        campaign: 'Tremor-OMD-Showtime-Desus&Mero_S2',
        CID: '2527',
        start_date: '2020-02-03',
        end_date: '2020-04-09'
      },
      {
        id: 1858,
        c_status: 'Active',
        campaign: 'Tremor-OMD-Showtime-Desus&Mero_S2',
        CID: '2528',
        start_date: '2020-02-03',
        end_date: '2020-04-09'
      }, ]

    };
    // return <TreeNode[]> json.data;
  }

  getEndingLineItem(params) {
    return {
      data: [{
        server_li_id: '17980',
        l_status: 'Active',
        line_item: 'Amazon_Canada-Amazon_Embrace__Q12020-L00105500_F-136820-P4-Desktop-15s',
        campaign: 'Tremor-Cadreon-Amazon_Canada-Amazon_Embrace__Q12020',
        CID: '2589',
        trafficking_delivery_progress: '43.896292',
        start_date: '2020-03-09',
        end_date: '2020-04-11',
        billing_delivery_progress: '0.000000',
        campaign_duration_progress: '95.220588200',
        impressions_per_day: 12449
      },
      {
        server_li_id: '17980',
        l_status: 'Active',
        line_item: 'Amazon_Canada-Amazon_Embrace__Q12020-L00105500_F-136820-P4-Desktop-15s',
        campaign: 'Tremor-Cadreon-Amazon_Canada-Amazon_Embrace__Q12020',
        CID: '2590',
        trafficking_delivery_progress: '43.896292',
        start_date: '2020-03-09',
        end_date: '2020-04-11',
        billing_delivery_progress: '0.000000',
        campaign_duration_progress: '95.220588200',
        impressions_per_day: 12449
      }]
    };
  }

  getStartingLineItem(params) {
    return {
      data: [{
        active: '1',
        account_id: 5,
        line_item_id: 18289,
        line_item_name: 'Ohio_Tuition_Trust_Authority-OTTA_Tax_Season_2020-L00105765_F-137223-P3-Mobile_Tablet-15s MOAT',
        campaign_id: 2608,
        campaign_name: 'Tremor-Fahlgren_Mortine-Ohio_Tuition_Trust_Authority-OTTA_Tax_Season_2020',
        start_date: '2020-04-10',
        end_date: '2020-04-19'
      },
      {
        active: '1',
        account_id: 5,
        line_item_id: 18289,
        line_item_name: 'Ohio_Tuition_Trust_Authority-OTTA_Tax_Season_2020-L00105765_F-137223-P3-Mobile_Tablet-15s MOAT',
        campaign_id: 2609,
        campaign_name: 'Tremor-Fahlgren_Mortine-Ohio_Tuition_Trust_Authority-OTTA_Tax_Season_2020',
        start_date: '2020-04-10',
        end_date: '2020-04-19'
      }]
    };
  }

  getCompaignPacingAhead(params) {
    return {
      data: [
        {
          // fulfilment_line_item.campaign_duration_progress: "9999.999999",
          server_li_id: '17961',
          line_item: 'California_Tobacca_Control_Program-Q1/Q2\'20_Menthol-L00105509_F-136831-P1-Desktop-30s',
          campaign: 'Tremor-Duncan_Channon-California_Tobacca_Control_Program-Q1_Q2\'20_Menthol',
          CID: '2582',
          trafficking_delivery_progress: '9999.999999',
          billing_delivery_progress: '0.000000',
          campaign_duration_progress: '28.906250000',
          impressions_per_day: 3466,
          start_date: '2020-03-09',
          end_date: '2020-06-28'
        },
        {
          // fulfilment_line_item.campaign_duration_progress: "9999.999999",
          server_li_id: '17961',
          line_item: 'California_Tobacca_Control_Program-Q1/Q2\'20_Menthol-L00105509_F-136831-P1-Desktop-30s',
          campaign: 'Tremor-Duncan_Channon-California_Tobacca_Control_Program-Q1_Q2\'20_Menthol',
          CID: '2583',
          trafficking_delivery_progress: '9999.999999',
          billing_delivery_progress: '0.000000',
          campaign_duration_progress: '28.906250000',
          impressions_per_day: 3466,
          start_date: '2020-03-09',
          end_date: '2020-06-28'
        }
      ]
    };
  }

  getCompaignPacingBehind(params) {
    return {
      data: [
        {
          // fulfilment_line_item.campaign_duration_progress: "0.023625",
          server_li_id: '17798',
          line_item: 'Volvo-AS074632_-_Volvo_Car_USA_(MindShare_NY__New_York):_Volvo_T2_2020_-_Miami-L00106164-F-138084-P2-Mobile_Tablet-30s',
          campaign: 'Tremor-Mindshare_USA-Volvo-AS074632_-_Volvo_Car_USA_(MindShare_NY__New_York):_Volvo_T2_2020_-_Miami',
          CID: '2573',
          trafficking_delivery_progress: '0.023625',
          billing_delivery_progress: '0.034425',
          campaign_duration_progress: '31.250000000',
          impressions_per_day: 7193,
          start_date: '2020-04-01',
          end_date: '2020-04-30'
        }
      ]
    };
  }

  getCompaignVCR(params) {
    return {
      data: [{
        id: 1863,
        campaign: 'Tremor-Hearts&Science-WBHE-Friends_Always_On_CTV',
        CID: '2532',
        trafficking_total_impressions: 550572,
        trafficking_total_video_completes: 532662,
        trafficking_total_vcr: 96.75
      },
      {
        id: 1863,
        campaign: 'Tremor-Hearts&Science-WBHE-Friends_Always_On_CTV',
        CID: '2533',
        trafficking_total_impressions: 550572,
        trafficking_total_video_completes: 532662,
        trafficking_total_vcr: 96.75
      }]
    };
  }

  getCompaignCTR(params) {
    return {
      data: [{
        id: 1809,
        campaign: 'Tremor-Vizeum_USA-Ohio_Lottery-Ohio_Lottery_Responsible_Gambling',
        CID: '2466',
        trafficking_total_impressions: 2237472,
        trafficking_total_clicks: 52883,
        trafficking_total_ctr: 2.36,
      },
      {
        id: 1809,
        campaign: 'Tremor-Vizeum_USA-Ohio_Lottery-Ohio_Lottery_Responsible_Gambling',
        CID: '2467',
        trafficking_total_impressions: 2237472,
        trafficking_total_clicks: 52883,
        trafficking_total_ctr: 3.36,
      }
      ]
    };
  }

  getCompaignVCROnCID(params) {
    return {
      data: [{
        server_li_id: '17503',
        line_item: 'WBHE-Friends_Always_On_CTV-P1-CTV',
        campaign: 'Tremor-Hearts&Science-WBHE-Friends_Always_On_CTV',
        CID: '2532',
        trafficking_total_impressions: 192167,
        trafficking_total_video_completes: 187086,
        trafficking_total_vcr: 97.36,

      },
      {
        server_li_id: '17503',
        line_item: 'WBHE-Friends_Always_On_CTV-P1-CTV',
        campaign: 'Tremor-Hearts&Science-WBHE-Friends_Always_On_CTV',
        CID: '2532',
        trafficking_total_impressions: 192167,
        trafficking_total_video_completes: 187086,
        trafficking_total_vcr: 97.36,

      }]
    };
  }

  getCompaignCTROnCID(params) {
    return {
      data: [{
        server_li_id: '17799',
        line_item: 'Volvo-AS074632_-_Volvo_Car_USA_(MindShare_NY__New_York):_Volvo_T2_2020_-_Miami-L00106164-F-138084-P2-Desktop-30s',
        campaign: 'Tremor-Mindshare_USA-Volvo-AS074632_-_Volvo_Car_USA_(MindShare_NY__New_York):_Volvo_T2_2020_-_Miami',
        CID: '2573',
        trafficking_total_impressions: 12,
        trafficking_total_clicks: 2,
        trafficking_total_ctr: 16.67
      },
      {
        server_li_id: '17799',
        line_item: 'Volvo-AS074632_-_Volvo_Car_USA_(MindShare_NY__New_York):_Volvo_T2_2020_-_Miami-L00106164-F-138084-P2-Desktop-30s',
        campaign: 'Tremor-Mindshare_USA-Volvo-AS074632_-_Volvo_Car_USA_(MindShare_NY__New_York):_Volvo_T2_2020_-_Miami',
        CID: '2573',
        trafficking_total_impressions: 12,
        trafficking_total_clicks: 2,
        trafficking_total_ctr: 16.67
      }]
    };
  }

  getExportReportData(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/common/exportData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
    // return <TreeNode[]> json.data;
  }
  getChildData(params: object) {
    const url = `${this.BASE_URL}/dummyData/childTable`;
    return this.http.get(url, params);
    // return <TreeNode[]> json.data;
  }
  getLastUpdatedData(appId) {
    const url = `${this.BASE_URL}/frankly/v1/common/getLastUpdatedData/${appId}`;
    return this.http.get(url);
  }
}
