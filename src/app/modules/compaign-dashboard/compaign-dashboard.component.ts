import { Component, OnInit, OnDestroy } from '@angular/core';
import { TreeNode } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { CommonLibService } from 'src/app/_services';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';
import { FetchApiDataService } from './fetch-api-data.service';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { Subscription } from 'rxjs';
import { SendEmailComponent } from '../send-email/send-email.component';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
@Component({
  selector: 'ym-compaign-dashboard',
  templateUrl: './compaign-dashboard.component.html',
  styleUrls: ['./compaign-dashboard.component.scss']
})
export class CompaignDashboardComponent implements OnInit , OnDestroy{
  private appConfigObs: Subscription;

  appConfig: object = {};
  lastUpdatedOn: Date;
  nextUpdated: Date;
  dataUpdatedThrough: Date;
  cardsJson = [];
  showCards = true;
  isPopupGroupbyView = false;
  isPopupChartView = false;
  aggTableData: TreeNode[];
  noTableData = false;
  dimColDef: any[];
  //  metricColDef: any[];
  aggTableColumnDef: any[];
  aggTableJson: Object;
  displayAggTable = true;

  showMainPieChartSource = false;
  mainPieChartJson: object;
  noDataMainPieChart = false;

  campaignsAssignedToMeTableData: TreeNode[];
  campaignsAssignedToMeTableJson: Object;
  campaignsAssignedToColDef: any[];

  endingLineItemTableData: TreeNode[];
  endingLineItemTableJson: Object;
  endingLineItemColDef: any[];

  startingLineItemTableData: TreeNode[];
  startingLineItemTableJson: Object;
  startingLineItemColDef: any[];

  campaignPacingAheadTableData: TreeNode[];
  campaignPacingAheadTableJson: Object;
  campaignPacingAheadColDef: any[];

  campaignPacingBehindTableData: TreeNode[];
  campaignPacingBehindTableJson: Object;
  campaignPacingBehindColDef: any[];

  campaignVCRTableData: TreeNode[];
  campaignVCRTableJson: Object;
  campaignVCRColDef: any[];

  campaignCTRTableData: TreeNode[];
  campaignCTRTableJson: Object;
  campaignCTRColDef: any[];

  filtersApplied: object = {};
  exportRequest: ExportRequest = <ExportRequest>{};

  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private htmltoimage: HtmltoimageService,
    private exportService: ExportdataService,
    private pptExport: ExportPptService,
    private formatNumPipe: FormatNumPipe,
  ) { }


  ngOnInit() {
    const data = this.dataFetchServ.getTableData('param');
    console.log('d---', data);


    this.campaignsAssignedToColDef = [
      {
        field: 'CID',
        displayName: 'CID',
        format: '',
        width: '120',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'campaign',
        displayName: 'Campaign Name',
        format: '',
        width: '270',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'start_date',
        displayName: 'Start Date',
        format: '',
        width: '150',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'end_date',
        displayName: 'End Date',
        format: '',
        width: '170',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'c_status',
        displayName: 'Active/Inactive',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      }
    ];

    this.campaignsAssignedToMeTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      reload: false,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,

      columns: this.libServ.deepCopy(this.campaignsAssignedToColDef),
      selectedColumns: this.libServ.deepCopy(this.campaignsAssignedToColDef),
      // frozenCols: this.libServ.deepCopy([...this.dimColDef.slice(0, 3)]),
      // frozenWidth:
      //   this.dimColDef
      //     .slice(0, 3)
      //     .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: false,
      columnSearch: false
    };

    this.endingLineItemColDef = [
      {
        field: 'CID',
        displayName: 'CID',
        format: '',
        width: '100',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'server_li_id',
        displayName: 'LID',
        format: '',
        width: '100',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'line_item',
        displayName: 'Line Item Name',
        format: '',
        width: '300',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'start_date',
        displayName: 'Start Date',
        format: '',
        width: '120',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'end_date',
        displayName: 'End Date',
        format: '',
        width: '120',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'l_status',
        displayName: 'Active/Inactive',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'trafficking_delivery_progress',
        displayName: 'Alphonso Progress Duration',
        format: 'percentage',
        width: '170',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'billing_delivery_progress',
        displayName: '3rd Party Delivery Progress',
        format: 'percentage',
        width: '150',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'campaign_duration_progress',
        displayName: 'Campaign Duration Progress',
        format: 'percentage',
        width: '175',
        value: '',
        condition: '',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'impressions_per_day',
        displayName: 'Impressions Per Day',
        format: 'number',
        width: '150',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
    ];

    this.endingLineItemTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      reload: false,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,

      columns: this.libServ.deepCopy(this.endingLineItemColDef),
      selectedColumns: this.libServ.deepCopy(this.endingLineItemColDef),
      // frozenCols: this.libServ.deepCopy([...this.dimColDef.slice(0, 3)]),
      // frozenWidth:
      //   this.dimColDef
      //     .slice(0, 3)
      //     .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: false,
      columnSearch: false
    };

    this.startingLineItemColDef = [
      {
        field: 'campaign_id',
        displayName: 'CID',
        format: '',
        width: '100',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'campaign_name',
        displayName: 'Campaign Name',
        format: '',
        width: '270',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'line_item_id',
        displayName: 'LID',
        format: '',
        width: '100',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'line_item_name',
        displayName: 'Line Item Name',
        format: '',
        width: '270',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'start_date',
        displayName: 'Start Date',
        format: '',
        width: '120',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'end_date',
        displayName: 'End Date',
        format: '',
        width: '120',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'active',
        displayName: 'Active/Inactive',
        width: '100',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'account_id',
        displayName: 'Account ID',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      }
    ];

    this.startingLineItemTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      reload: false,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,

      columns: this.libServ.deepCopy(this.startingLineItemColDef),
      selectedColumns: this.libServ.deepCopy(this.startingLineItemColDef),
      // frozenCols: this.libServ.deepCopy([...this.dimColDef.slice(0, 3)]),
      // frozenWidth:
      //   this.dimColDef
      //     .slice(0, 3)
      //     .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: false,
      columnSearch: false
    };

    this.campaignPacingAheadColDef = [
      {
        field: 'CID',
        displayName: 'CID',
        format: '',
        width: '100',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'server_li_id',
        displayName: 'LID',
        format: '',
        width: '100',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'line_item',
        displayName: 'Line Item Name',
        format: '',
        width: '300',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'start_date',
        displayName: 'Start Date',
        format: '',
        width: '120',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'end_date',
        displayName: 'End Date',
        format: '',
        width: '120',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'trafficking_delivery_progress',
        displayName: 'Alp. Del. Progress ',
        format: 'percentage',
        width: '150',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'billing_delivery_progress',
        displayName: '3rd Party Delivery Progress',
        format: 'percentage',
        width: '150',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'campaign_duration_progress',
        displayName: 'Campaign Duration Progress',
        format: 'percentage',
        width: '150',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'impressions_per_day',
        displayName: 'Impressions Per Day',
        format: 'number',
        width: '150',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
    ];

    this.campaignPacingAheadTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      reload: false,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,

      columns: this.libServ.deepCopy(this.campaignPacingAheadColDef),
      selectedColumns: this.libServ.deepCopy(this.campaignPacingAheadColDef),
      // frozenCols: this.libServ.deepCopy([...this.dimColDef.slice(0, 3)]),
      // frozenWidth:
      //   this.dimColDef
      //     .slice(0, 3)
      //     .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: false,
      columnSearch: false
    };

    this.campaignPacingBehindColDef = [
      {
        field: 'CID',
        displayName: 'CID',
        format: '',
        width: '100',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'server_li_id',
        displayName: 'LID',
        format: '',
        width: '100',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'line_item',
        displayName: 'Line Item Name',
        format: '',
        width: '300',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'start_date',
        displayName: 'Start Date',
        format: '',
        width: '120',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'end_date',
        displayName: 'End Date',
        format: '',
        width: '120',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'trafficking_delivery_progress',
        displayName: 'Alp. Del. Progress ',
        format: 'percentage',
        width: '150',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'billing_delivery_progress',
        displayName: '3rd Party Delivery Progress',
        format: 'percentage',
        width: '150',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'campaign_duration_progress',
        displayName: 'Campaign Duration Progress',
        format: 'percentage',
        width: '150',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'impressions_per_day',
        displayName: 'Impressions Per Day',
        format: 'number',
        width: '150',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
    ];

    this.campaignPacingBehindTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      reload: false,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,

      columns: this.libServ.deepCopy(this.campaignPacingBehindColDef),
      selectedColumns: this.libServ.deepCopy(this.campaignPacingBehindColDef),
      // frozenCols: this.libServ.deepCopy([...this.dimColDef.slice(0, 3)]),
      // frozenWidth:
      //   this.dimColDef
      //     .slice(0, 3)
      //     .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: false,
      columnSearch: false
    };


    this.campaignVCRColDef = [
      {
        field: 'trafficking_total_impressions',
        displayName: 'Impressions',
        format: 'number',
        width: '150',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'trafficking_total_video_completes',
        displayName: 'Completes',
        format: 'number',
        width: '150',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'trafficking_total_vcr',
        displayName: 'VCR',
        width: '155',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'percentage',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      }
    ];

    this.campaignVCRTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      reload: false,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,

      columns: this.libServ.deepCopy(this.campaignVCRColDef),
      selectedColumns: this.libServ.deepCopy(this.campaignVCRColDef),
      // frozenCols: this.libServ.deepCopy([...this.dimColDef.slice(0, 3)]),
      // frozenWidth:
      //   this.dimColDef
      //     .slice(0, 3)
      //     .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: false,
      columnSearch: false
    };

    this.campaignCTRColDef = [
      {
        field: 'trafficking_total_impressions',
        displayName: 'Impressions',
        format: 'number',
        width: '150',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'trafficking_total_clicks',
        displayName: 'Clicks',
        format: 'number',
        width: '170',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'trafficking_total_ctr',
        displayName: 'CTR',
        width: '155',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'percentage',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      }
    ];

    this.campaignCTRTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      reload: false,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,

      columns: this.libServ.deepCopy(this.campaignCTRColDef),
      selectedColumns: this.libServ.deepCopy(this.campaignCTRColDef),
      // frozenCols: this.libServ.deepCopy([...this.dimColDef.slice(0, 3)]),
      // frozenWidth:
      //   this.dimColDef
      //     .slice(0, 3)
      //     .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: false,
      columnSearch: false
    };


    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        console.log('appConfig', this.appConfig);

        this._titleService.setTitle(this.appConfig['displayName']);
        let startDate;
        let endDate;
        // const date_config = this.appConfig['filter']['filterConfig']['filters'][
        //   'datePeriod'
        // ][0];
        // if ((date_config['defaultDate'][0]['value'] === 0 && date_config['defaultDate'][0]['period'] === 'month') && (moment().format('DD') === '01' || moment().format('DD') === '02')) {
        //   startDate = moment().subtract(1, 'months').startOf('month');
        //   endDate = moment().subtract(1, 'months').endOf('month');
        // } else {
        //   if (
        //     date_config[
        //     'defaultDate'
        //     ][0]['startOf']
        //   ) {
        //     startDate = moment()
        //       .subtract(
        //         date_config['defaultDate'][0]['value'],
        //         date_config['defaultDate'][0]['period']
        //       )
        //       .startOf(
        //         date_config['defaultDate'][0]['period']
        //       );
        //   } else {
        //     startDate = moment().subtract(
        //       date_config['defaultDate'][0]['value'],
        //       date_config['defaultDate'][0]['period']
        //     );
        //   }
        //   endDate = moment().subtract(
        //     date_config[
        //     'defaultDate'
        //     ][1]['value'],
        //     date_config[
        //     'defaultDate'
        //     ][1]['period']
        //   );
        // }

        this.filtersApplied = {
          timeKeyFilter: {
            // time_key1: startDate.format('YYYYMMDD'),
            // time_key2: endDate.format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: [{ key: 'station_group' }] // this.appConfig["filter"]["filterConfig"]["groupBy"].filter(
          // v => v.selected
          // )
        };
        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        this.initialLoading();
      }
    });
  }

  initialLoading() {
    this.dataFetchServ
    .getLastUpdatedData(this.appConfig['id'])
    .subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }
      this.lastUpdatedOn = moment(data[0]['updated_at']).toDate();
      this.nextUpdated = moment(data[0]['next_run_at']).toDate();
      this.dataUpdatedThrough = moment(
        data[0]['source_updated_through'],
        'YYYYMMDD'
      ).toDate();
    });


    this.loadMainPieChart();
    this.AssignedCompaignTableData();
    this.endingLineItemLoadTableData();
    this.startingLineItemLoadTableData();
    this.campaignpacingAheadLoadTableData();
    this.campaignpacingBehindLoadTableData();
    this.campaignVCRLoadTableData();
    this.campaignCTRLoadTableData();
  }

  loadMainPieChart() {
    // const mainPieChartReqSource = {
    //   dimensions: ['source'],
    //   metrics: ['gross_revenue'],
    //   derived_metrics: [],
    //   timeKeyFilter: this.libServ.deepCopy(
    //     this.filtersApplied['timeKeyFilter']
    //   ),
    //   filters: this.libServ.deepCopy(this.filtersApplied['filters']),
    //   groupByTimeKey: {
    //     key: ['time_key', 'accounting_key'],
    //     interval: 'daily'
    //   },
    //   gidGroupBy: ['source'],
    //   orderBy: [{ key: 'gross_revenue', opcode: 'desc' }],
    //   limit: '5',
    //   offset: '0'
    // };
    // mainPieChartReqSource['orderBy'] = [
    //   { key: 'gross_revenue', opcode: 'desc' }
    // ];

    this.mainPieChartJson = {
      chartTypes: [{ key: 'doughnut', label: 'Pie Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Campaigns Running'
        },
        legend: {
          display: false
        },
        elements: {
          arc: {
            borderWidth: 0
          }
        },
        plugins: {
          datalabels: {
            formatter: (value, ctx) => {
              let sum = 0;
              const dataArr = ctx.chart.data.datasets[0].data;
              dataArr.map(data => {
                sum += data;
              });
              const percentage = (value * 100) / sum;
              // if (percentage > 10) {
              //   return `${this.formatNumPipe.transform(
              //     value,
              //     '$',
              //     []
              //   )} - ${percentage.toFixed(2)} %`;
              // } else {
              //   return '';
              // }
              if (percentage > 10) {
                return `${percentage.toFixed(2)} %`;
              } else {
                return '';
              }
            },
            color: 'black'
          }
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };
    this.mainPieChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          let sum = 0;
          const dataArr = data.datasets[0].data;
          dataArr.map(data => {
            sum += data;
          });
          const percentage = (currentValue * 100) / sum;
          return `${
            data.labels[tooltipItem.index]
            } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
        }
      }
    };
    this.showMainPieChartSource = false;
    // this.dataFetchServ.getUPRData(mainPieChartReqSource).subscribe(data => {
    const data = this.dataFetchServ.getCampaignStatus('mainPieChartReqSource');
    {
      const chartData = data['data'];
      const arr = [];
      arr.push(chartData[0]['active']);
      arr.push(chartData[0]['total'] - chartData[0]['active']);
      console.log('data--of pie', chartData);

      this.noDataMainPieChart = false;
      if (!chartData.length) {
        this.noDataMainPieChart = true;
        return;
      }
      // const sourceArr = Array.from(new Set(chartData.map(s => s['source'])));
      // const sourceArr = Array.from(new Set(chartData));
      const sourceArr = ['Live', 'Paused'];
      const colors = ['green', 'yellow'];
      this.mainPieChartJson['chartData']['labels'] = sourceArr;
      this.mainPieChartJson['chartData']['datasets'][0]['data'] = arr;
      this.mainPieChartJson['chartData']['datasets'][0][
        'backgroundColor'
      ] = colors;
      this.showMainPieChartSource = true;
    }
  }

  AssignedCompaignTableData() {
    // const tableReq = {
    //   dimensions: ['station_group'], // [this.filtersApplied['groupby'][0]['key']],
    //   metrics: [
    //     'dp_impressions',
    //     'gross_revenue',
    //     'total_expenses',
    //     'station_revenue_share_distribution',
    //     'total_recoupable_expenses'
    //   ],
    //   derived_metrics: ['client_net_share', 'gross_ecpm', 'client_net_ecpm'],
    //   timeKeyFilter: this.libServ.deepCopy(
    //     this.filtersApplied['timeKeyFilter']
    //   ),
    //   filters: this.libServ.deepCopy(this.filtersApplied['filters']),
    //   groupByTimeKey: {
    //     key: ['accounting_key', 'time_key'],
    //     interval: 'daily'
    //   },
    //   gidGroupBy: ['station_group'], // grpBys,
    //   orderBy: [{ key: 'accounting_key', opcode: 'desc' }],
    //   limit: '',
    //   offset: ''
    // };
    this.campaignsAssignedToMeTableJson['loading'] = true;
    // this.dataFetchServ.getPayoutData(params).subscribe(data => {
    const data = this.dataFetchServ.getTableData('param');
    {
      console.log('data---', data);

      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      } else {
        this.noTableData = false;
      }
      const tableData = data['data'];
      const arr = [];
      tableData.forEach((row: object) => {
        arr.push({
          data: row,
          children: [{ data: {} }]
        });
      });
      this.campaignsAssignedToMeTableData = <TreeNode[]>arr;
      this.campaignsAssignedToMeTableJson['totalRecords'] = arr.length;
      setTimeout(() => {
        this.campaignsAssignedToMeTableJson['loading'] = false;
      });
    }


  }

  endingLineItemLoadTableData() {
    // const tableReq = {
    //   dimensions: ['station_group'], // [this.filtersApplied['groupby'][0]['key']],
    //   metrics: [
    //     'dp_impressions',
    //     'gross_revenue',
    //     'total_expenses',
    //     'station_revenue_share_distribution',
    //     'total_recoupable_expenses'
    //   ],
    //   derived_metrics: ['client_net_share', 'gross_ecpm', 'client_net_ecpm'],
    //   timeKeyFilter: this.libServ.deepCopy(
    //     this.filtersApplied['timeKeyFilter']
    //   ),
    //   filters: this.libServ.deepCopy(this.filtersApplied['filters']),
    //   groupByTimeKey: {
    //     key: ['accounting_key', 'time_key'],
    //     interval: 'daily'
    //   },
    //   gidGroupBy: ['station_group'], // grpBys,
    //   orderBy: [{ key: 'accounting_key', opcode: 'desc' }],
    //   limit: '',
    //   offset: ''
    // };
    this.endingLineItemTableJson['loading'] = true;
    // this.dataFetchServ.getPayoutData(params).subscribe(data => {
    const data = this.dataFetchServ.getEndingLineItem('tableReq');
    {
      console.log('data---', data);

      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      } else {
        this.noTableData = false;
      }
      const tableData = data['data'];
      const arr = [];
      tableData.forEach((row: object) => {
        arr.push({
          data: row,
          children: [{ data: {} }]
        });
      });
      this.endingLineItemTableData = <TreeNode[]>arr;
      this.endingLineItemTableJson['totalRecords'] = arr.length;
      setTimeout(() => {
        this.endingLineItemTableJson['loading'] = false;
      });
    }


  }

  startingLineItemLoadTableData() {
    // const tableReq = {
    //   dimensions: ['station_group'], // [this.filtersApplied['groupby'][0]['key']],
    //   metrics: [
    //     'dp_impressions',
    //     'gross_revenue',
    //     'total_expenses',
    //     'station_revenue_share_distribution',
    //     'total_recoupable_expenses'
    //   ],
    //   derived_metrics: ['client_net_share', 'gross_ecpm', 'client_net_ecpm'],
    //   timeKeyFilter: this.libServ.deepCopy(
    //     this.filtersApplied['timeKeyFilter']
    //   ),
    //   filters: this.libServ.deepCopy(this.filtersApplied['filters']),
    //   groupByTimeKey: {
    //     key: ['accounting_key', 'time_key'],
    //     interval: 'daily'
    //   },
    //   gidGroupBy: ['station_group'], // grpBys,
    //   orderBy: [{ key: 'accounting_key', opcode: 'desc' }],
    //   limit: '',
    //   offset: ''
    // };
    this.endingLineItemTableJson['loading'] = true;
    // this.dataFetchServ.getPayoutData(params).subscribe(data => {
    const data = this.dataFetchServ.getStartingLineItem('tableReq');
    {
      console.log('data---', data);

      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      } else {
        this.noTableData = false;
      }
      const tableData = data['data'];
      const arr = [];
      tableData.forEach((row: object) => {
        arr.push({
          data: row,
          children: [{ data: {} }]
        });
      });
      this.startingLineItemTableData = <TreeNode[]>arr;
      this.startingLineItemTableJson['totalRecords'] = arr.length;
      setTimeout(() => {
        this.startingLineItemTableJson['loading'] = false;
      });
    }


  }

  campaignpacingAheadLoadTableData() {
    // const tableReq = {
    //   dimensions: ['station_group'], // [this.filtersApplied['groupby'][0]['key']],
    //   metrics: [
    //     'dp_impressions',
    //     'gross_revenue',
    //     'total_expenses',
    //     'station_revenue_share_distribution',
    //     'total_recoupable_expenses'
    //   ],
    //   derived_metrics: ['client_net_share', 'gross_ecpm', 'client_net_ecpm'],
    //   timeKeyFilter: this.libServ.deepCopy(
    //     this.filtersApplied['timeKeyFilter']
    //   ),
    //   filters: this.libServ.deepCopy(this.filtersApplied['filters']),
    //   groupByTimeKey: {
    //     key: ['accounting_key', 'time_key'],
    //     interval: 'daily'
    //   },
    //   gidGroupBy: ['station_group'], // grpBys,
    //   orderBy: [{ key: 'accounting_key', opcode: 'desc' }],
    //   limit: '',
    //   offset: ''
    // };
    this.campaignPacingAheadTableJson['loading'] = true;
    // this.dataFetchServ.getPayoutData(params).subscribe(data => {
    const data = this.dataFetchServ.getCompaignPacingAhead('tableReq');
    {
      console.log('data---', data);

      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      } else {
        this.noTableData = false;
      }
      const tableData = data['data'];
      const arr = [];
      tableData.forEach((row: object) => {
        arr.push({
          data: row,
          children: [{ data: {} }]
        });
      });
      this.campaignPacingAheadTableData = <TreeNode[]>arr;
      this.campaignPacingAheadTableJson['totalRecords'] = arr.length;
      setTimeout(() => {
        this.campaignPacingAheadTableJson['loading'] = false;
      });
    }


  }

  campaignpacingBehindLoadTableData() {
    // const tableReq = {
    //   dimensions: ['station_group'], // [this.filtersApplied['groupby'][0]['key']],
    //   metrics: [
    //     'dp_impressions',
    //     'gross_revenue',
    //     'total_expenses',
    //     'station_revenue_share_distribution',
    //     'total_recoupable_expenses'
    //   ],
    //   derived_metrics: ['client_net_share', 'gross_ecpm', 'client_net_ecpm'],
    //   timeKeyFilter: this.libServ.deepCopy(
    //     this.filtersApplied['timeKeyFilter']
    //   ),
    //   filters: this.libServ.deepCopy(this.filtersApplied['filters']),
    //   groupByTimeKey: {
    //     key: ['accounting_key', 'time_key'],
    //     interval: 'daily'
    //   },
    //   gidGroupBy: ['station_group'], // grpBys,
    //   orderBy: [{ key: 'accounting_key', opcode: 'desc' }],
    //   limit: '',
    //   offset: ''
    // };
    this.campaignPacingBehindTableJson['loading'] = true;
    // this.dataFetchServ.getPayoutData(params).subscribe(data => {
    const data = this.dataFetchServ.getCompaignPacingBehind('tableReq');
    {
      console.log('data---', data);

      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      } else {
        this.noTableData = false;
      }
      const tableData = data['data'];
      const arr = [];
      tableData.forEach((row: object) => {
        arr.push({
          data: row,
          children: [{ data: {} }]
        });
      });
      this.campaignPacingBehindTableData = <TreeNode[]>arr;
      this.campaignPacingBehindTableJson['totalRecords'] = arr.length;
      setTimeout(() => {
        this.campaignPacingBehindTableJson['loading'] = false;
      });
    }


  }

  campaignVCRLoadTableData() {
    // const tableReq = {
    //   dimensions: ['station_group'], // [this.filtersApplied['groupby'][0]['key']],
    //   metrics: [
    //     'dp_impressions',
    //     'gross_revenue',
    //     'total_expenses',
    //     'station_revenue_share_distribution',
    //     'total_recoupable_expenses'
    //   ],
    //   derived_metrics: ['client_net_share', 'gross_ecpm', 'client_net_ecpm'],
    //   timeKeyFilter: this.libServ.deepCopy(
    //     this.filtersApplied['timeKeyFilter']
    //   ),
    //   filters: this.libServ.deepCopy(this.filtersApplied['filters']),
    //   groupByTimeKey: {
    //     key: ['accounting_key', 'time_key'],
    //     interval: 'daily'
    //   },
    //   gidGroupBy: ['station_group'], // grpBys,
    //   orderBy: [{ key: 'accounting_key', opcode: 'desc' }],
    //   limit: '',
    //   offset: ''
    // };
    const colDef = this.libServ.deepCopy(this.campaignVCRColDef);
    console.log('colDef', colDef);
    colDef.unshift(
      {
        field: 'CID',
        displayName: 'CID',
        format: '',
        width: '100',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'campaign',
        displayName: 'Campaign Name',
        format: '',
        width: '270',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
    );
    console.log('colDef', colDef);
    this.campaignVCRTableJson['columns'] = colDef;
    this.campaignVCRTableJson['selectedColumns'] = colDef;

    this.campaignVCRTableJson['loading'] = true;
    // this.dataFetchServ.getPayoutData(params).subscribe(data => {
    const data = this.dataFetchServ.getCompaignVCR('tableReq');
    {
      console.log('data---', data);

      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      } else {
        this.noTableData = false;
      }
      const tableData = data['data'];
      const arr = [];
      tableData.forEach((row: object) => {
        arr.push({
          data: row,
          children: [{ data: {} }]
        });
      });
      this.campaignVCRTableData = <TreeNode[]>arr;
      this.campaignVCRTableJson['totalRecords'] = arr.length;
      setTimeout(() => {
        this.campaignVCRTableJson['loading'] = false;
      });
    }


  }

  campaignCTRLoadTableData() {
    // const tableReq = {
    //   dimensions: ['station_group'], // [this.filtersApplied['groupby'][0]['key']],
    //   metrics: [
    //     'dp_impressions',
    //     'gross_revenue',
    //     'total_expenses',
    //     'station_revenue_share_distribution',
    //     'total_recoupable_expenses'
    //   ],
    //   derived_metrics: ['client_net_share', 'gross_ecpm', 'client_net_ecpm'],
    //   timeKeyFilter: this.libServ.deepCopy(
    //     this.filtersApplied['timeKeyFilter']
    //   ),
    //   filters: this.libServ.deepCopy(this.filtersApplied['filters']),
    //   groupByTimeKey: {
    //     key: ['accounting_key', 'time_key'],
    //     interval: 'daily'
    //   },
    //   gidGroupBy: ['station_group'], // grpBys,
    //   orderBy: [{ key: 'accounting_key', opcode: 'desc' }],
    //   limit: '',
    //   offset: ''
    // };
    const colDef = this.libServ.deepCopy(this.campaignCTRColDef);
    console.log('colDef', colDef);
    colDef.unshift(
      {
        field: 'CID',
        displayName: 'CID',
        format: '',
        width: '100',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'campaign',
        displayName: 'Campaign Name',
        format: '',
        width: '270',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
    );
    console.log('colDef', colDef);
    this.campaignCTRTableJson['columns'] = colDef;
    this.campaignCTRTableJson['selectedColumns'] = colDef;

    this.campaignCTRTableJson['loading'] = true;
    // this.dataFetchServ.getPayoutData(params).subscribe(data => {
    const data = this.dataFetchServ.getCompaignCTR('tableReq');
    {
      console.log('data---', data);

      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      } else {
        this.noTableData = false;
      }
      const tableData = data['data'];
      const arr = [];
      tableData.forEach((row: object) => {
        arr.push({
          data: row,
          children: [{ data: {} }]
        });
      });
      this.campaignCTRTableData = <TreeNode[]>arr;
      this.campaignCTRTableJson['totalRecords'] = arr.length;
      setTimeout(() => {
        this.campaignCTRTableJson['loading'] = false;
      });
    }


  }

  campaignVCROnCIDLoadTableData() {
    // const tableReq = {
    //   dimensions: ['station_group'], // [this.filtersApplied['groupby'][0]['key']],
    //   metrics: [
    //     'dp_impressions',
    //     'gross_revenue',
    //     'total_expenses',
    //     'station_revenue_share_distribution',
    //     'total_recoupable_expenses'
    //   ],
    //   derived_metrics: ['client_net_share', 'gross_ecpm', 'client_net_ecpm'],
    //   timeKeyFilter: this.libServ.deepCopy(
    //     this.filtersApplied['timeKeyFilter']
    //   ),
    //   filters: this.libServ.deepCopy(this.filtersApplied['filters']),
    //   groupByTimeKey: {
    //     key: ['accounting_key', 'time_key'],
    //     interval: 'daily'
    //   },
    //   gidGroupBy: ['station_group'], // grpBys,
    //   orderBy: [{ key: 'accounting_key', opcode: 'desc' }],
    //   limit: '',
    //   offset: ''
    // };
    // this.campaignCTRTableJson['columns'] =
    const colDef = this.libServ.deepCopy(this.campaignVCRColDef);
    console.log('colDef', colDef);
    colDef.unshift(
      {
        field: 'server_li_id',
        displayName: 'LID',
        width: '100',
        exportConfig: {
          format: '',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'line_item',
        displayName: 'Line Item Name',
        width: '270',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      }
    );
    console.log('colDef', colDef);
    this.campaignVCRTableJson['columns'] = colDef;
    this.campaignVCRTableJson['selectedColumns'] = colDef;

    this.campaignVCRTableJson['loading'] = true;
    // this.dataFetchServ.getPayoutData(params).subscribe(data => {
    const data = this.dataFetchServ.getCompaignVCROnCID('tableReq');
    {
      console.log('data---', data);

      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      } else {
        this.noTableData = false;
      }
      const tableData = data['data'];
      const arr = [];
      tableData.forEach((row: object) => {
        arr.push({
          data: row,
          children: [{ data: {} }]
        });
      });
      this.campaignVCRTableData = <TreeNode[]>arr;
      this.campaignVCRTableJson['totalRecords'] = arr.length;
      setTimeout(() => {
        this.campaignVCRTableJson['loading'] = false;
      });
    }


  }

  campaignCTROnCIDLoadTableData() {
    // const tableReq = {
    //   dimensions: ['station_group'], // [this.filtersApplied['groupby'][0]['key']],
    //   metrics: [
    //     'dp_impressions',
    //     'gross_revenue',
    //     'total_expenses',
    //     'station_revenue_share_distribution',
    //     'total_recoupable_expenses'
    //   ],
    //   derived_metrics: ['client_net_share', 'gross_ecpm', 'client_net_ecpm'],
    //   timeKeyFilter: this.libServ.deepCopy(
    //     this.filtersApplied['timeKeyFilter']
    //   ),
    //   filters: this.libServ.deepCopy(this.filtersApplied['filters']),
    //   groupByTimeKey: {
    //     key: ['accounting_key', 'time_key'],
    //     interval: 'daily'
    //   },
    //   gidGroupBy: ['station_group'], // grpBys,
    //   orderBy: [{ key: 'accounting_key', opcode: 'desc' }],
    //   limit: '',
    //   offset: ''
    // };
    // this.campaignCTRTableJson['columns'] =
    const colDef = this.libServ.deepCopy(this.campaignCTRColDef);
    console.log('colDef', colDef);
    colDef.unshift(
      {
        field: 'server_li_id',
        displayName: 'LID',
        format: '',
        width: '120',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'line_item',
        displayName: 'Line Item Name',
        format: '',
        width: '250',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
    );
    console.log('colDef', colDef);
    this.campaignCTRTableJson['columns'] = colDef;
    this.campaignCTRTableJson['selectedColumns'] = colDef;

    this.campaignCTRTableJson['loading'] = true;
    // this.dataFetchServ.getPayoutData(params).subscribe(data => {
    const data = this.dataFetchServ.getCompaignCTROnCID('tableReq');
    {
      console.log('data---', data);

      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      } else {
        this.noTableData = false;
      }
      const tableData = data['data'];
      const arr = [];
      tableData.forEach((row: object) => {
        arr.push({
          data: row,
          children: [{ data: {} }]
        });
      });
      this.campaignCTRTableData = <TreeNode[]>arr;
      this.campaignCTRTableJson['totalRecords'] = arr.length;
      setTimeout(() => {
        this.campaignCTRTableJson['loading'] = false;
      });
    }

  }

  sendEmail() {
    const imageConfig = {
      imgName: `${this.appConfig['name']}.png`,
      type: 'image/png'
    };

    this.htmltoimage
      .canvasImage(document.getElementById('capture'), imageConfig)
      .then(canvas => {
        this.dialogService.open(SendEmailComponent, {
          header: 'Email Screenshot',
          contentStyle: {
            'max-height': '80vh',
            width: '30vw',
            overflow: 'auto'
          },
          data: canvas.toDataURL('image/png')
        });
      });
  }
  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }


  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['name']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    } else if (type === 'ppt') {
      const chartElements = document.getElementsByTagName('ym-charts');
      const slides = [];
      [].forEach.call(chartElements, (el, index) => {
        const c = el.getElementsByTagName('canvas')[0];

        slides.push({
          title: `${this.appConfig['name']}`,
          base64data: this.libServ.canvasToImage('white', c),
          width: 8,
          height: 3
        });

        if (index === chartElements.length - 1) {
          const pptConfig = {
            filename: 'Charts',
            logo: {
              visible: true,
              path: '/assets/images/ym.png',
              width: 0.53125,
              height: 0.458333
            },
            slides: slides
          };
          this.pptExport.exportPPT(pptConfig);
        }
      });
    }
    return false;
  }

}
