import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompaignDashboardComponent } from './compaign-dashboard.component';

describe('CompaignDashboardComponent', () => {
  let component: CompaignDashboardComponent;
  let fixture: ComponentFixture<CompaignDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompaignDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompaignDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
