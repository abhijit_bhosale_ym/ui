import { Routes } from '@angular/router';

import { GoalSettingAppComponent } from './goal-setting-app.component';
export const GoalSettingAppRoutes: Routes = [
  {
    path: '',
    component: GoalSettingAppComponent
  }
];
