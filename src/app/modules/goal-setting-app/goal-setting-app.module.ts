import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FilterContainerModule } from '../common/filter-container/filter-container.module';
import { GoalSettingAppRoutes } from './goal-setting-app.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { SharedModule } from 'src/app/_pipes/shared.module';
import { ChatModule } from '../common/chat/chat.module';
import { TreeTableModule } from 'primeng/treetable';
import { MultiSelectModule } from 'primeng/multiselect';
import { InputSwitchModule } from 'primeng/inputswitch';
import { GoalSettingAppComponent } from './goal-setting-app.component';
import { DropdownModule } from 'primeng/dropdown';

@NgModule({
  declarations: [GoalSettingAppComponent],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    FilterContainerModule,
    TreeTableModule,
    MultiSelectModule,
    InputSwitchModule,
    ChatModule,
    DropdownModule,
    RouterModule.forChild(GoalSettingAppRoutes)
  ],
  providers: [FormatNumPipe]
})
export class GoalSettingAppModule {}
