import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GoalSettingDailyDataComponent } from './daily-data.component';

describe('GoalSettingDailyDataComponent', () => {
  let component: GoalSettingDailyDataComponent;
  let fixture: ComponentFixture<GoalSettingDailyDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GoalSettingDailyDataComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GoalSettingDailyDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
