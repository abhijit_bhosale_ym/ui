import { Component, OnInit } from '@angular/core';

import { CommonLibService } from 'src/app/_services';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { FetchApiDataService } from '../fetch-api-data.service';
import * as moment from 'moment';
import { TreeNode } from 'primeng/api';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-daily-data',
  templateUrl: './daily-data.component.html',
  styleUrls: ['./daily-data.component.scss']
})
export class GoalSettingDailyDataComponent implements OnInit {
  tableDataReq: object;
  tableData: TreeNode[];
  tableColumnDef: any[];
  tableJson: Object;
  rowdata: object;
  apiData: any[];
  activeView: any;
  impressionChartVisited = false;
  CTRChartVisited = false;
  showImpLineChart = false;
  showCTRLineChart = false;
  ctrChartJson: object;
  impressionsChartJson: object;
  defaultChartsJson: object;
  exportRequest: ExportRequest = <ExportRequest>{};
  isExportReport = false;
  isKpi = false;
  flatTableData: TreeNode[];
  flatTableColumnDef: any[];
  flatTableJson: Object;
  isPacing = false;
  isCpr = false;
  statusList = ['Pacing', 'Performance'];
  statusLists = this.statusList.map(name => {
    return { label: name, value: name };
  });
  campaignName;


  constructor(
    private libServ: CommonLibService,
    private formatNumPipe: FormatNumPipe,
    private dataFetchServ: FetchApiDataService,
    private config: DynamicDialogConfig,
    private exportService: ExportdataService,
    private toastService: ToastService
  ) {
    this.rowdata = this.config.data['row'];
    this.isKpi = this.config.data['isKpi'];
    this.isCpr = this.config.data['isCpr'];
    this.campaignName = this.rowdata['drill_name']
  }

  ngOnInit() {
    this.exportRequest = this.config.data['exportRequest'];
    this.isExportReport = this.config.data['isExportReport'];

    if(this.isCpr)
    {
      this.tableColumnDef = [
        {
          field: 'time_key',
          displayName: 'Date',
          width: '120',
          exportConfig: {
            format: 'date',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          format: 'date',
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: true
          }
        },
        {
          field: 'impressions',
          displayName: 'Impressions',
          width: '100',
          exportConfig: {
            format: 'number',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          format: 'number',
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: true
          }
        },
        {
          field: '3rd_party_impressions',
          displayName: '3rd Party Impressions',
          width: '96',
          exportConfig: {
            format: 'number',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          format: 'number',
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: true
          }
        },
        {
          field: 'clicks',
          displayName: 'Clicks',
          width: '65',
          exportConfig: {
            format: 'number',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          format: 'number',
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: true
          }
        },
        {
          field: '3rd_party_clicks',
          displayName: '3rd Party Clicks',
          width: '65',
          exportConfig: {
            format: 'number',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          format: 'number',
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: true
          }
        },
        {
          field: 'ctr',
          displayName: 'CTR',
          width: '70',
          exportConfig: {
            format: 'percentage',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          format: 'percentage',
          formatConfig: [2],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: true
          }
        },
        {
          field: '3rd_party_ctr',
          displayName: '3rd Party CTR',
          width: '70',
          exportConfig: {
            format: 'percentage',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          format: 'percentage',
          formatConfig: [2],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: true
          }
        },
        {
          field: 'cpm',
          displayName: 'CPM',
          width: '65',
          exportConfig: {
            format: 'currency',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          format: '$',
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: true
          }
        },
        {
          field: 'revenue',
          displayName: 'Revenue',
          width: '80',
          exportConfig: {
            format: 'currency',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          format: '$',
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: true
          }
        },
        {
          field: '3rd_party_revenue',
          displayName: '3rd Party Revenue',
          width: '80',
          exportConfig: {
            format: 'currency',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          format: '$',
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: true
          }
        }
      ];
      this.tableJson = {
        page_size: 10,
        lazy: false,
        loading: false,
        export: true,
        sortMode: 'multiple',
        resizableColumns: true,
        columnResizeMode: 'fit',
        reorderableColumns: true,
        scrollHeight: '300px',
        totalRecords: 1000,
  
        columns: this.tableColumnDef,
        selectedColumns: this.tableColumnDef,
        scrollable: true,
        selectionMode: 'multiple',
        selectedColsModal: [],
        selectionDataKey: 'name',
        metaKeySelection: true,
        showHideCols: false,
        overallSearch: false,
        columnSearch: false
      };
    }
    else{
    this.tableColumnDef = [
      {
        field: 'time_key',
        displayName: 'Date',
        width: '100',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'date',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'campaign',
        displayName: 'Campaign',
        format: '',
        width: '170',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }

      },
      {
        field: 'line_item',
        displayName: 'Line Item ',
        format: '',
        width: '160',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
        {
          field: 'subject',
          displayName: 'Subject',
          format: '',
          width: '160',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }

      },

      {
        field: 'recommandation',
        displayName: 'Recommendations',
        format: '',
        width: '300',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }

    },
    {
      field: 'apply',
      displayName: 'Apply',
      format: '',
      width: '100',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: false
      }

  },

  {
    field: 'reject',
    displayName: 'Reject',
    format: '',
    width: '95',
    exportConfig: {
      format: 'string',
      styleinfo: {
        thead: 'default',
        tdata: 'white'
      }
    },
    formatConfig: [],
    options: {
      editable: false,
      colSearch: false,
      colSort: true,
      resizable: true,
      movable: false
    }

},
{
  field: 'user',
  displayName: 'User',
  format: '',
  width: '90',
  exportConfig: {
    format: 'string',
    styleinfo: {
      thead: 'default',
      tdata: 'white'
    }
  },
  formatConfig: [],
  options: {
    editable: false,
    colSearch: false,
    colSort: true,
    resizable: true,
    movable: false
  }
},
{
  field: 'application_status',
  displayName: 'Application Status',
  format: '',
  width: '190',
  exportConfig: {
    format: 'string',
    styleinfo: {
      thead: 'default',
      tdata: 'white'
    }
  },
  formatConfig: [],
  options: {
    editable: false,
    colSearch: false,
    colSort: true,
    resizable: true,
    movable: false
  }

},

{
  field: 'comments',
  displayName: 'Comments',
  format: '',
  width: '130',
  exportConfig: {
    format: 'string',
    styleinfo: {
      thead: 'default',
      tdata: 'white'
    }
  },
  formatConfig: [],
  options: {
    editable: false,
    colSearch: false,
    colSort: true,
    resizable: true,
    movable: false
  }

},


    ];
    this.tableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '300px',
      totalRecords: 1000,

      columns: this.tableColumnDef,
      selectedColumns: this.tableColumnDef,
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: false,
      overallSearch: false,
      columnSearch: false
    };
  }
    this.loadTable();
    this.defaultChartsJson = {
      chartTypes: [
        { key: 'line', label: 'Linear Trend' },
        { key: 'bar', label: 'Stack Trend', stacked: true }
      ],
      chartData: {
        labels: [],
        datasets: []
      },
      chartOptions: {
        title: {
          display: true,
          text: ''
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              display: true,
              scaleLabel: {
                display: true
              }
            }
          ]
        },
        pan: {
          enabled: true,
          mode: 'x'
        },
        zoom: {
          enabled: true,
          mode: 'x'
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: true,
      chartWidth: '',
      chartHeight: '300px'
    };
  }

  loadTable() {
    let grpby = [];
    let filters = {};
    if (typeof this.rowdata['$$treeLevel'] !== 'undefined') {
      filters = {
        dimensions: [
          {
            key: 'order_name',
            values: [this.rowdata['order_name']]
          }
        ],
        metrics: []
      };
      grpby = ['advertiser', 'order_name', 'source', 'trafficker'];
    } else {
      filters = {
        dimensions: [
          {
            key: 'lineitem',
            values: [this.rowdata['lineitem']]
          }
        ],
        metrics: []
      };
      grpby = [
        'advertiser',
        'costtype',
        'line_item_priority',
        'lineitem',
        'lineitemtype',
        'order_name',
        'source',
        'trafficker'
      ];
    }
    this.tableJson['loading'] = true;
    this.tableDataReq = {
      dimensions: [],
      metrics: [],
      derived_metrics: [],
      timeKeyFilter: [],
      filters: filters,
      groupByTimeKey: {
        key: ['time_key'],
        interval: 'daily'
      },
      gidGroupBy: grpby,
      orderBy: [{ key: 'time_key', opcode: 'desc' }],
      limit: '0',
      offset: '-1'
    };
    if(this.isCpr){
    this.dataFetchServ
      .getTableData(this.tableDataReq, this.activeView)
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          return;
        }

        this.apiData = data['data'];

        const arr = [];
        this.apiData.forEach((row: object) => {
          const obj = {
            data: row
          };
          arr.push(obj);
        });

        this.tableData = <TreeNode[]>arr;
        this.tableJson['totalRecords'] = this.apiData.length;
        this.tableJson['loading'] = false;
      });
    }
else{
  let data = {};
  if(!this.isPacing){
   data = {
     data : [
       {
         "time_key" : 20201218,
         "campaign" : this.rowdata['drill_name'],        
         "line_item" : "VW-RON-Desktop-300x600",
         "subject" : "Increase Daily budget cap",
         "recommandation" : "Current pacing is 60%. In order to reach goal, it is recommanded to increase daily cap from 5,000 to 25,000 Impressions	",
         "application_status" : "Success | 12-18-2020	03:21",
         "user" : "Aditya"
       },
       {
         "time_key" : 20201217,
         "campaign" : this.rowdata['drill_name'],        
         "line_item" : "Ad-Q4-Mobile-300x250",
         "subject" : "Add new Ad Units",
         "recommandation" : "Given the current pacing, it is suggested to add following Ad Units to this campaign. Ad Unit : Top_300x250, Post_970x250"	,
         "application_status" : "Success | 12-17-2020	21:20",
         "user" : "Padam"
       }
     ]
   }
 }
 if(this.isPacing){
   data = {
     data : [
       {
         "time_key" : 20201218,
         "campaign" : this.rowdata['drill_name'],        
         "line_item" : "MR-RON-Desktop-300x600",
         "subject" : "Less CTR for Marriott Campaign",
         "recommandation" : "To improve the CTR - Add better performing ad Unit (MId_300x600)	",
         "application_status" : "Success | 12-18-2020	03:21",
         "user" : "Aditya"
       },
       {
         "time_key" : 20201217,
         "campaign" : this.rowdata['drill_name'],        
         "line_item" : "Ty-Q4-Mobile-300x250",
         "subject" : "Low conversion rate for Toyota",
         "recommandation" : "Change targeting Country from California to Las Vegas"	,
         "application_status" : "Success | 12-17-2020	05:11",
         "user" : "Padam"
       }
     ]
   }
 }

 this.apiData = data['data'];

 const arr = [];
 this.apiData.forEach((row: object) => {
   const obj = {
     data: row
   };
   arr.push(obj);
 });

 this.tableData = <TreeNode[]>arr;
 this.tableJson['totalRecords'] = this.apiData.length;
 this.tableJson['loading'] = false;


}

  }

  exportTablePopup(fileFormat) {
    if (this.exportRequest['sendEmail'].length > 0) {
      this.toastService.displayToast({
        severity: 'info',
        summary: 'Export Report',
        detail:
          'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
        life: 10000
      });
      const columnDefs = this.tableColumnDef;
      const req = this.libServ.deepCopy(this.tableDataReq);
      req['limit'] = '0';
      req['offset'] = '-1';
      const sheetDetailsarray = [];
      const sheetlineDetails = {};
      sheetlineDetails['columnDef'] = columnDefs;
      sheetlineDetails['data'] = [];
      sheetlineDetails['sheetName'] = 'Daily Distribution';
      sheetlineDetails['isRequest'] = true;
      sheetlineDetails['request'] = {
        url: '',
        method: 'POST',
        param: req
      };
      sheetlineDetails['disclaimer'] = [
        {
          position: 'bottom',
          label: 'Note: Data present in the table may vary over a period of time.',
          color: '#000000'
        }
      ];
      sheetlineDetails['totalFooter'] = {
        available: false,
        custom: false
      };
      sheetlineDetails['tableTitle'] = {
        available: false,
        label: `
        ${
          typeof this.rowdata['$$treeLevel'] !== 'undefined'
            ? this.rowdata['order_name']
            : this.rowdata['lineitem']
          } Daily Distribution`
      };
      sheetlineDetails['image'] = [];

      sheetDetailsarray.push(sheetlineDetails);
      this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
      this.exportRequest['fileName'] =
        `
      ${
        typeof this.rowdata['$$treeLevel'] !== 'undefined'
          ? this.rowdata['order_name']
          : this.rowdata['lineitem']
        } Daily Distribution` + moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
      this.exportRequest['exportFormat'] = fileFormat;
      this.exportRequest['exportConfig'] = environment.exportConfig;

      // return false;
      this.dataFetchServ
        .getExportReportData(this.exportRequest)
        .subscribe(response => {
          console.log(response);
        });
    } else {
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Export Report',
        detail:
          'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email.Thanks!',
        life: 10000
      });
    }
  }

  tabChanged(e) {
    console.log(e);
    if (this.isCpr && e.index === 1 && !this.impressionChartVisited) {
      this.impressionChartVisited = true;
      this.loadImpressionchart();
    } else if (this.isCpr && e.index === 2 && !this.CTRChartVisited) {
      this.CTRChartVisited = true;
      this.loadCTRChart();
    }
    if(!this.isCpr && e.index == 1){
      this.isPacing = true;
      this.loadTable()
    }
    else{
      this.isPacing = false;
      this.loadTable();
    }
  }

  loadImpressionchart() {
    this.showImpLineChart = false;
    const colors = this.libServ.dynamicColors(2);
    this.impressionsChartJson = this.libServ.deepCopy(this.defaultChartsJson);
    this.impressionsChartJson['chartOptions']['title']['text'] =
      'Impressions Trend';
    this.impressionsChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
      callback: (value, index, values) => {
        return this.formatNumPipe.transform(value, 'number', []);
      }
    };
    this.impressionsChartJson['chartOptions']['scales']['yAxes'][0][
      'scaleLabel'
    ]['labelString'] = 'Impressions';
    this.impressionsChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return `${
            data.datasets[tooltipItem.datasetIndex].label
            } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
        }
      }
    };
    const chartData = this.apiData;
    const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));
    this.impressionsChartJson['chartData']['labels'] = datesArr.map(d =>
      moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
    );
    this.impressionsChartJson['chartData']['datasets'] = [
      {
        label: 'Impressions',
        borderColor: colors[0],
        fill: false,
        backgroundColor: colors[0],
        data: []
      },
      {
        label: '3rd Party Impressions',
        borderColor: colors[1],
        fill: false,
        backgroundColor: colors[1],
        data: []
      }
    ];

    const impArr = [];
    const threePImpArr = [];
    datesArr.forEach(time_key => {
      chartData.forEach(r => {
        if (r['time_key'] === time_key) {
          impArr.push(parseInt(r['impressions'], 10));
          threePImpArr.push(parseInt(r['3rd_party_impressions'], 10));
        }
      });
    });
    this.impressionsChartJson['chartData']['datasets'][0]['data'] = impArr;
    this.impressionsChartJson['chartData']['datasets'][1][
      'data'
    ] = threePImpArr;
    this.showImpLineChart = true;
  }

  loadCTRChart() {
    this.showCTRLineChart = false;
    const colors = this.libServ.dynamicColors(2);
    this.ctrChartJson = this.libServ.deepCopy(this.defaultChartsJson);
    this.ctrChartJson['chartOptions']['title']['text'] = 'CTR Trend';
    this.ctrChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
      callback: (value, index, values) => {
        return this.formatNumPipe.transform(value, 'percentage', [0]);
      }
    };
    this.ctrChartJson['chartOptions']['scales']['yAxes'][0]['scaleLabel'][
      'labelString'
    ] = 'CTR (%)';
    this.ctrChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return `${
            data.datasets[tooltipItem.datasetIndex].label
            } : ${this.formatNumPipe.transform(currentValue, 'percentage', [2])}`;
        }
      }
    };
    const chartData = this.apiData;
    const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));
    this.ctrChartJson['chartData']['labels'] = datesArr.map(d =>
      moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
    );
    this.ctrChartJson['chartData']['datasets'] = [
      {
        label: 'CTR',
        borderColor: colors[0],
        fill: false,
        backgroundColor: colors[0],
        data: []
      },
      {
        label: '3rd Party CTR',
        borderColor: colors[1],
        fill: false,
        backgroundColor: colors[1],
        data: []
      }
    ];

    const CTRArr = [];
    const threePCTRArr = [];
    datesArr.forEach(time_key => {
      chartData.forEach(r => {
        if (r['time_key'] === time_key) {
          CTRArr.push(parseFloat(r['ctr']));
          threePCTRArr.push(parseFloat(r['3rd_party_ctr']));
        }
      });
    });
    this.ctrChartJson['chartData']['datasets'][0]['data'] = CTRArr;
    this.ctrChartJson['chartData']['datasets'][1]['data'] = threePCTRArr;
    this.showCTRLineChart = true;
  }

  isHiddenColumn(col) { }
}
