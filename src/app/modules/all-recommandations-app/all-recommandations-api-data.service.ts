import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FetchUnfilledApiDataService {
  private BASE_URL: string = environment.baseUrl;

  constructor(private http: HttpClient) {}
  getData() {
    const url = `${this.BASE_URL}/frankly/v1/data-pipeline/getData`;
    return this.http.get(url, { responseType: 'json'});
  }



  getExportReportData(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/common/exportData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
    // return <TreeNode[]> json.data;
  }
}
