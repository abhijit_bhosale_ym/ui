import { Component, OnInit, OnDestroy } from '@angular/core';
import { TreeNode } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { CommonLibService } from 'src/app/_services';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';
import { FetchUnfilledApiDataService } from './all-recommandations-api-data.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { Subscription } from 'rxjs';
import { SendEmailComponent } from '../send-email/send-email.component';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';

@Component({
  selector: 'ym-all-recommandations-app',
  templateUrl: './all-recommandations-app.component.html',
  styleUrls: ['./all-recommandations-app.component.scss']
})
export class AllRecommandationsAppComponent implements OnInit, OnDestroy {
  appConfig: object = {};
  lastUpdatedOn: Date;

  aggTableData: TreeNode[];
  dimColDef: any[];
  metricColDef: any[];
  aggTableColumnDef: any[];
  aggTableJson: Object;
  displayAggTable = false;
  appConfigObs: Subscription;
  cars: any[];
  rowGroupMetadata: any;
  tableData: TreeNode[];
  tableColumnDef: any[];
  tableJson: Object;

  flatTableData: TreeNode[];
  flatTableColumnDef: any[];
  flatTableJson: Object;
  imgSrcJson: Object;
  exportRequest: ExportRequest = <ExportRequest>{};
  isExportReport = false;

  searchvalue = "";
  searchColDef = ['updated_at', 'source', 'name', 'source_link', 'source_updated_through'];
  newData: any[];
  isPacing = false;
  apiData = []
  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: FetchUnfilledApiDataService,
    private toastService: ToastService,
    private htmltoimage: HtmltoimageService,
    private pptExport: ExportPptService,
    private exportService: ExportdataService,
    private dialogService: DialogService
  ) { }

  ngOnInit() {
    this.tableColumnDef = [
      {
        field: 'time_key',
        displayName: 'Date',
        width: '100',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'date',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'campaign',
        displayName: 'Campaign',
        format: '',
        width: '145',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }

      },
      {
        field: 'line_item',
        displayName: 'Line Item ',
        format: '',
        width: '160',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
        {
          field: 'subject',
          displayName: 'Subject',
          format: '',
          width: '160',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }

      },

      {
        field: 'recommandation',
        displayName: 'Recommendations',
        format: '',
        width: '300',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }

    },
    {
      field: 'apply',
      displayName: 'Apply',
      format: '',
      width: '90',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: false
      }

  },

  {
    field: 'reject',
    displayName: 'Reject',
    format: '',
    width: '95',
    exportConfig: {
      format: 'string',
      styleinfo: {
        thead: 'default',
        tdata: 'white'
      }
    },
    formatConfig: [],
    options: {
      editable: false,
      colSearch: false,
      colSort: true,
      resizable: true,
      movable: false
    }
},
{
  field: 'user',
  displayName: 'User',
  format: '',
  width: '90',
  exportConfig: {
    format: 'string',
    styleinfo: {
      thead: 'default',
      tdata: 'white'
    }
  },
  formatConfig: [],
  options: {
    editable: false,
    colSearch: false,
    colSort: true,
    resizable: true,
    movable: false
  }

},

{
  field: 'application_status',
  displayName: 'Application Status',
  format: '',
  width: '190',
  exportConfig: {
    format: 'string',
    styleinfo: {
      thead: 'default',
      tdata: 'white'
    }
  },
  formatConfig: [],
  options: {
    editable: false,
    colSearch: false,
    colSort: true,
    resizable: true,
    movable: false
  }

},

{
  field: 'comments',
  displayName: 'Comments',
  format: '',
  width: '130',
  exportConfig: {
    format: 'string',
    styleinfo: {
      thead: 'default',
      tdata: 'white'
    }
  },
  formatConfig: [],
  options: {
    editable: false,
    colSearch: false,
    colSort: true,
    resizable: true,
    movable: false
  }

},


    ];
    this.tableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '300px',
      totalRecords: 1000,

      columns: this.tableColumnDef,
      selectedColumns: this.tableColumnDef,
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: false,
      overallSearch: false,
      columnSearch: false
    };
    this.loadTable();

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        this.isExportReport = this.appConfig['permissions'].some(
          o => o.name == 'data-pipeline-export-report'
        );
        this._titleService.setTitle(this.appConfig['displayName']);
        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        this.initialLoading();
      }
    });
  }

  initialLoading() {
    this.dataFetchServ.getData().subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }
      this.rowGroupMetadata = {};
      const data2 = data as [];
      const colorArr = [
        '#b3e5fc',
        '#ffccbc',
        '#b9f6ca',
        '#cfd8dc',
        '#e1bee7',
        '#b2dfdb'
      ];
      const arr = [];

      for (const r of data2) {
        const obj = { data: { rowspan: {} } };

        obj['data'] = r;

        // obj['data']['imgsrc'] =
        //   '../../assets/source_logos/' + this.imgSrcJson[r['source']]['src'];
        // obj['data']['link'] = this.imgSrcJson[r['source']]['link'];

        obj['data']['imgsrc'] =
          '../../assets/source_logos/';
        // obj['data']['link'] = this.imgSrcJson[r['source']]['link'];
        obj['data']['link'] = r['source_link'];

        if (r['status'] === 'done') {
          obj['data']['status'] = 'fav fa fa-check-square text-success';
        } else if (r['status'] === 'stopped') {
          obj['data']['status'] = 'fav fa fa-ban text-danger';
        } else if (r['status'] === 'inprogress') {
          obj['data']['status'] = 'fav fa fa-exclamation-triangle text-warning';
        }

        obj['data']['source_updated_through'] = moment(
          r['source_updated_through'],
          'YYYYMMDD'
        ).format('MM-DD-YYYY');

        arr.push(obj);
      }

      let index = 0;
      let counter = 0;
      if (arr) {
        for (let i = 0; i < arr.length; i++) {
          const rowData = arr[i].data;
          const appname = rowData.name;
          if (i === 0) {
            this.rowGroupMetadata[appname] = {
              index: 0,
              size: 1,
              isRow: 'false'
            };
            arr[i].data['rowspan'] = 1;
            arr[i].data['color'] = colorArr[counter];
            index++;
          } else {
            const previousRowData = arr[i - 1]['data'];
            const previousRowGroup = previousRowData.name;
            if (appname === previousRowGroup) {
              this.rowGroupMetadata[appname].size++;
              arr[i].data['color'] = colorArr[counter];
            } else {
              const obj = {};
              this.rowGroupMetadata[appname] = {
                index: i,
                size: 1,
                isRow: 'false'
              };
              counter++;
              arr[i].data['color'] = colorArr[counter];
            }
          }
        }
      }
      this.newData = arr;
      this.flatTableData = arr;
      this.flatTableJson['loading'] = false;
    });
  }
  loadTable() {
    let grpby = [];
    let filters = {};
    let data = {};
   if(!this.isPacing){
    data = {
      data : [
        {
          "time_key" : 20201217,
          "campaign" : "Mariott Q4 2020",
          "line_item" : "MR-RON-Desktop-300x600",
          "subject" : "Less CTR for Marriott Campaign",
          "recommandation" : "To improve the CTR - Add better performing ad Unit (MId_300x600)",
          "application_status" : "Success | 12-17-2020 13:21",
          "user" : "Aditya"
        },
        {
          "time_key" : 20201216,
          "campaign" : "Toyota Q4 2020",
          "line_item" : "Ty-Q4-Mobile-300x250",
          "subject" : "Add new Ad Units",
          "recommandation" : "Given the current pacing, it is suggested to add following Ad Units to this campaign. Ad Unit : Top_300x250, Post_970x250"		,
          "application_status" : "Success | 12-16-2020 14:32",
          "user" : "Padam"
        },
        {
          "time_key" : 20201215,
          "campaign" : "Volkswagen Q4 2020",
          "line_item" : "VW-RON-Desktop-300x250",
          "subject" : "Increase Daily budget cap",
          "recommandation" : "Current pacing is 60%. In order to reach goal, it is recommanded to increase daily cap from 5,000 to 25,000 Impressions"		,
          "application_status" : "Success | 12-15-2020 07:34",
          "user" : "Vijayalaxmi"
        },
        {
          "time_key" : 20201214,
          "campaign" : "Adidas Q4 2020",
          "line_item" : "Ad-Q4-Mobile-320x50",
          "subject" : "Add new Ad Units",
          "recommandation" : "Given the current pacing, it is suggested to add following Ad Units to this campaign. Ad Unit : Top_300x250, Post_970x250"			,
          "application_status" : "Success | 12-14-2020 05:15",
          "user" : "Aditya"
        }
      ]
    }
  }
  if(this.isPacing){
    data = {
      data : [
        {
          "time_key" : 20201217,
          "campaign" : "Volkswagen Q3 2020",
          "line_item" : "VW-RON-Desktop-300x600",
          "subject" : "Increase Daily budget cap",
          "recommandation" : "Current pacing is 60%. In order to reach goal, it is recommanded to increase daily cap from 5,000 to 25,000 Impressions	",
          "application_status" : "Success | 12-17-2020 03:21",
          "user" : "Aditya"
        },
        {
          "time_key" : 20201216,
          "campaign" : "Adidas Q4 2020",
          "line_item" : "Ad-Q4-Mobile-300x250",
          "subject" : "Low conversion rate for Toyota",
          "recommandation" : "Change targeting Country from California to Las Vegas"	,
          "application_status" : "Success | 12-16-2020 10:11",
          "user" : "Padam"
        }
      ]
    }
  }
        this.apiData = data['data'];

        const arr = [];
        this.apiData.forEach((row: object) => {
          const obj = {
            data: row
          };
          arr.push(obj);
        });

        this.tableData = <TreeNode[]>arr;
        this.tableJson['totalRecords'] = this.apiData.length;
        this.tableJson['loading'] = false;
//      });
  }

  tabChanged(e) {
    if(e.index == 1){
      this.isPacing = true;
      this.loadTable()
    }
    else{
      this.isPacing = false;
      this.loadTable();
    }
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['name']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    }
    return false;
  }

  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }

  onFiltersApplied(filterData: object) {

  }

  sendEmail() {
    const imageConfig = {
      imgName: `${this.appConfig['name']}.png`,
      type: 'image/png'
    };

    this.htmltoimage
      .canvasImage(document.getElementById('capture'), imageConfig)
      .then(canvas => {
        this.dialogService.open(SendEmailComponent, {
          header: 'Email Screenshot',
          contentStyle: {
            'max-height': '80vh',
            width: '30vw',
            overflow: 'auto'
          },
          data: canvas.toDataURL('image/png')
        });
      });
  }
}
