import { Routes } from '@angular/router';
import { AllRecommandationsAppComponent } from './all-recommandations-app.component';

export const AllRecommandationsAppRoutesAppRoutes: Routes = [
  {
    path: '',
    component: AllRecommandationsAppComponent
  }
];
