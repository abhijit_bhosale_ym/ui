import { Component, OnInit, OnDestroy } from '@angular/core';
import { TreeNode } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { CommonLibService, DataShareService } from 'src/app/_services';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';
import { FetchApiDataService } from './fetch-api-data.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { groupBy, filter } from 'rxjs/operators';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { Subscription } from 'rxjs';
import { SendEmailComponent } from '../send-email/send-email.component';

@Component({
  selector: 'ym-dashboard-freecycle',
  templateUrl: './dashboard-freecycle.component.html',
  styleUrls: ['./dashboard-freecycle.component.scss']
})
export class DashboardFreeCycleComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;
  noTableData = false;
  columns: object[];
  filtersApplied: object = {};
  showMainLineChartSource = false;
  noDataMainLineChart = false;
  showMainPieChartSource = false;
  showCharts = false;
  defaultChartsJson: object;
  mainLineChartJson: object;
  mainPieChartJson: object;
  showMainLineChartStation = false;
  noDataMainLineChartStation = false;
  showMainPieChartStation = false;
  mainLineChartJsonStation: object;
  mainPieChartJsonStation: object;
  showAdSizeLineChart = false;
  noDataAdSizeLineChart = false;
  showAdSizePieChart = false;
  adSizeLineChartJson: object;
  adSizePieChartJson: object;
  showDeviceTypeLineChart = false;
  noDataDeviceTypeLineChart = false;
  showDeviceTypePieChart = false;
  deviceTypeLineChartJson: object;
  deviceTypePieChartJson: object;
  showUnfilledLineChart = false;
  noDataUnfilledLineChart = false;
  showUnfilledPieChart = false;
  unfilledLineChartJson: object;
  unfilledPieChartJson: object;
  showUnfilledLostRevenueLineChart = false;
  noDataUnfilledLostRevenueLineChart = false;
  unfilledLostRevenueLineChartJson: object;
  noDataMainPieChart = false;
  noDataPieChartStation = false;
  noDataAdSizePieChart = false;
  noDataDeviceTypePieChart = false;
  base64URL;
  appConfig: object = {};

  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private formatNumPipe: FormatNumPipe,
    private exportService: ExportdataService,
    private pptExport: ExportPptService,
    private htmltoimage: HtmltoimageService // private exportService: ExportdataService
  ) {}

  ngOnInit() {
    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        this._titleService.setTitle(this.appConfig['name']);
        let startDate;
        let endDate;
        const date_config = this.appConfig['filter']['filterConfig']['filters'][
          'datePeriod'
        ][0];
        if (
          date_config['defaultDate'][0]['value'] === 0 &&
          date_config['defaultDate'][0]['period'] === 'month' &&
          (moment().format('DD') === '01' || moment().format('DD') === '02')
        ) {
          startDate = moment()
            .subtract(1, 'months')
            .startOf('month');
          endDate = moment()
            .subtract(1, 'months')
            .endOf('month');
        } else {
          if (date_config['defaultDate'][0]['startOf']) {
            startDate = moment()
              .subtract(
                date_config['defaultDate'][0]['value'],
                date_config['defaultDate'][0]['period']
              )
              .startOf(date_config['defaultDate'][0]['period']);
          } else {
            startDate = moment().subtract(
              date_config['defaultDate'][0]['value'],
              date_config['defaultDate'][0]['period']
            );
          }
          endDate = moment().subtract(
            date_config['defaultDate'][1]['value'],
            date_config['defaultDate'][1]['period']
          );
        }

        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: startDate.format('YYYYMMDD'),
            time_key2: endDate.format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: this.appConfig['filter']['filterConfig']['groupBy']
        };

        this.initialLoading();
      }
    });
  }

  initialLoading() {
    this.loadMainLineChart();

    this.loadMainPieChart();

    this.loadAdSizeLineChart();

    this.loadAdSizePieChart();

    this.loadDeviceTypeLineChart();

    this.loadDeviceTypePieChart();

    this.loadUnfilledLineChart();

    this.loadUnfilledLostRevenueLineChart();
  }

  loadMainPieChart() {
    const mainPieChartReqSource = {
      dimensions: ['source'],
      metrics: ['dp_revenue'],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [{ key: 'dp_revenue', opcode: 'desc' }],
      limit: '5',
      offset: '0'
    };
    mainPieChartReqSource['orderBy'] = [
      { key: 'dp_revenue', opcode: 'desc' }
    ];

    this.mainPieChartJson = {
      chartTypes: [{ key: 'pie', label: 'Pie Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Revenue Distribution Across Top 5 Demand Partner'
        },
        legend: {
          display: false
        },
        elements: {
          arc: {
            borderWidth: 0
          }
        },
        plugins: {
          datalabels: {
            formatter: (value, ctx) => {
              let sum = 0;
              const dataArr = ctx.chart.data.datasets[0].data;
              dataArr.map(data => {
                sum += data;
              });
              const percentage = (value * 100) / sum;
              // if (percentage > 10) {
              //   return `${this.formatNumPipe.transform(
              //     value,
              //     '$',
              //     []
              //   )} - ${percentage.toFixed(2)} %`;
              // } else {
              //   return '';
              // }
              if (percentage > 10) {
                return `${percentage.toFixed(2)} %`;
              } else {
                return '';
              }
            },
            color: 'black'
          }
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };
    this.mainPieChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          let sum = 0;
          const dataArr = data.datasets[0].data;
          dataArr.map(data => {
            sum += data;
          });
          const percentage = (currentValue * 100) / sum;
          return `${
            data.labels[tooltipItem.index]
          } : ${this.formatNumPipe.transform(
            currentValue,
            '$',
            []
          )} - ${percentage.toFixed(2)} %`;
        }
      }
    };
    this.showMainPieChartSource = false;
    this.dataFetchServ.getUPRData(mainPieChartReqSource).subscribe(data => {
      const chartData = data['data'];
      this.noDataMainPieChart = false;
      if (!chartData.length) {
        this.noDataMainPieChart = true;
        return;
      }
      const sourceArr = Array.from(new Set(chartData.map(s => s['source'])));
      const colors = this.libServ.dynamicColors(sourceArr.length);
      this.mainPieChartJson['chartData']['labels'] = sourceArr;
      this.mainPieChartJson['chartData']['datasets'][0]['data'] = Array.from(
        new Set(chartData.map(s => s['dp_revenue']))
      );
      this.mainPieChartJson['chartData']['datasets'][0][
        'backgroundColor'
      ] = colors;
      this.showMainPieChartSource = true;
    });
  }

  loadMainLineChart() {
    const mainLineChartReqSource = {
      dimensions: ['source'],
      metrics: ['dp_revenue', 'dp_impressions'],
      derived_metrics: ['rev_cpm'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      // orderBy: [],
      orderBy: [{ key: 'rev_cpm', opcode: 'desc' }],
      limit: '5',
      offset: '0'
    };
    const colors = this.libServ.dynamicColors(2);
    this.mainLineChartJson = {
      chartTypes: [{ key: 'bar', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'eCPM',
            type: 'bar',
            yAxisID: 'y-axis-1',

            borderColor: colors[1],
            fill: false,
            backgroundColor: colors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Top 5 Demand Partners By eCPM'
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Sources'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-1',
              scaleLabel: {
                display: true,
                labelString: 'eCPM($)'
              },
              position: 'left',
              name: '2',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', []);
                }
              }
              // scaleFontColor: "rgba(151,187,205,0.8)"
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
              } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    this.showMainLineChartSource = false;
    this.dataFetchServ.getUPRData(mainLineChartReqSource).subscribe(data => {
      const chartData = data['data'];
      this.noDataMainLineChart = false;
      if (!chartData.length) {
        this.noDataMainLineChart = true;
        return;
      }
      const sourceArr = Array.from(new Set(chartData.map(r => r['source'])));

      // this.mainLineChartJson['chartData']['labels'] = chartData.map(r => r['source']);
      this.mainLineChartJson['chartData']['labels'] = sourceArr.map(d => d);

      const revArr = [];
      const ecpmArr = [];
      sourceArr.forEach(source => {
        chartData.forEach(r => {
          if (r['source'] === source) {
            ecpmArr.push(r['rev_cpm']);
          }
        });
      });
      this.mainLineChartJson['chartData']['datasets'][0]['data'] = ecpmArr;
      this.showMainLineChartSource = true;
    });
  }

  loadAdSizeLineChart() {
    const adSizeLineChartReqAdSize = {
      dimensions: ['creative_size'],
      metrics: ['dp_revenue', 'dp_impressions'],
      derived_metrics: ['rev_cpm'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['creative_size'],
      // orderBy: [],
      orderBy: [{ key: 'rev_cpm', opcode: 'desc' }],
      limit: '5',
      offset: '0'
    };
    const colors = this.libServ.dynamicColors(2);
    this.adSizeLineChartJson = {
      chartTypes: [{ key: 'bar', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'eCPM',
            type: 'bar',
            yAxisID: 'y-axis-1',

            borderColor: colors[1],
            fill: false,
            backgroundColor: colors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Top 5 Best Performing Ad Sizes By eCPM'
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Ad Sizes'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-1',
              scaleLabel: {
                display: true,
                labelString: 'eCPM($)'
              },
              position: 'left',
              name: '2',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', []);
                }
              }
              // scaleFontColor: "rgba(151,187,205,0.8)"
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
              } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    this.showAdSizeLineChart = false;
    this.dataFetchServ.getUPRData(adSizeLineChartReqAdSize).subscribe(data => {
      const chartData = data['data'];
      this.noDataAdSizeLineChart = false;
      if (!chartData.length) {
        this.noDataAdSizeLineChart = true;
        return;
      }
      const sourceArr = Array.from(
        new Set(chartData.map(r => r['creative_size']))
      );

      // this.mainLineChartJson['chartData']['labels'] = chartData.map(r => r['source']);
      this.adSizeLineChartJson['chartData']['labels'] = sourceArr.map(d => d);

      const revArr = [];
      const ecpmArr = [];
      sourceArr.forEach(creative_size_rpt_name => {
        chartData.forEach(r => {
          if (r['creative_size'] === creative_size_rpt_name) {
            ecpmArr.push(r['rev_cpm']);
          }
        });
      });
      this.adSizeLineChartJson['chartData']['datasets'][0]['data'] = ecpmArr;
      this.showAdSizeLineChart = true;
    });
  }

  loadAdSizePieChart() {
    const adSizePieChartReq = {
      dimensions: ['creative_size'],
      metrics: ['dp_revenue'],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['creative_size'],
      orderBy: [{ key: 'dp_revenue', opcode: 'desc' }],
      limit: '5',
      offset: '0'
    };
    adSizePieChartReq['orderBy'] = [{ key: 'dp_revenue', opcode: 'desc' }];

    this.adSizePieChartJson = {
      chartTypes: [{ key: 'pie', label: 'Pie Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Revenue Distribution Across Top 5 Ad Sizes'
        },
        legend: {
          display: false
        },
        elements: {
          arc: {
            borderWidth: 0
          }
        },
        plugins: {
          datalabels: {
            formatter: (value, ctx) => {
              let sum = 0;
              const dataArr = ctx.chart.data.datasets[0].data;
              dataArr.map(data => {
                sum += data;
              });
              const percentage = (value * 100) / sum;
              // if (percentage > 10) {
              //   return `${this.formatNumPipe.transform(
              //     value,
              //     '$',
              //     []
              //   )} - ${percentage.toFixed(2)} %`;
              // } else {
              //   return '';
              // }
              if (percentage > 10) {
                return `${percentage.toFixed(2)} %`;
              } else {
                return '';
              }
            },
            color: 'black'
          }
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };
    this.adSizePieChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          let sum = 0;
          const dataArr = data.datasets[0].data;
          dataArr.map(data => {
            sum += data;
          });
          const percentage = (currentValue * 100) / sum;
          return `${
            data.labels[tooltipItem.index]
          } : ${this.formatNumPipe.transform(
            currentValue,
            '$',
            []
          )} - ${percentage.toFixed(2)} %`;
        }
      }
    };
    this.showAdSizePieChart = false;
    this.dataFetchServ.getUPRData(adSizePieChartReq).subscribe(data => {
      const chartData = data['data'];
      this.noDataAdSizePieChart = false;
      if (!chartData.length) {
        this.noDataAdSizePieChart = true;
        return;
      }
      const sourceArr = Array.from(
        new Set(chartData.map(s => s['creative_size']))
      );
      const colors = this.libServ.dynamicColors(sourceArr.length);
      this.adSizePieChartJson['chartData']['labels'] = sourceArr;
      this.adSizePieChartJson['chartData']['datasets'][0]['data'] = Array.from(
        new Set(chartData.map(s => s['dp_revenue']))
      );
      this.adSizePieChartJson['chartData']['datasets'][0][
        'backgroundColor'
      ] = colors;
      this.showAdSizePieChart = true;
    });
  }

  loadDeviceTypePieChart() {
    const deviceTypePieChartReq = {
      dimensions: ['device_category'],
      metrics: ['dp_revenue'],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['device_category'],
      orderBy: [{ key: 'dp_revenue', opcode: 'desc' }],
      limit: '5',
      offset: '0'
    };
    deviceTypePieChartReq['orderBy'] = [
      { key: 'dp_revenue', opcode: 'desc' }
    ];

    this.deviceTypePieChartJson = {
      chartTypes: [{ key: 'pie', label: 'Pie Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Revenue Distribution Across Device Types'
        },
        legend: {
          display: false
        },
        elements: {
          arc: {
            borderWidth: 0
          }
        },
        plugins: {
          datalabels: {
            formatter: (value, ctx) => {
              let sum = 0;
              const dataArr = ctx.chart.data.datasets[0].data;
              dataArr.map(data => {
                sum += data;
              });
              const percentage = (value * 100) / sum;
              // if (percentage > 10) {
              //   return `${this.formatNumPipe.transform(
              //     value,
              //     '$',
              //     []
              //   )} - ${percentage.toFixed(2)} %`;
              // } else {
              //   return '';
              // }
              if (percentage > 10) {
                return `${percentage.toFixed(2)} %`;
              } else {
                return '';
              }
            },
            color: 'black'
          }
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };
    this.deviceTypePieChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          let sum = 0;
          const dataArr = data.datasets[0].data;
          dataArr.map(data => {
            sum += data;
          });
          const percentage = (currentValue * 100) / sum;
          return `${
            data.labels[tooltipItem.index]
          } : ${this.formatNumPipe.transform(
            currentValue,
            '$',
            []
          )} - ${percentage.toFixed(2)} %`;
        }
      }
    };
    this.showDeviceTypePieChart = false;
    this.dataFetchServ.getUPRData(deviceTypePieChartReq).subscribe(data => {
      const chartData = data['data'];
      this.noDataDeviceTypePieChart = false;
      if (!chartData.length) {
        this.noDataDeviceTypePieChart = true;
        return;
      }
      const sourceArr = Array.from(
        new Set(chartData.map(s => s['device_category']))
      );
      const colors = this.libServ.dynamicColors(sourceArr.length);
      this.deviceTypePieChartJson['chartData']['labels'] = sourceArr;
      this.deviceTypePieChartJson['chartData']['datasets'][0][
        'data'
      ] = Array.from(new Set(chartData.map(s => s['dp_revenue'])));
      this.deviceTypePieChartJson['chartData']['datasets'][0][
        'backgroundColor'
      ] = colors;
      this.showDeviceTypePieChart = true;
    });
  }

  loadDeviceTypeLineChart() {
    const deviceTypeLineChartReq = {
      dimensions: ['time_key'],
      metrics: ['dp_revenue', 'dp_impressions'],
      derived_metrics: ['rev_cpm'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      // orderBy: [],
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };
    const colors = this.libServ.dynamicColors(2);
    this.deviceTypeLineChartJson = {
      chartTypes: [{ key: 'line', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'eCPM',
            type: 'line',
            yAxisID: 'y-axis-0',

            borderColor: colors[0],
            fill: false,
            backgroundColor: colors[0],
            data: []
          },
          {
            label: 'Revenue',
            type: 'line',
            yAxisID: 'y-axis-1',

            borderColor: colors[1],
            fill: false,
            backgroundColor: colors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Overall Revenue and eCPM Trend'
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: 'eCPM ($)'
              },
              position: 'right',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', []);
                }
              }
              // scaleFontColor: "rgba(151,137,200,0.8)"
            },
            {
              id: 'y-axis-1',
              scaleLabel: {
                display: true,
                labelString: 'Revenue ($)'
              },
              position: 'left',
              name: '2',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', [0]);
                }
              }
              // scaleFontColor: "rgba(151,187,205,0.8)"
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
              } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    this.showDeviceTypeLineChart = false;
    this.dataFetchServ.getUPRData(deviceTypeLineChartReq).subscribe(data => {
      const chartData = data['data'];

      this.noDataDeviceTypeLineChart = false;
      if (!chartData.length) {
        this.noDataDeviceTypeLineChart = true;
        return;
      }
      const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));

      this.deviceTypeLineChartJson['chartData']['labels'] = datesArr.map(d =>
        moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
      );

      const revArr = [];
      const ecpmArr = [];
      datesArr.forEach(time_key => {
        chartData.forEach(r => {
          if (r['time_key'] === time_key) {
            revArr.push(r['dp_revenue']);
            ecpmArr.push(r['rev_cpm']);
          }
        });
      });
      this.deviceTypeLineChartJson['chartData']['datasets'][0][
        'data'
      ] = ecpmArr;
      this.deviceTypeLineChartJson['chartData']['datasets'][1]['data'] = revArr;
      this.showDeviceTypeLineChart = true;
    });
  }

  loadUnfilledLineChart() {
    const unfilledLineChartReq = {
      dimensions: ['time_key'],
      metrics: ['unfilled_impressions'],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      // orderBy: [],
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };
    const colors = this.libServ.dynamicColors(2);
    this.unfilledLineChartJson = {
      chartTypes: [{ key: 'line', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'Impression',
            type: 'line',
            yAxisID: 'y-axis-0',

            borderColor: colors[1],
            fill: false,
            backgroundColor: colors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Unfilled Impressions'
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: 'Impressions'
              },
              position: 'left',
              name: '2',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, 'number', [0]);
                }
              }
              // scaleFontColor: "rgba(151,187,205,0.8)"
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
              } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    this.showUnfilledLineChart = false;
    this.dataFetchServ.getunfilledData(unfilledLineChartReq).subscribe(data => {
      const chartData = data['data'];
      this.noDataUnfilledLineChart = false;
      if (!chartData.length) {
        this.noDataUnfilledLineChart = true;
        return;
      }
      const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));

      this.unfilledLineChartJson['chartData']['labels'] = datesArr.map(d =>
        moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
      );

      const revArr = [];
      const ecpmArr = [];
      datesArr.forEach(time_key => {
        chartData.forEach(r => {
          if (r['time_key'] === time_key) {
            ecpmArr.push(r['unfilled_impressions']);
          }
        });
      });
      this.unfilledLineChartJson['chartData']['datasets'][0]['data'] = ecpmArr;
      this.showUnfilledLineChart = true;
    });
  }

  loadUnfilledLostRevenueLineChart() {
    const lostrevenueLineChartReq = {
      dimensions: ['time_key'],
      metrics: ['dp_revenue', 'dp_impressions'],
      derived_metrics: ['rev_cpm'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      // orderBy: [],
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };

    const unfilledChartReq = {
      dimensions: ['time_key'],
      metrics: ['unfilled_impressions'],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      // orderBy: [],
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };
    const colors = this.libServ.dynamicColors(2);
    this.unfilledLostRevenueLineChartJson = {
      chartTypes: [{ key: 'line', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'Unfilled Impressions',
            type: 'line',
            yAxisID: 'y-axis-0',

            borderColor: colors[0],
            fill: false,
            backgroundColor: colors[0],
            data: []
          },
          {
            label: 'Lost Revenue',
            type: 'line',
            yAxisID: 'y-axis-1',

            borderColor: colors[1],
            fill: false,
            backgroundColor: colors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Overall Lost Revenue and Unfilled Impressions Trend'
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: 'Unfilled Impressions'
              },
              position: 'right',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, 'number', [0]);
                }
              }
              // scaleFontColor: "rgba(151,137,200,0.8)"
            },
            {
              id: 'y-axis-1',
              scaleLabel: {
                display: true,
                labelString: 'Lost Revenue ($)'
              },
              position: 'left',
              name: '2',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', [0]);
                }
              }
              // scaleFontColor: "rgba(151,187,205,0.8)"
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              if (tooltipItem.datasetIndex !== 0) {
                return `${
                  data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
              }
              return `${
                data.datasets[tooltipItem.datasetIndex].label
              } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    this.showUnfilledLostRevenueLineChart = false;
    this.dataFetchServ.getUPRData(lostrevenueLineChartReq).subscribe(data => {
      const gross_ecpm = data['data'];

      this.noDataUnfilledLostRevenueLineChart = false;

      this.dataFetchServ.getunfilledData(unfilledChartReq).subscribe(data => {
        const chartData = data['data'];

        if (!gross_ecpm.length || !chartData.length) {
          this.noDataUnfilledLostRevenueLineChart = true;
          return;
        }

        const ecpmArr = [];
        const arr = [];
        const processedArray = [];
        gross_ecpm.forEach((item1, index1) => {
          chartData.forEach((item, index) => {
            if (item1.time_key === item.time_key) {
              const lr = (item1.rev_cpm * item.unfilled_impressions) / 1000;

              processedArray.push(lr);
              ecpmArr.push(item.unfilled_impressions);
              // moment(item.time_key, 'YYYYMMDD').format('YYYY-MM-DD')
              arr.push(item.time_key);
            }
          });
        });
        const datesArr = Array.from(new Set(arr.map(r => r['time_key'])));

        this.unfilledLostRevenueLineChartJson['chartData'][
          'labels'
        ] = arr.map(d => moment(d, 'YYYYMMDD').format('MM-DD-YYYY'));
        this.unfilledLostRevenueLineChartJson['chartData']['datasets'][0][
          'data'
        ] = ecpmArr;
        this.unfilledLostRevenueLineChartJson['chartData']['datasets'][1][
          'data'
        ] = processedArray;
        this.showUnfilledLostRevenueLineChart = true;
      });
    });
  }

  onFiltersApplied(filterData: object) {
    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];

    this.loadMainLineChart();

    this.loadMainPieChart();

    this.loadAdSizeLineChart();

    this.loadAdSizePieChart();

    this.loadDeviceTypeLineChart();

    this.loadDeviceTypePieChart();

    this.loadUnfilledLineChart();

    this.loadUnfilledLostRevenueLineChart();
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['name']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    } else if (type === 'ppt') {
      const chartElements = document.getElementsByTagName('ym-charts');
      const slides = [];
      [].forEach.call(chartElements, (el, index) => {
        const c = el.getElementsByTagName('canvas')[0];

        slides.push({
          title: `${this.appConfig['name']}`,
          base64data: this.libServ.canvasToImage('white', c),
          width: 8,
          height: 3
        });

        if (index === chartElements.length - 1) {
          const pptConfig = {
            filename: 'Charts',
            logo: {
              visible: true,
              path: '/assets/images/ym.png',
              width: 0.53125,
              height: 0.458333
            },
            slides: slides
          };
          this.pptExport.exportPPT(pptConfig);
        }
      });
    }
    return false;
  }

  sendEmail() {
    const imageConfig = {
      imgName: `${this.appConfig['name']}.png`,
      type: 'image/png'
    };

    this.htmltoimage
      .canvasImage(document.getElementById('capture'), imageConfig)
      .then(canvas => {
        this.dialogService.open(SendEmailComponent, {
          header: 'Email Screenshot',
          contentStyle: {
            'max-height': '80vh',
            width: '30vw',
            overflow: 'auto'
          },
          data: canvas.toDataURL('image/png')
        });
      });
  }

  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }
}
