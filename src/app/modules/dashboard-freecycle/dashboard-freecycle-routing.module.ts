import { Routes } from '@angular/router';
import { DashboardFreeCycleComponent } from './dashboard-freecycle.component';

export const DashboardAppRoutesAppRoutes: Routes = [
  {
    path: '',
    component: DashboardFreeCycleComponent
  }
];
