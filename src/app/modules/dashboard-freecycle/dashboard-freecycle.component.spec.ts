import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardFreeCycleComponent } from './dashboard-freecycle.component';

describe('DashboardAppComponent', () => {
  let component: DashboardFreeCycleComponent;
  let fixture: ComponentFixture<DashboardFreeCycleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DashboardFreeCycleComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardFreeCycleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
