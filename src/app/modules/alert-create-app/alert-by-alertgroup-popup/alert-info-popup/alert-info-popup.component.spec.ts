import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertInfoPopupComponent } from './alert-info-popup.component';

describe('AlertInfoPopupComponent', () => {
  let component: AlertInfoPopupComponent;
  let fixture: ComponentFixture<AlertInfoPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertInfoPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertInfoPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
