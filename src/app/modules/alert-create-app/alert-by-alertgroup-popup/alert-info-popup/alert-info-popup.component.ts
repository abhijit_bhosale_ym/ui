import { Component, OnInit } from '@angular/core';
import { FetchApiDataService } from '../../fetch-api-data.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { Subscription } from 'rxjs';
import * as moment from 'moment';
import { TreeNode } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';
import { DynamicDialogConfig,DynamicDialogRef,DialogService } from 'primeng/dynamicdialog';

@Component({
  selector: 'ym-alert-info-popup',
  templateUrl: './alert-info-popup.component.html',
  styleUrls: ['./alert-info-popup.component.scss']
})
export class AlertInfoPopupComponent implements OnInit {

  alertData: any;
  alertDetails: object;
  actionDetails: object;
  schedulerAShowInfo = false;
  schedulerStartDate: any;
  schedulerEndDate: any;
  schedulerAStartDate: any;
  schedulerAEndDate: any;

  channelSize = 0;
  ruleData: any;
  isStageCsv = true;
  dateRange: any;

  schedulerRShowInfo = true;
  dimensionsnamelist: any[];
  matricsnamelist: any[];
  conditionslist: any[];
  matricData = [];

  DimensionsList = [];
  MetricList = [];
  DerivedMetricsList = [];
  DimensionsConditionList: any;
  MetricConditionList: any;
  SortingColumnsList = [];


  constructor(
    private dataFetchServ: FetchApiDataService,
    private toastService: ToastService,
    private config: DynamicDialogConfig,
    private dialogService: DialogService,
    private ref: DynamicDialogRef

  ) {
    this.alertData = this.config.data;
  }

  ngOnInit() {
    this.loadAlertDetails();
  }

  bindruledata(event_id, stage_name) {
    return new Promise((resolve, reject) => {
      if (stage_name == "Table") {
        this.isStageCsv = false;
        const req1 = {
          instance_type: "stageConditionMapping",
          opcode: "get",
          stage_name: stage_name,
        };
        let req2 = {
          instance_type: "event",
          opcode: "getDimMetric",
          event_id: event_id
        };

        this.dataFetchServ.getAlertTableData(req2).subscribe(success => {
          this.dimensionsnamelist = [];
          let dimlist = [];
          success['opcode_data']['dimensions'].forEach(element => {
            dimlist.push({ key: element.name, label: element.alies, value: element.alies })
          });

          this.matricsnamelist = [];
          let metriclist = [];
          success['opcode_data']['metrics'].forEach((element, index) => {
            metriclist.push({ key: element.name, label: element.alies, value: element.alies })
            this.matricData.push({ id: index, text: element.alies });
          });
          this.dimensionsnamelist = dimlist;
          this.matricsnamelist = metriclist;
          this.dataFetchServ.getAlertTableData(req1).subscribe(success1 => {
            this.conditionslist = [];
            success1['opcode_data'].forEach(element => {
              this.conditionslist.push({ key: element.conditional_symbol, label: element.conditional_symbol, value: element.conditional_symbol })
            });
          });
          resolve();
        });

      } else {
        this.isStageCsv = true;
        reject();
      }
    })

  }

  loadAlertDetails() {
    const req1 = {
      instance_type: "event",
      opcode: "get",
      event_id: this.alertData['data']['rule_event_id'],
      user_id: this.alertData['user_id'],
      client_code: "FR"
    };

    const req2 = {
      instance_type: "rule",
      opcode: "get",
      event_id: this.alertData['data']['rule_event_id'],
      rule_id: this.alertData['data']['rule_id']
    }

    this.loadActionDetails();

    this.dataFetchServ.getAlertTableData(req1).subscribe(data => {
      this.alertDetails = data['opcode_data'];
      console.log("alet details",this.alertDetails);
      
      this.dataFetchServ.getAlertTableData(req2).subscribe(data => {
        this.ruleData = data['opcode_data'];

        if (this.ruleData['scheduler']['granularityType'] == "" && this.ruleData['status'] == 'Inactive') {
          this.schedulerRShowInfo = true;
        } else {
          this.schedulerRShowInfo = false;
          let datetimeStr = this.ruleData['scheduler']['startDate'] + " " + this.ruleData['scheduler']['granularityTime']; //suppose this is UK time
          let upDate = new Date(new Date().toLocaleString("en-US", { timeZone: "America/New_York" })); //London current time
          let currentDate = new Date(); //My current time
          let timeDifference = this.diff_minutes(currentDate, upDate);
          let date = new Date(datetimeStr);
          date.setMinutes(date.getMinutes() + timeDifference);
          this.schedulerStartDate = moment(date).format('MM/DD/YYYY hh:mm A Z');
          this.schedulerEndDate = moment(this.ruleData['scheduler']['endDate']).format('MM/DD/YYYY');
        }

        if (this.alertDetails['stage_name'] == 'Table') {
          this.isStageCsv = false;
          if (this.ruleData['data_selection']['selection_type'] == "custom range") {
            this.dateRange = moment(this.ruleData['data_selection']['start_date']).format('MM/DD/YYYY') + " To " + moment(this.ruleData['data_selection']['end_date']).format('MM/DD/YYYY');
          } else {
            this.dateRange = (this.ruleData['data_selection']['selection_type']).toLowerCase()
              .split(' ')
              .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
              .join(' ');
          }
          let sortcolData = [];

          this.bindruledata(this.alertData['data']['rule_event_id'], this.alertDetails['stage_name']).then(res => {
            console.log("res===", res);
            this.ruleData['column_mappings']['dimensions'].forEach((element) => {
              this.DimensionsList.push(this.dimensionsnamelist.find(o => o.key == element).label);
              sortcolData.push({ key: element, label: this.dimensionsnamelist.find(o => o.key == element).label });
            });
            this.ruleData['column_mappings']['metrics'].forEach((element) => {
              this.MetricList.push(this.matricsnamelist.find(o => o.key == element).label);
              sortcolData.push({ key: element, label: this.matricsnamelist.find(o => o.key == element).label });
            });

            this.ruleData['column_mappings']['derive_metrics'].forEach((element, i) => {
              let formulaString = "";
              sortcolData.push({ key: element.name, label: element.name });
              formulaString += element.name + ":-(";
              element.formula.split(" ").forEach((el, id) => {
                let el1 = el.toLowerCase().replace(/sum\((.*?)\)/g, '$1');
                if (this.matricsnamelist.find(o => o.key == el1)) {
                  formulaString += "sum(" + this.matricsnamelist.find(o => o.key == el1).label + ") ";

                } else if (el1 != "") {
                  formulaString += el1 + " ";
                }
              });
              formulaString += " )";
              this.DerivedMetricsList.push(formulaString);
            });

            this.DimensionsConditionList = "";
            this.ruleData['condition_mappings']['dimensions'].forEach((element, i) => {
              this.DimensionsConditionList += element.operator + " " + this.dimensionsnamelist.find(o => o.key == element.name).label + " " + element.condition + " ' " + element.values.join(', ') + " ' ";
            });

            this.MetricConditionList = "";
            this.ruleData['condition_mappings']['metrics'].forEach((element, i) => {

              if (element.type == 'Derived') {
                this.MetricConditionList += element.operator + " " + element.name + ":-( ";
                let formulaString = "";
                element.formula.split(" ").forEach((el, id) => {
                  let el1 = el.toLowerCase().replace(/sum\((.*?)\)/g, '$1');
                  if (this.matricsnamelist.some(o => o.key == el1)) {
                    formulaString += "sum(" + this.matricsnamelist.find(o => o.key == el1).label + ") ";

                  } else if (el1 != "") {
                    formulaString += el1 + " ";
                  }
                });
                this.MetricConditionList += formulaString + ") " + this.conditionslist.find(o => o.key == element.condition).label;
              } else {
                this.MetricConditionList += element.operator + " " + this.matricsnamelist.find(o => o.key == element.name).label + " " + this.conditionslist.find(o => o.key == element.condition).label;
              }

              if (element.condition == 'BETWEEN') {
                this.MetricConditionList += " " + element.values.split(",")[0] + " AND " + element.values.split(",")[1] + " ";
              } else {
                this.MetricConditionList += " " + element.values + " ";
              }

            });
            this.ruleData['condition_mappings']['sorting_by_columns'].forEach(el => {
              if (sortcolData.some(x => x.key == el)) {
                this.SortingColumnsList.push(sortcolData.find(x => x.key == el).label);
              }
            })


          })
        }
      })

    })
  }

  loadActionDetails() {
    const tableReq = {
      instance_type: "action",
      opcode: "get",
      event_id: this.alertData['data']['rule_event_id'],
      action_id: this.alertData['action_id'],
    };
    this.dataFetchServ.getAlertTableData(tableReq).subscribe(data => {
      this.actionDetails = data['opcode_data'];
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      } else {
        this.channelSize = this.actionDetails['channel'].length;
        if (this.actionDetails['scheduler']['granularityType'] == "" && this.actionDetails['status'] == 'Inactive') {
          this.schedulerAShowInfo = true;
        } else {
          this.schedulerAShowInfo = false;
          let datetimeStr = this.actionDetails['scheduler']['startDate'] + " " + this.actionDetails['scheduler']['granularityTime']; //suppose this is UK time
          let upDate = new Date(new Date().toLocaleString("en-US", { timeZone: "America/New_York" })); //London current time
          let currentDate = new Date(); //My current time
          let timeDifference = this.diff_minutes(currentDate, upDate);

          var date = new Date(datetimeStr);
          date.setMinutes(date.getMinutes() + timeDifference);
          this.schedulerAStartDate = moment(date).format('MM/DD/YYYY hh:mm A Z');
          this.schedulerAEndDate = moment(this.actionDetails['scheduler']['endDate']).format('MM/DD/YYYY');
        }

      }
    });
  }

  diff_minutes(dt2, dt1) {
    var diff = (dt2.getTime() - dt1.getTime()) / 1000;
    diff /= 60;
    return Math.abs(Math.round(diff));
  }

  CloseDialog() {
    this.ref.close();
  }
}
