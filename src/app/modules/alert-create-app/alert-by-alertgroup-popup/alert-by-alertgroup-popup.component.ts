import { Component, OnInit, OnDestroy } from '@angular/core';
import { TreeNode } from 'primeng/api';
import { MessageService, ConfirmationService } from 'primeng/api';
import { DynamicDialogConfig,DynamicDialogRef,DialogService } from 'primeng/dynamicdialog';
import * as moment from 'moment';
import { FetchApiDataService } from '../fetch-api-data.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { Subscription } from 'rxjs';
import { EditAlertComponent } from '../edit-alert/edit-alert.component'
import { AlertInfoPopupComponent } from './alert-info-popup/alert-info-popup.component'

import {
  Router,
  ActivatedRoute,
  NavigationExtras,
  NavigationEnd,
  RouterEvent,
} from '@angular/router';
@Component({
  selector: 'ym-alert-by-alertgroup-popup',
  templateUrl: './alert-by-alertgroup-popup.component.html',
  styleUrls: ['./alert-by-alertgroup-popup.component.scss']
})
export class AlertByAlertGroupPopupComponent implements OnInit {
  private appConfigObs: Subscription;
  audit_id: String;
  isStageCsv = false;
  dimensionsnamelist = [];
  matricsnamelist = [];
  conditionslist = [];
  DimensionsList = [];
  MetricList = [];
  DerivedMetricsList = [];
  SortingColumnsList = [];
  table_Data = [];
  

  ruleDetails = {};
  dateRange;
  DimensionsConditionList;
  MetricConditionList;
  eventData : any;
  aggTableJson: Object;
  globalSearchValue = "";
  colSearch : any = {};
  noTableData = false;
  aggTableData: TreeNode[];
  displayAggTable = true;
  tableDimColDef: any[];
  user_id : any;

  constructor(
    private dialogService: DialogService,
    private dataFetchServ: FetchApiDataService,
    private toastService: ToastService,
    private config: DynamicDialogConfig,
    private router: Router,
    private currentActivatedRoute: ActivatedRoute,
    public ref: DynamicDialogRef,
    private confirmationService: ConfirmationService
  ) {
    this.eventData = this.config.data['data'];
    this.user_id = this.config.data['user_id'];
  }

  ngOnInit() {

    this.tableDimColDef = [
      {
        field: 'rule_name',
        displayName: 'Alert Group Name',
        format: '',
        width: '200',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
      },

      {
        field: 'rule_status',
        displayName: 'Status',
        width: '40',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'run',
        displayName: 'Run',
        format: '',
        width: '30',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'edit',
        displayName: 'Edit',
        format: '',
        width: '30',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'scheduler_frequency',
        displayName: 'Scheduler frequency',
        format: '',
        width: '50',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },

      },
      {
        field: 'scheduler_status',
        displayName: 'Scheduler Status',
        width: '60',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'delete',
        displayName: 'Delete',
        width: '40',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: false,
          resizable: true,
          movable: true
        },
      },

      {
        field: 'info',
        displayName: 'Info',
        width: '30',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: false,
          resizable: true,
          movable: true
        },
      },

      {
        field: 'created_at',
        displayName: 'Created At',
        width: '100',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'updated_at',
        displayName: 'Updated At',
        width: '100',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
    ];
    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,
      footerColumns: this.tableDimColDef,
      columns: this.tableDimColDef,
      selectedColumns: this.tableDimColDef,
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: true
    };

    this.loadTable()
  }

  loadTable(){
    this.aggTableJson['loading'] = true;
    
    
    
    const tableReq = {
      // instance_type: "audit",
      // opcode: "getByUser",
      // user_id: this.appConfig['user']['id'].toString(),
      // start_date: this.start_Date,
      // end_date: this.end_Date,
      // severity: this.severity,
      // client_code: "FR",
      
        instance_type: "rule",
        opcode:"getAll",
        event_id:this.eventData['event_id'],
        column_search: this.colSearch,
        global_search: {
        keys : ['rule_name', 'rule_status', 
                'rule_id','created_at', 'updated_at','severity'],
        value : [this.globalSearchValue]    
              }
    };

    this.dataFetchServ.getAlertTableData(tableReq).subscribe(data => { 
      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      } else {
        this.noTableData = false;
      }

      this.table_Data = data['opcode_data'];
      const arr = [];
      this.table_Data.forEach((row: object) => {
        let obj = {};
        obj = {
          data: row
        };
        arr.push(obj);
      });

      this.reloadAggTable();
                  
      this.aggTableData = <TreeNode[]>arr;
      this.aggTableJson['totalRecords'] = data['opcode_data'].length;
      this.aggTableJson['loading'] = false;
    });
  }

  editAlert(ruleDetails){
    const ref = this.dialogService.open(EditAlertComponent, {
      header: 'Alert Group : ' + ruleDetails['rule_name'],
      contentStyle: { 'max-height': '80vh', width: '80vw', overflow: 'auto' },
      data: {data : ruleDetails,isEdit : true},
    });
    ref.onClose.subscribe((data1: string) => { });

  } 

  alertInfo(rowData) {
    const data = {
      data: rowData,
      action_id: this.eventData['action_id'],
      user_id: this.user_id
    };

    console.log("rowData",rowData);
    
    const ref = this.dialogService.open(AlertInfoPopupComponent, {
      header: 'Alert : ' + rowData['rule_name'],
      contentStyle: { 'max-height': '90vh', width: '60vw', overflow: 'auto' },
      data: data,
    });
    // ref.onClose.subscribe((data1: string) => { 

    // });
    //ref.onDestroy.subscribe(null)
    ref.onClose.subscribe((data1: string) => { });

  }
  addNewAlert(){
    const ref = this.dialogService.open(EditAlertComponent, {
      header: this.eventData['event_name'],
      contentStyle: { 'max-height': '80vh', width: '80vw', overflow: 'auto' },
      data: {data : this.eventData,isEdit : false}
    });
    ref.onClose.subscribe((data1: string) => { });
  }

  deleteAlert(rowData) {
    this.confirmationService.confirm({
      message: 'Do you want to delete this alert ?',
      header: 'Delete Alert',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        const req = {
          "instance_type": "rule",
          "opcode": "delete",
          "event_id": rowData['rule_event_id'],
          "rule_id": rowData['rule_id']

        }
        this.dataFetchServ
          .deleteAlert(req)
          .subscribe(data => {
            if (data['status'] === 0) {
              this.toastService.displayToast({
                severity: 'error',
                summary: 'Server Error',
                detail: 'Please refresh the page'
              });
              console.log(data['status_msg']);
              return;
            } else {
              this.toastService.displayToast({
                severity: 'success',
                summary: 'Delete Alert',
                detail: 'Alert deleted successfully!'
              });
              this.loadTable();
            }
          });
      },
      reject: () => {
      }
    })
  }

  runAlert(rowData) {
    console.log("row data in run",rowData);
    
    if (this.eventData['event_status'] !== 'Inactive') {
      if (rowData['rule_status'] !== 'Inactive') {
        this.confirmationService.confirm({
          message: 'Do you want to run this alert ?',
          header: 'Run Alert',
          icon: 'pi pi-exclamation-triangle',
          accept: () => {
            const req = {
              "event_id": rowData['rule_event_id'],
              "rule_id": rowData['rule_id']
            }
            this.dataFetchServ
              .runAlert(req)
              .subscribe(data => {
                if (data['status'] === 0) {
                  this.toastService.displayToast({
                    severity: 'error',
                    summary: 'Server Error',
                    detail: 'Please refresh the page'
                  });
                  console.log(data['status_msg']);
                  return;
                } else {
                  this.toastService.displayToast({
                    severity: 'success',
                    summary: 'Alert Run',
                    detail: 'Alert is run successfully!'
                  });
                  this.loadTable();
                }
              });
          },
          reject: () => {
          }
        })
      }
    }
  }

  scheduleStatus(rowData) {
    console.log("row data schedule data", rowData);
    if (rowData['rule_status'] !== 'Inactive') {
      if (rowData['scheduler_status'] !== 'Inactive') {
        let msg = "";
        if (rowData['scheduler_status'] == 'Pause') {
          msg = "Do you want to resume this scheduled alert?";
        } else if (rowData['scheduler_status'] == 'Active') {
          msg = "Do you want to pause this scheduled alert ?";
        }
        this.confirmationService.confirm({
          message: msg,
          header: 'Run Alert',
          icon: 'pi pi-exclamation-triangle',
          accept: () => {
            if (rowData['scheduler_status'] == 'Pause') {
              rowData['scheduler_status'] = "Active";
            } else if (rowData['scheduler_status'] == 'Active') {
              rowData['scheduler_status'] = "Pause";
            }
            const req = {
              instance_type: "rule",
              opcode: "updateSchedulerStatus",
              event_id: rowData['rule_event_id'],
              rule_id: rowData['rule_id'],
              scheduler_status: rowData['scheduler_status']
            }
            this.dataFetchServ
              .getAlertTableData(req)
              .subscribe(data => {
                if (data['status'] === 0) {
                  this.toastService.displayToast({
                    severity: 'error',
                    summary: 'Server Error',
                    detail: 'Please refresh the page'
                  });
                  console.log(data['status_msg']);
                  return;
                } else {
                  this.toastService.displayToast({
                    severity: 'success',
                    summary: 'Alert Run',
                    detail: 'Alert is run successfully!'
                  });
                  this.loadTable();
                }
              });
          },
          reject: () => {
          }
        })
      }
    }

  }
  reloadAggTable() {
    this.displayAggTable = false;
    setTimeout(() => {
      this.displayAggTable = true;
    }, 0);
  }

}
