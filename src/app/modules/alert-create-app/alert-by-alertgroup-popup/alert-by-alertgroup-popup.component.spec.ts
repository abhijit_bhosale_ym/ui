import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertByAlertGroupPopupComponent } from './alert-by-alertgroup-popup.component';

describe('AlertByAlertGroupPopupComponent', () => {
  let component: AlertByAlertGroupPopupComponent;
  let fixture: ComponentFixture<AlertByAlertGroupPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertByAlertGroupPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertByAlertGroupPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
