import { Routes } from '@angular/router';
import { AlertCreateAppComponent } from './alert-create-app.component';
import { AddAlertComponent  } from './add-alert/add-alert.component'

export const AlertCreateAppRoutesAppRoutes: Routes = [
  {
    path: '',
    component: AlertCreateAppComponent
  },
  {
    path: 'add-alert',
    component: AddAlertComponent
  },
];
