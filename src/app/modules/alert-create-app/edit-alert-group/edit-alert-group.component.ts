import { Component, OnInit } from '@angular/core';
import { FetchApiDataService } from '../fetch-api-data.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { Subscription } from 'rxjs';
import * as moment from 'moment';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms'; 4
import { SelectItem } from 'primeng/api';
import { ConfirmationService,SelectItemGroup, MenuItem } from 'primeng/api';
import { DynamicDialogConfig,DynamicDialogRef,DialogService } from 'primeng/dynamicdialog';

@Component({
  selector: 'ym-edit-alert-group',
  templateUrl: './edit-alert-group.component.html',
  styleUrls: ['./edit-alert-group.component.scss']
})
export class EditAlertGroupComponent implements OnInit {
  alertData: any;
  alertDetails: object;
  actionDetails: object;
  schedulerAShowInfo = false;
  schedulerStartDate: any;
  schedulerEndDate: any;
  channelSize = 0;
  alertName = "";
  alertGroupForm: FormGroup;
  alertdescription = "";
  availableAppName: any = [];
  selectedAppName: { app_name: '', display_app_name: '' }
  editFlag: any;
  actionForm: FormGroup;

  selectedSchecularStatus: any;
  schedulerFrequency = [{ alies: 'Once', name: 'Once' },
  { alies: 'Hourly', name: 'Hourly' },
  { alies: 'Daily', name: 'Daily' },
  { alies: 'Weekly', name: 'Weekly' },
  { alies: 'Monthly', name: 'Monthly' },
  { alies: 'Yearly', name: 'Yearly' }
  ]

  schedularStatus: any = [{ 'label': 'Active', 'value': 'Active' }, { 'label': 'Pause', 'value': 'Pause' }, { 'label': 'Inactive', 'value': 'Inactive' }]
  channelList: any = [{ 'alies': 'Email', 'name': 'Email' }];
  actionData: object;
  actionName: any;
  actionDesc: any;
  schedulerADetailsFlag = 'Off';
  selectedActionFrequency: { alies: '', name: '' };
  actionSelectedChannel: any[] = [];
  isEmailAction: boolean = false;
  isSlackAction: boolean = false;
  actionEmailChannel: any[];
  slackChannel: any[];
  isEmail = true;
  isSlackEmail = true;

  constructor(
    private dataFetchServ: FetchApiDataService,
    private toastService: ToastService,
    private config: DynamicDialogConfig,
    private formBuilder: FormBuilder,
    public ref: DynamicDialogRef
  ) {
    this.alertData = this.config.data;
  }

  ngOnInit() {
    this.alertGroupForm = this.formBuilder.group({
      alert_group_name: ['', Validators.required],
      app_name: ['', Validators.required],
      discription: ['']
    });


    this.schedulerStartDate = new Date();
    this.schedulerEndDate = new Date()
    this.schedulerEndDate.setDate(this.schedulerStartDate.getDate() + 1);

    this.editFlag = this.alertData['editFlag'];
    this.alertName = this.alertData['alertData']['event_name'];
    this.alertdescription = this.alertData['alertData']['event_description']
    this.selectedAppName = { app_name: this.alertData['alertData']['app_name'], display_app_name: this.alertData['alertData']['display_app_name'] }

  
    this.actionForm = this.formBuilder.group({
      action_name: ['', Validators.required],
      action_description: [''],
      action_frequency: [''],
      action_start_date: [''],
      action_end_date: [''],
      schedular_Status: [''],
      slackChannel: [''],
      actionEmailChannel: [''],
      channel: [''],
    });
    if (this.editFlag === "alertGroup") {
      this.loadAppNames();
    }
    else if (this.editFlag === "actionDetails") {
      this.loadActionDetails();
    }
  }
  get a() {
    return this.actionForm.controls;
  }
  get u() {
    return this.alertGroupForm.controls;
  }

  loadAppNames() {
    const req = {
      instance_type: "appTableMapping",
      opcode: "get",
      client_code: "FR"
    }
    this.dataFetchServ
      .getAlertTableData(req)
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        this.availableAppName = data['opcode_data']
      });
  }

  editAlertGroup() {
    const req = {
      instance_type: "event",
      opcode: "update",
      event_id: this.alertData['alertData']['event_id'],
      app_name: this.selectedAppName['app_name'],
      data_processing_stage: "Table",
      event_name: this.alertName,
      description: this.alertdescription,
      status: this.alertData['alertData']['action_status'],
      client_code: "FR",
      user_id: this.alertData['user_id']
    }
    this.dataFetchServ
      .updateAlertGroup(req)
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        } else {
          this.toastService.displayToast({
            severity: 'success',
            summary: 'Update Alert Group',
            detail: 'Alert Group updated successfully'
          });
        }
        this.ref.close(true);
      });
  }

  CloseDialog() {
    this.ref.close(false);
  }

  loadActionDetails() {
    const req = {
      instance_type: "action",
      opcode: "get",
      action_id: this.alertData['alertData']['action_id'],
      event_id: this.alertData['alertData']['event_id']
    }
    this.dataFetchServ
      .getAlertTableData(req)
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        } else {
          this.actionData = data['opcode_data'];
          this.actionName = this.actionData['action_name'];
          this.actionDesc = this.actionData['description'];

          console.log("actionData",this.actionData);
          

          if (this.actionData['scheduler']['granularityType'] == '' && this.actionData['status'] == 'Inactive') {
            this.schedulerADetailsFlag = 'Off';
          } else {
            this.schedulerADetailsFlag = 'On';
          }

          if (this.schedulerADetailsFlag == 'On') {

            if(this.actionData['scheduler']['startDate'] && this.actionData['scheduler']['endDate']){
            let datetimeStr = this.actionData['scheduler']['startDate'] + " " + this.actionData['scheduler']['granularityTime']; //suppose this is UK time
            // let upDate = new Date(new Date().toLocaleString("en-US", { timeZone: "America/New_York" })); //London current time
            // let currentDate = new Date(); //My current time
            // let timeDifference = this.diff_minutes(currentDate, upDate);
            // let date = new Date(datetimeStr);
            console.log("str",datetimeStr);
            //console.log("date",date);

            let date = moment(datetimeStr,'YYYY-MM-DD').format('MM/DD/YYYY hh:mm')
            console.log("date",date);
            
            //console.log("/////",);
            console.log("..",this.actionData['scheduler']['startDate']);
            
             console.log("...",new Date(datetimeStr));
              
            //date.setMinutes(date.getMinutes() + timeDifference);
           // this.schedulerStartDate = moment(date). =format('MM/DD/YYYY hh:mm');
           this.schedulerStartDate = date
           //this.schedulerStartDate = datetimeStr;
            this.schedulerEndDate = moment(this.actionData['scheduler']['endDate']).format('MM/DD/YYYY');
            }
            else{
              console.log("in else....");
              
              const today = new Date();
              let firstDate = new Date()
              let lastDate = new Date();
          
              firstDate.setDate(today.getDate() - 7);
              lastDate.setDate(today.getDate() - 1);
              
              this.schedulerStartDate = new Date();

              console.log("schedulerStartDate",this.schedulerStartDate);
              

              this.schedulerEndDate = new Date()
              this.schedulerEndDate.setDate(this.schedulerStartDate.getDate() + 1);
          
            }
            this.selectedActionFrequency = { alies: this.actionData['scheduler']['granularityType'], name: this.actionData['scheduler']['granularityType'] };
            // this.selectedSchecularStatus = {'label':this.actionData['status'],'value':this.actionData['status']}
            this.selectedSchecularStatus = this.actionData['status'];
            for (const element in this.actionData['channel']) {
              this.actionSelectedChannel.push({ 'alies': this.actionData['channel'][element]['source'], 'name': this.actionData['channel'][element]['source'] });
              if (this.actionData['channel'][element]['source'] == 'Email') {
                this.actionEmailChannel = this.actionData['channel'][element]['receivers']
              } else {
                this.slackChannel = this.actionData['channel'][element]['receivers'];
              }
            }
            if (this.actionSelectedChannel.filter(item => item['name'] == 'Email').length > 0)
              this.isEmailAction = true
            else
              this.isEmailAction = false

            if (this.actionSelectedChannel.filter(item => item['name'] == 'Slack').length > 0)
              this.isSlackAction = true
            else
              this.isSlackAction = false
          }
        }
      });
  }

  editActionDetails() {
    let channel = [{
      source: "Email",
      receivers: this.actionEmailChannel
    }]
    const req = {
      instance_type: "action",
      opcode: "update",
      action_id: this.alertData['alertData']['action_id'],
      name: this.actionName,
      description: this.actionDesc,
      status: this.selectedSchecularStatus,

      scheduler: {
        granularityType: this.selectedActionFrequency.name,
        granularityTime: moment(this.schedulerStartDate).format('HH:MM:SS'),
        granularityDay: "",
        granularityDate: moment(this.schedulerStartDate).format('DD'),
        startDate: moment(this.schedulerStartDate).format('YYYY-MM-DD'),
        endDate: moment(this.schedulerEndDate).format('YYYY-MM-DD')
      },
      channel: channel
    }

    this.dataFetchServ
      .updateAlertGroup(req)
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        } else {
          this.toastService.displayToast({
            severity: 'success',
            summary: 'Update Action Details',
            detail: 'Action Details updated successfully'
          });
        }
        this.ref.close(true);
      });
  }


  diff_minutes(dt2, dt1) {
    var diff = (dt2.getTime() - dt1.getTime()) / 1000;
    diff /= 60;
    return Math.abs(Math.round(diff));
  }

  selectSchedularStatus(e) {
  }

  onChangeActionChannel() {
    if (this.actionSelectedChannel.filter(item => item['name'] == 'Email').length > 0)
      this.isEmailAction = true
    else
      this.isEmailAction = false

    if (this.actionSelectedChannel.filter(item => item['name'] == 'Slack').length > 0)
      this.isSlackAction = true
    else
      this.isSlackAction = false
  }

  public onAddEmail(event) {
    this.isEmail = this.validateEmail(event.value)
    if(this.isEmail == false)
    {
      this.actionEmailChannel.pop();
    }
  }

  public onAddSlackEmail(event) {
    this.isSlackEmail = this.validateEmail(event.value)
  }

  validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
  // this.values.pop();
}
