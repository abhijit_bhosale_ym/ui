import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectButtonModule, SelectButton } from 'primeng/selectbutton';
import { ButtonModule } from 'primeng/button';
import { TabViewModule } from 'primeng/tabview';
import { TreeTableModule } from 'primeng/treetable';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MultiSelectModule } from 'primeng/multiselect';
import { ChipsModule } from 'primeng/chips';
import {
  InputTextareaModule,
  DropdownModule,
  CheckboxModule,
  InputSwitchModule,
  StepsModule
} from 'primeng';

import { MessageModule } from 'primeng/message';
import {SpinnerModule} from 'primeng/spinner';

import { SharedModule } from 'src/app/_pipes/shared.module';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { FilterContainerModule } from '../common/filter-container/filter-container.module';
import { CardsModule } from '../common/cards/cards.module';
import { ChartsModule } from '../common/charts/charts.module';
import { RouterModule } from '@angular/router';
import { AlertCreateAppRoutesAppRoutes } from './alert-create-app-routing.module';
import { AlertCreateAppComponent } from './alert-create-app.component';
import { TableModule } from 'primeng/table';
import { AddAlertComponent } from './add-alert/add-alert.component'
import {DragDropModule} from 'primeng/dragdrop';
import {AccordionModule} from 'primeng/accordion';
import {CardModule} from 'primeng/card';
import {CalendarModule} from 'primeng/calendar';
import {InputMaskModule} from 'primeng/inputmask';
import {AutoCompleteModule} from 'primeng/autocomplete';
import { ConfirmDialogModule } from 'primeng/confirmdialog';

@NgModule({
  declarations: [AlertCreateAppComponent,AddAlertComponent],
  imports: [
    CommonModule,
    SelectButtonModule,
    ButtonModule,
    TabViewModule,
    TreeTableModule,
    FormsModule,  
    ReactiveFormsModule,
    MultiSelectModule,
    SharedModule,
    FilterContainerModule,
    CardsModule,
    ChartsModule,
    TableModule,
    InputTextareaModule,
    DropdownModule,
    CheckboxModule,
    InputSwitchModule,
    StepsModule,
    MessageModule,
    DragDropModule,
    ChipsModule,
    AccordionModule,
    CardModule,
    SpinnerModule,
    CalendarModule,
    InputMaskModule,
    AutoCompleteModule,
    SelectButtonModule,
    ConfirmDialogModule,
    RouterModule.forChild(AlertCreateAppRoutesAppRoutes)
  ],
  providers: [FormatNumPipe]
})
export class AlertCreateAppRoutesAppModule {}
