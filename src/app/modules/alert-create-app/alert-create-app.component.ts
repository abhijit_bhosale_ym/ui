import { Component, OnInit, OnDestroy, ViewContainerRef} from '@angular/core';
import { TreeNode } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';
import { DynamicDialogConfig,DynamicDialogRef,DialogService } from 'primeng/dynamicdialog';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { CommonLibService } from 'src/app/_services';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';
import {
  Router,
  ActivatedRoute,
  NavigationExtras,
  NavigationEnd,
  RouterEvent
} from '@angular/router';
import {
  filter,
  takeUntil,
  pairwise,
  startWith,
  timeout
} from 'rxjs/operators';
import { Subject } from 'rxjs';

import { FetchApiDataService } from './fetch-api-data.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { Subscription } from 'rxjs';
import { AlertByAlertGroupPopupComponent } from './alert-by-alertgroup-popup/alert-by-alertgroup-popup.component'
import { AlertGroupInfoComponent } from './alert-group-info/alert-group-info.component'
import { EditAlertGroupComponent } from './edit-alert-group/edit-alert-group.component'
import { getLocaleFirstDayOfWeek } from '@angular/common';


@Component({
  selector: 'ym-alert-create-app',
  templateUrl: './alert-create-app.component.html',
  styleUrls: ['./alert-create-app.component.scss']
})
export class AlertCreateAppComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;
  appConfig: object = {};
  lastUpdatedOn: Date;
  filtersApplied: object = {};
  tableDimColDef: any[];
  aggTableJson: Object;
  nextUpdated: Date;
  dataUpdatedThrough: Date;
  aggTableData: TreeNode[];
  noTableData = false;
  start_Date: String;
  end_Date: String;
  severity = '';
  audit_id: String;
  table_Data = [];
  timeout: any;
  globalSearchValue = "";
  displayAggTable = true;
  conditionNames: any[];
  searchInput: any;
  value : any;
  colSearch : any = {};
  public destroyed = new Subject<any>();

  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private router: Router,
    private currentActivatedRoute: ActivatedRoute,
    private confirmationService: ConfirmationService,
  ) {
    this.router.events
      .pipe(
        filter((event: RouterEvent) => event instanceof NavigationEnd),
        pairwise(),
        filter((events: RouterEvent[]) => events[0].url === events[1].url),
        startWith('Initial call'),
        takeUntil(this.destroyed)
      )

      .subscribe(() => {
        // If it is a NavigationEnd event re-initalise the component
        // this.usersService.getUsersList().subscribe(res => {
        //   console.log('res-----', res);

        //   this.users = <IUser[]>res['data'];
        // });
      });


   }


  ngOnInit() {

    this.tableDimColDef = [
      {
        field: 'event_name',
        displayName: 'Alert Group Name',
        format: '',
        width: '190',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
      },
      {
        field: 'display_app_name',
        displayName: 'App Name',
        format: '',
        width: '120',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },

      {
        field: 'run',
        displayName: 'Run',
        format: '',
        width: '30',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: true
        },
      },
      {
        field: "scheduler_frequency['granularityType']",
        displayName: 'Scheduler frequency',
        format: '',
        width: '50',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },

      },
      {
        field: 'delete',
        displayName: 'Delete',
        width: '40',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: false,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'edit',
        displayName: 'Edit',
        width: '40',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: false,
          resizable: true,
          movable: true
        },
      },

      {
        field: 'info',
        displayName: 'Info',
        width: '30',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: false,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'update_action',
        displayName: 'Update Action',
        width: '50',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: false,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'created_at',
        displayName: 'Created At',
        width: '100',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'updated_at',
        displayName: 'Updated At',
        width: '100',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
    ];
    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,
      footerColumns: this.tableDimColDef,
      columns: this.tableDimColDef,
      selectedColumns: this.tableDimColDef,
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: true
    };

    this.conditionNames = [
      { label: 'GTE', value: '<<GTE' },
      { label: 'LTE', value: '<<LTE' }
      // { label: 'Equal', value: '' },
      // { label: 'like', value: '<<like' }
    ];

    this.searchInput = '>';
    // this.fieldName='revenue';
    this.value = this.conditionNames[0].value;


    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        console.log("appconfig--", this.appConfig);

        this._titleService.setTitle(this.appConfig['displayName']);
        let startDate;

        this.initialLoading();
      }
    });
  }

  initialLoading() {
    // this.dataFetchServ
    //   .getLastUpdatedData(this.appConfig['id'])
    //   .subscribe(data => {
    //     if (data['status'] === 0) {
    //       this.toastService.displayToast({
    //         severity: 'error',
    //         summary: 'Server Error',
    //         detail: 'Please refresh the page'
    //       });
    //       console.log(data['status_msg']);
    //       return;
    //     }
    //     this.lastUpdatedOn = moment(data[0]['updated_at']).toDate();
    //     this.nextUpdated = moment(data[0]['next_run_at']).toDate();
    //     this.dataUpdatedThrough = moment(data[0]['source_updated_through'], 'YYYYMMDD').toDate();
    //   });
//    this.loadTableData();
this.loadAlertGroups()
}
  customSearch(value, fieldName, inputValue, tableColDef) {
    tableColDef[
      tableColDef.findIndex(x => x.field == fieldName)
    ].condition = this.conditionNames[
      this.conditionNames.findIndex(x => x.value == value)
    ].label;

    if (value != '') {
      this.onSearchChanged(tableColDef);
    }
  }


  loadAlertGroups(){

    const tableReq = {
      instance_type: "event",
      opcode: "getAll",
      user_id: this.appConfig['user']['id'].toString(),
      client_code: "FR",
      column_search: this.colSearch,
      global_search: {
        keys : ['action_status', 'app_name', 'created_at','display_app_name', 'event_id',
                'event_name', 'stage_name', 'updated_at'],
        value : [this.globalSearchValue]
              }
    };

    this.dataFetchServ.getAlertTableData(tableReq).subscribe(data => {
      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      } else {
        this.noTableData = false;
      }

      this.table_Data = data['opcode_data'];
      const arr = [];
      this.table_Data.forEach((row: object) => {
        // row['event_happen_time'] = this.convertToLocalDate(row['event_happen_time']);
        // row['delivery_channel'] = row['delivery_channel'].map(e=>e['source'])
        let obj = {};
        obj = {
          data: row
        };
        arr.push(obj);
      });

      this.reloadAggTable();

      this.aggTableData = <TreeNode[]>arr;
      this.aggTableJson['totalRecords'] = data['opcode_data'].length;
      this.aggTableJson['loading'] = false;
    });
  }
  onSearchChanged(tableColDef) {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
     this.timeout = setTimeout(() => {
      let obj = {};

      tableColDef.forEach(element => {
           if (element.value != '' && element.value != null){

            if(element.field == 'delivered_Channel')
                obj['delivery_channel'] = element.value
            else
                obj[element.field] = element.value
           }
      })

      this.colSearch = obj;
      this.loadTableData()
       }, 3000);
  }

  runAlertGroup(event_id){

    this.dataFetchServ.runAlert({event_id : event_id.toString()}).subscribe(data => {
      if (data['status'] === 0) {
      }
    });
  }

  addAlert(){
    console.log("in add ");
    this.router.navigate(['/alert-mgmt/add-alert'], {
      relativeTo: this.currentActivatedRoute
    });

  }

  resetSearch(fieldName, tableColDef) {
    console.log('reset', fieldName, tableColDef);
    tableColDef.find(o => o.field == fieldName).value = '';
    this.onSearchChanged(tableColDef);
  }


  convertToLocalDate = (datetimeStr) => {
    let upDate = new Date(new Date().toLocaleString("en-US", { timeZone: "America/New_York" })); //London current time
    let currentDate = new Date(); //My current time
    let timeDifference = this.diff_minutes(currentDate, upDate);
    var date = new Date(datetimeStr);
    date.setMinutes(date.getMinutes() + timeDifference);
    return moment(date).format('MM/DD/YYYY hh:mm A Z');
  }

  diff_minutes(dt2, dt1) {
    var diff = (dt2.getTime() - dt1.getTime()) / 1000;
    diff /= 60;
    return Math.abs(Math.round(diff));
  }

  loadTableData() {


//     "{
//       ""instance_type"": ""event"",
//       ""opcode"": ""getAll"",
//       ""user_id"":""1"",
//       ""client_code"":""FR"",
//       ""column_search"": {
//               ""action_status"": ""active"",
//               ""action_scheduler"": ""daily""
//       },
//       ""global_search"": {
//               ""keys"":[""stage_name"",""event_name""],
//               ""value"": [""table""]
//       }
// }"

    this.aggTableJson['loading'] = true;



    const tableReq = {
      instance_type: "event",
      opcode: "getAll",
      user_id: this.appConfig['user']['id'].toString(),
      start_date: this.start_Date,
      end_date: this.end_Date,
      severity: this.severity,
      client_code: "FR",
      column_search: this.colSearch,
      global_search: {
        keys : ['action_status', 'app_name', 'created_at','display_app_name', 'event_id',
                'event_name', 'stage_name', 'updated_at'],
        value : [this.globalSearchValue]
              }
    };




    this.dataFetchServ.getAlertTableData(tableReq).subscribe(data => {
      console.log("==========",data);
      console.log("-----------",data['opcode_data'].length);


      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      } else {
        this.noTableData = false;
      }

      this.table_Data = data['opcode_data'];
      const arr = [];
      this.table_Data.forEach((row: object) => {
        row['event_happen_time'] = this.convertToLocalDate(row['event_happen_time']);
        row['delivery_channel'] = row['delivery_channel'].map(e=>e['source'])
        let obj = {};
        obj = {
          data: row
        };
        arr.push(obj);
      });

      this.reloadAggTable();


      console.log("------------------",data['opcode_data']);

      this.aggTableData = <TreeNode[]>arr;
      this.aggTableJson['totalRecords'] = data['opcode_data'].length;
      this.aggTableJson['loading'] = false;

    });
  }

  onFiltersApplied(filterData: Object) {
    console.log("filterData----", filterData);
    this.start_Date = moment(filterData['date'][0], 'YYYYMMDD').format('YYYY-MM-DD');
    this.end_Date = moment(filterData['date'][1], 'YYYYMMDD').format('YYYY-MM-DD');

    //if (!(JSON.stringify(filterData['filter']['dimensions']) === '{}')) {
    //  this.severity = filterData['filter']['dimensions'];
    //}

    console.log("filterData['filter']['dimensions']",filterData['filter']['dimensions']);
    console.log("........",filterData['filter']['dimensions'].length  );
    console.log(JSON.stringify(filterData['filter']['dimensions']));


    if((JSON.stringify(filterData['filter']['dimensions']) === '{}'))
        this.severity = ''
    else
        this.severity = filterData['filter']['dimensions']['severity'].join(',');

    this.loadTableData();
  }

  editAlert(rowData) {
    const data = {
      data: rowData,
      user_id: this.appConfig['user']['id'].toString()
    };
    const ref = this.dialogService.open(AlertByAlertGroupPopupComponent, {
      header: 'Alert Group : ' + rowData['event_name'],
      contentStyle: { 'max-height': '80vh', width: '80vw', overflow: 'auto'},
      data: data,
    });
    ref.onDestroy.subscribe(()=>{
        document.querySelector('body > p-dynamicdialog').remove();
    })
  }

  isHiddenColumn(col: Object) {

    console.log("..............",  this.aggTableJson['selectedColumns'].some(
      (c: Object) => c['field'] === col['field']
    ) ||
    this.aggTableJson['frozenCols'].some(
      (c: Object) => c['field'] === col['field']
    )
  );


    return (
      this.aggTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }
  onGlobalSearchChanged(searchValue, tableColDef) {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    this.timeout = setTimeout(() => {
      // this.flatTableReq['filters']['globalSearch'] = {
      //   dimensions: [],
      //   value: searchValue
      // };
      // tableColDef.forEach(element => {
      //   this.flatTableReq['filters']['globalSearch']['dimensions'].push(
      //     element.field
      //   );
      // });
      // this.loadFlatTableData(this.flatTableReq);
      this.globalSearchValue = searchValue;
      //this.loadTableData();
      this.loadAlertGroups();
    }, 3000);
  }

  alertGroupInfo(rowData) {
    const data = {
      event_id: rowData.event_id,
      action_id: rowData.action_id,
      user_id: this.appConfig['user']['id'].toString()
    };

    const ref = this.dialogService.open(AlertGroupInfoComponent, {
      header: 'Alert Group : ' + rowData['event_name'],
      contentStyle: { 'max-height': '90vh', width: '60vw', overflow: 'auto' },
      data: data,
    });
    ref.onClose.subscribe((data1: string) => { });
  }

  editAlertGroup(rowData) {
    console.log("rowData",rowData);

    const data = {
      alertData: rowData,
      user_id: this.appConfig['user']['id'].toString(),
      editFlag:"alertGroup"
    };

    const ref = this.dialogService.open(EditAlertGroupComponent, {
      header: 'Alert Group : ' + rowData['event_name'],
      contentStyle: { 'max-height': '90vh', width: '70vw', overflow: 'auto' },
      data: data,
    });
    ref.onClose.subscribe((data1: string) => {
      if (data1) {
        this.loadAlertGroups()
      }

    });
  }

  editActionDetails(rowData) {
    const data = {
      alertData: rowData,
      user_id: this.appConfig['user']['id'].toString(),
      editFlag:"actionDetails"
    };

    console.log("rowData",rowData);

    const ref = this.dialogService.open(EditAlertGroupComponent, {
      header: 'Update Action : ' + rowData['event_name'],
      contentStyle: { 'max-height': '90vh', width: '70vw', overflow: 'auto' },
      data: data,
    });
    ref.onClose.subscribe((data1: string) => {
      if (data1) {
        this.loadAlertGroups()
      }

    });
  }
  deleteAlertGroup(rowData) {
    this.confirmationService.confirm({
      message: 'Do you want to delete this alert group with its all alerts and an action',
      header: 'Delete Alert Group',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        const req = {
          "instance_type": "event",
          "opcode": "delete",
          "event_id": rowData['event_id'],
        }
        this.dataFetchServ
          .deleteAlert(req)
          .subscribe(data => {
            if (data['status'] === 0) {
              this.toastService.displayToast({
                severity: 'error',
                summary: 'Server Error',
                detail: 'Please refresh the page'
              });
              console.log(data['status_msg']);
              return;
            } else {
              this.toastService.displayToast({
                severity: 'success',
                summary: 'Delete Alert Group',
                detail: 'Alert Group deleted successfully'
              });
              this.loadAlertGroups();
            }
          });
      },
      reject: () => {
      }
    })
  }

  reloadAggTable() {
    this.displayAggTable = false;
    setTimeout(() => {
      this.displayAggTable = true;
    }, 0);
  }

  ngOnDestroy() {
    this.destroyed.next();
    this.destroyed.complete();
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }

}
