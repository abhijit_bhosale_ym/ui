import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertDashboardAppComponent } from './alert-dashboard-app.component';

describe('AlertDashboardAppComponent', () => {
  let component: AlertDashboardAppComponent;
  let fixture: ComponentFixture<AlertDashboardAppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertDashboardAppComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertDashboardAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
