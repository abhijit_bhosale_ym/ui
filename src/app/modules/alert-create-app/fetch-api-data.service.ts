import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FetchApiDataService {
  private BASE_URL: string = environment.baseUrl;

  constructor(private http: HttpClient) {}

  getAlertTableData(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/alert-mgmt/getData`;
    return this.http.post(url, params);
  }

  runAlert(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/alert-mgmt/runAlert`;
    return this.http.post(url, params);
  }

  getLastUpdatedData(appId) {
    const url = `${this.BASE_URL}/frankly/v1/common/getLastUpdatedData/${appId}`;
    return this.http.get(url);
  }

  public updateAlertGroup(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/alert-mgmt/getData`;
    return this.http.post(url, params);
  }
  
  deleteAlert(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/alert-mgmt/getData`;
    return this.http.post(url, params);
  }
}
