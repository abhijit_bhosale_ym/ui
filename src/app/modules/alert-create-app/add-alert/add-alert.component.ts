import { Component, OnInit, OnDestroy,Input } from '@angular/core';
import { SelectItemGroup, MenuItem } from 'primeng/api';
import { DynamicDialogConfig,DynamicDialogRef,DialogService } from 'primeng/dynamicdialog';

import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router,ActivatedRoute } from '@angular/router';
import { RoleService } from 'src/app/_services/users/role.service';
import { TeamService } from 'src/app/_services/users/team.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { UsersService, PlatformConfigService } from 'src/app/_services';
import { Subscription } from 'rxjs';
import { CommonLibService } from 'src/app/_services';
import { environment } from 'src/environments/environment';
import { FetchApiDataService } from '../fetch-api-data.service';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import * as moment from 'moment';
import { filter } from 'rxjs/operators';
import { getLocaleDateTimeFormat } from '@angular/common';


@Component({
  selector: 'ym-add-alert',
  templateUrl: './add-alert.component.html',
  styleUrls: ['./add-alert.component.scss']
})
export class AddAlertComponent implements OnInit, OnDestroy {
  @Input() childMessage: string;
  private appConfigObs: Subscription;
  private BASE_URL: string = environment.baseUrl;

  //appConfig: object = {filter : {filterConfig : { filters : {defaultDate : [], datePeriod : []} }}};
  appConfig: object = {filter : {filterConfig : { filters : {datePeriod : []}}}};
  userFlow: MenuItem[];
  activeStepIdx: any;
  isCreateRole = false;
  isCreateTeam = false;

  alertGroupDetailsForm: FormGroup;
  alertDetailsForm: FormGroup;
  alertDetailsDimForm : FormGroup;
  derivedForm: FormGroup;
  conditionMappingForm: FormGroup;

  availableAppName : any = []
  availableStages : any = []
  availableDimensions : any = []
  availableMetric : any = []
  selectedAppName : { app_name : '', display_app_name:''}
  selectedDimensions : []//{alies : '', name : ''}
  selectedMetric : []//{alies : '', name : ''}
  selectedMetricForAverage : []
  selectedStages: { stage_name: ''}  
  severities : any = [{'label' : 'Highest', 'value' : 'Highest'},{'label' : 'Medium','value' : 'Medium'},{'label' : 'Lowest','value' : 'Lowest'}]
  availableDimConditions : any = [{'label' : 'Include', 'value' : 'include'},{'label' : 'Exclude','value' : 'exclude'}]
  availableDimValues : any = [];
  selectedDimConditions : any;
  selectedDimValues : [];
  selectedSeverities : any//{'label' : 'Highest', 'value' : 'Highest'}
  selectedDimsMapping : {alies : '', name:''}
  dimMappingArray : any = [];
  event_id;
  enableDatePicker =false;
  addDerived = false;
  isTimeBucketCondition1 = false;
  sortingConditions :  any = [{'label' : 'ASC', 'value' : 'asc'},{'label' : 'DESC','value' : 'desc'},{'label' : 'BOTH','value' : 'both'}]
  schedularStatus :  any = [{'label' : 'Active', 'value' : 'active'},{'label' : 'Pause','value' : 'pause'},{'label' : 'Inactive','value' : 'inactive'}]
  channelList :  any = [{'alies' : 'Email','name' : 'email'}];
  operatorList = [{alies : '+', name : 'plus', type : 'op'},
                  {alies : '-', name : 'minus',  ype : 'op'},
                  {alies : '*', name : 'mulit', type : 'op'},
                  {alies : '/', name : 'divid', type : 'op'} 
                  ]
  bracketList = [ {alies : '(', name : 'divid',type : 'sbrt'},
                  {alies : ')', name : 'divid',type : 'ebrt'},
                ]                
  formatList = [
                {alies : 'Currency', name : 'currency'},
                {alies : 'Number', name : 'number'},
                {alies : 'Percentage', name : 'percentage'}
               ]
  availableAndOr = [{alies : 'AND', name : 'AND'},
                    {alies : 'OR', name : 'OR'}]
  metricMappingCondtions = [{alies : '=', name : 'equals'},
                  {alies : '>', name : 'greater'},
                  {alies : '>=', name : 'greater euqal'},
                  {alies : '<', name : 'less'},
                  {alies : '<=', name : 'less equal'},
                  {alies : 'Between', name : 'between'},
                  {alies : '!=', name : 'not equal'},
                  {alies : 'Top', name : 'worst'},
                  {alies : 'Worst', name : 'worst'}, 
                  ]

  schedulerFreuwncy = [{alies : 'Once', name : 'once'},
                      {alies : 'Hourly', name : 'hourly'},
                      {alies : 'Daily', name : 'daily'},
                      {alies : 'Weekly', name : 'weekly'},
                      {alies : 'Monthly', name : 'monthly'},
                      {alies : 'Yearly', name : 'yearly'}
  ]

  availableExpected = [  
                  {alies : 'UP', name : 'up'},
                  {alies : 'Down', name : 'down'}]

  selectedSorting : any 
  selectedExpected : any;              
  addDerivedLbl : any = "Add Derived Metric : Please select atleast 2 Metrics"
  selectedformat : any;       
  selectedMerticArray : any = [];
  selectedMerticArrayObj : any = [];
  selectedAndOr : any;
  selectedSortingMetric  = [];
  selectedSchecularStatus : any;
  selectedChannel : [];
  actionSelectedChannel : [];
  selectedTimeBucketMetric : {};
  selectedTimeBucketCondition : {}
  selectedActionSchecularStatus : any; 
  timeBucketCondition1;
  timeBucketCondition2;
  emailChannel : [];
  isValidAlertChannelEmail : boolean = true
  isValidActionChannelEmail : boolean = true
  availableTotalMetrics : any;

  isEmail : boolean = false;
  isSlack : boolean = false;
  isEmailAction : boolean = false;
  isSlackAction : boolean = false;

  selectedFrequency : any;
  selectedActionFrequency : any
  actionStartDate : any;
  actionEndDate : any
  selectedData : any;
  draggedMetric : any;
  derivedMetricArray : any = [];
  derivedMetricName : any;
  enableMetrics : true;
  enableOperators : false;
  enableBrackets : true;                
  isValidDerviedMetric : boolean = true;             
  conditionMappingFormArr : any = [];
  metricMappingFormArr : any = [];             
  sortingNoRecords : any;
  timeBucketFormArray : any = [];
  schedularStartDate : any;
  schedularEndDate : any;
  schedularMinDate : '20200502'
  isTimeBucketCondition = false;
  alertDateRange = []
  actionName;
  actionDesc = '';
  slackChannelAction
  emailChannelAction
  actionEmailChannel
  dateRangeSelectionType = 'custom'
  isEdit : boolean = false;
  groupedCars : any = [];
  sub :any
  alertName = ""
  selectedSortingColumns : any = [];
  availableSortingColumns : any = []
  isprevAlertGroup = false;  

  constructor(
    private formBuilder: FormBuilder,
    public router: Router,
    private roleServ: RoleService,
    private teamServ: TeamService,
    private userService: UsersService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private platformConfigService: PlatformConfigService,
    private libServ: CommonLibService,
    private dataFetchServ: FetchApiDataService,
    private appConfigService: AppConfigService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {

    console.log("....routs.......",this.router.getCurrentNavigation());
    console.log("...",this.childMessage);
    

    this.userFlow = [
      {
        label: 'Fill Alert Group Details',
        command: (event: any) => {
          this.activeStepIdx = 0;
        }
      },
      {
        label: 'Fill Alert Details',
        command: (event: any) => {
          this.activeStepIdx = 1;
        }
      },
      {
        label: 'Alert Group : Action Details',
        command: (event: any) => {
          this.activeStepIdx = 2;
        }
      },
    ];



    this.activeStepIdx = 1 ;
    
    //this.getDimMetricByEventId();
    this.addDerived = false
    this.isTimeBucketCondition = false
    console.log("app config : ",this.appConfig);
    
    const today = new Date();
    let firstDate = new Date()
    let lastDate = new Date();

    firstDate.setDate(today.getDate() - 7);
    lastDate.setDate(today.getDate() - 1);

    this.actionStartDate = new Date()
    this.actionEndDate = new Date()
    this.actionEndDate.setDate(this.actionStartDate.getDate() + 1);

    this.schedularStartDate = new Date();
    this.schedularEndDate = new Date()
    this.schedularEndDate.setDate(this.schedularStartDate.getDate() + 1);


    this.alertDateRange.push(firstDate,lastDate)

    this.timeBucketFormArray.push({
      selection_type : 'last_7_days',
      rangeDates : [firstDate,lastDate],
      contribution : false
    })

    let obj = 
      [
        {
              "key": "time_key",
              "label": "Date" ,
              "minDate": "20180901",
              "filterType": "date_range",
              "defaultDate": [
                    {
                          "value": 0,
                          "period": "month",
                          "startOf": true
                    },
                    {
                          "value": 0,
                          "period": "day"
                    }
              ]
        }
  ]
    
  this.appConfig['filter']['filterConfig']['filters']['datePeriod'] = obj;

    this.selectedSeverities = this.severities[0].label
    this.selectedDimConditions = this.availableDimConditions[1]. value
    this.selectedSorting = this.sortingConditions[0].value
    this.selectedSchecularStatus = this.schedularStatus[0].value    
    this.selectedActionSchecularStatus = this.schedularStatus[0].value
    this.alertGroupDetailsForm = this.formBuilder.group({
      alertGroupName: ['', Validators.required],
      selectedAppName : [this.selectedAppName, Validators.required],
   //   selectedStages : [this.selectedStages, Validators.required],//new FormControl(this.selectedStages,Validators.required),

      alertGroupDesc : new FormControl()
    });

    this.alertDetailsForm = this.formBuilder.group({
      alertName: ['', Validators.required],
      // alertDesc : new FormControl(),
      // severity : new FormControl(),
      // selectedDimensions : [this.selectedDimensions,Validators.required],
      // selectedMetric : [this.selectedMetric,Validators.required],
      // selectedMetricForAverage :  new FormControl(),//[this.selectedMetricForAverage,Validators.required],
      //  //addDerivedForm : new FormControl(),     
      // alertDateRange : new FormControl(), 
     // sortingNoRecords: ['', [Validators.required, Validators.pattern("^[0-9]*$")]]
    });
    this.alertDetailsDimForm = this.formBuilder.group({
      selectedDimensions : ['', Validators.required],
      selectedMetric : ['', Validators.required]

    });

    this.derivedForm = this.formBuilder.group({
      draggedMetric : new FormControl(),
      selectedformat : [this.selectedformat, Validators.required],
      addButton : new FormControl(),
      resetButton : new FormControl(),
      derivedMetricName : ['',Validators.required],
      selectedExpected : [this.selectedExpected, Validators.required],
    })

    this.conditionMappingForm = this.formBuilder.group({
      selectedDimsMapping : [this.selectedDimsMapping,Validators.required],
      dimConditions : new FormControl(), //[this.selectedDimConditions,Validators.required]
      selectedDimValues : new FormControl(),
      selectedAndOr : new FormControl()
    })



    this.loadAppNames();
  }

  onDateSelected() {
  //  this.dateRangeSelectionType = 
    document.body.click();
  }
  changeDate(period: string) {
    const today = new Date();
    let firstDate = new Date();
    let lastDate = new Date();
    let rangeDates = [];

    console.log('range dates in change', rangeDates);
    console.log('period',period);
    
    switch (period) {
      case 'yesterday':
        {
          firstDate.setDate(today.getDate() - 1);
          rangeDates = [firstDate, firstDate];
        }
        break;
      case 'last_7_days':
        {
          firstDate.setDate(today.getDate() - 7);
          lastDate.setDate(today.getDate() - 1);
          rangeDates = [firstDate, lastDate];
        }
        break;
        case 'day_before_yesterday':
          {
            firstDate.setDate(today.getDate() - 2);
            lastDate.setDate(today.getDate() - 2);
            rangeDates = [firstDate, lastDate];
          }
          break;
        case 'last_15_days':
          {
            firstDate.setDate(today.getDate() - 15);
            lastDate.setDate(today.getDate() - 1);
            rangeDates = [firstDate, lastDate];
          }
          break;
      case 'last_30_days':
        {
          firstDate.setDate(today.getDate() - 30);
          lastDate.setDate(today.getDate() - 1);
          rangeDates = [firstDate, lastDate];
        }
        break;
      case 'this_month':
        {
          firstDate = new Date(today.getFullYear(), today.getMonth(), 1);
          lastDate = new Date(today.getFullYear(), today.getMonth() + 1, 0);
          rangeDates = [firstDate, lastDate];
        }
        break;
      case 'last_month':
        {
          firstDate = new Date(
            today.getFullYear() - (today.getMonth() > 0 ? 0 : 1),
            (today.getMonth() - 1 + 12) % 12,
            1
          );
          lastDate = new Date(today.getFullYear(), today.getMonth(), 0);
          rangeDates = [firstDate, lastDate];
        }
        break;
        case 'this_year':
          {
            firstDate = new Date(
              moment()
                .startOf('year')
                .toDate()
            );
            lastDate.setDate(today.getDate());
            rangeDates = [firstDate, lastDate];
  
          }
          break; 
        case 'last_year' :
         firstDate = moment().subtract(1, 'years').startOf('year').toDate()
         lastDate = moment().subtract(1, 'years').endOf('year').toDate()
        rangeDates = [firstDate, lastDate];
        break;
       default :
         period = 'custom'    
    }
    return rangeDates;
  }
  onChangeTimeBucketDate(period: string, i){

    let d = this.changeDate(period);
    this.timeBucketFormArray[i].rangeDates = d;

  }
  onChangeSev(){
    console.log("selected ser",this.selectedSeverities);    
  }


  dragStart(event,metric: any) {   
    console.log("DRAG START",metric);
    
      this.draggedMetric = metric
  }

drop(event) {
  console.log("in drop ",this.draggedMetric);
  console.log("alies",this.draggedMetric['alies']);
  
  this.selectedMerticArrayObj.push(this.draggedMetric)
  this.selectedMerticArray.push(this.draggedMetric['alies']);
  this.selectedData = this.selectedMerticArray.join('')

  console.log("selected data ",this.selectedData);
  console.log("selectedMerticArray",this.selectedMerticArray);
  
}
dragEnd(event) {
    this.draggedMetric = null;
}

onRemoveChip(e)
{
  console.log("event",e);
  //this.selectedMerticArrayObj = this.selectedMerticArrayObj
  console.log("selectedMerticArrayObj",this.selectedMerticArrayObj);
  
  console.log("",this.selectedMerticArray);
  console.log("selected arr ",this.selectedMerticArray);  
}

onResetDerviedMetric(){
  this.selectedMerticArray = [];
  this.selectedMerticArrayObj = [];
  this.derivedMetricName = '';
  this.selectedformat = {};

}
onAddDerivedMetrics(){

  console.log("on add",this.selectedMerticArray);
  console.log("selectedMerticArrayObj",this.selectedMerticArrayObj);
  this.isValidDerviedMetric = true;

  let sbkt = 0;
  let ebkt = 0;
  
  if(this.selectedMerticArrayObj.length == 0)
      this.isValidDerviedMetric = false;
  else{
  this.selectedMerticArrayObj.forEach((element, idx)=> {

    console.log("..........ele",element);
    console.log("idx",idx);
    
    if(element['type'] = 'sbrt')
        sbkt = sbkt + 1;
    
    if(element['type'] = 'ebkt')
        ebkt = ebkt + 1;    

    if(this.selectedMerticArrayObj.length == 1 && idx == 0 && (element['type']== 'op' ||  element['type']== 'ebkt'))
      {
       this.isValidDerviedMetric = false; 
      }
  
    else if(element['type'] == 'metric'){
      if(this.selectedMerticArrayObj[idx-1] != undefined && this.selectedMerticArrayObj[idx-1]['type'] != 'op' && this.selectedMerticArrayObj[idx-1]['type'] != 'sbrt')
          {
            console.log("in if metric - ");
            
            this.isValidDerviedMetric = false; 
          }
      if(this.selectedMerticArrayObj[idx+1] != undefined &&  this.selectedMerticArrayObj[idx+1]['type'] != 'op' && this.selectedMerticArrayObj[idx+1]['type'] != 'ebrt' && this.selectedMerticArrayObj[idx+1]['type'] != 'sbrt')
          {
            console.log("in if metric + ");
            this.isValidDerviedMetric = false; 
          }

        }
    else if(element['type'] == 'op'){
      if(this.selectedMerticArrayObj[idx-1] != undefined && (this.selectedMerticArrayObj[idx-1]['type'] != 'metric' && this.selectedMerticArrayObj[idx-1]['type'] != 'ebrt' && this.selectedMerticArrayObj[idx-1]['type'] != 'number')){
        console.log("in if op - ");
        this.isValidDerviedMetric = false;
      }
      if (this.selectedMerticArrayObj[idx+1] != undefined && (this.selectedMerticArrayObj[idx+1]['type'] != 'metric') && (this.selectedMerticArrayObj[idx+1]['type'] != 'sbrt'))  { 
        console.log("in if metric op + ");
        this.isValidDerviedMetric = false;
      }
    }

    else if(element['type'] == 'number'){
      if(this.selectedMerticArrayObj[idx-1] != undefined && (this.selectedMerticArrayObj[idx-1]['type'] != 'op' && this.selectedMerticArrayObj[idx-1]['type'] != 'sbrt')){
        console.log("in if number - ");
        this.isValidDerviedMetric = false;
      }
      if (this.selectedMerticArrayObj[idx+1] != undefined && (this.selectedMerticArrayObj[idx+1]['type'] != 'op') && (this.selectedMerticArrayObj[idx+1]['type'] != 'ebrt'))  { 
        console.log("in if metric number + ");
        this.isValidDerviedMetric = false;
      }
    }

   else if( element['type'] == 'sbrt') 
    {
      if(this.selectedMerticArrayObj[idx-1] != undefined && this.selectedMerticArrayObj[idx-1]['type'] != 'op'){
        console.log("in if sbrt - ");
        this.isValidDerviedMetric = false;
      
        }
      else if(this.selectedMerticArrayObj[idx+1] != undefined && (this.selectedMerticArrayObj[idx+1]['type'] != 'metric' && this.selectedMerticArrayObj[idx-1]['type'] != 'number')){
        console.log("in if sbrt + ");
        this.isValidDerviedMetric = false
      }
    }
    else if(element['type'] == 'ebrt') 
    {
      if(this.selectedMerticArrayObj[idx-1] != undefined && ( this.selectedMerticArrayObj[idx-1]['type'] != 'metric'))
      {   
       this.isValidDerviedMetric = false;
       console.log("in if ebrt - ");
      }
      else if(this.selectedMerticArrayObj[idx+1] != undefined && (this.selectedMerticArrayObj[idx+1]['type'] != 'op')){
        console.log("in if ekbt + "); 
        this.isValidDerviedMetric = false
      }
    }
  });


  if(this.selectedMerticArrayObj[this.selectedMerticArrayObj.length-1]['type'] == 'op' || this.selectedMerticArrayObj[this.selectedMerticArrayObj.length-1]['type'] == 'sbrt'){
    console.log("in ...............");
    
    this.isValidDerviedMetric = false;  
  }

  if(ebkt % 2 == 0)
    this.isValidDerviedMetric = false;  

  if(ebkt % 2 == 0)
    this.isValidDerviedMetric = false;  

  console.log("valid---------",this.isValidDerviedMetric);
  

  if(this.isValidDerviedMetric){
    const obj = {
    name : this.derivedMetricName,
    format : this.selectedformat.alies,
    formula : this.selectedMerticArray.join(' '),  
    formulaArr : this.selectedMerticArray,
    expected : this.selectedExpected
  }


  

  //this.derivedMetricArray[this.derivedMetricArray.length]=(this.selectedMerticArray.join(''))
  this.derivedMetricArray[this.derivedMetricArray.length] = obj


  console.log("derived metric array",this.derivedMetricArray);
  

  this.availableTotalMetrics.push({name : this.derivedMetricName, alies : this.derivedMetricName, type : 'metric',isDerived : true,formula : this.selectedMerticArray.join(' '),})


  this.onResetDerviedMetric()
}
  // this.selectedMerticArray = [];
  // this.derivedMetricName = '';
  // this.selectedformat = {};
  
  }

 // this.derivedMetricArray[this.derivedMetricArray.length]=(this.selectedMerticArray.join(''))

// this.derivedMetricArray[this.derivedMetricArray.length] = obj

  console.log("derived",this.derivedMetricArray);
  // this.selectedMerticArray = [];
  // this.derivedMetricName = '';
  // this.selectedformat = {};
  
}

onChangeDerivedChkBox(value)
{
  this.addDerived = !value              
}
onChangeTimeBucketCondition(value){

  console.log("..............................",value);
  
  
  this.isTimeBucketCondition1 = !value

}
  appNameChanged(){

    console.log("selected app ",this.selectedAppName);
    const req = {
      "instance_type": "appTableMapping",
      "opcode":"getApp",
      "client_code":"FR",
      "app_name": this.selectedAppName['app_name']
    }

    this.dataFetchServ
    .getAlertTableData(req)
    .subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }

      this.availableStages = data['opcode_data']

    });
  }
  stageChanged(){

  }

  loadAppNames(){    
    const req = {

        "instance_type": "appTableMapping",
        "opcode":"get",
        "client_code":"FR"
    }

    this.dataFetchServ
    .getAlertTableData(req)
    .subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }

      console.log("app names : ",data);
      
      this.availableAppName = data['opcode_data']
    });
  }

  getDimMetricByEventId(){
    const req ={  
        "instance_type": "event",
        "opcode": "getDimMetric",
        //"event_id": "360"//this.event_id
        "event_id": this.event_id
    }

    this.dataFetchServ
    .getAlertTableData(req)
    .subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }


//       "{
//         ""opcode_data"": {
//                 ""dimensions"": [{
//                         ""alies"": ""Source Name"",
//                         ""name"": ""source_name""
//                 }],
//                 ""metrics"": [{
//                         ""alies"": ""Impressions"",
//                         ""contribution"": ""True"",
//                         ""format"": ""number"",
//                         ""name"": ""impressions""
//                 }]
//         },
//         ""status"": 1,
//         ""status_msg"": ""success""
// }"

      console.log("dim metric list : ",data);
     
      this.availableDimensions = data['opcode_data']['dimensions']
      
   //   this.availableMetric = data["opcode_data"]['metrics']
      

      this.availableMetric = data["opcode_data"]['metrics'].map( x => {
            x = Object.assign(x,{type : "metric", isDerived : false})
        return x
        })
      
      this.availableTotalMetrics = this.libServ.deepCopy(this.availableMetric)

      this.availableSortingColumns = this.libServ.deepCopy(this.availableDimensions);
      this.availableSortingColumns.push(...this.availableMetric);

      console.log("dims",this.availableDimensions);
      console.log("metrics",this.availableMetric);
  
      this.conditionMappingFormArr.push({
        dims : this.availableDimensions,
        condition : this.availableDimConditions,
        values : [],
        selectedDim : '',
        selectedCondition : this.availableDimConditions[1]. value,
        selectedValues : []
      })

      this.metricMappingFormArr.push({
        metric : this.availableTotalMetrics,
        selectedMetric : {},
        condition : this.metricMappingCondtions,
        selectedCondition : {},
        value : '',
        isBetweenValue : ''
      })
    });
  }

  onMetricMappingRemoved(index){
      this.metricMappingFormArr.splice(index,1)
  }
  onDimMappingRemoved(index){
    this.conditionMappingFormArr.splice(index,1)
  }
  selectedMetricConditionMap(name){

  }
  removeMetric(metric){

    console.log("metric",metric);
    console.log("selectedMerticArrayObj",this.derivedMetricArray);
    
   // delete this.derivedMetricArray[metric]
   this.derivedMetricArray.splice(this.derivedMetricArray.indexOf(metric),1)
    console.log("selectedMerticArrayObj",this.derivedMetricArray);
    
  }
  removeDimMapping(dim){
    this.dimMappingArray.splice(this.dimMappingArray.indexOf(dim),1)
  }

  onAddChips(e){
    console.log("event : ",e);
    const arr = ['0','1','2','3','4','5','6','7','8','9','.']
    console.log("...",parseFloat(e.value));

    
   if(!isNaN(parseFloat(e.value)))
    {
      console.log("in if nan");

      const obj = {alies : e.value, type : 'number'}
      //this.selectedMerticArray.push(e.value)
      this.selectedMerticArrayObj.push(obj)
    }
    else
     this.selectedMerticArray.splice(this.selectedMerticArray.length-1)

    console.log("selected arr",this.selectedMerticArrayObj);
    

    }

    onAddEmailChips(e){
      console.log('in chips add',e);
      let isValid = true;

      let regexp = new RegExp(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      );
     this.emailChannel.forEach(val => {
        if (!regexp.test(val)) {
          isValid = false
        }
      });
  
      if(isValid)
        this.isValidAlertChannelEmail = true
      else
        this.isValidAlertChannelEmail = false

    }

    onRemoveEmailChip(e)
    {
      let regexp = new RegExp(
        /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      );

      let isValid = true;

      this.emailChannel.forEach(val => {
        if (!regexp.test(val)) {
          isValid = false
        }
      });

      if(isValid)
        this.isValidAlertChannelEmail = true
      else
        this.isValidAlertChannelEmail = false
    }

  onAlertGroupSubmit( ){

    //if(this.userFlow.length == 3)    
  //  {

    let opcode = "create";
    if(this.isprevAlertGroup)
      opcode = "update"


      if(this.alertGroupDetailsForm.value.alertGroupDesc === null){
        this.alertGroupDetailsForm.value.alertGroupDesc = ""
      }

      const req = {
        "instance_type": "event",
        "opcode": opcode,
        "app_name": this.alertGroupDetailsForm.value.selectedAppName['app_name'],
        "data_processing_stage": "Table",//this.alertGroupDetailsForm.value.selectedStages['stage_name'],
        "event_name": this.alertGroupDetailsForm.value.alertGroupName,
        "description":this.alertGroupDetailsForm.value.alertGroupDesc,
        "status": "Active",
        "client_code": "FR",
        "user_id":"1"
      }

      if(this.isprevAlertGroup)
        req['event_id'] = this.event_id;

      this.dataFetchServ
      .getAlertTableData(req)
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }

        if(data['opcode_data']){
            this.event_id = data['opcode_data']
            let msg = "Alert Group created successfully"
            
            if(this.isprevAlertGroup)
              msg = "Alert Group updated successfully"


            this.toastService.displayToast({
              severity: 'success',
              summary: 'Alert Group',
              detail: msg
            });


          this.activeStepIdx = 1 
            
          this.getDimMetricByEventId();
          }
      });
    //}
  }
   

  onNextCondtionMapping()
  {
    this.activeStepIdx = 3 

  }
  onBackCondtionMapping()
  {
    this.activeStepIdx = 1
  }
  onNextSortingSchedular(){

    let channel = [];
    if(this.actionEmailChannel != []){  
     let channel = {
      source : "Email",
      receivers : this.actionEmailChannel
    }
  }

    let scheduler = {};
    if(this.selectedActionFrequency != undefined){
     scheduler = {
      granularityType: this.selectedActionFrequency.name,
      granularityTime:  moment(this.actionStartDate).format('HH:MM:SS'),
      granularityDay: this.selectedActionFrequency.name == 'weekly' ? moment(this.actionStartDate).format('DD') : '',
      granularityDate:  moment(this.actionStartDate).format('DD'),
      startDate:  moment(this.actionStartDate).format('YYYY-MM-DD'),
      endDate: moment(this.actionEndDate).format('YYYY-MM-DD')
   }
  }
  else{
   scheduler = {
     granularityType : "",//this.selectedFrequency.name,
     granularityTime : "",//moment(this.schedularStartDate[0]).format('HH:MM:SS') ,
     granularityDay : "",//this.selectedFrequency.name == 'weekly' ? moment(this.schedularStartDate[0]).format('DD') : '',
     granularityDate : "",//moment(this.schedularStartDate[0]).format('DD'),
     startDate: "",//moment(this.schedularStartDate).format('YYYY-MM-DD'),
     endDate: ""//moment(this.schedularEndDate).format('YYYY-MM-DD'), 
  }
 }

    let obj = {
    
        instance_type: "action",
        opcode:"create",
        event_id:this.event_id,//"360",
        //event_id:this.event_id,
        name: this.actionName,
        description: this.actionDesc,
        status: this.selectedActionSchecularStatus,
        scheduler: scheduler,
        channel: channel
        }
  
      console.log("obj",obj);
      

      this.dataFetchServ
      .getAlertTableData(obj)
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
  
        console.log("status",data);
        
        if(data['status_msg'] == 'success'){
  
          this.toastService.displayToast({
            severity: 'success',
            summary: 'Action',
            detail: 'Action created successfully'
          });


          // this.activeStepIdx = 0;
          // this.userFlow.splice(2,1)
          this.goBack();
        }
  
      });

    //this.activeStepIdx = 0

    
  }
  onBackSortingSchedular(){
    this.activeStepIdx = 1
  }
  onChangeDimCondition(){
    console.log("selected dim",this.selectedDimsMapping);
    
const req =
    {
      "instance_type": "event",
      "opcode": "getDimValues",
      "dim_name":this.selectedDimsMapping.name,
      "event_id": this.event_id
     // "event_id": this.event_id
  }

  this.dataFetchServ
  .getAlertTableData(req)
  .subscribe(data => {
    if (data['status'] === 0) {
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Server Error',
        detail: 'Please refresh the page'
      });
      console.log(data['status_msg']);
      return;
    }

    console.log("result : ",data);

    if(data['opcode_data']){


//    const data = {
//       "opcode_data": ["247press", "950", "actkb", "AGM", "ajax", "am950"],
//       "status": 1,
//       "status_msg": "success"
// }

    data['opcode_data'].forEach(element =>{

      const obj = {
        label : element,
        value : element
      }
      this.availableDimValues.push(obj);
    })

       this.availableDimValues = data['opcode_data']

   }
  });


  }


  onChangeAlertDate(period: string){
    this.alertDateRange = this.changeDate(period);
    this.dateRangeSelectionType = period;
  }

  onChangeChannel()
  {
    console.log("selected channel",this.selectedChannel);
    
    if(this.selectedChannel.filter(item => item['name'] === 'email').length > 0)
      this.isEmail = true
    else
      this.isEmail = false
  
    if(this.selectedChannel.filter(item => item['name'] === 'slack').length > 0)
      this.isSlack = true
    else
      this.isSlack = false

  }
  onChangeActionChannel(){

    if(this.actionSelectedChannel.filter(item => item['name'] === 'email').length > 0)
    this.isEmailAction = true
  else
    this.isEmailAction = false

  if(this.actionSelectedChannel.filter(item => item['name'] === 'slack').length > 0)
    this.isSlackAction = true
  else
    this.isSlackAction = false
  }

  selectedDimConditionMap(name){


    console.log("name",name);
    

    const req =
    {
      "instance_type": "event",
      "opcode": "getDimValues",
      "dim_name":name['name'],
      "event_id": this.event_id
     // "event_id": this.event_id

  }

  this.dataFetchServ
  .getAlertTableData(req)
  .subscribe(data => {
    if (data['status'] === 0) {
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Server Error',
        detail: 'Please refresh the page'
      });
      console.log(data['status_msg']);
      return;
    }

    console.log("result : ",data);

    if(data['opcode_data']){


  //  const data = {
  //     "opcode_data": ["247press", "950", "actkb", "AGM", "ajax", "am950"],
  //     "status": 1,
  //     "status_msg": "success"
//}

    data['opcode_data'].forEach(element =>{

      const obj = {
        label : element,
        value : element
      }
      this.conditionMappingFormArr[this.conditionMappingFormArr.length-1]['values'].push(obj) 
      //this.availableDimValues.push(obj);
    })

        this.availableDimValues = data['opcode_data']

   }
  });    
  }

  selectedConditionMap(name){
    this.conditionMappingFormArr[this.conditionMappingFormArr.length-1]['selectedCondition'] = name

  }


  onMetricMappingAdded(){

    // metric : this.availableMetric,
    // selectedMetric : {},
    // condition : this.metricMappingCondtions,
    // selectedCondition : {},
    // value : ''


    this.metricMappingFormArr.push({
      metric : this.availableTotalMetrics,
      selectedMetric : {},
      value : '',
      condition : this.metricMappingCondtions,
      selectedCondition : {},//this.metricMappingCondtions[1]. value,
      andOrConditions : this.availableAndOr,
      selectedAndOr : '',
      isBetweenValue : ''
    })

    console.log("metricMappingFormArr",this.metricMappingFormArr);
    
  }

  onSelectDate(){
    console.log("schedularStartDate",this.schedularStartDate);
    
  }
  onDimMappingAdded(){

      this.conditionMappingFormArr.push({
        dims : this.availableDimensions,
        condition : this.availableDimConditions,
        values : [],
        selectedDim : '',
        selectedCondition : this.availableDimConditions[1]. value,
        selectedValues : [],
        andOrConditions : this.availableAndOr,
        selectedAndOr : ''
      })

      console.log("conditionMappingFormArr",this.conditionMappingFormArr);
      


    //    console.log("this.selectedDimConditions",this.selectedDimConditions);
    

    // const obj = {
    //   dim : this.selectedDimsMapping.alies,
    //   condition : this.selectedDimConditions,
    //   values : this.selectedDimValues,
    //   andOr : this.selectedAndOr
    // }

    // this.dimMappingArray.push(obj);

    // console.log("dimMappingArray",this.dimMappingArray);
    
    // this.selectedDimsMapping = {alies : '', name:''};
    // this.selectedDimConditions = this.availableDimConditions[1]. value
    // this.selectedDimValues = [];
    
  }
  onTimeBucketAdded(item){
    console.log("item",item);
  
    const date = this.changeDate('last_7_days')
    this.timeBucketFormArray.push({
      selection_type : 'last_7_days',
      rangeDates : date,
      contribution : false
    })
  }

  onChangeMetric(){
    this.selectedMetricForAverage = []
  }

  onTimeBucketRemoved(index){
    this.timeBucketFormArray.splice(index,1)

  }

  resetAlertForm(){
    this.alertDetailsForm.reset();
    this.emailChannel= [];
    this.conditionMappingFormArr = [];
    this.metricMappingFormArr = [];
    this.timeBucketFormArray = [];
    this.selectedFrequency = {};
    this.schedularStartDate = new Date();
    this.schedularEndDate = new Date();
    this.selectedSortingMetric = []    
    this.selectedSorting = {}
    this.sortingNoRecords = ""
    this.derivedMetricArray = []
    this.alertDetailsForm.reset();
    this.selectedMetricForAverage = []
//    this.addDerived =false;
    this.addDerived = !this.addDerived

    this.conditionMappingFormArr.push({
      dims : this.availableDimensions,
      condition : this.availableDimConditions,
      values : [],
      selectedDim : '',
      selectedCondition : this.availableDimConditions[1]. value,
      selectedValues : []
    })

    this.metricMappingFormArr.push({
      metric : this.availableTotalMetrics,
      selectedMetric : {},
      condition : this.metricMappingCondtions,
      selectedCondition : {},
      value : '',
      isBetweenValue : ''
    })
    let firstDate = new Date()
    let lastDate = new Date();

    const today = new Date();

    firstDate.setDate(today.getDate() - 7);
    lastDate.setDate(today.getDate() - 1);
    this.timeBucketFormArray.push({
      selection_type : 'last_7_days',
      rangeDates : [firstDate,lastDate],
      contribution : false
    })

  
  }

  onSaveAlert(){

    //   if(this.alertName != "" || this.selectedSortingColumns.length > 0 || this.sortingNoRecords != ""){
    //     this.isValidAlert = false
    //   }
    //  else{
      //this.isValidAlert = true;
      // console.log('alertDetailsForm',this.alertDetailsForm);
      console.log("sev",this.selectedSeverities);
      console.log("metric",this.selectedMerticArray);
      console.log("dims",this.selectedDimensions);
      console.log("onDateSelected",this.dateRangeSelectionType); 
      
    //   let data_selection = {
    //     "start_date": moment(this.alertDateRange[0]).format('YYYY-MM-DD'),
    //      "time_granularity": "daily", 
    //       "end_date": moment(this.alertDateRange[1]).format('YYYY-MM-DD'), 
    //       "selection_type": this.dateRangeSelectionType
    //     }
      
  
    //     console.log("email channels",this.emailChannel);
        
    //     let channel = [];
    //   if(this.emailChannel.length > 0){  
    //   channel = [{
    //     source : "Email",
    //     receivers : this.emailChannel
    //   }]
    // }
    //   let dimensions = []
  
    //   if(this.conditionMappingFormArr.length > 0){
    //   this.conditionMappingFormArr.forEach((element,i) => {
    //     if(i < this.conditionMappingFormArr.length-1){
    //       console.log("i",i);
  
    //       let andOr = ''
    //     if(element['selectedAndOr'])
    //        andOr = element['selectedAndOr'].name
  
    //     let obj = {
    //       operator : andOr,
    //       name : element['selectedDim'].name,
    //       values : element['selectedValues'],
    //       condition : element['selectedCondition']
    //     }
    //     dimensions.push(obj)
    //   }
    //   });
    // }
  
    //   let metrics = [];
  
    //   console.log("metricMappingFormArr........",this.metricMappingFormArr);  
  
    //   if(this.metricMappingFormArr.length > 0){
    //   this.metricMappingFormArr.forEach((element,i) => {
    //   if(i < this.metricMappingFormArr.length-1){
    //     console.log("m i",element);
        
    //     let obj = {}
    //     let andOr = ''
    //     if(element['selectedAndOr'])
    //        andOr = element['selectedAndOr'].name
  
    //     if(element['selectedMetric'] != {} && element['selectedMetric']['isDerived']){
  
    //        obj = {
    //       operator: andOr,
    //       name: element['selectedMetric'].name,
    //       values: element['value'],
    //       condition: element['selectedCondition']['alies'],
    //       type: "Derived",
    //       formula: element['selectedMetric']['formula']
    //       }
    //     }   
    //    else{ 
    //      obj = {
    //       operator : andOr,
    //       name : element['selectedMetric'].name,
    //       values : element['value'],
    //       condition : element['selectedCondition']['alies']
    //     }
    //   }
  
    //   console.log("obj..",obj);
      
    //     metrics.push(obj)
    // }
    //   });
    // }
  
    // console.log(".....metrics...",metrics);
    
    //   let timeBucket = []
  
    //   if(this.timeBucketFormArray.length > 0){
    //   this.timeBucketFormArray.forEach((element,i) => {
    //     if(i< this.timeBucketFormArray.length-1){
    //       console.log("t i",i);
          
    //       let obj = {
    //       selection_type: element['selection_type'],
    //       start_date: element['rangeDates'][0],
    //       end_date:element['rangeDates'][1],
    //       time_granularity : "daily",
    //       contribution : element['contribution']
    //     }
  
    //     timeBucket.push(obj)
    //   }
    //   });
    // }
    // console.log("timeBucketCondition2",this.timeBucketCondition2);
    // console.log("selectedTimeBucketMetric",this.selectedTimeBucketMetric);
    
    //   let timeBucketCondition = []
    //   if(this.isTimeBucketCondition && this.selectedTimeBucketMetric != {})
    //    {
    //       timeBucketCondition.push({
    //       operator : '',
    //       name : this.selectedTimeBucketMetric['name'],
    //       values : this.timeBucketCondition1 + ","+this.timeBucketCondition2,
    //       condition : 'BETWEEN',
  
    //      })
  
    //    } 
  
    //    console.log("selected f",this.selectedFrequency);
       
    //    let scheduler = {
    //    granularityType : this.selectedFrequency.name,
    //    granularityTime : moment(this.schedularStartDate[0]).format('HH:MM:SS') ,
    //     granularityDay : this.selectedFrequency.name == 'weekly' ? moment(this.schedularStartDate[0]).format('DD') : '',
    //     granularityDate : moment(this.schedularStartDate[0]).format('DD'),
    //     "startDate": moment(this.schedularStartDate).format('YYYY-MM-DD'),
    //     "endDate": moment(this.schedularEndDate).format('YYYY-MM-DD'), 
    //   }
  
    //   console.log("this.sortingNoRecords",this.sortingNoRecords);
      
    //   let condition = {
    //     "sorting_by_columns": this.selectedSortingColumns.map(item => item['name']),
    //     "dimensions": dimensions,
    //     "metrics": metrics,
    //     "sorting_algo": this.selectedSorting,
    //     "limit": this.sortingNoRecords
    //   }
  
    //   let derivedMetrics = []
  
    //   console.log("derivedMetricArray",this.derivedMetricArray);
      
    //   if(this.derivedMetricArray.length > 0){
    //   this.derivedMetricArray.forEach(element => {
  
    //     let obj = {
    //     name : element['name'],
    //     formula: element['formula'],
    //     format: element['format'],
    //     expected: element['expected']
    //     }
  
    //     derivedMetrics.push(obj)
    //   });
    // }
  
    //   console.log("derivedMetrics",derivedMetrics);
      
    //   let metrics_for_avg = []
    //   if(this.selectedMetricForAverage.length > 0){
    //      metrics_for_avg = this.selectedMetricForAverage.map(item => item['name'])
    //   }
    //   let column_selection = {
    //     dimensions : this.selectedDimensions.map(item => item['name']),
    //     metrics : this.selectedMetric.map(item => item['name']),
    //     derive_metrics : derivedMetrics,
    //     metrics_for_avg : metrics_for_avg
    //   }

    let data_selection = {
      "start_date": moment(this.alertDateRange[0]).format('YYYY-MM-DD'),
       "time_granularity": "daily", 
        "end_date": moment(this.alertDateRange[1]).format('YYYY-MM-DD'), 
        "selection_type": this.dateRangeSelectionType.replace(/[/_/]/g, ' ')
      }
    

      console.log("email channels",this.emailChannel);
      
      let channel = [];
    if(this.emailChannel != []){  
    channel = [{
      source : "Email",
      receivers : this.emailChannel
    }]
  }
    let dimensions = []

    if(this.conditionMappingFormArr.length > 0){
    this.conditionMappingFormArr.forEach((element,i) => {
      if(i < this.conditionMappingFormArr.length-1){
        console.log("i",i);

        let andOr = ''
      if(element['selectedAndOr'])
         andOr = element['selectedAndOr'].name

      let obj = {
        operator : andOr,
        name : element['selectedDim'].name,
        values : element['selectedValues'],
        condition : element['selectedCondition']
      }
      dimensions.push(obj)
    }
    });
  }

    let metrics = [];

    console.log("metricMappingFormArr........",this.metricMappingFormArr);

//     "operator": "",
// "name": "net_revenue",
// "values": "100,1000",
// "condition": "BETWEEN",
// "type": "Direct",
// "formula": ""


    if(this.metricMappingFormArr.length > 0){
    this.metricMappingFormArr.forEach((element,i) => {
    if(i < this.metricMappingFormArr.length-1){
      console.log("m i",element);
      
      let obj = {}
      let andOr = ''
      if(element['selectedAndOr'])
         andOr = element['selectedAndOr'].name

      if(element['selectedMetric'] != {} && element['selectedMetric']['isDerived']){

         obj = {
        operator: andOr,
        name: element['selectedMetric'].name,
        values: element['value'],
        condition: element['selectedCondition']['alies'],
        type: "Derived",
        formula: element['selectedMetric']['formula']
        }
      }   
     else{ 
       obj = {
        operator : andOr,
        name : element['selectedMetric'].name,
        values : element['value'],
        condition : element['selectedCondition']['alies'],
        type: "Direct"
      }
    }

    console.log("obj..",obj);
    
      metrics.push(obj)
  }
    });
  }

  console.log(".....metrics...",metrics);
  
    let timeBucket = []

    console.log("timeBucketFormArray",this.timeBucketFormArray);
    

    if(this.timeBucketFormArray.length > 0){
    this.timeBucketFormArray.forEach((element,i) => {
      if(i< this.timeBucketFormArray.length-1){
        console.log("t i",i);
        
        let obj = {
        selection_type: element['selection_type'],
        start_date: moment(element['rangeDates'][0]).format('YYYY-MM-DD'),
        end_date:moment(element['rangeDates'][1]).format('YYYY-MM-DD'),
        time_granularity : "daily",
        contribution : element['contribution']
      }

      timeBucket.push(obj)
    }
    });
  }
  console.log("timeBucketCondition2",this.timeBucketCondition2);
  console.log("selectedTimeBucketMetric",this.selectedTimeBucketMetric);
  
    let timeBucketCondition = []
    if(this.isTimeBucketCondition && this.selectedTimeBucketMetric != {})
     {
        timeBucketCondition.push({
        operator : '',
        name : this.selectedTimeBucketMetric['name'],
        values : this.timeBucketCondition1 + ","+this.timeBucketCondition2,
        condition : 'BETWEEN',
        type : "Direct"  
       })

     } 

     console.log("selected f",this.selectedFrequency);
     let scheduler = {};
     if(this.selectedFrequency != undefined){
      scheduler = {
      granularityType : this.selectedFrequency.name,
      granularityTime : moment(this.schedularStartDate[0]).format('HH:MM:SS') ,
      granularityDay : this.selectedFrequency.name == 'weekly' ? moment(this.schedularStartDate[0]).format('DD') : '',
      granularityDate : moment(this.schedularStartDate[0]).format('DD'),
      "startDate": moment(this.schedularStartDate).format('YYYY-MM-DD'),
      "endDate": moment(this.schedularEndDate).format('YYYY-MM-DD'), 
    }
   }
   else{
    scheduler = {
      granularityType : "",//this.selectedFrequency.name,
      granularityTime : "",//moment(this.schedularStartDate[0]).format('HH:MM:SS') ,
      granularityDay : "",//this.selectedFrequency.name == 'weekly' ? moment(this.schedularStartDate[0]).format('DD') : '',
      granularityDate : "",//moment(this.schedularStartDate[0]).format('DD'),
      startDate: "",//moment(this.schedularStartDate).format('YYYY-MM-DD'),
      endDate: ""//moment(this.schedularEndDate).format('YYYY-MM-DD'), 
   }
  }
    console.log("this.sortingNoRecords",this.sortingNoRecords);
    let limit = "";
    if(this.sortingNoRecords != undefined)
      limit = this.sortingNoRecords;

    let condition = {
      "sorting_by_columns": this.selectedSortingColumns.map(item => item['name']),
      "dimensions": dimensions,
      "metrics": metrics,
      "sorting_algo": this.selectedSorting,
      "limit": limit
    }

    let derivedMetrics = []

    console.log("derivedMetricArray",this.derivedMetricArray);
    
    if(this.derivedMetricArray.length > 0){
    this.derivedMetricArray.forEach(element => {

      let obj = {
      name : element['name'],
      formula: element['formula'],
      format: element['format'],
      expected: element['expected']['name']
      }

      derivedMetrics.push(obj)
    });
  }

    console.log("derivedMetrics",derivedMetrics);
    
    let metrics_for_avg = []
    if(this.selectedMetricForAverage !=undefined && this.selectedMetricForAverage.length > 0){
       metrics_for_avg = this.selectedMetricForAverage.map(item => item['name'])
    }
      
     let metric = [];
     let dim = [];
    if(this.selectedMetric != undefined && this.selectedMetric.length > 0)
       metric = this.selectedMetric.map(item => item['name'])
       
    if(this.selectedDimensions != undefined && this.selectedDimensions.length > 0)
       dim = this.selectedDimensions.map(item => item['name'])

    let column_selection = {
      dimensions : dim,
      metrics : metric,
      derive_metrics : derivedMetrics,
      metrics_for_avg : metrics_for_avg
    }
  
    console.log("column_selection",column_selection);
    
      console.log("column_selection",column_selection);
      let obj = {};
        obj = {
        "instance_type": "rule",
        "opcode":"create",
        //"event_id":"360",
        event_id : this.event_id,
        severity:this.selectedSeverities,
        "rule_name": this.alertName,
        "description": '',//this.alertDetailsForm.value.description,
        "manual_query": "",
        "status": "Active",
        data_selection : data_selection,
        time_buckets : timeBucket,
        time_buckets_condition : timeBucketCondition,
        column_selection : column_selection,
        channel : channel,
        scheduler_status : 'Active',//this.selectedSchecularStatus,
        conditions : condition,
        scheduler : scheduler
        }
      
      console.log("obj",obj);
    
      this.dataFetchServ
      .getAlertTableData(obj)
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
  
        console.log("status",data);
        
        if(data['status_msg'] == 'success'){
      
  
          this.toastService.displayToast({
            severity: 'success',
            summary: 'Alert Update',
            detail: 'Alert updated successfully'
          });
  
  
          this.resetAlertForm();
          if(this.userFlow.length == 3)
            this.activeStepIdx = 2;
          else
            this.activeStepIdx = 0;
        }
  
      });
    //}
    }
  // onSaveAlert(){

  //   console.log('alertDetailsForm',this.alertDetailsForm);
  //   console.log("sev",this.selectedSeverities);
  //   console.log("metric",this.selectedMerticArray);
  //   console.log("dims",this.selectedDimensions);
    
  //   console.log("onDateSelected",this.dateRangeSelectionType);
    
    
  //   let data_selection = {
  //     "start_date": moment(this.alertDetailsForm.value.alertDateRange[0]).format('YYYY-MM-DD'),
  //      "time_granularity": "daily", 
  //       "end_date": moment(this.alertDetailsForm.value.alertDateRange[1]).format('YYYY-MM-DD'), 
  //       "selection_type": this.dateRangeSelectionType
  //     }
    

  //     console.log("email channels",this.emailChannel);
      

  //   let channel = {
  //     source : "Email",
  //     receivers : this.emailChannel
  //   }

  //   let dimensions = []

  //   this.conditionMappingFormArr.forEach((element,i) => {

      
  //     if(i < this.conditionMappingFormArr.length-1){
  //       console.log("i",i);

  //       let andOr = ''
  //     if(element['selectedAndOr'])
  //        andOr = element['selectedAndOr'].name

  //     let obj = {
  //       operator : andOr,
  //       name : element['selectedDim'].name,
  //       values : element['selectedValues'],
  //       condition : element['selectedCondition']
  //     }
  //     dimensions.push(obj)
  //   }
  //   });

  //   let metrics = [];

  //   console.log("metricMappingFormArr........",this.metricMappingFormArr);
    

  //   this.metricMappingFormArr.forEach((element,i) => {
  //   if(i < this.metricMappingFormArr.length-1){
  //     console.log("m i",i);
      
  //     let obj = {}
  //     let andOr = ''
  //     if(element['selectedAndOr'])
  //        andOr = element['selectedAndOr'].name

  //     if(element['selectedMetric'].isDerived){

  //        obj = {
  //       operator: andOr,
  //       name: element['selectedMetric'].name,
  //       values: element['value'],
  //       condition: element['selectedCondition'],
  //       type: "Derived",
  //       formula: element['selectedMetric']['formula']
  //       }
  //     }   
  //    else{ 
  //      obj = {
  //       operator : andOr,
  //       name : element['selectedMetric'].name,
  //       values : element['value'],
  //       condition : element['selectedCondition']['alies']
  //     }
  //   }
  //     metrics.push(obj)
  // }
  //   });

  //   let timeBucket = []

  //   this.timeBucketFormArray.forEach((element,i) => {
  //     if(i< this.timeBucketFormArray.length-1){
  //       console.log("t i",i);
        
  //       let obj = {
  //       selection_type: element['selection_type'],
  //       start_date: element['rangeDates'][0],
  //       end_date:element['rangeDates'][1],
  //       time_granularity : "daily",
  //       contribution : element['contribution']
  //     }

  //     timeBucket.push(obj)
  //   }
  //   });

  //   // let timeBucketCondition = []
  //   // if(this.isTimeBucketCondition)
  //   //  {
  //   //    timeBucketCondition.push({
  //   //     operator : '',
  //   //     name : this.selectedTimeBucketMetric,
  //   //     values : this.timeBucketCondition1 + ","+this.timeBucketCondition2,
  //   //     condition : this.selectedTimeBucketCondition,

  //   //    })

  //   //  } 

  //   let scheduler = {
  //     granularityType : this.selectedFrequency.name,
  //     granularityTime : moment(this.schedularStartDate[0]).format('HH:MM:SS'), 
  //     granularityDay : this.selectedFrequency.name == 'weekly' ? moment(this.schedularStartDate[0]).format('DD') : '',
  //     granularityDate : moment(this.schedularStartDate[0]).format('DD'),
  //     "startDate": moment(this.schedularStartDate).format('YYYY-MM-DD'),
  //     "endDate": moment(this.schedularEndDate).format('YYYY-MM-DD'), 
  //   }

  //   let condition = {
  //     "sorting_by_columns": this.selectedSortingMetric.map(item => item['name']),
  //     "dimensions": dimensions,
  //     "metrics": metrics,
  //     "sorting_algo": this.selectedSorting,
  //     "limit": this.sortingNoRecords

  //   }

  //   let derivedMetrics = []

  //   this.derivedMetricArray.forEach(element => {

  //     let obj = {
  //     name : element['name'],
  //     formula: element['formulaArr'].join(''),
  //     format: element['format'],
  //     expected: element['expected'].alies
  //     }

  //     derivedMetrics.push(obj)

  //   });

  //   let column_selection = {
  //     dimensions : this.alertDetailsForm.value.selectedDimensions.map(item => item['name']),
  //     metrics : this.alertDetailsForm.value.selectedMetric.map(item => item['name']),
  //     derive_metrics : derivedMetrics,
  //     metrics_for_avg : this.selectedMetricForAverage.map(item => item['name'])
  //   }


  //   console.log("column_selection",column_selection);
    
  //   let obj = {
  //       "instance_type": "rule",
  //       "opcode":"create",
  //       "event_id":"360", 
  //       //  event_id : this.event_id,
  //       severity:this.alertDetailsForm.value.severity,
  //       "rule_name": this.alertDetailsForm.value.alertName,
  //       "description": '',//this.alertDetailsForm.value.description,
  //       "manual_query": "",
  //       "status": "Active",
  //       data_selection : data_selection,
  //       time_buckets : timeBucket,
  //       time_buckets_condition : [],
  //       column_selection : column_selection,
  //       channel : channel,
  //       scheduler_status : 'Active',//this.selectedSchecularStatus,
  //       conditions : condition,
  //       scheduler : scheduler
  //   }

  //   console.log("obj",obj);
  
  //   this.dataFetchServ
  //   .getAlertTableData(obj)
  //   .subscribe(data => {
  //     if (data['status'] === 0) {
  //       this.toastService.displayToast({
  //         severity: 'error',
  //         summary: 'Server Error',
  //         detail: 'Please refresh the page'
  //       });
  //       console.log(data['status_msg']);
  //       return;
  //     }

  //     console.log("status",data);
      
  //     if(data['status_msg'] == 'success'){
    

  //       this.toastService.displayToast({
  //         severity: 'success',
  //         summary: 'Alert Created',
  //         detail: 'Alert created successfully'
  //       });


  //       this.resetAlertForm();
  //       if(this.userFlow.length == 3)
  //         this.activeStepIdx = 2;
  //       else
  //         this.activeStepIdx = 0;
  //     }

  //   });

  // }

  goBack() {
    this.router.navigate(['/alert-mgmt']);
  }

  onAlertSubmit(){
    this.activeStepIdx = 2
  }
  goBackAlert(){
    this.isprevAlertGroup = true;
    this.activeStepIdx = 0
  }

  filterCountryMultiple(event) {

    console.log('event',event);
    
}

  ngOnDestroy() {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }
}
