import { Component, OnInit } from '@angular/core';
import { FetchApiDataService } from '../fetch-api-data.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { DynamicDialogConfig,DynamicDialogRef,DialogService } from 'primeng/dynamicdialog';

import * as moment from 'moment';

@Component({
  selector: 'ym-alert-group-info',
  templateUrl: './alert-group-info.component.html',
  styleUrls: ['./alert-group-info.component.scss']
})
export class AlertGroupInfoComponent implements OnInit {

  alertData: any;
  alertDetails: object;
  actionDetails: object;
  schedulerAShowInfo = false;
  schedulerStartDate: any;
  schedulerEndDate: any;
  channelSize = 0;
  
  constructor(
    private dataFetchServ: FetchApiDataService,
    private toastService: ToastService,
    private config: DynamicDialogConfig,
    public ref: DynamicDialogRef
  ) {
    this.alertData = this.config.data;
  }

  ngOnInit() {
    this.loadTableData();
    this.loadActionDetails();
  }

  loadTableData() {
    const tableReq = {
      instance_type: "event",
      opcode: "get",
      event_id: this.alertData['event_id'],
      user_id: this.alertData['user_id'],
      client_code: "FR"
    };

    this.dataFetchServ.getAlertTableData(tableReq).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      } else {
        this.alertDetails = data['opcode_data'];
      }
    });

  }

  loadActionDetails() {
    const tableReq = {
      instance_type: "action",
      opcode: "get",
      event_id: this.alertData['event_id'],
      action_id: this.alertData['action_id'],
    };

    this.dataFetchServ.getAlertTableData(tableReq).subscribe(data => {
      this.actionDetails = data['opcode_data'];
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      } else {
        this.channelSize = this.actionDetails['channel'].length;
        if (this.actionDetails['scheduler']['granularityType'] == "" && this.actionDetails['status'] == 'Inactive') {
          this.schedulerAShowInfo = true;
        } else {
          this.schedulerAShowInfo = false;
          let datetimeStr = this.actionDetails['scheduler']['startDate'] + " " + this.actionDetails['scheduler']['granularityTime']; //suppose this is UK time
          let upDate = new Date(new Date().toLocaleString("en-US", { timeZone: "America/New_York" })); //London current time
          let currentDate = new Date(); //My current time
          let timeDifference = this.diff_minutes(currentDate, upDate);

          var date = new Date(datetimeStr);
          date.setMinutes(date.getMinutes() + timeDifference);

          this.schedulerStartDate = moment(date).format('MM/DD/YYYY hh:mm A Z');
          this.schedulerEndDate = moment(this.actionDetails['scheduler']['endDate']).format('MM/DD/YYYY');
        }

      }
    });
  }

  diff_minutes(dt2, dt1) {
    var diff = (dt2.getTime() - dt1.getTime()) / 1000;
    diff /= 60;
    return Math.abs(Math.round(diff));
  }

  CloseDialog() {
    this.ref.close(false);
  }
}
