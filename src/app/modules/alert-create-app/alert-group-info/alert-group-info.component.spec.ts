import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertGroupInfoComponent } from './alert-group-info.component';

describe('AlertGroupInfoComponent', () => {
  let component: AlertGroupInfoComponent;
  let fixture: ComponentFixture<AlertGroupInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertGroupInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertGroupInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
