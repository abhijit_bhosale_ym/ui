import { Routes } from '@angular/router';
import { AlertDashboardAppComponent } from './alert-dashboard-app.component';

export const AlertDashboardAppRoutesAppRoutes: Routes = [
  {
    path: '',
    component: AlertDashboardAppComponent
  }
];
