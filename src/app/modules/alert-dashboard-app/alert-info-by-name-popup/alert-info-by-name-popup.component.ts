import { Component, OnInit, OnDestroy } from '@angular/core';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
import * as moment from 'moment';
import { FetchApiDataService } from '../fetch-api-data.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'ym-alert-info-by-name-popup',
  templateUrl: './alert-info-by-name-popup.component.html',
  styleUrls: ['./alert-info-by-name-popup.component.scss']
})
export class AlertInfoByNamePopupComponent implements OnInit {
  private appConfigObs: Subscription;
  audit_id: String;
  isStageCsv = false;
  dimensionsnamelist = [];
  matricsnamelist = [];
  conditionslist = [];
  DimensionsList = [];
  MetricList = [];
  DerivedMetricsList = [];
  SortingColumnsList = [];
  TimeBuckets = [];
  ruleDetails = {event_name : '',
                app_name: '',
                rule_severity: '',
                rule_name: '',
                rule_description: '',
                condition_mappings: {sorting_algo: '', limit : ''}
                };
  dateRange;
  DimensionsConditionList;
  MetricConditionList;
  MetricsForAvg = [];

  ruleData: any;
  constructor(
    private dataFetchServ: FetchApiDataService,
    private toastService: ToastService,
    private config: DynamicDialogConfig,
  ) {
    // this.audit_id = this.config.data;
    this.ruleData = this.config.data;
  }

  ngOnInit() {
    this.loadTableData();
  }

  loadTableData() {
    const tableReq = {
      instance_type: 'audit',
      opcode: 'getAuditInfo',
      audit_id: this.ruleData['audit_id'],
      column_search: {},
      global_search: {
        keys : ['app_name', 'rule_name', 'event_name', 'event_description', 'event_status', 'data_selection', 'column_mappings', 'condition_mappings', 'rule_status',
               'rule_description', 'rule_channel', 'user_id', 'stage_name', 'action_name', 'action_description', 'action_status', 'action_scheduler',
                'action_channel', 'audit_event_happen_time', 'audit_rule_id', 'rule_severity', 'result_data', 'is_fired', 'audit_channel', 'audit_is_read',
                'created_at', 'updated_at', 'time_buckets'],
        value : ['']
              }
    };
    this.dataFetchServ.getAlertDetails(tableReq).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);

        return;
      } else {
        this.DimensionsList = [];
        this.MetricList = [];
        const sortcolData = [];
        this.MetricsForAvg = [];
        this.DerivedMetricsList = [];
        this.SortingColumnsList = [];
        if (data['status'] == '1') {
          this.ruleDetails = data['opcode_data'];
          if (this.ruleDetails['data_selection']['selection_type'] == 'custom range') {
            this.dateRange = moment(this.ruleDetails['data_selection']['start_date'].format('MM/DD/YYYY') + ' To ' + moment(this.ruleDetails['data_selection']['end_date'].format('MM/DD/YYYY')));
          } else {
            this.dateRange = (this.ruleDetails['data_selection']['selection_type'].toLowerCase()
              .split(' ')
              .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
              .join(' '));
          }
          if (this.ruleDetails['stage_name'] == 'Table') {
            this.bindruledata(this.ruleData['audit_id'], this.ruleDetails['stage_name']).then(success => {
              this.ruleDetails['column_mappings']['dimensions'].forEach((element) => {
                this.DimensionsList.push(element);
                  sortcolData.push(element);
              });
              this.ruleDetails['column_mappings']['metrics'].forEach((element) => {
                 this.MetricList.push(element);
                  sortcolData.push(element);
              });

              this.ruleDetails['column_mappings']['metrics_for_avg'].forEach((element) => {
                 this.MetricsForAvg.push(element);
                sortcolData.push(element);
              });
              this.ruleDetails['column_mappings']['derive_metrics'].forEach((element, i) => {
                let formulaString = '';
                sortcolData.push({ key: element.name, label: element.name });
                formulaString += element.name + '(';
                element.formula.split(' ').forEach((el, id) => {
                  const el1 = el.toLowerCase().replace(/sum\((.*?)\)/g, '$1');
                  if (this.matricsnamelist.find(o => o.key == el1)) {
                    formulaString += 'sum(' + this.matricsnamelist.find(o => o.key == el1).label + ') ';

                  } else if (el1 != '') {
                    formulaString += el1 + ' ';
                  }
                });
                formulaString += ')';
                this.DerivedMetricsList.push(formulaString);
              });
              this.DimensionsConditionList = '';
              this.ruleDetails.condition_mappings['dimensions'].forEach((element, i) => {
                this.DimensionsConditionList += element.operator + ' ' + element.name + ' ' + element.condition + ' \' ' + element.values.join(', ') + ' \' ';
              });

              this.MetricConditionList = '';
              this.ruleDetails.condition_mappings['metrics'].forEach((element, i) => {

                if (element.type == 'Derived') {
                  this.MetricConditionList += element.operator + ' ' + element.name + '( ';
                  let formulaString = '';
                  element.formula.split(' ').forEach((el, id) => {
                    const el1 = el.toLowerCase().replace(/sum\((.*?)\)/g, '$1');
                    if (this.matricsnamelist.some(o => o.key == el1)) {
                      formulaString += 'sum(' + this.matricsnamelist.find(o => o.key == el1).label + ') ';
                    } else if (el1 != '') {
                      formulaString += el1 + ' ';
                    }
                  });
                  this.MetricConditionList += formulaString + ') ' + this.conditionslist.find(o => o.key == element.condition).label;
                } else {
                  this.MetricConditionList += element.operator + ' ' + this.matricsnamelist.find(o => o.key == element.name).label + ' ' + this.conditionslist.find(o => o.key == element.condition).label;
                }
                if (element.condition == 'BETWEEN') {
                  this.MetricConditionList += ' ' + element.values.split(',')[0] + ' AND ' + element.values.split(',')[1] + ' ';
                } else {
                  this.MetricConditionList += element.values + ' ';
                }
              });
              this.ruleDetails['condition_mappings']['sorting_by_columns'].forEach(el => {
                // if (sortcolData.some(x => x.key == el)) {
                  this.SortingColumnsList.push(el);
               // }
              });

              this.ruleDetails['time_buckets'].forEach(el => {
                  this.TimeBuckets.push({contribution : el['contribution'], selection_type : el['selection_type']});
              });

            });

          } else {
            this.isStageCsv = true;
            this.matricsnamelist = [
              { key: 'Impressions', label: 'Impressions', value: 'Impressions' },
              { key: 'Clicks', label: 'Clicks', value: 'Clicks' },
              { key: 'Revenue', label: 'Revenue', value: 'Revenue' }
            ];

            this.ruleDetails['column_mappings']['metrics'].forEach((element) => {
              this.MetricList.push(this.matricsnamelist.find(o => o.key == element).label);
            });

          }
        }
      }
    });
  }

  bindruledata(event_id, stage_name) {
    const defq = new Promise((resolve, reject) => {
      if (stage_name == 'Table') {
        this.isStageCsv = false;
        const req1 = {
          instance_type: 'stageConditionMapping',
          opcode: 'get',
          stage_name: stage_name,
        };

        const req2 = {
          instance_type: 'event',
          opcode: 'get',
          event_id: this.ruleData['event_id'],
          user_id : 1,
          client_code : 'FR'
        };
        this.dataFetchServ.getAlertByEventId(req2).subscribe(success => {
          this.dimensionsnamelist = [];
          resolve(true);
        });
      } else {
        this.isStageCsv = true;
        reject(true);
      }
    });
    return defq;
  }
}
