import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertInfoByNamePopupComponent } from './alert-info-by-name-popup.component';

describe('AlertInfoByNamePopupComponent', () => {
  let component: AlertInfoByNamePopupComponent;
  let fixture: ComponentFixture<AlertInfoByNamePopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertInfoByNamePopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertInfoByNamePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
