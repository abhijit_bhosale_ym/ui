import { Component, OnInit, OnDestroy } from '@angular/core';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
import { TreeNode } from 'primeng/api';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { CommonLibService } from 'src/app/_services';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';
import { FetchApiDataService } from '../fetch-api-data.service';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { Subscription } from 'rxjs';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';

@Component({
  selector: 'ym-alert-result-popup',
  templateUrl: './alert-result-popup.component.html',
  styleUrls: ['./alert-result-popup.component.scss']
})
export class AlertResultPopupComponent implements OnInit {
  private appConfigObs: Subscription;
  audit_id: String;
  isStageCsv = false;
  dimensionsnamelist = [];
  matricsnamelist = [];
  conditionslist = [];
  ruleDetails = {};
  tableDimColDef: any[];
  aggTableJson: Object;
  aggTableData: TreeNode[];
  noTableData = false;
  actionDetails = [];
  resultDetails;
  isResult: String;
  showTable = true;
  tableData = [];
  event_id: any;
  sortingColName : any;

  constructor(
    private dataFetchServ: FetchApiDataService,
    private config: DynamicDialogConfig,
  ) {
    this.audit_id = this.config.data['audit_id'];
    this.event_id = this.config.data['event_id'];
  }


  ngOnInit() {

    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,
      footerColumns: this.tableDimColDef,
      columns: this.tableDimColDef,
      selectedColumns: this.tableDimColDef,
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };

    this.loadRuleResult();
  }

  loadRuleResult() {
    const req2 = {
      instance_type: 'audit',
      opcode: 'getAuditInfo',
      audit_id: this.audit_id,
      column_search: {},
      global_search: {
        keys : ['app_name', 'rule_name', 'event_name', 'event_description', 'event_status', 'data_selection', 'column_mappings', 'condition_mappings', 'rule_status',
               'rule_description', 'rule_channel', 'user_id', 'stage_name', 'action_name', 'action_description', 'action_status', 'action_scheduler',
                'action_channel', 'audit_event_happen_time', 'audit_rule_id', 'rule_severity', 'result_data', 'is_fired', 'audit_channel', 'audit_is_read',
                'created_at', 'updated_at'],
        value : ['']
              }
    };
    this.dataFetchServ.getAlertDetails(req2).subscribe(sucess => {
      if (sucess['status'] == '1') {

        this.ruleDetails = sucess['opcode_data'];
        this.actionDetails = sucess['opcode_data']['action_scheduler'];
        this.resultDetails = this.ruleDetails['result_data'];
        const contributionData = [];
        if (this.ruleDetails['stage_name'] == 'Table') {

          this.tableDimColDef = [];

          let resultDetailsData = [];
          let resultDetailsDataDesc = [];
          let isBoth = false;

          this.sortingColName = this.ruleDetails['condition_mappings']['sorting_by_columns'].join(' ');
          
          if(typeof this.resultDetails == 'string'){
          const temp1 = [];
          for (let i = 0; i < this.resultDetails.length; i++) {

          if (this.resultDetails[i] != '&' && this.resultDetails[i] != '#' && this.resultDetails[i] != 'x' && this.resultDetails[i] != ';') {
              if (this.resultDetails[i + 2] != '&' &&  this.resultDetails[i + 3] != '#' &&
                this.resultDetails[i + 1] != '&' &&  this.resultDetails[i + 2] != '#' &&
                this.resultDetails[i - 2] != '#' && this.resultDetails[i + 1] != '&' &&
                this.resultDetails[i - 3] != '#' && this.resultDetails[i - 4] != '&' &&
                this.resultDetails[i - 4] != '#' && this.resultDetails[i - 5] != '&' &&
                this.resultDetails[i - 5] != '#' && this.resultDetails[i - 6] != '&' ) {
                 temp1.push(this.resultDetails[i]);
              }
            }
          }

           this.resultDetails =  temp1.join('');

          if (JSON.parse(this.resultDetails).ASC) {
            resultDetailsData = JSON.parse(this.resultDetails).ASC; // this.resultDetails['ASC'];
            this.isResult = 'ASC';
          } else if (JSON.parse(this.resultDetails).DESC) {
            resultDetailsData = JSON.parse(this.resultDetails).DESC; // this.resultDetails['DESC'];
             resultDetailsDataDesc = JSON.parse(this.resultDetails).DESC; // this.resultDetails['DESC'];
            this.isResult = 'DESC';
          } else if (JSON.parse(this.resultDetails).BOTH  == 'BOTH') {
            this.isResult = 'BOTH';
            resultDetailsData = JSON.parse(this.resultDetails).BOTH;
            isBoth = true;
            resultDetailsDataDesc = JSON.parse(this.resultDetails).BOTH;
          }
        }
        else{                 
          if (this.resultDetails.ASC) {
            resultDetailsData = this.resultDetails.ASC; // this.resultDetails['ASC'];
            this.isResult = 'ASC';
          } else if (this.resultDetails.DESC) {
            resultDetailsData = this.resultDetails.DESC; // this.resultDetails['DESC'];
             resultDetailsDataDesc = this.resultDetails.DESC; // this.resultDetails['DESC'];
            this.isResult = 'DESC';
          } else if (this.resultDetails.BOTH  == 'BOTH') {
            this.isResult = 'BOTH';
            resultDetailsData = this.resultDetails.BOTH;
            isBoth = true;
            resultDetailsDataDesc = this.resultDetails.BOTH;
          }
        }

          this.bindruledata(this.event_id, this.ruleDetails['stage_name']).then((success) => {

            if (resultDetailsData != null) {
           const colDefOptions = resultDetailsData.map((o) => {
            return Object.keys(o);
        }).reduce((prev, curr) => {
            return prev.concat(curr);
        }).filter((col, i, array) => {
            return array.indexOf(col) === i;
        });

            colDefOptions.forEach((el, i) => {
              const obj = {
                field: '',
                displayName: '',
                width: 150,
               // format: 'string',
                exportConfig: {
                  format: '',
                  styleinfo: {
                    thead: 'default',
                    tdata: 'white'
                  }
                },
                formatConfig: [],
                options: {
                  editable: false,
                  colSearch: false,
                  colSort: true,
                  resizable: true,
                  movable: true
                },
              };
              if (this.dimensionsnamelist.some(o => o.label == el)) {
                obj['field'] = el;
                obj['displayName'] = el;
                obj['format'] = '';
                this.tableDimColDef.unshift(obj);
              } else if (this.matricsnamelist.some(o => o.label == el)) {

                obj['field'] = el;
                obj['displayName'] = el;
                obj['format'] = ''; // this.matricsnamelist.find(o => o.label == el).format;
                this.tableDimColDef.push(obj);
              } else if (this.ruleDetails['column_mappings']['derive_metrics'].some(o => o.name == el)) {
                obj['field'] = el;
                obj['displayName'] = el;
                obj['format'] = ''; // this.ruleDetails['column_mappings']['derive_metrics'].find(o => o.name == el).format;
                this.tableDimColDef.push(obj);
              } else {
                obj['field'] = el;
                obj['displayName'] = el;
                obj['format'] = ''; // 'percentage';
                contributionData.push(obj);
              }
            });
          }
            this.reloadAggTable();
            this.tableDimColDef = this.tableDimColDef.concat(contributionData);
            this.aggTableJson['selectedColumns'] = this.tableDimColDef;
            this.aggTableJson['totalRecords'] = resultDetailsData.length;
            this.tableData = resultDetailsData;
            if (isBoth) {
                 this.aggTableJson['totalRecords'] = resultDetailsDataDesc.length;
                this.tableData = resultDetailsDataDesc;
            }
            const arr = [];
            for (const r of this.tableData) {
              const obj = {};
              obj['data'] = r;
              arr.push(obj);
            }
            this.aggTableData = <TreeNode[]>arr;
            this.aggTableJson['totalRecords'] = this.tableData.length;
            this.aggTableJson['loading'] = false;
          });
        } else {

          this.tableDimColDef = [
            {
              field: 'source',
              displayName: 'Source',
              format: 'String',
              width: '100',
              exportConfig: {
                format: 'string',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: false
              },
            },
            {
              field: 'stage',
              displayName: 'Stage',
              format: 'String',
              width: '180',
              exportConfig: {
                format: 'String',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: true
              },
            },
            {
              field: 'file_type',
              displayName: 'File Type',
              width: '180',
              exportConfig: {
                format: 'String',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              format: 'String',
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: true
              },
            },
            {
              field: 'file_exits',
              displayName: 'File Exists',
              width: '180',
              exportConfig: {
                format: 'String',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              format: 'String',
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: true
              },
            },
            {
              field: 'date_format_matched',
              displayName: 'Date Format Matched',
              width: '180',
              exportConfig: {
                format: 'String',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              format: 'String',
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: true
              },
            },
            {
              field: 'file_name',
              displayName: 'File Name',
              width: '180',
              exportConfig: {
                format: 'String',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              format: 'String',
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: true
              },
            },
            {
              field: 'size',
              displayName: 'Size',
              width: '180',
              exportConfig: {
                format: 'String',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              format: 'String',
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: true
              },
            },
            {
              field: 'impressions',
              displayName: 'Impressions',
              width: '180',
              exportConfig: {
                format: 'String',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              format: 'number',
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: true
              },
            },
            {
              field: 'revenue',
              displayName: 'Revenue',
              width: '180',
              exportConfig: {
                format: 'currency',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              format: 'currency',
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: true
              },
            },
            {
              field: 'click',
              displayName: 'Click',
              width: '180',
              exportConfig: {
                format: 'String',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              format: 'number',
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: true
              },
            },
            {
              field: 'success',
              displayName: 'Success',
              width: '180',
              exportConfig: {
                format: 'String',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              format: 'String',
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: true
              },
            },
          ];
          delete this.resultDetails.is_cond_met;
          delete this.resultDetails.type;
          const data = [];

          for (const source in JSON.parse(this.resultDetails['opcode_data'])) {
            let issuccess = true;

            issuccess = 'success' in this.resultDetails['source'];
            for (const stage in this.resultDetails['source']) {
              if (stage != 'success') {
                for (const file_type in this.resultDetails['source']['stage']) {
                  const obj = {};
                  obj['source'] = source;
                  obj['stage'] = stage;
                  obj['file_type'] = file_type;
                  obj['success'] = issuccess;
                  for (const key in this.resultDetails['source']['stage']['file_type']) {
                    obj[key] = this.resultDetails['source']['stage']['file_type']['key'];
                  }
                  data.push(obj);
                }
              }
            }
          }

          setTimeout(() => {
            this.aggTableJson['totalRecords'] = data.length;
            this.tableData = data;
          }, 0);

          const arr = [];
            for (const r of this.tableData) {
              const obj = {};
              obj['data'] = r;
              arr.push(obj);
            }
          this.aggTableData = <TreeNode[]>arr;
          this.aggTableJson['totalRecords'] = this.tableData.length;
          this.aggTableJson['loading'] = false;
        }
      }

    });
  }

  bindruledata(event_id, stage_name) {
    const defq = new Promise((resolve, reject) => {
      const eventid = event_id;
      // this.popupOpened = true;
      if (stage_name == 'Table') {
        this.isStageCsv = false;
        const req1 = {
          instance_type: 'stageConditionMapping',
          opcode: 'get',
          stage_name: stage_name,
        };
        const req2 = {
          instance_type: 'event',
          opcode: 'getDimMetric',
          event_id: event_id,
        };

        this.dataFetchServ.getAlertByEventId(req2).subscribe(success => {
          this.dimensionsnamelist = [];
          success['opcode_data']['dimensions'].forEach(element => {
            this.dimensionsnamelist.push({ key: element.name, label: element.alies, value: element.alies });
          });
          this.matricsnamelist = [];
          success['opcode_data']['metrics'].forEach((element, index) => {
            this.matricsnamelist.push({ key: element.name, label: element.alies, value: element.alies, format: element.format });
          });
          this.dataFetchServ.getAlertStageCondition(req1).subscribe(success1 => {
            this.conditionslist = [];
            success1['opcode_data'].forEach(element => {
              this.conditionslist.push({ key: element.conditional_symbol, label: element.conditional_symbol, value: element.conditional_symbol });

            });
            resolve(true);
            // this.popupOpened = false;
          });
        });
      } else {
        this.isStageCsv = true;
        // this.popupOpened = false;
        reject(true);
      }
    });
    return defq;
  }

  reloadAggTable() {
    this.showTable = false;
    setTimeout(() => {
      this.showTable = true;
    }, 0);
  }

}
