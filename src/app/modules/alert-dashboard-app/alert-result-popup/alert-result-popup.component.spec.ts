import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertResultPopupComponent } from './alert-result-popup.component';

describe('AlertResultPopupComponent', () => {
  let component: AlertResultPopupComponent;
  let fixture: ComponentFixture<AlertResultPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlertResultPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertResultPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
