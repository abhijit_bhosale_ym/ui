import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FetchApiDataService {
  private BASE_URL: string = environment.baseUrl;

  constructor(private http: HttpClient) {}

  getAlertTableData(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/alert-dashboard/getData`;
    return this.http.post(url, params);
  }

  getLastUpdatedData(appId) {
    const url = `${this.BASE_URL}/frankly/v1/common/getLastUpdatedData/${appId}`;
    return this.http.get(url);
  }

  getAlertDetails(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/alert-dashboard/getAlertDetails`;
    return this.http.post(url, params);
  }

  getAlertStageCondition(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/alert-dashboard/getStageConditionMapping`;
    return this.http.post(url, params);
  }

  getAlertByEventId(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/alert-dashboard/getAlertByEventId`;
    return this.http.post(url, params);
  }
}
