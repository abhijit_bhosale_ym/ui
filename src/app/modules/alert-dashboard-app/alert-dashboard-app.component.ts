import { Component, OnInit, OnDestroy } from '@angular/core';
import { DynamicDialogConfig, DialogService } from 'primeng/dynamicdialog';
import { TreeNode } from 'primeng/api';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { CommonLibService } from 'src/app/_services';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';
import { FetchApiDataService } from './fetch-api-data.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { Subscription } from 'rxjs';
import { AlertInfoByNamePopupComponent } from '../alert-dashboard-app/alert-info-by-name-popup/alert-info-by-name-popup.component';
import { AlertResultPopupComponent } from '../alert-dashboard-app/alert-result-popup/alert-result-popup.component';
@Component({
  selector: 'ym-alert-dashboard-app',
  templateUrl: './alert-dashboard-app.component.html',
  styleUrls: ['./alert-dashboard-app.component.scss']
})
export class AlertDashboardAppComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;

  appConfig: object = {};
  lastUpdatedOn: Date;
  filtersApplied: object = {};
  tableDimColDef: any[];
  aggTableJson: Object;
  nextUpdated: Date;
  dataUpdatedThrough: Date;
  aggTableData: TreeNode[];
  noTableData = false;
  start_Date: String;
  end_Date: String;
  severity = '';
  audit_id: String;
  table_Data = [];
  timeout: any;
  globalSearchValue = '';
  displayAggTable = true;
  conditionNames: any[];
  searchInput: any;
  value: any;
  colSearch: any = {};

  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
  ) { }


  ngOnInit() {

    this.tableDimColDef = [
      {
        field: 'severity',
        displayName: 'Severity',
        format: '',
        width: '80',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
      },
      {
        field: 'event_name',
        displayName: 'Alert Group Name',
        format: '',
        width: '180',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'rule_name',
        displayName: 'Alert Name',
        width: '180',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'result',
        displayName: 'Alert Result',
        format: '',
        width: '70',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'event_happen_time',
        displayName: 'Generated Time',
        format: '',
        width: '170',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },

      },
      {
        field: 'delivered_Channel',
        displayName: 'Delivered Channel',
        width: '80',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },

    ];
    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,
      footerColumns: this.tableDimColDef,
      columns: this.tableDimColDef,
      selectedColumns: this.tableDimColDef,
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };

    this.conditionNames = [
      { label: 'GTE', value: '<<GTE' },
      { label: 'LTE', value: '<<LTE' }
    ];

    this.searchInput = '>';
    // this.fieldName='revenue';
    this.value = this.conditionNames[0].value;


    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        console.log('appconfig--', this.appConfig);

        this._titleService.setTitle(this.appConfig['displayName']);
        let startDate;
        if (
          this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
          'defaultDate'
          ][0]['startOf']
        ) {
          startDate = moment()
            .subtract(
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['value'],
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['period']
            )
            .startOf(
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['period']
            );
        } else {
          startDate = moment().subtract(
            this.appConfig['filter']['filterConfig']['filters'][
            'datePeriod'
            ][0]['defaultDate'][0]['value'],
            this.appConfig['filter']['filterConfig']['filters'][
            'datePeriod'
            ][0]['defaultDate'][0]['period']
          );
        }
        const endDate = moment().subtract(
          this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
          'defaultDate'
          ][1]['value'],
          this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
          'defaultDate'
          ][1]['period']
        );

        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: startDate.format('YYYY-MM-DD'),
            time_key2: endDate.format('YYYY-MM-DD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: [{ key: 'unified_pricing_rule' }]
        };
        this.start_Date = this.filtersApplied['timeKeyFilter'].time_key1;
        this.end_Date = this.filtersApplied['timeKeyFilter'].time_key2;
        this.initialLoading();
      }
    });
  }

  initialLoading() {
    this.dataFetchServ
      .getLastUpdatedData(this.appConfig['id'])
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        this.lastUpdatedOn = moment(data[0]['updated_at']).toDate();
        this.nextUpdated = moment(data[0]['next_run_at']).toDate();
        this.dataUpdatedThrough = moment(data[0]['source_updated_through'], 'YYYYMMDD').toDate();
      });
    this.loadTableData();
  }
  customSearch(value, fieldName, inputValue, tableColDef) {

    tableColDef[
      tableColDef.findIndex(x => x.field === fieldName)
    ].condition = this.conditionNames[
      this.conditionNames.findIndex(x => x.value === value)
    ].label;

    if (value != '') {
      this.onSearchChanged(tableColDef);
    }
  }

  onSearchChanged(tableColDef) {

    if (this.timeout) {
      clearTimeout(this.timeout);
    }

  this.timeout = setTimeout(() => {
    const obj = {};
    tableColDef.forEach(element => {
        if (element.value != '' && element.value != null) {

        if (element.field === 'delivered_Channel') {
            obj['delivery_channel'] = element.value;
        } else {
            obj[element.field] = element.value;
        }
        }
      });

      this.colSearch = obj;

      //   this.flatTableReq['filters']['dimensions'].forEach(element => {
    //     if (element.key.split('<<').length > 0) {
    //       this.flatTableReq['filters']['dimensions'].splice(
    //         this.flatTableReq['filters']['dimensions'].findIndex(
    //           x => x.key === element.key
    //         ),
    //         1
    //       );
    //     }
    //   });

      // this.flatTableReq['filters']['metrics'] = [];
      // tableColDef.forEach(element => {
      //   if (element.value != '' && element.value != null) {
      //     if (element.condition === 'Equal') {
      //       this.flatTableReq['filters'][element.columnType].push({
      //         key: element.field,
      //         values: [parseFloat(element.value)]
      //       });
      //     } else {
      //       this.flatTableReq['filters'][element.columnType].push({
      //         key: `${element.field}<<${element.condition}`,
      //         values:
      //           element.columnType === 'metrics'
      //             ? [parseFloat(element.value)]
      //             : [element.value.toString()]
      //       });
      //     }
      //   }

      this.loadTableData();

       }, 3000);

//      this.loadFlatTableData(this.flatTableReq);
    // }, 3000);
  }

  resetSearch(fieldName, tableColDef) {
    tableColDef.find(o => o.field === fieldName).value = '';
    this.onSearchChanged(tableColDef);
  }


  convertToLocalDate = (datetimeStr) => {
    const upDate = new Date(new Date().toLocaleString('en-US', { timeZone: 'America/New_York' })); // London current time
    const currentDate = new Date(); // My current time
    const timeDifference = this.diff_minutes(currentDate, upDate);
    const date = new Date(datetimeStr);
    date.setMinutes(date.getMinutes() + timeDifference);
    return moment(date).format('MM/DD/YYYY hh:mm A Z');
  }

  diff_minutes(dt2, dt1) {
    let diff = (dt2.getTime() - dt1.getTime()) / 1000;
    diff /= 60;
    return Math.abs(Math.round(diff));
  }

  loadTableData() {
    this.aggTableJson['loading'] = true;
    const tableReq = {
      instance_type: 'audit',
      opcode: 'getByUser',
      user_id: this.appConfig['user']['id'].toString(),
      start_date: this.start_Date,
      end_date: this.end_Date,
      severity: this.severity,
      client_code: 'FR',
      column_search: this.colSearch,
      global_search: {
        keys : ['app_name', 'rule_name', 'rule_status', 'rule_scheduler_frequency', 'event_name', 'audit_id', 'event_happen_time',
                'rule_id', 'severity', 'is_read', 'delivery_channel', 'result_data', 'is_fired', 'stage_name', 'client_code', 'created_at', 'updated_at'],
        value : [this.globalSearchValue]
              }
    };

    this.dataFetchServ.getAlertTableData(tableReq).subscribe(data => {
      this.aggTableJson['loading'] = false;
      if(data){
      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        this.aggTableJson['loading'] = true;
        return;
      } else {
        this.noTableData = false;
      }

      this.table_Data = data['opcode_data'];
      const arr = [];
      this.table_Data.forEach((row: object) => {
        row['event_happen_time'] = this.convertToLocalDate(row['event_happen_time']);
        row['delivery_channel'] = row['delivery_channel'].map(e => e['source']);
        let obj = {};
        obj = {
          data: row
        };
        arr.push(obj);
      });

      this.reloadAggTable();

      this.aggTableData = <TreeNode[]>arr;
      this.aggTableJson['totalRecords'] = data['opcode_data'].length;
      this.aggTableJson['loading'] = false;
    }
    });
  }

  onFiltersApplied(filterData: Object) {
    console.log('filterData----', filterData);
    this.start_Date = moment(filterData['date'][0], 'YYYYMMDD').format('YYYY-MM-DD');
    this.end_Date = moment(filterData['date'][1], 'YYYYMMDD').format('YYYY-MM-DD');

    if ((JSON.stringify(filterData['filter']['dimensions']) === '{}')) {
        this.severity = '';
    } else {
        this.severity = filterData['filter']['dimensions']['severity'].join(',');
    }

    this.loadTableData();
  }

  showAlertData(ruleData) {
    const ref = this.dialogService.open(AlertInfoByNamePopupComponent, {
      header: 'Alert : ' + ruleData['rule_name'],
      contentStyle: { 'max-height': '80vh', width: '60vw', overflow: 'auto' },
      data: ruleData,
    });
    ref.onClose.subscribe((data1: string) => { });
  }

  showResultData(ruleData) {
    const ref = this.dialogService.open(AlertResultPopupComponent, {
      header: 'Alert : ' + ruleData['rule_name'],
      contentStyle: { 'max-height': '80vh', width: '80vw', overflow: 'auto' },
      data: ruleData,
    });
    ref.onClose.subscribe((data1: string) => { });
  }

  isHiddenColumn(col: Object) {
    return (
      this.aggTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }
  onGlobalSearchChanged(searchValue, tableColDef) {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    this.timeout = setTimeout(() => {
      // this.flatTableReq['filters']['globalSearch'] = {
      //   dimensions: [],
      //   value: searchValue
      // };
      // tableColDef.forEach(element => {
      //   this.flatTableReq['filters']['globalSearch']['dimensions'].push(
      //     element.field
      //   );
      // });
      // this.loadFlatTableData(this.flatTableReq);
      this.globalSearchValue = searchValue;
      this.loadTableData();

    }, 3000);
  }

  reloadAggTable() {
    this.displayAggTable = false;
    setTimeout(() => {
      this.displayAggTable = true;
    }, 0);
  }
  ngOnDestroy() {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }

}
