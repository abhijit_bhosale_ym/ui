import { Routes } from '@angular/router';
import { UnfilledFreecycleComponent } from './unfilled-freecycle.component';

export const UnfilledFreeCycleAppRoutes: Routes = [
  {
    path: '',
    component: UnfilledFreecycleComponent
  }
];
