import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnfilledFreecycleComponent } from './unfilled-freecycle.component';

describe('UnfilledFreecycleComponent', () => {
  let component: UnfilledFreecycleComponent;
  let fixture: ComponentFixture<UnfilledFreecycleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnfilledFreecycleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnfilledFreecycleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
