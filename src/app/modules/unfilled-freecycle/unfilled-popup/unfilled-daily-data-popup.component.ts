import { Component, OnInit } from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { FetchUnfilledApiDataService } from '../fetch-unfilled-api-data.service';
import * as moment from 'moment';
import { TreeNode } from 'primeng/api';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';

@Component({
  selector: 'ym-unfilled-daily-data-popup',
  templateUrl: './unfilled-daily-data-popup.component.html',
  styleUrls: ['./unfilled-daily-data-popup.component.scss']
})
export class UnfilledPopupComponent implements OnInit {
  lineChartJson: object;
  showLineChart = false;
  filtersApplied: object;
  showMainLineChart = false;
  mainLineChartJson: object;
  tabChangedFlag = false;

  tableDataReq: object;
  tableData: TreeNode[];
  tableColumnDef: any[];
  dimColDef: any[];
  tableJson: Object;
  noDataMainLineChart = false;

  constructor(
    private libServ: CommonLibService,
    private formatNumPipe: FormatNumPipe,
    private dataFetchServ: FetchUnfilledApiDataService,
    private config: DynamicDialogConfig,
    private exportService: ExportdataService,
    private toastService: ToastService
  ) {
    this.filtersApplied = this.config.data['filters'];
  }

  ngOnInit() {
    this.loadLineChart();
  }

  loadLineChart() {
    let grpBys = this.filtersApplied['groupby'].map(e => e.key);
    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          this.filtersApplied['filters']['dimensions']
            .filter(f => f.values.length)
            .map(m => m.key)
            .concat(grpBys)
        )
      );
    }

    const lineChartReq = {
      dimensions: ['accounting_key'],
      metrics: ['unfilled_impressions'],
      derived_metrics: [],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.filtersApplied['filters'],
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: grpBys,
      orderBy: [{ key: 'accounting_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };
    const colors = this.libServ.dynamicColors(2);
    this.mainLineChartJson = {
      chartTypes: [{ key: 'line', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'Impressions',
            type: 'line',
            borderColor: colors[1],
            fill: false,
            backgroundColor: colors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Unfilled Impressions'
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Months'
              }
            }
          ],
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: 'Impressioms'
              },
              position: 'left',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, 'number', []);
                }
              }
            }
          ]
        },
        tooltips: {
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
              } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    this.showMainLineChart = false;
    this.dataFetchServ.getunfilledData(lineChartReq).subscribe(data => {
      const chartData = data['data'] as [];
      if (typeof chartData === 'undefined' || !chartData.length) {
        this.noDataMainLineChart = true;
      } else {
        const datesArr = Array.from(
          new Set(chartData.map(r => r['accounting_key']))
        );

        this.mainLineChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MMM-YYYY')
        );

        const revArr = [];
        datesArr.forEach(time_key => {
          chartData.forEach(r => {
            if (r['accounting_key'] === time_key) {
              revArr.push(r['unfilled_impressions']);
            }
          });
        });
        this.mainLineChartJson['chartData']['datasets'][0]['data'] = revArr;
        this.showMainLineChart = true;
        this.noDataMainLineChart = false;
      }
    });
  }

  tabChanged(e) {
    if (!this.tabChangedFlag) {
      this.loadLineChart();
      this.tabChangedFlag = true;
    }
    // console.log('tab changed', e);
  }

  chartSelected(e) {}
}
