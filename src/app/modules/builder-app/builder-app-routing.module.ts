import { Routes } from '@angular/router';
import { BuilderAppComponent } from './builder-app.component';

export const BuilderAppRoutes: Routes = [
  {
    path: '',
    component: BuilderAppComponent
  }
];
