import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpressionDetailsDailyDataPopupComponent } from './expression-details-data-popup.component';

describe('Rev360DailyDataPopupComponent', () => {
  let component: ExpressionDetailsDailyDataPopupComponent;
  let fixture: ComponentFixture<ExpressionDetailsDailyDataPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ExpressionDetailsDailyDataPopupComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpressionDetailsDailyDataPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
