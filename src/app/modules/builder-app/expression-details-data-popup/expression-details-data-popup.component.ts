import { Component, OnInit } from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { FetchApiDataService } from '../fetch-api-data.service';
import * as moment from 'moment';
import { TreeNode } from 'primeng/api';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { from } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'ym-expression-details-data-popup',
  templateUrl: './expression-details-data-popup.component.html',
  styleUrls: ['./expression-details-data-popup.component.scss']
})
export class ExpressionDetailsDailyDataPopupComponent implements OnInit {
  lineChartJson: object;
  showLineChart = false;
  filtersApplied: object;
  tableDataReq: object;
  tableData: TreeNode[];
  tableColumnDef: any[];
  dimColDef: any[];
  tableJson: Object;
  showMainLineChart = false;
  noDataMainLineChart = false;
  mainLineChartJson: object;
  tData: any;
  heading = '';
  exportRequest: ExportRequest = <ExportRequest>{};

  constructor(
    private libServ: CommonLibService,
    private formatNumPipe: FormatNumPipe,
    private dataFetchServ: FetchApiDataService,
    private config: DynamicDialogConfig,
    private exportService: ExportdataService,
    private toastService: ToastService
  ) {
    // this.filtersApplied = this.config.data['filters'];
    // this.heading = this.config.data['heading'];
    // this.exportRequest = this.config.data['exportRequest'];
  }

  ngOnInit() {
    this.dimColDef = [

      {
        field: 'name',
        displayName: 'Name',
        format: '',
        width: '200',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'expression',
        displayName: 'Builder Expression',
        format: '',
        width: '140',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: 0
      },
      {
        field: 'attributes',
        displayName: 'Attributes',
        format: '',
        width: '140',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: 0
      },
     {
      field: 'source',
      displayName: 'Sources',
      format: '',
      width: '140',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: true
      },
      footerTotal: 0
    },
    ];
    this.tableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '300px',
      totalRecords: 1000,

      columns: this.dimColDef,
      selectedColumns: this.dimColDef,
      // frozenCols: [],
      // frozenWidth: '0px',
      // columns: this.aggTableColumnDef.slice(5),
      // selectedColumns: this.aggTableColumnDef.slice(5),
      // frozenCols: [...this.aggTableColumnDef.slice(0, 5)],
      // frozenWidth:
      //   this.aggTableColumnDef
      //     .slice(0, 5)
      //     .reduce((tot, cur) => tot + parseInt(cur.width), 0) + 'px',

      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: false,
      overallSearch: false,
      columnSearch: false
    };
    this.loadTable();
    // this.loadMainLineChart();
  }

  loadTable() {
 //   this.tableJson['loading'] = true;
    // this.tableDataReq = {
    //   dimensions: ['time_key', 'unified_pricing_rule'],
    //   metrics: [
    //     'adx_ad_request',
    //     'adx_impressions',
    //     'total_estimated_revenue',
    //     'total_impressions',
    //     'adx_estimated_revenue',
    //     'adx_impressions',
    //     'adx_matched_request',
    //     'yield_group_estimated_revenue',
    //     'yield_group_impressions'
    //   ],
    //   derived_metrics: [
    //     'yield_group_ecpm',
    //     'adx_ecpm',
    //     'total_ecpm',
    //     'adx_fill_rate'
    //   ],
    //   timeKeyFilter: this.libServ.deepCopy(
    //     this.filtersApplied['timeKeyFilter']
    //   ),
    //   filters: this.libServ.deepCopy(this.filtersApplied['filters']),
    //   groupByTimeKey: {
    //     key: ['time_key', 'accounting_key'],
    //     interval: 'daily'
    //   },
    //   gidGroupBy: ['unified_pricing_rule'],
    //   orderBy: [{ key: 'time_key', opcode: 'desc' }],
    //   limit: '',
    //   offset: ''
    // };

    // this.dataFetchServ.getUPRData(this.tableDataReq).subscribe(data => {
    //   if (data['status'] === 0) {
    //     this.toastService.displayToast({
    //       severity: 'error',
    //       summary: 'Server Error',
    //       detail: 'Please refresh the page'
    //     });
    //     console.log(data['status_msg']);
    //     return;
    //   }

    const data = {
      'data': [
        {
          'name': 'Donor above 50k dollor',
          'expression': 'builder expression1',
          'attributes': 'Chrismas high value donor',
          'source': 'CRM'
        },
        {
          'name': 'Donor above 50k dollor',
          'expression': 'builder expression2',
          'attributes': 'Frequent site visitors',
          'source': 'Mail '
        }
      ],
      'totalItems': 2
    };


      this.tData = data['data'];
      const tableData = data['data'];

      console.log('data ', this.tData);


      const arr = [];
      tableData.forEach((row: object) => {
        const obj = {
          data: row
        };
        arr.push(obj);
//      });

console.log('arr', arr);


      this.tableData = <TreeNode[]>arr;
      this.tableJson['totalRecords'] = data['totalItems'];
      this.tableJson['loading'] = false;

  //    this.loadMainLineChart();
    });


  }

  exportTablePopup(fileFormat) {
    if (this.exportRequest['sendEmail'].length > 0) {
      this.toastService.displayToast({
        severity: 'info',
        summary: 'Export Report',
        detail:
          'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
        life: 10000
      });
    const columnDefs = this.libServ.deepCopy(this.dimColDef);
    const req = this.libServ.deepCopy(this.tableDataReq);
    req['limit'] = '';
    req['offset'] = '';
    req['isTable'] = false;
      const sheetDetailsarray = [];
      const sheetDetails = {};
      sheetDetails['columnDef'] = this.libServ.deepCopy(columnDefs);
      sheetDetails['data'] = [];
      sheetDetails['sheetName'] = 'Daily Breakdown';
      sheetDetails['isRequest'] = true;
      sheetDetails['request'] = {
        url: '/api/flask/upr/cassandradata',
        method: 'POST',
        param: req
      };
      sheetDetails['disclaimer'] = [
        {
          position: 'top',
          label:
            'Data persent in below table is not final and may varies over period of time',
          color: '#3A37CF'
        },
        {
          position: 'bottom',
          label: 'Thank You',
          color: '#EC6A15'
        }
      ];
      sheetDetails['totalFooter'] = {
        available: true,
        custom: true
      };
      sheetDetails['tableTitle'] = {
        available: true,
        label: this.heading + ' Daily Breakdown'
      };
      sheetDetails['image'] = [
        {
          available: true,
          path: environment.exportConfig.exportLogo,
          position: 'top'
        }
      ];
      sheetDetailsarray.push(sheetDetails);
      this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
      this.exportRequest['fileName'] = this.heading + ' Daily Breakdown ' +
        moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
      this.exportRequest['exportFormat'] = fileFormat;
      this.exportRequest['exportConfig'] = environment.exportConfig;
      this.dataFetchServ
        .getExportReportData(this.exportRequest)
        .subscribe(response => {
          console.log(response);
        });

  } else {
    this.toastService.displayToast({
      severity: 'error',
      summary: 'Export Report',
      detail:
        'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
      life: 10000
    });
  }
  }

  loadMainLineChart() {
    const deviceTypeLineChartReq = {
      dimensions: ['time_key'],
      metrics: ['gross_revenue', 'dp_impressions'],
      derived_metrics: ['gross_ecpm'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      // orderBy: [],
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };

    const colors = this.libServ.dynamicColors(2);
    this.mainLineChartJson = {
      chartTypes: [{ key: 'line', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'Revenue',
            type: 'line',
            yAxisID: 'y-axis-0',

            borderColor: colors[0],
            fill: false,
            backgroundColor: colors[0],
            data: []
          },
          {
            label: 'eCPM',
            type: 'line',
            yAxisID: 'y-axis-1',

            borderColor: colors[1],
            fill: false,
            backgroundColor: colors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Overall Revenue and eCPM Trend'
        },
        legend: {
          display: true,
          onClick: e => e.stopPropagation()
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: ''
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: 'Revenue ($)'
              },
              position: 'right',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', [0]);
                }
              }
              // scaleFontColor: "rgba(151,137,200,0.8)"
            },
            {
              id: 'y-axis-1',
              scaleLabel: {
                display: true,
                labelString: 'eCPM ($)'
              },
              position: 'left',
              name: '2',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', [0]);
                }
              }
              // scaleFontColor: "rgba(151,187,205,0.8)"
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
              } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    this.showMainLineChart = false;
      const chartData = this.tData;

      this.noDataMainLineChart = false;
      if (!chartData.length) {
        this.noDataMainLineChart = true;
        return;
      }
      const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));
      const datearr = datesArr.reverse();

      this.mainLineChartJson['chartData']['labels'] = datearr.map(d =>
        moment(d, 'YYYYMMDD').format('YYYY-MM-DD')
      );

      const revArr = [];
      const ecpmArr = [];
      datearr.forEach(time_key => {
        chartData.forEach(r => {
          if (r['time_key'] === time_key) {
            revArr.push(r['total_estimated_revenue']);
            ecpmArr.push(r['total_ecpm']);
          }
        });
      });

      this.mainLineChartJson['chartData']['datasets'][0]['data'] = revArr;
      this.mainLineChartJson['chartData']['datasets'][1]['data'] = ecpmArr;
      this.showMainLineChart = true;
  }

  isHiddenColumn(col: Object) {}

  tabChanged(event) {

  }
}
