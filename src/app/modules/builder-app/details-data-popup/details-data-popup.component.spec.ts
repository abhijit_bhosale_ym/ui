import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsDataPopupComponent } from './details-data-popup.component';

describe('DetailsDataPopupComponent', () => {
  let component: DetailsDataPopupComponent;
  let fixture: ComponentFixture<DetailsDataPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DetailsDataPopupComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsDataPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
