import { Component, OnInit } from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { FetchApiDataService } from '../fetch-api-data.service';
import * as moment from 'moment';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { DynamicDialogConfig } from 'primeng';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-platform-publisher-daily-data-popup',
  templateUrl: './platform-publisher-daily-data-popup.component.html',
  styleUrls: ['./platform-publisher-daily-data-popup.component.scss']
})
export class PlatformPublisherDailyDataPopupComponent implements OnInit {
  lineChartJson: object;
  showLineChart = false;
  filtersApplied: object;
  tabChangedFlag = false;
  tableDimColDef: any[];
  // tableMatColDef: object;
  tableDataReq: object;
  tableData: TreeNode[];
  tableColumnDef: any[];
  dimColDef: any[];
  tableJson: Object;
  isExportReport = false;
  exportRequest: ExportRequest = <ExportRequest>{};

  constructor(
    private libServ: CommonLibService,
    private formatNumPipe: FormatNumPipe,
    private dataFetchServ: FetchApiDataService,
    private config: DynamicDialogConfig,
    private exportService: ExportdataService,
    private toastService: ToastService,
    private confirmationService: ConfirmationService
  ) {
    this.filtersApplied = this.config.data['filters'];
    this.exportRequest = this.config.data['exportRequest'];
    // this.email = this.config.data['email'];
  }

  ngOnInit() {
    this.isExportReport = this.config.data['isExportReport'];
    this.tableDimColDef = [{
      field: 'derived_inventry_name',
      displayName: 'Inventory Type',
      format: '',
      width: '160',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: false,
        linkable: true
      },
      footerTotal: '-'
    },
    {
      field: 'station_group',
      displayName: 'Media Group',
      format: '',
      width: '230',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: false,
        linkable: true
      },
      footerTotal: '-'
    },

    {
      field: 'derived_station_rpt',
      displayName: 'Property',
      format: '',
      width: '190',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: false,
        linkable: true
      },
      footerTotal: '-'
    }, {
      field: 'derived_ad_type_name',
      displayName: 'Ad type',
      format: '',
      width: '230',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: false,
        linkable: true
      },
      footerTotal: '-'
    },
    {
      field: 'derived_device_type2_name',
      displayName: 'Device',
      format: '',
      width: '230',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: false,
        linkable: true
      },
      footerTotal: '-'
    },
    {
      field: 'creative_size_rpt_name',
      displayName: 'Ad Size',
      format: '',
      width: '230',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: false,
        linkable: true
      },
      footerTotal: '-'
    },
    {
      field: 'source',
      displayName: 'Advertiser',
      format: '',
      width: '230',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: false,
        linkable: true
      },
      footerTotal: '-'
    },
    {
      field: 'buy_type_rpt_name',
      displayName: 'Deal type',
      format: '',
      width: '230',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: false,
        linkable: true
      },
      footerTotal: '-'
    },
    {
      field: 'order_name',
      displayName: 'Campaign',
      format: '',
      width: '230',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: false,
        linkable: true
      },
      footerTotal: '-'
    }];
    this.tableColumnDef = [
      {
        field: 'gross_revenue',
        displayName: 'Gross Revenue',
        width: '155',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true,
          linkable: false
        },
        footerTotal: '0'
      },
      {
        field: 'station_revenue_share_distribution',
        displayName: 'Net Revenue',
        width: '155',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true,
          linkable: false
        },
        footerTotal: '0'
      },
      {
        field: 'opportunities',
        displayName: 'Opportunities',
        format: 'number',
        width: '170',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true,
          linkable: false
        },
        footerTotal: '0'
      },
      {
        field: 'dp_impressions',
        displayName: 'Billable Imps.',
        format: 'number',
        width: '170',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true,
          linkable: false
        },
        footerTotal: '0'
      },

      // {
      //   field: 'dfp_impressions',
      //   displayName: 'DFP Imps.',
      //   format: 'number',
      //   width: '170',
      //   exportConfig: {
      //     format: 'number',
      //     styleinfo: {
      //       thead: 'default',
      //       tdata: 'white'
      //     }
      //   },
      //   formatConfig: [],
      //   options: {
      //     editable: false,
      //     colSearch: false,
      //     colSort: true
      //     resizable: true,
      //     movable: true,
      // linkable: false
      //   },
      //   footerTotal: '-'
      // },

      {
        field: 'gross_ecpm',
        displayName: 'eCPM',
        format: '$',
        width: '135',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true,
          linkable: false
        },
        footerTotal: '0'
      },

      {
        field: 'fill_rate_platform',
        displayName: 'Fill rate(%)',
        format: 'percentage',
        width: '130',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true,
          linkable: false
        },
        footerTotal: '0'
      },


      {
        field: 'viewability',
        displayName: 'Viewability',
        format: 'percentage',
        width: '170',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true,
          linkable: false
        },
        footerTotal: '0'
      },
      {
        field: 'completion_rate_platform',
        displayName: 'Completion Rate',
        format: 'percentage',
        width: '170',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true,
          linkable: false
        },
        footerTotal: '0'
      },
    ];
    this.tableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '400px',
      totalRecords: 1000,

      columns: this.tableColumnDef,
      selectedColumns: this.tableColumnDef,
      // frozenCols: [],
      // frozenWidth: '0px',
      // columns: this.aggTableColumnDef.slice(5),
      // selectedColumns: this.aggTableColumnDef.slice(5),
      // frozenCols: [...this.dimColDef.slice(0, 1)],
      // frozenWidth:
      //   this.dimColDef
      //     .slice(0, 1)
      //     .reduce((tot, cur) => tot + parseInt(cur.width), 0) + 'px',

      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: false,
      overallSearch: false,
      columnSearch: false
    };

    this.loadTable();
  }

  loadTable() {
    this.tableJson['loading'] = true;

    // let grpBys = this.filtersApplied['groupby'].map(e => e.key);
    // if (
    //   this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
    //   this.filtersApplied['filters']['dimensions'].length === 0
    // ) {
    //   this.filtersApplied['filters']['dimensions'] = [];
    // } else {
    //   grpBys = Array.from(
    //     new Set(
    //       this.filtersApplied['filters']['dimensions']
    //         .filter(f => f.values.length)
    //         .map(m => m.key)
    //         .concat(grpBys)
    //     )
    //   );
    // }

    this.tableDimColDef.forEach(c => {
      if (c.field == this.config.data['fieldName']) {
        this.tableColumnDef.unshift(c);
      }
    });
    // this.tableColumnDef.unshift();
    this.tableColumnDef.unshift({
      field: 'time_key',
      displayName: 'Date',
      format: 'date',
      width: '100',
      exportConfig: {
        format: 'date',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: false
      }
    });
    // grpBys.push('time_key');

    this.tableDataReq = {
      dimensions: ['time_key', this.config.data['fieldName']],
      metrics: [
        'dp_impressions',
        'gross_revenue',
        'station_revenue_share_distribution',
        'opportunities',
        'measurable',
        'viewable',
        'video_starts',
        'video_completes'],
      derived_metrics: ['gross_ecpm', 'viewability', 'completion_rate_platform', 'fill_rate_platform'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys(),
      orderBy: [{ key: 'time_key', opcode: 'desc' }],
      limit: '',
      offset: ''
    };
    // For Dev purpose
    // this.tableDataReq['gidGroupBy'] = [
    //   'ad_size',
    //   'ad_type',
    //   'ad_units',
    //   'bidding_type',
    //   'device_category',
    //   'geography',
    //   'sales_channel',
    //   'sales_type',
    //   'site',
    //   'source'
    // ];

    this.dataFetchServ.getRevMgmtData(this.tableDataReq).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }
      const tableData = data['data'];

      const arr = [];
      tableData.forEach((row: object) => {
        const obj = {
          data: row
        };
        arr.push(obj);
      });

      this.tableData = <TreeNode[]>arr;
      this.tableJson['totalRecords'] = data['totalItems'];
      this.tableJson['loading'] = false;
    });
  }

  getGrpBys() {
    let grpBys = this.filtersApplied['groupby'].map(e => e.key);
    // let grpBys = [];
    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          grpBys.concat(
            this.filtersApplied['filters']['dimensions']
              .filter(f => f.values.length)
              .map(m => m.key)
          )
        )
      );
    }
    return grpBys;
  }


  isHiddenColumn(col: Object) { }

  loadLineChart() {
    // let grpBys = this.filtersApplied['groupby'].map(e => e.key);
    // if (
    //   this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
    //   this.filtersApplied['filters']['dimensions'].length === 0
    // ) {
    //   this.filtersApplied['filters']['dimensions'] = [];
    // } else {
    //   grpBys = Array.from(
    //     new Set(
    //       this.filtersApplied['filters']['dimensions']
    //         .filter(f => f.values.length)
    //         .map(m => m.key)
    //         .concat(grpBys)
    //     )
    //   );
    // }

    const lineChartReq = {
      dimensions: ['time_key'],
      metrics: ['dp_impressions', 'gross_revenue'],
      derived_metrics: ['gross_ecpm'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['station_group'],
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };
    // For Dev purpose
    // lineChartReq['gidGroupBy'] = [
    //   'ad_size',
    //   'ad_type',
    //   'ad_units',
    //   'bidding_type',
    //   'device_category',
    //   'geography',
    //   'sales_channel',
    //   'sales_type',
    //   'site',
    //   'source'
    // ];
    const colors = this.libServ.dynamicColors(2);
    this.lineChartJson = {
      chartTypes: [{ key: 'bar', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'eCPM',
            type: 'line',
            yAxisID: 'y-axis-0',

            borderColor: colors[0],
            fill: false,
            backgroundColor: colors[0],
            data: []
          },
          {
            label: 'Revenue',
            type: 'bar',
            yAxisID: 'y-axis-1',

            borderColor: colors[1],
            fill: false,
            backgroundColor: colors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: ' Gross Revenue vs Gross eCPM'
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: 'Gross eCPM ($)'
              },
              position: 'right',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', []);
                }
              }
              // scaleFontColor: "rgba(151,137,200,0.8)"
            },
            {
              id: 'y-axis-1',
              scaleLabel: {
                display: true,
                labelString: 'Gross Revenue ($)'
              },
              position: 'left',
              name: '2',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', [0]);
                }
              }
              // scaleFontColor: "rgba(151,187,205,0.8)"
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    this.showLineChart = false;
    this.dataFetchServ.getRevMgmtData(lineChartReq).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }
      const chartData = data['data'];

      const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));

      this.lineChartJson['chartData']['labels'] = datesArr.map(d =>
        moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
      );

      const revArr = [];
      const ecpmArr = [];
      datesArr.forEach(time_key => {
        chartData.forEach(r => {
          if (r['time_key'] === time_key) {
            revArr.push(r['gross_revenue']);
            ecpmArr.push(r['gross_ecpm']);
          }
        });
      });
      this.lineChartJson['chartData']['datasets'][0]['data'] = ecpmArr;
      this.lineChartJson['chartData']['datasets'][1]['data'] = revArr;
      this.showLineChart = true;
    });
  }

  exportTable(fileFormat) {
    if (this.tableData.length == 0) {
      this.confirmationService.confirm({
        message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
        header: 'Information',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
        },
        reject: () => {
        }
      });
    } else {
      if (this.exportRequest['sendEmail'].length > 0) {
        this.toastService.displayToast({
          severity: 'info',
          summary: 'Export Report',
          detail:
            'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
          life: 10000
        });

        const columnDefs = this.tableColumnDef;
        const req = this.libServ.deepCopy(this.tableDataReq);
        req['limit'] = '';
        req['offset'] = '';
        req['isTable'] = false;

        let sheetDetails = {};
        sheetDetails['columnDef'] = columnDefs;
        sheetDetails['data'] = [];
        sheetDetails['sheetName'] = this.config.data['fieldData'] + ' Daily Distribution';
        sheetDetails['isRequest'] = true;
        sheetDetails['request'] = {
          url: '/api/flask/performance/cassandradata',
          method: 'POST',
          param: req
        };
        sheetDetails['disclaimer'] = [
          {
            position: 'bottom',
            label: 'Note: Data present in the table may vary over a period of time.',
            color: '#000000'
          }
        ];
        sheetDetails['totalFooter'] = {
          available: true,
          custom: true
        };
        sheetDetails['tableTitle'] = {
          available: false,
          label: this.config.data['fieldData'] + ' Daily Distribution'
        };
        sheetDetails['image'] = [
          {
            available: true,
            path: environment.exportConfig.exportLogo,
            position: 'top'
          }
        ];
        let sheetDetailsarray = [];
        sheetDetailsarray.push(sheetDetails);
        this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
        this.exportRequest['fileName'] =
          this.config.data['fieldData'] + ' Daily Distribution' +
          moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
        this.exportRequest['exportFormat'] = fileFormat;
        this.exportRequest['exportConfig'] = environment.exportConfig;
        // console.log('exportreport', this.exportRequest);
        // return false;
        this.dataFetchServ
          .getExportReportData(this.exportRequest)
          .subscribe(response => {
            console.log(response);
          });

      } else {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Export Report',
          detail:
            'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
          life: 10000
        });
      }
    }
    return false;
  }

  tabChanged(e) {
    if (!this.tabChangedFlag) {
      this.loadLineChart();
      this.tabChangedFlag = true;
    }
  }

  chartSelected(e) { }
}
