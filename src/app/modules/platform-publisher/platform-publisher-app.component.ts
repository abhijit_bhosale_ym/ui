import { Component, OnInit, OnDestroy } from '@angular/core';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { CommonLibService } from 'src/app/_services';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';
import { FetchApiDataService } from './fetch-api-data.service';
import { PlatformPublisherDailyDataPopupComponent } from './platform-publisher-daily-data-popup/platform-publisher-daily-data-popup.component';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { Subscription } from 'rxjs';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { SendEmailComponent } from '../send-email/send-email.component';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { environment } from 'src/environments/environment';
import { DialogService } from 'primeng';
// import { PlatformPublisherExportPopupComponent } from './platform-publisher-export-popup/platform-publisher-export-popup.component'

@Component({
  selector: 'ym-platform-publisher-app',
  templateUrl: './platform-publisher-app.component.html',
  styleUrls: ['./platform-publisher-app.component.scss']
})
export class PlatformPublisherAppComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;
  timeZone = environment.timeZone;
  public reportType = 'isPlatform';
  appConfig: object = {};
  appConfig1: object = {};
  lastUpdatedOn: Date;
  collapsedCustom = false;
  cardsJson = [];
  showCards = true;
  chartColors: string[];
  selectedDates: Date[];

  noDataImpressionsLineChart = false;
  showImpressionsLineChart = false;
  ImpressionsLineChartJson: object;
  isExportReport = false;
  noDataRevenueLineChart = false;
  showRevenueLineChart = false;
  RevenueLineChartJson: object;

  noDataEcpmLineChart = false;
  showEcpmLineChart = false;
  EcpmLineChartJson: object;


  showMonthlyChart = false;
  MonthlyBarChartJson: object;
  MonthlyStackChartJson: object;
  noDataMonthlyChart = false;
  nextUpdated: Date;
  dataUpdatedThrough: Date;
  timeout: any;

  sourcesJson = {
    titantv: {
      Columns: ['titantv_page_views', 'titantv_revenue', 'titantv_rpm'],
      DisplayName: 'TitanTv'
    },
    antennaweb: {
      Columns: [
        'antennaweb_page_views',
        'antennaweb_revenue',
        'antennaweb_rpm'
      ],
      DisplayName: 'Antennaweb'
    }
  };

  aggTableData: TreeNode[];
  noTableData = false;
  tableDimColDef: any[];
  tableMatColDef: any[];
  finaltableDimColDef: any[];
  aggTableColumnDef: any[];
  aggTableJson: Object;
  displayAggTable = false;
  toggleView = true;
  filtersApplied: object = {};
  exportReportData: object = {};
  tableDataReq: object = {};
  exportRequest: ExportRequest = <ExportRequest>{};

  constructor(
    private appConfigService: AppConfigService,
    public libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private htmltoimage: HtmltoimageService,
    private exportService: ExportdataService,
    private pptExport: ExportPptService,
    private formatNumPipe: FormatNumPipe,
    private confirmationService: ConfirmationService
  ) { }

  ngOnInit() {

    this.tableDimColDef = [{
      field: 'derived_inventry_name',
      displayName: 'Inventory Type',
      format: '',
      width: '160',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: false,
        linkable: true
      },
      footerTotal: '-'
    },
    {
      field: 'station_group',
      displayName: 'Media Group',
      format: '',
      width: '230',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: false,
        linkable: true
      },
      footerTotal: '-'
    },

    {
      field: 'derived_station_rpt',
      displayName: 'Property',
      format: '',
      width: '190',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: false,
        linkable: true
      },
      footerTotal: '-'
    }, {
      field: 'derived_ad_type_name',
      displayName: 'Ad type',
      format: '',
      width: '230',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: false,
        linkable: true
      },
      footerTotal: '-'
    },
    {
      field: 'derived_device_type2_name',
      displayName: 'Device',
      format: '',
      width: '230',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: false,
        linkable: true
      },
      footerTotal: '-'
    },
    {
      field: 'creative_size_rpt_name',
      displayName: 'Ad Size',
      format: '',
      width: '230',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: false,
        linkable: true
      },
      footerTotal: '-'
    },
    {
      field: 'source',
      displayName: 'Advertiser',
      format: '',
      width: '230',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: false,
        linkable: true
      },
      footerTotal: '-'
    },
    {
      field: 'buy_type_rpt_name',
      displayName: 'Deal type',
      format: '',
      width: '230',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: false,
        linkable: true
      },
      footerTotal: '-'
    },
    {
      field: 'order_name',
      displayName: 'Campaign',
      format: '',
      width: '230',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: false,
        linkable: true
      },
      footerTotal: '-'
    }];
    this.tableMatColDef = [
      {
        field: 'gross_revenue',
        displayName: 'Gross Revenue',
        width: '155',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true,
          linkable: false
        },
        footerTotal: '0'
      },
      {
        field: 'station_revenue_share_distribution',
        displayName: 'Net Revenue',
        width: '155',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true,
          linkable: false
        },
        footerTotal: '0'
      },
      {
        field: 'opportunities',
        displayName: 'Opportunities',
        format: 'number',
        width: '170',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true,
          linkable: false
        },
        footerTotal: '0'
      },
      {
        field: 'dp_impressions',
        displayName: 'Billable Imps.',
        format: 'number',
        width: '170',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true,
          linkable: false
        },
        footerTotal: '0'
      },


      // {
      //   field: 'measurable',
      //   displayName: 'Measurable',
      //   format: 'number',
      //   width: '170',
      //   exportConfig: {
      //     format: 'number',
      //     styleinfo: {
      //       thead: 'default',
      //       tdata: 'white'
      //     }
      //   },
      //   formatConfig: [],
      //   options: {
      //     editable: false,
      //     colSearch: false,
      //     colSort: true,
      //     resizable: true,
      //     movable: true,
      //     linkable: false
      //   },
      //   footerTotal: '0'
      // },
      // {
      //   field: 'viewable',
      //   displayName: 'Viewable',
      //   format: 'number',
      //   width: '170',
      //   exportConfig: {
      //     format: 'number',
      //     styleinfo: {
      //       thead: 'default',
      //       tdata: 'white'
      //     }
      //   },
      //   formatConfig: [],
      //   options: {
      //     editable: false,
      //     colSearch: false,
      //     colSort: true,
      //     resizable: true,
      //     movable: true,
      //     linkable: false
      //   },
      //   footerTotal: '0'
      // },


      // {
      //   field: 'video_starts',
      //   displayName: 'Video Starts',
      //   format: 'number',
      //   width: '170',
      //   exportConfig: {
      //     format: 'number',
      //     styleinfo: {
      //       thead: 'default',
      //       tdata: 'white'
      //     }
      //   },
      //   formatConfig: [],
      //   options: {
      //     editable: false,
      //     colSearch: false,
      //     colSort: true,
      //     resizable: true,
      //     movable: true,
      //     linkable: false
      //   },
      //   footerTotal: '0'
      // },
      // {
      //   field: 'video_completes',
      //   displayName: 'Video Completes',
      //   format: 'number',
      //   width: '170',
      //   exportConfig: {
      //     format: 'number',
      //     styleinfo: {
      //       thead: 'default',
      //       tdata: 'white'
      //     }
      //   },
      //   formatConfig: [],
      //   options: {
      //     editable: false,
      //     colSearch: false,
      //     colSort: true,
      //     resizable: true,
      //     movable: true,
      //     linkable: false
      //   },
      //   footerTotal: '0'
      // },
      // {
      //   field: 'dfp_impressions',
      //   displayName: 'DFP Imps.',
      //   format: 'number',
      //   width: '170',
      //   exportConfig: {
      //     format: 'number',
      //     styleinfo: {
      //       thead: 'default',
      //       tdata: 'white'
      //     }
      //   },
      //   formatConfig: [],
      //   options: {
      //     editable: false,
      //     colSearch: false,
      //     colSort: true
      //     resizable: true,
      //     movable: true,
      // linkable: false
      //   },
      //   footerTotal: '-'
      // },

      {
        field: 'gross_ecpm',
        displayName: 'eCPM',
        format: '$',
        width: '135',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true,
          linkable: false
        },
        footerTotal: '0'
      },

      {
        field: 'fill_rate_platform',
        displayName: 'Fill rate(%)',
        format: 'percentage',
        width: '130',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true,
          linkable: false
        },
        footerTotal: '0'
      },


      {
        field: 'viewability',
        displayName: 'Viewability',
        format: 'percentage',
        width: '170',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true,
          linkable: false
        },
        footerTotal: '0'
      },
      {
        field: 'completion_rate_platform',
        displayName: 'Completion Rate',
        format: 'percentage',
        width: '170',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true,
          linkable: false
        },
        footerTotal: '0'
      },
    ];
    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,

      // columns: this.tableDimColDef,
      // selectedColumns: this.tableDimColDef,
      // frozenCols: [],
      // frozenWidth: '0px',

      // columns: this.tableDimColDef.slice(3),
      // selectedColumns: this.tableDimColDef.slice(3),
      // frozenCols: [...this.tableDimColDef.slice(0, 3)],
      // frozenWidth:
      //   this.tableDimColDef
      //     .slice(0, 3)
      //     .reduce((tot, cur) => tot + parseInt(cur.width), 0) + 'px',


      footerColumns: this.tableMatColDef,

      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        this.isExportReport = this.appConfig['permissions'].some(
          o => o.name === 'platform-export-report'
        );
        this._titleService.setTitle(this.appConfig['displayName']);
        let startDate;
        let endDate;
        const date_config = this.appConfig['filter']['filterConfig']['filters'][
          'datePeriod'
        ][0];
        if ((date_config['defaultDate'][0]['value'] === 0 && date_config['defaultDate'][0]['period'] === 'month') && (moment().format('DD') === '01' || moment().format('DD') === '02')) {
          startDate = moment().subtract(1, 'months').startOf('month');
          endDate = moment().subtract(1, 'months').endOf('month');
        } else {
          if (
            date_config[
            'defaultDate'
            ][0]['startOf']
          ) {
            startDate = moment()
              .subtract(
                date_config['defaultDate'][0]['value'],
                date_config['defaultDate'][0]['period']
              )
              .startOf(
                date_config['defaultDate'][0]['period']
              );
          } else {
            startDate = moment().subtract(
              date_config['defaultDate'][0]['value'],
              date_config['defaultDate'][0]['period']
            );
          }
          endDate = moment().subtract(
            date_config[
            'defaultDate'
            ][1]['value'],
            date_config[
            'defaultDate'
            ][1]['period']
          );
        }

        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: startDate.format('YYYYMMDD'),
            time_key2: endDate.format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: this.appConfig['filter']['filterConfig']['groupBy'].filter(
            v => v.selected
          )
        };
        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        this.appConfig1 = this.appConfig;
        this.initialLoading();
      }
    });
  }

  initialLoading() {
    this.chartColors = this.libServ.dynamicColors(5);
    this.dataFetchServ
      .getLastUpdatedData(this.appConfig['id'])
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        this.lastUpdatedOn = moment(data[0]['updated_at']).toDate();
        this.nextUpdated = moment(data[0]['next_run_at']).toDate();
        this.dataUpdatedThrough = moment(
          data[0]['source_updated_through'],
          'YYYYMMDD'
        ).toDate();
      });

    this.tableDataReq = {
      dimensions: [],
      metrics: [
        'dp_impressions',
        'gross_revenue',
        'station_revenue_share_distribution',
        'opportunities',
        'measurable',
        'viewable',
        'video_starts',
        'video_completes'],
      derived_metrics: ['gross_ecpm', 'viewability', 'completion_rate_platform', 'fill_rate_platform'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys(),
      orderBy: [
        // { key: 'accounting_key', opcode: 'asc' }

        // { key: 'derived_station_rpt', opcode: 'asc' }
      ],
      limit: '',
      offset: ''
    };

    this.loadTableData(this.tableDataReq);
    const loadGraph = {
      dimensions: ['time_key'],
      metrics: [
        'dp_impressions',
        'gross_revenue',
        'station_revenue_share_distribution',
        'opportunities',
        'measurable',
        'viewable',
        'video_starts',
        'video_completes'],
      derived_metrics: ['gross_ecpm', 'viewability', 'completion_rate_platform', 'fill_rate_platform'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: { key: ['time_key'], interval: 'daily' },
      gidGroupBy: ['source'],
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: '',
      isTable: false
    };

    this.loadMainLineChart(loadGraph);
  }

  resetPagination(tt1) {
    if (typeof tt1 !== 'undefined') {
      tt1.reset();
    }
  }

  loadTableData(param) {
    let grpBysFilter = [];
    this.finaltableDimColDef = [];

    // if (this.reportType == 'isPublisher') {
    //   grpBysFilter.push('time_key');
    //   //   if (!(this.tableDimColDef.some(o => o.field == 'time_key'))) {
    //   //     this.tableDimColDef.unshift({
    //   //       field: 'time_key',
    //   //       displayName: 'Date',
    //   //       format: '',
    //   //       width: '100',
    //   //       exportConfig: {
    //   //         format: 'string',
    //   //         styleinfo: {
    //   //           thead: 'default',
    //   //           tdata: 'white'
    //   //         }
    //   //       },
    //   //       formatConfig: [],
    //   //       options: {
    //   //         editable: false,
    //   //         colSearch: false,
    //   //         colSort: true,
    //   //         resizable: true,
    //   //         movable: false
    //   //       },
    //   //       footerTotal: '-'
    //   //     });
    //   //   }
    //   // } else {
    //   //   if (this.tableDimColDef.some(o => o.field == 'time_key')) {
    //   //     this.tableDimColDef.splice(0, 1);
    //   //   }
    // }
    // grpBysFilter.push('accounting_key');
    grpBysFilter = grpBysFilter.concat(this.filtersApplied['groupby'].map(e => e.key));

    grpBysFilter.forEach(ele => {
      this.finaltableDimColDef.push(this.tableDimColDef.find(x => x.field == ele));
    })
    this.tableDataReq['dimensions'] = grpBysFilter;

    // tableDimColDef = this.libServ.deepCopy(this.tableDimColDef);
    this.finaltableDimColDef = this.finaltableDimColDef.concat(this.tableMatColDef);
    this.aggTableJson['columns'] = this.finaltableDimColDef.slice(1);
    this.aggTableJson['selectedColumns'] = this.finaltableDimColDef.slice(1);
    this.aggTableJson['frozenCols'] = this.finaltableDimColDef.slice(0, 1)
    this.aggTableJson['frozenWidth'] = this.finaltableDimColDef
      .slice(0, 1)
      .reduce((tot, cur) => tot + parseInt(cur.width), 0) + 'px',
      // this.aggTableJson['footerColumns'] = this.finaltableDimColDef;
      this.reloadAggTable();

    this.aggTableJson['loading'] = true;


    const TotalFooterReq = {
      dimensions: [],
      metrics: [
        'dp_impressions',
        'gross_revenue',
        'station_revenue_share_distribution',
        'opportunities',
        'measurable',
        'viewable',
        'video_starts',
        'video_completes'],
      derived_metrics: ['gross_ecpm', 'viewability', 'completion_rate_platform', 'fill_rate_platform'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys(),
      orderBy: [],
      limit: '',
      offset: ''
    };
    this.dataFetchServ.getRevMgmtData(param).subscribe(data => {
      this.dataFetchServ.getRevMgmtData(TotalFooterReq).subscribe(dataTotal => {
        if (data['status'] === 0) {
          this.noTableData = true;
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        } else {
          this.noTableData = false;
        }

        const tableData = data['data'];
        const totalFooterData = dataTotal['data'];
        console.log('data...', data['data']);

        const arr = [];
        tableData.forEach((row: object) => {
          // const ak = row['accounting_key'];
          // delete row['accounting_key'];
          // row['accounting_key'] = moment(ak, 'YYYYMMDD').format('MMMM-YYYY');
          // if (this.reportType == 'isPublisher') {
          //   const tk = row['time_key'];
          //   delete row['time_key'];
          //   row['time_key'] = moment(tk, 'YYYYMMDD').format('MM-DD-YYYY');
          // }
          // row['derived_station_rpt'] = 'All';
          let obj = {};
          obj = {
            data: row
          };
          arr.push(obj);
        });
        if (totalFooterData[0] !== undefined) {
          for (const key in totalFooterData[0]) {
            if (this.tableMatColDef.some(o => o.field == key)) {

              this.tableMatColDef.find(x => x.field == key).footerTotal =
                totalFooterData[0][key];

            }
          }
        } else {
          this.aggTableJson['footerColumns'].forEach(c => {
            if (this.tableMatColDef.some(o => o.field == c.field)) {
              c['footerTotal'] = 0;
            }
          });
        }
        this.aggTableData = <TreeNode[]>arr;
        this.aggTableJson['totalRecords'] = tableData.length;
        this.aggTableJson['loading'] = false;
        // this.addReportTotal(tableData);
      });
    });
  }

  onGlobalSearchChanged(searchValue, tableColDef) {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    this.timeout = setTimeout(() => {
      this.tableDataReq['filters']['globalSearch'] = {
        dimensions: [],
        value: searchValue
      };
      tableColDef.forEach(element => {
        this.tableDataReq['filters']['globalSearch']['dimensions'].push(
          element.field
        );
      });
      this.loadTableData(this.tableDataReq);
    }, 3000);
  }


  onTableDrill(e: Event) {
    console.log('Drilled', e);
    if (!e['node']['childLoaded']) {
      this.aggTableJson['loading'] = true;
      const tableReq = {
        dimensions: ['station_group', 'derived_station_rpt'],
        metrics: [
          'dp_impressions',
          'gross_revenue',
          'station_revenue_share_distribution',
          'opportunities',
          'measurable',
          'viewable',
          'video_starts',
          'video_completes'],
        derived_metrics: ['gross_ecpm', 'viewability', 'completion_rate_platform', 'fill_rate_platform'],
        timeKeyFilter: this.libServ.deepCopy(
          this.filtersApplied['timeKeyFilter']
        ),
        filters: this.libServ.deepCopy(this.filtersApplied['filters']),
        groupByTimeKey: {
          key: [],
          interval: 'daily'
        },
        gidGroupBy: ['derived_station_rpt', 'station_group'], // this.getGrpBys(),
        orderBy: [],
        limit: '',
        offset: ''
      };

      // const timeKey1 = e['node']['data']['accounting_key'];
      // const timeKey2 = moment(timeKey1, 'YYYYMMDD')
      //   .endOf('month')
      //   .format('YYYYMMDD');

      // tableReq['timeKeyFilter'] = { time_key1: timeKey1, time_key2: timeKey2 };

      const grpBys = ['derived_station_rpt', 'station_group']; // this.filtersApplied['groupby'].map(e => e.key);
      for (const g of grpBys) {
        if (e['node']['data'][g] !== 'All') {
          // tableReq['dimensions'].push(g);
          if (!tableReq['gidGroupBy'].includes(g)) {
            tableReq['gidGroupBy'].push(g);
          }
          if (
            tableReq['filters']['dimensions'].findIndex(e => e.key === g) === -1
          ) {
            tableReq['filters']['dimensions'].push({
              key: g,
              values: [e['node']['data'][g]]
            });
          } else {
            tableReq['filters']['dimensions'].splice(
              tableReq['filters']['dimensions'].findIndex(e => e.key === g),
              1
            );
            tableReq['filters']['dimensions'].push({
              key: g,
              values: [e['node']['data'][g]]
            });
            if (
              tableReq['filters']['dimensions']
                .find(e => e.key === g)
              ['values'].findIndex(v => v === e['node']['data'][g]) === -1
            ) {
              tableReq['filters']['dimensions'].push(e['node']['data'][g]);
            }
          }
        }
        // else {
        //   tableReq['dimensions'].push(g);
        //   if (!tableReq['gidGroupBy'].includes(g)) {
        //     tableReq['gidGroupBy'].push(g);
        //   }
        //   break;
        // }
      }

      // tableReq['gidGroupBy'] = this.getGrpBys();
      // For Dev purpose
      // tableReq['gidGroupBy'] = [
      //   'ad_size',
      //   'ad_type',
      //   'ad_units',
      //   'bidding_type',
      //   'device_category',
      //   'geography',
      //   'sales_channel',
      //   'sales_type',
      //   'site',
      //   'source'
      // ];

      this.dataFetchServ.getRevMgmtData(tableReq).subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        const childData = data['data'];
        const arr = [];

        childData.forEach((row: object) => {
          this.aggTableJson['frozenCols'].forEach(r => {
            if (typeof row[r['field']] === 'undefined') {
              row[r['field']] = 'All';
            }
          });
          let obj = {};
          if (row[grpBys[grpBys.length - 1]] === 'All') {
            obj = {
              data: row,
              children: [{ data: {} }]
            };
          } else {
            obj = {
              data: row
            };
          }
          obj['data']['isExpanded'] = '1';
          arr.push(obj);
        });

        this.aggTableJson['loading'] = false;
        e['node']['children'] = <TreeNode[]>arr;
        this.aggTableData = [...this.aggTableData];
        e['node']['childLoaded'] = true;
      });
    }
  }

  reloadAggTable() {
    this.displayAggTable = false;
    setTimeout(() => {
      this.displayAggTable = true;
    }, 0);
  }

  loadCards(cardsReq) {
    this.showCards = false;
    this.dataFetchServ.getRevMgmtData(cardsReq).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }
      if (!this.libServ.isEmptyObj(data['data'])) {
        this.cardsJson.map(o => {
          o['value'] = data['data'][0][o['field']];
        });
      } else {
        this.cardsJson.map(o => {
          o['value'] = 0;
        });
      }
      this.showCards = true;
    });
  }

  onLazyLoadAggTable(e: Event) {
    console.log('Lazy Agg', e);
  }

  openPopup(row, field) {
    const grpBys = [];
    const filtersApplied = this.libServ.deepCopy(this.filtersApplied);
    const fieldIdx = filtersApplied['groupby'].findIndex(
      e => e['key'] === field
    );
    filtersApplied['groupby'].forEach((grp, i) => {
      if (i <= fieldIdx) {
        if (
          filtersApplied['filters']['dimensions'].findIndex(
            e => e.key === grp['key']
          ) !== -1
        ) {
          filtersApplied['filters']['dimensions'].splice(
            filtersApplied['filters']['dimensions'].findIndex(
              e => e.key === grp['key']
            ),
            1
          );
        }
        filtersApplied['filters']['dimensions'].push({
          key: grp['key'],
          values:
            grp['key'] == 'accounting_key'
              ? [moment(row[grp['key']]).format('YYYYMMDD')]
              : [row[grp['key']]]
        });
      }
    });

    filtersApplied['groupby'] = grpBys;
    const data = {
      filters: filtersApplied,
      fieldName: field,
      fieldData: row[field],
      exportRequest: this.exportRequest,
      isExportReport: this.isExportReport

    };
    const ref = this.dialogService.open(
      PlatformPublisherDailyDataPopupComponent,
      {
        header: row[field] + ' Daily Distribution',
        contentStyle: { width: '80vw', overflow: 'auto' },
        data: data
      }
    );
    ref.onClose.subscribe((data: string) => { });
  }



  isHiddenColumn(col: Object) {
    return (
      this.aggTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }


  /* --------------------- On Filter Apply Button Clicked --------------------- */

  onFiltersApplied(filterData: object) {
    console.log('filterData', filterData);
    this.filtersApplied['filters']['dimensions'] = [];

    // tslint:disable-next-line: forin
    for (const k in filterData['filter']['dimensions']) {
      if (filterData['filter']['dimensions'][k].length !== 0) {
        this.filtersApplied['filters']['dimensions'].push({
          key: k,
          values: filterData['filter']['dimensions'][k]
        });
      }
    }

    this.filtersApplied['groupby'] = filterData['groupby'];
    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];

    this.tableDataReq = {
      dimensions: [],
      metrics: [
        'dp_impressions',
        'gross_revenue',
        'station_revenue_share_distribution',
        'opportunities',
        'measurable',
        'viewable',
        'video_starts',
        'video_completes'],
      derived_metrics: ['gross_ecpm', 'viewability', 'completion_rate_platform', 'fill_rate_platform'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys(),
      orderBy: [
        // { key: 'accounting_key', opcode: 'asc' }

        // { key: 'derived_station_rpt', opcode: 'asc' }
      ],
      limit: '',
      offset: ''
    };
    this.loadTableData(this.tableDataReq);

    const grpBys = this.getGrpBys();

    const cardsReq = {
      dimensions: [],
      metrics: [
        'dp_impressions',
        'gross_revenue',
        'station_revenue_share_distribution',
        'opportunities',
        'measurable',
        'viewable',
        'video_starts',
        'video_completes'],
      derived_metrics: ['gross_ecpm', 'viewability', 'completion_rate_platform', 'fill_rate_platform'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: { key: [], interval: 'daily' },
      gidGroupBy: grpBys.length > 0 ? grpBys : ['source'],
      orderBy: [],
      limit: '',
      offset: '',
      isTable: false
    };
    this.loadCards(cardsReq);

    const loadGraph = {
      dimensions: ['time_key'],
      metrics: [
        'dp_impressions',
        'gross_revenue',
        'station_revenue_share_distribution',
        'opportunities',
        'measurable',
        'viewable',
        'video_starts',
        'video_completes'],
      derived_metrics: ['gross_ecpm', 'viewability', 'completion_rate_platform', 'fill_rate_platform'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: { key: ['time_key'], interval: 'daily' },
      gidGroupBy: grpBys.length > 0 ? grpBys : ['source'],
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: '',
      isTable: false
    };

    this.loadMainLineChart(loadGraph);
  }

  getGrpBys() {
    let grpBys = this.filtersApplied['groupby'].map(e => e.key);
    // let grpBys = [];
    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          grpBys.concat(
            this.filtersApplied['filters']['dimensions']
              .filter(f => f.values.length)
              .map(m => m.key)
          )
        )
      );
    }
    return grpBys;
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['name']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    } else if (type === 'ppt') {
      const chartElements = document.getElementsByTagName('ym-charts');
      const slides = [];
      [].forEach.call(chartElements, (el, index) => {
        const c = el.getElementsByTagName('canvas')[0];

        slides.push({
          title: `${this.appConfig['name']}`,
          base64data: this.libServ.canvasToImage('white', c),
          width: 8,
          height: 3
        });

        if (index === chartElements.length - 1) {
          const pptConfig = {
            filename: 'Charts',
            logo: {
              visible: true,
              path: '/assets/images/ym.png',
              width: 0.53125,
              height: 0.458333
            },
            slides: slides
          };
          this.pptExport.exportPPT(pptConfig);
        }
      });
    }
    return false;
  }

  loadMainLineChart(params) {
    const colors = this.libServ.dynamicColors(2);
    this.RevenueLineChartJson = {
      chartTypes: [{ key: 'bar', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'Net Revenue',
            type: 'line',
            yAxisID: 'y-axis-0',

            borderColor: colors[0],
            fill: false,
            backgroundColor: colors[0],
            data: []
          },
          {
            label: 'Gross Revenue',
            type: 'bar',
            yAxisID: 'y-axis-1',

            borderColor: colors[1],
            fill: false,
            backgroundColor: colors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Gross Revenue vs Net Revenue'
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: 'Net Revenue ($)'
              },
              position: 'right',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', [0]);
                }
              }
              // scaleFontColor: "rgba(151,137,200,0.8)"
            },
            {
              id: 'y-axis-1',
              scaleLabel: {
                display: true,
                labelString: 'Gross Revenue ($)'
              },
              position: 'left',
              name: '2',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', [0]);
                }
              }
              // scaleFontColor: "rgba(151,187,205,0.8)"
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    this.ImpressionsLineChartJson = {
      chartTypes: [{ key: 'bar', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'Fill Rate',
            type: 'line',
            yAxisID: 'y-axis-0',

            borderColor: colors[0],
            fill: false,
            backgroundColor: colors[0],
            data: []
          },
          {
            label: 'Impressions',
            type: 'bar',
            yAxisID: 'y-axis-1',

            borderColor: colors[1],
            fill: false,
            backgroundColor: colors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Impressions vs Fill Rate'
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: 'Fill Rate(%)'
              },
              position: 'right',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, 'percentage', [2]);
                }
              }
              // scaleFontColor: "rgba(151,137,200,0.8)"
            },
            {
              id: 'y-axis-1',
              scaleLabel: {
                display: true,
                labelString: 'Impressions'
              },
              position: 'left',
              name: '2',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, 'number', []);
                }
              }
              // scaleFontColor: "rgba(151,187,205,0.8)"
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              // console.log('tooltipItem.datasetIndex', tooltipItem)
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return tooltipItem.datasetIndex == 0 ? `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, 'percentage', [2])}` : `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    this.EcpmLineChartJson = {
      chartTypes: [{ key: 'bar', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'Viewability',
            type: 'line',
            yAxisID: 'y-axis-0',

            borderColor: colors[0],
            fill: false,
            backgroundColor: colors[0],
            data: []
          },
          {
            label: 'eCPM',
            type: 'bar',
            yAxisID: 'y-axis-1',

            borderColor: colors[1],
            fill: false,
            backgroundColor: colors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'eCPM vs Viewability'
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: 'Viewability'
              },
              position: 'right',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, 'percentage', [2]);
                }
              }
              // scaleFontColor: "rgba(151,137,200,0.8)"
            },
            {
              id: 'y-axis-1',
              scaleLabel: {
                display: true,
                labelString: 'eCPM ($)'
              },
              position: 'left',
              name: '2',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', []);
                }
              }
              // scaleFontColor: "rgba(151,187,205,0.8)"
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return tooltipItem.datasetIndex == 0 ? `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, 'percentage', [2])}` : `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    this.showImpressionsLineChart = false;
    this.showRevenueLineChart = false;
    this.showEcpmLineChart = false;
    this.dataFetchServ.getRevMgmtData(params).subscribe(data => {
      const chartData = data['data'] as [];
      if (typeof chartData === 'undefined' || !chartData.length) {
        this.noDataImpressionsLineChart = true;
        this.noDataRevenueLineChart = true;
        this.noDataEcpmLineChart = true;
      } else {
        const datesArr = Array.from(
          new Set(chartData.map(r => r['time_key']))
        );

        this.ImpressionsLineChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );

        this.RevenueLineChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );

        this.EcpmLineChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );

        const impArr = [];
        const revArr = [];
        const ecpmArr = [];
        const fillArr = [];
        const ViewArr = [];
        const netRevArr = [];
        datesArr.forEach(time_key => {
          chartData.forEach(r => {
            if (r['time_key'] === time_key) {
              impArr.push(r['dp_impressions']);
              revArr.push(r['gross_revenue']);
              ecpmArr.push(r['gross_ecpm']);
              fillArr.push(r['fill_rate_platform']);
              ViewArr.push(r['viewability']);
              netRevArr.push(r['station_revenue_share_distribution']);
            }
          });
        });


        this.ImpressionsLineChartJson['chartData']['datasets'][0]['data'] = fillArr;
        this.RevenueLineChartJson['chartData']['datasets'][0]['data'] = netRevArr;
        this.EcpmLineChartJson['chartData']['datasets'][0]['data'] = ViewArr;
        this.ImpressionsLineChartJson['chartData']['datasets'][1]['data'] = impArr;
        this.RevenueLineChartJson['chartData']['datasets'][1]['data'] = revArr;
        this.EcpmLineChartJson['chartData']['datasets'][1]['data'] = ecpmArr;



        this.showImpressionsLineChart = true;
        this.showRevenueLineChart = true;
        this.showEcpmLineChart = true;
        this.noDataImpressionsLineChart = false;
        this.noDataRevenueLineChart = false;
        this.noDataEcpmLineChart = false;
      }
    });
  }

  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }

  sendEmail() {
    const imageConfig = {
      imgName: `${this.appConfig['name']}.png`,
      type: 'image/png'
    };

    this.htmltoimage
      .canvasImage(document.getElementById('capture'), imageConfig)
      .then(canvas => {
        this.dialogService.open(SendEmailComponent, {
          header: 'Email Screenshot',
          contentStyle: {
            'max-height': '80vh',
            width: '30vw',
            overflow: 'auto'
          },
          data: canvas.toDataURL('image/png')
        });
      });
  }

  changeReport() {
    console.log('report', this.reportType);
    this.loadTableData(this.tableDataReq);
  }

  openExportPopup(row, field) {

    const grpBys = [];
    const filtersApplied = this.libServ.deepCopy(this.filtersApplied);
    const fieldIdx = filtersApplied['groupby'].findIndex(
      e => e['key'] === field
    );
    filtersApplied['groupby'].forEach((grp, i) => {
      if (i <= fieldIdx) {
        if (
          filtersApplied['filters']['dimensions'].findIndex(
            e => e.key === grp['key']
          ) !== -1
        ) {
          filtersApplied['filters']['dimensions'].splice(
            filtersApplied['filters']['dimensions'].findIndex(
              e => e.key === grp['key']
            ),
            1
          );
        }
        filtersApplied['filters']['dimensions'].push({
          key: grp['key'],
          values:
            grp['key'] == 'accounting_key'
              ? [moment(row[grp['key']]).format('YYYYMMDD')]
              : [row[grp['key']]]
        });
      }
    });

    filtersApplied['groupby'] = grpBys;
    const data = {
      filters: filtersApplied,
      field: row[field],
      appName: this.exportRequest['appName'],
      email: this.exportRequest['sendEmail']
    };
    // const ref = this.dialogService.open(PlatformPublisherExportPopupComponent, {
    //   header: row[field] + ' Daily Distribution',
    //   contentStyle: { width: '80vw', overflow: 'auto' },
    //   data: data
    // });
    // ref.onClose.subscribe((data: string) => { });

    return false;
  }

  exportTable(fileFormat) {
    if (this.aggTableData.length == 0) {
      this.confirmationService.confirm({
        message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
        header: 'Information',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
        },
        reject: () => {
        }
      });
    } else {
      if (this.exportRequest['sendEmail'].length > 0) {
        this.toastService.displayToast({
          severity: 'info',
          summary: 'Export Report',
          detail:
            'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
          life: 10000
        });

        const columnDefs = this.finaltableDimColDef;
        const req = this.libServ.deepCopy(this.tableDataReq);
        req['limit'] = '';
        req['offset'] = '';
        req['isTable'] = false;

        let sheetDetails = {};
        sheetDetails['columnDef'] = columnDefs;
        sheetDetails['data'] = [];
        sheetDetails['sheetName'] = 'Platform Performance Data';
        sheetDetails['isRequest'] = true;
        sheetDetails['request'] = {
          url: '/api/flask/performance/cassandradata',
          method: 'POST',
          param: req
        };
        sheetDetails['disclaimer'] = [
          {
            position: 'bottom',
            label: 'Note: Data present in the table may vary over a period of time.',
            color: '#000000'
          }
        ];
        sheetDetails['totalFooter'] = {
          available: true,
          custom: true
        };
        sheetDetails['tableTitle'] = {
          available: false,
          label: 'Platform Performance Data'
        };
        sheetDetails['image'] = [
          {
            available: true,
            path: environment.exportConfig.exportLogo,
            position: 'top'
          }
        ];
        let sheetDetailsarray = [];
        sheetDetailsarray.push(sheetDetails);
        this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
        this.exportRequest['fileName'] =
          'Platform Performance Data ' +
          moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
        this.exportRequest['exportFormat'] = fileFormat;
        this.exportRequest['exportConfig'] = environment.exportConfig;
        // console.log('exportreport', this.exportRequest);
        // return false;
        this.dataFetchServ
          .getExportReportData(this.exportRequest)
          .subscribe(response => {
            console.log(response);
          });

      } else {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Export Report',
          detail:
            'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
          life: 10000
        });
      }
    }
    return false;
  }
}
