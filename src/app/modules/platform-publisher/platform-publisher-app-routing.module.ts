import { Routes } from '@angular/router';
import { PlatformPublisherAppComponent } from './platform-publisher-app.component';

export const PlatformPublisherAppRoutes: Routes = [
  {
    path: '',
    component: PlatformPublisherAppComponent
  }
];
