import { Component, OnInit, OnDestroy } from '@angular/core';
import { TreeNode } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { CommonLibService } from 'src/app/_services';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';
import { FetchApiDataService } from './fetch-api-data.service';
import { DSPConnectionDailyDataPopupComponent } from './dsp-connection-daily-data-popup/dsp-connection-daily-data-popup.component';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { Subscription } from 'rxjs';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { SortEvent } from 'primeng/api';
import { LoginComponent } from 'src/app/components';
import { timeout } from 'q';

// import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
// @AutoUnsubscribe()
@Component({
  selector: 'ym-dsp-connection-app',
  templateUrl: './dsp-connection-app.component.html',
  styleUrls: ['./dsp-connection-app.component.scss']
})
export class DSPConnectionAppComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;

  appConfig: object = {};
  lastUpdatedOn: Date;

  cardsJson = [];
  showCards = true;
  mainLineChartJson: object;
  mainPieChartJson: object;
  metricChartJson: object;

  showMetricChart = false;
  showMainLineChart = false;
  noDataMainLineChart = false;
  showMainPieChart = false;
  noDataMetricChart = false;

  aggTableData: TreeNode[];
  noTableData = false;
  dimColDef: any[];
  metricColDef: any[];
  aggTableColumnDef: any[];
  aggTableJson: Object;
  displayAggTable = true;

  filtersApplied: object = {};
  currentSort: object;
  showMainPieChartSource = false;
  noDataMainPieChart = false;
  nextUpdated: Date;
  dataUpdatedThrough: Date;

  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private htmltoimage: HtmltoimageService,
    private exportService: ExportdataService,
    private pptExport: ExportPptService,
    private formatNumPipe: FormatNumPipe
  ) { }

  ngOnInit() {
    this.currentSort = {};
    this.cardsJson = [
      {
        field: 'total_cookie_pool',
        displayName: 'Total Cookie Pool',
        value: 0,
        imageClass: 'fas fa-running',
        format: 'number',
        config: [],
        color: '#84d683'
      },
      {
        field: 'audience_segment',
        displayName: 'Audience Segment',
        value: 0,
        imageClass: 'fas fa-users',
        format: 'number',
        config: [],
        color: '#3BB9FF'
      },

      {
        field: 'total_dsps',
        displayName: 'Total DSPs',
        value: 0,
        imageClass: 'fas fa-eye',
        format: 'number',
        config: [],
        color: '#F87431'
      },
      {
        field: 'active_dsps',
        displayName: 'Active DSPs',
        value: 0,
        imageClass: ' fas fa-handshake',
        format: 'number',
        config: [],
        color: '#bd6d88'
      },

      {
        field: 'active_cookie_pool',
        displayName: 'Active Cookie Pool',
        value: 0,
        imageClass: 'fas fa-cookie-bite',
        format: 'number',
        config: [],
        color: '#b8a9c9'
      },
      {
        field: 'spend',
        displayName: 'Spend',
        value: 0,
        imageClass: 'fas fa-dollar-sign',
        format: '$',
        config: [],
        color: '#b0c30c'
      }
    ];
    this.dimColDef = [
      {
        field: 'audience_name',
        displayName: 'Audience Name',
        format: '',
        width: '270',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true
        },
        footerTotal: '-'
      },
      {
        field: 'dsp_name',
        displayName: 'DSP',
        format: '',
        width: '200',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true
        },
        footerTotal: '-'
      },
      {
        field: 'campaign_name',
        displayName: 'Campaigns',
        format: '',
        width: '170',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: false
        },
        footerTotal: '0'
      }
    ];

    this.metricColDef = [
      {
        field: 'spend',
        displayName: 'Spend',
        format: '$',
        width: '80',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: false
        },
        footerTotal: '0'
      },

      // {
      //   field: 'audience_id',
      //   displayName: 'Audience Id',
      //   format: '',
      //   width: '130',
      //   exportConfig: {
      //     format: 'string',
      //     styleinfo: {
      //       thead: 'default',
      //       tdata: 'white'
      //     }
      //   },
      //   formatConfig: [],
      //   options: {
      //     editable: false,
      //     colSearch: true,
      //     colSort: true,
      //     resizable: false
      //   },
      //   footerTotal: '0'
      // },

      {
        field: 'audience_status',
        displayName: 'Status',
        width: '60',
        format: '',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: false
        },
        footerTotal: '0'
      }
    ];

    // this.aggTableColumnDef.push(this.dimColDef);
    // this.aggTableColumnDef.push(this.metricColDef);

    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'expand',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,

      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: false,
      overallSearch: false,
      columnSearch: false
    };

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        this._titleService.setTitle(this.appConfig['displayName']);
        let startDate;
        if (
          this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
          'defaultDate'
          ][0]['startOf']
        ) {
          startDate = moment()
            .subtract(
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['value'],
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['period']
            )
            .startOf(
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['period']
            );
        } else {
          startDate = moment().subtract(
            this.appConfig['filter']['filterConfig']['filters'][
            'datePeriod'
            ][0]['defaultDate'][0]['value'],
            this.appConfig['filter']['filterConfig']['filters'][
            'datePeriod'
            ][0]['defaultDate'][0]['period']
          );
        }
        const endDate = moment().subtract(
          this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
          'defaultDate'
          ][1]['value'],
          this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
          'defaultDate'
          ][1]['period']
        );

        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: startDate.format('YYYYMMDD'),
            time_key2: endDate.format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: this.appConfig['filter']['filterConfig']['groupBy'].filter(
            v => v.selected
          )
        };
        //  this.exportRequest['appName'] = this.appConfig['name'].toString();
        // this.exportRequest['sendEmail'] = this.appConfig['user'][
        // 'contactEmail'
        // ].split(',');
        this.initialLoading();
      }
    });
  }

  initialLoading() {
    this.dataFetchServ
      .getLastUpdatedData(this.appConfig['id'])
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        this.lastUpdatedOn = moment(data[0]['updated_at']).toDate();
        this.nextUpdated = moment(data[0]['next_run_at']).toDate();
        this.dataUpdatedThrough = moment(
          data[0]['source_updated_through']
        ).toDate();
      });

    this.loadTableData();
    const cardsReq = {
      dimensions: [],
      metrics: ['spend'],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: { dimensions: [], metrics: [] },
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: [],
      orderBy: [],
      limit: '',
      offset: ''
    };
    this.loadCards(cardsReq);

    const mainLineChartReq = {
      dimensions: ['time_key'],
      metrics: ['spend'],
      derived_metrics: [],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: {
        dimensions: [],
        metrics: []
      },
      groupByTimeKey: {
        key: ['time_key'],
        interval: 'daily'
      },
      gidGroupBy: [],
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '', //   Max value of Int MySql
      offset: ''
    };
    this.loadMainLineChart(mainLineChartReq);

    const mainPieChartReq = {
      dimensions: ['dsp_name'],
      metrics: ['spend'],
      derived_metrics: [],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys(),
      orderBy: [{ key: 'spend', opcode: 'desc' }],
      limit: '',
      offset: ''
    };
    this.loadMainPieChart(mainPieChartReq);
  }

  resetPagination(tt1) {
    if (typeof tt1 !== 'undefined') {
      tt1.reset();
    }
  }

  reset(table) {
    table.reset();
    this.loadTableData();
  }

  loadPieChart() {
    const mainPieChartReqSource = {
      dimensions: ['source'],
      metrics: ['dp_revenue'],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys(), //  ['source'],
      //   orderBy: [],
      orderBy: [{ key: 'dp_revenue', opcode: 'desc' }],
      limit: '',
      offset: ''
    };

    this.mainPieChartJson = {
      chartTypes: [{ key: 'pie', label: 'Pie Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Revenue Distribution Across Sources'
        },
        legend: {
          display: false
        },
        elements: {
          arc: {
            borderWidth: 0
          }
        },
        plugins: {
          datalabels: {
            formatter: (value, ctx) => {
              let sum = 0;
              const dataArr = ctx.chart.data.datasets[0].data;
              dataArr.map(data => {
                sum += data;
              });
              const percentage = (value * 100) / sum;
              //   if (percentage > 10) {
              //     return `${this.formatNumPipe.transform(
              //       value,
              //       '$',
              //       []
              //     )} - ${percentage.toFixed(2)} %`;
              //   } else {
              //     return '';
              //   }
              if (percentage > 10) {
                return `${percentage.toFixed(2)} %`;
              } else {
                return '';
              }
            },
            color: 'black'
          }
        }
      },
      zoomLabel: false,
      chartWidth: '100%',
      chartHeight: '300px'
    };
    this.mainPieChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          let sum = 0;
          const dataArr = data.datasets[0].data;
          dataArr.map(data1 => {
            sum += data1;
          });
          const percentage = (currentValue * 100) / sum;
          return `${
            data.labels[tooltipItem.index]
            } : ${this.formatNumPipe.transform(
              currentValue,
              '$',
              []
            )} - ${percentage.toFixed(2)} %`;
        }
      }
    };
    this.showMainPieChartSource = false;
    if (
      mainPieChartReqSource['gidGroupBy'].filter(
        item => item === 'accounting_key'
      ).length >= 1
    ) {
      mainPieChartReqSource['gidGroupBy'].splice(
        mainPieChartReqSource['gidGroupBy'].findIndex(
          item => item === 'accounting_key'
        ),
        1
      );
    }

    if (
      mainPieChartReqSource['gidGroupBy'].length === 0 ||
      mainPieChartReqSource['gidGroupBy'].findIndex(item => item === 'source') <
      0
    ) {
      mainPieChartReqSource['gidGroupBy'].push('source');
    }

    this.dataFetchServ.getRevMgmtData(mainPieChartReqSource).subscribe(data => {
      const chartData = data['data'];

      this.noDataMainPieChart = false;
      if (!chartData.length) {
        this.noDataMainPieChart = true;
        return;
      }

      const sources = Array.from(new Set(chartData.map(s => s['source'])));
      const colors = this.libServ.dynamicColors(sources.length);
      this.mainPieChartJson['chartData']['labels'] = sources;
      this.mainPieChartJson['chartData']['datasets'][0]['data'] = Array.from(
        new Set(chartData.map(s => s['dp_revenue']))
      );

      setTimeout(() => {
        this.mainPieChartJson['chartData']['datasets'][0][
          'backgroundColor'
        ] = colors;
        this.showMainPieChartSource = true;
      }, 0);
    });
  }

  loadMainPieChart(params) {
    params['orderBy'] = [{ key: 'spend', opcode: 'desc' }];

    this.mainPieChartJson = {
      chartTypes: [{ key: 'pie', label: 'Pie Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Spend by DSP'
        },
        legend: {
          display: false
          // labels: {
          //   generateLabels: function(chart) {
          //     var data = chart.data;
          //     console.log('chart' ,chart)
          //     if (data.labels.length && data.datasets.length) {
          //       return data.labels.map(function(label, i) {
          //         var meta = chart.getDatasetMeta(0);
          //         var style = meta.controller.getStyle(i);

          //         return {
          //           text: label,
          //           fillStyle: style.backgroundColor,
          //           strokeStyle: style.borderColor,
          //           lineWidth: style.borderWidth,
          //           hidden: isNaN(data.datasets[0].data[i]) || meta.data[i].hidden,

          //           // Extra data used for toggling the correct item
          //           index: i
          //         };
          //       });
          //     }
          //     return [];
          //   }
          // }
        },
        elements: {
          arc: {
            borderWidth: 0
          }
        },
        plugins: {
          datalabels: {
            formatter: (value, ctx) => {
              let sum = 0;
              const dataArr = ctx.chart.data.datasets[0].data;
              dataArr.map(data => {
                sum += data;
              });
              const percentage = (value * 100) / sum;
              if (percentage > 10) {
                return `${percentage.toFixed(2)} %`;
                // ${this.formatNumPipe.transform(
                //   value,
                //   '$',
                //   []
                // )} - ${percentage.toFixed(2)} %`;
              } else {
                return '';
              }
            },
            color: 'black'
          }
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };
    this.mainPieChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          let sum = 0;
          const dataArr = data.datasets[0].data;
          dataArr.map(data => {
            sum += data;
          });
          const percentage = (currentValue * 100) / sum;
          return `${
            data.labels[tooltipItem.index]
            } : ${this.formatNumPipe.transform(
              currentValue,
              '$',
              []
            )} - ${percentage.toFixed(2)} %`;
        }
      }
    };
    this.showMainPieChart = false;
    this.dataFetchServ.getRevMgmtData(params).subscribe(data => {
      //  const data = {"data": [
      //   {
      //     "dp_revenue": 1220469.3799991545,
      //     "source": "MediaMath"
      //   },
      //   {
      //     "dp_revenue": 1220469.3799991545,
      //     "source": "DoubleClick"
      //   },
      //   {
      //     "dp_revenue": 581047.7535639718,
      //     "source": "LiveRamp"
      //   },
      //   {
      //     "dp_revenue": 112422.84000000003,
      //     "source": "Choozle"
      //   },
      //   {
      //     "dp_revenue": 94687.930000016,
      //     "source": "AppNexus"
      //   },
      //   {
      //     "dp_revenue": 58360.36000000965,
      //     "source": "Choozle"
      //   },
      //   {
      //     "dp_revenue": 52763.620000007635,
      //     "source": "Amazon"
      //   },
      //   {
      //     "dp_revenue": 51811.18119199999,
      //     "source": "TubeMogul"
      //   },
      //   {
      //     "dp_revenue": 46448.66000000611,
      //     "source": " BrightRoll"
      //   },
      //   {
      //     "dp_revenue": 39489.777866262186,
      //     "source": "Facebook"
      //   }
      // ],
      // "totalItems": 10
      // }

      const chartData = data['data'];
      if (typeof chartData === 'undefined' || !chartData.length) {
        this.noDataMainPieChart = true;
        return;
      } else {
        const sources = Array.from(new Set(chartData.map(s => s['dsp_name'])));
        const colors = this.libServ.dynamicColors(sources.length);
        this.mainPieChartJson['chartData']['labels'] = sources;
        this.mainPieChartJson['chartData']['datasets'][0]['data'] = Array.from(
          new Set(chartData.map(s => s['spend']))
        );
        this.mainPieChartJson['chartData']['datasets'][0][
          'backgroundColor'
        ] = colors;
        this.showMainPieChart = true;
        this.noDataMainPieChart = false;
      }
    });
  }

  loadMainLineChart(params) {
    const colors = this.libServ.dynamicColors(2);
    this.mainLineChartJson = {
      chartTypes: [{ key: 'line', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          // {
          //   label: 'eCPM',
          //   type: 'line',
          //   yAxisID: 'y-axis-0',

          //   borderColor: colors[0],
          //   fill: false,
          //   backgroundColor: colors[0],
          //   data: []
          // },
          {
            label: 'Spend',
            type: 'line',
            yAxisID: 'y-axis-1',

            borderColor: colors[1],
            fill: false,
            backgroundColor: colors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Daily Spend'
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            // {
            //   id: 'y-axis-0',
            //   scaleLabel: {
            //     display: true,
            //     labelString: 'eCPM'
            //   },
            //   position: 'right',
            //   name: '1',
            //   ticks: {
            //     callback: (value, index, values) => {
            //       return this.formatNumPipe.transform(value, '$', [0]);
            //     }
            //   }
            //   //   scaleFontColor: 'rgba(151,137,200,0.8)'
            // },
            {
              id: 'y-axis-1',
              scaleLabel: {
                display: true,
                labelString: 'Spend ($)'
              },
              position: 'left',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', [0]);
                }
              }
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];

              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
            }
          }
        },
        pan: {
          enabled: true,
          mode: 'x'
        },
        zoom: {
          enabled: true,
          mode: 'x'
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    // if (
    //   params['gidGroupBy'].findIndex(item => item === 'accounting_key') >= 0
    // ) {
    //   if (!params['dimensions'].includes('accounting_key')) {
    //     params['dimensions'].push('accounting_key');
    //   }

    //   params['gidGroupBy'].splice(
    //     params['gidGroupBy'].findIndex(item => item === 'accounting_key'),
    //     1
    //   );
    // }

    // if (params['gidGroupBy'].length === 0) {
    //   params['gidGroupBy'].push('source');
    // }

    this.showMainLineChart = false;
    this.dataFetchServ.getRevMgmtData(params).subscribe(data => {
      const chartData = data as {};
      this.noDataMainLineChart = false;
      if (!chartData['data'].length) {
        this.noDataMainLineChart = true;
        return;
      }

      // const chartData = {
      //   "data": [
      //     {
      //       "dp_impressions": 117722261,
      //       "dp_revenue": 188810.89942512452,
      //       "cpm": 1.6038674233849834,
      //       "time_key": 20200101
      //     },
      //     {
      //       "dp_impressions": 111146075,
      //       "dp_revenue": 167475.53249290644,
      //       "cpm": 1.5068056383719033,
      //       "time_key": 20200102
      //     },
      //     {
      //       "dp_impressions": 102995082,
      //       "dp_revenue": 144178.44672634994,
      //       "epm": 1.3998575847179766,
      //       "time_key": 20200103
      //     },
      //     {
      //       "dp_impressions": 100261095,
      //       "dp_revenue": 137544.62656460822,
      //       "epm": 1.3718643963005612,
      //       "time_key": 20200104
      //     },
      //     {
      //       "dp_impressions": 89316452,
      //       "dp_revenue": 118180.44294710692,
      //       "cpm": 1.3231654448959405,
      //       "time_key": 20200105
      //     },
      //     {
      //       "dp_impressions": 114575891,
      //       "dp_revenue": 153825.13543943386,
      //       "cpm": 1.3425611103424353,
      //       "time_key": 20200106
      //     },
      //     {
      //       "dp_impressions": 128473843,
      //       "dp_revenue": 174109.84521753428,
      //       "cpm": 1.355216292685619,
      //       "time_key": 20200107
      //     },
      //     {
      //       "dp_impressions": 117029987,
      //       "dp_revenue": 153802.23486900426,
      //       "cpm": 1.3142121845147625,
      //       "time_key": 20200108
      //     },
      //     {
      //       "dp_impressions": 119903583,
      //       "dp_revenue": 169277.4918314082,
      //       "cpm": 1.4117800952737851,
      //       "time_key": 20200109
      //     },
      //     {
      //       "dp_impressions": 119679985,
      //       "dp_revenue": 176340.82256838508,
      //       "cpm": 1.4734362021217253,
      //       "time_key": 20200110
      //     },
      //     {
      //       "dp_impressions": 93682153,
      //       "dp_revenue": 133781.59746181057,
      //       "cpm": 1.4280371786695656,
      //       "time_key": 20200111
      //     },
      //     {
      //       "dp_impressions": 92234825,
      //       "dp_revenue": 130900.51062140972,
      //       "cpm": 1.4192091828808664,
      //       "time_key": 20200112
      //     },
      //     {
      //       "dp_impressions": 120480850,
      //       "dp_revenue": 166268.26301167463,
      //       "cpm": 1.3800389274451055,
      //       "time_key": 20200113
      //     },
      //     {
      //       "dp_impressions": 126489029,
      //       "dp_revenue": 185370.98211740667,
      //       "cpm": 1.4655103575615769,
      //       "time_key": 20200114
      //     },
      //     {
      //       "dp_impressions": 130751742,
      //       "dp_revenue": 189212.75956198105,
      //       "cpm": 1.4471146362392713,
      //       "time_key": 20200115
      //     },
      //     {
      //       "dp_impressions": 14531411,
      //       "dp_revenue": 29848.37213888276,
      //       "cpm": 2.054058765448363,
      //       "time_key": 20200116
      //     }
      //   ],
      //   "totalItems": 16
      // }

      const datesArr = Array.from(
        new Set(chartData['data'].map(r => r['time_key']))
      );
      // const dimData = Array.from(
      //   new Set(chartData['data'].map(r => r['audience_name']))
      // );
      // colors = this.libServ.dynamicColors(dimData.length);
      this.mainLineChartJson['chartData']['labels'] = datesArr.map(d =>
        moment(d, 'YYYYMMDD').format('YYYY-MM-DD')
      );
      const ecpmArr = [];
      const revArr = [];

      datesArr.forEach(time_key => {
        chartData['data'].forEach(r => {
          if (r['time_key'] === time_key) {
            revArr.push(r['spend']);
            // ecpmArr.push(r['cpm']);
          }
        });
      });

      setTimeout(() => {
        // this.mainLineChartJson['chartData']['datasets'][0]['data'] = ecpmArr;
        this.mainLineChartJson['chartData']['datasets'][0]['data'] = revArr;
        this.showMainLineChart = true;
      }, 0);
    });
  }

  chartSelected(data: Event) {
    //   console.log('Chart Selected', data);
  }

  loadTableData() {
    //   Remove unwanted columns
    const finalColDef = this.libServ.deepCopy(this.metricColDef);

    const grpBys = this.getGrpBys();
    const grpBysFilter = this.filtersApplied['groupby'].map(e => e.key);

    grpBysFilter.reverse().forEach(grp => {
      finalColDef.unshift(this.dimColDef.find(e => e.field === grp));
    });

    this.aggTableColumnDef = finalColDef.slice();
    this.aggTableJson['columns'] = this.aggTableColumnDef.slice(
      grpBysFilter.length
    );
    this.aggTableJson['selectedColumns'] = this.aggTableColumnDef.slice(
      grpBysFilter.length
    );
    this.aggTableJson['frozenCols'] = [
      ...this.aggTableColumnDef.slice(0, grpBysFilter.length)
    ];
    this.aggTableJson['frozenWidth'] =
      this.aggTableColumnDef
        .slice(0, grpBysFilter.length)
        .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px';

    this.reloadAggTable();

    this.aggTableJson['loading'] = true;

    const tableReq = {
      dimensions: [this.filtersApplied['groupby'][0]['key']],
      metrics: ['impressions'],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: { key: [], interval: 'daily' },
      gidGroupBy: grpBys,
      orderBy: [
        { key: this.filtersApplied['groupby'][0]['key'], opcode: 'asc' }
      ],
      limit: '',
      offset: ''
    };
    // For Dev purpose
    // tableReq['gidGroupBy'] = [
    //   'ad_size',
    //   'ad_type',
    //   'ad_units',
    //   'bidding_type',
    //   'device_category',
    //   'geography',
    //   'sales_channel',
    //   'sales_type',
    //   'site',
    //   'source'
    // ];

    // if(tableReq['gidGroupBy'].findIndex(item=>item==='accounting_key')>=0){
    //   tableReq['gidGroupBy'].splice(tableReq['gidGroupBy'].findIndex(item=>item==='accounting_key'),1)
    //   tableReq['dimensions'].push('accounting_key');
    // }

    this.dataFetchServ.getRevMgmtData(tableReq).subscribe(data => {
      if (data['status'] === 0 || !data['data'].length) {
        this.noTableData = true;
        // this.toastService.displayToast({
        //   severity: 'error',
        //   summary: 'Server Error',
        //   detail: 'Please refresh the page'
        // });

        console.log(data['status_msg']);
        return;
      } else {
        this.noTableData = false;
      }

      const tableData = data['data'];

      const arr = [];
      tableData.forEach((row: object) => {
        if (row['audience_status'] == '1') { row['audience_status'] = 'Active'; } else { row['audience_status'] = 'Inactive'; }

        for (let i = 0; i < grpBysFilter.length; i++) {
          if (i !== grpBysFilter.length - 1) {
            row[grpBysFilter[i]] = 'All';
          }
        }
        let obj = {};

        if (grpBysFilter.length === 1) {
          obj = {
            data: row
          };
        } else {
          obj = {
            data: row,
            children: [{ data: {} }]
          };
        }

        arr.push(obj);
      });

      this.aggTableData = <TreeNode[]>arr;
      this.aggTableJson['totalRecords'] = data['totalItems'];

      this.aggTableJson['loading'] = false;
    });
  }

  reloadAggTable() {
    this.displayAggTable = false;
    setTimeout(() => {
      this.displayAggTable = true;
    }, 0);
  }

  loadCards(cardsReq) {
    this.showCards = false;
    this.dataFetchServ.getRevMgmtData(cardsReq).subscribe(data => {

      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }
      if (!this.libServ.isEmptyObj(data['data'])) {
        data['data'][0]['total_cookie_pool'] = '78124'; // Math.floor(Math.random() * Math.floor(10000))
        data['data'][0]['audience_segment'] = '7'; // Math.floor(Math.random() * Math.floor(10000)),
        data['data'][0]['total_dsps'] = '3'; // Math.floor(Math.random() * Math.floor(10)),
        data['data'][0]['active_dsps'] = '3'; // Math.floor(Math.random() * Math.floor(10)),
        data['data'][0]['active_cookie_pool'] = '65650'; // Math.floor(Math.random() * Math.floor(100000)),

        this.cardsJson.map(o => {
          o['value'] = data['data'][0][o['field']];
          this.showCards = true;
        });
      } else {
        this.cardsJson.map(o => {
          o['value'] = 0;
          this.showCards = true;
        });
      }
    });
  }

  getFooters(grpBys) {
    const tableReq1 = {
      dimensions: [],
      metrics: ['dp_revenue', 'dp_impressions', 'dp_clicks'],
      derived_metrics: ['cpm', 'cpc', 'ctr'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.filtersApplied['filters'],
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },

      gidGroupBy: this.getGrpBys(), //   grpBys,
      orderBy: [],
      limit: '',
      offset: ''
    };

    if (
      tableReq1['gidGroupBy'].findIndex(item => item === 'accounting_key') >= 0
    ) {
      tableReq1['gidGroupBy'].splice(
        tableReq1['gidGroupBy'].findIndex(item => item === 'accounting_key'),
        1
      );
    }

    if (tableReq1['gidGroupBy'].length === 0) {
      tableReq1['gidGroupBy'].push('source');
    }

    this.dataFetchServ.getRevMgmtData(tableReq1).subscribe(data11 => {
      if (data11['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data11['status_msg']);
        return;
      }

      const data1 = data11['data'][0];
      this.aggTableJson['selectedColumns'].forEach(c => {
        if (typeof data1 !== 'undefined') {
          c['footerTotal'] = data1[c['field']];
        } else {
          c['footerTotal'] = 0;
        }
      });
    });
  }

  onTableDrill(e: Event) {
    console.log('Drilled', e);
    if (!e['node']['childLoaded']) {

      this.aggTableJson['loading'] = true;
      const tableReq = {
        dimensions: [],
        metrics: ['impressions'],
        derived_metrics: [],
        timeKeyFilter: this.libServ.deepCopy(
          this.filtersApplied['timeKeyFilter']
        ),
        filters: this.libServ.deepCopy(this.filtersApplied['filters']),
        groupByTimeKey: {
          key: [],
          interval: 'daily'
        },
        gidGroupBy: this.getGrpBys(),
        orderBy: [],
        limit: '',
        offset: ''
      };
      const grpBys = this.filtersApplied['groupby'].map(e3 => e3.key);

      for (const g of grpBys) {
        if (e['node']['data'][g] !== 'All') {
          tableReq['dimensions'].push(g);
          if (!tableReq['gidGroupBy'].includes(g)) {
            tableReq['gidGroupBy'].push(g);
          }
          if (
            tableReq['filters']['dimensions'].findIndex(e2 => e2.key === g) ===
            -1
          ) {
            tableReq['filters']['dimensions'].push({
              key: g,
              values: [e['node']['data'][g]]
            });
          } else {
            tableReq['filters']['dimensions'].splice(
              tableReq['filters']['dimensions'].findIndex(e1 => e1.key === g),
              1
            );
            tableReq['filters']['dimensions'].push({
              key: g,
              values: [e['node']['data'][g]]
            });
            //   if (
            //     tableReq['filters']['dimensions']
            //       .find(e => e.key === g)
            //       ['values'].findIndex(v => v === e['node']['data'][g]) === -1
            //   ) {
            //     tableReq['filters']['dimensions'].push(e['node']['data'][g]);
            //   }
          }
        } else {
          tableReq['dimensions'].push(g);
          if (!tableReq['gidGroupBy'].includes(g)) {
            tableReq['gidGroupBy'].push(g);
          }
          break;
        }
      }

      if (
        tableReq['gidGroupBy'].findIndex(item => item === 'accounting_key') >= 0
      ) {
        tableReq['gidGroupBy'].splice(
          tableReq['gidGroupBy'].findIndex(item => item === 'accounting_key'),
          1
        );
      }

      this.dataFetchServ.getRevMgmtData(tableReq).subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }

        //   let data ={};

        //   if(e['node']['data']['audience'] === 'All')
        //   {
        //     data = {
        //     "data": [
        //       {
        //         "id": "5e207b84826692.64351966",
        //         "name": e['node']['data']['name'],
        //         "status": "Active",
        //        // "campaign": "Chrismas high value donor",
        //         "campaign" : "All",
        //        "audience": "Donor above 50k dollor",
        //         "spend" : 3442
        //       },
        //       {
        //         "id": "4e207b84826692.64351966",
        //         "name": e['node']['data']['name'],
        //         "status": "Active",
        //         // "campaign": "Chrismas high value donor",
        //         "campaign" : "All",
        //         "audience": "website visit > 2",
        //         "spend" : 5784
        //       },
        //       {
        //         "id": "1e207b84826692.64351966",
        //         "name": e['node']['data']['name'],
        //         "status": "Active",
        //         // "campaign": "Chrismas high value donor",
        //         "campaign" : "All",
        //         "audience": "Age above 20",
        //         "spend" : 1200
        //       },
        //       {
        //         "id": "3e207b84826692.64351966",
        //         "name": e['node']['data']['name'],
        //         "status": "Active",
        //         // "campaign": "Chrismas high value donor",
        //         "campaign" : "All",
        //         "audience": "Youth with gender male	",
        //         "spend" : 2340
        //       },
        //       {
        //         "id": "7e207b84826692.64351966",
        //         "name": e['node']['data']['name'],
        //         "status": "Active",
        //         // "campaign": "Chrismas high value donor",
        //         "campaign" : "All",
        //         "audience": "age <> 18 to 24	",
        //         "spend" : 3000

        //       },
        //       {
        //         "id": "9e207b84826692.64351966",
        //         "name": e['node']['data']['name'],
        //         "status": "Active",
        //         // "campaign": "Chrismas high value donor",
        //         "campaign" : "All",
        //         "audience": " age <> 25 to 40	",
        //         "spend" : 1000
        //       },

        //     ],
        //     "totalItems": 7
        //   }
        // }
        // else{
        //   data = {
        //     "data": [
        //       {
        //         "id": "5e207b84826692.64351966",
        //         "name": e['node']['data']['name'],
        //         "status": "Active",
        //         "campaign": "Casablanca",
        //       //  "audience": "Donor above 50k dollor",
        //         "audience" :e['node']['data']['audience'],
        //         "spend" : 900
        //       },
        //       {
        //         "id": "4e207b84826692.64351966",
        //         "name": e['node']['data']['name'],
        //         "status": "Active",
        //         "campaign": "Excellance",
        //         "audience" :e['node']['data']['audience'],
        //         // "audience": "website visit > 2",
        //         "spend" : 760
        //       },
        //       {
        //         "id": "1e207b84826692.64351966",
        //         "name": e['node']['data']['name'],
        //         "status": "Active",
        //         "campaign": "Pronovias",
        //         // "campaign" : "All",
        //        // "audience": "Age above 20",
        //        "audience" :e['node']['data']['audience'],
        //        "spend" : 840
        //       },
        //       {
        //         "id": "3e207b84826692.64351966",
        //         "name": e['node']['data']['name'],
        //         "status": "Active",
        //         "campaign": "Bahamas",
        //         //"campaign" : "All",
        //         //"audience": "Youth with gender male	",
        //         "audience" :e['node']['data']['audience'],
        //         "spend" : 690
        //       },
        //       // {
        //       //   "id": "7e207b84826692.64351966",
        //       //   "name": e['node']['data']['name'],
        //       //   "status": "Active",
        //       //   // "campaign": "Chrismas high value donor",
        //       //   "campaign" : "All",
        //       //   "audience": "age <> 18 to 24	",
        //       //   "spend" : 2334

        //       // },
        //       // {
        //       //   "id": "9e207b84826692.64351966",
        //       //   "name": e['node']['data']['name'],
        //       //   "status": "Active",
        //       //   // "campaign": "Chrismas high value donor",
        //       //   "campaign" : "All",
        //       //   "audience": " age <> 25 to 40	",
        //       //   "spend" : 1121
        //       // },

        //     ],
        //     "totalItems": 7
        //   }
        // }
        const childData = data['data'];
        const arr = [];

        childData.forEach((row: object) => {
          row['attribute'] = 'Details';

          if (row['audience_status'] == '1') { row['audience_status'] = 'Active'; } else { row['audience_status'] = 'Inactive'; }

          this.aggTableJson['frozenCols'].forEach(r => {
            if (typeof row[r['field']] === 'undefined') {
              row[r['field']] = 'All';
            }
          });
          let obj = {};
          if (row[grpBys[grpBys.length - 1]] === 'All') {
            obj = {
              data: row,
              children: [{ data: {} }]
            };
          } else {
            obj = {
              data: row
            };
          }
          arr.push(obj);
        });
        this.aggTableJson['loading'] = false;
        e['node']['children'] = <TreeNode[]>arr;

        console.log('arr', arr);

        this.aggTableData = [...this.aggTableData];
        console.log('this.aggTableData', this.aggTableData);

        e['node']['childLoaded'] = true;
      });
    }
  }

  onLazyLoadAggTable(e: Event) {
    console.log('Lazy Agg', e);
  }

  openPopup(row, field) {
    const grpBys = [];
    const filtersApplied = this.libServ.deepCopy(this.filtersApplied);
    let str = '';

    const fieldIdx = filtersApplied['groupby'].findIndex(
      e => e['key'] === field
    );
    filtersApplied['groupby'].forEach((grp, i) => {
      if (i <= fieldIdx) {
        if (
          filtersApplied['filters']['dimensions'].findIndex(
            e => e.key === grp['key']
          ) !== -1
        ) {
          filtersApplied['filters']['dimensions'].splice(
            filtersApplied['filters']['dimensions'].findIndex(
              e => e.key === grp['key']
            ),
            1
          );
        }
        filtersApplied['filters']['dimensions'].push({
          key: grp['key'],
          values: [row[grp['key']]]
        });
        if (str === '') {
          str = str + row[grp['key']];
        } else {
          str = str + ' > ' + row[grp['key']];
        }

      }
    });

    const grpByDims = filtersApplied['groupby'].slice(
      0,
      filtersApplied['groupby'].findIndex(item => item.key === field) + 1
    );

    filtersApplied['gidGroupBy'] = this.getGrpBys();

    filtersApplied['groupby'] = grpBys;

    filtersApplied['filters']['dimensions'] = [
      { key: field, values: [row[field]] }
    ];

    const data = {
      filters: filtersApplied,
      groupBy: this.libServ.deepCopy(this.filtersApplied['groupby']),
      grpbyColDef: grpByDims,
      field: field,
      row: row
    };

    const ref = this.dialogService.open(DSPConnectionDailyDataPopupComponent, {
      header: str,
      contentStyle: { width: '80vw', overflow: 'auto' },
      data: data
    });
    ref.onClose.subscribe((data1: string) => { });
  }

  exportTable(table, fileFormat) {
    if (fileFormat !== 'Excel') {
      const grpBys = this.getGrpBys();
      const colDef = this.libServ.deepCopy(this.aggTableColumnDef);
      grpBys.push('time_key');
      const tableReq = {
        dimensions: this.filtersApplied['groupby'].map(e => e.key),
        metrics: ['dp_revenue', 'dp_impressions', 'dp_clicks'],
        derived_metrics: ['ctr', 'cpc', 'cpm'],
        timeKeyFilter: this.libServ.deepCopy(
          this.filtersApplied['timeKeyFilter']
        ),
        filters: this.libServ.deepCopy(this.filtersApplied['filters']),
        groupByTimeKey: {
          key: ['accounting_key', 'time_key'],
          interval: 'daily'
        },
        gidGroupBy: this.filtersApplied['groupby'].map(e => e.key), //  grpBys.filter(v => v !== 'time_key'),
        orderBy: [{ key: 'time_key', opcode: 'asc' }],
        limit: '',
        offset: ''
      };

      tableReq['dimensions'].push('time_key');
      tableReq['isTable'] = false;
      this.dataFetchServ.getRevMgmtData(tableReq).subscribe(success => {
        if (success['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(success['status_msg']);
          return;
        }
        const data = [];
        const obj = {
          field: 'time_key',
          displayName: 'Date',
          format: 'date',
          width: '250',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: false
          }
        };

        colDef.splice(0, 0, obj);
        const dailyData = this.libServ.deepCopy(success['data']);
        dailyData.forEach((o: object) => {
          const tk = o['time_key'];
          o['time_key'] = moment(tk, 'YYYYMMDD').format('YYYY-MM-DD');
        });

        data.push({
          data: [
            {
              data: dailyData,
              columnDefs: colDef
            }
          ],

          sheetname: 'Daily Breakdown'
        });

        this.exportService.exportReport(
          data,
          'Daily Breakdown ' +
          ' From ' +
          moment(
            this.filtersApplied['timeKeyFilter']['time_key1'],
            'YYYYMMDD'
          ).format('MM-DD-YYYY') +
          ' To ' +
          moment(
            this.filtersApplied['timeKeyFilter']['time_key2'],
            'YYYYMMDD'
          ).format('MM-DD-YYYY'),
          fileFormat,
          false
        );
      });
    } else {
      const data = {
        filters: this.filtersApplied,
        colDef: this.aggTableColumnDef
      };

    }

    return false;
  }

  customSort(event: SortEvent) {
    if (!this.libServ.isEqual(this.currentSort, event.multiSortMeta[0])) {
      this.currentSort = event.multiSortMeta[0];
      event.data.sort((data1, data2) => {
        const value1 = data1['data'][this.currentSort['field']];
        const value2 = data2['data'][this.currentSort['field']];
        let result = null;

        if (value1 == null && value2 != null) {
          result = -1;
        } else if (value1 != null && value2 == null) {
          result = 1;
        } else if (value1 == null && value2 == null) {
          result = 0;
        } else if (typeof value1 === 'string' && typeof value2 === 'string') {
          result = value1.localeCompare(value2);
        } else {
          result = value1 < value2 ? -1 : value1 > value2 ? 1 : 0;
        }
        return this.currentSort['order'] * result;
      });
    }
  }

  isHiddenColumn(col: Object) {
    return (
      this.aggTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }

  /* --------------------- On Filter Apply Button Clicked --------------------- */

  onFiltersApplied(filterData: object) {
    console.log('filterData', filterData);
    this.filtersApplied['filters']['dimensions'] = [];

    //   tslint:disable-next-line: forin
    for (const k in filterData['filter']['dimensions']) {
      if (filterData['filter']['dimensions'][k].length !== 0) {
        this.filtersApplied['filters']['dimensions'].push({
          key: k,
          values: filterData['filter']['dimensions'][k]
        });
      }
    }

    this.filtersApplied['groupby'] = filterData['groupby'];
    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];

    this.loadTableData();

    const grpBys = this.getGrpBys();

    const cardsReq = {
      dimensions: [],
      metrics: ['spend'],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: [],
      orderBy: [],
      limit: '',
      offset: ''
    };

    this.loadCards(cardsReq);

    const mainLineChartReq = {
      dimensions: ['time_key'],
      metrics: ['spend'],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key'],
        interval: 'daily'
      },
      gidGroupBy: grpBys,
      orderBy: [],
      limit: '', //   Max value of Int MySql
      offset: ''
    };
    this.loadMainLineChart(mainLineChartReq);

    const mainPieChartReq = {
      dimensions: ['dsp_name'],
      metrics: ['spend'],
      derived_metrics: [],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: ['dsp_name'],
      orderBy: [{ key: 'spend', opcode: 'desc' }],
      limit: '',
      offset: ''
    };
    this.loadMainPieChart(mainPieChartReq);
  }

  loadSourceRevnueChart() {
    const req = {
      dimensions: ['source', 'time_key'],
      metrics: ['dp_revenue'],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys(), //  ['source'],
      //   orderBy: [],
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };
    let colors = this.libServ.dynamicColors(2);
    this.metricChartJson = {
      chartTypes: [{ key: 'bar', label: 'Stack Trend', stacked: true }],
      chartData: {
        labels: [],
        datasets: []
      },
      chartOptions: {
        title: {
          display: true,
          text: ''
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              display: true,
              scaleLabel: {
                display: true
              }
            }
          ]
        },
        pan: {
          enabled: true,
          mode: 'x'
        },
        zoom: {
          enabled: false,
          mode: 'x'
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    if (
      req['gidGroupBy'].filter(item => item === 'accounting_key').length >= 1
    ) {
      req['gidGroupBy'].splice(
        req['gidGroupBy'].findIndex(item => item === 'accounting_key'),
        1
      );
    }

    if (
      req['gidGroupBy'].length === 0 ||
      req['gidGroupBy'].findIndex(item => item === 'source') < 0
    ) {
      req['gidGroupBy'].push('source');
    }

    this.metricChartJson['chartOptions']['title']['text'] =
      'Sources By Revenue';
    this.metricChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
      callback: (value, index, values) => {
        return this.formatNumPipe.transform(value, '$', [0]);
      }
    };
    this.metricChartJson['chartOptions']['scales']['yAxes'][0]['scaleLabel'][
      'labelString'
    ] = 'Revenue ($)';
    this.metricChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return `${
            data.datasets[tooltipItem.datasetIndex].label
            } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
        }
      }
    };

    this.showMetricChart = false;
    this.dataFetchServ.getRevMgmtData(req).subscribe(data => {
      const chartData = data['data'];
      const dimData = Array.from(new Set(chartData.map(s => s['source'])));
      this.noDataMetricChart = false;
      if (!chartData.length) {
        this.noDataMetricChart = true;
        return;
      }

      const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));
      this.metricChartJson['chartData']['labels'] = datesArr.map(d =>
        moment(d, 'YYYYMMDD').format('YYYY-MM-DD')
      );
      colors = this.libServ.dynamicColors(dimData.length);

      setTimeout(() => {
        dimData.forEach((src, i) => {
          const revArr = [];
          datesArr.forEach(time_key => {
            chartData.forEach(r => {
              if (r['source'] === src && r['time_key'] === time_key) {
                revArr.push(r['dp_revenue']);
              }
            });
          });

          this.metricChartJson['chartData']['datasets'].push({
            label: src,
            data: revArr,
            borderColor: colors[i],
            fill: false,
            backgroundColor: colors[i]
          });
        });
        this.showMetricChart = true;
      }, 0);
    });
  }

  getGrpBys() {
    //   let grpBys = this.filtersApplied['groupby'].map(e => e.key);
    let grpBys = [this.filtersApplied['groupby'][0]['key']];
    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          grpBys.concat(
            this.filtersApplied['filters']['dimensions']
              .filter(f => f.values.length)
              .map(m => m.key)
          )
        )
      );
    }
    return grpBys;
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['displayName']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    } else if (type === 'ppt') {
      const chartElements = document.getElementsByTagName('ym-charts');
      const slides = [];
      [].forEach.call(chartElements, (el, index) => {
        const c = el.getElementsByTagName('canvas')[0];

        slides.push({
          title: `${this.appConfig['name']}`,
          base64data: this.libServ.canvasToImage('white', c),
          width: 8,
          height: 3
        });

        if (index === chartElements.length - 1) {
          const pptConfig = {
            filename: 'Charts',
            logo: {
              visible: true,
              path: '/assets/images/ym.png',
              width: 0.53125,
              height: 0.458333
            },
            slides: slides
          };
          this.pptExport.exportPPT(pptConfig);
        }
      });
    }
    return false;
  }

  tabChanged(e) {
    switch (e.index) {
      case 0:
        break;
      case 1:
        this.loadSourceRevnueChart();
        break;
    }
  }

  ngOnDestroy(): void {
    this.appConfigObs.unsubscribe();
  }
}
