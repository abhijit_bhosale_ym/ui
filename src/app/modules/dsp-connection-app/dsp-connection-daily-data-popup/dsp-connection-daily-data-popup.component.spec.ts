import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DSPConnectionDailyDataPopupComponent } from './dsp-connection-daily-data-popup.component';

describe('DSPConnectionDailyDataPopupComponent', () => {
  let component: DSPConnectionDailyDataPopupComponent;
  let fixture: ComponentFixture<DSPConnectionDailyDataPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DSPConnectionDailyDataPopupComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DSPConnectionDailyDataPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
