import { Routes } from '@angular/router';
import { DSPConnectionAppComponent } from './dsp-connection-app.component';

export const DSPConnectionAppRoutes: Routes = [
  {
    path: '',
    component: DSPConnectionAppComponent
  }
];
