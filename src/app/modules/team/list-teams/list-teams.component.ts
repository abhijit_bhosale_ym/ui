import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';
import { AppConfigService, TeamService, CommonLibService, ToastService } from 'src/app/_services';
import { DialogService } from 'primeng/dynamicdialog';
import { TeamDetailsComponent } from '../team-details/team-details.component';
import { EditTeamComponent } from '../edit-team/edit-team.component';

@Component({
  selector: 'ym-list-teams',
  templateUrl: './list-teams.component.html',
  styleUrls: ['./list-teams.component.scss']
})
export class ListTeamsComponent implements OnInit, OnDestroy {

  public columns: any[];
  private appConfigObs: Subscription;
  private isEditPermission = false;
  public appConfig: object = {};
  public teams: any[];

  constructor(
    private appConfigService: AppConfigService,
    private teamService: TeamService,
    private _titleService: Title,
    private dialogService: DialogService,
    private toastService: ToastService,
    private libServ: CommonLibService
  ) { }

  ngOnInit() {
    this.columns = [
      {
        field: 'name',
        header: 'Name',
        width: '245'
      },
      {
        field: 'details',
        header: 'Details',
        width: '100'
      },
      {
        field: 'createdAt',
        header: 'Created at',
        width: '215'
      },
      {
        field: 'updatedAt',
        header: 'Edited At',
        width: '215'
      }
    ];
    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        console.log('appConfig', this.appConfig);
        this._titleService.setTitle(this.appConfig['displayName']);
        this.isEditPermission = this.appConfig['permissions'].some(
          o => o.name === 'um-update-team'
        );
        if (!(this.columns.some(o => o.field == 'edit'))) {
          this.isEditPermission ? this.columns.push({ field: 'edit', header: 'Edit', width: '60' }) : '';
        }
      }
      this.loadTable();
    });

  }


  loadTable() {
    this.teamService.getTeams().subscribe(res => {
      if (res['status']) {
        this.teams = res['data'];
      } else {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Something went wrong',
          detail: 'Error creating User'
        });
      }
    });
  }
  teamDetails(team) {
    const ref = this.dialogService.open(TeamDetailsComponent, {
      header: ' Team Details ',
      contentStyle: { 'min-height': '65vh', 'max-height': '65vh', width: '60vw', overflow: 'auto' },
      data: team
    });
    ref.onClose.subscribe((data1: string) => { });
  }

  editTeam(teamDtls) {
    const data = {
      team: teamDtls,
      userTeamId: this.appConfig['user']['team']['id'],
      userRoleId: this.appConfig['user']['role']['id']
    };

    const ref = this.dialogService.open(EditTeamComponent, {
      header: ' Edit Team ',
      contentStyle: { 'min-height': '80vh', 'max-height': '80vh', width: '60vw', overflow: 'auto' },
      data: data
    });
    ref.onClose.subscribe((data1: string) => { });
  }

  decreaseSearchWidth(width) {
    return width - 30;
  }
  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }

}
