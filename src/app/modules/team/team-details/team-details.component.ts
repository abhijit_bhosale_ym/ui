import { Component, OnInit } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { TeamService } from 'src/app/_services/users/team.service';
import { SearchStringPipe } from 'src/app/_pipes/search-string.pipe';
import { ToastService } from 'src/app/_services';
import { environment } from 'src/environments/environment';

interface ITeamDetails {
  id: number;
  name: string;
  ownerRoleId: number;
  ownerTeamId: number;
  ownerUserId: number;
  resources: string;
  createdAt: number;
  updatedAt: number;
}

@Component({
  selector: 'ym-team-details',
  templateUrl: './team-details.component.html',
  styleUrls: ['./team-details.component.scss']
})
export class TeamDetailsComponent {
  public team: ITeamDetails;
  public detailsLoaded = false;
  public resources: any[];
  public teamDetails: object;
  defaultDimensions = [];
  private BASE_URL: string = environment.baseUrl;


  constructor(
    private config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private teamService: TeamService,
    private toastService: ToastService,
    private searchStringPipe: SearchStringPipe
  ) {
    this.team = this.config.data;
  }

  ngOnInit() {
    console.log('team data---', this.config.data);
    this.defaultDimensions = [
      {
        label: 'Payout Resources',
        key: 'payout_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/common/filter`,
        enable: false,
        permissionNames: [
          'Publisher Payouts',
          'Frankly Payout',
          'Revenue Management 360',
          '3P Statements',
          'analysis_report',
          'Revenue Dashboard',
          'Finance Audit',
          'payment-contact-info'
        ],
        value: [
          {
            label: 'Station Group',
            filter_key: 'dim_station_group',
            key: 'station_group',
            config_json: null,
            value: ['User have access to All Station Group']
          },
          {
            label: 'Station',
            filter_key: 'dim_payout_station',
            key: 'derived_station_rpt',
            config_json: null,
            value: ['User have access to All Station']
          },
          {
            label: 'Source',
            filter_key: 'dim_payout_source',
            key: 'source',
            config_json: null,
            value: ['User have access to All Source']
          }
        ]
      },
      {
        label: 'Revenue Resources',
        key: 'revenue_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/revmgmt/filter`,
        enable: false,
        permissionNames: ['Revenue Management'],
        value: [
          {
            label: 'Source',
            key: 'rev_source',
            filter_key: 'dim_rev_source',
            config_json: null,
            value: ['User have access to All Source']
          }
        ]
      },
      {
        label: 'Unfilled Resources',
        key: 'unfilled_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/unfilled-inventory/filter`,
        enable: false,
        permissionNames: [
          'Unfilled',
          'analysis_report',
          'Revenue Dashboard'
        ],
        value: [
          {
            label: 'Stations',
            key: 'station',
            filter_key: 'dim_unfilled_station',
            config_json: null,
            value: ['User have access to All Unfilled Resources']
          }
        ]
      },
      {
        label: 'Vast Resources',
        key: 'vast_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/vast-errors/filter`,
        enable: false,
        permissionNames: [
          'Vast Errors',
          'analysis_report',
          'Revenue Dashboard'
        ],
        value: [
          {
            label: 'Advertiser',
            key: 'advertiser',
            filter_key: 'dim_advertiser',
            config_json: null,
            value: ['User have access to All Advertiser']
          }
        ]
      },
      {
        label: 'CPR Resources',
        key: 'cpr_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/campaign-progress-report/filter`,
        enable: false,
        permissionNames: ['CPR'],
        value: [
          {
            label: 'Order',
            key: 'order_name',
            filter_key: 'dim_order_name',
            config_json: null,
            value: ['User have access to All Order']
          },
          {
            label: 'Line Item',
            key: 'lineitem',
            filter_key: 'dim_line_item',
            config_json: null,
            value: ['User have access to All Line Item']
          }
        ]
      },
      {
        label: 'Prebid Resources',
        key: 'prebid_publisher_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/prebid/filter`,
        enable: false,
        permissionNames: ['Prebid Analytics'],
        value: [
          {
            label: 'Publisher Name',
            key: 'publisher_name',
            filter_key: 'dim_prebid_publisher',
            config_json: null,
            value: 'Publisher Name'
          }
        ]
      },
      {
        label: 'Revenue Resources Freecycle',
        key: 'revenue_resources_fc',
        filterUrl: `${this.BASE_URL}/freecycle/v1/revmgmt-fc/filter`,
        enable: false,
        permissionNames: ['Revenue Management FreeCycle',
        'Analysis Report FreeCycle',
        'Revenue Dashboard FreeCycle'],
        value: [
          {
            label: 'Source',
            key: 'source',
            filter_key: 'dim_source',
            config_json: null,
            value: 'Source'
          }
        ]
      },
      {
        label: 'Unfilled Resources Freecycle',
        key: 'unfilled_resources_fc',
        filterUrl: `${this.BASE_URL}/freecycle/v1/unfilled-inventory-fc/filter`,
        enable: false,
        permissionNames: [
          'Unfilled Inventory FreeCycle',
          'Analysis Report FreeCycle',
          'Revenue Dashboard FreeCycle'
        ],
        value: [
          {
            label: 'Ad Unit',
            key: 'ad_unit',
            filter_key: 'dim_ad_unit',
            config_json: null,
            value: 'Ad unit'
          }
        ]
      },
      {
        label: 'ASBN Programmatic Revenue Resources',
        key: 'asbn_prog_rev_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/asbn-revmgmt/filter`,
        enable: false,
        permissionNames: [
          'Revenue Management Asbn'
        ],
        value: [
          {
            label: 'Source',
            filter_key: 'dim_prog_rev_source',
            key: 'source',
            config_json: null,
            value: 'Source'
          }
        ]
      },
      {
        label: 'ASBN Revenue Management Resources',
        key: 'asbn_rev_360_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/asbn-rev360/filter`,
        enable: false,
        permissionNames: [
          'Revenue Management 360 Asbn'
        ],
        value: [
          {
            label: 'Station Group',
            filter_key: 'dim_rev_360_media_group',
            key: 'media_group',
            config_json: null,
            value: 'Station Group'
          },
          {
            label: 'Station',
            filter_key: 'dim_rev_360_property',
            key: 'property',
            config_json: null,
            value: 'Station'
          },
          {
            label: 'Source',
            filter_key: 'dim_rev_360_source',
            key: 'source',
            config_json: null,
            value: 'Source'
          }
        ]
      }
    ];

    this.teamService.getUsersRolesAssociatedWithTeam(this.team['id']).subscribe(res => {
      if (res['status']) {
        console.log(res);
        this.teamDetails = res['data'];
        if (typeof res['data']['resources'] !== 'string') {
          res['data']['resources'] = JSON.stringify(res['data']['resources']);
        }
        let resources: any = [];
        try {
          resources = JSON.parse(res['data']['resources']);
        } catch (error) {
          resources = '';
          console.log(error);
        }
        if (resources !== '') {
          if (resources.length) {
            this.resources = JSON.parse(this.team.resources).filter(
              v => v.value.length > 0
            );
            this.resources.forEach(resource => {
              this.defaultDimensions.forEach(dim => {
                dim.value.forEach(element => {
                  // value.forEach(element => {
                  if (element['filter_key'] == resource['key']) {
                    dim.enable = true;
                    element['value'] = resource['value'];
                  }
                  // });
                });
              });
            });


          } else {
            this.resources = [];
            this.defaultDimensions.forEach(dim => {
              dim.enable = true;
            });
          }
        } else {
          this.resources = [];
          this.defaultDimensions.forEach(dim => {
            dim.enable = true;
          });
        }
        this.detailsLoaded = true;
        this.defaultDimensions = this.defaultDimensions.filter(x => x.enable);
      } else {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Something went wrong',
          detail: 'Error while fetching details'
        });
      }
    });
  }
}
