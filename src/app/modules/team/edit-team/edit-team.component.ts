import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { PlatformConfigService, CommonLibService } from 'src/app/_services';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { RoleService } from '../../../_services/users/role.service';
import { TeamService } from '../../../_services/users/team.service';
import { Subscription, from } from 'rxjs';
import { TreeNode, SelectItemGroup } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { DialogService } from 'primeng/dynamicdialog';
import { ImpactedUserComponent } from '../../users/impacted-user/impacted-user.component';
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder
} from '@angular/forms';
import { environment } from 'src/environments/environment';
import { resource } from 'selenium-webdriver/http';


@Component({
  selector: 'ym-edit-team',
  templateUrl: './edit-team.component.html',
  styleUrls: ['./edit-team.component.scss'],
  providers: [DialogService]
})
export class EditTeamComponent implements OnInit, OnDestroy {
  private platConfigObs: Subscription;
  private BASE_URL: string = environment.baseUrl;
  description: string;
  selectedPermissions: any[];
  originalResources: any[];
  teamRegisterForm: FormGroup;
  noPermissionsFlag = false;
  permCols: any[];
  permissionsData = [];
  team: object;
  userRoleId: any;
  isTeamExists = false;
  formTeamModel = {
    teamName: '',
    selectedDims: []
  };
  groupedApps: SelectItemGroup[];

  defaultDimensions = [];
  availableDimensions = [];
  constructor(

    private roleServ: RoleService,
    private teamServ: TeamService,
    private dialogService: DialogService,
    private platformConfigService: PlatformConfigService,
    public libServ: CommonLibService,
    private toastService: ToastService,
    private config: DynamicDialogConfig,
    public dynamicDialogRef: DynamicDialogRef,
    private fb: FormBuilder,
    private router: Router) {
    this.team = this.config['data']['team'];
  }
  ngOnInit() {

    this.teamRegisterForm = this.fb.group({
      team: ['', [Validators.required]]
    });
    this.defaultDimensions = [
      {
        label: 'Payout Resources',
        key: 'payout_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/common/filter`,
        enable: false,
        permissionNames: [
          'Publisher Payouts',
          'Frankly Payout',
          'Revenue Management 360',
          '3P Statements',
          'analysis_report',
          'Revenue Dashboard',
          'Finance Audit',
          'payment-contact-info'
        ],
        value: [
          {
            label: 'Station Group',
            filter_key: 'dim_station_group',
            key: 'station_group',
            config_json: null,
            value: 'Station Group'
          },
          {
            label: 'Station',
            filter_key: 'dim_payout_station',
            key: 'derived_station_rpt',
            config_json: null,
            value: 'Station'
          },
          {
            label: 'Source',
            filter_key: 'dim_payout_source',
            key: 'source',
            config_json: null,
            value: 'Source'
          }
        ]
      },
      {
        label: 'Revenue Resources',
        key: 'revenue_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/revmgmt/filter`,
        enable: false,
        permissionNames: ['Revenue Management'],
        value: [
          {
            label: 'Source',
            key: 'rev_source',
            filter_key: 'dim_rev_source',
            config_json: null,
            value: 'Rev Source'
          }
        ]
      },
      {
        label: 'Unfilled Resources',
        key: 'unfilled_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/unfilled-inventory/filter`,
        enable: false,
        permissionNames: [
          'Unfilled',
          'analysis_report',
          'Revenue Dashboard'
        ],
        value: [
          {
            label: 'Stations',
            key: 'station',
            filter_key: 'dim_unfilled_station',
            config_json: null,
            value: 'Unfilled Station'
          }
        ]
      },
      {
        label: 'Vast Resources',
        key: 'vast_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/vast-errors/filter`,
        enable: false,
        permissionNames: [
          'Vast Errors',
          'analysis_report',
          'Revenue Dashboard'
        ],
        value: [
          {
            label: 'Advertiser',
            key: 'advertiser',
            filter_key: 'dim_advertiser',
            config_json: null,
            value: 'Advertiser'
          }
        ]
      },
      {
        label: 'CPR Resources',
        key: 'cpr_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/campaign-progress-report/filter`,
        enable: false,
        permissionNames: ['CPR'],
        value: [
          {
            label: 'Order',
            key: 'order_name',
            filter_key: 'dim_order_name',
            config_json: null,
            value: 'Order'
          },
          {
            label: 'Line Item',
            key: 'lineitem',
            filter_key: 'dim_line_item',
            config_json: null,
            value: 'Line Item'
          }
        ]
      },
      {
        label: 'KPI Resources',
        key: 'kpi_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/daily-kpi-lite-app/filter`,
        enable: false,
        permissionNames: [
          'daily-kpi',
          'daily-kpi-lite'
        ],
        value: [
          {
            label: 'Property',
            key: 'derived_station_rpt',
            filter_key: 'dim_derived_station_rpt',
            config_json: null,
            value: []
          }
        ]
      },
      {
        label: 'Camapign Management Resources',
        key: 'campaign_resources',
        filterUrl: `${this.BASE_URL}/unified/v1/campaign-delivery-dashboard`,
        enable: false,
        permissionNames: [
          'Campaign Management'
        ],
        value: [
          {
            label: 'Advertiser',
            filter_key: 'advertisers',
            key: 'getAdvertiserList',
            config_json: null,
            value: []
          },
          {
            label: 'Connector',
            filter_key: 'connector',
            key: 'getConnectionList',
            config_json: null,
            value: 'Connector'
          },
        ]
      },
      {
        label: 'AMP Resources',
        key: 'amp_resources',
        filterUrl: `${this.BASE_URL}/unified/v1/digital-property-analytics/filter`,
        enable: false,
        permissionNames: [
          'digital-property-analytics'
        ],
        value: [
          {
            label: 'Publisher Name',
            filter_key: 'dim_publisher_name',
            key: 'publisher_name',
            config_json: null,
            value: []
          },
        ]
      },
      {
        label: 'Revenue Resources Freecycle',
        key: 'revenue_resources_fc',
        filterUrl: `${this.BASE_URL}/freecycle/v1/revmgmt-fc/filter`,
        enable: false,
        permissionNames: ['Revenue Management FreeCycle',
        'Analysis Report FreeCycle',
        'Revenue Dashboard FreeCycle'],
        value: [
          {
            label: 'Source',
            key: 'source',
            filter_key: 'dim_source',
            config_json: null,
            value: 'Source'
          }
        ]
      },
      {
        label: 'Unfilled Resources Freecycle',
        key: 'unfilled_resources_fc',
        filterUrl: `${this.BASE_URL}/freecycle/v1/unfilled-inventory-fc/filter`,
        enable: false,
        permissionNames: [
          'Unfilled Inventory FreeCycle',
          'Analysis Report FreeCycle',
          'Revenue Dashboard FreeCycle'
        ],
        value: [
          {
            label: 'Ad Unit',
            key: 'ad_unit',
            filter_key: 'dim_ad_unit',
            config_json: null,
            value: 'Ad unit'
          }
        ]
      },{
        label: 'Prebid Resources',
        key: 'prebid_publisher_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/prebid/filter`,
        enable: false,
        permissionNames: ['Prebid Analytics'],
        value: [
          {
            label: 'Publisher Name',
            key: 'publisher_name',
            filter_key: 'dim_prebid_publisher',
            config_json: null,
            value: 'Publisher Name'
          }
        ]
      },
      {
        label: 'ASBN Programmatic Revenue Resources',
        key: 'asbn_prog_rev_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/asbn-revmgmt/filter`,
        enable: false,
        permissionNames: [
          'Revenue Management Asbn'
        ],
        value: [
          {
            label: 'Source',
            filter_key: 'dim_prog_rev_source',
            key: 'source',
            config_json: null,
            value: 'Source'
          }
        ]
      },
      {
        label: 'ASBN Revenue Management Resources',
        key: 'asbn_rev_360_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/asbn-rev360/filter`,
        enable: false,
        permissionNames: [
          'Revenue Management 360 Asbn'
        ],
        value: [
          {
            label: 'Station Group',
            filter_key: 'dim_rev_360_media_group',
            key: 'media_group',
            config_json: null,
            value: []
          },
          {
            label: 'Station',
            filter_key: 'dim_rev_360_property',
            key: 'property',
            config_json: null,
            value: []
          },
          {
            label: 'Source',
            filter_key: 'dim_rev_360_source',
            key: 'source',
            config_json: null,
            value: []
          }
        ]
      }
    ];

    this.originalResources = JSON.parse(
      this.team['resources']
    );

    this.defaultDimensions.forEach(dim => {
      dim.value.forEach(element => {
        this.originalResources.forEach(resource => {
          if (resource.key == element.filter_key) {
            element.value = resource.value;
          }
        });
      });

    });


    console.log('value', this.defaultDimensions);
    this.availableDimensions = this.libServ.deepCopy(this.defaultDimensions);
    this.platConfigObs = this.roleServ.getRoleDetails(this.team['relatedRoleId']).subscribe(res => {

      // console.log('team.......', this.team)
      this.groupedApps = res['data']['permissions']
        .filter(e => e.permissionType === 'Application')
        .map(e => {
          const app = this.libServ.deepCopy(e);
          app['id'] = app['appId'];
          this.availableDimensions.forEach(element => {
            element['permissionNames'].includes(e.name) ? element['enable'] = true : '';
          });
          return app;
        });
    });
    this.formTeamModel.teamName = this.team['name'];
    console.log('this.formTeamModel', this.formTeamModel);
  }

  submitHandler() {
    const resources = [];
    console.log('this.formTeamModel.selectedDims', this.formTeamModel.selectedDims);
    for (const key of Object.keys(this.formTeamModel.selectedDims)) {
      const defaultD = this.availableDimensions.find(grp => grp.key === key);
      defaultD.value.forEach(valueObj => {
        const obj = {
          key: valueObj['filter_key'],
          display_name: valueObj['label'],
          value: this.formTeamModel.selectedDims[key][valueObj.filter_key]
        };
        resources.push(obj);
      });
    }
    // console.log('this.selectedPermissions11', this.selectedPermissions);
    // this.formTeamModel.selectedDims[''] = [];
    const removeResources = {};

    // console.log('this.formTeamModel', resources, this.originalResources);

    this.originalResources.forEach(resource => {
      const dimValues = [];
      resources.forEach(newResource => {
        if (resource.key == newResource.key) {
          resource.value.forEach(element => {
            if (!(newResource.value.some(x => x == element))) {
              dimValues.push(element);
            }
          });
        }
      });
      if (dimValues.length > 0) {
        removeResources[resource.key] = dimValues;
      }
    });
    console.log('removeResources', removeResources);

    const reqData = {
      name: this.formTeamModel.teamName,
      resources: JSON.stringify(resources)
    };

    if (!(this.libServ.isEmptyObj(removeResources))) {
      this.teamServ
        .getUsersByTeam(this.team['id'])
        .subscribe(res1 => {
          const userData = res1 as [];

          if (userData.length > 0) {
            const data = {
              data: userData
            };

            const ref = this.dialogService.open(ImpactedUserComponent, {
              header: 'May Be Impacted User List',
              contentStyle: {
                'min-height': '65vh',
                'max-height': '65vh',
                width: '60vw',
                overflow: 'auto'
              },
              data: data
            });
            ref.onClose.subscribe((data: string) => {
              console.log('data', data);
              if (data) {
                const reqUpdateData = {
                  // removepermission: removeResources.map(x => x.id),
                  removepermissionsDetails: removeResources
                };
                this.teamServ.updateImpactedTeam(this.team['id'], reqUpdateData).subscribe(res => {
                  this.teamServ
                    .updateTeam(this.team['id'], reqData)
                    .subscribe(res => {
                      if (res['status'] === 0) {
                        // user.status = !user.status;
                        this.toastService.displayToast({
                          severity: 'error',
                          summary: 'Server Error',
                          detail: 'Please refresh the page'
                        });
                        console.log(res['status_msg']);
                        return;
                      } else {
                        this.router.navigate(['/teams']);
                        this.toastService.displayToast({
                          severity: 'success',
                          summary: 'Team Updated',
                          detail: 'Team updated successfully'
                        });
                      }
                      this.dynamicDialogRef.close(null);
                      // this.dialogService.dialogComponentRef.destroy();
                    });
                });
              } else {

              }

            });
          } else {


            const reqUpdateData = {
              // removepermission: removeResources.map(x => x.id),
              removepermissionsDetails: removeResources
            };
            this.teamServ.updateImpactedTeam(this.team['id'], reqUpdateData).subscribe(res => {
              this.teamServ
                .updateTeam(this.team['id'], reqData)
                .subscribe(res => {
                  if (res['status'] === 0) {
                    // user.status = !user.status;
                    this.toastService.displayToast({
                      severity: 'error',
                      summary: 'Server Error',
                      detail: 'Please refresh the page'
                    });
                    console.log(res['status_msg']);
                    return;
                  } else {
                    this.router.navigate(['/teams']);
                    this.toastService.displayToast({
                      severity: 'success',
                      summary: 'Team Updated',
                      detail: 'Team updated successfully'
                    });
                  }
                  this.dynamicDialogRef.close(null);
                  // this.dialogService.dialogComponentRef.destroy();
                });
            });

          }
        });
    } else {
      this.teamServ.updateTeam(this.team['id'], reqData)
        .subscribe(res1 => {
          if (res1['status']) {

            // this.role['name'] = this.roleName;
            // this.role['description'] = this.description
            this.router.navigate(['/teams']);
            // this.dialogService.dialogComponentRef.destroy();
            this.dynamicDialogRef.close(null);
            this.toastService.displayToast({
              severity: 'success',
              summary: 'Team Updated',
              detail: 'Team updated successfully'
            });
          } else {

            this.toastService.displayToast({
              severity: 'error',
              summary: 'Something went wrong',
              detail: 'Error updating Role'
            });
          }
        });

    }

  }

  validateTeamAlreadyExist(teamControl) {
    console.log("team   :", teamControl.value)
    this.isTeamExists = false;
    if (teamControl.value && this.team['name'].toLowerCase() != teamControl.value.toLowerCase()) {
      this.teamServ.teamAlreadyExist(teamControl.value).subscribe(result => {
        if (result) {
          this.isTeamExists = true;
        }
        else {
          this.isTeamExists = false;
        }
      })
    }
  }


  ngOnDestroy(): void {
    if (this.platConfigObs && !this.platConfigObs.closed) {
      this.platConfigObs.unsubscribe();
    }
  }

  goBack() {
    this.dynamicDialogRef.close(null);
  }
}
