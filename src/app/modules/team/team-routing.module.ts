import { Routes } from '@angular/router';
import { ListTeamsComponent } from './list-teams/list-teams.component';

export const TeamRoutingModule: Routes = [
  {
    path: '',
    component: ListTeamsComponent
  }
];
