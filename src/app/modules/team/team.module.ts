import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListTeamsComponent } from './list-teams/list-teams.component';
import { TeamRoutingModule } from './team-routing.module';
import { TableModule } from 'primeng/table';
import { RouterModule } from '@angular/router';
import {EditTeamComponent} from './edit-team/edit-team.component';

@NgModule({
  declarations: [ListTeamsComponent],
  imports: [
    CommonModule,
    TableModule,
    RouterModule.forChild(TeamRoutingModule)
  ]
})
export class TeamModule { }
