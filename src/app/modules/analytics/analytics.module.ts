import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AnalyticsRoutingModule } from './analytics-routing.module';
import { AnalyticsComponent } from './analytics.component';
import { TabViewModule } from 'primeng/tabview';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { SharedModule } from 'src/app/_pipes/shared.module';
import { ChartsModule } from '../common/charts/charts.module';
import { FilterContainerModule } from '../common/filter-container/filter-container.module';
import { CardsModule } from '../common/cards/cards.module';
import {CardModule} from 'primeng/card';
import {TableModule} from 'primeng/table';
import { InputSwitchModule } from 'primeng/inputswitch';
import { FormsModule } from '@angular/forms';
import {SelectButtonModule} from 'primeng/selectbutton';

@NgModule({
  declarations: [AnalyticsComponent],
  imports: [
    CommonModule,
    AnalyticsRoutingModule,
    TabViewModule,
    SharedModule,
    ChartsModule,
    CardModule,
    FilterContainerModule,
    CardsModule,
    TableModule,
    FormsModule,
    InputSwitchModule,
    SelectButtonModule
  ],
  providers: [FormatNumPipe]
})
export class AnalyticsModule { }
