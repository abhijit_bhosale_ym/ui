import { Component, OnInit, ViewChild } from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { FetchApiDataService } from '../fetch-api-data.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import * as moment from 'moment';
import * as ChartGeo from 'chartjs-chart-geo';
import 'chartjs-chart-matrix';
import { TabView } from 'primeng/tabview';
import { Table } from 'primeng/table';
import { DynamicDialogConfig } from 'primeng';

@Component({
  selector: 'ym-analytics-daily-data-popup',
  templateUrl: './analytics-daily-data-popup.component.html',
  styleUrls: ['./analytics-daily-data-popup.component.scss']
})
export class AnalyticsDailyDataPopupComponent implements OnInit {
  public cardsJson = [];
  public mainLineChartJson: object;
  public showMainLineChart = false;
  public noDataMainLineChart = false;
  private requestParam: object;
  rowData = {};
  public showAcquireUserChart = false;
  public noDataAcquireUserChart = true;
  public acquireUserChartJson: object;
  public pageViewCounts = [];
  public pageViewCols = [];
  public noTableData = true;
  public showGeography = true;
  public showTrafficRefferel = true;

  flatTableData = [];
  flatTableColumnDef: any[];
  flatTableJson: Object;

  trafficTableData = [];
  trafficTableColDef: any[];
  trafficTableJson: Object;

  geoTableData = [];
  geoTableColDef: any[];
  geoTableJson: Object;

  public showGeoSessionChart = false;
  public noDataGeoSessionChart = true;
  public geoSessionChartJson: object;

  @ViewChild('dt') aggTableRef: Table;
  @ViewChild('audienceTabView') audienceTabView: TabView;
  @ViewChild('acquireUserTabView') acquireUserTabView: TabView;

  constructor(
    private libServ: CommonLibService,
    private formatNumPipe: FormatNumPipe,
    private dataFetchServ: FetchApiDataService,
    private toastService: ToastService,
    private config: DynamicDialogConfig,
  ) {
    this.rowData = this.config.data;
  }

  ngOnInit() {
    this.cardsJson = [
      {
        field: 'users',
        displayName: 'Users',
        value: 0,
        imageClass: 'fas fa-eye',
        format: 'number',
        config: [],
        color: '#5cb85c',
        infoTitle:''
      },
      {
        field: 'sessions',
        displayName: 'Sessions',
        value: 0,
        imageClass: 'fas fa-hand-holding-usd',
        format: 'number',
        config: [],
        color: '#5bc0de',
        infoTitle:''
      },
      {
        field: 'avg_session_duration',
        displayName: 'Avg. Session Duration',
        value: 0,
        imageClass: 'fas fa-dollar-sign',
        format: 'secToMins',
        config: [],
        color: '#FFDC00',
        infoTitle:'Formula : Average(Activity End Time - Activity Start Time)'
      },
      // {
      //   field: 'bounce_rate',
      //   displayName: 'Bounce Rate',
      //   value: 0,
      //   format: 'percentage',
      //   config: [2],
      //   color: '#fff',
      //   imageClass: '',
      //   infoTitle:'Formula : Count(Unique Page Views) / Count(Sessions)'
      // }
    ];

    this.flatTableColumnDef = [
      {
        field: 'time_key',
        displayName: 'Date',
        format: 'date',
        width: '175',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'user_count',
        displayName: 'Users',
        format: 'number',
        width: '175',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'session_count',
        displayName: 'Sessions',
        format: 'number',
        width: '175',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'avg_session_duration',
        displayName: 'Avg. Session Duration',
        format: 'secToMins',
        width: '175',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: true
        }
      },
      // {
      //   field: 'bounce_rate',
      //   displayName: 'Bounce Rate',
      //   format: 'percentage',
      //   width: '175',
      //   exportConfig: {
      //     format: '',
      //     styleinfo: {
      //       thead: 'default',
      //       tdata: 'white'
      //     }
      //   },
      //   formatConfig: [],
      //   options: {
      //     editable: false,
      //     colSearch: false,
      //     colSort: false,
      //     resizable: true,
      //     movable: true
      //   }
      // }
    ];
    this.flatTableJson = {
      page_size: 10,
      page: 0,
      lazy: false,
      loading: true,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '300px',
      totalRecords: 1000,
      columns: this.flatTableColumnDef,
      selectedColumns: this.flatTableColumnDef,
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: false,
      columnSearch: false
    };

    this.trafficTableColDef = [
      {
        field: 'time_key',
        displayName: 'Date',
        format: 'date',
        width: '175',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'refferal_channel',
        displayName: 'Traffic Channel',
        format: '',
        width: '175',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'user_count',
        displayName: 'Users',
        format: '',
        width: '80',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: true
        }
      },
    ];
    this.trafficTableJson = {
      page_size: 10,
      page: 1,
      lazy: true,
      loading: true,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '200px',
      totalRecords: 1000,
      columns: this.trafficTableColDef,
      selectedColumns: this.trafficTableColDef,
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: false,
      columnSearch: false
    };

    this.geoTableColDef = [
      {
        field: 'country_name',
        displayName: 'Country',
        format: '',
        width: '175',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'session_count',
        displayName: 'Session Count',
        format: '',
        width: '175',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: true
        }
      }
    ];
    this.geoTableJson = {
      page_size: 10,
      page: 1,
      lazy: true,
      loading: true,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '200px',
      totalRecords: 1000,
      columns: this.geoTableColDef,
      selectedColumns: this.geoTableColDef,
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: false,
      columnSearch: false
    };

    this.requestParam = {
      dimensions: [],
      metrics: [],
      derived_metrics: [],
      timeKeyFilter: {},
      filters: { dimensions: [], metrics: [] },
      groupByTimeKey: { key: [], interval: 'daily' },
      gidGroupBy: '',
      orderBy: [
        { key: 'time_key', opcode: 'asc' }
      ],
      limit: '',
      offset: '',
      isCount: false
    };
    this.defaultDataLoad();
  }

  defaultDataLoad() {
    const requestAudience = Object.assign({}, this.requestParam);
    requestAudience['metrics'] = [
      'user_count',
      'session_count',
      'bounce_rate'
    ];
    requestAudience['derived_metrics'] = ['avg_session_duration'];
    requestAudience['dimensions'] = ['time_key'];
    requestAudience['timeKeyFilter'] = this.rowData['timeKeyFilter'];
    requestAudience['filters'] = {
      dimensions: [
        { key: 'decoded_page_url', values: [this.rowData['rowData']['decoded_page_url']] },
        { key: 'library_name', values: ['analytics-web'] }],
      metrics: []
    }
    if(this.rowData['toggleBotStatus'] !== null)
      requestAudience['filters']['dimensions'].push({key: 'bot_status', values: [this.rowData['toggleBotStatus'] ? '1' : '0'] });
    requestAudience['gidGroupBy'] = [];
    requestAudience['groupByTimeKey'] = { key: ['time_key'], interval: 'daily' };
    requestAudience['orderBy'] = [{ key: 'time_key', opcode: 'asc' }];
    this.getAudienceOverview(requestAudience);
  }

  getAudienceOverview(request) {
    this.showMainLineChart = false;
    this.noDataMainLineChart = false;
    const colors = this.libServ.dynamicColors(2);
    this.mainLineChartJson = {
      chartTypes: [{ key: 'bar', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'Users',
            type: 'line',
            yAxisID: 'y-axis-0',

            borderColor: colors[0],
            fill: false,
            backgroundColor: colors[0],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: false,
          text: 'Users'
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: 'Users'
              },
              position: 'left',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, 'number', []);
                }
              }
              // scaleFontColor: "rgba(151,137,200,0.8)"
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };
    this.flatTableJson['loading'] = true;
    this.dataFetchServ.getTableData(request).subscribe(response => {
      if (response['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(response['status_msg']);
        return;
      }
      const chartData = response['data'];
      if (typeof chartData === 'undefined' || !chartData.length) {
        this.noDataMainLineChart = true;
      } else {
        const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));

        this.mainLineChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );
        const usersArr = [];
        const sessionsArr = [];
        const sessionDurationArr = [];
        const bounceRateArr = [];
        datesArr.forEach(time_key => {
          chartData.forEach(r => {
            if (r['time_key'] === time_key) {
              usersArr.push(r['user_count']);
              sessionsArr.push(r['session_count']);
              sessionDurationArr.push(r['avg_session_duration']);
              bounceRateArr.push(r['bounce_rate']);
            }
          });
        });
        this.mainLineChartJson['dataObj'] = {
          'users': usersArr,
          'sessions': sessionsArr,
          'avg_session_duration': sessionDurationArr,
          'bounce_rate': bounceRateArr
        };

        this.mainLineChartJson['chartData']['datasets'][0]['data'] = usersArr;
        setTimeout(() => {
          for (const t of this.audienceTabView.tabs) {
            if (t.selected) {
              t.selected = false;
            }
          }
          this.audienceTabView.tabs[0].selected = true;
          this.showMainLineChart = true;
          this.noDataMainLineChart = false;
        }, 0);
        this.getDailyPageUrl(chartData);
      }
    });
  }

  audienceTabChange(e) {
    const currentTab = this.cardsJson[e.index];
    this.showMainLineChart = false;
    this.noDataMainLineChart = true;
    setTimeout(() => {
      this.mainLineChartJson['chartData']['datasets'][0]['label'] = currentTab.displayName === 'Session Duration' ? 'Avg. Session Duration' : currentTab.displayName;
      this.mainLineChartJson['chartOptions']['title']['text'] = currentTab.displayName === 'Session Duration' ? 'Avg. Session Duration' : currentTab.displayName;
      this.mainLineChartJson['chartOptions']['scales']['yAxes'][0]['scaleLabel']['labelString'] = currentTab.displayName === 'Session Duration' ? 'Avg. Session Duration' : currentTab.displayName;
      this.mainLineChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
        callback: (value, index, values) => {
          return this.formatNumPipe.transform(
            currentTab.format === 'percentage' ? value : value,
            currentTab.format,
            currentTab.config
          );
        }
      };
      this.mainLineChartJson['chartOptions']['tooltips']['callbacks'] = {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return `${
            data.datasets[tooltipItem.datasetIndex].label
            } : ${
            this.formatNumPipe.transform(
              currentTab.format === 'percentage' ? currentValue : currentValue,
              currentTab.format,
              currentTab.config
            )
            }`;
        }
      };
      this.mainLineChartJson['chartData']['datasets'][0]['data'] = typeof this.mainLineChartJson['dataObj'] !== 'undefined'
        ? this.mainLineChartJson['dataObj'][currentTab['field']]
        : [];
      this.noDataMainLineChart = false;
      this.showMainLineChart = true;
    }, 0);
  }

  mainTabChange(e) {
    if (e.index === 0) {
      const requestAudience = Object.assign({}, this.requestParam);
      requestAudience['metrics'] = [
        'user_count',
        'session_count',
        'bounce_rate'
      ];
      requestAudience['derived_metrics'] = ['avg_session_duration'];
      requestAudience['dimensions'] = ['time_key'];
      requestAudience['timeKeyFilter'] = this.rowData['timeKeyFilter'];
      requestAudience['filters'] = { dimensions: [{ key: "decoded_page_url", values: [this.rowData['rowData']['decoded_page_url']] }], metrics: [] }
      requestAudience['gidGroupBy'] = [];
      requestAudience['groupByTimeKey'] = { key: ['time_key'], interval: 'daily' };
      requestAudience['orderBy'] = [{ key: 'time_key', opcode: 'asc' }];
      this.getAudienceOverview(requestAudience);
    } else if (e.index === 1) {
      const requestAquiredUser = Object.assign({}, this.requestParam);
      requestAquiredUser['dimensions'] = ['refferal_channel', 'time_key'];
      requestAquiredUser['metrics'] = ['user_count'];
      requestAquiredUser['timeKeyFilter'] = this.rowData['timeKeyFilter'];
      requestAquiredUser['filters'] = { dimensions: [{ key: "decoded_page_url", values: [this.rowData['rowData']['decoded_page_url']] }], metrics: [] }
      requestAquiredUser['gidGroupBy'] = ['refferal_channel', 'time_key'];
      requestAquiredUser['groupByTimeKey'] = { key: [], interval: 'daily' };
      requestAquiredUser['orderBy'] = [{ key: 'time_key', opcode: 'asc' }];
      this.getAcquiredUsersOverview(requestAquiredUser, 0);
    } else if (e.index === 2) {
      const requestGeoMap = Object.assign({}, this.requestParam);
      requestGeoMap['metrics'] = ['session_count'];
      requestGeoMap['dimensions'] = ['country_name'];
      requestGeoMap['timeKeyFilter'] = this.rowData['timeKeyFilter'];
      requestGeoMap['filters'] = { dimensions: [{ key: "decoded_page_url", values: [this.rowData['rowData']['decoded_page_url']] }], metrics: [] }
      requestGeoMap['gidGroupBy'] = ['country'];
      requestGeoMap['groupByTimeKey'] = { key: [], interval: 'daily' };
      requestGeoMap['orderBy'] = [{ key: 'session_count', opcode: 'desc' }];
      this.getGeoMap(requestGeoMap);
    }
  }

  acquireUserTabChange(e) {
    if (e.index === 0) {
      this.trafficTableColDef.splice(1, 2,
        {
          field: 'refferal_channel',
          displayName: 'Traffic Channel',
          format: '',
          width: '175',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: false,
            resizable: true,
            movable: true
          }
        },
        {
          field: 'user_count',
          displayName: 'Users',
          format: '',
          width: '80',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: false,
            resizable: true,
            movable: true
          }
        }
      )
      const requestAquiredUser = Object.assign({}, this.requestParam);
      requestAquiredUser['dimensions'] = ['refferal_channel', 'time_key'];
      requestAquiredUser['metrics'] = ['user_count'];
      requestAquiredUser['timeKeyFilter'] = this.rowData['timeKeyFilter'];
      requestAquiredUser['filters'] = { dimensions: [{ key: "decoded_page_url", values: [this.rowData['rowData']['decoded_page_url']] }], metrics: [] }
      requestAquiredUser['gidGroupBy'] = ['refferal_channel', 'time_key'];
      requestAquiredUser['groupByTimeKey'] = { key: [], interval: 'daily' };
      requestAquiredUser['orderBy'] = [{ key: 'time_key', opcode: 'asc' }];
      this.getAcquiredUsersOverview(requestAquiredUser, 0);
    } else if (e.index === 1) {
      this.trafficTableColDef.splice(1, 2,
        {
          field: 'referer',
          displayName: 'Refferel URL',
          format: '',
          width: '175',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: false,
            resizable: true,
            movable: true
          }
        },
        {
          field: 'page_views',
          displayName: 'Page Views',
          format: '',
          width: '80',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: false,
            resizable: true,
            movable: true
          }
        })
      const requestAquiredUser = Object.assign({}, this.requestParam);
      requestAquiredUser['dimensions'] = ['referer', 'time_key'];
      requestAquiredUser['metrics'] = ['page_views'];
      requestAquiredUser['timeKeyFilter'] = this.rowData['timeKeyFilter'];
      requestAquiredUser['filters'] = { dimensions: [{ key: "decoded_page_url", values: [this.rowData['rowData']['decoded_page_url']] }], metrics: [] }
      requestAquiredUser['gidGroupBy'] = ['referer', 'time_key'];
      requestAquiredUser['groupByTimeKey'] = { key: [], interval: 'daily' };
      requestAquiredUser['orderBy'] = [{ key: 'time_key', opcode: 'asc' }];

      this.getAcquiredUsersOverview(requestAquiredUser, 1);
    }
  }

  getDailyPageUrl(chartData) {
    this.noTableData = true;
    this.flatTableData = chartData;
    this.flatTableJson['totalRecords'] = chartData.length;
    setTimeout(() => {
      this.flatTableJson['loading'] = false;
      this.noTableData = false;
    },0);
  }

  getDailyTrafficRefferel(chartData) {
    this.showTrafficRefferel = true;
    this.trafficTableData = chartData;
    this.trafficTableJson['totalRecords'] = chartData.length;
    setTimeout(() => {
      this.trafficTableJson['loading'] = false;
      this.showTrafficRefferel = false;
    });
  }

  getDailyGeography(chartData) {
    this.showGeography = true;
    this.geoTableData = chartData;
    this.geoTableJson['totalRecords'] = chartData.length;
    setTimeout(() => {
      this.geoTableJson['loading'] = false;
      this.showGeography = false;
    });
  }

  getAcquiredUsersOverview(request, idx) {
    this.showAcquireUserChart = false;
    this.noDataAcquireUserChart = false;
    this.acquireUserChartJson = {
      chartTypes: [{ key: 'bar', label: 'Traffic Channel Chart', stacked: true }],
      chartData: {
        labels: [],
        datasets: []
      },
      chartOptions: {
        title: {
          display: false,
          text: 'How do you acquire users?'
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: 'Users'
              },
              position: 'left',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '', []);
                }
              }
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              if (currentValue) {
                return `${
                  data.datasets[tooltipItem.datasetIndex].label
                  } : ${this.formatNumPipe.transform(currentValue, '', [])}`;
              } else {
                return null;
              }
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '311px'
    };
    this.trafficTableJson['loading'] = true;
    this.dataFetchServ.getTableData(request).subscribe(response => {

      if (response['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(response['status_msg']);
        return;
      }

      const chartData = response['data'];
      if (typeof chartData === 'undefined' || !chartData.length) {
        this.noDataAcquireUserChart = true;
      } else {
        this.acquireUserChartJson['chartTypes'] = [{ key: 'bar', label: 'Traffic Channel Chart', stacked: true }];
        this.acquireUserChartJson['chartOptions']['title']['text'] = 'How do you acquire users?'

        this.getDailyTrafficRefferel(chartData);
        const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));

        this.acquireUserChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('YYYY-MM-DD')
        );
        this.acquireUserChartJson['chartData']['datasets'] = [];
        for (const t of this.acquireUserTabView.tabs) {
          if (t.selected) {
            t.selected = false;
          }
        }
        this.acquireUserTabView.tabs[idx].selected = true;
        if (idx === 0) {
          const sourcesJson = Array.from(new Set(chartData.map(r => r['refferal_channel'])));
          const colors = this.libServ.dynamicColors(sourcesJson.length);

          sourcesJson.forEach((src, i) => {
            const pageViewsArr = [];
            datesArr.forEach((time_key, i2) => {
              let value = null;
              chartData.forEach((r, i1) => {
                if (r['refferal_channel'] === src && r['time_key'] === time_key) {
                  value = r['user_count']
                }
              });
              pageViewsArr.push(value);
            });
            this.acquireUserChartJson['chartData']['datasets'].push({
              label: src,
              type: 'bar',
              data: pageViewsArr,
              borderColor: colors[i],
              fill: false,
              backgroundColor: colors[i]
            });
          });
          this.acquireUserChartJson['note'] = null;
        } else {
          const sourcesJson = Array.from(new Set(chartData.map(r => r['referer'])));
          const colors = this.libServ.dynamicColors(sourcesJson.length);

          sourcesJson.forEach((src, i) => {
            const pageViewsArr = [];
            datesArr.forEach(time_key => {
              let value = null;
              chartData.forEach(r => {
                if (r['time_key'] === time_key && r['referer'] === src) {
                  value = r['page_views'];
                }
              });
              pageViewsArr.push(value);
            });
            this.acquireUserChartJson['chartData']['datasets'].push({
              label: src,
              type: 'bar',
              data: pageViewsArr,
              borderColor: colors[i],
              fill: false,
              backgroundColor: colors[i]
            });
          });
          this.acquireUserChartJson['note'] = { txt: 'Showing Top 5 Referrals by Page Views' };
        }
        this.showAcquireUserChart = true;
        this.noDataAcquireUserChart = false;
      }
    });
  }

  getGeoMap(request) {
    this.dataFetchServ.getGeoContriesData().subscribe(data => {
      const countries = ChartGeo.topojson.feature(data, data['objects']['countries']).features;
      this.geoSessionChartJson = {
        chartTypes: [{ key: 'choropleth', label: 'Sessions by Country' }],
        chartData: {
          labels: [],
          datasets: [{
            label: 'Countries',
            data: [],
          }]
        },
        chartOptions: {
          title: {
            display: false,
            text: 'Sessions by Country'
          },
          showOutline: true,
          showGraticule: true,
          legend: {
            display: false
          },
          scale: {
            projection: 'equalEarth'
          },
          geo: {
            colorScale: {
              display: true,
            },
          },
          plugins: {}
        },
        zoomLabel: false,
        chartWidth: '',
        chartHeight: '443px'
      };

      this.showGeoSessionChart = false;
      this.noDataGeoSessionChart = false;
      this.geoTableJson['loading'] = true;
      this.dataFetchServ.getTableData(request).subscribe(response => {
        if (response['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(response['status_msg']);
          return;
        }
        if (!this.libServ.isEmptyObj(response['data'])) {
          const chartData = response['data'];
          this.getDailyGeography(chartData);
          if (typeof chartData === 'undefined' || !chartData.length) {
            this.noDataGeoSessionChart = true;
          } else {

            const session = [];
            const label = [];
            countries.forEach(d => {
              chartData.forEach(element => {
                if (d.properties.name === element['country_name']) {
                  label.push(element['country_name']);
                  session.push({
                    feature: d,
                    value: element['session_count']
                  });
                }
              });
            });
            this.geoSessionChartJson['chartData']['labels'] = label;
            this.geoSessionChartJson['chartData']['datasets'][0]['data'] = session;

            this.showGeoSessionChart = true;
            this.noDataGeoSessionChart = false;
          }
        }

      });
    });
  }

}
