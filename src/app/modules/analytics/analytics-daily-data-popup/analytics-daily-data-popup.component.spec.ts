import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalyticsDailyDataPopupComponent } from './analytics-daily-data-popup.component';

describe('AnalyticsDailyDataPopupComponent', () => {
  let component: AnalyticsDailyDataPopupComponent;
  let fixture: ComponentFixture<AnalyticsDailyDataPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnalyticsDailyDataPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalyticsDailyDataPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
