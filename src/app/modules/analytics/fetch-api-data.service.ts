import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FetchApiDataService {
  private BASE_URL: string = environment.baseUrl;

  constructor(private http: HttpClient) {}

  getLastUpdatedData(appId) {
    const url = `${this.BASE_URL}/frankly/v1/common/getLastUpdatedData/${appId}`;
    return this.http.get(url, { responseType: 'text' });
  }

  getAudienceMgmtData(params: object) {
    const url = `${this.BASE_URL}/dmp/v1/analytics/getUserSessionData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
  }

  getUserByTimeData(params: object) {
    const url = `${this.BASE_URL}/dmp/v1/analytics/getUserByTime`;
    return this.http.post(url, params);
  }

  getDailyTraficSource(params: object, idx: number) {
    const url = `${this.BASE_URL}/dmp/v1/analytics/getDailyTraficSource?idx=${idx}`;
    return this.http.post(url, params);
  }

  getDeviceSessions(params: object) {
    const url = `${this.BASE_URL}/dmp/v1/analytics/getDeviceSessions`;
    return this.http.post(url, params);
  }

  getJsonCoOrdinates() {
    const url = `${this.BASE_URL}/dmp/v1/audience/getJsonCoOrdinates`;
    return this.http.get(url);
  }

  getGeoData(params: object) {
    const url = `${this.BASE_URL}/dmp/v1/analytics/getGeoData`;
    return this.http.post(url, params);
  }

  getGeoContriesData() {
    return this.http.get('../../../assets/json/countries-110m.json');
  }

  getTableData(params: object) {
    const url = `${this.BASE_URL}/unified/v1/digital-property-analytics/getData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
  }

  getAudienceMgmtAndroidData(requestAndroidAudience: {} & object) {
    const url = `${this.BASE_URL}/dmp/v1/analytics/getAudienceMgmtAndroidData`;
    return this.http.post(url, requestAndroidAudience);
  }

  getAndroidGeoData(requestAndroidGeoMap: {} & object) {
    const url = `${this.BASE_URL}/dmp/v1/analytics/getAndroidGeoData`;
    return this.http.post(url, requestAndroidGeoMap);
  }

  getAndroidScreenViewCount(params: {}) {
    const url = `${this.BASE_URL}/dmp/v1/analytics/getAndroidScreenViewCount`;
    return this.http.post(url, params);
  }

  getAndroidEventNamesAndCount(params: {} & object) {
    const url = `${this.BASE_URL}/dmp/v1/analytics/getAndroidEventNamesAndCount`;
    return this.http.post(url, params);
  }
}
