import { Component, OnInit, OnDestroy, QueryList, ElementRef, ViewChildren, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { FetchApiDataService } from './fetch-api-data.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { CommonLibService } from 'src/app/_services';
import { Subscription } from 'rxjs';
import * as moment from 'moment-timezone';
import * as ChartGeo from 'chartjs-chart-geo';
import 'chartjs-chart-matrix';
import { TabView } from 'primeng/tabview';
import { Table } from 'primeng/table';
import { AnalyticsDailyDataPopupComponent } from "./analytics-daily-data-popup/analytics-daily-data-popup.component"
import { DialogService } from 'primeng';

@Component({
  selector: 'ym-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.scss']
})
export class AnalyticsComponent implements OnInit, OnDestroy {
  timezone: string;
  private appConfigObs: Subscription;
  private requestParam: object;
  private filtersApplied: object;
  public appConfig: object = {};
  public mainLineChartJson: object;
  public showMainLineChart = false;
  public noDataMainLineChart = false;
  public showDevicePiaChart = false;
  public noDataDevicePiaChart = true;
  public devicePiaChartJson: object;
  public showAcquireUserChart = false;
  public noDataAcquireUserChart = true;
  public acquireUserChartJson: object;
  public nextUpdated: Date;
  public dataUpdatedThrough: Date;
  public lastUpdatedOn: Date;
  public cardsJson = [];
  public showCards = true;
  public pageViewCounts = [];
  public pageViewCols = [];

  public showGeoSessionChart = false;
  public noDataGeoSessionChart = true;
  public geoSessionChartJson: object;

  /** Android Dashlets Config Start*/
  public cardsJsonAndroid = [];
  public mainLineAndroidChartJson: object;
  public showMainLineAndroidChart = false;
  public noDataMainLineAndroidChart = false;

  public pageViewAndroidCounts = [];
  public pageViewAndroidCols = [];

  public showGeoSessionAndroidChart = false;
  public noDataGeoSessionAndroidChart = true;
  public geoSessionAndroidChartJson: object;

  public noTableAndroidData = true;
  botStatusOpt : any = [{'label' : 'Bot', 'value' : true},{'label' : 'ALL','value' : null},{'label' : 'Non Bot','value' : false}]



  noEventTableData: boolean;
  eventTableCols: object;
  eventTableCounts: any;
  @ViewChild('dataTableAndroid') dataTableAndroid: Table;
  @ViewChild('audienceAndroidTabView') audienceAndroidTabView: TabView;
  @ViewChild('eventTable') eventTable: Table;
  /** Android Dashlets Config End*/

  public showUserByTimeChart = false;
  public noDataUserByTimeChart = true;
  public userByTimeChartJson: object;

  public showUserRetentionChart = false;
  public noDataUserRetentionChart = true;
  public userRetentionChartJson: object;

  public activeUserChartJson: object;
  public noDataActiveUserChart = true;
  public showActiveUserChart = false;
  toggleBotStatus = false;
  public noTableData = true;
  pageViewJson: object;
  eventTableJson: object;
  pageViewAndroidJson: object;
  first = 0;
  rows = 10;
  dateDifff: any;

  @ViewChild('dt') aggTableRef: Table;
  @ViewChild('audienceTabView') audienceTabView: TabView;
  @ViewChild('acquireUserTabView') acquireUserTabView: TabView;


  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private formatNumPipe: FormatNumPipe,
    private toastService: ToastService,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService
  ) { }

  ngOnInit(): void {
    this.toggleBotStatus = false;
    this.pageViewCols = [
      { field: 'decoded_page_url', header: 'URL', width: 'auto', format: '', formatConfig: [] },
      { field: 'page_views', header: 'Page Views', width: '130px', format: 'number', formatConfig: [] },
      { field: 'user_count', header: 'Users', width: '130px', format: 'number', formatConfig: [] },
      { field: 'session_count', header: 'Sessions', width: '130px', format: 'number', formatConfig: [] },
      { field: 'session_duration', header: 'Session Duration', width: '150px', format: 'secToMins', formatConfig: [] },
      // { field: 'bounce_rate', header: 'Bounce Rate', width: '140px', format: 'percentage', formatConfig: [2] }
      // { field: 'page_views', header: 'GEO', width: '140px', format: 'number', formatConfig: [] }
    ];

    this.eventTableCols = [
      { field: 'event_name', header: 'Event Name', width: 'auto', format: '', formatConfig: [] },
      { field: 'event_count', header: 'Event Count', width: '140px', format: 'number', formatConfig: [] },
    ];

    this.pageViewAndroidCols = [
      { field: 'page_path', header: 'Page Path', width: 'auto', format: '', formatConfig: [] },
      { field: 'page_views', header: 'Page Views', width: '110px', format: 'number', formatConfig: [] },
      { field: 'user_count', header: 'Users', width: '110px', format: 'number', formatConfig: [] },
      { field: 'session_count', header: 'Sessions', width: '110px', format: 'number', formatConfig: [] },
      { field: 'session_duration', header: 'Session Duration', width: '160px', format: 'secToMins', formatConfig: [] },
      // { field: 'bounce_rate', header: 'Bounce Rate', width: '120px', format: 'percentage', formatConfig: [2] }
    ];

    this.pageViewJson = {
      page_size: 10,
      page: 0,
      lazy: false,
      loading: true,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '400px',
      totalRecords: 1000,
      columns: this.pageViewCols,
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };

    this.eventTableJson = {
      page_size: 10,
      page: 0,
      lazy: false,
      loading: true,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '400px',
      totalRecords: 1000,
      columns: this.eventTableCols,
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };

    this.pageViewAndroidJson = {
      page_size: 10,
      page: 0,
      lazy: false,
      loading: true,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '400px',
      totalRecords: 1000,
      columns: this.pageViewAndroidCols,
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };


    this.cardsJson = [
      {
        field: 'users',
        displayName: 'Users',
        value: 0,
        imageClass: 'fas fa-eye',
        format: 'number',
        config: [],
        color: '#5cb85c',
        infoTitle: ''
      },
      {
        field: 'sessions',
        displayName: 'Sessions',
        value: 0,
        imageClass: 'fas fa-hand-holding-usd',
        format: 'number',
        config: [],
        color: '#5bc0de',
        infoTitle: ''
      },
      {
        field: 'avg_session_duration',
        displayName: 'Avg. Session Duration',
        value: 0,
        imageClass: 'fas fa-dollar-sign',
        format: 'secToMins',
        config: [],
        color: '#FFDC00',
        infoTitle: 'Formula : Average(Activity End Time - Activity Start Time)'
      },
      // {
      //   field: 'bounce_rate',
      //   displayName: 'Bounce Rate',
      //   value: 0,
      //   format: 'percentage',
      //   config: [2],
      //   color: '#fff',
      //   imageClass: '',
      //   infoTitle: 'Formula : Count(Unique Page Views) / Count(Sessions)'
      // }
    ];
    this.cardsJsonAndroid = [
      {
        field: 'users',
        displayName: 'Users',
        value: 0,
        imageClass: 'fas fa-eye',
        format: 'number',
        config: [],
        color: '#5cb85c',
        infoTitle: ''
      },
      {
        field: 'event_count',
        displayName: 'Event Count',
        value: 0,
        imageClass: 'fas fa-hand-holding-usd',
        format: 'number',
        config: [],
        color: '#5bc0de',
        infoTitle: ''
      },
      {
        field: 'conversions',
        displayName: 'Conversions',
        value: 0,
        imageClass: 'fas fa-dollar-sign',
        format: 'number',
        config: [2],
        color: '#FFDC00',
        infoTitle: ''
      }
    ];
    this.acquireUserChartJson = {
      chartTypes: [{ key: 'bar', label: 'Traffic Channel Chart', stacked: true }],
      chartData: {
        labels: [],
        datasets: []
      },
      chartOptions: {
        title: {
          display: false,
          text: 'How do you acquire users?'
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: 'Users'
              },
              position: 'left',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, 'number', []);
                }
              }
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              if (currentValue) {
                return `${
                  data.datasets[tooltipItem.datasetIndex].label
                  } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
              } else {
                return null;
              }
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '311px'
    };
    this.getAppConfig();
  }

  getAppConfig() {
    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      // console.log(appConfig);
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        this._titleService.setTitle(this.appConfig['displayName']);
        let startDate = moment().subtract(7, 'days');
        let endDate = moment();
        let groupBy = '';
        if (!this.libServ.isEmptyObj(this.appConfig['filter']['filterConfig'])) {
          groupBy = typeof this.appConfig['filter']['filterConfig']['groupBy'] !== 'undefined'
            ? this.appConfig['filter']['filterConfig']['groupBy'].filter(v => v.selected)
            : '';
          if (typeof this.appConfig['filter']['filterConfig']['filters']['datePeriod'] !== 'undefined') {
            if (
              this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
              'defaultDate'
              ][0]['startOf']
            ) {
              startDate = moment()
                .subtract(
                  this.appConfig['filter']['filterConfig']['filters'][
                  'datePeriod'
                  ][0]['defaultDate'][0]['value'],
                  this.appConfig['filter']['filterConfig']['filters'][
                  'datePeriod'
                  ][0]['defaultDate'][0]['period']
                )
                .startOf(
                  this.appConfig['filter']['filterConfig']['filters'][
                  'datePeriod'
                  ][0]['defaultDate'][0]['period']
                );
            } else {
              startDate = moment().subtract(
                this.appConfig['filter']['filterConfig']['filters'][
                'datePeriod'
                ][0]['defaultDate'][0]['value'],
                this.appConfig['filter']['filterConfig']['filters'][
                'datePeriod'
                ][0]['defaultDate'][0]['period']
              );
            }
            endDate = moment().subtract(
              this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
              'defaultDate'
              ][1]['value'],
              this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
              'defaultDate'
              ][1]['period']
            );
          }
        }
        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: startDate.format('YYYYMMDD'),
            time_key2: endDate.format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: groupBy
        };
        this.requestParam = {
          dimensions: [],
          metrics: [],
          derived_metrics: [],
          timeKeyFilter: {},
          filters: { dimensions: [], metrics: [] },
          groupByTimeKey: { key: [], interval: 'daily' },
          gidGroupBy: '',
          orderBy: [
            { key: 'time_key', opcode: 'asc' }
          ],
          limit: '',
          offset: '',
          isCount: false
        };
        this.lastUpdatedAt();
        this.defaultDataLoad();

      }
    });
  }

  lastUpdatedAt() {
    this.dataFetchServ
      .getLastUpdatedData(this.appConfig['id'])
      .subscribe(data => {
        var zone_name = moment.tz.guess();
        this.timezone = moment.tz(zone_name).format('z');
        const lastUpdateObj = JSON.parse(data)[0];
        this.lastUpdatedOn = moment.utc(lastUpdateObj['updated_at']).tz(zone_name).toDate();
        this.nextUpdated = moment.utc(lastUpdateObj['next_run_at']).tz(zone_name).toDate();
        this.dataUpdatedThrough = moment.utc(lastUpdateObj['source_updated_through'], 'YYYYMMDD').tz(zone_name).toDate();
      });
  }

  defaultDataLoad() {
    const requestAudience = this.libServ.deepCopy(this.requestParam);
    requestAudience['metrics'] = [
      'user_count',
      'session_count',
      'bounce_rate'
    ];
    requestAudience['derived_metrics'] = ['avg_session_duration'];
    requestAudience['dimensions'] = ['time_key'];
    requestAudience['timeKeyFilter'] = this.filtersApplied['timeKeyFilter'];
    requestAudience['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);
    requestAudience['filters']['dimensions'] = [
      // { key: 'library_name', values: ['ALL'] },
      // { key: 'publisher_name', values: ['ALL'] },
      // { key: 'hours', values: ['ALL'] },
      { key: 'referer', values: ['ALL'] },
      { key: 'refferal_channel', values: ['ALL'] },
      { key: 'country_name', values: ['ALL'] },
      { key: 'decoded_page_url', values: ['ALL'] },
      { key: 'device_type', values: ['ALL'] },
      { key: 'event_name', values: ['ALL'] },
      { key: 'page_path', values: ['ALL'] },
      { key: 'library_name', values: ['analytics-web'] }
    ];
    requestAudience['gidGroupBy'] = [];
    requestAudience['groupByTimeKey'] = { key: ['time_key'], interval: 'daily' };
    requestAudience['orderBy'] = [{ key: 'time_key', opcode: 'asc' }];

    this.dateDifff = moment(this.filtersApplied['timeKeyFilter']['time_key2'], 'YYYYMMDD').diff(moment(this.filtersApplied['timeKeyFilter']['time_key1'], 'YYYYMMDD'), 'days') + 1;
    if (this.dateDifff === 1 || this.dateDifff === 2) {
      requestAudience['dimensions'].push('hours');
      requestAudience['groupByTimeKey'] = { key: ['time_key', 'hours'], interval: 'daily' };
    } else {
      requestAudience['filters']['dimensions'].push({ key: 'hours', values: ['ALL'] });
    }
    if(this.toggleBotStatus !== null)
      requestAudience['filters']['dimensions'].push({ key: 'bot_status', values: [this.toggleBotStatus ? '1' : '0'] });
    this.getAudienceOverview(requestAudience);
    const requestDeviceType = this.libServ.deepCopy(this.requestParam);
    requestDeviceType['dimensions'] = ['device_type'];
    requestDeviceType['metrics'] = ['session_count'];
    requestDeviceType['timeKeyFilter'] = this.filtersApplied['timeKeyFilter'];
    requestDeviceType['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);
    requestDeviceType['gidGroupBy'] = ['device_type'];
    requestDeviceType['groupByTimeKey'] = { key: [], interval: 'daily' };
    requestDeviceType['orderBy'] = [{ key: 'session_count', opcode: 'desc' }];
    if(this.toggleBotStatus !== null)
      requestDeviceType['filters']['dimensions'].push({ key: 'bot_status', values: [this.toggleBotStatus ? '1' : '0'] });
    requestDeviceType['filters']['dimensions'].push({ key: 'library_name', values: ['analytics-web'] });
    this.getDeviceType(requestDeviceType);

    const requestUserByTime = this.libServ.deepCopy(this.requestParam);
    requestUserByTime['metrics'] = ['user_count'];
    requestUserByTime['dimensions'] = ['time_key', 'hours'];
    requestUserByTime['timeKeyFilter'] = this.filtersApplied['timeKeyFilter'];
    requestUserByTime['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);
    requestUserByTime['gidGroupBy'] = [];
    requestUserByTime['groupByTimeKey'] = { key: ['time_key'], interval: 'daily' };
    requestUserByTime['orderBy'] = [{ key: 'user_count', opcode: 'desc' }];
    if(this.toggleBotStatus !== null)
      requestUserByTime['filters']['dimensions'].push({ key: 'bot_status', values: [this.toggleBotStatus ? '1' : '0'] });
    requestUserByTime['filters']['dimensions'].push({key : 'library_name', values:['analytics-web']});
    this.getUserByTime(requestUserByTime);

    const requestGeoMap = this.libServ.deepCopy(this.requestParam);
    requestGeoMap['dimensions'] = ['country_name'];
    requestGeoMap['metrics'] = ['session_count'];
    requestGeoMap['timeKeyFilter'] = this.filtersApplied['timeKeyFilter'];
    requestGeoMap['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);
    requestGeoMap['gidGroupBy'] = ['country_name'];
    requestGeoMap['groupByTimeKey'] = { key: [], interval: 'daily' };
    requestGeoMap['orderBy'] = [{ key: 'session_count', opcode: 'desc' }];
    if(this.toggleBotStatus !== null)
      requestGeoMap['filters']['dimensions'].push({ key: 'bot_status', values: [this.toggleBotStatus ? '1' : '0'] });
    requestGeoMap['filters']['dimensions'].push({key: 'library_name', values:['analytics-web']});
    this.getGeoMap(requestGeoMap);
    const requestAquiredUser = this.libServ.deepCopy(this.requestParam);
    requestAquiredUser['dimensions'] = ['refferal_channel', 'time_key'];
    requestAquiredUser['metrics'] = ['user_count'];
    requestAquiredUser['timeKeyFilter'] = this.filtersApplied['timeKeyFilter'];
    requestAquiredUser['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);
    requestAquiredUser['gidGroupBy'] = ['refferal_channel', 'time_key'];
    requestAquiredUser['groupByTimeKey'] = { key: [], interval: 'daily' };
    requestAquiredUser['orderBy'] = [{ key: 'time_key', opcode: 'asc' }];
    if(this.toggleBotStatus !== null)
      requestAquiredUser['filters']['dimensions'].push({ key: 'bot_status', values: [this.toggleBotStatus ? '1' : '0'] });
    requestAquiredUser['filters']['dimensions'].push({key : 'library_name', values:['analytics-web']});
    this.getAcquiredUsersOverview(requestAquiredUser, 0);

    // const requestUserRetention = this.libServ.deepCopy(this.requestParam);
    // requestUserRetention['metrics'] = ['user'];
    // requestUserRetention['timeKeyFilter'] = this.filtersApplied['timeKeyFilter'];
    // requestUserRetention['filters'] = this.filtersApplied['filters'];
    // requestUserRetention['gidGroupBy'] = '';
    // requestUserRetention['groupByTimeKey'] = { key: ['timekey'], interval: 'daily' };
    // requestUserRetention['orderBy'] = [{ key: 'user', opcode: 'desc' }];
    // this.getUserRetentionChart(requestUserRetention);
    // const requestActiveUser = this.libServ.deepCopy(this.requestParam);
    // requestActiveUser['metrics'] = ['user_count', 'time_key'];
    // requestActiveUser['timeKeyFilter'] = this.filtersApplied['timeKeyFilter'];
    // requestActiveUser['filters'] = this.filtersApplied['filters'];
    // requestActiveUser['gidGroupBy'] = [];
    // requestActiveUser['groupByTimeKey'] = { key: ['time_key'], interval: 'daily' };
    // requestActiveUser['orderBy'] = [{ key: 'user_count', opcode: 'desc' }];
    // this.getActiveUsersChart(requestActiveUser);

    const requestPageViewCount = this.libServ.deepCopy(this.requestParam);
    // requestPageViewCount['metrics'] = ['user_count','page_views'];
    requestPageViewCount['metrics'] = ['user_count',
      'page_views', 'session_count',
      'session_duration',
      'bounce_rate']
    requestPageViewCount['dimensions'] = ['decoded_page_url'];
    requestPageViewCount['timeKeyFilter'] = this.filtersApplied['timeKeyFilter'];
    requestPageViewCount['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);
    requestPageViewCount['gidGroupBy'] = ['decoded_page_url'];
    requestPageViewCount['orderBy'] = [{ key: 'page_views', opcode: 'desc' }];
    requestPageViewCount['limit'] = '10';
    requestPageViewCount['offset'] = '0';
    requestPageViewCount['isCount'] = true;
    if(this.toggleBotStatus !== null)
      requestPageViewCount['filters']['dimensions'].push({ key: 'bot_status', values: [this.toggleBotStatus ? '1' : '0'] });
    requestPageViewCount['filters']['dimensions'].push({key : 'library_name', values:['analytics-web']});
    this.getPageViewCount(requestPageViewCount);

    const requestAndroidEventNamesAndCount = this.libServ.deepCopy(this.requestParam);
    requestAndroidEventNamesAndCount['metrics'] = ['event_count'];
    requestAndroidEventNamesAndCount['dimensions'] = ['event_name'];
    requestAndroidEventNamesAndCount['timeKeyFilter'] = this.filtersApplied['timeKeyFilter'];
    requestAndroidEventNamesAndCount['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);
    requestAndroidEventNamesAndCount['filters']['dimensions'] = [
      { key: 'library_name', values: ['analytics-android'] }
    ];
    requestAndroidEventNamesAndCount['gidGroupBy'] = ['event_name'];
    requestAndroidEventNamesAndCount['orderBy'] = [{ key: 'event_count', opcode: 'desc' }];
    requestAndroidEventNamesAndCount['limit'] = '10';
    requestAndroidEventNamesAndCount['offset'] = '0';
    requestAndroidEventNamesAndCount['isCount'] = true;
    if(this.toggleBotStatus !== null)
      requestAndroidEventNamesAndCount['filters']['dimensions'].push({ key: 'bot_status', values: [this.toggleBotStatus ? '1' : '0'] });
    this.getAndroidEventNamesAndCount(requestAndroidEventNamesAndCount);

    const requestAndroidScreenViewCount = this.libServ.deepCopy(this.requestParam);
    requestAndroidScreenViewCount['metrics'] = ['user_count',
      'page_views', 'session_count',
      'session_duration',
      'bounce_rate'];
    requestAndroidScreenViewCount['dimensions'] = ['page_path'];
    requestAndroidScreenViewCount['timeKeyFilter'] = this.filtersApplied['timeKeyFilter'];
    requestAndroidScreenViewCount['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);
    requestAndroidScreenViewCount['filters']['dimensions'] = [
      { key: 'library_name', values: ['analytics-android'] }
    ];
    requestAndroidScreenViewCount['gidGroupBy'] = ['page_path'];
    requestAndroidScreenViewCount['orderBy'] = [{ key: 'page_views', opcode: 'desc' }];
    requestAndroidScreenViewCount['limit'] = '10';
    requestAndroidScreenViewCount['offset'] = '0';
    requestAndroidScreenViewCount['isCount'] = true;
    if(this.toggleBotStatus !== null)
      requestAndroidScreenViewCount['filters']['dimensions'].push({ key: 'bot_status', values: [this.toggleBotStatus ? '1' : '0'] });
    this.getAndroidScreenViewCount(requestAndroidScreenViewCount);

    const requestAndroidGeoMap = this.libServ.deepCopy(this.requestParam);
    requestAndroidGeoMap['dimensions'] = ['country_name'];
    requestAndroidGeoMap['metrics'] = ['session_count'];
    requestAndroidGeoMap['timeKeyFilter'] = this.filtersApplied['timeKeyFilter'];
    requestAndroidGeoMap['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);
    requestAndroidGeoMap['filters']['dimensions'] = [
      { key: 'library_name', values: ['analytics-android'] }
    ];
    requestAndroidGeoMap['gidGroupBy'] = ['country_name'];
    requestAndroidGeoMap['groupByTimeKey'] = { key: [], interval: 'daily' };
    requestAndroidGeoMap['orderBy'] = [{ key: 'session_count', opcode: 'desc' }];
    if(this.toggleBotStatus !== null)
      requestAndroidGeoMap['filters']['dimensions'].push({ key: 'bot_status', values: [this.toggleBotStatus ? '1' : '0'] });
    this.getAndroidGeoMap(requestAndroidGeoMap);

    const requestAndroidAudience = this.libServ.deepCopy(this.requestParam);
    requestAndroidAudience['metrics'] = [
      'user_count',
      'event_count',
      'conversions'
    ];
    requestAndroidAudience['dimensions'] = ['time_key'];
    requestAndroidAudience['timeKeyFilter'] = this.filtersApplied['timeKeyFilter'];
    requestAndroidAudience['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);
    requestAndroidAudience['filters']['dimensions'] = [
      { key: 'library_name', values: ['analytics-android'] },
      { key: 'hours', values: ['ALL'] },
      { key: 'referer', values: ['ALL'] },
      { key: 'refferal_channel', values: ['ALL'] },
      { key: 'country_name', values: ['ALL'] },
      { key: 'decoded_page_url', values: ['ALL'] },
      { key: 'device_type', values: ['ALL'] },
      { key: 'event_name', values: ['ALL'] },
      { key: 'page_path', values: ['ALL'] },
    ];
    requestAndroidAudience['gidGroupBy'] = [];
    requestAndroidAudience['groupByTimeKey'] = { key: ['time_key'], interval: 'daily' };
    requestAndroidAudience['orderBy'] = [{ key: 'time_key', opcode: 'asc' }];
    if(this.toggleBotStatus !== null)
      requestAndroidAudience['filters']['dimensions'].push({ key: 'bot_status', values: [this.toggleBotStatus ? '1' : '0'] });
    this.getAndroidAudienceOverview(requestAndroidAudience);
  }

  getAndroidEventNamesAndCount(requestAndroidEventNamesAndCount: {} & object) {
    this.eventTableJson['loading'] = true;
    this.dataFetchServ.getTableData(requestAndroidEventNamesAndCount).subscribe(response => {
      if (response['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(response['status_msg']);
        this.eventTableJson['loading'] = true;
        return;
      }
      const chartData = response['data'];
      this.eventTableCounts = chartData;
      this.eventTableJson['totalRecords'] = response['totalItems'];
      this.eventTableJson['loading'] = false;
      this.eventTableJson['lazy'] = true;

    });
  }

  getAndroidScreenViewCount(requestAndroidScreenViewCount: {} & object) {
    this.pageViewAndroidJson['loading'] = true;
    this.dataFetchServ.getTableData(requestAndroidScreenViewCount).subscribe(response => {
      if (response['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(response['status_msg']);
        this.pageViewAndroidJson['loading'] = true;
        return;
      }
      const chartData = response['data'];
      this.pageViewAndroidCounts = chartData;
      this.pageViewAndroidJson['totalRecords'] = response['totalItems'];
      this.pageViewAndroidJson['loading'] = false;
      this.pageViewAndroidJson['lazy'] = true;

    });
  }

  getAndroidGeoMap(requestAndroidGeoMap: {} & object) {
    this.dataFetchServ.getGeoContriesData().subscribe(data => {

      const countries = ChartGeo.topojson.feature(data, data['objects']['countries']).features;
      this.geoSessionAndroidChartJson = {
        chartTypes: [{ key: 'choropleth', label: 'Users by Country' }],
        chartData: {
          labels: [],
          datasets: [{
            label: 'Countries',
            data: [],
          }]
        },
        chartOptions: {
          title: {
            display: false,
            text: 'Users by Country'
          },
          showOutline: true,
          showGraticule: true,
          legend: {
            display: false
          },
          scale: {
            projection: 'equalEarth'
          },
          geo: {
            colorScale: {
              display: true,
              position: 'left'
            },
          },
          tooltips: {
            callbacks: {
              label: (tooltipItem, data) => {
                const currentValue = tooltipItem.value;
                return `${data.labels[tooltipItem.index]} : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
              }
            }
          },
          layout: {
            padding: {
              left: 0,
              right: 4,
              top: 0,
              bottom: 0
            }
          },
          plugins: {}
        },
        zoomLabel: false,
        chartWidth: '',
        chartHeight: '370px'
      };

      this.showGeoSessionAndroidChart = false;
      this.noDataGeoSessionAndroidChart = false;
      this.dataFetchServ.getTableData(requestAndroidGeoMap).subscribe(response => {
        if (response['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(response['status_msg']);
          return;
        }
        // if (!this.libServ.isEmptyObj(response['data'])) {
        const chartData = response['data'];
        if (typeof chartData === 'undefined' || !chartData.length) {
          this.noDataGeoSessionAndroidChart = true;
        } else {

          const userCount = [];
          const label = [];
          countries.forEach(d => {
            chartData.forEach(element => {
              if (d.properties.name === element['country_name']) {
                label.push(element['country_name']);
                userCount.push({
                  feature: d,
                  value: element['session_count']
                });
              }
            });
          });
          this.geoSessionAndroidChartJson['chartData']['labels'] = label;
          this.geoSessionAndroidChartJson['chartData']['datasets'][0]['data'] = userCount;

          this.showGeoSessionAndroidChart = true;
          this.noDataGeoSessionAndroidChart = false;
        }

      });
    });
  }

  getAndroidAudienceOverview(requestAndroidAudience: {} & object) {
    this.showMainLineAndroidChart = false;
    this.noDataMainLineAndroidChart = false;
    const colors = this.libServ.dynamicColors(2);
    this.mainLineAndroidChartJson = {
      chartTypes: [{ key: 'bar', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'Users',
            type: 'line',
            yAxisID: 'y-axis-0',

            borderColor: colors[0],
            fill: false,
            backgroundColor: colors[0],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: false,
          text: 'Users'
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: 'Users'
              },
              position: 'left',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '', []);
                }
              }
              // scaleFontColor: "rgba(151,137,200,0.8)"
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, '', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };
    this.dataFetchServ.getTableData(requestAndroidAudience).subscribe(response => {
      if (response['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(response['status_msg']);
        return;
      }
      const chartData = response['data'];
      if (typeof chartData === 'undefined' || !chartData.length) {
        this.noDataMainLineAndroidChart = true;
      } else {
        const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));

        this.mainLineAndroidChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );
        const usersArr = [];
        const eventCountArr = [];
        const conversionCountArr = [];
        datesArr.forEach(time_key => {
          chartData.forEach(r => {
            if (r['time_key'] === time_key) {
              usersArr.push(r['user_count']);
              eventCountArr.push(r['event_count']);
              conversionCountArr.push(r['conversions']);
            }
          });
        });
        this.mainLineAndroidChartJson['dataObj'] = {
          'users': usersArr,
          'event_count': eventCountArr,
          'conversions': conversionCountArr
        };

        this.mainLineAndroidChartJson['chartData']['datasets'][0]['data'] = usersArr;
        setTimeout(() => {
          for (const t of this.audienceAndroidTabView.tabs) {
            if (t.selected) {
              t.selected = false;
            }
          }
          this.audienceAndroidTabView.tabs[0].selected = true;
          this.showMainLineAndroidChart = true;
          this.noDataMainLineAndroidChart = false;
        }, 0);
      }
    });
  }

  audienceAndroidTabChange(e) {
    const currentTab = this.cardsJsonAndroid[e.index];
    this.showMainLineAndroidChart = false;
    this.noDataMainLineAndroidChart = false;
    setTimeout(() => {
      this.mainLineAndroidChartJson['chartData']['datasets'][0]['label'] = currentTab.displayName === 'Session Duration' ? 'Avg. Session Duration' : currentTab.displayName;
      this.mainLineAndroidChartJson['chartOptions']['title']['text'] = currentTab.displayName === 'Session Duration' ? 'Avg. Session Duration' : currentTab.displayName;
      this.mainLineAndroidChartJson['chartOptions']['scales']['yAxes'][0]['scaleLabel']['labelString'] = currentTab.displayName === 'Session Duration' ? 'Avg. Session Duration' : currentTab.displayName;
      this.mainLineAndroidChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
        callback: (value, index, values) => {
          return this.formatNumPipe.transform(
            currentTab.format === 'percentage' ? value * 100 : value,
            currentTab.format,
            currentTab.config
          );
        }
      };
      this.mainLineAndroidChartJson['chartOptions']['tooltips']['callbacks'] = {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return `${
            data.datasets[tooltipItem.datasetIndex].label
            } : ${
            this.formatNumPipe.transform(
              currentTab.format === 'percentage' ? currentValue * 100 : currentValue,
              currentTab.format,
              currentTab.config
            )
            }`;
        }
      };
      this.mainLineAndroidChartJson['chartData']['datasets'][0]['data'] = typeof this.mainLineAndroidChartJson['dataObj'] !== 'undefined'
        ? this.mainLineAndroidChartJson['dataObj'][currentTab['field']]
        : [];
      if (typeof this.mainLineAndroidChartJson['dataObj'] !== 'undefined') {
        this.noDataMainLineAndroidChart = false;
        this.showMainLineAndroidChart = true;
      } else {
        this.noDataMainLineAndroidChart = true;
        this.showMainLineAndroidChart = false;
      }

    }, 0);
  }

  onFiltersApplied(filterData: object) {
    this.filtersApplied['filters']['dimensions'] = [];
    for (const k in filterData['filter']['dimensions']) {
      if (filterData['filter']['dimensions'][k].length !== 0) {
        this.filtersApplied['filters']['dimensions'].push({
          key: k,
          values: filterData['filter']['dimensions'][k]
        });
      }

    }

    this.first = 0;

    this.filtersApplied['groupby'] = filterData['groupby'];
    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];
    this.defaultDataLoad();
  }

  getPageViewCount(request) {
    this.pageViewJson['loading'] = true;
    this.dataFetchServ.getTableData(request).subscribe(response => {
      if (response['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(response['status_msg']);
        this.pageViewJson['loading'] = true;
        return;
      }
      const chartData = response['data'];
      this.pageViewCounts = chartData;
      this.pageViewJson['totalRecords'] = response['totalItems'];
      this.pageViewJson['loading'] = false;
      this.pageViewJson['lazy'] = true;

    });
  }

  // getActiveUsersChart(request) {
  //   const colors = this.libServ.dynamicColors(2);
  //   this.activeUserChartJson = {
  //     chartTypes: [{ key: 'bar', label: 'Line Chart' }],
  //     chartData: {
  //       labels: [],
  //       datasets: [
  //         {
  //           label: 'Active Users',
  //           type: 'line',
  //           yAxisID: 'y-axis-0',

  //           borderColor: colors[0],
  //           fill: false,
  //           backgroundColor: colors[0],
  //           data: []
  //         }
  //       ]
  //     },
  //     chartOptions: {
  //       title: {
  //         display: false,
  //         text: 'Active Users'
  //       },
  //       legend: {
  //         display: true
  //       },
  //       scales: {
  //         xAxes: [
  //           {
  //             display: true,
  //             scaleLabel: {
  //               display: true,
  //               labelString: 'Date'
  //             }
  //           }
  //         ],
  //         yAxes: [
  //           {
  //             id: 'y-axis-0',
  //             scaleLabel: {
  //               display: true,
  //               labelString: 'Active Users'
  //             },
  //             position: 'left',
  //             name: '1',
  //             ticks: {
  //               callback: (value, index, values) => {
  //                 return this.formatNumPipe.transform(value, '', []);
  //               }
  //             }
  //             // scaleFontColor: "rgba(151,137,200,0.8)"
  //           }
  //         ]
  //       },
  //       tooltips: {
  //         mode: 'index',
  //         intersect: false,
  //         callbacks: {
  //           label: (tooltipItem, data) => {
  //             const currentValue =
  //               data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
  //             return `${
  //               data.datasets[tooltipItem.datasetIndex].label
  //               } : ${this.formatNumPipe.transform(currentValue, '', [])}`;
  //           }
  //         }
  //       },
  //       plugins: {
  //         datalabels: false
  //       }
  //     },
  //     zoomLabel: false,
  //     chartWidth: '',
  //     chartHeight: '330px'
  //   };

  //   this.showActiveUserChart = false;
  //   this.noDataActiveUserChart = true;
  //   this.dataFetchServ.getAudienceMgmtData(request).subscribe(response => {
  //     if (response['status'] === 0) {
  //       this.toastService.displayToast({
  //         severity: 'error',
  //         summary: 'Server Error',
  //         detail: 'Please refresh the page'
  //       });
  //       console.log(response['status_msg']);
  //       return;
  //     }
  //     const chartData = response['data'];
  //     if (typeof chartData === 'undefined' || !chartData.length) {
  //       this.noDataActiveUserChart = true;
  //     } else {
  //       const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));

  //       this.activeUserChartJson['chartData']['labels'] = datesArr.map(d =>
  //         moment(d, 'YYYYMMDD').format('YYYY-MM-DD')
  //       );
  //       const usersArr = [];
  //       datesArr.forEach(time_key => {
  //         chartData.forEach(r => {
  //           if (r['time_key'] === time_key) {
  //             usersArr.push(r['user_count']);
  //           }
  //         });
  //       });
  //       this.activeUserChartJson['chartData']['datasets'][0]['data'] = usersArr;
  //       this.showActiveUserChart = true;
  //       this.noDataActiveUserChart = false;
  //     }
  //   });
  // }

  getUserByTime(request) {
    // request['timeKeyFilter'] = {
    //   time_key1: moment().subtract(1, 'weeks').startOf('week').format('YYYYMMDD'),
    //   time_key2: moment().subtract(1, 'weeks').endOf('week').format('YYYYMMDD')
    // };
    // request['timeKeyFilter'] = {time_key1: 20200413, time_key2: 20200420}
    this.userByTimeChartJson = {
      chartTypes: [{ key: 'matrix', label: 'Line Chart' }],
      chartData: {
        datasets: [
          {
            label: 'Time',
            data: [],
            backgroundColor: function (ctx) {
              const alpha = ctx.dataset.data[ctx.dataIndex].alpha + 0.2;
              if (alpha > 1) {
                return `rgba(7, 140, 242, ${alpha})`;
              } else {
                return `rgba(7, 174, 242, ${alpha})`;
              }

            },
            borderColor: function (ctx) {
              const alpha = ctx.dataset.data[ctx.dataIndex].alpha + 0.2;
              if (alpha > 1) {
                return `rgba(7, 140, 242, ${alpha})`;
              } else {
                return `rgba(7, 174, 242, ${alpha})`;
              }
            },
            borderWidth: { left: 1, right: 1, top: 1, bottom: 1 },
            // width: function (ctx) {
            //   const a = ctx.chart.chartArea;
            //   return (a.right - a.left) / 16;
            // },
            height: function (ctx) {
              const a = ctx.chart.chartArea;
              return (a.bottom - a.top) / 28;
            }
          }]
      },
      chartOptions: {
        title: {
          display: false,
          text: 'Users by time of day'
        },
        legend: {
          display: false
        },
        tooltips: {
          callbacks: {
            title: function () { return ''; },
            label: (item, data) => {
              const v = data.datasets[item.datasetIndex].data[item.index];
              return [moment(v.x, 'YYYY-MM-DD').format('MM-DD-YYYY') + ' at ' + v['hours'] + ':00 hour ' + this.formatNumPipe.transform(v['user_count'], 'number', []) + ' Users'];
            }
          }
        },
        scales: {
          xAxes: [{
            type: 'time',
            offset: true,
            time: {
              unit: 'day',
              displayFormats: {
                day: 'MM-DD-YYYY'
              }

            },
            ticks: {
            },
            gridLines: {
              display: false
            },
            scaleLabel: {
              display: true,
              labelString: 'Date'
            }
          }],
          yAxes: [{
            type: 'time',
            position: 'right',
            labels: ['00:00', '04:00', '08:00', '12:00', '16:00', '20:00', '24:00'],
            time: {
              unit: 'hour',
              parser: 'HH:mm',
              min: '00:00',
              max: '24:00',
              displayFormats: {
                hour: 'HH'
              }
            },
            ticks: {
              source: 'labels',
              reverse: false
            },
            gridLines: {
              display: false
            },
            scaleLabel: {
              display: true,
              labelString: 'Time'
            }
          }]
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '380px'
    };
    this.showUserByTimeChart = false;
    this.noDataUserByTimeChart = false;

    this.dataFetchServ.getTableData(request).subscribe(response => {
      if (response['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(response['status_msg']);
        return;
      }
      const chartData = response['data'];

      if (typeof chartData === 'undefined' || !chartData.length) {
        this.noDataUserByTimeChart = true;
      } else {
        const maxValue = chartData.reduce((max, p) => p.user_count > max ? p.user_count : max, chartData[0].user_count);
        this.userByTimeChartJson['chartData']['datasets'][0]['colorArr'] = [1, 2, 3, 4, 5].map(user_count => Math.ceil(user_count * (maxValue / 5)));
        chartData.forEach(element => {
          element['x'] = moment(element['time_key'], 'YYYYMMDD');
          element['alpha'] = parseFloat((parseInt(element['user_count']) / maxValue).toFixed(2));
        });

        this.userByTimeChartJson['chartData']['datasets'][0]['data'] = chartData;
        this.showUserByTimeChart = true;
        this.noDataUserByTimeChart = false;
      }
    });
  }

  // getUserRetentionChart(request) {
  //   this.userRetentionChartJson = {
  //     chartTypes: [{ key: 'matrix', label: 'Line Chart' }],
  //     chartData: {
  //       datasets: [{
  //         label: '',
  //         data: [
  //           { x: '2019-04-11', y: '08:00', v: 26 },
  //           { x: '2019-04-11', y: '09:00', v: 30 },
  //           { x: '2019-04-11', y: '10:00', v: 59 },
  //           { x: '2019-04-11', y: '11:00', v: 93 },
  //           { x: '2019-04-11', y: '12:00', v: 67 },
  //           { x: '2019-04-11', y: '13:00', v: 95 },
  //           { x: '2019-04-11', y: '14:00', v: 57 },
  //           { x: '2019-04-11', y: '15:00', v: 21 },
  //           { x: '2019-04-11', y: '16:00', v: 24 },
  //           { x: '2019-04-12', y: '08:00', v: 27 },
  //           { x: '2019-04-12', y: '09:00', v: 28 },
  //           { x: '2019-04-12', y: '10:00', v: 91 },
  //           { x: '2019-04-12', y: '11:00', v: 91 },
  //           { x: '2019-04-12', y: '12:00', v: 61 },
  //           { x: '2019-04-12', y: '13:00', v: 61 },
  //           { x: '2019-04-12', y: '14:00', v: 75 },
  //           { x: '2019-04-12', y: '15:00', v: 30 },
  //           { x: '2019-04-12', y: '16:00', v: 72 },
  //           { x: '2019-04-13', y: '08:00', v: 23 },
  //           { x: '2019-04-13', y: '09:00', v: 59 },
  //           { x: '2019-04-13', y: '10:00', v: 55 },
  //           { x: '2019-04-13', y: '11:00', v: 30 },
  //           { x: '2019-04-13', y: '12:00', v: 60 },
  //           { x: '2019-04-13', y: '13:00', v: 22 },
  //           { x: '2019-04-13', y: '14:00', v: 63 },
  //           { x: '2019-04-13', y: '15:00', v: 81 },
  //           { x: '2019-04-13', y: '16:00', v: 78 },
  //           { x: '2019-04-14', y: '08:00', v: 32 },
  //           { x: '2019-04-14', y: '09:00', v: 63 },
  //           { x: '2019-04-14', y: '10:00', v: 15 },
  //           { x: '2019-04-14', y: '11:00', v: 52 },
  //           { x: '2019-04-14', y: '12:00', v: 57 },
  //           { x: '2019-04-14', y: '13:00', v: 76 },
  //           { x: '2019-04-14', y: '14:00', v: 56 },
  //           { x: '2019-04-14', y: '15:00', v: 90 },
  //           { x: '2019-04-14', y: '16:00', v: 48 },
  //           { x: '2019-04-15', y: '08:00', v: 97 },
  //           { x: '2019-04-15', y: '09:00', v: 27 },
  //           { x: '2019-04-15', y: '10:00', v: 40 },
  //           { x: '2019-04-15', y: '11:00', v: 95 },
  //           { x: '2019-04-15', y: '12:00', v: 75 },
  //           { x: '2019-04-15', y: '13:00', v: 16 },
  //           { x: '2019-04-15', y: '14:00', v: 91 },
  //           { x: '2019-04-15', y: '15:00', v: 40 },
  //           { x: '2019-04-15', y: '16:00', v: 67 },
  //           { x: '2019-04-16', y: '08:00', v: 64 },
  //           { x: '2019-04-16', y: '09:00', v: 93 },
  //           { x: '2019-04-16', y: '10:00', v: 50 },
  //           { x: '2019-04-16', y: '11:00', v: 63 },
  //           { x: '2019-04-16', y: '12:00', v: 17 },
  //           { x: '2019-04-16', y: '13:00', v: 42 },
  //           { x: '2019-04-16', y: '14:00', v: 68 },
  //           { x: '2019-04-16', y: '15:00', v: 15 },
  //           { x: '2019-04-16', y: '16:00', v: 78 },
  //           { x: '2019-04-17', y: '08:00', v: 52 },
  //           { x: '2019-04-17', y: '09:00', v: 31 },
  //           { x: '2019-04-17', y: '10:00', v: 78 },
  //           { x: '2019-04-17', y: '11:00', v: 58 },
  //           { x: '2019-04-17', y: '12:00', v: 46 },
  //           { x: '2019-04-17', y: '13:00', v: 31 },
  //           { x: '2019-04-17', y: '14:00', v: 86 },
  //           { x: '2019-04-17', y: '15:00', v: 45 },
  //           { x: '2019-04-17', y: '16:00', v: 72 }
  //         ],
  //         backgroundColor: function (ctx) {
  //           const value = ctx.dataset.data[ctx.dataIndex].v;
  //           const alpha = (value - 5) / 99;
  //           return `rgba(135, 206, 235, ${alpha})`;
  //         },
  //         borderColor: function (ctx) {
  //           const value = ctx.dataset.data[ctx.dataIndex].v;
  //           const alpha = (value - 5) / 99;
  //           return `rgba(135, 206, 235, ${alpha})`;
  //         },
  //         borderWidth: { left: 0, right: 0 },
  //         width: function (ctx) {
  //           const a = ctx.chart.chartArea;
  //           return (a.right - a.left) / 8;
  //         },
  //         height: function (ctx) {
  //           const a = ctx.chart.chartArea;
  //           return (a.bottom - a.top) / 14;
  //         }
  //       }]
  //     },
  //     chartOptions: {
  //       title: {
  //         display: false,
  //         text: 'User retention'
  //       },
  //       legend: {
  //         display: false
  //       },
  //       tooltips: {
  //         callbacks: {
  //           title: function () { return ''; },
  //           label: function (item, data) {
  //             const v = data.datasets[item.datasetIndex].data[item.index];
  //             return ['x: ' + v.x, 'y: ' + v.y, 'v: ' + v.v];
  //           }
  //         }
  //       },
  //       scales: {
  //         xAxes: [{
  //           type: 'time',
  //           offset: true,
  //           time: {
  //             unit: 'day'
  //           },
  //           ticks: {
  //           },
  //           gridLines: {
  //             display: false
  //           }
  //         }],
  //         yAxes: [{
  //           type: 'time',
  //           position: 'right',
  //           labels: ['08:00', '12:00', '16:00'],
  //           time: {
  //             unit: 'hour',
  //             parser: 'HH:mm',
  //             min: '06:00',
  //             max: '18:00',
  //             displayFormats: {
  //               hour: 'HH'
  //             }
  //           },
  //           ticks: {
  //             source: 'labels',
  //             reverse: false
  //           },
  //           gridLines: {
  //             display: false
  //           }
  //         }]
  //       },
  //       plugins: {
  //         datalabels: false
  //       }
  //     },
  //     zoomLabel: false,
  //     chartWidth: '',
  //     chartHeight: '380px'
  //   };
  //   this.showUserRetentionChart = true;
  //   this.noDataUserRetentionChart = false;
  //   console.log('userRetentionChartJson', this.userRetentionChartJson);

  // }

  getAudienceOverview(request) {
    this.showMainLineChart = false;
    this.noDataMainLineChart = false;
    const colors = this.libServ.dynamicColors(2);
    this.mainLineChartJson = {
      chartTypes: [{ key: 'line', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'Users',
            type: 'line',
            yAxisID: 'y-axis-0',

            borderColor: colors[0],
            fill: false,
            backgroundColor: colors[0],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: false,
          text: 'Users'
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: 'Users'
              },
              position: 'left',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, 'number', []);
                }
              }
              // scaleFontColor: "rgba(151,137,200,0.8)"
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    // this.showMainLineChart = false;
    // this.noDataMainLineChart = false;
    console.log('request', request)
    this.dataFetchServ.getTableData(request).subscribe(response => {
      if (response['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(response['status_msg']);
        // this.noDataMainLineChart = true;
        return;
      }
      const chartData = response['data'];
      if (typeof chartData === 'undefined' || !chartData.length) {
        this.noDataMainLineChart = true;
      } else {
        const usersArr = [];
        const sessionsArr = [];
        const sessionDurationArr = [];
        const bounceRateArr = [];
        const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));

        if (this.dateDifff === 1 || this.dateDifff === 2) {
          this.mainLineChartJson['chartOptions']['scales']['xAxes'] = [{
            type: 'time',
            distribution: 'series',
            display: true,
            time: {
              unit: 'hour',
              displayFormats: {
                hour: 'h:mm a'
              }
            },
            ticks: {
              major: {
                unit: 'day',
                enabled: true,
                fontStyle: 'bold',
                fontSize: 14,
              }
            },
            scaleLabel: {
              display: true,
              labelString: 'Date'
            }
          }]

          const hours = Array.from(new Set(chartData.map(r => r['hours']))) as [];
          hours.sort((a, b) => a - b);
          const datetime = [];
          datesArr.forEach(time_key => {
            hours.forEach(hour => {
              chartData.forEach(r => {
                if (r['time_key'] === time_key && r['hours'] === hour) {
                  datetime.push(moment(r['time_key'], 'YYYYMMDD').format('YYYY-MM-DD') + " " + r['hours'] + ":00");
                }
              });
            })
          });
          this.mainLineChartJson['chartData']['labels'] = datetime;
          datesArr.forEach(time_key => {
            hours.forEach(hour => {
              chartData.forEach(r => {
                if (r['time_key'] === time_key && r['hours'] === hour) {
                  usersArr.push(r['user_count']);
                  sessionsArr.push(r['session_count']);
                  sessionDurationArr.push(r['avg_session_duration']);
                  bounceRateArr.push(r['bounce_rate']);
                }
              });
            });
          });
        } else {
          this.mainLineChartJson['chartData']['labels'] = datesArr.map(d =>
            moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
          );

          datesArr.forEach(time_key => {
            chartData.forEach(r => {
              if (r['time_key'] === time_key) {
                usersArr.push(r['user_count']);
                sessionsArr.push(r['session_count']);
                sessionDurationArr.push(r['avg_session_duration']);
                bounceRateArr.push(r['bounce_rate']);
              }
            });
          })
        }
        this.mainLineChartJson['dataObj'] = {
          'users': usersArr,
          'sessions': sessionsArr,
          'avg_session_duration': sessionDurationArr,
          'bounce_rate': bounceRateArr
        };

        this.mainLineChartJson['chartData']['datasets'][0]['data'] = usersArr;
        setTimeout(() => {
          for (const t of this.audienceTabView.tabs) {
            if (t.selected) {
              t.selected = false;
            }
          }
          this.audienceTabView.tabs[0].selected = true;
          this.showMainLineChart = true;
          this.noDataMainLineChart = false;
        }, 0);
      }
    });
  }

  getAcquiredUsersOverview(request, idx) {
    // const colors = this.libServ.dynamicColors(8);
    this.showAcquireUserChart = false;
    this.noDataAcquireUserChart = false;
    this.dataFetchServ.getTableData(request).subscribe(response => {

      if (response['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(response['status_msg']);
        return;
      }
      const chartData = response['data'];
      if (typeof chartData === 'undefined' || !chartData.length) {
        this.noDataAcquireUserChart = true;
      } else {
        const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));

        this.acquireUserChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );
        this.acquireUserChartJson['chartData']['datasets'] = [];
        for (const t of this.acquireUserTabView.tabs) {
          if (t.selected) {
            t.selected = false;
          }
        }
        this.acquireUserTabView.tabs[idx].selected = true;
        if (idx === 0) {
          const sourcesJson = Array.from(new Set(chartData.map(r => r['refferal_channel'])));
          const colors = this.libServ.dynamicColors(sourcesJson.length);

          sourcesJson.forEach((src, i) => {
            const pageViewsArr = [];
            datesArr.forEach((time_key, i2) => {
              let value = null;
              chartData.forEach((r, i1) => {
                if (r['refferal_channel'] === src && r['time_key'] === time_key) {
                  value = r['user_count']
                }
              });
              pageViewsArr.push(value);
            });
            // console.log(src, pageViewsArr);
            this.acquireUserChartJson['chartData']['datasets'].push({
              label: src,
              type: 'bar',
              data: pageViewsArr,
              borderColor: colors[i],
              fill: false,
              backgroundColor: colors[i]
            });
          });
          this.acquireUserChartJson['note'] = null;
        } else {
          // chart
          const sourcesJson = Array.from(new Set(chartData.map(r => r['referer'])));
          const colors = this.libServ.dynamicColors(sourcesJson.length);

          sourcesJson.forEach((src, i) => {
            const pageViewsArr = [];
            datesArr.forEach(time_key => {
              let value = null;
              chartData.forEach(r => {
                if (r['time_key'] === time_key && r['referer'] === src) {
                  value = r['page_views'];
                }
              });
              pageViewsArr.push(value);
            });
            this.acquireUserChartJson['chartData']['datasets'].push({
              label: src,
              type: 'bar',
              data: pageViewsArr,
              borderColor: colors[i],
              fill: false,
              backgroundColor: colors[i]
            });
          });
          this.acquireUserChartJson['note'] = { txt: 'Showing Top 5 Referrals by Page Views' };
        }
        // console.log(this.acquireUserChartJson);
        this.showAcquireUserChart = true;
        this.noDataAcquireUserChart = false;
      }
    });
  }

  audienceTabChange(e) {
    const currentTab = this.cardsJson[e.index];
    this.showMainLineChart = false;
    // this.noDataMainLineChart = false;
    setTimeout(() => {
      this.mainLineChartJson['chartData']['datasets'][0]['label'] = currentTab.displayName === 'Session Duration' ? 'Avg. Session Duration' : currentTab.displayName;
      this.mainLineChartJson['chartOptions']['title']['text'] = currentTab.displayName === 'Session Duration' ? 'Avg. Session Duration' : currentTab.displayName;
      this.mainLineChartJson['chartOptions']['scales']['yAxes'][0]['scaleLabel']['labelString'] = currentTab.displayName === 'Session Duration' ? 'Avg. Session Duration' : currentTab.displayName;
      this.mainLineChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
        callback: (value, index, values) => {
          return this.formatNumPipe.transform(
            currentTab.format === 'percentage' ? value : value,
            currentTab.format,
            currentTab.config
          );
        }
      };
      this.mainLineChartJson['chartOptions']['tooltips']['callbacks'] = {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return `${
            data.datasets[tooltipItem.datasetIndex].label
            } : ${
            this.formatNumPipe.transform(
              currentTab.format === 'percentage' ? currentValue : currentValue,
              currentTab.format,
              currentTab.config
            )
            }`;
        }
      };
      this.mainLineChartJson['chartData']['datasets'][0]['data'] = typeof this.mainLineChartJson['dataObj'] !== 'undefined'
        ? this.mainLineChartJson['dataObj'][currentTab['field']]
        : [];
      // console.log(this.mainLineChartJson);
      // this.noDataMainLineChart = false;
      this.showMainLineChart = true;
    }, 0);
  }

  acquireUserTabChange(e) {
    if (e.index === 0) {
      const requestAquiredUser = this.libServ.deepCopy(this.requestParam);
      requestAquiredUser['dimensions'] = ['refferal_channel', 'time_key'];
      requestAquiredUser['metrics'] = ['user_count'];
      requestAquiredUser['timeKeyFilter'] = this.filtersApplied['timeKeyFilter'];
      requestAquiredUser['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);
      requestAquiredUser['gidGroupBy'] = ['refferal_channel', 'time_key'];
      requestAquiredUser['groupByTimeKey'] = { key: [], interval: 'daily' };
      requestAquiredUser['orderBy'] = [{ key: 'time_key', opcode: 'asc' }];
      if(this.toggleBotStatus !== null)
        requestAquiredUser['filters']['dimensions'].push({ key: 'bot_status', values: [this.toggleBotStatus ? '1' : '0'] });
      requestAquiredUser['filters']['dimensions'].push({ key: 'library_name', values: ['analytics-web'] });
      this.getAcquiredUsersOverview(requestAquiredUser, 0);
    } else if (e.index === 1) {
      const requestAquiredUser = this.libServ.deepCopy(this.requestParam);
      requestAquiredUser['dimensions'] = ['referer', 'time_key'];
      requestAquiredUser['metrics'] = ['page_views'];
      requestAquiredUser['timeKeyFilter'] = this.filtersApplied['timeKeyFilter'];
      requestAquiredUser['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);
      requestAquiredUser['gidGroupBy'] = ['referer', 'time_key'];
      requestAquiredUser['groupByTimeKey'] = { key: [], interval: 'daily' };
      requestAquiredUser['orderBy'] = [{ key: 'time_key', opcode: 'asc' }];
      if(this.toggleBotStatus !== null)
        requestAquiredUser['filters']['dimensions'].push({ key: 'bot_status', values: [this.toggleBotStatus ? '1' : '0'] });
      requestAquiredUser['filters']['dimensions'].push({ key: 'library_name', values: ['analytics-web'] });
      this.getAcquiredUsersOverview(requestAquiredUser, 1);
    }
  }

  getDeviceType(request) {
    this.devicePiaChartJson = {
      chartTypes: [{ key: 'doughnut', label: 'Pie Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: false,
          text: 'Sessions by Device Type'
        },
        legend: {
          display: false
        },
        elements: {
          arc: {
            borderWidth: 0
          }
        },
        tooltips: {
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${data.labels[tooltipItem.index]} : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
            }
          }
        },
        plugins: {
          labels: [
            {
              render: function (args) {
                return args.percentage > 10 ? args.percentage + '%' : '';
              },
              fontStyle: 'bold',
              fontColor: '#000',
              precision: 2
            }
          ],
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '330px'
    };
    this.showDevicePiaChart = false;
    this.noDataDevicePiaChart = false;
    this.dataFetchServ.getTableData(request).subscribe(res => {
      if (res['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(res['status_msg']);
        return;
      }
      if (!this.libServ.isEmptyObj(res['data'])) {
        const chartData = res['data'];
        if (typeof chartData === 'undefined' || !chartData.length) {
          this.noDataDevicePiaChart = true;
        } else {
          const deviceType = Array.from(
            new Set(chartData.map(s => s['device_type']))
          );
          const colors = this.libServ.dynamicColors(deviceType.length);
          this.devicePiaChartJson['chartData']['labels'] = deviceType;
          this.devicePiaChartJson['chartData']['datasets'][0]['data'] = Array.from(
            new Set(chartData.map(s => s['session_count']))
          );
          this.devicePiaChartJson['chartData']['datasets'][0][
            'backgroundColor'
          ] = colors;
          this.showDevicePiaChart = true;
          this.noDataDevicePiaChart = false;
        }
      }
    });
  }

  getGeoMap(request) {
    this.dataFetchServ.getGeoContriesData().subscribe(data => {
      const countries = ChartGeo.topojson.feature(data, data['objects']['countries']).features;
      // console.log(countries.map((d) => ({feature: d, value: Math.random()})));
      this.geoSessionChartJson = {
        chartTypes: [{ key: 'choropleth', label: 'Sessions by Country' }],
        chartData: {
          labels: [],
          datasets: [{
            label: 'Countries',
            data: [],
          }]
        },
        chartOptions: {
          title: {
            display: false,
            text: 'Sessions by Country'
          },
          showOutline: true,
          showGraticule: true,
          legend: {
            display: false
          },
          scale: {
            projection: 'equalEarth',
          },
          geo: {
            colorScale: {
              display: true,
              position: 'left'
            },
          },
          tooltips: {
            callbacks: {
              label: (tooltipItem, data) => {
                const currentValue = tooltipItem.value;
                return `${data.labels[tooltipItem.index]} : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
              }
            }
          },
          plugins: {}
        },
        zoomLabel: false,
        chartWidth: '',
        chartHeight: '431px'
      };

      this.showGeoSessionChart = false;
      this.noDataGeoSessionChart = false;
      this.dataFetchServ.getTableData(request).subscribe(response => {
        if (response['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(response['status_msg']);
          return;
        }
        if (!this.libServ.isEmptyObj(response['data'])) {
          const chartData = response['data'];
          if (typeof chartData === 'undefined' || !chartData.length) {
            this.noDataGeoSessionChart = true;
          } else {

            const session = [];
            const label = [];
            countries.forEach(d => {
              chartData.forEach(element => {
                if (d.properties.name === element['country_name']) {
                  label.push(element['country_name']);
                  session.push({
                    feature: d,
                    value: element['session_count']
                  });
                }
              });
            });
            this.geoSessionChartJson['chartData']['labels'] = label;
            this.geoSessionChartJson['chartData']['datasets'][0]['data'] = session;

            this.showGeoSessionChart = true;
            this.noDataGeoSessionChart = false;
          }
        }

      });
    });
  }

  onLazyLoadFlatTable(e: Event, tbleName) {
    console.log('tbleName', tbleName, e)
    if (tbleName == 'PageViewCount') {
      const requestPageViewCount = this.libServ.deepCopy(this.requestParam);
      // requestPageViewCount['metrics'] = ['user_count','page_views'];
      requestPageViewCount['metrics'] = ['user_count',
        'page_views', 'session_count',
        'session_duration',
        'bounce_rate']
      requestPageViewCount['dimensions'] = ['decoded_page_url'];
      requestPageViewCount['timeKeyFilter'] = this.filtersApplied['timeKeyFilter'];
      requestPageViewCount['gidGroupBy'] = ['decoded_page_url'];
      requestPageViewCount['orderBy'] = [{ key: 'page_views', opcode: 'desc' }];
      requestPageViewCount['isCount'] = true;
      let orderby = [];
      if (
        typeof e['sortField'] !== 'undefined' &&
        e['sortField'] !== null
      ) {
        orderby.push({
          key: e['sortField'],
          opcode: e['sortOrder'] === -1 ? 'desc' : 'asc'
        });
      } else {
        orderby = [{ key: 'page_views', opcode: 'desc' }];
      }
      requestPageViewCount['limit'] = e['rows'];
      requestPageViewCount['offset'] = e['first'];
      requestPageViewCount['orderBy'] = orderby;
      if(this.toggleBotStatus !== null)
        requestPageViewCount['filters']['dimensions'].push({ key: 'bot_status', values: [this.toggleBotStatus ? '1' : '0'] });
      requestPageViewCount['filters']['dimensions'].push({ key: 'library_name', values: ['analytics-web'] });
      this.getPageViewCount(requestPageViewCount);

    }
    if (tbleName == 'pageViewAndroid') {
      const requestAndroidScreenViewCount = this.libServ.deepCopy(this.requestParam);
      requestAndroidScreenViewCount['metrics'] = ['user_count',
        'page_views', 'session_count',
        'session_duration',
        'bounce_rate'];
      requestAndroidScreenViewCount['dimensions'] = ['page_path'];
      requestAndroidScreenViewCount['timeKeyFilter'] = this.filtersApplied['timeKeyFilter'];
      requestAndroidScreenViewCount['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);
      requestAndroidScreenViewCount['filters']['dimensions'] = [
        { key: 'library_name', values: ['analytics-android'] }
      ];
      requestAndroidScreenViewCount['gidGroupBy'] = ['page_path'];
      requestAndroidScreenViewCount['orderBy'] = [{ key: 'page_views', opcode: 'desc' }];
      requestAndroidScreenViewCount['isCount'] = true;
      let orderby = [];
      if (
        typeof e['sortField'] !== 'undefined' &&
        e['sortField'] !== null
      ) {
        orderby.push({
          key: e['sortField'],
          opcode: e['sortOrder'] === -1 ? 'desc' : 'asc'
        });
      } else {
        orderby = [{ key: 'page_views', opcode: 'desc' }];
      }

      requestAndroidScreenViewCount['limit'] = e['rows'];
      requestAndroidScreenViewCount['offset'] = e['first'];
      requestAndroidScreenViewCount['orderBy'] = orderby;
      if(this.toggleBotStatus !== null)
        requestAndroidScreenViewCount['filters']['dimensions'].push({ key: 'bot_status', values: [this.toggleBotStatus ? '1' : '0'] });
      this.getAndroidScreenViewCount(requestAndroidScreenViewCount);
    }

    if (tbleName == 'eventTable') {
      const requestAndroidEventNamesAndCount = this.libServ.deepCopy(this.requestParam);
      requestAndroidEventNamesAndCount['metrics'] = ['event_count'];
      requestAndroidEventNamesAndCount['dimensions'] = ['event_name'];
      requestAndroidEventNamesAndCount['timeKeyFilter'] = this.filtersApplied['timeKeyFilter'];
      requestAndroidEventNamesAndCount['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);
      requestAndroidEventNamesAndCount['filters']['dimensions'] = [
        { key: 'library_name', values: ['analytics-android'] }
      ];
      requestAndroidEventNamesAndCount['gidGroupBy'] = ['event_name'];
      requestAndroidEventNamesAndCount['orderBy'] = [{ key: 'event_count', opcode: 'desc' }];
      requestAndroidEventNamesAndCount['isCount'] = true;
      let orderby = [];
      if (
        typeof e['sortField'] !== 'undefined' &&
        e['sortField'] !== null
      ) {
        orderby.push({
          key: e['sortField'],
          opcode: e['sortOrder'] === -1 ? 'desc' : 'asc'
        });
      } else {
        orderby = [{ key: 'event_count', opcode: 'desc' }];
      }

      requestAndroidEventNamesAndCount['limit'] = e['rows'];
      requestAndroidEventNamesAndCount['offset'] = e['first'];
      requestAndroidEventNamesAndCount['orderBy'] = orderby;
      if(this.toggleBotStatus !== null)
        requestAndroidEventNamesAndCount['filters']['dimensions'].push({ key: 'bot_status', values: [this.toggleBotStatus ? '1' : '0'] });
      this.getAndroidEventNamesAndCount(requestAndroidEventNamesAndCount);
    }
  }

  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }

  openPopup(rowData) {
    const data = {
      rowData: rowData,
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      toggleBotStatus: this.toggleBotStatus
    }
    const ref = this.dialogService.open(AnalyticsDailyDataPopupComponent, {
      header: 'Daily data: ' + rowData['decoded_page_url'],
      contentStyle: { width: '80vw', height: '90vh', overflow: 'auto' },
      data: data
    });
    ref.onClose.subscribe((data1: string) => { });
  }
}
