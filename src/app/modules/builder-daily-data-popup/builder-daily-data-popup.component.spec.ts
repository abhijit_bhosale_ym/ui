import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuilderDailyDataPopupComponent } from './builder-daily-data-popup.component';

describe('Rev360DailyDataPopupComponent', () => {
  let component: BuilderDailyDataPopupComponent;
  let fixture: ComponentFixture<BuilderDailyDataPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BuilderDailyDataPopupComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuilderDailyDataPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
