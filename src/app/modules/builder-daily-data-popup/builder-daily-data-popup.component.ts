import { Component, OnInit } from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { FetchApiDataService } from '../builder-app/fetch-api-data.service';
import * as moment from 'moment';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { TreeNode } from 'primeng/api';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { map, startWith, filter, pluck } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-builder-daily-data-popup',
  templateUrl: './builder-daily-data-popup.component.html',
  styleUrls: ['./builder-daily-data-popup.component.scss']
})
export class BuilderDailyDataPopupComponent implements OnInit {
  lineChartJson: object;
  showLineChart = false;
  filtersApplied: object;
  tableDataReq: object;
  tableData: TreeNode[];
  tableColumnDef: any[];
  dimColDef: any[];
  tableJson: Object;
  showMainLineChart = false;
  noDataMainLineChart = false;
  mainLineChartJson: object;
  tData: any;
  heading = '';
  name: any;
  description: any;
  expression1: any;
  exportRequest: ExportRequest = <ExportRequest>{};
  dspList: any;

  constructor(
    private libServ: CommonLibService,
    private formatNumPipe: FormatNumPipe,
    private dataFetchServ: FetchApiDataService,
    private config: DynamicDialogConfig,
    private exportService: ExportdataService,
    private toastService: ToastService
  ) {
    // console.log(" ===============this.config", this.config);
    this.filtersApplied = this.config.data['filters'];
  }


  myControl = new FormControl();
  hiddenCtrl = new FormControl();

  private fieldList: SuggestionDetails = { Name: Action.Field, Value: [], Valid: ['string'] };
  private operatorList: SuggestionDetails = { Name: Action.Operator, Value: ['=', '!=', '>'], Valid: ['string'] };
  private valueList: SuggestionDetails = { Name: Action.Value, Value: [], Valid: ['string'] };
  private expressionList: SuggestionDetails = { Name: Action.Expression, Value: ['And', 'Or'], Valid: ['string'] };

  private operator: string[] = this.operatorList.Value;
  private value: string[] = this.valueList.Value;
  private expression: string[] = this.expressionList.Value;

  private get field(): string[] {
    return this.fieldList.Value;
  }

  filteredOptions: Observable<string[]>;
  private searchList: SelectedOption[] = [];

  private get selectionList(): SelectionDict[] {
    return [
      { Name: Action.Field, Value: this.field, NextSelection: Action.Operator },
      { Name: Action.Operator, Value: this.operator, NextSelection: Action.Value },
      { Name: Action.Value, Value: this.value, NextSelection: Action.Expression },
      { Name: Action.Expression, Value: this.expression, NextSelection: Action.Field }
    ];
  }

  private defaultSelection: string = Action.Field;
  private currentEvent: string;
  private response: ApiResponse[] = [];




  ngOnInit() {

    this.getDsps();

    this.fieldList;
    this.getSearchObject();
    this.filteredOptions = this.myControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );

    // console.log(".............filter : ",this.filteredOptions);


    this.dimColDef = [

      {
        field: 'name',
        displayName: 'Name',
        format: '',
        width: '200',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'expression',
        displayName: 'Builder Expression',
        format: '',
        width: '140',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: 0
      },
      {
        field: 'attributes',
        displayName: 'Attributes',
        format: '',
        width: '140',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: 0
      },
     {
      field: 'source',
      displayName: 'Sources',
      format: '',
      width: '140',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: true
      },
      footerTotal: 0
    },
    ];
    this.tableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '300px',
      totalRecords: 1000,

      columns: this.dimColDef,
      selectedColumns: this.dimColDef,
      // frozenCols: [],
      // frozenWidth: '0px',
      // columns: this.aggTableColumnDef.slice(5),
      // selectedColumns: this.aggTableColumnDef.slice(5),
      // frozenCols: [...this.aggTableColumnDef.slice(0, 5)],
      // frozenWidth:
      //   this.aggTableColumnDef
      //     .slice(0, 5)
      //     .reduce((tot, cur) => tot + parseInt(cur.width), 0) + 'px',

      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: false,
      overallSearch: false,
      columnSearch: false
    };
    this.loadTable();
    // this.loadMainLineChart();
  }

  loadTable() {
 //   this.tableJson['loading'] = true;
    // this.tableDataReq = {
    //   dimensions: ['time_key', 'unified_pricing_rule'],
    //   metrics: [
    //     'adx_ad_request',
    //     'adx_impressions',
    //     'total_estimated_revenue',
    //     'total_impressions',
    //     'adx_estimated_revenue',
    //     'adx_impressions',
    //     'adx_matched_request',
    //     'yield_group_estimated_revenue',
    //     'yield_group_impressions'
    //   ],
    //   derived_metrics: [
    //     'yield_group_ecpm',
    //     'adx_ecpm',
    //     'total_ecpm',
    //     'adx_fill_rate'
    //   ],
    //   timeKeyFilter: this.libServ.deepCopy(
    //     this.filtersApplied['timeKeyFilter']
    //   ),
    //   filters: this.libServ.deepCopy(this.filtersApplied['filters']),
    //   groupByTimeKey: {
    //     key: ['time_key', 'accounting_key'],
    //     interval: 'daily'
    //   },
    //   gidGroupBy: ['unified_pricing_rule'],
    //   orderBy: [{ key: 'time_key', opcode: 'desc' }],
    //   limit: '',
    //   offset: ''
    // };

    // this.dataFetchServ.getUPRData(this.tableDataReq).subscribe(data => {
    //   if (data['status'] === 0) {
    //     this.toastService.displayToast({
    //       severity: 'error',
    //       summary: 'Server Error',
    //       detail: 'Please refresh the page'
    //     });
    //     console.log(data['status_msg']);
    //     return;
    //   }

    const data = {
      'data': [
        {
          'name': 'Donor above 50k dollor',
          'expression': 'builder expression1',
          'attributes': 'Chrismas high value donor',
          'source': 'CRM'
        },
        {
          'name': 'Donor above 50k dollor',
          'expression': 'builder expression2',
          'attributes': 'Frequent site visitors',
          'source': 'Mail '
        }
      ],
      'totalItems': 2
    };


      this.tData = data['data'];
      const tableData = data['data'];

      console.log('data ', this.tData);


      const arr = [];
      tableData.forEach((row: object) => {
        const obj = {
          data: row
        };
        arr.push(obj);
//      });

console.log('arr', arr);


      this.tableData = <TreeNode[]>arr;
      this.tableJson['totalRecords'] = data['totalItems'];
      this.tableJson['loading'] = false;

  //    this.loadMainLineChart();
    });


  }

  exportTablePopup(fileFormat) {
    if (this.exportRequest['sendEmail'].length > 0) {
      this.toastService.displayToast({
        severity: 'info',
        summary: 'Export Report',
        detail:
          'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
        life: 10000
      });
    const columnDefs = this.libServ.deepCopy(this.dimColDef);
    const req = this.libServ.deepCopy(this.tableDataReq);
    req['limit'] = '';
    req['offset'] = '';
    req['isTable'] = false;
      const sheetDetailsarray = [];
      const sheetDetails = {};
      sheetDetails['columnDef'] = this.libServ.deepCopy(columnDefs);
      sheetDetails['data'] = [];
      sheetDetails['sheetName'] = 'Daily Breakdown';
      sheetDetails['isRequest'] = true;
      sheetDetails['request'] = {
        url: '/api/flask/upr/cassandradata',
        method: 'POST',
        param: req
      };
      sheetDetails['disclaimer'] = [
        {
          position: 'top',
          label:
            'Data persent in below table is not final and may varies over period of time',
          color: '#3A37CF'
        },
        {
          position: 'bottom',
          label: 'Thank You',
          color: '#EC6A15'
        }
      ];
      sheetDetails['totalFooter'] = {
        available: true,
        custom: true
      };
      sheetDetails['tableTitle'] = {
        available: true,
        label: this.heading + ' Daily Breakdown'
      };
      sheetDetails['image'] = [
        {
          available: true,
          path: environment.exportConfig.exportLogo,
          position: 'top'
        }
      ];
      sheetDetailsarray.push(sheetDetails);
      this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
      this.exportRequest['fileName'] = this.heading + ' Daily Breakdown ' +
        moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
      this.exportRequest['exportFormat'] = fileFormat;
      this.exportRequest['exportConfig'] = environment.exportConfig;
      this.dataFetchServ
        .getExportReportData(this.exportRequest)
        .subscribe(response => {
          console.log(response);
        });

  } else {
    this.toastService.displayToast({
      severity: 'error',
      summary: 'Export Report',
      detail:
        'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
      life: 10000
    });
  }
  }


  getDsps() {

  //  console.log("===============================>>>>",this.filtersApplied['filters']);


    const tableReq = {
      dimensions: ['dsp_name'],
      metrics: [],
      derived_metrics: [],
      timeKeyFilter:
        this.filtersApplied['timeKeyFilter'],
      filters: this.filtersApplied['filters'],
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: ['audience_name', 'dsp_name'],
      orderBy: [],
      limit: '',
      offset: ''
    };

    this.dataFetchServ.getRevMgmtData(tableReq).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }

      const childData = data['data'];
      this.dspList = childData;

      this.dspList.forEach(element => {

        element['sync_timestamp'] = moment(new Date(element['sync_timestamp'])).format('YYYY-MM-DD HH:MM:SS');
      });

    });
  }

  loadMainLineChart() {
    const deviceTypeLineChartReq = {
      dimensions: ['time_key'],
      metrics: ['gross_revenue', 'dp_impressions'],
      derived_metrics: ['gross_ecpm'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      // orderBy: [],
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };

    const colors = this.libServ.dynamicColors(2);
    this.mainLineChartJson = {
      chartTypes: [{ key: 'line', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'Revenue',
            type: 'line',
            yAxisID: 'y-axis-0',

            borderColor: colors[0],
            fill: false,
            backgroundColor: colors[0],
            data: []
          },
          {
            label: 'eCPM',
            type: 'line',
            yAxisID: 'y-axis-1',

            borderColor: colors[1],
            fill: false,
            backgroundColor: colors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Overall Revenue and eCPM Trend'
        },
        legend: {
          display: true,
          onClick: e => e.stopPropagation()
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: ''
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: 'Revenue ($)'
              },
              position: 'right',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', [0]);
                }
              }
              // scaleFontColor: "rgba(151,137,200,0.8)"
            },
            {
              id: 'y-axis-1',
              scaleLabel: {
                display: true,
                labelString: 'eCPM ($)'
              },
              position: 'left',
              name: '2',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', [0]);
                }
              }
              // scaleFontColor: "rgba(151,187,205,0.8)"
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
              } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    this.showMainLineChart = false;
      const chartData = this.tData;

      this.noDataMainLineChart = false;
      if (!chartData.length) {
        this.noDataMainLineChart = true;
        return;
      }
      const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));
      const datearr = datesArr.reverse();

      this.mainLineChartJson['chartData']['labels'] = datearr.map(d =>
        moment(d, 'YYYYMMDD').format('YYYY-MM-DD')
      );

      const revArr = [];
      const ecpmArr = [];
      datearr.forEach(time_key => {
        chartData.forEach(r => {
          if (r['time_key'] === time_key) {
            revArr.push(r['total_estimated_revenue']);
            ecpmArr.push(r['total_ecpm']);
          }
        });
      });

      this.mainLineChartJson['chartData']['datasets'][0]['data'] = revArr;
      this.mainLineChartJson['chartData']['datasets'][1]['data'] = ecpmArr;
      this.showMainLineChart = true;
  }

  isHiddenColumn(col: Object) {}

  tabChanged(event) {

  }


  getSearchObject(): void {
    // HTTP response
    // var response = from([
    //   {
    //     "Result": {
    //       "DisplayName": "Name",
    //       "SearchType": "Field",
    //       "AutoCompleteValues": [
    //         "Vijaya",
    //         "Shrikant",
    //         "Sunil",
    //         "Vrishank",
    //         "Rupali"
    //       ]
    //     }
    //   },
    //   {
    //     "Result": {
    //       "DisplayName": "Company",
    //       "SearchType": "Field",
    //       "AutoCompleteValues": [
    //         "Youtube",
    //         "Git",
    //         "Techno",
    //         "Saviour",
    //         "TechnoSaviour"
    //       ]
    //     }
    //   },
    //   {
    //     "Result": {
    //       "DisplayName": "Language",
    //       "SearchType": "Field",
    //       "AutoCompleteValues": [
    //         "DotNet",
    //         "Python",
    //         "Java",
    //         "Javascript",
    //         "Typescript"
    //       ]
    //     }
    //   }
    // ]);


    this.dataFetchServ.getFieldList().subscribe(data => {

    //   console.log("==============data",JSON.parse(data));

      const data1 = JSON.parse(data);

      // console.log("DATA1",data1.data);

      this.response.push(data1['data']);
      // console.log('this.response',this.response);

      data1['data'].forEach(val => {
      this.response.push(val['Result']);
      this.fieldList.Value = this.response.filter(r => r.SearchType == Action.Field).map<string>(r => r.DisplayName.toString());
      this.myControl.setValue(''); // trigger the autocomplete to populate new values
   });
  });




    // response.subscribe(val => {
    //   this.response.push(val.Result);
    //   this.fieldList.Value = this.response.filter(r => r.SearchType == Action.Field).map<string>(r => r.DisplayName.toString());
    //   this.myControl.setValue(''); // trigger the autocomplete to populate new values
    // })

    // console.log("response",response);

  }

  // autocomplete material ui events
  _filter(value: string): string[] {

   // console.log("==========filter",value);


    const optionListToBePopulated: string[] = this.getOptionList();

    console.log('optionListToBePopulated', optionListToBePopulated);
    const searchText = this.getSearchText(value);

    // console.log("searchText",searchText);
    // console.log("searchText.includes('(')",searchText.includes('('));


 //  if(!searchText.includes('(')){
   //   console.log("in if");

      return optionListToBePopulated.filter(option => option.toLowerCase().indexOf(searchText.toLowerCase().trim()) != -1);
    // }
    // else{
      // console.log("in else");
      // return optionListToBePopulated;
   // }
    }



  displayFn(value: string): string {

     console.log('in display...............');

     console.log('value', value);
     console.log('this.currentEvent', this.currentEvent);


    console.log('========value', value);

   // console.log("......................",this.);


    if (!!value) {
      this.searchList.push(new SelectedOption(value, this.currentEvent, this.getNextEvent(this.currentEvent), ''));
    }

      // console.log("this.searchList",this.searchList);


    return this.searchList.length > 0 ? this.searchList.map(s => s.Value).join(' ') : '';
  }

  // private functions
  // ----------oldText--- Get Autocomplete List START --------------------
  private getOptionList(): string[] {

    // console.log("on option list");


    if (this.searchList == null || this.searchList == undefined || this.searchList.length === 0) {
      this.currentEvent = this.defaultSelection;
      return this.field;
    }

    const lastElement: SelectedOption = <SelectedOption>this.searchList.slice(-1).pop();
    const currentList = this.selectionList.find(s => s.Name.toLowerCase() === lastElement.Next.toLowerCase());
    this.currentEvent = currentList ? currentList.Name : this.defaultSelection;

    // console.log("===============this.searchList",this.searchList);
     console.log('===========currentList', currentList);
    // console.log("=========this.field",this.field);


    return currentList ? this.getValues(currentList) : this.field;
  }

  private getValues(currentList: SelectionDict): string[] {
    if (this.currentEvent.toLowerCase() != 'value') { return currentList.Value; }

    const selectedField = this.getlastField();
    const selectedValue = selectedField ? selectedField.Value : '';
    const filteredResponse = this.response.find(r => r.DisplayName === selectedValue);

     console.log('============selectedField==============', selectedField);

     console.log('==============selectedValue', selectedValue);
     console.log('===========Response============', filteredResponse);
     console.log('==========filteredResponse.AutoCompleteValues', filteredResponse);

    if (filteredResponse == undefined || filteredResponse.AutoCompleteValues.length == 0) {
      this.dataFetchServ.getValueList(filteredResponse['key']).subscribe(data => {

     //   console.log("=================== api response ====>>>>>>>>>>============",JSON.parse(data));

        const res = JSON.parse(data);

     //   console.log("res=========",res);


        filteredResponse.AutoCompleteValues = res['data'];

      });
    }

  //  return filteredResponse ? filteredResponse.AutoCompleteValues : [];
    return filteredResponse.AutoCompleteValues;

  }
  // ------------- Get Autocomplete List END --------------------



  // --------------- START : Get the search text based on which the autocomplete will populate --------
  private getSearchText(value: string): string {
    const oldText = this.searchList.map(s => s.Value).join(' ');
    this.handleBackspace(value);
    return value.trim().replace(oldText, '');
  }

  private handleBackspace(searchValue: string): void {
    const oldText = this.searchList.map(s => s.Value).join(' ');
    const previousListName = this.searchList.length != 0 ? this.searchList[this.searchList.length - 1].PopulatedFrom : '';
    const prevList = this.selectionList.find(s => s.Name.toLowerCase() === previousListName.toLowerCase());
    let prevListValue = prevList ? prevList.Value : [];


    if (previousListName == Action.Value) {
      const lastField = this.getlastField();
      const lastFieldValue = lastField ? lastField.Value : '';
      const filteredResponse = this.response.find(r => r.DisplayName === lastFieldValue);
      prevListValue = filteredResponse ? filteredResponse.AutoCompleteValues : [];
    }

    if ((prevListValue ? prevListValue.indexOf(searchValue) === -1 : false) && oldText.trim().length > searchValue.trim().length) {
      this.searchList.pop();
    }
  }

  // --------------- END : Get the search text based on which the autocomplete will populate --------

  private getNextEvent(currentEvent: string): string {


   // console.log("in next event ");


    const currentList = this.selectionList.find(s => s.Name.toLowerCase() === currentEvent.toLowerCase());
    return currentList ? currentList.NextSelection : this.defaultSelection;


    console.log('============', this.expression);
    console.log('=============', this.expressionList);



  }

  private getlastField(): SelectedOption | undefined {
    if (this.searchList.length === 0) { return undefined; }
    let i: number = this.searchList.length - 1;
    for (i; i >= 0; i--) {
      if (this.searchList[i].PopulatedFrom == Action.Field) {
        return this.searchList[i];
      }
    }
    return undefined;
  }


  selectEvent(item: any) {
    // do something with selected item
    console.log('on select', item);

  }

  onChangeSearch(val: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.


  //  console.log("===========onsearch change========",val);

    this.getOptionList();
    console.log('on search change', val);
    this.displayFn(val);

  }

  onFocused(e: any) {
    // do something when input is focused
    console.log('om focuses', e);

  }

  onSave() {
    console.log('=======on save============', this.searchList);

   // let str = this.searchList()

  }

}

class SelectedOption {
  public Value: string;
  public PopulatedFrom: string;
  public Next: string;
  public key: string;

  constructor(value: string, populatedFrom: string, next: string, key: string ) {
    this.Value = value;
    this.PopulatedFrom = populatedFrom;
    this.Next = next;
    this.key = key;
  }

}
class SuggestionDetails {
  public Name: string;
  public Valid: string[];
  public Value: string[];
}

class SelectionDict {
  public Name: string;
  public Value: string[];
  public NextSelection: string;
}

// Server response
class ApiResponse {
  public DisplayName: string;
  public SearchType: string;
  public AutoCompleteValues: string[];
}

enum Action {
  Field = 'Field',
  Operator = 'Operator',
  Value = 'Value',
  Expression = 'Expression'
}
