import {
  Component,
  OnInit,
  QueryList,
  ElementRef,
  ViewChildren,
  OnDestroy,
  ViewChild,
  AfterViewInit,
  AfterContentInit
} from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { FetchApiDataService } from './fetch-api-data.service';
import { TreeNode } from 'primeng/api';
import { ToastService } from '../../_services/toast-notification/toast.service';
import { DialogService } from 'primeng/dynamicdialog';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import * as moment from 'moment';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { Title } from '@angular/platform-browser';
import { DataShareService } from 'src/app/_services/data-share/data-share.service';
import { Subscription } from 'rxjs';

import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { FilterContainerComponent } from '../common/filter-container/filter-container.component';
@AutoUnsubscribe()
@Component({
  selector: 'ym-date-compare-app',
  templateUrl: './date-compare-app.component.html',
  styleUrls: ['./date-compare-app.component.css']
})
export class DateCompareAppComponent
  implements OnInit, OnDestroy, AfterContentInit {
  private appConfigObs: Subscription = null;
  appConfig: object = {};

  showMainLineChart = false;
  noDataMainLineChart = false;
  noDataMsg = 'No data found.';
  showCharts = false;
  defaultChartsJson: object;
  mainLineChartJson: object;
  mainPieChartJson: object;
  revenueChartJson: object;
  cpcChartJson: object;
  ctrChartJson: object;
  cpmChartJson: object;
  clicksChartJson: object;
  impressionsChartJson: object;
  dimensionsDropdown: any[];
  selectedDimension: any[];
  metricsDropdown: any[];
  selectedMetric: any[];
  metricChartJson: object;
  noDataMetricChart = false;
  selectedTabIndex = 1;
  displayAggTable = true;
  showMetricChart = false;
  noTableData = false;
  orderByDefault = 1;
  dateRange: {};
  seletedValue: object = { Dimension: 'source', Metric: 'dp_revenue' };

  chartsDimMultiselectButton: object = {
    data: [
      {
        label: 'Source',
        value: 'source',
        isAvailable: true
      },
      {
        label: 'Publisher',
        value: 'publisher',
        isAvailable: false
      },
      {
        label: 'Publisher Site URL',
        value: 'website',
        isAvailable: false
      },
      {
        label: 'Country',
        value: 'country',
        isAvailable: false
      },
      {
        label: 'Geography',
        value: 'geography',
        isAvailable: false
      }
    ],
    model: 'source'
  };
  chartsMetricsMultiselectButton: object = {
    data: [
      {
        label: 'Revenue',
        value: 'dp_revenue',
        type: 'metric',
        dimensionsList: [],
        format: '$',
        formatValue: '$',
        digits: []
      },
      {
        label: 'Impressions',
        value: 'dp_impressions',
        type: 'metric',
        dimensionsList: [],
        format: '',
        formatValue: 'number',
        digits: []

      },
      {
        label: 'Clicks',
        value: 'dp_clicks',
        type: 'metric',
        dimensionsList: [],
        format: '',
        formatValue: 'number',
        digits: []
      },
      {
        label: 'CTR',
        value: 'ctr',
        type: 'derivedmetric',
        dimensionsList: ['dp_clicks', 'dp_impressions'],
        format: '%',
        formatValue: 'percentage',
        digits: [2]
      },
      {
        label: 'CPC',
        value: 'cpc',
        type: 'derivedmetric',
        dimensionsList: ['dp_revenue', 'dp_clicks'],
        format: '$',
        formatValue: '$',
        digits: []
      },
      {
        label: 'CPM',
        value: 'cpm',
        type: 'derivedmetric',
        dimensionsList: ['dp_revenue', 'dp_impressions'],
        format: '$',
        formatValue: '$',
        digits: []

      }
    ],
    model: 'dp_revenue'
  };

  @ViewChildren('ttAgg') aggTableRef: QueryList<ElementRef>;
  @ViewChildren('ttFlat') flatTableRef: QueryList<ElementRef>;
  // @ViewChild(FilterContainerComponent) filters: FilterContainerComponent;
  @ViewChildren(FilterContainerComponent) filters: FilterContainerComponent; // QueryList<DaterangePickerComponent>;

  lastUpdatedOn: Date;
  /* ---------------------------------- Table --------------------------------- */

  filtersApplied: object = {};

  /* ---------------------------------- Table --------------------------------- */

  constructor(
    private appConfigService: AppConfigService,
    private _titleService: Title,
    private libServ: CommonLibService,
    private dataFetchServ: FetchApiDataService,
    private toastService: ToastService,
    private dialogService: DialogService,
    private htmltoimage: HtmltoimageService,
    private pptExport: ExportPptService,
    private exportService: ExportdataService,
    private formatNumPipe: FormatNumPipe
  ) { }

  ngOnInit() {
    /* --------------------------------- Charts --------------------------------- */
    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        console.log('appConfig', this.appConfig);
        this._titleService.setTitle(
          this.appConfig['displayName']);
        let startDate;
        let endDate;
        const date_config = this.appConfig['filter']['filterConfig']['filters'][
          'datePeriod'
        ][0];
        if ((date_config['defaultDate'][0]['value'] === 0 && date_config['defaultDate'][0]['period'] === 'month') && (moment().format('DD') === '01' || moment().format('DD') === '02')) {
          startDate = moment().subtract(1, 'months').startOf('month');
          endDate = moment().subtract(1, 'months').endOf('month');
        } else {
          if (
            date_config[
            'defaultDate'
            ][0]['startOf']
          ) {
            startDate = moment()
              .subtract(
                date_config['defaultDate'][0]['value'],
                date_config['defaultDate'][0]['period']
              )
              .startOf(
                date_config['defaultDate'][0]['period']
              );
          } else {
            startDate = moment().subtract(
              date_config['defaultDate'][0]['value'],
              date_config['defaultDate'][0]['period']
            );
          }
          endDate = moment().subtract(
            date_config[
            'defaultDate'
            ][1]['value'],
            date_config[
            'defaultDate'
            ][1]['period']
          );
        }


        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: startDate.format('YYYYMMDD'),
            time_key2: endDate.format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: this.appConfig['filter']['filterConfig']['groupBy'].filter(
            v => v.selected
          )
        };

        this.initialLoading();
      }
    });
  }

  ngAfterContentInit() {
  }

  /* ------------------------------- Table Start ------------------------------ */

  initialLoading() {
    console.log('this.appConfig last up', this.appConfig['id']);
    // this.dataFetchServ
    //   .getLastUpdatedData(this.appConfig['id'])
    //   .subscribe(data => {
    //     if (data['status'] === 0) {
    //       this.toastService.displayToast({
    //         severity: 'error',
    //         summary: 'Server Error',
    //         detail: 'Please refresh the page'
    //       });
    //       console.log(data['status_msg']);
    //       return;
    //     }
    //     this.lastUpdatedOn = moment(data).toDate();
    //   });
  }

  /* --------------------- On Filter Apply Button Clicked --------------------- */

  loadMainLineChart(param, dateRange1) {
    // const colors = this.libServ.dynamicColors(8);
    this.dateRange = this.libServ.deepCopy(dateRange1);
    this.showMainLineChart = false;

    param['timeKeyFilter'] = {
      time_key1: this.dateRange['dateBucketCurrent'][0],
      time_key2: this.dateRange['dateBucketCurrent'][1]
    };

    let chartData = [];
    let chartData1 = [];

    this.dataFetchServ.getTableData(param).subscribe(data => {
      chartData = data['data'];

      if (this.dateRange['dateBucketPrevious']) {
        param['timeKeyFilter'] = {
          time_key1: this.dateRange['dateBucketPrevious'][0],
          time_key2: this.dateRange['dateBucketPrevious'][1]
        };

        this.dataFetchServ.getTableData(param).subscribe(data1 => {
          chartData1 = data1['data'];
          this.noDataMainLineChart = false;
          if (!chartData.length && !chartData1.length) {
            this.noDataMainLineChart = true;
            this.noDataMsg = 'No data found.';
            return;
          }

          this.drawChart(dateRange1, chartData, chartData1);
        });
      } else {
        this.noDataMainLineChart = false;
        if (!chartData.length) {
          this.noDataMainLineChart = true;
          this.noDataMsg = 'No data found.';
          return;
        }
        this.drawChart(dateRange1, chartData, chartData1);
      }
    });
  }

  drawChart(dateRange1, chartData, chartData1) {

    const metrisData = this.chartsMetricsMultiselectButton['data'].find(x => x.value == this.seletedValue['Metric']);
    const colors = ['#F78841', '#45E195', '#5996FB', '#E4E93A', '#FBF72C', '#24F7E5', '#4055FB', '#FB5994'];
    this.dateRange = this.libServ.deepCopy(dateRange1);

    this.showMainLineChart = false;
    let daysDiff = moment(
      this.dateRange['dateBucketCurrent'][0],
      'YYYYMMDD'
    ).diff(moment(this.dateRange['dateBucketCurrent'][1], 'YYYYMMDD'), 'days');


    if (this.dateRange['dateBucketPrevious'] != undefined) {
      const daysDiffPrev = moment(
        this.dateRange['dateBucketPrevious'][0],
        'YYYYMMDD'
      ).diff(moment(this.dateRange['dateBucketPrevious'][1], 'YYYYMMDD'), 'days');

      if (Math.abs(daysDiffPrev) > Math.abs(daysDiff)) {
        daysDiff = daysDiffPrev;
      }
    }

    const days = Math.abs(daysDiff);
    this.mainLineChartJson = {};

    const dimKey = this.seletedValue['Dimension'];

    const sources = this.filtersApplied['filters']['dimensions'].find(
      dims => dims.key == dimKey
    ).values;


    console.log('dimKey', dimKey, sources);
    const chartDataSet = {
      labels: [],
      datasets: [],
      isDateCompare: true
    };

    const dataObj = {};
    for (let i = 0; i < days + 1; i++) {
      let f = [];
      let f1 = [];
      const bkt1 = moment(this.dateRange['dateBucketCurrent'][0], 'YYYYMMDD')
        .add(Math.abs(i), 'days')
        .format('YYYYMMDD');

      chartDataSet.labels.push(moment(bkt1, 'YYYYMMDD').format('YYYY-MM-DD'));

      f = chartData.filter(item => item.time_key == bkt1);

      if (this.dateRange['dateBucketPrevious']) {
        const bkt2 = moment(this.dateRange['dateBucketPrevious'][0], 'YYYYMMDD')
          .add(Math.abs(i), 'days')
          .format('YYYYMMDD');

        f1 = chartData1.filter(item => item.time_key == bkt2);
      }

      sources.forEach(source => {
        typeof dataObj[`tk1>>>${source}`] === 'undefined'
          ? (dataObj[`tk1>>>${source}`] = [])
          : '';

        const tk1sdata = f.find(item => item[dimKey] === source);
        typeof tk1sdata !== 'undefined'
          ? dataObj[`tk1>>>${source}`].push(tk1sdata[this.seletedValue['Metric']])
          : dataObj[`tk1>>>${source}`].push(null);

        if (this.dateRange['dateBucketPrevious'] != undefined) {
          typeof dataObj[`tk2>>>${source}`] === 'undefined'
            ? (dataObj[`tk2>>>${source}`] = [])
            : '';

          const tk2sdata = f1.find(item => item[dimKey] === source);
          typeof tk2sdata !== 'undefined'
            ? dataObj[`tk2>>>${source}`].push(tk2sdata[this.seletedValue['Metric']])
            : dataObj[`tk2>>>${source}`].push(null);
        }
      });
    }
    let i = 0;

    const datestring1 = moment(this.dateRange['dateBucketCurrent'][0], 'YYYYMMDD').format('MM-DD-YYYY') + ' - ' + moment(this.dateRange['dateBucketCurrent'][1], 'YYYYMMDD').format('MM-DD-YYYY');
    let datestring2 = '';
    if (this.dateRange['dateBucketPrevious'] != undefined) {
      datestring2 = moment(this.dateRange['dateBucketPrevious'][0], 'YYYYMMDD').format('MM-DD-YYYY') + ' - ' + moment(this.dateRange['dateBucketPrevious'][1], 'YYYYMMDD').format('MM-DD-YYYY');
    }
    // const dataindex = 0;
    for (const sourceDataKey in dataObj) {
      const sourceName = sourceDataKey.split('>>>')[1];
      const bucketIndex = sourceDataKey.split('>>>')[0];
      chartDataSet.datasets.push({
        label: sourceName,
        data: dataObj[sourceDataKey],
        fill: false,
        borderColor: colors[i],
        displayLegend: true,
        bucketIndex: bucketIndex,
        bucketName: bucketIndex == 'tk1' ? datestring1 : datestring2,
        dataindex: i
      });
      i++;
    }

    const that = this;
    this.mainLineChartJson = {
      chartTypes: [{ key: 'line', label: 'Line Chart' }],
      chartData: chartDataSet,
      chartOptions: {
        isDateCompare: true,
        title: {
          display: true,
          text: metrisData['label'] + ' Comparison Trend'
        },
        legend: {
          display: true,
          labels: {
            fontColor: 'rgb(255, 99, 132)',
            filter: function (legendItem, data) {
              console.log('ddddddddd...', legendItem, data);
              return legendItem.index != 1;
            }
          }
        },

        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Dates'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: metrisData['format'] !== '' ? metrisData['label'] + ' (' + metrisData['format'] + ')' : metrisData['label']
              },
              position: 'left',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, metrisData['formatValue'], metrisData['digits']);
                }
              }
            }
          ]
        },
        tooltips: {
          enabled: false,
          mode: 'index',
          position: 'nearest',
          custom: function (tooltipModel) {
            // Tooltip Element
            let tooltipEl = document.getElementById('chartjs-tooltip');

            // Create element on first render
            if (!tooltipEl) {
              tooltipEl = document.createElement('div');
              tooltipEl.id = 'chartjs-tooltip';
              tooltipEl.innerHTML = '<table></table>';
              document.body.appendChild(tooltipEl);
            }

            // Hide if no tooltip
            if (tooltipModel.opacity === 0) {
              tooltipEl.style.opacity = '0';
              return;
            }

            // Set caret Position
            // tooltipEl.classList.remove('above', 'below', 'no-transform');
            if (tooltipModel.yAlign) {
              tooltipEl.classList.add(tooltipModel.yAlign);
            } else {
              tooltipEl.classList.add('no-transform');
            }

            function getBody(bodyItem) {
              return bodyItem.lines;
            }

            // Set Text
            if (tooltipModel.body) {
              const titleLines = tooltipModel.title || [];
              const bodyLines = tooltipModel.body.map(getBody);

              let innerHtml = '<tr><td><table><tbody>';

              if (tooltipModel.dataPoints) {
                let tk1Src1, tk1Src2: any;

                let style = '';
                style += '; height : 10px';
                style += '; width : 9px';
                style += '; display: inline-block';
                style += '; margin-right : 3px ';

                let isTitle = false;
                let isTitle1 = false;
                if (
                  sources[0] &&
                  dataObj[`${'tk1'}${'>>>'}${sources[0]}`][
                  tooltipModel.dataPoints[0].index
                  ] != null
                ) {

                  if (
                    chartData.length == 0 ||
                    chartData.length < chartData1.length
                  ) {

                    innerHtml +=
                      '<tr><th style="padding: 2px 0">' +
                      moment(that.dateRange['dateBucketCurrent'][0], 'YYYYMMDD')
                        .add(tooltipModel.dataPoints[0].index, 'days')
                        .format('YYYY-MM-DD') +
                      '</th></tr>';

                    isTitle = true;
                  } else {
                    titleLines.forEach(function (title) {

                      innerHtml += '<tr><th style="padding: 5px 0">' + title + '</th></tr>';
                      isTitle = true;
                    });
                  }

                  // let colors = tooltipModel.labelColors[0];
                  const bgColor = chartDataSet.datasets.find(x => x.bucketIndex == 'tk1' && x.label == sources[0]).borderColor;

                  style += '; background:' + bgColor;
                  style += '; border-color:' + bgColor;

                  const span = '<span style="' + style + '"></span>';

                  tk1Src1 =
                    dataObj[`${'tk1'}${'>>>'}${sources[0]}`][
                      tooltipModel.dataPoints[0].index
                    ] == null
                      ? '<style>display : none</style>'
                      : that.formatNumPipe.transform(
                        dataObj[`${'tk1'}${'>>>'}${sources[0]}`][
                        tooltipModel.dataPoints[0].index
                        ],
                        metrisData['formatValue'],
                        metrisData['digits']
                      ) + '</td></tr>';
                  innerHtml +=
                    '<tr><td style="padding: 5px 0">' +
                    span +
                    sources[0] +
                    ' : ' +
                    tk1Src1 +
                    '</td></tr>';
                }

                if (
                  sources[1] &&
                  dataObj[`${'tk1'}${'>>>'}${sources[1]}`][
                  tooltipModel.dataPoints[0].index
                  ] != null
                ) {


                  if (!isTitle) {
                    if (
                      chartData.length == 0 ||
                      chartData.length < chartData1.length
                    ) {

                      innerHtml +=
                        '<tr><th>' +
                        moment(that.dateRange['dateBucketCurrent'][0], 'YYYYMMDD')
                          .add(tooltipModel.dataPoints[0].index, 'days')
                          .format('YYYY-MM-DD') +
                        '</th></tr>';

                      isTitle = true;
                    } else {
                      titleLines.forEach(function (title) {

                        innerHtml += '<tr><th>' + title + '</th></tr>';
                        isTitle = true;
                      });
                    }
                  }

                  // let colors: any;
                  // if (tooltipModel.labelColors.length == 0)
                  //   colors = tooltipModel.labelColors[0];
                  // else
                  //   colors = tooltipModel.labelColors[1];
                  const bgColor = chartDataSet.datasets.find(x => x.bucketIndex == 'tk1' && x.label == sources[1]).borderColor;

                  style += '; background:' + bgColor;
                  style += '; border-color:' + bgColor;

                  const span = '<span style="' + style + '"></span>';

                  tk1Src2 =
                    dataObj[`${'tk1'}${'>>>'}${sources[1]}`][
                      tooltipModel.dataPoints[0].index
                    ] == null
                      ? '<style>display : none</style>'
                      : that.formatNumPipe.transform(
                        dataObj[`${'tk1'}${'>>>'}${sources[1]}`][
                        tooltipModel.dataPoints[0].index
                        ],
                        metrisData['formatValue'],
                        metrisData['digits']
                      ) + '</td></tr>';
                  innerHtml +=
                    '<tr><td style="padding: 5px 0">' +
                    span +
                    sources[1] +
                    ' : ' +
                    tk1Src2 +
                    '</td></tr>';
                }

                if (
                  that.dateRange['dateBucketPrevious'] != undefined &&
                  sources[0] &&
                  dataObj[`${'tk2'}${'>>>'}${sources[0]}`][
                  tooltipModel.dataPoints[0].index
                  ] != undefined &&
                  dataObj[`${'tk2'}${'>>>'}${sources[0]}`][
                  tooltipModel.dataPoints[0].index
                  ] != null
                ) {

                  const tempTitle = moment(that.dateRange['dateBucketPrevious'][0], 'YYYYMMDD')
                    .add(tooltipModel.dataPoints[0].index, 'days')
                    .format('YYYY-MM-DD');

                  if (tempTitle) {
                    isTitle1 = true;
                  }


                  innerHtml +=
                    '<tr><th>' +
                    moment(that.dateRange['dateBucketPrevious'][0], 'YYYYMMDD')
                      .add(tooltipModel.dataPoints[0].index, 'days')
                      .format('YYYY-MM-DD') +
                    '</th></tr>';

                  // let colors: any;
                  // if (tooltipModel.labelColors.length > 2)
                  //   colors = tooltipModel.labelColors[2];
                  // else colors = tooltipModel.labelColors[0];
                  const bgColor = chartDataSet.datasets.find(x => x.bucketIndex == 'tk2' && x.label == sources[0]).borderColor;

                  style += '; background:' + bgColor;
                  style += '; border-color:' + bgColor;

                  const span = '<span style="' + style + '"></span>';

                  const tk2Src1 =
                    dataObj[`${'tk2'}${'>>>'}${sources[0]}`][
                      tooltipModel.dataPoints[0].index
                    ] == null
                      ? '<style>display : none</style>'
                      : that.formatNumPipe.transform(
                        dataObj[`${'tk2'}${'>>>'}${sources[0]}`][
                        tooltipModel.dataPoints[0].index
                        ],
                        metrisData['formatValue'],
                        metrisData['digits']
                      ) + '</td></tr>';
                  innerHtml +=
                    '<tr><td style="padding: 5px 0">' +
                    span +
                    sources[0] +
                    ' : ' +
                    tk2Src1 +
                    '</td></tr>';
                }

                if (
                  that.dateRange['dateBucketPrevious'] != undefined &&
                  sources[1] &&
                  dataObj[`${'tk2'}${'>>>'}${sources[1]}`][
                  tooltipModel.dataPoints[0].index
                  ] != undefined &&
                  dataObj[`${'tk2'}${'>>>'}${sources[1]}`][
                  tooltipModel.dataPoints[0].index
                  ] != null
                ) {
                  // let colors: any;


                  if (!isTitle1) {
                    innerHtml +=
                      '<tr><th>' +
                      moment(that.dateRange['dateBucketPrevious'][0], 'YYYYMMDD')
                        .add(tooltipModel.dataPoints[0].index, 'days')
                        .format('YYYY-MM-DD') +
                      '</th></tr>';
                    isTitle1 = true;
                  }

                  // if (tooltipModel.labelColors.length > 3)
                  //   colors = tooltipModel.labelColors[3];
                  // else if (tooltipModel.labelColors.length == 3)
                  //   colors = tooltipModel.labelColors[2];
                  // else if (tooltipModel.labelColors.length == 2)
                  //   colors = tooltipModel.labelColors[1];
                  // else if (tooltipModel.labelColors.length == 1)
                  //   colors = tooltipModel.labelColors[0];
                  // else
                  //   colors = tooltipModel.labelColors[2];

                  const bgColor = chartDataSet.datasets.find(x => x.bucketIndex == 'tk2' && x.label == sources[1]).borderColor;

                  style += '; background:' + bgColor;
                  style += '; border-color:' + bgColor;

                  const span = '<span style="' + style + '"></span>';

                  const tk2Src2 =
                    dataObj[`${'tk2'}${'>>>'}${sources[1]}`][
                      tooltipModel.dataPoints[0].index
                    ] == null
                      ? '<style>display : none</style>'
                      : that.formatNumPipe.transform(
                        dataObj[`${'tk2'}${'>>>'}${sources[1]}`][
                        tooltipModel.dataPoints[0].index
                        ],
                        metrisData['formatValue'],
                        metrisData['digits']
                      ) + '</td></tr>';

                  innerHtml +=
                    '<tr><td style="padding: 5px 0">' +
                    span +
                    sources[1] +
                    ' : ' +
                    tk2Src2 +
                    '</td></tr>';

                }
              }

              const tableRoot = tooltipEl.querySelector('table');
              tableRoot.innerHTML = innerHtml;
            }

            // `this` will be the overall tooltip
            const position = this._chart.canvas.getBoundingClientRect();

            // Display, position, and set styles for font
            tooltipEl.style.opacity = '1';
            tooltipEl.style.position = 'absolute';
            tooltipEl.style.background = '#f2f7f8';
            tooltipEl.style.fontFamily = tooltipModel._bodyFontFamily;
            tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
            tooltipEl.style.fontStyle = tooltipModel._bodyFontStyle;
            tooltipEl.style.padding =
              tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';
            tooltipEl.style.pointerEvents = 'none';
            tooltipEl.style.left = (position.left + window.pageXOffset + tooltipModel.caretX) > (position.width - tooltipEl.clientWidth) ? ((position.left + window.pageXOffset + tooltipModel.caretX) - tooltipEl.clientWidth) + 'px' : position.left + window.pageXOffset + tooltipModel.caretX + 'px';
            tooltipEl.style.top =
              position.top + window.pageYOffset + tooltipModel.caretY - tooltipModel.height + 'px';

          }
        },
        pan: {
          enabled: true,
          mode: 'x'
        },
        zoom: {
          enabled: true,
          mode: 'x'
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };
    console.log('chartdata', this.mainLineChartJson);
    this.showMainLineChart = true;
  }

  getDateRange(startDate, endDate) {
    const now = startDate,
      dates = [];

    while (now.isSameOrBefore(endDate)) {
      dates.push(now.format('M/D/YYYY'));
      now.add(1, 'days');
    }

    return dates;
  }
  onFiltersApplied(filterData: object) {
    this.seletedValue = { Dimension: 'source', Metric: 'dp_revenue' };
    this.chartsMetricsMultiselectButton['model'] = this.seletedValue['Metric'];

    const filters = [];
    this.chartsDimMultiselectButton['data'].forEach(element => {
      element.isAvailable = false;
    });
    // tslint:disable-next-line: fori
    if (filterData['filter']['dimensions'] != {}) {
      this.filtersApplied['filters']['dimensions'] = [];
    }

    // tslint:disable-next-line: forin
    for (const k in filterData['filter']['dimensions']) {
      if (filterData['filter']['dimensions'][k].length !== 0) {
        this.filtersApplied['filters']['dimensions'].push({
          key: k,
          values: filterData['filter']['dimensions'][k]
        });

        this.chartsDimMultiselectButton['data'].find(x => x.value == k).isAvailable = true;
      }
    }
    if (this.filtersApplied['filters']['dimensions'].length == 0) {
      this.noDataMainLineChart = true;
      this.showMainLineChart = false;
      this.noDataMsg = 'Please select values from filter to see the comparison trend.';
      return;
    }
    this.chartsDimMultiselectButton['model'] = this.chartsDimMultiselectButton['data'].filter(x => x.isAvailable)[0]['value'];
    this.seletedValue['Dimension'] = this.chartsDimMultiselectButton['data'].filter(x => x.isAvailable)[0]['value'];
    this.filtersApplied['groupby'] = filterData['groupby'];
    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];
    // const metrisData = this.chartsMetricsMultiselectButton['data'].find(x => x.value == this.seletedValue['Metric']);

    const chartReq = {
      dimensions: ['time_key', this.seletedValue['Dimension']],
      metrics: ['dp_revenue'],
      derived_metrics: [],
      timeKeyFilter: [],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys(),
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };

    this.loadMainLineChart(chartReq, filterData['date']);
  }

  getGrpBys() {
    let grpBys = []; // [this.filtersApplied['groupby'][0]['key']];
    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          grpBys.concat(
            this.filtersApplied['filters']['dimensions']
              .filter(f => f.values.length)
              .map(m => m.key)
          )
        )
      );
    }
    return grpBys;
  }

  showToast() {
    this.toastService.displayToast({
      severity: 'success',
      summary: 'Saple Toast',
      detail: 'Sample App Loaded'
    });
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['displayName']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    } else if (type === 'ppt') {
      const chartElements = document.getElementsByTagName('ym-charts');
      const slides = [];
      [].forEach.call(chartElements, (el, index) => {
        const c = el.getElementsByTagName('canvas')[0];

        slides.push({
          title: `${this.appConfig['displayName']}`,
          base64data: this.libServ.canvasToImage('white', c),
          width: 8,
          height: 3
        });

        if (index === chartElements.length - 1) {
          const pptConfig = {
            filename: 'Charts',
            logo: {
              visible: true,
              path: '/assets/images/ym.png',
              width: 0.53125,
              height: 0.458333
            },
            slides: slides
          };
          this.pptExport.exportPPT(pptConfig);
        }
      });
    }
    return false;
  }
  changeValues() {

    if (this.filtersApplied['filters']['dimensions'].length == 0) {
      this.noDataMainLineChart = true;
      this.showMainLineChart = false;
      this.noDataMsg = 'Please select values from filter to see the comparison trend.';
      return;
    }
    // console.log('date...', this.chartsDimMultiselectButton, this.chartsMetricsMultiselectButton, this.seletedValue)
    if (this.seletedValue['Dimension'] != this.chartsDimMultiselectButton['model'] || this.seletedValue['Metric'] != this.chartsMetricsMultiselectButton['model']) {
      this.seletedValue['Dimension'] = this.libServ.deepCopy(this.chartsDimMultiselectButton['model']);
      this.seletedValue['Metric'] = this.libServ.deepCopy(this.chartsMetricsMultiselectButton['model']);
      const metrisData = this.chartsMetricsMultiselectButton['data'].find(x => x.value == this.seletedValue['Metric']);

      const chartReq = {
        dimensions: ['time_key', this.seletedValue['Dimension']],
        metrics: metrisData['type'] == 'metric' ? [metrisData['value']] : metrisData['dimensionsList'],
        derived_metrics: metrisData['type'] == 'derivedmetric' ? [metrisData['value']] : [],
        timeKeyFilter: [],
        filters: this.libServ.deepCopy(this.filtersApplied['filters']),
        groupByTimeKey: {
          key: ['time_key', 'accounting_key'],
          interval: 'daily'
        },
        gidGroupBy: this.getGrpBys(),
        orderBy: [{ key: 'time_key', opcode: 'asc' }],
        limit: '',
        offset: ''
      };

      this.loadMainLineChart(chartReq, this.dateRange);
    }
  }
  getdata() {
    return this.chartsDimMultiselectButton['data'].filter(x => x.isAvailable);
  }

  ngOnDestroy(): void {
    this.appConfigObs.unsubscribe();
  }
}
