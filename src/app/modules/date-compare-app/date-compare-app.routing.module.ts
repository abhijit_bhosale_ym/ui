import { Routes } from '@angular/router';

import { DateCompareAppComponent } from './date-compare-app.component';
export const SampleAppRoutes: Routes = [
  {
    path: '',
    component: DateCompareAppComponent
    // data: {
    //   breadcrumb: 'Yield Management'
    // }
  }
];
