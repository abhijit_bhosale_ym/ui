import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { TreeNode } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class FetchApiDataService {
  private BASE_URL: string = environment.baseUrl;

  constructor(private http: HttpClient) {}

  getTableData(params: object) {
    const url = `${this.BASE_URL}/izooto/v1/trend-compare/getData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
    // return <TreeNode[]> json.data;
  }
  getChildData(params: object) {
    const url = `${this.BASE_URL}api/dummyData/childTable`;
    return this.http.get(url, params);
    // return <TreeNode[]> json.data;
  }
  getRevMgmtData(params: object) {
    const url = `${this.BASE_URL}/izooto/v1/trend-compare/getData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
  }

  getLastUpdatedData(appId) {
    const url = `${this.BASE_URL}api/rev-mgmt/getLastUpdatedData/${appId}`;
    return this.http.get(url, { responseType: 'text' });
  }

}
