import { Component, OnInit } from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { FetchApiDataService } from '../fetch-api-data.service';
import * as moment from 'moment';
import { TreeNode } from 'primeng/api';
import { DynamicDialogConfig,DynamicDialogRef } from 'primeng/dynamicdialog';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';

@Component({
  selector: 'ym-campaign-daily-data-popup',
  templateUrl: './campaign-daily-data-popup.component.html',
  styleUrls: ['./campaign-daily-data-popup.component.scss']
})
export class CampaignDailyDataPopupComponent implements OnInit {
  lineChartJson: object;
  showLineChart = false;
  toggleView = true;
  filtersApplied: object;
  noLineChart = false;
  tabChangedFlag = false;

  tableDataReq: object;
  tableData: TreeNode[];
  tableColumnDef: any[];
  dimColDef: any[];
  tableJson: Object;
  popupTitle = '';
  heading = '';
  defaultChartsJson: object;
  showImpLineChart = false;
  impressionsChartJson: object;
  impressionChartVisited = false;
  CTRChartVisited = false;
  showCompletionLineChart = false;
  descrepancyChartVisited = false;
  completionChartJson: object;
  discrepancyChartJson: object;
  showDiscrepancyLineChart = false;
  showCTRLineChart = false;
  ctrChartJson : object;
  completionChartVisited = false;
  rowData = []
  exportData = []


  apiData=[];



  exportRequest: ExportRequest = <ExportRequest>{};

  constructor(
    private libServ: CommonLibService,
    private formatNumPipe: FormatNumPipe,
    private dataFetchServ: FetchApiDataService,
    private config: DynamicDialogConfig,
    private exportService: ExportdataService,
    private toastService: ToastService,
    public ref: DynamicDialogRef,
  ) {
    this.rowData = this.config.data['row'];
    this.toggleView = this.config.data['toggleView'];
    this.popupTitle = this.config.data['title'];
    this.heading = this.config.data['heading'];
  }

  ngOnInit() {
    this.exportRequest = this.config.data['exportRequest'];
    this.tableColumnDef = [
      {
        field: 'date',
        displayName: 'Date',
        format: '',
        width: '90',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'trafficking_impressions',
        displayName: 'Alphonso Imps.',
        format: 'number',
        width: '130',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'billing_impressions',
        displayName: '3rd Party Imps.',
        format: 'number',
        width: '130',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'pp_impressions',
        displayName: 'PP Imps.',
        format: 'number',
        width: '130',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'daily_budget_cap',
        displayName: 'Daily Budget Cap',
        format: 'number',
        width: '150',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'discrepancy_percentage',
        displayName: 'Imps. Discp. Perc.',
        format: 'percentage',
        width: '110',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'trafficking_clicks',
        displayName: 'Alphonso Clicks',
        format: 'number',
        width: '110',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'trafficking_ctr',
        displayName: 'Alphonso CTR',
        format: 'percentage',
        width: '110',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'cpm_rate',
        displayName: 'Dealsheet CPM',
        format: '$',
        width: '110',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'billing_clicks',
        displayName: '3rd Party Clicks',
        format: 'number',
        width: '110',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'billing_ctr',
        displayName: '3rd Party CTR',
        format: 'percentage',
        width: '110',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'pp_vcr',
        displayName: 'PP VCR',
        format: 'percentage',
        width: '110',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'trafficking_completions',
        displayName: 'Alphonso Compl.',
        format: 'number',
        width: '110',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'billing_completions',
        displayName: '3rd Party Compl.',
        format: 'number',
        width: '110',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'pp_completions',
        displayName: 'PP Compl.',
        format: 'number',
        width: '110',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'discepancy_completion_percentage',
        displayName: 'Compl. Discp. Perc.',
        format: 'percentage',
        width: '150',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'fraud_or_sivt_incidents',
        displayName: 'Fraud Impressions',
        format: 'number',
        width: '140',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'fraud_per',
        displayName: 'Fraud %',
        format: 'percentage',
        width: '110',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'viewablility',
        displayName: 'Viewability',
        format: 'percentage',
        width: '110',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'brand_safety',
        displayName: 'Brand Safety',
        format: 'percentage',
        width: '110',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },


      {
        field: 'out_of_geo',
        displayName: 'Out of Geo',
        format: 'percentage',
        width: '110',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'monitored_impressions',
        displayName: 'Monitored Impressions',
        format: 'number',
        width: '140',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'measured_impressions',
        displayName: 'Measured Impressions',
        format: 'number',
        width: '140',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'givt_rate',
        displayName: 'GIVT Rate',
        format: 'percentage',
        width: '110',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'valid_and_viewable_impressions',
        displayName: 'Valid & Viewable Impressions',
        format: 'number',
        width: '180',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'valid_and_viewable_rate',
        displayName: 'Valid & Viewable Rate',
        format: 'percentage',
        width: '150',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'trafficking_completion_rate',
        displayName: 'Alphonso Compl. Rate',
        format: 'percentage',
        width: '150',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'billing_completion_rate',
        displayName: '3rd Party Compl. Rate',
        format: 'percentage',
        width: '150',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'billable_amount',
        displayName: 'Billable Amt.',
        format: '$',
        width: '110',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      }
    ];
  
    this.tableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '300px',
      totalRecords: 1000,

      columns: this.tableColumnDef.slice(1),
      selectedColumns: this.tableColumnDef.slice(1),
      frozenCols: [...this.tableColumnDef.slice(0, 1)],
      frozenWidth:
        this.tableColumnDef
          .slice(0, 1)
          .reduce((tot, cur) => tot + parseInt(cur.width), 0) + 'px',

      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: false,
      columnSearch: false
    };

    this.loadTable();
    this.defaultChartsJson = {
      chartTypes: [{ key: 'line', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: []
      },
      chartOptions: {
        title: {
          display: true,
          text: ''
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              display: true,
              scaleLabel: {
                display: true
              }
            }
          ]
        },
        pan: {
          enabled: true,
          mode: 'x'
        },
        zoom: {
          enabled: true,
          mode: 'x'
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: true,
      chartWidth: '',
      chartHeight: '300px'
    };
  }

  highlightRowClass(row){
    if (row['pp_vcr'] > 100 || row['trafficking_completion_rate'] > 100 || row['billing_completion_rate'] > 100) {
        return 'ui-grid-row-highlight';
    }
    if (row['trafficking_impressions'] == 0 && row['trafficking_clicks'] == 0 && row['trafficking_completions'] == 0) {
        return 'ui-grid-row-highlight-no-data';
    }
    return '';
};

  loadTable() {
    const tableDataReq ={
      timeKey1 : this.rowData['flight_start'].toString().split("-").join(""),
      timeKey2 : this.rowData['flight_end'].toString().split("-").join("")
    }

    if(this.rowData['line_item_id'])
      tableDataReq['lineItemId'] = this.rowData['line_item_id']
    else
      tableDataReq['campaignId'] = this.rowData['campaign_id']
    
      this.dataFetchServ.getDailyCampaignData(tableDataReq).subscribe((data) => {  
        if (data['status'] === 0) {
          // this.noTableData = true;
          this.toastService.displayToast({                                                                                                                                                                                                                                                                                                          
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          return;
        } else {
       //   this.noTableData = false;
       const tableData = data['data'];
       this.apiData = tableData;
       //const tableData = data as [];
       const arr = [];                                                                                                                                                                                    
       tableData.forEach((row: object) => {
        this.rowData['line_item_id'] ? row['server_li_id'] = this.rowData['line_item_id'] : '' 
        arr.push({
           data: row,
           children: [{ data: {} }]
         });
       });
       this.tableData = <TreeNode[]>arr;
       this.tableJson['totalRecords'] = data['total'];
       setTimeout(() => {
         this.tableJson                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ['loading'] = false;
       }); 
      
      }
      }); 

//    this.tableJson['loading'] = true;

  }

  exportTablePopup(fileFormat) {
    if (this.exportRequest['sendEmail'].length > 0) {
      this.toastService.displayToast({
        severity: 'info',
        summary: 'Export Report',
        detail:
          'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
        life: 10000
      });
    const columnDefs = this.tableColumnDef;
    // const req = this.libServ.deepCopy(this.tableDataReq);
    // req['limit'] = '';
    // req['offset'] = '';
    // req['isTable'] = false;

    let fieldName = 'campaign';
    let displayName = "Campaign";
    let idField = "server_campaign_id";
    let idName = "CID" 

    if(this.rowData['line_item_id']){
        fieldName = 'Campaing'
        displayName = "Line Item"
        idField = 'server_li_id'
        idName = 'LID'
    }

    let nameCol = {
      field: fieldName,
      displayName: displayName,
      format: '',
      width: '95',
      value : '',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          format: 'string',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: true,
        colSort: true,
        resizable: true,
        movable: false
      }
    }
    let idCol = {
      field: idField,
      displayName: idName,
      format: '',
      width: '95',
      value : '',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          format: 'string',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: true,
        colSort: true,
        resizable: true,
        movable: false
      }
    }
    let costType = {
      field: 'cost_type',
      displayName: 'Cost Type',
      format: '',
      width: '95',
      value : '',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          format: 'string',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: true,
        colSort: true,
        resizable: true,
        movable: false
      }
    }
    let alpSource = {
      field: 'trafficking_source',
      displayName: 'Alphonso Source',
      format: '',
      width: '95',
      value : '',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          format: 'string',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: true,
        colSort: true,
        resizable: true,
        movable: false
      }
    }

    let billingSource = {
      field: 'billing_source',
      displayName: '3rd Party Source',
      format: '',
      width: '95',
      value : '',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          format: 'string',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: true,
        colSort: true,
        resizable: true,
        movable: false
      }
    }


 //   columnDefs.splice(1,0,nameCol);
    columnDefs.splice(0,0,idCol);
    columnDefs.splice(2,0,nameCol);
    columnDefs.splice(3,0,costType);
    columnDefs.splice(4,0,alpSource);
    columnDefs.splice(5,0,billingSource);

    const sheetDetailsarray = [];
        const sheetDetails = {};
        sheetDetails['columnDef'] = this.libServ.deepCopy(columnDefs);
        sheetDetails['data'] = this.apiData;
        sheetDetails['sheetName'] = 'Campaign Details';
        sheetDetails['isRequest'] = false;
        sheetDetails['request'] = {
          url: '',
            method: '',
            param: ''
        };
        sheetDetails['disclaimer'] = [
          {
            position: 'top',
            label:
              'Data present in below table is not final and may varies over period of time',
            color: '#3A37CF'
          }
        ];
        sheetDetails['totalFooter'] = {
          available: false,
          custom: false
        };
        sheetDetails['tableTitle'] = {
          available: true,
          label: 'Daily Distribution'
        };
        sheetDetails['image'] = [
          {
            available: true,
            path: 'ExportImages/yuktaone-logo.png',
            position: 'top'
          }
        ];
        sheetDetailsarray.push(sheetDetails);
        this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
        this.exportRequest['fileName'] = 'Campaign Details ' +
          moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
        this.exportRequest['exportFormat'] = fileFormat;
        this.dataFetchServ
          .getExportReportData(this.exportRequest)
          .subscribe(response => {
            console.log(response);
          });

    } else {
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Export Report',
        detail:
          'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email.Thanks!',
        life: 10000
      });
    }
  }


  loadImpressionchart() {
    this.showImpLineChart = false;
    const colors = this.libServ.dynamicColors(2);
    this.impressionsChartJson = this.libServ.deepCopy(this.defaultChartsJson);
    this.impressionsChartJson['chartOptions']['title']['text'] =
      'Impressions Trend';
    this.impressionsChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
      callback: (value, index, values) => {
        return this.formatNumPipe.transform(value, 'number', []);
      }
    };
    this.impressionsChartJson['chartOptions']['scales']['yAxes'][0][
      'scaleLabel'
    ]['labelString'] = 'Impressions';
    this.impressionsChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return `${
            data.datasets[tooltipItem.datasetIndex].label
            } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
        }
      }
    };
    const chartData = this.apiData;
    console.log("chartData",chartData);
    
    const datesArr = Array.from(new Set(chartData.map(r => r['date'])));
    this.impressionsChartJson['chartData']['labels'] = datesArr.map(d =>
      moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
    );
    this.impressionsChartJson['chartData']['datasets'] = [
      {
        label: 'Alphonso Impressions',
        borderColor: colors[0],
        fill: false,
        backgroundColor: colors[0],
        data: []
      },
      {
        label: '3rd Party Impressions',
        borderColor: colors[1],
        fill: false,
        backgroundColor: colors[1],
        data: []
      }
    ];

    const impArr = [];
    const threePImpArr = [];
    datesArr.forEach(time_key => {
      chartData.forEach(r => {
        if (r['date'] === time_key) {
          impArr.push(parseInt(r['trafficking_impressions'], 10));
          threePImpArr.push(parseInt(r['billing_impressions'], 10));
        }
      });
    });
    this.impressionsChartJson['chartData']['datasets'][0]['data'] = impArr;
    this.impressionsChartJson['chartData']['datasets'][1]['data'] = threePImpArr;
    this.showImpLineChart = true;
  }

  loadCompletionChart() {
    this.showCompletionLineChart = false;
    const colors = this.libServ.dynamicColors(2);
    this.completionChartJson = this.libServ.deepCopy(this.defaultChartsJson);
    this.completionChartJson['chartOptions']['title']['text'] = 'Completion Rate Breakdown Line-chart';
    this.completionChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
      callback: (value, index, values) => {
        return this.formatNumPipe.transform(value, 'percentage', [2]);
      }
    };
    this.completionChartJson['chartOptions']['scales']['yAxes'][0]['scaleLabel'][
      'labelString'
    ] = 'Completion Rate';
    this.completionChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return `${
            data.datasets[tooltipItem.datasetIndex].label
            } : ${this.formatNumPipe.transform(currentValue, 'percentage', [2])}`;
        }
      }
    };
    const chartData = this.apiData;
    const datesArr = Array.from(new Set(chartData.map(r => r['date'])));
    this.completionChartJson['chartData']['labels'] = datesArr.map(d =>
      moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
    );
    this.completionChartJson['chartData']['datasets'] = [
      {
        label: 'Alphonso Completion Rate',
        borderColor: colors[0],
        fill: false,
        backgroundColor: colors[0],
        data: []
      },
      {
        label: '3rd Party Completion Rate',
        borderColor: colors[1],
        fill: false,
        backgroundColor: colors[1],
        data: []
      }
    ];

    const CTRArr = [];
    const threePCTRArr = [];
    datesArr.forEach(time_key => {
      chartData.forEach(r => {
        if (r['date'] === time_key) {
          CTRArr.push(parseFloat(r['trafficking_completion_rate']));
          threePCTRArr.push(parseFloat(r['billing_completion_rate']));
        }
      });
    });
    this.completionChartJson['chartData']['datasets'][0]['data'] = CTRArr;
    this.completionChartJson['chartData']['datasets'][1]['data'] = threePCTRArr;
    this.showCompletionLineChart = true;
  }

  loadDescrepancyChart() {
    this.showDiscrepancyLineChart = false;
    const colors = this.libServ.dynamicColors(2);
    this.discrepancyChartJson = this.libServ.deepCopy(this.defaultChartsJson);
    this.discrepancyChartJson['chartOptions']['title']['text'] = 'Discrepancy Breakdown Line-chart';
    this.discrepancyChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
      callback: (value, index, values) => {
        return this.formatNumPipe.transform(value, 'percentage', [2]);
      }
    };
    this.discrepancyChartJson['chartOptions']['scales']['yAxes'][0]['scaleLabel'][
      'labelString'
    ] = 'Discrepancy Percentage';
    this.discrepancyChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return `${
            data.datasets[tooltipItem.datasetIndex].label
            } : ${this.formatNumPipe.transform(currentValue, 'percentage', [2])}`;
        }
      }
    };
    const chartData = this.apiData;
    const datesArr = Array.from(new Set(chartData.map(r => r['date'])));
    this.discrepancyChartJson['chartData']['labels'] = datesArr.map(d =>
      moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
    );
    this.discrepancyChartJson['chartData']['datasets'] = [
      {
        label: 'Impressions Discrepancy Percentage',
        borderColor: colors[0],
        fill: false,
        backgroundColor: colors[0],
        data: []
      },
      {
        label: 'Completions Discrepancy Percentage',
        borderColor: colors[1],
        fill: false,
        backgroundColor: colors[1],
        data: []
      }
    ];

    const CTRArr = [];
    const threePCTRArr = [];
    datesArr.forEach(time_key => {
      chartData.forEach(r => {
        if (r['date'] === time_key) {
          CTRArr.push(parseFloat(r['discrepancy_percentage']));
          threePCTRArr.push(parseFloat(r['discepancy_completion_percentage']));
        }
      });
    });
    this.discrepancyChartJson['chartData']['datasets'][0]['data'] = CTRArr;
    this.discrepancyChartJson['chartData']['datasets'][1]['data'] = threePCTRArr;
    this.showDiscrepancyLineChart = true;
  }

  loadCTRChart() {
    this.showCTRLineChart = false;
    const colors = this.libServ.dynamicColors(2);
    this.ctrChartJson = this.libServ.deepCopy(this.defaultChartsJson);
    this.ctrChartJson['chartOptions']['title']['text'] = 'CTR Breakdown Line-chart';
    this.ctrChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
      callback: (value, index, values) => {
        return this.formatNumPipe.transform(value, 'percentage', [2]);
      }
    };
    this.ctrChartJson['chartOptions']['scales']['yAxes'][0]['scaleLabel'][
      'labelString'
    ] = 'CTR';
    this.ctrChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return `${
            data.datasets[tooltipItem.datasetIndex].label
            } : ${this.formatNumPipe.transform(currentValue, 'percentage', [2])}`;
        }
      }
    };
    const chartData = this.apiData;
    const datesArr = Array.from(new Set(chartData.map(r => r['date'])));
    this.ctrChartJson['chartData']['labels'] = datesArr.map(d =>
      moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
    );
    this.ctrChartJson['chartData']['datasets'] = [
      {
        label: 'Alphonso CTR',
        borderColor: colors[0],
        fill: false,
        backgroundColor: colors[0],
        data: []
      },
      {
        label: '3rd Party CTR',
        borderColor: colors[1],
        fill: false,
        backgroundColor: colors[1],
        data: []
      }
    ];

    const CTRArr = [];
    const threePCTRArr = [];
    datesArr.forEach(time_key => {
      chartData.forEach(r => {
        if (r['date'] === time_key) {
          CTRArr.push(parseFloat(r['trafficking_ctr']));
          threePCTRArr.push(parseFloat(r['billing_ctr']));
        }
      });
    });
    this.ctrChartJson['chartData']['datasets'][0]['data'] = CTRArr;
    this.ctrChartJson['chartData']['datasets'][1]['data'] = threePCTRArr;
    this.showCTRLineChart = true;
  }


  isHiddenColumn(col: Object) {}


  tabChanged(e) {
    console.log(e);
    if (e.index === 1 && !this.impressionChartVisited) {
      this.impressionChartVisited = true;
      this.loadImpressionchart();
    } else if (e.index === 2 && !this.completionChartVisited) {
      this.completionChartVisited = true;
      this.loadCompletionChart();
    } else if(e.index == 3 && !this.descrepancyChartVisited)
    {
      this.descrepancyChartVisited = true;
      this.loadDescrepancyChart();
    } else if(e.index == 4 && !this.CTRChartVisited)
    {
      this.CTRChartVisited = true;
      this.loadCTRChart();
    }
  }

  chartSelected(e) {}

  goBack(){
      this.ref.close(null);
  }
}
 