import { Routes } from '@angular/router';

import { ActiveCampaignsComponent } from './active-campaigns.component';
export const ActiveCampaignAppRoutes: Routes = [
  {
    path: '',
    component: ActiveCampaignsComponent
    // data: {
    //   breadcrumb: 'Yield Management'
    // }
  }
];
