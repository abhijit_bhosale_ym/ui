import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,HttpParams} from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { TreeNode } from 'primeng/api';
import {Observable} from 'rxjs'
// import { RequestOptions } from '@angular/http';
import { HttpRequest} from '@angular/common/http';



const headerOption = {
  headers : new HttpHeaders({'Content-Type':'application/json                                                                                                 '})
}



@Injectable({
  providedIn: 'root'
})
export class FetchApiDataService {
  private BASE_URL: string = environment.baseUrl;
  private options =  {headers: new  HttpHeaders({ 'Content-Type': 'application/json'})};
  val ="";

  constructor(private http: HttpClient) { }

  getTableData(params: object) {
    const url = `${this.BASE_URL}/unified/v1/active-campaigns/getData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
  }

  getLineItemByCampaignData(params: String) {
    const url = `${this.BASE_URL}/unified/v1/active-campaigns/getLineItemsByCampaignId/`+params;  
    return this.http.post(url,{});
  }
  getDailyCampaignData(params: object) {
    const url = `${this.BASE_URL}/unified/v1/active-campaigns/getCampaignDailyData`;
     return this.http.post(url, params);
  }
  public getTeamMembers() {
    return this.http.get(
      `${this.BASE_URL}/unified/v1/active-campaigns/getAllTeamMembers`
    );
 }
 public getBizOpsMembers() {
  return this.http.get(
    `${this.BASE_URL}/unified/v1/active-campaigns/getAllBizOpsMembers`
  );
}

 public getCostTypes() {
  return this.http.get(
    `${this.BASE_URL}/unified/v1/active-campaigns/getAllCostTypes`
  );
}


 updateTeamMembers(params: object) {
  const url = `${this.BASE_URL}/unified/v1/active-campaigns/update-team-members`;
  if (!('isTable' in params)) {
    params = Object.assign(params, { isTable: true });
  }
  return this.http.post(url, params);
}

updateFulfilment(params: object) {
  const url = `${this.BASE_URL}/unified/v1/active-campaigns/update-fulfilment`;
  if (!('isTable' in params)) {
    params = Object.assign(params, { isTable: true });
  }
  return this.http.post(url, params);
}

updateLineItemData(params: object) {
  const url = `${this.BASE_URL}/unified/v1/active-campaigns/update-line-item-data`;
  if (!('isTable' in params)) {
    params = Object.assign(params, { isTable: true });
  }
  return this.http.post(url, params);
}

addCampaignFav(params: object) {
  const url = `${this.BASE_URL}/unified/v1/active-campaigns/addCampaignFav`;
  if (!('isTable' in params)) {
    params = Object.assign(params, { isTable: true });
  }
  return this.http.post(url, params);
}

deleteCampaignFav(params: object) {
  const url = `${this.BASE_URL}/unified/v1/active-campaigns/deleteCampaignFav`;
  if (!('isTable' in params)) {
    params = Object.assign(params, { isTable: true });
  }
  return this.http.post(url, params);
}

getExportReportData(params: object) {
  const url = `${this.BASE_URL}/frankly/v1/common/exportData`;
  if (!('isTable' in params)) {
    params = Object.assign(params, { isTable: true });
  }
  return this.http.post(url, params);
  // return <TreeNode[]> json.data;
}


}
