import { Component, OnInit, OnDestroy,ViewChild } from '@angular/core';
import { TreeNode } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { CommonLibService } from 'src/app/_services';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';
import { FetchApiDataService } from './fetch-api-data.service';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { Subscription } from 'rxjs';
import { SendEmailComponent } from '../send-email/send-email.component';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { CampaignDailyDataPopupComponent } from './campaign-daily-data-popup/campaign-daily-data-popup.component'
import { CampaignEditPopupComponent } from './campaign-edit-popup/campaign-edit-popup.component'
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { ConfirmationService } from 'primeng/api';
import { MultiSelect } from 'primeng/multiselect';
import { CommentJson } from 'src/app/_interfaces/commentJson';
import { CommentService } from '../common/chat/comment.service';
import { ChatComponent } from '../common/chat/chat/chat.component';

@Component({
  selector: 'ym-active-campaigns',
  templateUrl: './active-campaigns.component.html',
  styleUrls: ['./active-campaigns.component.scss']
})

export class ActiveCampaignsComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;

  appConfig: object = {};
  lastUpdatedOn: Date;
  nextUpdated: Date;
  dataUpdatedThrough: Date;

  aggTableData: TreeNode[];
  aggTableJson: Object;
  aggColDef: any[];
  noTableData = false;
  displayAggTable = true;
  tableRequest = {};
  data = true;
  timeout: any;

  editDropdownOptionsArray = [];
  bizOpsMemberArray = [];
  costTypesOptionsArray = [];
  flag = true;
  toggleValue = true;

  filtersApplied: object = {};
  exportRequest: ExportRequest = <ExportRequest>{};
  teamMembers = {};
  selectedBizOpsMembers : any =[];
  showhide = true;
  saveViewJson = {};
  isSaveView: boolean;
  show = false;
  isOpenBizOps = false;
  commentData = [];
  chatcount = 0;
  commentJson: CommentJson = <CommentJson>{};
  bellIconLoaded = false;

  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private htmltoimage: HtmltoimageService,
    private exportService: ExportdataService,
    private pptExport: ExportPptService,
    private formatNumPipe: FormatNumPipe,
    private confirmationService: ConfirmationService,
    private commentservice: CommentService,
  ) { }
  @ViewChild("tbledata") multiSelect: MultiSelect;
  @ViewChild("multiselectTeamMembers") multiselectTeamMembers: MultiSelect;
//  
  openMutlti(e){
    this.show = true;

    this.multiSelect.show();
    e.stopPropagation();      
  }


  ngOnInit() {

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        console.log('appConfig', this.appConfig);

        this._titleService.setTitle(this.appConfig['displayName']);
        let startDate;
        let endDate;
        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        this.commentJson['app_id'] = this.appConfig['id'];
        this.commentJson['user_id'] = this.appConfig['user']['id'];
        this.commentJson['receiver'] = this.appConfig['route'];
        this.commentJson['user_name'] = this.appConfig['user']['userName'];
        this.initialLoading();
      }
    });
        this.aggColDef = [
      {
        field: 'server_campaign_id',
        displayName: 'CID/LID',
        format: '',
        width: '105',
        value : '',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            format: 'string',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'flight_start',
        displayName: 'Flight Start Date',
        format: '',
        width: '80',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        value : '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'flight_end',
        displayName: 'Flight End Date',
        format: '',
        width: '80',
        value : '',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'campaign',
        displayName: 'Campaign / Line Item Name',
        format: '',
        width: '240',
        value : ''                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                ,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'c_status',
        displayName: 'Status',
        width: '80',
        value : '',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'comments',
        displayName: 'Comments',
        width: '78',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'is_campaign_favorite',
        displayName: 'Fav.',
        width: '40',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'team_members',
        displayName: 'BDA*',
        width: '130',
        value : '',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'bizops_team_members',
        displayName: 'BizOps*',
        width: '130',
        value : '',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'trafficking_delivery_progress',
        displayName: 'Alphonso Deli. Progress',
        width: '110',
        value : '',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'percentage',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'billing_delivery_progress',
        displayName: '3rd Party Deli. Progress',  
        width: '110',
        value : '',
        exportConfig: {
          format: 'string', 
          styleinfo: {  
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'percentage',
          formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'pp_delivery_progress',
        displayName: 'PP Deli. Progress',
        width: '100',
        value : '',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'percentage',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        displayName: 'Camp. Duration Progress',
        field : 'campaign_duration_progress',
        width: '110',
        value : '',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'percentage',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'discrepancy_percentage',
        displayName: 'Discrepancy Percentage',
        width: '110',
        value : '',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'percentage',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },


      {
        field: 'io_number',
        displayName: 'IO',
        width: '75',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'io_target_imp_completions',
        displayName: 'IO Target Imps. / Compls. **',
        width: '155',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },

      {
        field: 'target_impressions',
        displayName: 'Target Imps.',
        width: '155',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'c_budget_cost',
        displayName: 'Budget ***',
        width: '155',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'cpm_rate',
        displayName: 'Rate ***',
        width: '155',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'floatCurrency',
        formatConfig: [4],
        options: {
          editable: true,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'billable_amount',
        displayName: 'Billable Amt.',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },

      {
        field: 'cost_type',
        displayName: 'Cost Type',
        width: '130',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },

      {
        field: 'billing_source',
        displayName: '3rd Party Source',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'billing_total_vcr',
        displayName: '3rd Party Tot. VCR',
        width: '130',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        value : '',

        format: 'percentage',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },

      {
        field: 'trafficking_total_vcr',
        displayName: 'Alphonso Tot. VCR',
        width: '130',
        value : '',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'percentage',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },

      {
        field: 'trafficking_total_ctr',
        displayName: 'Alphonso Total CTR',
        width: '140',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'percentage',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },

      {
        field: 'billing_total_ctr',
        displayName: '3rd Party Total CTR',
        width: '140',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'percentage',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },



      {
        field: 'givt_rate',
        displayName: 'GIVT Rate',
        format: 'percentage',
        width: '110',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

    
      {
        field: 'action_on_daily_budget_cap',
        displayName: 'Action on Daily Budget Cap',
        width: '135',
        value : '',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'daily_budget_cap',
        displayName: 'Daily Budget Cap',
        width: '130',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'impressions_per_day',
        displayName: 'Imps. per Day',
        width: '130',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'continue_or_stop',
        displayName: 'Continue / Stop',
        width: '130',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'monthly_budget',
        displayName: 'Monthly Budget',
        width: '130',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },

      {
        field: 'third_party_site_served',
        displayName: '3rd Party Site Served',
        width: '140',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },

      {
        field: 'days_remaining_for_flight_start_date',
        displayName: 'Days to Start Camp.',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'days_remaining_for_campaign_end',
        displayName: 'Days to End Camp.',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'trafficking_source',
        displayName: 'Alphonso Source',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'trafficking_total_impressions',
        displayName: 'Alphonso Total Imps.',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'trafficking_total_video_completes',
        displayName: 'Alphonso Total Video Completes',
        width: '170',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'trafficking_total_clicks',
        displayName: 'Alphonso Total Clicks',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'billing_total_impressions',
        displayName: '3rd Party Total Imps.',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'billing_total_video_completes',
        displayName: '3rd Party Total Video Completes',
        width: '180',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'billing_total_clicks',
        displayName: '3rd Party Total Clicks',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'pp_total_impressions',
        displayName: 'PP Total Imps.',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'pp_total_completes',
        displayName: 'PP Total Video Completes',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'trafficking_todays_impressions',
        displayName: 'Alphonso Todays Imps.',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'trafficking_todays_video_completes',
        displayName: 'Alphonso Todays Video Completes',
        width: '180',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'trafficking_todays_clicks',
        displayName: 'Alphonso Todays Clicks',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'trafficking_todays_vcr',
        displayName: 'Alphonso Todays VCR',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'percentage',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },



      {
        field: 'trafficking_todays_ctr',
        displayName: 'Alphonso Todays CTR',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'percentage',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'pixel_pinger_todays_impression',
        displayName: 'PP. Todays Imps.',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'pixel_pinger_todays_vcr',
        displayName: 'PP. Todays VCR',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'percentage',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },

      {
        field: 'headway_pacing',
        displayName: 'Headway Pacing **',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'percentage',
        formatConfig: [2],
        options: {
          editable: true,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'todays_full_pacing_catchup',
        displayName: 'Todays full pacing catchup',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'impressions_target_for_previous_day',
        displayName: 'Imps. Target for previous day',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'total_impressions_left',
        displayName: 'Total Imps. / Compls. left',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'todays_total_impressions_left',
        displayName: 'Todays Total Imps. left',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'pacing_type',
        displayName: 'Pacing Type',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'uniques',
        displayName: 'Uniques ***',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'feasibility',
        displayName: 'Feasibility ***',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },

      {
        field: 'tunein',
        displayName: 'Tune-in*',
        width: '110',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'premier',
        displayName: 'Premiere *',
        width: '110',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'closure',
        displayName: 'Closure *',
        width: '110',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'comscore',
        displayName: 'Survey *',
        width: '110',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },

      {
        field: 'daily_spend',
        displayName: 'Alp. Spend',
        width: '155',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'white_list',
        displayName: 'Whitelist **',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'black_list',
        displayName: 'Blacklist **',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },


      {
        field: 'ias_pre_bid_implemented',
        displayName: 'IAS Pre Bid Implemented **',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'app_dispersion',
        displayName: 'App Dispersion **',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'os_dispersion',
        displayName: 'OS Dispersion **',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'diff_alp_campaign_duration_progress',
        displayName: 'Diff Alp & Camp. Duration Progress',
        width: '140',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'percentage',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'diff_thirdparty_campaign_duration_progress',
        displayName: 'Diff 3rd party & Camp. Duration Progress',
        width: '140',
        value : '',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'percentage',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'fraud_or_sivt_incidents',
        displayName: 'Fraud Impressions',
        width: '70',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'fraud_per',
        displayName: 'Fraud %',
        width: '130',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'percentage',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'viewablility',
        displayName: 'Viewability %',
        width: '130',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'percentage',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'givt_rate',
        displayName: 'GIVT Rate',
        width: '130',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'percentage',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'valid_and_viewable_impressions',
        displayName: 'Valid & Viewable Impressions',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'valid_and_viewable_rate',
        displayName: 'Valid & Viewable Rate',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'percentage',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },

      {
        field: '3p_file_search_string',
        displayName: '3P File Search String',
        width: '120',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'string',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'brand_saftey',
        displayName: 'Brand Safety %',
        width: '140',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'percentage',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },

      {
        field: 'out_of_geo',
        displayName: 'Out Of Geo %',
        width: '140',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'percentage',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'monitored_impressions',
        displayName: 'Monitored Impressions',
        format: 'number',
        width: '140',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'measured_impressions',
        displayName: 'Measured Impressions',
        format: 'number',
        width: '140',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },



      {
        field: '3p_verification_partner',
        displayName: '3P Verification Partner',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'string',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        }
      },



    ];

    this.aggTableJson = {
      page_size: 10, 
      lazy: true,
      loading: false,
      export: true,
      reload: false,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,
      columns: this.libServ.deepCopy(this.aggColDef.slice(5)),
      selectedColumns: this.libServ.deepCopy(this.aggColDef.slice(5)),
      frozenCols: this.libServ.deepCopy([...this.aggColDef.slice(0, 5)]),
      frozenWidth:
        this.aggColDef
          .slice(0, 5)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: false,
      columnSearch: true
    };
  }
  ngAfterViewInit() {
    setTimeout(() => {
      if ($('.ui-table-scrollable-wrapper').length) {
        let wrapper = $('.ui-table-scrollable-wrapper');
        wrapper.each(function () {
          let w = $(this);
          let frozen_rows: any = w.find('.ui-table-frozen-view tr');
          let unfrozen_rows = w.find('.ui-table-unfrozen-view tr');
          for (let i = 0; i < frozen_rows.length; i++) {
            if (frozen_rows.eq(i).height() > unfrozen_rows.eq(i).height()) {
              unfrozen_rows.eq(i).height(50);
            } else if (frozen_rows.eq(i).height() < unfrozen_rows.eq(i).height()) {
              frozen_rows.eq(i).height(50);
            }
          }
        });
      }
    });

    


  }
  initialLoading() {
    this.UpdateBellIcon();
    this.getAllBizOpsMembers();
    this.tableRequest['limit'] = 10;
    this.tableRequest['offset'] = 0;
    this.tableRequest['isCampaign'] = true;
    this.loadTableData(this.tableRequest);
  }

getAllTeamMembers(){
    this.dataFetchServ.getTeamMembers().subscribe((data)=>{
      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      }
      this.editDropdownOptionsArray = data['data'];      
    });
}

getAllBizOpsMembers(){
  this.dataFetchServ.getBizOpsMembers().subscribe((data)=>{
    if (data['status'] === 0) {
      this.noTableData = true;
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Server Error',
        detail: 'Please refresh the page'
      });
      return;
    }
    this.bizOpsMemberArray = data['data'];      
//    this.selectedBizOpsMembers.push(this.bizOpsMemberArray[1].label)
    // console.log("selectedBizOpsMembers",this.bizOpsMemberArray);
    
  });
}

getCostTypes(){
  this.dataFetchServ.getCostTypes().subscribe((data)=>{
    if (data['status'] === 0) {
      this.noTableData = true;
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Server Error',
        detail: 'Please refresh the page'
      });
      return;
    }          
    this.costTypesOptionsArray[0] = data['data'].find(item => item['label']=='CPM');
    this.costTypesOptionsArray[1] = data['data'].find(item => item['label']=='CPVC');   
  });
}

exportTable(fileFormat) {
  const data = [];

  if (this.aggTableData.length == 0) {
    this.confirmationService.confirm({
      message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
      header: 'Information',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
      },
      reject: () => {
      }
    });
  } else {
    if (this.exportRequest['sendEmail'].length > 0) {
      this.toastService.displayToast({
        severity: 'info',
        summary: 'Export Report',
        detail: 
          'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
        life: 10000
      });
      const finalColDef = this.libServ.deepCopy(this.aggColDef);
      finalColDef.splice(0, 0, ...this.aggColDef);
      finalColDef.splice(finalColDef.findIndex(itm => itm.field === 'is_campaign_favorite'),2);
      finalColDef.splice(finalColDef.findIndex(itm => itm.field === 'comments'),2);
      const tableReq = this.libServ.deepCopy(this.tableRequest);//this.libServ.deepCopy(this.tableRequestParam);

        this.dataFetchServ.getTableData(this.tableRequest).subscribe(data1 => {
          const tableData = data1['data'] as [];
          const creativeData = data1['data'] as [];
          const colDef = this.libServ.deepCopy(finalColDef);
          colDef.find(ele => ele.field == 'server_campaign_id').displayName = 'CID';
          colDef.find(ele => ele.field == 'campaign').displayName = 'Campaign Name'
          const sheetDetailsarray = [];
          const sheetDetails = {};
          sheetDetails['columnDef'] = colDef;
          sheetDetails['data'] = tableData;
          sheetDetails['sheetName'] = 'Campaign Data';
          sheetDetails['isRequest'] = false;
          sheetDetails['request'] = {
            url: '',
            method: '',
            param: ''
          };
          sheetDetails['disclaimer'] = [
            {
              position: 'top',
              label:    
                'Data present in below table is not final and may varies over period of time',
              color: '#3A37CF'
            }
          ];
          sheetDetails['totalFooter'] = {
            available: false,
            custom: false
          };
          sheetDetails['tableTitle'] = {
            available: true,
            label: 'Campaign '
          };
          sheetDetails['image'] = [
            {
              available: true,
              path: 'ExportImages/yuktaone-logo.png',
              position: 'top'
            }
          ];

          sheetDetailsarray.push(sheetDetails);
          this.dataFetchServ.getLineItemByCampaignData("1").subscribe(data2 => {
          const tableData1 = data2['data'] as [];
  
          tableData1.forEach((row: object) => {
            row['server_cid'] = row['server_campaign_id'];
            row['server_campaign_id'] = row['server_li_id'];
            row['campaign'] = row['line_item'];
            row['c_status'] = row['l_status'];
            row['uniques'] = row['l_uniques'];
            row['c_budget_cost'] = row['l_budget_cost']; 
          })  

          const sheetDetailsCreative = {};
          const lineItemDef = this.libServ.deepCopy(finalColDef);
          lineItemDef.find(ele => ele.field == 'server_campaign_id').displayName = 'LID';
          lineItemDef.find(ele => ele.field == 'campaign').displayName = 'Line Item Name'
          lineItemDef.splice(
            3,
            0,
            {
              field: 'campaign_name',
              displayName: 'Campaign Name',
              format: '',
              width: '200',
              exportConfig: {
                format: 'String',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: true
              }
            })
          sheetDetailsCreative['columnDef'] = lineItemDef;
          sheetDetailsCreative['data'] = tableData1;
          sheetDetailsCreative['sheetName'] = 'Line Item Data';
          sheetDetailsCreative['isRequest'] = false;
          sheetDetailsCreative['request'] = {
            url: '',
            method: '',
            param: ''
          };
          sheetDetailsCreative['disclaimer'] = [
            {
              position: 'top',
              label:
                'Data present in below table is not final and may varies over period of time',
              color: '#3A37CF'
            }
          ];
          sheetDetailsCreative['totalFooter'] = {
            available: false,
            custom: false
          };
          sheetDetailsCreative['tableTitle'] = {
            available: true,
            label: 'Line Item Data'
          };
          sheetDetailsCreative['image'] = [
            {
              available: true,
              path: 'ExportImages/yuktaone-logo.png',
              position: 'top'
            }
          ];
         sheetDetailsarray.push(sheetDetailsCreative);

         this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
         this.exportRequest['fileName'] =
           'Campaign Fulfilment ' + moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
         this.exportRequest['exportFormat'] = fileFormat;
         this.dataFetchServ
           .getExportReportData(this.exportRequest)
           .subscribe(response => {
             console.log(response);
           });
        });
        })
    } else {
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Export Report',
        detail:
          'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email.Thanks!',
        life: 10000
      });
    }
  }

}

  loadTableData(tableReq) {
    if(!this.tableRequest['isCampaign']){
      this.loadTableDataLineItem(tableReq);
      return;
    }
    this.aggTableJson['loading'] = true;
    this.dataFetchServ.getTableData(this.tableRequest).subscribe((data) => {
      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      } else {
        this.noTableData = false;
      }
      const tableData = data['data'];
      const arr = [];
      tableData.forEach((row: object) => {
        row['selectedBizOps'] = [];
        if(row['bizops_team_members']) {
          row['bizops_team_members'].split(',').forEach(element => {
            // console.log(".row['selectedBizOps']",element);
            
            row['selectedBizOps'].push({key : element, label : element})            
          });
        }

        row['selectedTeamMembers'] = [];
        if(row['team_members']) {
          row['team_members'].split(',').forEach(element => {
            // console.log(".row['team_members']",element);
            
            row['selectedTeamMembers'].push({key : element, label : element})            
          });
        }

        // console.log("selectedTeamMembers....",row['selectedTeamMembers']);
        

        if(row['3p_file_search_string'] == null){
          row['3p_file_search_string']= "";
        }
        if(row['premier']){
          let temp = row['premier'].substring(0,10);
          Date.parse(temp) ? row['premierDate'] = temp :row['premierDate'] = "";     
        }
        if(row['tunein']){
          let temp = row['tunein'].substring(0,10);
          Date.parse(temp) ? row['tuneinDate'] = temp :row['tuneinDate'] = "";     
        }
        if(row['closure']){
          let temp = row['closure'].substring(0,10);
          Date.parse(temp) ? row['closureDate'] = temp :row['closureDate'] = "";     
        }
        if(row['comscore']){
          let temp = row['comscore'].substring(0,10);          
          Date.parse(temp) ? row['comscoreDate'] = temp :row['comscoreDate'] = "";
        }        
      arr.push({
          data: row,
          children: [{ data: {} }]
        });
      });    
      let selectedCol = [];
      if (typeof this.saveViewJson['selectedColDefs'] != 'undefined' && this.appConfig['filter']['filterConfig']['saveView'] && (this.isSaveView)) {        
        selectedCol = this.saveViewJson['selectedColDefs'];
        // this.aggTableJson['columns'] = selectedCol;
        this.aggTableJson['selectedColumns'] = selectedCol;
      } else {
        // this.aggTableJson['columns'] =  this.libServ.deepCopy(this.aggColDef.slice(5));
        this.aggTableJson['selectedColumns'] = this.libServ.deepCopy(this.aggColDef.slice(5))
      }
      this.aggTableData = <TreeNode[]>arr;
      this.aggTableJson['totalRecords'] = data['total'];
      this.getAllTeamMembers();
      this.getCostTypes();
      setTimeout(() => {
        this.aggTableJson['loading'] = false;
      });
    });
  }

  loadTableDataLineItem(tableReq) {
    this.aggTableJson['loading'] = true;
    this.dataFetchServ.getLineItemByCampaignData("1").subscribe((data) => {
      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      } else {
        this.noTableData = false;
      }
      const tableData = data['data'];
      const arr = [];
      tableData.forEach((row: object) => {
        row['server_cid'] = row['server_campaign_id'];
        row['server_campaign_id'] = row['server_li_id'];
        row['campaign'] = row['line_item'];
        row['c_status'] = row['l_status'];
        row['uniques'] = row['l_uniques'];
        row['c_budget_cost'] = row['l_budget_cost'];
        row['is_line_item'] = true;

        arr.push({
          data: row,
        });
      });      
      this.aggTableData = <TreeNode[]>arr;
      this.aggTableJson['totalRecords'] = data['total'];
      setTimeout(() => {
        this.aggTableJson['loading'] = false;
      });
    });
  }

  toggleFavorite(rowData){
    let req = {};    
    if(rowData['is_campaign_favorite'] == 'hasNot') 
    {
      req['user_id'] = this.appConfig['user']['id'];
      req['campaign_id'] = rowData['campaign_id'];
            
      this.dataFetchServ.addCampaignFav(req).subscribe((data) => {
        if (data['status'] === 0) {
          this.noTableData = true;
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          return;
        } else {
          
          this.toastService.displayToast({
            severity: 'success',
            summary: 'Campaign favorite',
            detail: "Campaign has been added to favorite"
          });
          rowData['is_campaign_favorite'] = "has"
        }  
      });
    }
    else{
      req['campaign_id'] = rowData['campaign_id'];
      this.dataFetchServ.deleteCampaignFav(req).subscribe((data) => {  
        if (data['status'] === 0) {
          this.noTableData = true;
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          return;
        } else {
          
          this.toastService.displayToast({
            severity: 'success',
            summary: 'Campaign Update',
            detail: "Campaign has been removed from favorite"
          });
          rowData['is_campaign_favorite'] = "hasNot"
        }  
      });
    }
  }

  onPanelHide(){
    
  }

  afterEditCell(rowdata,flag,event,value){     
    let message;
    let req = {campaignId : rowdata['campaign_id']}
    
    switch(flag){
      case 'team_members' : 
        this.multiselectTeamMembers.overlayVisible = false;      
        if(rowdata['selectedTeamMembers'] && rowdata['selectedTeamMembers'][0]== '')
          rowdata['selectedTeamMembers']= rowdata['selectedTeamMembers'].slice(0);
        req['teamMembers'] = rowdata['selectedTeamMembers'].map(item => item['label']).toString();                                                               
        rowdata['team_members'] = rowdata['selectedTeamMembers'].map(item => item['label']).toString();
        message = 'BDA updated for CID-'+rowdata['server_campaign_id'] +' successfully';
        this.editCellCampaign(req,message);
        break;

      case 'bizops_team_members' :
        this.multiSelect.overlayVisible = false;
        if(rowdata['selectedBizOps'] && rowdata['selectedBizOps'][0]== '')
          rowdata['selectedBizOps']= rowdata['selectedBizOps'].slice(0);
        req['bizOpsTeamMembers'] = rowdata['selectedBizOps'].map(item => item['label']).toString();                                               
        message = 'BizOps member updated for CID-'+rowdata['server_campaign_id'] +' successfully';
        rowdata['bizops_team_members'] = rowdata['selectedBizOps'].map(item => item['label']).toString();    
        this.editCellCampaign(req,message);
        break;

      case 'c_budget_cost':
       if(event.keyCode == 13 && value != null && value >= 0){     
          rowdata['c_budget_cost'] = value;
          req['c_budget_cost'] = value;
          if(rowdata['is_line_item']){
            message = 'LID-'+rowdata['server_campaign_id'] +' budget updated successfully'
            req['lineItemId'] = rowdata['line_item_id']
            this.editCellLineItem(req,message);
           }else {  
            message = 'CID-'+rowdata['server_campaign_id'] +' budget updated successfully'
             this.editCellCampaign(req,message);   
           }
       } 
       break;

       case 'cpm_rate':         
        if(event.keyCode == 13 && value != null && value >= 0){
           rowdata['cpm_rate'] = value;
           req['cpm_rate'] = value;
           if(rowdata['is_line_item']){
              message = 'LID-'+rowdata['server_campaign_id'] +' rate updated successfully'
              req['lineItemId'] = rowdata['line_item_id']
              this.editCellLineItem(req,message);
           }else {  
              message = 'CID-'+rowdata['server_campaign_id'] +' rate updated successfully'
              this.editCellCampaign(req,message);
           }
         }       
        break;

        case 'feasibility':
          if(event.keyCode == 13 && value != null && value != ''){
             rowdata['feasibility'] = value;
             req['feasibility'] = value;
             if(rowdata['is_line_item']){
              message = 'LID-'+rowdata['server_campaign_id'] +' feasibility updated successfully'
              req['lineItemId'] = rowdata['line_item_id']
              this.editCellLineItem(req,message);
           }else {  
              message = 'CID-'+rowdata['server_campaign_id'] +' feasibility updated successfully'
              this.editCellCampaign(req,message);
           }
        }       
          break;

        case 'uniques':
          if(event.keyCode == 13 && value != null && value != ''){
              rowdata['uniques'] = value;
              req['uniques'] = value;
              if(rowdata['is_line_item']){
                message = 'LID-'+rowdata['server_campaign_id'] +' uniques updated successfully'
                req['lineItemId'] = rowdata['line_item_id']
                this.editCellLineItem(req,message);
             }else {  
                message = 'CID-'+rowdata['server_campaign_id'] +' uniques updated successfully'
                this.editCellCampaign(req,message);
             }
            }       
            break;

         case 'white_list':
          if(event.keyCode == 13 && value != null && value != ''){
              rowdata['white_list'] = value;
              req['white_list'] = value;
              message = 'LID-'+rowdata['server_campaign_id'] +' whitelist updated successfully'
              req['lineItemId'] = rowdata['line_item_id']
              this.editCellLineItem(req,message);
            }       
              break;

          case 'black_list':
            if(event.keyCode == 13 && value != null && value != ''){
                rowdata['black_list'] = value;
                req['black_list'] = value;
                message = 'LID-'+rowdata['server_campaign_id'] +' blacklist updated successfully'
                req['lineItemId'] = rowdata['line_item_id']
                this.editCellLineItem(req,message);
              }       
              break;

          case 'ias_pre_bid_implemented':
             if(event.keyCode == 13 && value != null && value != ''){
                  rowdata['ias_pre_bid_implemented'] = value;
                  req['ias_pre_bid_implemented'] = value;
                  message = 'LID-'+rowdata['server_campaign_id'] +' IAS Pre Bid Implemented updated successfully'
                  req['lineItemId'] = rowdata['line_item_id']
                  this.editCellLineItem(req,message);
              }       
              break;
            
            case 'app_dispersion':
              if(event.keyCode == 13 && value != null && value != ''){
                  rowdata['app_dispersion'] = value;
                  req['app_dispersion'] = value;
                  message = 'LID-'+rowdata['server_campaign_id'] +' App dispersion updated successfully'
                  req['lineItemId'] = rowdata['line_item_id']
                  this.editCellLineItem(req,message);
              }       
              break;

            case 'os_dispersion':
              if(event.keyCode == 13 && value != null && value != ''){
                  rowdata['os_dispersion'] = value;
                  req['os_dispersion'] = value;
                  message = 'LID-'+rowdata['server_campaign_id'] +' OS dispersion updated successfully'
                  req['lineItemId'] = rowdata['line_item_id']
                  this.editCellLineItem(req,message);
              }       
              break;

            case 'io_target_imp_completions':
              if(event.keyCode == 13 && value != null && value != ''){
                  rowdata['io_target_imp_completions'] = value;
                  req['io_target_imp_completions'] = value;
                  message = 'LID-'+rowdata['server_campaign_id'] +' IO target imp completions updated successfully'
                  req['lineItemId'] = rowdata['line_item_id']
                  this.editCellLineItem(req,message);
              }       
              break; 
            
            case 'cost_type':
              if(rowdata['is_line_item']){   
                rowdata['cost_type'] = rowdata['cost_type']['label']
                delete req['campaignId'];
                req['cost_type'] = this.costTypesOptionsArray.find(item => item['label'] == rowdata['cost_type'])['key']
                message = 'LID-'+rowdata['server_campaign_id'] +' cost type updated successfully'
                req['lineItemId'] = rowdata['line_item_id']
                this.editFulfilment(req,message);             
              }  
              else
              {   
                req['campaignId'] = rowdata['campaign_id']
                rowdata['cost_type'] = rowdata['cost_type']['label']
                req['cost_type'] = this.costTypesOptionsArray.find(item => item['label'] == rowdata['cost_type'])['key']                                                         
                message = 'Cost type updated for CID-'+rowdata['server_campaign_id'] +' successfully';
                this.editFulfilment(req,message);
             }
              break;

            case 'headway_pacing':
              if(event.keyCode == 13 && value != null && value != ''){
                  rowdata['headway_pacing'] = value;
                  req['headway_pacing'] = value;
                  message = 'LID-'+rowdata['server_campaign_id'] +' headway pacing updated successfully'
                  req['lineItemId'] = rowdata['line_item_id']
                  this.editCellLineItem(req,message);
              }       
              break;
    }
  }

  editCellCampaign(req,message){
    console.log("in edit fun.....");
    
    this.dataFetchServ.updateTeamMembers(req).subscribe((data) => {
      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      } else {
        this.toastService.displayToast({
          severity: 'success',
          summary: 'Campaign Update',
          detail: message
        });
      }
    })
  }

  editFulfilment(req,message){
    this.dataFetchServ.updateFulfilment(req).subscribe((data) => {
      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      } else {
        this.toastService.displayToast({
          severity: 'success',
          summary: 'Campaign Update',
          detail: message
        });
      }
    })
  }

  editCellLineItem(req,message){
    this.dataFetchServ.updateLineItemData(req).subscribe((data) => {
      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      } else {
        this.toastService.displayToast({
          severity: 'success',
          summary: 'Campaign Update',
          detail: message
        });
      }
    })

  }

  onSearchChanged(searchvalue, column, flag) {
    let colValue = this.aggColDef.find(item => item['field'] == column)['value']=searchvalue;    
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    this.timeout = setTimeout(() => {
    let colValue = this.aggColDef.find(item => item['field'] == column);
    this.tableRequest['searchValue'] = [];
    this.aggColDef.find(item => item['field'] == column)['value'] = searchvalue;
    this.aggColDef.forEach(element => {
    let tempField = element['field']
    tempField = "trafficking_source" ? "traffic.source" : element['field'];
    console.log("temFfield",tempField);
    
    if (element.value !== '' && element.value != null) {
      this.tableRequest['searchValue'].push({column : tempField, searchValue : "%"+element['value']+"%"})
      }
    });
    if(this.tableRequest['searchValue'].length == 0) {
      delete this.tableRequest['searchValue'] 
      this.tableRequest['isCampaign'] = true
    }
      this.loadTableData(this.tableRequest);
   }, 3000);
  }

  progressRowClass = (row) => {
    const status = row['c_status'];
    const endDate = moment(row['flight_end'], 'YYYYMMDD');
    const today = moment().startOf('day');
    let rowClass = '';
    if (typeof row['line_item_id'] !== 'undefined') {
      const cStartDate = moment(row['campaign_start'], 'YYYYMMDD');
      const cEndDate = moment(row['campaign_end'], 'YYYYMMDD');
      const startDate = moment(row['flight_start'], 'YYYYMMDD');
      if (endDate.isAfter(cEndDate) || cStartDate.isAfter(startDate)) {
        rowClass = 'ui-grid-row-blue';
      }
    } else {
      const targetImprs = parseInt(row['target_impressions'], 10);
      const campaignBudget = parseInt(row['campaign_budget'], 10);
      if (targetImprs > campaignBudget) {
        rowClass += 'ui-grid-row-dark-megenta';
      }
    }
    if (endDate.isSameOrAfter(today) && status === 'InActive') {
      rowClass = 'ui-grid-row-paused ';
    }
    return rowClass;
  };

  isHiddenColumn(col: Object) {
    return (
      this.aggTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }

  changeSearchFlag(flag) {    
    if(flag){  
      this.tableRequest['isCampaign'] = true;
      this.loadTableData(this.tableRequest);
     }
     else {
      this.tableRequest['isCampaign'] = false;
      this.tableRequest['searchValue'] && this.tableRequest['searchValue'].length > 0 ? this.loadTableData(this.tableRequest) : '';
    }
  }

  onTableDrill(e: Event) {
    if (!e['node']['childLoaded']) {
      this.aggTableJson['loading'] = true;
      
      this.dataFetchServ.getLineItemByCampaignData(e['node']['data']['campaign_id']).subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        const childData = data['data'];
        const arr = [];

        childData.forEach((row: object) => {
          row['server_cid'] = row['server_campaign_id'];
          row['server_campaign_id'] = row['server_li_id'];
          row['campaign'] = row['line_item'];
          row['c_status'] = row['l_status'];
          row['uniques'] = row['l_uniques'];
          row['c_budget_cost'] = row['l_budget_cost'];
          row['is_line_item'] = true;
          let obj = {};
          obj = {
            data: row
          }
          obj['data']['isExpanded'] = '1';
          arr.push(obj);
        });

        this.aggTableJson['loading'] = false;
        e['node']['children'] = <TreeNode[]>arr;
        this.aggTableData = [...this.aggTableData];
        e['node']['childLoaded'] = true;
      });
    }
  }

  osiProgressClass(row, col) {
    const regexp = /^[0-9]+(.[0-9]{1,9})?$/;
    let tagClass = 'badge ';

    if (regexp.test(row['campaign_duration_progress'])) {
      const cellValue = parseFloat(row[col]) - parseFloat(row['campaign_duration_progress']);

      if (cellValue === 0.00) {
        tagClass = 'badge badge-secondary';
      } else if (cellValue < -5.00) {
        tagClass = 'badge badge-danger';
      } else if (cellValue >= -5.00 && cellValue < -3.00) {
        tagClass = 'badge badge-warning';
      } else if (cellValue >= -3.00 && cellValue < 5.00) {
        tagClass = 'badge badge-success';
      } else if (cellValue >= 5.00) {
        tagClass = 'badge badge-primary';
      }
      return tagClass;
    }
    return tagClass;
  }

  discrepancyClass(row, col) {
    let tagClass = 'badge ';
    const cellValue = Math.abs(parseFloat(row[col]));
    if (cellValue === 100.00)
      tagClass = 'badge badge-secondary';
    else if (cellValue > 5)
      tagClass = 'badge badge-danger';
    return tagClass;

  }

  onLazyLoadFlatTable(e: Event) {
    if (
      typeof this.tableRequest !== 'undefined' &&
      this.aggTableJson['lazy']
    ) {
      let orderby = [];
      if (
        typeof e['multiSortMeta'] !== 'undefined' &&
        e['multiSortMeta'] !== null
      ) {
        e['multiSortMeta'].forEach(sort => {
          
          let tempField = sort['field']
          tempField = 'trafficking_source' ? "'source'" : sort['field'];
      
          orderby.push({
            key: tempField,
            opcode: sort['order'] === -1 ? 'desc' : 'asc'
          });
        });
      } else {
        orderby = [{ key: 'campaign_id', opcode: 'ASC' }];
      }     
      
      console.log("order by",orderby);
      
      
      this.tableRequest['limit'] = e['rows'];
      this.tableRequest['offset'] = e['first'];
      this.tableRequest['orderBy'] = orderby;
      this.loadTableData(this.tableRequest);
    }
  }


  vcrTemplateClass(row, col) {
    const cellValue = parseFloat(row[col]);
    let tagClass = 'badge';
    if (cellValue === 0) tagClass = ' badge badge-secondary';
    else if (cellValue > 0 && cellValue < 80.00) tagClass = 'badge badge-danger';
    else if (cellValue >= 80.00 && cellValue < 85.00) tagClass = ' badge badge-warning';
    else if (cellValue >= 85.00) tagClass = 'badge badge-primary';
    return tagClass;
  }

  impressionPerDay(row, col) {
    if ((row[col] * row['days_remaining_for_campaign_end']) > (row['target_impressions'] - (row['trafficking_total_impressions'] - row['trafficking_todays_impressions']))) {
      return 'tag tag-danger';
    }
    return '';
  }

  onFiltersApplied(filterData: object) {
    console.log("filter data",filterData);
    
    let filterStr;  
    for (const k in filterData['filter']['dimensions']) {
        let costTypeArr : any =[];
        if(k == 'cost_type')
        {
          filterData['filter']['dimensions'][k].forEach(element => {
            if(element == 'CPM') 
              costTypeArr.push('2')
            else if(element == 'CPVC')
              costTypeArr.push('4');  
          });
         
        if(costTypeArr.length > 0) 
          {
            if(filterStr == undefined)
              filterStr =  "cost_type_id in ("+ costTypeArr.join(",")+")";
            else
            filterStr = filterStr +"and cost_type_id in ("+ costTypeArr.join(",")+")";
          }
      }
      if( k == 'campaign' && filterData['filter']['dimensions'][k].length){
        if(filterStr == undefined)
           filterStr = k+" in ("+filterData['filter']['dimensions'][k].map(x => "'" + x + "'").toString()+")" 
          else
            filterStr = filterStr+" and "+k+" in ("+filterData['filter']['dimensions'][k].map(x => "'" + x + "'").toString()+")"        
        }
      }

    let dateStr;

    if(filterData['date']){
    for (const k in filterData['date'])
    { 
      let key = Object.keys(filterData['date'][k]);
      
      if(filterData['date'][k][key]){
      if(dateStr == undefined)
        dateStr =  key + ' between '+ filterData['date'][k][key].join(' AND ')       
      else
        dateStr = dateStr +" AND " + key + ' between '+ filterData['date'][k][key].join(' AND ')     
      }
    }
  }

  if(dateStr) filterStr == undefined ? filterStr = dateStr : filterStr = filterStr + " AND " +dateStr; 
    this.tableRequest['filter'] = filterStr;
    this.loadTableData(this.tableRequest);
}

  openEditPopup(row,field){    
    const data = {
      row: row,
      field: field
    };

    const ref = this.dialogService.open(CampaignEditPopupComponent, {
      header: this.aggColDef.find(item => item['field'] == field)['displayName'],
      contentStyle: { width: '50vw', height: '300px', overflow: 'auto' },
      data: data
    });
    ref.onClose.subscribe((data1: string) => { 
      this.loadTableData(this.tableRequest);
    });
  }


  openPopup(row) {
    const data = {
      row: row,
      exportRequest: this.exportRequest
    };

    let header;
    row['is_line_item'] ? header = "Daily Line Item Data : "+`${row['campaign']}` :  header = "Daily Campaign Data : "+`${row['campaign']}`

    const ref = this.dialogService.open(CampaignDailyDataPopupComponent, {
      header: header,
      contentStyle: { width: '90vw', height: '570px', overflow: 'auto' },
      data: data
    });
  }

  linkToBeeswax(row) {
    let src = '';
    let win = null;
    if (typeof row['line_item_id'] === 'undefined') {
      src = `http://alphonso.beeswax.com/advertisers/0/campaigns/${row['server_campaign_id']}/line_items?all=1`;
    } else {
      src = `http://alphonso.beeswax.com/advertisers/0/campaigns/${row['server_cid']}/line_items/${row['server_campaign_id']}/edit`;
    }
    win = window.open(src, '_blank');
    win.focus();
    win = null;
  };

  sendEmail() {
    const imageConfig = {
      imgName: `${this.appConfig['name']}.png`,
      type: 'image/png'
    };

    this.htmltoimage
      .canvasImage(document.getElementById('capture'), imageConfig)
      .then(canvas => {
        this.dialogService.open(SendEmailComponent, {
          header: 'Email Screenshot',
          contentStyle: {
            'max-height': '80vh',
            width: '30vw',
            overflow: 'auto'
          },
          data: canvas.toDataURL('image/png')
        });
      });
  }
  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }
  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['name']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    } else if (type === 'ppt') {
      const chartElements = document.getElementsByTagName('ym-charts');
      const slides = [];
      [].forEach.call(chartElements, (el, index) => {
        const c = el.getElementsByTagName('canvas')[0];

        slides.push({
          title: `${this.appConfig['name']}`,
          base64data: this.libServ.canvasToImage('white', c),
          width: 8,
          height: 3
        });

        if (index === chartElements.length - 1) {
          const pptConfig = {
            filename: 'Charts',
            logo: {
              visible: true,
              path: '/assets/images/ym.png',
              width: 0.53125,
              height: 0.458333
            },
            slides: slides
          };
          this.pptExport.exportPPT(pptConfig);
        }
      });
    }
    return false;
  }

  clickOnComment(comment: object) {
    const req1 = {
      user_id: this.appConfig['user']['id'],
      app_id: this.appConfig['id']
    };

    this.commentservice
      .getAppCommentData(req1)
      .subscribe(commentData => {        
        if (commentData['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          return;
        }
        // this.commentData = commentData['data'];
        if (typeof comment['config'] == 'string') {
          this.commentJson['config'] = JSON.parse(comment['config']);
        } else {
          this.commentJson['config'] = comment['config'];
        }

        const data = {};
        const CommentSortData = [];
        commentData['data'].forEach(element => {
          const commentconfig = JSON.parse(element.config);
          if (
            this.commentJson['config']['campaign'] == commentconfig.campaign
            && this.commentJson['config']['lineitem'] == commentconfig.lineitem
          ) {
            CommentSortData.push(element);
          }
        });
        data['commentJson'] = this.commentJson;
        data['commentData'] = CommentSortData;
        const chatSetting = {
          showChatMsg: false,
          title: `${this.commentJson['config']['comment_on'] == 'campaign'
            ? this.commentJson['config']['campaign']
            : this.commentJson['config']['lineitem']
            }`,
          showCommentTextArea: true,
          chatURL: '/active-campaigns/send/message',
          data: data
        };
        const ref = this.dialogService.open(ChatComponent, {
          header: `Comment on ${
            this.commentJson['config']['comment_on'] == 'campaign'
              ? this.commentJson['config']['campaign']
              : this.commentJson['config']['lineitem']
            }`,
          contentStyle: { width: '30vw', height: '370px', overflow: 'auto' },
          data: chatSetting,
          width: '30vw'
        });

        ref.onClose.subscribe(() => {          
          // let UnreadCommentIdList = CommentSortData.filter(o => o.is_read === "N"
          // && o.user_id != this.commentJson['user_id']).map(x => x.comment_id);

          // console.log('popup close', CommentSortData, UnreadCommentIdList);
          // if (UnreadCommentIdList.length > 0) {
          this.UpdateBellIcon();
          // }
        });
      });

  }

  addComment(row) {
    const req1 = {
      user_id: this.appConfig['user']['id'],
      app_id: this.appConfig['id']
    };

    this.commentservice
      .getAppCommentData(req1)
      .subscribe(commentData => {
        if (commentData['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          return;
        }
        const data = {};
        const CommentSortData = [];
        const config = {
          comment_on: row['line_item'] ? 'lineitem' : 'campaign',
          campaign: row['campaign'],
          lineitem: row['line_item'] ? row['line_item'] : row['campaign']
        };
        commentData['data'].forEach(element => {
          const commentconfig = JSON.parse(element.config);          
          if (config.campaign == commentconfig.campaign && config.lineitem == commentconfig.lineitem) {
            CommentSortData.push(element);
          }
        });

        this.commentJson['config'] = config;
        data['commentJson'] = this.commentJson;
        data['commentData'] = CommentSortData;
        const chatSetting = {
          showChatMsg: false,
          title: row['line_item'] ? row['line_item'] : row['campaign'],
          showCommentTextArea: true,
          chatURL: '/active-campaigns/send/message',
          data: data
        };

        const ref = this.dialogService.open(ChatComponent, {
          header: `Comment on ${row['line_item'] ? row['line_item'] : row['campaign']}`,
          contentStyle: { width: '30vw', height: '370px', overflow: 'auto' },
          data: chatSetting,
          width: '30vw'
        });

        ref.onClose.subscribe(() => {
          // let UnreadCommentIdList = CommentSortData.filter(o => o.is_read === "N" && o.user_id != this.commentJson['user_id']).map(x => x.comment_id);
          // console.log('popup close', CommentSortData,UnreadCommentIdList);
          // if (UnreadCommentIdList.length > 0) {
          this.UpdateBellIcon();
          // }
        });
      });

  }

  UpdateBellIcon() {
    const req1 = {
      user_id: this.appConfig['user']['id'],
      app_id: this.appConfig['id']
    };

    this.commentservice
      .getAppBellCommentData(req1)
      .subscribe(commentData => {
        if (commentData['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          return;
        }
        this.bellIconLoaded = false;
        this.commentData = commentData['data'];        
        this.chatcount = this.commentData.filter(x => x.is_read === 'N').length;
        this.bellIconLoaded = false;
        setTimeout(() => {
          this.bellIconLoaded = true;
        }, 0);
     });
    }
  onViewSelect(jsonData) {
    this.saveViewJson = JSON.parse(jsonData['saveViewsJson']['state_json']);
  }

  saveViewState(value) {
    this.isSaveView = value['toggleValue'];
  }
}

