import { Component, OnInit } from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { FetchApiDataService } from '../fetch-api-data.service';
import * as moment from 'moment';
import { TreeNode } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef, DialogService} from 'primeng/dynamicdialog';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';

@Component({
  selector: 'ym-campaign-edit-popup',
  templateUrl: './campaign-edit-popup.component.html',
  styleUrls: ['./campaign-edit-popup.component.scss']
})
export class CampaignEditPopupComponent implements OnInit {
  popupTitle = '';
  heading = '';
  rowData = [];
  link;
  date;
  field;

  exportRequest: ExportRequest = <ExportRequest>{};

  constructor(
    private libServ: CommonLibService,
    private formatNumPipe: FormatNumPipe,
    private dataFetchServ: FetchApiDataService,
    private config: DynamicDialogConfig,
    private exportService: ExportdataService,
    private toastService: ToastService,
    private ref: DynamicDialogRef,
    
  ) {
    this.rowData = this.config.data['row'];
    this.popupTitle = this.config.data['title'];
    this.heading = this.config.data['heading'];
    this.field = this.config.data['field']
  }

  ngOnInit() {
    
    if(this.rowData[this.field].match(/\(([^)]+)\)/))
     this.link = this.rowData[this.field].match(/\(([^)]+)\)/)[1];
     this.date = new Date(this.rowData[this.field].replace(this.link,"").replace("()",""))

     if(!Date.parse(this.date)) this.date = null;

     if(!this.link && !this.date && this.rowData[this.field])
        this.link = this.rowData[this.field].replace("()","")
  }

 updateData(){
    let message;
    let req = {campaignId : this.rowData['campaign_id']}

    req[this.field] = ""; 
    if(this.date && this.date != '' && this.link)
     req[this.field] = moment(this.date).format('YYYY-MM-DD')+"("+this.link+")";
    else if(this.date && this.date != '')
      req[this.field] = moment(this.date).format('YYYY-MM-DD');//this.date;
    else if(this.link)
       req[this.field] = "()"+this.link;  

    message = 'CID-'+this.rowData['server_campaign_id'] +' updated successfully'
       
    this.editCampaignData(req,message);   
 }

  onDateChanged(date){
    if(date)
      this.date = moment(date).format('YYYY-MM-DD');
    else
      this.date = "";        
  }

editCampaignData(req,message){
  this.dataFetchServ.updateTeamMembers(req).subscribe((data) => {
    if (data['status'] === 0) {
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Server Error',
        detail: 'Please refresh the page'
      });
      return;
    } else {
      this.toastService.displayToast({
        severity: 'success',
        summary: 'Campaign Update',
        detail: message
      });
      this.ref.close(req['tunein']);
    }
  })
}

close(){
  this.ref.close();
}

}
 