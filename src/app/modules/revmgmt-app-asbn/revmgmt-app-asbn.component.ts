import {
  Component,
  OnInit,
  QueryList,
  ElementRef,
  ViewChildren,
  OnDestroy
} from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { FetchApiDataService } from './fetch-api-data.service';
import { ToastService } from '../../_services/toast-notification/toast.service';
import { ConfirmationService, TreeNode } from 'primeng/api';
import { RevmgmtAppAsbnDailyComponent } from './revmgmt-app-asbn-daily/revmgmt-app-asbn-daily.component';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import * as moment from 'moment';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { Title } from '@angular/platform-browser';
import { DataShareService } from 'src/app/_services/data-share/data-share.service';
import { Subscription } from 'rxjs';
import { float } from 'html2canvas/dist/types/css/property-descriptors/float';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { SendEmailComponent } from '../send-email/send-email.component';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { environment } from 'src/environments/environment';
import { DialogService } from 'primeng';

@Component({
  selector: 'ym-revmgmt-app-asbn',
  templateUrl: './revmgmt-app-asbn.component.html',
  styleUrls: ['./revmgmt-app-asbn.component.scss']
})
export class RevMgmtAppAsbnComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;
  timeZone = environment.timeZone;
  appConfig: object = {};
  cardsJson = [];
  showCards = false;
  noDataPieChart = false;
  showMainLineChart = false;
  noDataMainLineChart = false;
  showMainPieChart = false;
  showCharts = false;
  defaultChartsJson: object;
  mainLineChartJson: object;
  mainPieChartJson: object;
  revenueChartJson: object;
  ecpmChartJson: object;
  fillrateChartJson: object;
  impressionsChartJson: object;
  searchInput: any;
  cols: any[];
  conditionNames: any[];
  timeout: any;
  fieldName: any;
  value: any;
  isExportReport = false;
  @ViewChildren('ttAgg') aggTableRef: QueryList<ElementRef>;
  @ViewChildren('ttFlat') flatTableRef: QueryList<ElementRef>;

  /* ---------------------------------- Aggreagated Table JSON --------------------------------- */

  //  ''33Across','AppNexus','Connatix','Conversant','Google Ad Exchange',
  // 'Google Ad Exchange - PD & PG','Google Adsense for Search',' Google EB - EB.IndexExchange',
  // 'Google EB - EB.PubMatic',
  // 'Google EB - EB.Rubicon','Google EB - EB.RythmOne',' Google EB - EB.Smaato',' Google EB - EB.Sovrn',
  // 'Google EB - EB.SpotX','Google EB - EB.YieldMo','Google EB - Frankly.IndexExchange','Google EB - Frankly.Rubicon',
  // 'Google EB - Frankly.RythmOne','Google EB - Frankly.Smaato-EB','Google EB - Frankly.Sovrn',
  // 'Google EB - Frankly.YieldMo-EB',' Google EB - [nw] Openx','Google EB - [nw] PubMatic','Google EB - [nw] TripleLift',
  // 'RevContent','Rubicon','Smart Asset','YieldMo''

  sourcesJson = {
    'Xandr': {
      columns: [
        'xandr_revenue',
        'xandr_cpm',
        'xandr_impressions',
        'xandr_ctr',
        'xandr_fill_rate',
        'xandr_viewability'
      ],
      displayName: 'Xandr',
      defaultView: true,
      width: 150
    },
    'VDX': {
      columns: [
        'vdx_revenue',
        'vdx_cpm',
        'vdx_impressions',
        'vdx_fill_rate'
      ],
      displayName: 'VDX',
      defaultView: true,
      width: 150
    },
    'Airguide': {
      columns: [
        'airguide_revenue',
        'airguide_cpm',
        'airguide_impressions',
        'airguide_ctr',
        'airguide_fill_rate',
        'airguide_viewability'
      ],
      displayName: 'Airguide',
      defaultView: true,
      width: 150
    },
    'MonetizeMore': {
      columns: [
        'monetizemore_revenue',
        'monetizemore_cpm',
        'monetizemore_impressions',
        'monetizemore_fill_rate'
      ],
      displayName: 'MonetizeMore',
      defaultView: true,
      width: 150
    },
  };

  chartsMultiselectButton: object = {
    data: [
      {
        label: 'Revenue',
        value: 'revenue'
      },
      {
        label: 'eCPM',
        value: 'ecpm'
      },
      {
        label: 'Fill Rate',
        value: 'fillrate'
      },
      {
        label: 'Impressions',
        value: 'impressions'
      }
    ],
    model: ['revenue']
  };

  lastUpdatedOn: Date;
  /* ---------------------------------- Table --------------------------------- */

  flatTableReq: object;
  aggreTableReq: object;
  flatTableData: TreeNode[];
  flatTableColumnDef: any[];
  flatTableJson: Object;

  aggTableData: TreeNode[];
  aggTableColumnDef: any[];
  aggTableJson: object;
  jumpToSource: object = {
    totalSources: [],
    selected: []
  };
  showJumpTo = true;
  nextUpdated: Date;
  dataUpdatedThrough: Date;

  selectedChartName;
  filtersApplied: object = {};
  exportRequest: ExportRequest = <ExportRequest>{};

  appFeaturesConfigs = {};
  mainCardsConfig = {};

  /* ---------------------------------- Table --------------------------------- */

  constructor(
    private appConfigService: AppConfigService,
    private _titleService: Title,
    private libServ: CommonLibService,
    private dataFetchServ: FetchApiDataService,
    private toastService: ToastService,
    private dialogService: DialogService,
    private htmltoimage: HtmltoimageService,
    private pptExport: ExportPptService,
    private exportService: ExportdataService,
    private formatNumPipe: FormatNumPipe,
    private confirmationService: ConfirmationService
  ) { }

  ngOnInit() {
    /* ---------------------------------- Cards --------------------------------- */

    /* --------------------------------- Charts --------------------------------- */

    this.defaultChartsJson = {
      chartTypes: [
        { key: 'line', label: 'Linear Trend' },
        { key: 'bar', label: 'Stack Trend', stacked: true }
        // { key: 'pie', label: 'Pie Chart' },
        // { key: 'radar', label: 'Radar Chart' },
        // { key: 'doughnut', label: 'Doughnut Chart' },
        // { key: 'polarArea', label: 'PolarArea Chart' },
        // { key: 'bubble', label: 'Bubble Chart' }
      ],
      chartData: {
        labels: [],
        datasets: []
      },
      chartOptions: {
        title: {
          display: true,
          text: ''
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              display: true,
              scaleLabel: {
                display: true
              }
            }
          ]
        },
        pan: {
          enabled: true,
          mode: 'x'
        },
        zoom: {
          enabled: true,
          mode: 'x'
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    /* ------------------------------- Table Start ------------------------------ */

    this.flatTableColumnDef = [
      {
        field: 'time_key',
        displayName: 'Date',
        format: 'date',
        width: '150',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'source',
        displayName: 'Source',
        format: '',
        value: '',
        condition: 'like',
        columnType: 'dimensions',
        isClearSearch: true,
        width: '175',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'revenue',
        displayName: 'Revenue',
        format: '$',
        width: '175',
        value: '',
        condition: 'GTE',
        columnType: 'metrics',
        isClearSearch: true,
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'cpm',
        displayName: 'eCPM',
        format: '$',
        width: '175',
        value: '',
        condition: 'GTE',
        columnType: 'metrics',
        isClearSearch: true,
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'fill_rate',
        displayName: 'Fill Rate',
        format: 'percentage',
        width: '175',
        value: '',
        condition: 'GTE',
        columnType: 'metrics',
        isClearSearch: true,
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'impressions',
        displayName: 'Paid Impressions',
        format: 'number',
        value: '',
        condition: 'GTE',
        columnType: 'metrics',
        isClearSearch: true,
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        width: '175',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      }
    ];
    this.flatTableJson = {
      page_size: 10,
      page: 0,
      lazy: false,
      loading: true,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '200px',
      totalRecords: 1000,
      columns: this.flatTableColumnDef.slice(1),
      selectedColumns: this.flatTableColumnDef.slice(1),
      frozenCols: [this.flatTableColumnDef[0]],
      // frozenWidth: '250px',
      frozenWidth:
        this.flatTableColumnDef
          .slice(0, 1)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: true
    };
    this.aggTableColumnDef = [
      {
        field: 'time_key',
        displayName: 'Month-Year',
        format: 'date',
        formatConfig: ['MMM-YY'],
        width: '115',
        exportConfig: {
          format: 'date<<MMM-YY<<-',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'time_key1',
        displayName: 'Date',
        format: 'date',
        width: '95',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: ['MM-DD-YYYY'],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'source_revenue',
        displayName: 'Total Revenue',
        width: '135',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'source_cpm',
        displayName: 'Total eCPM',
        format: '$',
        width: '125',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'source_impressions',
        displayName: 'Total Impressions',
        format: 'number',
        width: '120',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      }
    ];
    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '300px',
      totalRecords: 1000,
      columns: this.aggTableColumnDef.slice(5),
      selectedColumns: this.aggTableColumnDef.slice(5),
      frozenCols: [...this.aggTableColumnDef.slice(0, 5)],
      frozenWidth:
        this.aggTableColumnDef
          .slice(0, 5)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: false,
      columnSearch: false
    };

    this.conditionNames = [
      { label: 'GTE', value: '<<GTE' },
      { label: 'LTE', value: '<<LTE' }
      // { label: 'Equal', value: '' },
      // { label: 'like', value: '<<like' }
    ];

    this.searchInput = '>';
    // this.fieldName='revenue';
    this.value = this.conditionNames[0].value;

    /* -------------------------------- Table End ------------------------------- */

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        console.log('appConfig', this.appConfig);
        this.isExportReport = this.appConfig['permissions'].some(
          o => o.name === 'rev-mgmt-asbn-export-reports'
        );
        this._titleService.setTitle(this.appConfig['displayName']);
        let startDate;
        let endDate;
        const date_config = this.appConfig['filter']['filterConfig']['filters'][
          'datePeriod'
        ][0];

        if (
          date_config['defaultDate'][0]['value'] === 0 &&
          date_config['defaultDate'][0]['period'] === 'month' &&
          (moment().format('DD') === '01' || moment().format('DD') === '02')
        ) {
          startDate = moment()
            .subtract(1, 'months')
            .startOf('month');
          endDate = moment()
            .subtract(1, 'months')
            .endOf('month');
        } else {
          if (date_config['defaultDate'][0]['startOf']) {
            startDate = moment()
              .subtract(
                date_config['defaultDate'][0]['value'],
                date_config['defaultDate'][0]['period']
              )
              .startOf(date_config['defaultDate'][0]['period']);
          } else {
            startDate = moment().subtract(
              date_config['defaultDate'][0]['value'],
              date_config['defaultDate'][0]['period']
            );
          }
          endDate = moment().subtract(
            date_config['defaultDate'][1]['value'],
            date_config['defaultDate'][1]['period']
          );
        }
        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: startDate.format('YYYYMMDD'),
            time_key2: endDate.format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: this.appConfig['filter']['filterConfig']['groupBy'].filter(
            v => v.selected
          )
        };
        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        this.initialLoading();
      }
    });
  }

  /* ------------------------------- Table Start ------------------------------ */

  initialLoading() {
    console.log('this.appConfig last up', this.appConfig['id']);
    this.dataFetchServ
      .getLastUpdatedData(this.appConfig['id'])
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        this.lastUpdatedOn = moment(data[0]['updated_at']).toDate();
        this.nextUpdated = moment(data[0]['next_run_at']).toDate();
        this.dataUpdatedThrough = moment(
          data[0]['source_updated_through'],
          'YYYYMMDD'
        ).toDate();
      });

    this.libServ.getAppFeaturesConfigs(this.appConfig['id']).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: data['message']
        });
        console.log(data['message']);
        return;
      }
      this.appFeaturesConfigs = data['data'];
      this.appFeaturesConfigs['cardsConfig']['cards'].forEach(element => {
        if (element['name'] === 'main') {
          this.mainCardsConfig = element['config'];
          this.cardsJson = this.mainCardsConfig['list'];
          if (this.mainCardsConfig['display']) {
            this.loadCards();
          }
        }
      });
    });

    const mainLineChartReq = {
      dimensions: ['time_key'],
      metrics: ['revenue', 'impressions'],
      derived_metrics: ['cpm'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: [
        // 'ad_size',
        // 'ad_type',
        // 'ad_units',
        // 'bidding_type',
        // 'device_category',
        // 'geography',
        // 'sales_channel',
        // 'sales_type',
        // 'site',
        'source'
      ],
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };

    // For dev purpose
    // mainLineChartReq['gidGroupBy'] = [
    //   'ad_size',
    //   'ad_type',
    //   'ad_units',
    //   'bidding_type',
    //   'device_category',
    //   'geography',
    //   'sales_channel',
    //   'sales_type',
    //   'site',
    //   'source'
    // ];
    this.loadMainLineChart(mainLineChartReq);
    const mainPieChartReq = {
      dimensions: ['source'],
      metrics: ['revenue'],
      derived_metrics: [],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [],
      limit: '',
      offset: ''
    };
    this.loadMainPieChart(mainPieChartReq);

    const chartReq = {
      dimensions: ['source', 'time_key'],
      metrics: [
        'revenue',
        'impressions',
        'clicks',
        'ad_requests'
      ],
      derived_metrics: ['cpm', 'fill_rate'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };
    this.loadCharts(chartReq);
    this.flatTableReq = {
      dimensions: ['source', 'time_key'],
      metrics: [
        'revenue',
        'impressions',
        'clicks',
        'ad_requests'
      ],
      derived_metrics: ['cpm', 'fill_rate'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [],
      limit: '',
      offset: ''
    };

    this.loadFlatTableData(this.flatTableReq);

    this.aggreTableReq = {
      //    'brightcom_dfp_impressions', 'brightcom_dfp_clicks', 'brightcom_dfp_ctr'
      dimensions: ['accounting_key'],
      metrics: [
        'revenue',
        'impressions',
        'clicks',
        'ad_requests',
        'viewable_impressions',
        'measurable_impressions'
      ],
      derived_metrics: ['cpm', 'ctr', 'fill_rate', 'viewability'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [],
      limit: '',
      offset: ''
    };

    // For dev purpose
    // this.aggreTableReq['gidGroupBy'] = [
    //   'ad_size',
    //   'ad_type',
    //   'ad_units',
    //   'bidding_type',
    //   'device_category',
    //   'geography',
    //   'sales_channel',
    //   'sales_type',
    //   'site',
    //   'source'
    // ];
    //  this.loadAggTable(this.aggreTableReq);
  }

  loadMainLineChart(params) {
    const colors = this.libServ.dynamicColors(2);
    this.mainLineChartJson = {
      chartTypes: [{ key: 'bar', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'eCPM',
            type: 'line',
            yAxisID: 'y-axis-0',

            borderColor: colors[0],
            fill: false,
            backgroundColor: colors[0],
            data: []
          },
          {
            label: 'Revenue',
            type: 'bar',
            yAxisID: 'y-axis-1',

            borderColor: colors[1],
            fill: false,
            backgroundColor: colors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Revenue vs eCPM'
        },
        legend: {
          display: true,
          // onClick: e => e.stopPropagation()
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: 'eCPM ($)'
              },
              position: 'right',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', []);
                }
              }
              // scaleFontColor: 'rgba(151,137,200,0.8)'
            },
            {
              id: 'y-axis-1',
              scaleLabel: {
                display: true,
                labelString: 'Revenue ($)'
              },
              position: 'left',
              name: '2',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', []);
                }
              }
              // scaleFontColor: 'rgba(151,187,205,0.8)'
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    this.showMainLineChart = false;
    this.dataFetchServ.getTableData(params).subscribe(data => {
      const chartData = data['data'];
      this.noDataMainLineChart = false;
      if (!chartData.length) {
        this.noDataMainLineChart = true;
        return;
      }
      const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));

      this.mainLineChartJson['chartData']['labels'] = datesArr.map(d =>
        moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
      );

      const revArr = [];
      const ecpmArr = [];
      datesArr.forEach(time_key => {
        chartData.forEach(r => {
          if (r['time_key'] === time_key) {
            revArr.push(r['revenue']);
            ecpmArr.push(r['cpm']);
          }
        });
      });
      this.mainLineChartJson['chartData']['datasets'][0]['data'] = ecpmArr;
      this.mainLineChartJson['chartData']['datasets'][1]['data'] = revArr;
      this.showMainLineChart = true;
    });
  }

  loadMainPieChart(params) {
    params['orderBy'] = [{ key: 'revenue', opcode: 'desc' }];

    this.mainPieChartJson = {
      chartTypes: [{ key: 'pie', label: 'Pie Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Revenue Distribution Across Partners'
        },
        legend: {
          display: false
          // labels: {
          //   generateLabels: function(chart) {
          //     var data = chart.data;
          //     console.log('chart' ,chart)
          //     if (data.labels.length && data.datasets.length) {
          //       return data.labels.map(function(label, i) {
          //         var meta = chart.getDatasetMeta(0);
          //         var style = meta.controller.getStyle(i);

          //         return {
          //           text: label,
          //           fillStyle: style.backgroundColor,
          //           strokeStyle: style.borderColor,
          //           lineWidth: style.borderWidth,
          //           hidden: isNaN(data.datasets[0].data[i]) || meta.data[i].hidden,

          //           // Extra data used for toggling the correct item
          //           index: i
          //         };
          //       });
          //     }
          //     return [];
          //   }
          // }
        },
        elements: {
          arc: {
            borderWidth: 0
          }
        },
        plugins: {
          datalabels: {
            formatter: (value, ctx) => {
              let sum = 0;
              const dataArr = ctx.chart.data.datasets[0].data;
              dataArr.map(data => {
                sum += data;
              });
              const percentage = (value * 100) / sum;
              if (percentage > 10) {
                return `${percentage.toFixed(2)} %`;
                // ${this.formatNumPipe.transform(
                //   value,
                //   '$',
                //   []
                // )} - ${percentage.toFixed(2)} %`;
              } else {
                return '';
              }
            },
            color: 'black'
          }
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };
    this.mainPieChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          let sum = 0;
          const dataArr = data.datasets[0].data;
          dataArr.map(data => {
            sum += data;
          });
          const percentage = (currentValue * 100) / sum;
          return `${
            data.labels[tooltipItem.index]
            } : ${this.formatNumPipe.transform(
              currentValue,
              '$',
              []
            )} - ${percentage.toFixed(2)} %`;
        }
      }
    };
    this.showMainPieChart = false;
    this.dataFetchServ.getTableData(params).subscribe(data => {
      const chartData = data['data'];
      if (typeof chartData === 'undefined' || !chartData.length) {
        this.noDataPieChart = true;
      } else {
        const sources = Array.from(
          new Set(chartData.map(s => s['source']))
        );
        const colors = this.libServ.dynamicColors(sources.length);
        this.mainPieChartJson['chartData']['labels'] = sources;
        this.mainPieChartJson['chartData']['datasets'][0]['data'] = Array.from(
          new Set(chartData.map(s => s['revenue']))
        );
        this.mainPieChartJson['chartData']['datasets'][0][
          'backgroundColor'
        ] = colors;
        this.showMainPieChart = true;
        this.noDataPieChart = false;
      }
    });
  }

  resetPagination(tt1, tt2) {
    if (typeof tt1 !== 'undefined') {
      tt1.reset();
    }
    if (typeof tt2 !== 'undefined') {
      tt2.reset();
    }
  }

  loadCards() {
    this.showCards = false;
    const apiUrl = this.mainCardsConfig['api']['url'];
    const cardsReq = this.libServ.deepCopy(
      this.mainCardsConfig['api']['request']
    );
    cardsReq['timeKeyFilter'] = this.libServ.deepCopy(
      this.filtersApplied['timeKeyFilter']
    );
    cardsReq['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);

    this.libServ.getDataFromDynamicAPI(apiUrl, cardsReq).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error - Contact to administator',
          detail: data
        });
        console.log(data['status_msg']);
        return;
      }
      if (!this.libServ.isEmptyObj(data['data'])) {
        this.cardsJson[1].format = '$';
        const endDate = moment(
          this.filtersApplied['timeKeyFilter']['time_key2']
        ).isAfter(moment())
          ? moment().format('YYYYMMDD')
          : moment(this.filtersApplied['timeKeyFilter']['time_key2']).format(
            'YYYYMMDD'
          );
        const startDate = moment(
          this.filtersApplied['timeKeyFilter']['time_key1']
        ).isAfter(moment())
          ? moment()
            .startOf('month')
            .format('YYYYMMDD')
          : moment(this.filtersApplied['timeKeyFilter']['time_key1']).format(
            'YYYYMMDD'
          );
        const monthEnd = moment(startDate)
          .endOf('month')
          .format('YYYYMMDD');
        let dateDiff = parseInt(endDate) - (parseInt(startDate) - 1);

        if (
          dateDiff &&
          !moment(
            this.filtersApplied['timeKeyFilter']['time_key1'],
            'YYYYMMDD'
          ).isBefore(moment().startOf('month'))
        ) {
          const dailyRev =
            parseFloat(data['data'][0]['revenue']) / dateDiff;
          dateDiff = parseInt(monthEnd) - (parseInt(startDate) - 1);
          const expectedRev = dailyRev * dateDiff;
          data['data'][0]['projected_revenue'] = expectedRev;

          // if (
          //   this.cardsJson.findIndex(
          //     item => item.field == 'projected_revenue'
          //   ) == -1
          // ) {
          // const obj = {
          //   field: 'projected_revenue',
          //   displayName: 'Projected Revenue',
          //   value: 'N/A',
          //   imageClass: 'fas fa-hand-holding-usd',
          //   format: '$',
          //   config: [],
          //   color: '#88767d'
          // };
          // this.cardsJson.splice(1, 0, obj);
        } else {
          this.cardsJson[1].format = '';
          data['data'][0]['projected_revenue'] = 'N/A';
        }
        //   if (
        //     this.cardsJson.findIndex(
        //       item => item.field == 'projected_revenue'
        //     ) > -1
        //   ) {
        //     this.cardsJson.splice(1, 1);
        //   }
        // }

        this.cardsJson.map(o => {
          o['value'] = data['data'][0][o['field']];
        });

      } else {
        this.cardsJson[1].format = '$';
        this.cardsJson.map(o => {
          o['value'] = 0;
          this.showCards = true;
        });
      }
      this.getDaysAverage(apiUrl, cardsReq,data['data']);
      this.showCards = this.mainCardsConfig['display'];
    });
  }

  getDaysAverage(apiUrl,params, currentData) {
    const totalDataDateDiff = moment(params['timeKeyFilter']['time_key2'], 'YYYYMMDD').diff(
      moment(params['timeKeyFilter']['time_key1'], 'YYYYMMDD'),
      'days'
    );
    // const totalDataDateDiff = parseInt(params['timeKeyFilter']['time_key2'], 10) - parseInt(params['timeKeyFilter']['time_key1'], 10);
    const time_key1 = moment(params['timeKeyFilter']['time_key1'], 'YYYYMMDD')
      .subtract('days', 1)
      .format('YYYYMMDD');
    const time_key2 = moment(time_key1, 'YYYYMMDD')
      .subtract('days', totalDataDateDiff)
      .format('YYYYMMDD');
    params['timeKeyFilter'] = { 'time_key1': time_key2, 'time_key2': time_key1 };

    this.libServ.getDataFromDynamicAPI(apiUrl,params).subscribe(data => {
      if (data['data'].length &&  currentData.length) {
        const last7Days = data['data'][0];
        if (!this.libServ.isEmptyObj(last7Days)) {
          this.cardsJson.map(o => {
            let last7DaysAvg =  0;
            let totalAvg =  0;
            if(o['metaConfig']){
              if (o.field == 'impressions' || o.field == 'revenue') {
                last7DaysAvg = parseFloat(last7Days[o['field']]) / totalDataDateDiff + 1 || 0;
                totalAvg = parseFloat(currentData[0][o['field']]) / (totalDataDateDiff + 1) || 0;
              }
              else {
                totalAvg = parseFloat(currentData[0][o['field']]);
                last7DaysAvg = parseFloat(last7Days[o['field']]);
              }
            o['metaConfig']['avgValue'] = ((totalAvg - last7DaysAvg) / totalAvg * 100).toFixed(2);
            o['metaConfig']['icon'] = totalAvg >= last7DaysAvg ? 'fas fa-arrow-up green' : 'fas fa-arrow-down red';
            o['metaConfig']['dateRange'] = `${moment(params['timeKeyFilter']['time_key1'], 'YYYYMMDD').format('MMM-DD-YYYY')} - ${moment(params['timeKeyFilter']['time_key2'], 'YYYYMMDD').format('MMM-DD-YYYY')}`;
            }
          });
        }
        this.showCards = true;
      } else {
        this.cardsJson.map(o => {
          if(o['metaConfig']){
          o['metaConfig']['avgValue'] = 'N/A';
          o['metaConfig']['icon'] = 'fas fa-minus';
          o['metaConfig']['dateRange'] = `${moment(params['timeKeyFilter']['time_key1'], 'YYYYMMDD').format('MMM-DD-YYYY')} - ${moment(params['timeKeyFilter']['time_key2'], 'YYYYMMDD').format('MMM-DD-YYYY')}`;
          }
        });
        this.showCards = true;
      }
    });
  }

  loadCardsOld() {
    console.log(" in old cards");

    this.showCards = false;
    const params = {};
    this.dataFetchServ.getTableData(params).subscribe(data => {
      if (!this.libServ.isEmptyObj(data['data'])) {
        this.cardsJson[1].format = '$';
        const endDate = moment(
          this.filtersApplied['timeKeyFilter']['time_key2']
        ).isAfter(moment())
          ? moment().format('YYYYMMDD')
          : moment(this.filtersApplied['timeKeyFilter']['time_key2']).format(
            'YYYYMMDD'
          );
        const startDate = moment(
          this.filtersApplied['timeKeyFilter']['time_key1']
        ).isAfter(moment())
          ? moment()
            .startOf('month')
            .format('YYYYMMDD')
          : moment(this.filtersApplied['timeKeyFilter']['time_key1']).format(
            'YYYYMMDD'
          );
        const monthEnd = moment(startDate)
          .endOf('month')
          .format('YYYYMMDD');
        let dateDiff = parseInt(endDate) - (parseInt(startDate) - 1);

        if (
          dateDiff &&
          !moment(
            this.filtersApplied['timeKeyFilter']['time_key1'],
            'YYYYMMDD'
          ).isBefore(moment().startOf('month'))
        ) {
          const dailyRev =
            parseFloat(data['data'][0]['revenue']) / dateDiff;
          dateDiff = parseInt(monthEnd) - (parseInt(startDate) - 1);
          const expectedRev = dailyRev * dateDiff;
          data['data'][0]['projected_revenue'] = expectedRev;

          // if (
          //   this.cardsJson.findIndex(
          //     item => item.field == 'projected_revenue'
          //   ) == -1
          // ) {
          // const obj = {
          //   field: 'projected_revenue',
          //   displayName: 'Projected Revenue',
          //   value: 'N/A',
          //   imageClass: 'fas fa-hand-holding-usd',
          //   format: '$',
          //   config: [],
          //   color: '#88767d'
          // };
          // this.cardsJson.splice(1, 0, obj);
        } else {
          this.cardsJson[1].format = '';
          data['data'][0]['projected_revenue'] = 'N/A';
        }
        //   if (
        //     this.cardsJson.findIndex(
        //       item => item.field == 'projected_revenue'
        //     ) > -1
        //   ) {
        //     this.cardsJson.splice(1, 1);
        //   }
        // }

        this.cardsJson.map(o => {
          o['value'] = data['data'][0][o['field']];
        });
        this.showCards = true;
      } else {
        this.cardsJson[1].format = '$';
        this.cardsJson.map(o => {
          o['value'] = 0;
          this.showCards = true;
        });
      }
    });
  }

  loadCharts(params: Object) {
    // Reset Data
    this.revenueChartJson = this.libServ.deepCopy(this.defaultChartsJson);
    this.revenueChartJson['chartOptions']['title']['text'] = 'Revenue Trend';
    this.revenueChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
      // Include a dollar sign in the ticks
      callback: (value, index, values) => {
        return this.formatNumPipe.transform(value, '$', [0]);
      }
    };
    this.revenueChartJson['chartOptions']['scales']['yAxes'][0]['scaleLabel'][
      'labelString'
    ] = 'Revenue ($)';
    this.revenueChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return `${
            data.datasets[tooltipItem.datasetIndex].label
            } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
        }
      }
    };
    this.ecpmChartJson = this.libServ.deepCopy(this.defaultChartsJson);
    this.ecpmChartJson['chartTypes'] = [
      { key: 'line', label: 'Linear Trend' }
    ]
    this.ecpmChartJson['chartOptions']['title']['text'] = 'eCPM Trend';
    this.ecpmChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
      // Include a dollar sign in the ticks
      callback: (value, index, values) => {
        return this.formatNumPipe.transform(value, '$', []);
      }
    };
    this.ecpmChartJson['chartOptions']['scales']['yAxes'][0]['scaleLabel'][
      'labelString'
    ] = 'eCPM ($)';
    this.ecpmChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return `${
            data.datasets[tooltipItem.datasetIndex].label
            } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
        }
      }
    };
    this.fillrateChartJson = this.libServ.deepCopy(this.defaultChartsJson);
    this.fillrateChartJson['chartTypes'] = [
      { key: 'line', label: 'Linear Trend' }
    ]
    this.fillrateChartJson['chartOptions']['title']['text'] = 'Fill Rate Trend';
    this.fillrateChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
      // Include a dollar sign in the ticks
      callback: (value, index, values) => {
        return this.formatNumPipe.transform(value, 'percentage', [2]);
      }
    };
    this.fillrateChartJson['chartOptions']['scales']['yAxes'][0]['scaleLabel'][
      'labelString'
    ] = 'Fill Rate (%)';
    this.fillrateChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return `${
            data.datasets[tooltipItem.datasetIndex].label
            } : ${this.formatNumPipe.transform(currentValue, 'percentage', [2])}`;
        }
      }
    };
    this.impressionsChartJson = this.libServ.deepCopy(this.defaultChartsJson);
    this.impressionsChartJson['chartOptions']['title']['text'] =
      'Paid Impressions Trend';
    this.impressionsChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
      // Include a dollar sign in the ticks
      callback: (value, index, values) => {
        return this.formatNumPipe.transform(value, 'number', []);
      }
    };
    this.impressionsChartJson['chartOptions']['scales']['yAxes'][0][
      'scaleLabel'
    ]['labelString'] = 'Impressions';
    this.impressionsChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return `${
            data.datasets[tooltipItem.datasetIndex].label
            } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
        }
      }
    };
    this.showCharts = false;
    this.dataFetchServ.getTableData(params).subscribe(data => {
      const chartData = data['data'];

      if (typeof chartData === 'undefined' || !chartData.length) {
        this.showCharts = false;
      } else {
        const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));

        const sources = Array.from(
          new Set(chartData.map(s => s['source']))
        );
        const colors = this.libServ.dynamicColors(sources.length);
        this.revenueChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );
        this.ecpmChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );
        this.fillrateChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );
        this.impressionsChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );

        sources.forEach((src, i) => {
          const revArr = [];
          const ecpmArr = [];
          const fillrateArr = [];
          const impArr = [];
          datesArr.forEach(time_key => {
            chartData.forEach(r => {
              if (r['source'] === src && r['time_key'] === time_key) {
                revArr.push(r['revenue']);
                ecpmArr.push(r['cpm']);
                fillrateArr.push(r['fill_rate']);
                impArr.push(r['impressions']);
              }
            });
          });

          this.revenueChartJson['chartData']['datasets'].push({
            label: src,
            data: revArr,
            borderColor: colors[i],
            fill: false,
            backgroundColor: colors[i]
          });
          this.ecpmChartJson['chartData']['datasets'].push({
            label: src,
            data: ecpmArr,
            borderColor: colors[i],
            fill: false,
            backgroundColor: colors[i]
          });
          this.fillrateChartJson['chartData']['datasets'].push({
            label: src,
            data: fillrateArr,
            borderColor: colors[i],
            fill: false,
            backgroundColor: colors[i]
          });
          this.impressionsChartJson['chartData']['datasets'].push({
            label: src,
            data: impArr,
            borderColor: colors[i],
            fill: false,
            backgroundColor: colors[i]
          });
        });
        this.showCharts = true;
      }
      console.log("revenueChartJson", this.revenueChartJson);

    });
  }

  onLazyLoadFlatTable(e: Event) {
    if (
      typeof this.flatTableReq !== 'undefined' &&
      this.flatTableJson['lazy']
    ) {
      console.log('onLazyLoadFlatTable', this.flatTableJson['lazy']);
      let orderby = [];
      if (
        typeof e['multiSortMeta'] !== 'undefined' &&
        e['multiSortMeta'] !== null
      ) {
        e['multiSortMeta'].forEach(sort => {
          orderby.push({
            key: sort['field'],
            opcode: sort['order'] === -1 ? 'desc' : 'asc'
          });
        });
      } else {
        orderby = [{ key: 'time_key', opcode: 'ASC' }];
      }
      // orderby = orderby.slice(0, orderby.length - 1);
      this.flatTableReq['limit'] = e['rows'];
      this.flatTableReq['offset'] = e['first'];
      this.flatTableReq['orderBy'] = orderby;
      this.loadFlatTableData(this.flatTableReq);
    }
  }

  onLazyLoadAggTable(e: Event) {
    console.log('Lazy Agg', e);
  }

  loadFlatTableData(params: Object) {
    this.flatTableJson['loading'] = true;

    this.dataFetchServ.getTableData(params).subscribe(data => {
      const arr = [];
      for (const r of data['data']) {
        const obj = {};
        obj['children'] = [{}];
        // if (
        //   typeof r['dfp_impressions'] !== 'undefined' &&
        //   r['dfp_impressions'] > 0
        // ) {
        //   r['fill_rate'] = (r['impressions'] * 100) / r['dfp_impressions'];
        // } else {
        //   r['fill_rate'] =
        //     r['request'] > 0
        //       ? (r['impressions'] * 100) / r['request']
        //       : r['fill_rate'];
        // }
        obj['data'] = r;
        arr.push(obj);
      }
      this.flatTableData = <TreeNode[]>arr;
      this.flatTableJson['totalRecords'] = data['totalItems'];
      this.flatTableJson['loading'] = false;
      setTimeout(() => {
        this.flatTableJson['lazy'] = true;
      }, 0);
    });
  }

  isHiddenColumn(col: Object) {
    return (
      this.flatTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.flatTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }

  onTableDrill(e: Event) {
    console.log('Drilled', e);
    if (!e['node']['childLoaded']) {
      this.aggTableJson['loading'] = true;

      let time_key1 = moment(e['node']['data']['time_key'], 'YYYYMMDD')
        .startOf('month')
        .format('YYYYMMDD');
      if (time_key1 < this.filtersApplied['timeKeyFilter']['time_key1']) {
        time_key1 = this.filtersApplied['timeKeyFilter']['time_key1'];
      }
      let time_key2 = moment(e['node']['data']['time_key'], 'YYYYMMDD')
        .endOf('month')
        .format('YYYYMMDD');
      if (time_key2 > this.filtersApplied['timeKeyFilter']['time_key2']) {
        time_key2 = this.filtersApplied['timeKeyFilter']['time_key2'];
      }

      const timeKeyFilter = {
        time_key1: time_key1,
        time_key2: time_key2
      };

      const totalsColreq = {
        dimensions: ['time_key'],
        metrics: ['revenue', 'impressions'],
        derived_metrics: ['cpm'],
        timeKeyFilter: timeKeyFilter, // this.filtersApplied['timeKeyFilter'],
        filters: this.libServ.deepCopy(this.filtersApplied['filters']),
        groupByTimeKey: {
          key: ['time_key', 'accounting_key'],
          interval: 'daily'
        },
        gidGroupBy: ['source'],
        orderBy: [],
        limit: '',
        offset: ''
      };

      const grpBys = this.filtersApplied['groupby'].map(e => e.key);
      for (const g of grpBys) {
        if (e['node']['data'][g] !== 'All') {
          totalsColreq['dimensions'].push(g);

          if (
            totalsColreq['filters']['dimensions'].findIndex(
              e => e.key === g
            ) === -1
          ) {
            totalsColreq['filters']['dimensions'].push({
              key: g,
              values: [e['node']['data'][g]]
            });
          } else {
            if (
              totalsColreq['filters']['dimensions']
                .find(e => e.key === g)
              ['values'].findIndex(v => v === e['node']['data'][g]) === -1
            ) {
              totalsColreq['filters']['dimensions'].push(e['node']['data'][g]);
            }
          }
        } else {
          totalsColreq['dimensions'].push(g);
          break;
        }
      }

      totalsColreq['gidGroupBy'] = ['source'];

      // For dev purpose
      // totalsColreq['gidGroupBy'] = [
      //   'ad_size',
      //   'ad_type',
      //   'ad_units',
      //   'bidding_type',
      //   'device_category',
      //   'geography',
      //   'sales_channel',
      //   'sales_type',
      //   'site',
      //   'source'
      // ];

      const sourcesDatareq = {
        dimensions: ['source', 'time_key', 'accounting_key'],
        metrics: [
          'revenue',
          'impressions',
          'clicks',
          'ad_requests',
          'viewable_impressions',
          'measurable_impressions'
        ],
        derived_metrics: ['cpm', 'ctr', 'fill_rate', 'viewability'],
        timeKeyFilter: timeKeyFilter, // this.filtersApplied['timeKeyFilter'],
        filters: this.libServ.deepCopy(this.filtersApplied['filters']),
        groupByTimeKey: {
          key: ['time_key', 'accounting_key'],
          interval: 'daily'
        },
        gidGroupBy: [
          // 'ad_size',
          // 'ad_type',
          // 'ad_units',
          // 'bidding_type',
          // 'device_category',
          // 'geography',
          // 'sales_channel',
          // 'sales_type',
          // 'site',
          'source'
        ],
        orderBy: [],
        limit: '',
        offset: ''
      };
      // For dev purpose
      // sourcesDatareq['gidGroupBy'] = [
      //   'ad_size',
      //   'ad_type',
      //   'ad_units',
      //   'bidding_type',
      //   'device_category',
      //   'geography',
      //   'sales_channel',
      //   'sales_type',
      //   'site',
      //   'source'
      // ];

      this.dataFetchServ.getTableData(totalsColreq).subscribe(data => {
        this.dataFetchServ.getTableData(sourcesDatareq).subscribe(data1 => {
          const totalsColData = data['data'];
          const srcWiseData = data1['data'];

          const map = this.dataMapping(totalsColData, srcWiseData);
          map.map(o => delete o['children']);
          map.map(o => delete o['data']['time_key']);

          this.aggTableJson['loading'] = false;

          e['node']['children'] = map;
          this.aggTableData = [...this.aggTableData];
          e['node']['childLoaded'] = true;
        });
      });
    }
  }

  /* -------------------------------- Table End ------------------------------- */

  /* ------------------------------- Load Charts ------------------------------ */

  chartSelected(data: Event) {
    console.log('Chart Selected', data);
  }

  getChartJson(chartName) {
    switch (chartName) {
      case 'revenue':
        return this.revenueChartJson;
      case 'ecpm':
        return this.ecpmChartJson;
      case 'fillrate':
        return this.fillrateChartJson;
      case 'impressions':
        return this.impressionsChartJson;
      default:
        break;
    }
  }

  /* --------------------- On Filter Apply Button Clicked --------------------- */

  onFiltersApplied(filterData: object) {
    this.filtersApplied['filters']['dimensions'] = [];
    if (filterData['filter']['dimensions']['source'] != undefined) {
      // for (const source in this.sourcesJson) {
      //   this.sourcesJson[source]['defaultView'] = false;
      // }
      filterData['filter']['dimensions']['source'].forEach(item => {
        if (this.sourcesJson[item] != undefined) {
          this.sourcesJson[item]['defaultView'] = true;
        }
      });

      for (const k in filterData['filter']['dimensions']) {
        if (filterData['filter']['dimensions'][k].length !== 0) {
          this.filtersApplied['filters']['dimensions'].push({
            key: k,
            values: filterData['filter']['dimensions'][k]
          });
        }
      }

      this.filtersApplied['groupby'] = filterData['groupby'];
    }
    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];

    if (this.mainCardsConfig['display']) {
      this.loadCards();
    }

    const chartReq = {
      dimensions: ['source', 'time_key'],
      metrics: [
        'revenue',
        'impressions',
        'ad_requests'
      ],
      derived_metrics: ['cpm', 'fill_rate'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };

    this.loadCharts(chartReq);

    this.flatTableReq = {
      dimensions: ['source', 'time_key'],
      metrics: [
        'revenue',
        'impressions',
        'ad_requests'
      ],
      derived_metrics: ['cpm', 'fill_rate'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [],
      limit: '',
      offset: ''
    };
    this.flatTableJson['lazy'] = false;
    setTimeout(() => {
      this.loadFlatTableData(this.flatTableReq);
    }, 0);

    const mainLineChartReq = {
      dimensions: ['time_key'],
      metrics: ['revenue', 'impressions'],
      derived_metrics: ['cpm'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };

    this.loadMainLineChart(mainLineChartReq);
    const mainPieChartReq = {
      dimensions: ['source'],
      metrics: ['revenue'],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [],
      limit: '',
      offset: ''
    };
    this.loadMainPieChart(mainPieChartReq);

    this.aggreTableReq = {
      dimensions: ['accounting_key'],
      metrics: [
        'revenue',
        'impressions',
        'clicks',
        'ad_requests',
        'viewable_impressions',
        'measurable_impressions'
      ],
      derived_metrics: ['cpm', 'ctr', 'fill_rate', 'viewability'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [],
      limit: '',
      offset: ''
    };

    this.aggTableJson['loading'] = true;
    this.loadAggTable(this.aggreTableReq);
  }

  getGrpBys() {
    let grpBys = this.filtersApplied['groupby'].map(e => e.key);

    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          grpBys.concat(
            this.filtersApplied['filters']['dimensions']
              .filter(f => f.values.length)
              .map(m => m.key)
          )
        )
      );
    }
    return grpBys;
  }

  openDialog(selectedTimkey, fieldName) {
    let tabHeading;
    let dialogData;

    if (fieldName === 'time_key') {
      const time_key1 = moment(selectedTimkey, 'YYYYMMDD')
        .startOf('month')
        .format('YYYYMMDD');
      const time_key2 = moment(selectedTimkey, 'YYYYMMDD')
        .endOf('month')
        .format('YYYYMMDD');

      dialogData = {
        time_key: {
          time_key1: time_key1,
          time_key2: time_key2
        },
        filterData: this.filtersApplied
      };
      tabHeading =
        ' ( Date Range : ' +
        moment(time_key1, 'YYYYMMDD').format('MM-DD-YYYY') +
        ' - ' +
        moment(time_key2, 'YYYYMMDD').format('MM-DD-YYYY') +
        ' )';
    } else {
      dialogData = {
        time_key: {
          time_key1: selectedTimkey,
          time_key2: selectedTimkey
        },
        filterData: this.filtersApplied
      };
      tabHeading =
        '( Day : ' +
        moment(selectedTimkey, 'YYYYMMDD').format('MM-DD-YYYY') +
        ' )';
    }

    this.dialogService.open(RevmgmtAppAsbnDailyComponent, {
      header: 'Sources Trend ' + tabHeading,
      width: '70%',
      data: dialogData,
      styleClass: 'ui-dialog-custom-class'
    });
  }

  showToast() {
    this.toastService.displayToast({
      severity: 'success',
      summary: 'Saple Toast',
      detail: 'Sample App Loaded'
    });
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['name']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    } else if (type === 'ppt') {
      const chartElements = document.getElementsByTagName('ym-charts');
      const slides = [];
      [].forEach.call(chartElements, (el, index) => {
        const c = el.getElementsByTagName('canvas')[0];

        slides.push({
          title: `${this.appConfig['name']}`,
          base64data: this.libServ.canvasToImage('white', c),
          width: 8,
          height: 3
        });

        if (index === chartElements.length - 1) {
          const pptConfig = {
            filename: 'Charts',
            logo: {
              visible: true,
              path: '/assets/images/ym.png',
              width: 0.53125,
              height: 0.458333
            },
            slides: slides
          };
          this.pptExport.exportPPT(pptConfig);
        }
      });
    }
    return false;
  }

  loadAggTable(timeKeyFilter) {
    const sourcesDatareq = {
      dimensions: ['source', 'accounting_key'], // derived_sales_type_name
      metrics: [
        'revenue',
        'impressions',
        'clicks',
        'ad_requests',
        'viewable_impressions',
        'measurable_impressions'
      ],
      derived_metrics: ['cpm', 'ctr', 'fill_rate', 'viewability'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'], // derived_sales_type_name
      orderBy: [],
      limit: '',
      offset: ''
    };

    const totalsRowreqSrc = {
      dimensions: ['source'],
      metrics: [
        'revenue',
        'impressions',
        'clicks',
        'ad_requests',
        'viewable_impressions',
        'measurable_impressions'
      ],
      derived_metrics: ['cpm', 'ctr', 'fill_rate', 'viewability'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [],
      limit: '',
      offset: ''
    };

    this.dataFetchServ.getTableData(timeKeyFilter).subscribe(data => {
      this.dataFetchServ.getTableData(sourcesDatareq).subscribe(data1 => {
        const totalsColData = data['data'];
        const srcWiseData = data1['data'];

        const srcs = Array.from(new Set(srcWiseData.map(s => s['source'])));
        this.addSources(srcs);
        this.jumpToSource['totalSources'] = [];
        srcs.forEach(e => {
          this.jumpToSource['totalSources'].push({
            label: e,
            value: e
          });
        });
        // this.jumpToSource['selected'] = [srcs[0]];

        const map = this.dataMapping(totalsColData, srcWiseData);
        // map.map(o => delete o['data']['time_key1']);
        map.forEach(el => {
          el['data']['time_key1'] = 'All';
        });
        this.aggTableData = map;

        this.dataFetchServ.getTableData(totalsRowreqSrc).subscribe(data1 => {
          const srcWiseData = data1['data'];

          srcWiseData.forEach(e => {
            const src = String(e['source'])
              .toLowerCase()
              .replace(/ /g, '_');
            this.aggTableColumnDef.forEach(c => {
              if (src + '_revenue' === c['field']) {
                c['footerTotal'] = e['revenue'];
              } else if (src + '_cpm' === c['field']) {
                c['footerTotal'] = e['cpm'];
              } else if (src + '_ctr' === c['field']) {
                c['footerTotal'] = e['ctr'];
              } else if (src + '_impressions' === c['field']) {
                c['footerTotal'] = e['impressions'];
              } else if (src + '_clicks' === c['field']) {
                c['footerTotal'] = e['clicks'];
              } else if (src + '_fill_rate' === c['field']) {
                c['footerTotal'] = e['fill_rate'];
              } else if (src + '_viewability' === c['field']) {
                c['footerTotal'] = e['viewability'];
              }
            });
          });

          this.aggTableJson['frozenCols'][2]['footerTotal'] = this.cardsJson[0][
            'value'
          ];
          this.aggTableJson['frozenCols'][3]['footerTotal'] = this.cardsJson[2][
            'value'
          ];
          this.aggTableJson['frozenCols'][4]['footerTotal'] = this.cardsJson[3][
            'value'
          ];
          this.aggTableJson['totalRecords'] = map.length;
          this.aggTableJson['loading'] = false;
        });
      });
    });
  }

  dataMapping(totals: [], srcWise: []) {
    const arr = [];

    let tks = [];
    let timeKeyAcc = 'accounting_key';

    if (Array.from(new Set(totals.map(o => o['time_key'])))[0] === undefined) {
      tks = Array.from(new Set(totals.map(o => o['accounting_key'])));
    } else {
      tks = Array.from(new Set(totals.map(o => o['time_key'])));
      timeKeyAcc = 'time_key';
    }

    tks.forEach(t => {
      const obj = {
        data: {},
        children: [{ data: {} }]
      };
      totals.forEach(tot_obj => {
        if (tot_obj[timeKeyAcc] === t) {
          obj['data']['time_key'] = t;
          obj['data']['time_key1'] = t;
          obj['data']['source_revenue'] = tot_obj['revenue'];
          obj['data']['source_cpm'] = tot_obj['cpm'];
          obj['data']['source_impressions'] = tot_obj['impressions'];
        }
      });

      srcWise.forEach(s_obj => {
        if (s_obj[timeKeyAcc] === t) {
          const src = String(s_obj['source'])
            .toLowerCase()
            .replace(/ /g, '_');

          obj['data'][src + '_revenue'] = s_obj['revenue'];
          obj['data'][src + '_cpm'] = s_obj['cpm'];
          obj['data'][src + '_ctr'] = s_obj['ctr'];
          obj['data'][src + '_clicks'] = s_obj['clicks']
          obj['data'][src + '_impressions'] = s_obj['impressions'];
          obj['data'][src + '_fill_rate'] = s_obj['fill_rate'];
          obj['data'][src + '_viewability'] = s_obj['viewability'];
        }
      });
      arr.push(obj);
    });
    return <TreeNode[]>arr;
  }

  addSources(srcs) {
    this.aggTableColumnDef = this.libServ.deepCopy(
      this.aggTableColumnDef.slice(0, 5)
    );

    srcs.forEach((s: String) => {
      // console.log('source... ', s);
      const src = s.toLowerCase().replace(/ /g, '_');

      if (this.sourcesJson.hasOwnProperty(s.toString())) {
        this.sourcesJson[s.toString()]['columns'].forEach(element => {
          if (this.sourcesJson[s.toString()]['defaultView']) {
            switch (element) {
              case src + '_revenue':
                this.aggTableColumnDef.push({
                  field: src + '_revenue',
                  displayName:
                    this.sourcesJson[s.toString()]['displayName'] + ' Revenue',
                  width: this.sourcesJson[s.toString()]['width'] + 10,
                  format: '$',
                  exportConfig: {
                    format: 'currency',
                    styleinfo: {
                      thead: 'default',
                      tdata: 'white'
                    }
                  },
                  style: { color: '#007ad9' },
                  formatConfig: [],
                  options: {
                    editable: false,
                    colSearch: false,
                    colSort: true,
                    resizable: true,
                    movable: true
                  },
                  footerTotal: 0
                });
                break;

              case src + '_cpm':
                this.aggTableColumnDef.push({
                  field: src + '_cpm',
                  displayName:
                    this.sourcesJson[s.toString()]['displayName'] + ' eCPM',
                  width: this.sourcesJson[s.toString()]['width'] + 10,
                  format: '$',
                  exportConfig: {
                    format: 'currency',
                    styleinfo: {
                      thead: 'default',
                      tdata: 'white'
                    }
                  },
                  formatConfig: [],
                  options: {
                    editable: false,
                    colSearch: false,
                    colSort: true,
                    resizable: true,
                    movable: true
                  },
                  footerTotal: 0
                });
                break;

              case src + '_impressions':
                this.aggTableColumnDef.push({
                  field: src + '_impressions',
                  displayName:
                    this.sourcesJson[s.toString()]['displayName'] +
                    ' Paid Impressions',
                  width: this.sourcesJson[s.toString()]['width'] + 30,
                  format: 'number',
                  formatConfig: [],
                  exportConfig: {
                    format: 'number',
                    styleinfo: {
                      thead: 'default',
                      tdata: 'white'
                    }
                  },
                  options: {
                    editable: false,
                    colSearch: false,
                    colSort: true,
                    resizable: true,
                    movable: true
                  },
                  footerTotal: 0
                });
                break;

              case src + '_clicks':
                this.aggTableColumnDef.push({
                  field: src + '_clicks',
                  displayName:
                    this.sourcesJson[s.toString()]['displayName'] + ' Clicks',
                  width: this.sourcesJson[s.toString()]['width'] + 10,
                  format: 'number',
                  formatConfig: [],
                  exportConfig: {
                    format: 'number',
                    styleinfo: {
                      thead: 'default',
                      tdata: 'white'
                    }
                  },
                  options: {
                    editable: false,
                    colSearch: false,
                    colSort: true,
                    resizable: true,
                    movable: true
                  },
                  footerTotal: 0
                });
                break;
              case src + '_ctr':
                this.aggTableColumnDef.push({
                  field: src + '_ctr',
                  displayName:
                    this.sourcesJson[s.toString()]['displayName'] + ' CTR',
                  width: this.sourcesJson[s.toString()]['width'] + 10,
                  format: 'percentage',
                  formatConfig: [2],
                  exportConfig: {
                    format: 'percentage',
                    styleinfo: {
                      thead: 'default',
                      tdata: 'white'
                    }
                  },
                  options: {
                    editable: false,
                    colSearch: false,
                    colSort: true,
                    resizable: true,
                    movable: true
                  },
                  footerTotal: 0
                });
                break;

              case src + '_fill_rate':
                this.aggTableColumnDef.push({
                  field: src + '_fill_rate',
                  displayName:
                    this.sourcesJson[s.toString()]['displayName'] +
                    ' Fill Rate',
                  width: this.sourcesJson[s.toString()]['width'] + 20,
                  format: 'percentage',
                  formatConfig: [2],
                  exportConfig: {
                    format: 'percentage',
                    styleinfo: {
                      thead: 'default',
                      tdata: 'white'
                    }
                  },
                  options: {
                    editable: false,
                    colSearch: false,
                    colSort: true,
                    resizable: true,
                    movable: true
                  },
                  footerTotal: 0
                });
                break;

              case src + '_viewability':
                this.aggTableColumnDef.push({
                  field: src + '_viewability',
                  displayName:
                    this.sourcesJson[s.toString()]['displayName'] +
                    ' Viewability',
                  width: this.sourcesJson[s.toString()]['width'] + 10,
                  format: 'percentage',
                  formatConfig: [2],
                  exportConfig: {
                    format: 'percentage',
                    styleinfo: {
                      thead: 'default',
                      tdata: 'white'
                    }
                  },
                  options: {
                    editable: false,
                    colSearch: false,
                    colSort: true,
                    resizable: true,
                    movable: true
                  },
                  footerTotal: 0
                });
                break;
            }
          }
        });
      }
    });

    this.aggTableJson['columns'] = this.aggTableColumnDef.slice(5);
    this.aggTableJson['selectedColumns'] = this.aggTableColumnDef.slice(5);
  }

  tabChanged(e) {
    if (e.index === 1 && this.aggTableData === undefined) {
      this.aggTableJson['loading'] = true;
      this.loadAggTable(this.aggreTableReq);
    }
  }

  exportTable(table, fileFormat) {
    if (this.exportRequest['sendEmail'].length > 0) {
      if (table === 'agg') {
        if (this.aggTableData.length == 0) {
          this.confirmationService.confirm({
            message:
              'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
            header: 'Information',
            icon: 'pi pi-exclamation-triangle',
            accept: () => { },
            reject: () => { }
          });
        } else {
          this.toastService.displayToast({
            severity: 'info',
            summary: 'Export Report',
            detail:
              'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
            life: 10000
          });

          const columnDefs = this.aggTableColumnDef;

          const totalsColreq = {
            dimensions: ['accounting_key'],
            metrics: ['revenue', 'impressions'],
            derived_metrics: ['cpm'],
            timeKeyFilter: this.filtersApplied['timeKeyFilter'],
            filters: this.libServ.deepCopy(this.filtersApplied['filters']),
            groupByTimeKey: {
              key: ['accounting_key'],
              interval: 'daily'
            },
            gidGroupBy: [
              // 'ad_size',
              // 'ad_type',
              // 'ad_units',
              // 'bidding_type',
              // 'device_category',
              // 'geography',
              // 'sales_channel',
              // 'sales_type',
              // 'site',
              'source'
            ],
            orderBy: [],
            limit: '',
            offset: '',
            isTable: true
          };
          // For dev purpose
          // totalsColreq['gidGroupBy'] = [
          //   'ad_size',
          //   'ad_type',
          //   'ad_units',
          //   'bidding_type',
          //   'device_category',
          //   'geography',
          //   'sales_channel',
          //   'sales_type',
          //   'site',
          //   'source'
          // ];

          const sourcesDatareq = {
            dimensions: ['source', 'accounting_key'],
            metrics: [
              'revenue',
              'impressions',
              'clicks',
              'ad_requests',
              'viewable_impressions',
              'measurable_impressions'
            ],
            derived_metrics: ['cpm', 'ctr', 'fill_rate', 'viewability'],
            timeKeyFilter: this.filtersApplied['timeKeyFilter'],
            filters: this.libServ.deepCopy(this.filtersApplied['filters']),
            groupByTimeKey: {
              key: ['accounting_key'],
              interval: 'daily'
            },
            gidGroupBy: [
              // 'ad_size',
              // 'ad_type',
              // 'ad_units',
              // 'bidding_type',
              // 'device_category',
              // 'geography',
              // 'sales_channel',
              // 'sales_type',
              // 'site',
              'source'
            ],
            orderBy: [],
            limit: '',
            offset: '',
            isTable: true
          };
          // For dev purpose
          // sourcesDatareq['gidGroupBy'] = [
          //   'ad_size',
          //   'ad_type',
          //   'ad_units',
          //   'bidding_type',
          //   'device_category',
          //   'geography',
          //   'sales_channel',
          //   'sales_type',
          //   'site',
          //   'source'
          // ];

          const totalsDailyColreq = {
            dimensions: ['time_key'],
            metrics: [
              'revenue',
              'impressions',
              'clicks',
              'ad_requests',
              'viewable_impressions',
              'measurable_impressions'
            ],
            derived_metrics: ['cpm', 'ctr', 'fill_rate', 'viewability'],
            timeKeyFilter: this.filtersApplied['timeKeyFilter'],
            filters: this.libServ.deepCopy(this.filtersApplied['filters']),
            groupByTimeKey: {
              key: ['time_key', 'accounting_key'],
              interval: 'daily'
            },
            gidGroupBy: [
              // 'ad_size',
              // 'ad_type',
              // 'ad_units',
              // 'bidding_type',
              // 'device_category',
              // 'geography',
              // 'sales_channel',
              // 'sales_type',
              // 'site',
              'source'
            ],
            orderBy: [],
            limit: '',
            offset: '',
            isTable: true
          };

          // For dev purpose
          // totalsDailyColreq['gidGroupBy'] = [
          //   'ad_size',
          //   'ad_type',
          //   'ad_units',
          //   'bidding_type',
          //   'device_category',
          //   'geography',
          //   'sales_channel',
          //   'sales_type',
          //   'site',
          //   'source'
          // ];

          const sourcesDailyDatareq = {
            dimensions: ['source', 'time_key'],
            metrics: [
              'revenue',
              'impressions',
              'clicks',
              'ad_requests',
              'viewable_impressions',
              'measurable_impressions'
            ],
            derived_metrics: ['cpm', 'ctr', 'fill_rate', 'viewability'],
            timeKeyFilter: this.filtersApplied['timeKeyFilter'],
            filters: this.libServ.deepCopy(this.filtersApplied['filters']),
            groupByTimeKey: {
              key: ['time_key', 'accounting_key'],
              interval: 'daily'
            },
            gidGroupBy: [
              // 'ad_size',
              // 'ad_type',
              // 'ad_units',
              // 'bidding_type',
              // 'device_category',
              // 'geography',
              // 'sales_channel',
              // 'sales_type',
              // 'site',
              'source'
            ],
            orderBy: [],
            limit: '',
            offset: '',
            isTable: true
          };

          // For dev purpose
          // sourcesDailyDatareq['gidGroupBy'] = [
          //   'ad_size',
          //   'ad_type',
          //   'ad_units',
          //   'bidding_type',
          //   'device_category',
          //   'geography',
          //   'sales_channel',
          //   'sales_type',
          //   'site',
          //   'source'
          // ];

          this.dataFetchServ.getTableData(totalsColreq).subscribe(data => {
            this.dataFetchServ.getTableData(sourcesDatareq).subscribe(data1 => {
              const totalsColData = data['data'];
              const srcWiseData = data1['data'];

              let mapMonthly = this.dataMapping(totalsColData, srcWiseData);
              // mapMonthly.map(o => delete o['data']['time_key']);
              mapMonthly = mapMonthly.map(o => o['data']);

              // mapMonthly.forEach((o: object) => {
              //   const tk = o['time_key1'];
              //   delete o['time_key'];

              //   o['time_key'] = tk;
              // });
              this.dataFetchServ
                .getTableData(totalsDailyColreq)
                .subscribe(data => {
                  this.dataFetchServ
                    .getTableData(sourcesDailyDatareq)
                    .subscribe(data1 => {
                      const totalsDailyColData = data['data'];
                      const srcDailyWiseData = data1['data'];

                      let mapDaily = this.dataMapping(
                        totalsDailyColData,
                        srcDailyWiseData
                      );

                      // mapDaily.map(o => delete o['data']['time_key']);
                      mapDaily = mapDaily.map(o => o['data']);
                      // mapDaily.forEach((o: object) => {
                      //   const tk = o['time_key1'];
                      //   delete o['time_key1'];
                      //   o['time_key1'] = moment(tk, 'YYYYMMDD').format(
                      //     'MM/DD/YYYY'
                      //   );
                      // });

                      const totalObj = {};
                      columnDefs.forEach(ele => {
                        if ('footerTotal' in ele) {
                          totalObj[ele.field] = ele.footerTotal;
                        }
                      });
                      totalObj[columnDefs[0].field] = 'Total';
                      totalObj['source_revenue'] = this.cardsJson[0]['value'];
                      totalObj['source_cpm'] = this.cardsJson[2]['value'];
                      totalObj['source_impressions'] = this.cardsJson[3][
                        'value'
                      ];
                      mapMonthly.push(totalObj);
                      mapDaily.push(totalObj);
                      const sheetDetailsarray = [];
                      const sheetDetails = {};
                      sheetDetails['columnDef'] = this.libServ
                        .deepCopy(columnDefs)
                        .splice(0, 1)
                        .concat(columnDefs.slice(2));
                      sheetDetails['data'] = mapMonthly;
                      sheetDetails['sheetName'] = 'Monthly Data Distribution';
                      sheetDetails['isRequest'] = false;
                      sheetDetails['request'] = {
                        url: '',
                        method: '',
                        param: {
                          timeKeyFilter: this.libServ.deepCopy(
                            this.filtersApplied['timeKeyFilter']
                          )
                        }
                      };
                      sheetDetails['disclaimer'] = [
                        {
                          position: 'bottom',
                          label: 'Note: Data present in the table may vary over a period of time.',
                          color: '#000000'
                        }
                      ];
                      sheetDetails['totalFooter'] = {
                        available: true,
                        custom: false
                      };
                      sheetDetails['tableTitle'] = {
                        available: false,
                        label: 'Monthly Data Distribution'
                      };
                      sheetDetails['image'] = [
                        {
                          available: true,
                          path: environment.exportConfig.exportLogo,
                          position: 'top'
                        }
                      ];

                      const sheetlineDetails = {};
                      sheetlineDetails['columnDef'] = this.libServ
                        .deepCopy(columnDefs)
                        .slice(1);
                      sheetlineDetails['data'] = mapDaily;
                      sheetlineDetails['sheetName'] = 'Daily Data Distribution';
                      sheetlineDetails['isRequest'] = false;
                      sheetlineDetails['request'] = {
                        url: '',
                        method: '',
                        param: {}
                      };
                      sheetlineDetails['disclaimer'] = [
                        {
                          position: 'bottom',
                          label: 'Note: Data present in the table may vary over a period of time.',
                          color: '#000000'
                        }
                      ];
                      sheetlineDetails['totalFooter'] = {
                        available: true,
                        custom: false
                      };
                      sheetlineDetails['tableTitle'] = {
                        available: false,
                        label: 'Daily Data Distribution'
                      };
                      sheetlineDetails['image'] = [
                        {
                          available: true,
                          path: environment.exportConfig.exportLogo,
                          position: 'top'
                        }
                      ];

                      sheetDetailsarray.push(sheetDetails, sheetlineDetails);
                      this.exportRequest['sheetDetails'] = <SheetDetails[]>(
                        sheetDetailsarray
                      );
                      this.exportRequest['fileName'] =
                        'Programmatic Revenue Data ' +
                        moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
                      this.exportRequest['exportFormat'] = fileFormat;
                      this.exportRequest['exportConfig'] = environment.exportConfig;
                      // console.log('exportreport', this.exportRequest);
                      // return false;
                      this.dataFetchServ
                        .getExportReportData(this.exportRequest)
                        .subscribe(response => {
                          console.log(response);
                        });
                    });
                });
            });
          });
        }
      } else if (table === 'flat') {
        if (this.flatTableData.length == 0) {
          this.confirmationService.confirm({
            message:
              'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
            header: 'Information',
            icon: 'pi pi-exclamation-triangle',
            accept: () => { },
            reject: () => { }
          });
        } else {
          this.toastService.displayToast({
            severity: 'info',
            summary: 'Export Report',
            detail:
              'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
            life: 10000
          });
          const columnDefs = this.flatTableColumnDef;
          const req = this.libServ.deepCopy(this.flatTableReq);
          req['limit'] = '';
          req['offset'] = '';
          req['isTable'] = false;

          const sheetDetails = {};
          sheetDetails['columnDef'] = columnDefs;
          sheetDetails['data'] = [];
          sheetDetails['sheetName'] = 'Programmatic Daily Data';
          sheetDetails['isRequest'] = true;
          sheetDetails['request'] = {
            url: '/api/flask/payout_asbn/cassandradata',
            method: 'POST',
            param: req
          };
          sheetDetails['disclaimer'] = [
            {
              position: 'bottom',
              label: 'Note: Data present in the table may vary over a period of time.',
              color: '#000000'
            }
          ];
          sheetDetails['totalFooter'] = {
            available: true,
            custom: true,
            notIncludeColumn: ['fill_rate']
          };
          sheetDetails['tableTitle'] = {
            available: false,
            label: 'Programmatic Daily Data'
          };
          sheetDetails['image'] = [
            {
              available: true,
              path: environment.exportConfig.exportLogo,
              position: 'top'
            }
          ];
          const sheetDetailsarray = [];
          sheetDetailsarray.push(sheetDetails);
          this.exportRequest['sheetDetails'] = <SheetDetails[]>(
            sheetDetailsarray
          );
          this.exportRequest['fileName'] =
            'Programmatic Daily Trends ' +
            moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
          this.exportRequest['exportFormat'] = fileFormat;
          this.exportRequest['exportConfig'] = environment.exportConfig;
          // console.log('exportreport', this.exportRequest);
          // return false;
          this.dataFetchServ
            .getExportReportData(this.exportRequest)
            .subscribe(response => {
              console.log(response);
            });
        }
      }
    } else {
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Export Report',
        detail:
          'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
        life: 10000
      });
    }
    return false;
  }

  colShowHide() {
    if (
      this.aggTableJson['columns'].length !==
      this.aggTableJson['selectedColumns'].length
    ) {
      this.showJumpTo = false;
    } else {
      this.showJumpTo = true;
    }
  }

  jumpTo(table) {
    const body = table.containerViewChild.nativeElement.getElementsByClassName(
      'ui-treetable-scrollable-body'
    )[1];
    const scrollWidth = this.aggTableJson['columns']
      .slice(
        0,
        this.aggTableJson['columns'].findIndex(e =>
          e.displayName.startsWith(this.jumpToSource['selected'])
        )
      )
      .reduce((tot: any, v: any) => tot + parseInt(v.width, 10), 0);

    body.scrollLeft = scrollWidth;
  }

  customSearch(value, fieldName, inputValue, tableColDef) {
    tableColDef[
      tableColDef.findIndex(x => x.field == fieldName)
    ].condition = this.conditionNames[
      this.conditionNames.findIndex(x => x.value == value)
    ].label;

    if (value != '') {
      this.onSearchChanged(tableColDef);
    }
  }

  onGlobalSearchChanged(searchValue, tableColDef) {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    this.timeout = setTimeout(() => {
      this.flatTableReq['filters']['globalSearch'] = {
        dimensions: [],
        value: searchValue
      };
      tableColDef.forEach(element => {
        this.flatTableReq['filters']['globalSearch']['dimensions'].push(
          element.field
        );
      });
      this.loadFlatTableData(this.flatTableReq);
    }, 3000);
  }

  onSearchChanged(tableColDef) {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    this.timeout = setTimeout(() => {
      this.flatTableReq['filters']['dimensions'].forEach(element => {
        if (element.key.split('<<').length > 0) {
          this.flatTableReq['filters']['dimensions'].splice(
            this.flatTableReq['filters']['dimensions'].findIndex(
              x => x.key === element.key
            ),
            1
          );
        }
      });

      this.flatTableReq['filters']['metrics'] = [];
      tableColDef.forEach(element => {
        if (element.value !== '' && element.value != null) {
          if (element.condition === 'Equal') {
            this.flatTableReq['filters'][element.columnType].push({
              key: element.field,
              values: [parseFloat(element.value)]
            });
          } else {
            this.flatTableReq['filters'][element.columnType].push({
              key: `${element.field}<<${element.condition}`,
              values:
                element.columnType === 'metrics'
                  ? [parseFloat(element.value)]
                  : [element.value.toString()]
            });
          }
        }
      });
      this.loadFlatTableData(this.flatTableReq);
    }, 3000);
  }

  resetSearch(fieldName, tableColDef) {
    console.log('reset', fieldName, tableColDef);
    tableColDef.find(o => o.field === fieldName).value = '';
    this.onSearchChanged(tableColDef);
  }

  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }

  sendEmail() {
    const imageConfig = {
      imgName: `${this.appConfig['name']}.png`,
      type: 'image/png'
    };

    this.htmltoimage
      .canvasImage(document.getElementById('capture'), imageConfig)
      .then(canvas => {
        this.dialogService.open(SendEmailComponent, {
          header: 'Email Screenshot',
          contentStyle: {
            'max-height': '80vh',
            width: '30vw',
            overflow: 'auto'
          },
          data: canvas.toDataURL('image/png')
        });
      });
  }
}
