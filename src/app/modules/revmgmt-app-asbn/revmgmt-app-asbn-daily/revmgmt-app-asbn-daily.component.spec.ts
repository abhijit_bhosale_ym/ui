import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RevmgmtAppAsbnDailyComponent } from './revmgmt-app-asbn-daily.component';

describe('RevmgmtAppAsbnDailyComponent', () => {
  let component: RevmgmtAppAsbnDailyComponent;
  let fixture: ComponentFixture<RevmgmtAppAsbnDailyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RevmgmtAppAsbnDailyComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RevmgmtAppAsbnDailyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
