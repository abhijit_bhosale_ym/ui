import { Routes } from '@angular/router';

import { RevMgmtAppAsbnComponent } from './revmgmt-app-asbn.component';
export const RevMgmtAppAsbnRoutes: Routes = [
  {
    path: '',
    component: RevMgmtAppAsbnComponent
    // data: {
    //   breadcrumb: 'Yield Management'
    // }
  }
];
