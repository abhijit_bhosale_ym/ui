import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Audience360DailyDataPopupComponent } from './audience360-daily-data-popup.component';

describe('Audience360DailyDataPopupComponent', () => {
  let component: Audience360DailyDataPopupComponent;
  let fixture: ComponentFixture<Audience360DailyDataPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Audience360DailyDataPopupComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Audience360DailyDataPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
