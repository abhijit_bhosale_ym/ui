import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FetchApiDataService {
  private BASE_URL: string = environment.baseUrl;

  constructor(private http: HttpClient) {}

  // getTableData(params: object) {
  //   const url = `${this.BASE_URL}/prebid-data/mainTable`;
  //   if (!('isTable' in params)) {
  //     params = Object.assign(params, { isTable: true });
  //   }
  //   return this.http.post(url, params);
  // }

  // getExportReportData(params: object) {
  //   const url = `${this.BASE_URL}/frankly/v1/common/exportData`;
  //   if (!('isTable' in params)) {
  //     params = Object.assign(params, { isTable: true });
  //   }
  //   return this.http.post(url, params);
  //   // return <TreeNode[]> json.data;
  // }

  // getRevMgmtData(params: object) {
  //   const url = `${this.BASE_URL}/dmp/v1/adops360/getData`;
  //   if (!('isTable' in params)) {
  //     params = Object.assign(params, { isTable: true });
  //   }
  //   return this.http.post(url, params);
  // }

  // getLastUpdatedData(appId) {
  //   const url = `${this.BASE_URL}/frankly/v1/common/getLastUpdatedData/${appId}`;
  //   return this.http.get(url, { responseType: 'text' });
  // }

  getMetaData() {
    const url = `${this.BASE_URL}/dmp/v1/audience-segment-360/getMetaData`;
    return this.http.get(url);
  }

  createSegment(params: {}) {
    const url = `${this.BASE_URL}/dmp/v1/audience-segment-360/createSegment`;
    return this.http.post(url, params);
  }

  updateSegment(params: {}) {
    const url = `${this.BASE_URL}/dmp/v1/audience-segment-360/updateSegment`;
    return this.http.post(url, params);
  }

  getSuggestionsForField(fieldName: string) {
    const url = `${this.BASE_URL}/dmp/v1/audience-segment-360/getSuggestionsForField?fieldName=${fieldName}`;
    return this.http.get(url).toPromise();
  }

  getExportReportData(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/common/exportData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
    // return <TreeNode[]> json.data;
  }

  getCardsData(params: object) {
    const url = `${this.BASE_URL}/dmp/v1/audience-segment-360/getCardsData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
  }

  getTableData(params: object) {
    const url = `${this.BASE_URL}/dmp/v1/audience-segment-360/getTableData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
  }

  getRevMgmtData(params: object) {
    const url = `${this.BASE_URL}/dmp/v1/audience-segment-360/getTableData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
  }

  getLastUpdatedData(appId) {
    const url = `${this.BASE_URL}/frankly/v1/common/getLastUpdatedData/${appId}`;
    return this.http.get(url, { responseType: 'text' });
  }

}
