import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DSPDetailsDataPopupComponent } from './dsp-details-data-popup.component';

describe('DSPDetailsDataPopupComponent', () => {
  let component: DSPDetailsDataPopupComponent;
  let fixture: ComponentFixture<DSPDetailsDataPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DSPDetailsDataPopupComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DSPDetailsDataPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
