import { Component, OnInit } from '@angular/core';
import { TreeNode, SelectItem } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { DialogService } from 'primeng/dynamicdialog';
import { CreateSegmentComponent } from '../create-segment/create-segment.component';

import { ToastService } from 'src/app/_services/toast-notification/toast.service';

@Component({
  selector: 'ym-attribute-daily-data-popup',
  templateUrl: './attribute-daily-data-popup.component.html',
  styleUrls: ['./attribute-daily-data-popup.component.scss'],
  providers:[DialogService]
})
export class AttributeDailyDataPopupComponent implements OnInit {
  heading = '';
  row: any;
  qbConfig;
  segmentJson;
  public lastSyncTime:string;
  public dspNameOptions: SelectItem[];
  public selectedDSPs: SelectItem[];

  constructor(
    private config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private toastService: ToastService,
    private dialogService: DialogService,

  ) {
     this.heading = this.config.data['heading'];
     this.row = this.config.data['row'];
     this.segmentJson = JSON.parse(this.row.segment_expression).expressionJson;
     this.qbConfig = this.config.data['qbConfig']
  }

  ngOnInit() {
    const lastSyncDate = new Date();
    lastSyncDate.setDate(lastSyncDate.getDate() -2);
    this.lastSyncTime = lastSyncDate.toLocaleString();
    this.dspNameOptions = [
      {label: 'Google AdManager', value: 'Google AdManager'},
      {value: 'MediaMath', label: 'MediaMath'},
      {value: 'DoubleClick', label: 'DoubleClick'},
      {value: 'LiveRamp', label: 'LiveRamp'},
      {value: 'Choozle', label: 'Choozle'},
      {value: 'TubeMogul', label: 'TubeMogul'},
      {value: 'BrightRoll', label: 'BrightRoll'},
      {value: 'AppNexus', label: 'AppNexus'}
    ];
  }

  editSegment() {



    //this.ref.close(this.row);
    const ref = this.dialogService.open(CreateSegmentComponent, {
      header: 'Edit Audience Segment Definition',
      contentStyle: { width: '60vw', overflow: 'auto' },
      data: {qbConfig : this.qbConfig, 'defination': this.row, isUpdated : true}
    });

  }

  tabChanged(event) {
    console.log(event);
  }

  activateSegment() {
    this.toastService.displayToast({
      severity: 'success',
      summary: 'Request Successful',
      detail: 'Audience Segment Sync Request Successful'
    });
  }

  copyMessage(){
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = this.row.segment_expression;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.toastService.displayToast({
      severity: 'success',
      summary: 'Copy to Clipboard',
      detail: 'Audience Segment Definition copied to clipboard successfully'
    });
  }

}
