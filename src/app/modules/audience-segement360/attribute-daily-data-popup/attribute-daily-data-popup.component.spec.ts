import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttributeDailyDataPopupComponent } from './attribute-daily-data-popup.component';

describe('Rev360DailyDataPopupComponent', () => {
  let component: AttributeDailyDataPopupComponent;
  let fixture: ComponentFixture<AttributeDailyDataPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AttributeDailyDataPopupComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttributeDailyDataPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
