import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AudienceSegement360Component } from './audience-segement360.component';

describe('AudienceSegement360Component', () => {
  let component: AudienceSegement360Component;
  let fixture: ComponentFixture<AudienceSegement360Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AudienceSegement360Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AudienceSegement360Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
