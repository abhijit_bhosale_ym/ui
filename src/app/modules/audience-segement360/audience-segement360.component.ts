import { Component, OnDestroy, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';
import { SortEvent, TreeNode } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { Subscription } from 'rxjs';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { CommonLibService } from 'src/app/_services';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { AttributeDailyDataPopupComponent } from './attribute-daily-data-popup/attribute-daily-data-popup.component';
import { DSPDetailsDataPopupComponent } from './dsp-details-data-popup/dsp-details-data-popup.component';
import { FetchApiDataService } from './fetch-api-data.service';
import { CreateSegmentComponent } from './create-segment/create-segment.component';

@Component({
  selector: 'ym-audience-segment360',
  templateUrl: './audience-segement360.component.html',
  styleUrls: ['./audience-segement360.component.scss']
})
export class AudienceSegment360Component implements OnInit, OnDestroy {
  private appConfigObs: Subscription;

  appConfig: object = {};
  lastUpdatedOn: Date;

  cardsJson = [];
  showCards = true;
  mainLineChartJson: object;
  mainPieChartJson: object;
  metricChartJson: object;

  showMetricChart = false;
  showMainLineChart = false;
  noDataMainLineChart = false;
  showMainPieChart = false;
  noDataMetricChart = false;

  aggTableData: TreeNode[];
  noTableData = false;
  dimColDef: any[];
  metricColDef: any[];
  aggTableColumnDef: any[];
  aggTableJson: Object;
  displayAggTable = true;

  filtersApplied: object = {};
  currentSort: object;
  showMainPieChartSource = false;
  noDataMainPieChart = false;
  nextUpdated: Date;
  dataUpdatedThrough: Date;
  expressions: any;
  qbConfig = {};
  timezone : String;

  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private htmltoimage: HtmltoimageService,
    private exportService: ExportdataService,
    private pptExport: ExportPptService,
    private formatNumPipe: FormatNumPipe
  ) { }

  ngOnInit() {
    this.currentSort = {};

    this.cardsJson = [
      {
        field: 'active_audience_segment',
        displayName: 'Active Audience Segments',
        value: 10,
        imageClass: 'fas fa-running',
        format: 'number',
        config: [],
        color: '#84d683'
      },
      {
        field: 'total_audience_segment',
        displayName: 'Total Audience Segments',
        value: 20,
        imageClass: 'fas fa-users',
        format: 'number',
        config: [],
        color: '#3BB9FF'
      },

      {
        field: 'audience_segment_user',
        displayName: 'Audience Segment Users',
        value: 504010,
        imageClass: 'fas fa-eye',
        format: 'number',
        config: [],
        color: '#F87431'
      },
      {
        field: 'active_dsps',
        displayName: 'Active DSPs',
        value: 5,
        imageClass: ' fas fa-handshake',
        format: 'number',
        config: [],
        color: '#bd6d88'
      },

      {
        field: 'cookie_pool',
        displayName: 'Cookie Pool',
        value: 610500,
        imageClass: 'fas fa-cookie-bite',
        format: 'number',
        config: [],
        color: '#b8a9c9'
      },
      {
        field: 'ecpm',
        displayName: 'eCPM',
        value: 2.75,
        imageClass: 'fas fa-dollar-sign',
        format: '$',
        config: [],
        color: '#b0c30c'
      },
    ];
    this.dimColDef = [
      {
        field: 'segment_name',
        displayName: 'Audience Segment Name',
        format: '',
        width: '350',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true
        },
        footerTotal: '-'
      }
    ];

    this.metricColDef = [
      {
        field: 'attributes',
        displayName: 'Attribute',
        format: '',
        width: '300',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true
        },
      },
      {
        field: 'is_active',
        displayName: 'Status',
        format: '',
        width: '100',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true
        }
      },
      {
        field: 'is_update_needed',
        displayName: 'Is Rolling?',
        format: '',
        width: '100',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true
        }
      },
      {
        field: 'audience_pool_count',
        displayName: 'Users',
        format: 'number',
        width: '100',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true
        },
      },
      {
        field: 'updated_at',
        displayName: 'Users Updated On',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true
        },
      },
      {
        field: 'created_at',
        displayName: 'Definition Updated On',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true
        },
      }
    ];

    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'expand',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,

      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: false,
      overallSearch: false,
      columnSearch: false
    };

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        let startDate = moment().subtract(7, 'days');
        let endDate = moment();
        this._titleService.setTitle(this.appConfig['displayName']);
        if (typeof this.appConfig['filter']['filterConfig']['filters']['datePeriod'] !== 'undefined') {
          if (
            this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
            'defaultDate'
            ][0]['startOf']
          ) {
            startDate = moment()
              .subtract(
                this.appConfig['filter']['filterConfig']['filters'][
                'datePeriod'
                ][0]['defaultDate'][0]['value'],
                this.appConfig['filter']['filterConfig']['filters'][
                'datePeriod'
                ][0]['defaultDate'][0]['period']
              )
              .startOf(
                this.appConfig['filter']['filterConfig']['filters'][
                'datePeriod'
                ][0]['defaultDate'][0]['period']
              );
          } else {
            startDate = moment().subtract(
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['value'],
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['period']
            );
          }
          endDate = moment().subtract(
            this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
            'defaultDate'
            ][1]['value'],
            this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
            'defaultDate'
            ][1]['period']
          );
        }
        this.filtersApplied = {
          filters: { dimensions: [], metrics: [] },
          timeKeyFilter: {
            time_key1: startDate.format('YYYYMMDD'),
            time_key2: endDate.format('YYYYMMDD')
          },
          groupby: this.appConfig['filter']['filterConfig']['groupBy'].filter(
            v => v.selected
          )
        };
        console.log("qbConfig", this.qbConfig);
        this.qbConfig = this.appConfig['filter']['qbConfig']

        this.lastUpdatedAt();
        this.initialLoading();
      }
    });
  }

  lastUpdatedAt() {
    this.dataFetchServ
      .getLastUpdatedData(this.appConfig['id'])
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        // this.lastUpdatedOn = moment(data).toDate();
        var zone_name = moment.tz.guess();
        this.timezone = moment.tz(zone_name).format('z');
        const lastUpdateObj = JSON.parse(data)[0];
        this.lastUpdatedOn = moment.utc(lastUpdateObj['updated_at']).tz(zone_name).toDate();
        this.nextUpdated = moment.utc(lastUpdateObj['next_run_at']).tz(zone_name).toDate();
        this.dataUpdatedThrough = moment.utc(lastUpdateObj['source_updated_through'], 'YYYYMMDD').tz(zone_name).toDate();
      });
  }

  initialLoading() {

    this.loadTableData();

    this.loadCards();

    this.loadMainLineChart();

    // const mainPieChartReq = {
    //   dimensions: ['audience_name'],
    //   metrics: ['spend'],
    //   derived_metrics: [],
    //   timeKeyFilter: this.filtersApplied['timeKeyFilter'],
    //   filters: this.libServ.deepCopy(this.filtersApplied['filters']),
    //   groupByTimeKey: {
    //     key: [],
    //     interval: 'daily'
    //   },
    //   gidGroupBy: ['audience_name'],
    //   orderBy: [{ key: 'dp_revenue', opcode: 'desc' }],
    //   limit: '',
    //   offset: ''
    // };
    // this.loadMainPieChart(mainPieChartReq);
    // this.loadSourceRevnueChart();
    // this.loadPieChart();
  }

  resetPagination(tt1) {
    if (typeof tt1 !== 'undefined') {
      tt1.reset();
    }
  }

  reset(table) {
    table.reset();
    this.loadTableData();
  }

  create() {
    const ref = this.dialogService.open(CreateSegmentComponent, {
      header: 'Create Audience Segment Definition',
      contentStyle: { width: '60vw', overflow: 'auto' },
      data: { qbConfig: this.qbConfig }
    });
    ref.onClose.subscribe((data1: string) => {
      this.initialLoading();
    });
  }

  // loadMainPieChart(params) {
  //   this.mainPieChartJson = {
  //     chartTypes: [{ key: 'pie', label: 'Pie Chart' }],
  //     chartData: {
  //       labels: [],
  //       datasets: [
  //         {
  //           data: []
  //         }
  //       ]
  //     },
  //     chartOptions: {
  //       title: {
  //         display: true,
  //         text: 'Audience Distribution Over Segments'
  //       },
  //       legend: {
  //         display: false
  //       },
  //       elements: {
  //         arc: {
  //           borderWidth: 0
  //         }
  //       },
  //       plugins: {
  //         datalabels: {
  //           formatter: (value, ctx) => {
  //             let sum = 0;
  //             const dataArr = ctx.chart.data.datasets[0].data;
  //             dataArr.map(data => {
  //               sum += data;
  //             });
  //             const percentage = (value * 100) / sum;
  //             if (percentage > 10) {
  //               return `${percentage.toFixed(2)} %`;
  //             } else {
  //               return '';
  //             }
  //           },
  //           color: 'black'
  //         }
  //       }
  //     },
  //     zoomLabel: false,
  //     chartWidth: '',
  //     chartHeight: '300px'
  //   };
  //   this.mainPieChartJson['chartOptions']['tooltips'] = {
  //     callbacks: {
  //       label: (tooltipItem, data) => {
  //         const currentValue =
  //           data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
  //         let sum = 0;
  //         const dataArr = data.datasets[0].data;
  //         dataArr.map(data => {
  //           sum += data;
  //         });
  //         const percentage = (currentValue * 100) / sum;
  //         return `${
  //           data.labels[tooltipItem.index]
  //           } : ${this.formatNumPipe.transform(
  //             currentValue,
  //             'number',
  //             []
  //           )} - ${percentage.toFixed(2)} %`;
  //       }
  //     }
  //   };
  //   this.showMainPieChart = false;
  //   this.dataFetchServ.getTableData(params).subscribe(data => {
  //     const chartData = data['data'];

  //     if (typeof chartData === 'undefined' || !chartData.length) {
  //       this.noDataMainPieChart = true;
  //       return;
  //     } else {
  //       const sources = Array.from(
  //         new Set(chartData.map(s => s['segment_name']))
  //       );
  //       const colors = this.libServ.dynamicColors(sources.length);
  //       this.mainPieChartJson['chartData']['labels'] = sources;
  //       this.mainPieChartJson['chartData']['datasets'][0]['data'] = Array.from(
  //         new Set(chartData.map(s => s['audience_pool_count']))
  //       );
  //       this.mainPieChartJson['chartData']['datasets'][0][
  //         'backgroundColor'
  //       ] = colors;
  //       this.showMainPieChart = true;
  //       this.noDataMainPieChart = false;
  //     }
  //   });

  // }

  loadMainLineChart() {

    const params = {
      dimensions: ['time_key'],
      metrics: ['spend', 'ecpm'],
      derived_metrics: [],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key'],
        interval: 'daily'
      },
      gidGroupBy: [],
      orderBy: [{ key: 'tbl_segments.audience_pool_count', opcode: 'desc' }],
      limit: '',  //   Max value of Int MySql
      offset: ''
    };
    let colors = this.libServ.dynamicColors(2);
    this.mainLineChartJson = {
      chartTypes: [{ key: 'bar', label: 'Line Chart', stacked: false }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'Audience Segment User',
            type: 'bar',
            yAxisID: 'y-axis-0',

            borderColor: colors[0],
            fill: false,
            backgroundColor: colors[0],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Users per Audience Segment'
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Audience Segments'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: 'Audience Segment User'
              },
              position: 'left',
              name: '1',
              ticks: {
                callback: (value) => {
                  return this.formatNumPipe.transform(value, 'number', []);
                }
              }
              //   scaleFontColor: 'rgba(151,137,200,0.8)'
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
            }
          }
        },
        pan: {
          enabled: true,
          mode: 'x'
        },
        zoom: {
          enabled: true,
          mode: 'x'
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };


    this.showMainLineChart = false;
    this.dataFetchServ.getTableData(params).subscribe(data => {
      const chartData = data as {};
      this.noDataMainLineChart = false;
      if (!chartData['data'].length) {
        this.noDataMainLineChart = true;
        return;
      }

      const datesArr = Array.from(
        new Set(chartData['data'].map(r => r['segment_name']))
      );

      colors = this.libServ.dynamicColors(datesArr.length);
      this.mainLineChartJson['chartData']['labels'] = datesArr;

      const ecpmArr = [];

      datesArr.forEach(time_key => {
        chartData['data'].forEach(r => {
          if (r['segment_name'] === time_key) {
            ecpmArr.push(r['audience_pool_count']);
          }
        });
      });
      setTimeout(() => {
        this.mainLineChartJson['chartData']['datasets'][0]['data'] = ecpmArr;
        this.showMainLineChart = true;
      }, 0);
    });
  }

  chartSelected() { }

  loadTableData() {
    this.aggTableJson['loading'] = true;

    const tableReq = {
      dimensions: [],
      metrics: ['impressions'],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: { key: [], interval: 'daily' },
      gidGroupBy: this.getGrpBys(),
      orderBy: [],
      limit: '',
      offset: ''
    };
    const finalColDef = this.libServ.deepCopy(this.metricColDef);

    finalColDef.splice(0, 0, ...this.dimColDef);
    this.aggTableColumnDef = finalColDef.slice();
    this.aggTableJson['columns'] = this.aggTableColumnDef.slice(
      this.dimColDef.length
    );
    this.aggTableJson['selectedColumns'] = this.aggTableColumnDef.slice(
      this.dimColDef.length
    );
    this.aggTableJson['frozenCols'] = [
      ...this.aggTableColumnDef.slice(0, this.dimColDef.length)
    ];
    this.aggTableJson['frozenWidth'] =
      this.aggTableColumnDef
        .slice(0, this.dimColDef.length)
        .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px';

    this.reloadAggTable();

    this.dataFetchServ.getTableData(tableReq).subscribe(data => {

      if (data['status'] === 0 || !data['data'].length) {
        this.noTableData = true;
        // this.toastService.displayToast({
        //   severity: 'error',
        //   summary: 'Server Error',
        //   detail: 'Please refresh the page'
        // });

        console.log(data['status_msg']);
        return;
      } else {
        this.noTableData = false;
      }

      const tableData = data['data'];

      const arr = [];
      tableData.forEach((row: object) => {
        let obj = {};
        if (row['is_active'] == 'YES') {
          row['is_active'] = 'Active';
        } else {
          row['is_active'] = 'Inactive';
        }
        row['attributes'] = row['attributes'].map(a => a['display_name']).join(', ');
        row['updated_at'] = new Date(row['updated_at']).toLocaleString();
        row['created_at'] = new Date(row['created_at']).toLocaleString();
        obj = {
          data: row
        };
        arr.push(obj);
      });

      this.aggTableData = <TreeNode[]>arr;
      this.aggTableJson['totalRecords'] = data['totalItems'];
      this.aggTableJson['loading'] = false;
    });
  }

  reloadAggTable() {
    this.displayAggTable = false;
    setTimeout(() => {
      this.displayAggTable = true;
    }, 0);
  }

  loadCards() {
    const cardsReq = {
      dimensions: [],
      metrics: [],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: [],
      orderBy: [],
      limit: '',
      offset: ''
    };
    this.showCards = false;
    this.dataFetchServ.getCardsData(cardsReq).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }
      if (!this.libServ.isEmptyObj(data['data'])) {
        this.cardsJson.map(o => {
          o['value'] = data['data'][0][o['field']];
          this.showCards = true;
        });
      } else {
        this.cardsJson.map(o => {
          o['value'] = 0;
          this.showCards = true;
        });
      }
    });
  }

  getFooters() {
    const tableReq1 = {
      dimensions: [],
      metrics: ['dp_revenue', 'dp_impressions', 'dp_clicks'],
      derived_metrics: ['cpm', 'cpc', 'ctr'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.filtersApplied['filters'],
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },

      gidGroupBy: this.getGrpBys(),  //   grpBys,
      orderBy: [],
      limit: '',
      offset: ''
    };

    if (
      tableReq1['gidGroupBy'].findIndex(item => item === 'accounting_key') >= 0
    ) {
      // if (!tableReq1['dimensions'].includes('accounting_key')) {
      //   tableReq1['dimensions'].push('accounting_key');
      // }

      tableReq1['gidGroupBy'].splice(
        tableReq1['gidGroupBy'].findIndex(item => item === 'accounting_key'),
        1
      );
    }

    if (tableReq1['gidGroupBy'].length === 0) {
      tableReq1['gidGroupBy'].push('source');
    }

    this.dataFetchServ.getRevMgmtData(tableReq1).subscribe(data11 => {
      if (data11['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data11['status_msg']);
        return;
      }

      const data1 = data11['data'][0];
      this.aggTableJson['selectedColumns'].forEach(c => {
        if (typeof data1 !== 'undefined') {
          c['footerTotal'] = data1[c['field']];
        } else {
          c['footerTotal'] = 0;
        }
      });
    });
  }

  onTableDrill(e: Event) {
    console.log('Drilled', e);
    if (!e['node']['childLoaded']) {
      this.aggTableJson['loading'] = true;
      const tableReq = {
        dimensions: [],
        metrics: ['impressions'],
        derived_metrics: [],
        timeKeyFilter: this.libServ.deepCopy(
          this.filtersApplied['timeKeyFilter']
        ),
        filters: this.libServ.deepCopy(this.filtersApplied['filters']),
        groupByTimeKey: {
          key: [],
          interval: 'daily'
        },
        gidGroupBy: this.getGrpBys(),
        orderBy: [],
        limit: '',
        offset: ''
      };
      const grpBys = this.filtersApplied['groupby'].map(e3 => e3.key);

      for (const g of grpBys) {
        if (e['node']['data'][g] !== 'All') {
          tableReq['dimensions'].push(g);
          if (!tableReq['gidGroupBy'].includes(g)) {
            tableReq['gidGroupBy'].push(g);
          }
          if (
            tableReq['filters']['dimensions'].findIndex(e2 => e2.key === g) === -1
          ) {
            tableReq['filters']['dimensions'].push({
              key: g,
              values: [e['node']['data'][g]]
            });
          } else {
            tableReq['filters']['dimensions'].splice(
              tableReq['filters']['dimensions'].findIndex(e1 => e1.key === g),
              1
            );
            tableReq['filters']['dimensions'].push({
              key: g,
              values: [e['node']['data'][g]]
            });
            //   if (
            //     tableReq['filters']['dimensions']
            //       .find(e => e.key === g)
            //       ['values'].findIndex(v => v === e['node']['data'][g]) === -1
            //   ) {
            //     tableReq['filters']['dimensions'].push(e['node']['data'][g]);
            //   }
          }
        } else {
          tableReq['dimensions'].push(g);
          if (!tableReq['gidGroupBy'].includes(g)) {
            tableReq['gidGroupBy'].push(g);
          }
          break;
        }
      }

      if (
        tableReq['gidGroupBy'].findIndex(item => item === 'accounting_key') >= 0
      ) {
        tableReq['gidGroupBy'].splice(
          tableReq['gidGroupBy'].findIndex(item => item === 'accounting_key'),
          1
        );
      }

      this.dataFetchServ.getRevMgmtData(tableReq).subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }

        const childData = data['data'];
        const arr = [];

        childData.forEach((row: object) => {

          if (row['audience_status'] == '1') {
            row['audience_status'] = 'Active';
          } else {
            row['audience_status'] = 'Inactive';
          }

          row['attribute'] = 'Details';

          this.aggTableJson['frozenCols'].forEach(r => {
            if (typeof row[r['field']] === 'undefined') {
              row[r['field']] = 'All';
            }
          });
          let obj = {};
          if (row[grpBys[grpBys.length - 1]] === 'All') {
            obj = {
              data: row,
              children: [{ data: {} }]
            };
          } else {
            obj = {
              data: row
            };
          }
          arr.push(obj);
        });
        this.aggTableJson['loading'] = false;
        e['node']['children'] = <TreeNode[]>arr;
        this.aggTableData = [...this.aggTableData];
        e['node']['childLoaded'] = true;
      });
    }
  }

  onLazyLoadAggTable(e: Event) {
    console.log('Lazy Agg', e);
  }

  openPopup(row, field) {
    const grpBys = [];
    const filtersApplied = this.libServ.deepCopy(this.filtersApplied);
    let str = '';

    const fieldIdx = filtersApplied['groupby'].findIndex(
      e => e['key'] === field
    );
    filtersApplied['groupby'].forEach((grp, i) => {
      if (i <= fieldIdx) {
        if (
          filtersApplied['filters']['dimensions'].findIndex(
            e => e.key === grp['key']
          ) !== -1
        ) {
          filtersApplied['filters']['dimensions'].splice(
            filtersApplied['filters']['dimensions'].findIndex(
              e => e.key === grp['key']
            ),
            1
          );
        }
        filtersApplied['filters']['dimensions'].push({
          key: grp['key'],
          values: [row[grp['key']]]
        });

        if (str === '') {
          str = str + row[grp['key']];
        } else {
          str = str + ' > ' + row[grp['key']];
        }
        filtersApplied['filters']['dimensions'].push({
          key: grp['key'],
          values: [row[grp['key']]]
        });
        // }

      }
    });
    const grpByDims = filtersApplied['groupby'].slice(
      0,
      filtersApplied['groupby'].findIndex(item => item.key === field) + 1
    );
    filtersApplied['gidGroupBy'] = this.getGrpBys();
    filtersApplied['groupby'] = grpBys;
    filtersApplied['filters']['dimensions'] = [{ 'key': 'segment_name', 'values': [row['segment_name']] }];
    filtersApplied['segment_name'] = [row['segment_name']];

    const data = {
      filters: filtersApplied,
      groupBy: this.libServ.deepCopy(this.filtersApplied['groupby']),
      grpbyColDef: grpByDims,
      'field': field,
      'row': row,
      'heading': str,
      qbConfig: this.qbConfig
    };

    console.log("...........field............", field);


    if (field === 'segment_name') {
      const ref = this.dialogService.open(AttributeDailyDataPopupComponent, {
        header: 'Audience Segment Details',
        contentStyle: { width: '80vw', overflow: 'auto', appendTo: 'body' },
        data: data
      });
      ref.onClose.subscribe((data: string) => { });

      //   ref.onClose.subscribe((row) => {
      //  this.initialLoading();
      //  ref.destroy();

      //   console.log(".........row....",row);
      //   if (typeof row !== 'undefined') {
      //     const ref = this.dialogService.open(CreateSegmentComponent, {
      //       header: 'Edit Audience Segment Defination',
      //       contentStyle: { width: '60vw', overflow: 'auto' },
      //       data: {qbConfig : this.qbConfig, 'defination': row}
      //     });
      //     ref.onClose.subscribe(() => {
      //     //  this.initialLoading();
      //     });
      //   }
      //});
    } else if (field === 'dsp_name') {
      const ref = this.dialogService.open(DSPDetailsDataPopupComponent, {
        header: str,
        contentStyle: { width: '80vw', overflow: 'auto' },
        data: data
      });
      ref.onClose.subscribe(() => { });
    }
  }

  exportTable(tableType, fileFormat) {
    const colDef = this.libServ.deepCopy(this.aggTableColumnDef);
    const tableReq = {
      dimensions: [],
      metrics: ['impressions'],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: { key: [], interval: 'daily' },
      gidGroupBy: this.getGrpBys(),
      orderBy: [],
      limit: '',
      offset: ''
    };

    tableReq['isTable'] = false;
    this.dataFetchServ.getTableData(tableReq).subscribe(success => {
      if (success['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(success['status_msg']);
        return;
      }
      const data = [];
      success['data'].forEach((row: object) => {
        if (row['is_active'] == 'YES') {
          row['is_active'] = 'Active';
        } else {
          row['is_active'] = 'Inactive';
        }
        row['attributes'] = row['attributes'].map(a => a['display_name']).join(', ');
        row['updated_at'] = new Date(row['updated_at']).toLocaleString();
        row['created_at'] = new Date(row['created_at']).toLocaleString();
      });

      data.push({
        data: [
          {
            data: success['data'],
            columnDefs: colDef
          }
        ],

        sheetname: 'Segments'
      });

      this.exportService.exportReport(
        data,
        'Segments' + moment().format('MM-DD-YYYY'),
        fileFormat,
        false
      );
    });
    return false;
  }

  customSort(event: SortEvent) {
    if (!this.libServ.isEqual(this.currentSort, event.multiSortMeta[0])) {
      this.currentSort = event.multiSortMeta[0];
      event.data.sort((data1, data2) => {
        const value1 = data1['data'][this.currentSort['field']];
        const value2 = data2['data'][this.currentSort['field']];
        let result = null;

        if (value1 == null && value2 != null) {
          result = -1;
        } else if (value1 != null && value2 == null) {
          result = 1;
        } else if (value1 == null && value2 == null) {
          result = 0;
        } else if (typeof value1 === 'string' && typeof value2 === 'string') {
          result = value1.localeCompare(value2);
        } else {
          result = value1 < value2 ? -1 : value1 > value2 ? 1 : 0;
        }
        return this.currentSort['order'] * result;
      });
    }
  }

  isHiddenColumn(col: Object) {
    return (
      this.aggTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }

  /* --------------------- On Filter Apply Button Clicked --------------------- */

  onFiltersApplied(filterData: object) {
    // console.log('filterData', filterData);
    this.filtersApplied['filters']['dimensions'] = [];

    //   tslint:disable-next-line: forin
    for (const k in filterData['filter']['dimensions']) {
      if (filterData['filter']['dimensions'][k].length !== 0) {
        this.filtersApplied['filters']['dimensions'].push({
          key: k,
          values: filterData['filter']['dimensions'][k]
        });
      }
    }

    // this.filtersApplied['groupby'] = filterData['groupby'];
    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];

    this.loadTableData();

    this.loadCards();

    this.loadMainLineChart();

    // const mainPieChartReq = {
    //   dimensions: ['audience_name'],
    //   metrics: ['spend'],
    //   derived_metrics: [],
    //   timeKeyFilter: this.filtersApplied['timeKeyFilter'],
    //   filters: this.libServ.deepCopy(this.filtersApplied['filters']),
    //   groupByTimeKey: {
    //     key: [],
    //     interval: 'daily'
    //   },
    //   gidGroupBy: ['audience_name'],
    //   orderBy: [{ key: 'spend', opcode: 'desc' }],
    //   limit: '',
    //   offset: ''
    // };

    // this.loadSourceRevnueChart();
    // this.loadMainPieChart(mainPieChartReq);
  }
  //  }
  loadSourceRevnueChart() {
    const req = {
      dimensions: ['source', 'time_key'],
      metrics: ['dp_revenue'],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys(),  //  ['source'],
      orderBy: [],
      // orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };
    let colors = this.libServ.dynamicColors(2);
    this.metricChartJson = {
      chartTypes: [{ key: 'bar', label: 'Stack Trend', stacked: true }],
      chartData: {
        labels: [],
        datasets: []
      },
      chartOptions: {
        title: {
          display: true,
          text: ''
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              display: true,
              scaleLabel: {
                display: true
              }
            }
          ]
        },
        pan: {
          enabled: true,
          mode: 'x'
        },
        zoom: {
          enabled: false,
          mode: 'x'
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };


    if (req['gidGroupBy'].filter(item => item === 'accounting_key').length >= 1) {
      req['gidGroupBy'].splice(req['gidGroupBy'].findIndex(item => item === 'accounting_key'), 1);
    }

    if (
      req['gidGroupBy'].length === 0 ||
      req['gidGroupBy'].findIndex(item => item === 'source') < 0
    ) {
      req['gidGroupBy'].push('source');
    }


    this.metricChartJson['chartOptions']['title']['text'] =
      'Sources By Revenue';
    this.metricChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
      callback: (value) => {
        return this.formatNumPipe.transform(value, '$', [0]);
      }
    };
    this.metricChartJson['chartOptions']['scales']['yAxes'][0]['scaleLabel'][
      'labelString'
    ] = 'Revenue ($)';
    this.metricChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return `${
            data.datasets[tooltipItem.datasetIndex].label
            } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
        }
      }
    };

    this.showMetricChart = false;
    this.dataFetchServ.getRevMgmtData(req).subscribe(data => {
      const chartData = data['data'];
      const dimData = Array.from(new Set(chartData.map(s => s['source'])));
      this.noDataMetricChart = false;
      if (!chartData.length) {
        this.noDataMetricChart = true;
        return;
      }

      const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));
      this.metricChartJson['chartData']['labels'] = datesArr.map(d =>
        moment(d, 'YYYYMMDD').format('YYYY-MM-DD')
      );
      colors = this.libServ.dynamicColors(dimData.length);

      setTimeout(() => {
        dimData.forEach((src, i) => {
          const revArr = [];
          datesArr.forEach(time_key => {
            chartData.forEach(r => {
              if (r['source'] === src && r['time_key'] === time_key) {
                revArr.push(r['dp_revenue']);
              }
            });
          });

          this.metricChartJson['chartData']['datasets'].push({
            label: src,
            data: revArr,
            borderColor: colors[i],
            fill: false,
            backgroundColor: colors[i]
          });
        });
        this.showMetricChart = true;
      }, 0);
    });
  }

  getGrpBys() {
    //   let grpBys = this.filtersApplied['groupby'].map(e => e.key);
    let grpBys = [];
    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          grpBys.concat(
            this.filtersApplied['filters']['dimensions']
              .filter(f => f.values.length)
              .map(m => m.key)
          )
        )
      );
    }
    return grpBys;
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['displayName']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    } else if (type === 'ppt') {
      const chartElements = document.getElementsByTagName('ym-charts');
      const slides = [];
      [].forEach.call(chartElements, (el, index) => {
        const c = el.getElementsByTagName('canvas')[0];

        slides.push({
          title: `${this.appConfig['name']}`,
          base64data: this.libServ.canvasToImage('white', c),
          width: 8,
          height: 3
        });

        if (index === chartElements.length - 1) {
          const pptConfig = {
            filename: 'Charts',
            logo: {
              visible: true,
              path: '/assets/images/ym.png',
              width: 0.53125,
              height: 0.458333
            },
            slides: slides
          };
          this.pptExport.exportPPT(pptConfig);
        }
      });
    }
    return false;
  }

  tabChanged(e) {
    switch (e.index) {
      case 0:
        break;
      case 1:
        this.loadSourceRevnueChart();
        break;
    }
  }

  ngOnDestroy(): void {
    this.appConfigObs.unsubscribe();
  }
}
