import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  private suggestions$: BehaviorSubject<any[]> = new BehaviorSubject([]);
  private metaData$: BehaviorSubject<any[]> = new BehaviorSubject([]);
  private getSuggestionsData$: BehaviorSubject<object> = new BehaviorSubject({fieldName: '', fieldValue: ''});

  public getSuggestionsDataObs = this.getSuggestionsData$.asObservable();
  constructor() { }

  getSuggestions(fieldName, fieldValue) {
    console.log(fieldName, fieldValue);
  }

  setMetaData(data) {
    this.metaData$.next(data);
  }

  setSuggestions(data) {
    this.suggestions$.next(data);
  }

  getMetaDataSubject() {
    return this.metaData$;
  }

  getSuggestionSubject() {
    return this.suggestions$;
  }

  getGetSuggestionsDataSubject() {
    return this.getSuggestionsData$;
  }
}
