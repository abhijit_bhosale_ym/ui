import {
  NgModule,
  CUSTOM_ELEMENTS_SCHEMA,
  NO_ERRORS_SCHEMA
} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder } from '@angular/forms';
//import { RuleSet, QueryBuilderConfig, Option } from '../query-builder';
import { FetchApiDataService } from '../../audience-segement360/fetch-api-data.service';
import { Segment } from './segment.interface';
import { ToastService, CommonLibService } from 'src/app/_services';
//import { getSQL, getTableNames, getWhereConditionFields } from '../../../_helpers/sql.helper'
//import { getSQL, getTableNames, getWhereConditionFields } from '../../../_helpers/sql.helpers.sql'
//import { SelectItem } from       'primeng/api';
import { DynamicDialogRef, DialogService } from 'primeng/dynamicdialog';
// import { DynamicDialogConfig } from './dynamicdialog-config';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
import { AppService } from './app.service';

import { BehaviorSubject } from 'rxjs';
// import                                                                                                                   { YqlAutocomplateComponent } from '../../../../../projects/yql-autocomplate/src/lib/yql-autocomplate.component'
import { YqlAutocomplateModule } from '../../../../../projects/yql-autocomplate/src/public-api'
import { query } from '@angular/animations';
let that = null;

@Component({
  selector: 'ym-create-segment',
  templateUrl: './create-segment.component.html',
  styleUrls: ['./create-segment.component.scss']
})
export class CreateSegmentComponent implements OnInit {

  public queryCtrl: FormControl;
  public enableQueryBuilder:boolean = false;
  public allowRuleset: boolean = true;
  public allowCollapse: boolean = false;
  public lastSyncTime:string;
  //public dspNameOptions: SelectItem[];
  //public selectedDSPs: SelectItem[];
  public row: object;
  public isGroupBy: boolean = false;
  public GroupDims: string[];
  public isUpdateOps: boolean = false;
  qbConfig : {};
  autoCompleteValues : {};
  fieldSet = [];
  query;
  suggestions$: BehaviorSubject<any[]>;
  metaData$: BehaviorSubject<any[]>;
  getSuggestionsData$: BehaviorSubject<object>;

  public segmentData: Segment = {
    name: '',
    description: '',
    query: ''
  };


  


  data = {
  };
   value: any[];


  // public query: RuleSet = {
  //   "condition": "and",
  //   "rules": []
  // };

  // public segmentData: Segment = {
  //   name: '',
  //   description: '',
  //   query: this.query
  // };
  
  // public config: QueryBuilderConfig;
  
  constructor(
    private formBuilder: FormBuilder,
    private dataFetchService: FetchApiDataService,
    private toastService: ToastService,
//    private dialogConfig: DynamicDialogConfig,
    private libServ: CommonLibService,
    public ref: DynamicDialogRef,
    private config: DynamicDialogConfig,
    private appService: AppService,
    private dialogService: DialogService,
  ) {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
    that = this;
    this.enableQueryBuilder = true;
   
    this.qbConfig = this.config.data['qbConfig']['yql'][0];
    this.appService.setMetaData(this.qbConfig['config']['fields']);

    this.getSuggestionsData$ = this.appService.getGetSuggestionsDataSubject();
    this.metaData$ = this.appService.getMetaDataSubject();
    this.suggestions$ = this.appService.getSuggestionSubject();

    if(this.config.data['isUpdated']){
      this.isUpdateOps = true;
      this.query = JSON.parse(this.config.data['defination']['segment_expression'])['expressionJson'];
      this.qbConfig['exp'] = this.query;
      this.segmentData.name = this.config.data['defination']['segment_name']
      this.segmentData.description = this.config.data['defination']['segment_description']
      console.log("segmentData",this.segmentData);
      
    }
     this.row = this.config.data['defination'];
     
     
  }

  
  // getOptions(field: any): any {
  //   that.GroupDims.push(field);
  //   if (that.config['fields'][field]['type'] == 'category') {
  //     return that.dataFetchService.getSuggestionsForField(field);
  //   } else {
  //     if (['number', 'boolean'].includes(that.config['fields'][field]['type'])) {
  //       that.isGroupBy = true;
  //     }
  //     return new Promise((resolve, reject) => {
  //       resolve([]);
  //     });
  //   }
    
  // }

  ngOnInit() {

    console.log("....config.....",this.config);
    
    console.log(".........row.......",this.row);

    console.log("......query..........",this.query);
    

    this.queryCtrl = this.formBuilder.control(this.segmentData);

    console.log("......qbCongig....popup",this.config.data['qbConfig']['yql'][0]);

    const lastSyncDate = new Date();
    lastSyncDate.setDate(lastSyncDate.getDate() -2);
    this.lastSyncTime = lastSyncDate.toLocaleString();
    // this.dspNameOptions = [
    //   {label: 'Google AdManager', value: 'Google AdManager'},
    //   {value: 'MediaMath', label: 'MediaMath'},
    //   {value: 'DoubleClick', label: 'DoubleClick'},
    //   {value: 'LiveRamp', label: 'LiveRamp'},
    //   {value: 'Choozle', label: 'Choozle'},
    //   {value: 'TubeMogul', label: 'TubeMogul'},
    //   {value: 'BrightRoll', label: 'BrightRoll'},
    //   {value: 'AppNexus', label: 'AppNexus'}
    // ];
  }


  whereClause(){
    let fields = this.qbConfig['config']['fields']['visibleFieldNames'] as [];
    let expArr = (this.query['expressionJson']).split(" ");
    let tempExpArr = [];
    
    console.log("expArr",expArr);
    console.log(expArr);
    console.log("fields",fields);
    
    for(let i = 0; i < expArr.length; i++)
  {
      if(fields.find(item => item['displayName'] == expArr[i])){
        let fieldValue = fields.find(item => item['displayName'] == expArr[i])['value'];
        tempExpArr.push(fieldValue);
        this.fieldSet.push(fieldValue);
        
    }
      else
        tempExpArr.push(expArr[i]);

  }

  console.log("tempExpArr",tempExpArr);
  console.log("tempExpArr",tempExpArr.join(" "));
  let whereClause = tempExpArr.join(" ").replace("[","("); 
  whereClause = whereClause.replace("]",")");

  return whereClause;
  }

  getTableNames(fieldsSet){
    let tableNameSet = new Set<string>();
    fieldsSet.forEach(field => {
        let tableName = field.split('.')[0];
        console.log(tableName);
        tableNameSet.add(tableName);
    });
    console.log(tableNameSet);
    return tableNameSet;

  }
  onSave() {
    

    let data = {};
    data['segment_name'] = this.segmentData.name;
    data['segment_description'] = this.segmentData.description;
    data['segment_expression'] = JSON.stringify(this.query);
    data['where_clause'] = this.whereClause();
    const tableList = Array.from(this.getTableNames(this.fieldSet));
    
    console.log("data['where_clause']",data['where_clause']);
    console.log("tableList",tableList);
    
    
    
    let tableName = ' ';
    if (tableList.length === 3) {
      tableName = `tbl_event_details 
      JOIN tbl_session_details ON tbl_event_details.unique_session_id = tbl_session_details.unique_session_id 
      JOIN tbl_user_details ON tbl_session_details.unique_user_id = tbl_user_details.unique_user_id`;
    } else if (tableList.length === 2) {
      if (tableList.includes('tbl_user_details') && tableList.includes('tbl_session_details')) {
        tableName = `tbl_session_details JOIN tbl_user_details ON tbl_session_details.unique_user_id = tbl_user_details.unique_user_id`;
      } else if (tableList.includes('tbl_user_details') && tableList.includes('tbl_event_details')) {
        tableName = `tbl_event_details JOIN tbl_user_details ON tbl_event_details.unique_user_id = tbl_user_details.unique_user_id`;
      } else if (tableList.includes('tbl_session_details') && tableList.includes('tbl_event_details')) {
        tableName = `tbl_session_details JOIN tbl_event_details ON tbl_event_details.unique_user_id = tbl_session_details.unique_user_id`;
      }
    } else if (tableList.length === 1) {
      tableName = tableList.pop();
    }

    console.log("tableName", tableName);
    
    data['table_name'] = tableName;
    data['select_clause'] = `DISTINCT ${tableName.split(' ')[0]}.unique_user_id`;
    data['having_clause'] = null;
    data['group_by_clause'] = null;
    data['order_by_clause'] = null;
    data['limit_clause'] = null;
    data['table_type'] = 'COMPLETE';
    data['is_update_needed'] = 'YES';
    data['is_active'] = 'YES';
    data['field_set'] = this.fieldSet;
    console.log("data",data);
    
    if (this.isUpdateOps) {
      data['id'] = this.row['id'];
      this.dataFetchService.updateSegment(data).subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        this.toastService.displayToast({
          severity: 'success',
          summary: 'Segment Updated',
          detail: 'Segment Updated successfully'
        });
        this.segmentData = {
          name: '',
          description: '',
          query: ''
        };
        this.GroupDims = [];
        this.isGroupBy = false;
        this.ref.close();
      });
    } else {
      this.dataFetchService.createSegment(data).subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        this.toastService.displayToast({
          severity: 'success',
          summary: 'Segment Created',
          detail: 'Segment stored successfully'
        });
        this.segmentData = {
          name: '',
          description: '',
          query: ''
        };
        this.GroupDims = [];
        this.isGroupBy = false;
        this.ref.close();
      });
    }
  }

  tabChanged(event) {
    console.log(event);
  }

  activateSegment() {
    this.toastService.displayToast({
      severity: 'success',
      summary: 'Request Successful',
      detail: 'Audience Segment Sync Request Successful'
    });
  }

  getAutocompleteValues(field: string) {
    this.value = this.autoCompleteValues[field];
  }

  expressionOutCome(expOut) {
    console.log("............", expOut);
    this.query = expOut;
    console.log("");
 }

 goBack() {
  this.dialogService.dialogComponentRef.destroy();
}
}