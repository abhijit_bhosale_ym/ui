export interface Segment {
    name: string;
    description: string;
    query: string;
}