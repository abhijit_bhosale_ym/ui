import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectButtonModule } from 'primeng/selectbutton';
import { ButtonModule } from 'primeng/button';
import { TabViewModule } from 'primeng/tabview';
import { TreeTableModule } from 'primeng/treetable';
import { InputSwitchModule } from 'primeng/inputswitch';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MultiSelectModule } from 'primeng/multiselect';

import { SharedModule } from 'src/app/_pipes/shared.module';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { FilterContainerModule } from '../common/filter-container/filter-container.module';
import { CardsModule } from '../common/cards/cards.module';
import { ChartsModule } from '../common/charts/charts.module';
import { RouterModule } from '@angular/router';
import { AudienceSegement360AppRoutes } from './audience-segement360-routing.module';
import { AudienceSegment360Component } from './audience-segement360.component';
// import { YqlAutocomplateComponent } from '../../../../projects/yql-autocomplate/src/lib/yql-autocomplate.component'


@NgModule({
  declarations: [AudienceSegment360Component],
  imports: [
    CommonModule,
    SelectButtonModule,
    ButtonModule,
    TabViewModule,
    TreeTableModule,
    InputSwitchModule,
    FormsModule,
    ReactiveFormsModule,
    MultiSelectModule,
    SharedModule,
    FilterContainerModule,
    CardsModule,
    ChartsModule,
    // YqlAutocomplateComponent,
    RouterModule.forChild(AudienceSegement360AppRoutes)
  ],
  providers: [FormatNumPipe],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AudienceSegement360AppModule {}
