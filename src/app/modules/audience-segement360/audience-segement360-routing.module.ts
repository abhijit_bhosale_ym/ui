import { Routes } from '@angular/router';
import { AudienceSegment360Component } from './audience-segement360.component';

export const AudienceSegement360AppRoutes: Routes = [
  {
    path: '',
    component: AudienceSegment360Component
  }
];
