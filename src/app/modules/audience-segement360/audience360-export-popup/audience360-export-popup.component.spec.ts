import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Audience360ExportPopupComponent } from './audience360-export-popup.component';

describe('Audience360ExportPopupComponent', () => {
  let component: Audience360ExportPopupComponent;
  let fixture: ComponentFixture<Audience360ExportPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Audience360ExportPopupComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Audience360ExportPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
