import { Routes } from '@angular/router';

import { RevenueCompareAppComponent } from './revenue-compare-app.component';
export const RevenueCompareRoutes: Routes = [
  {
    path: '',
    component: RevenueCompareAppComponent
    // data: {
    //   breadcrumb: 'Yield Management'
    // }
  }
];
