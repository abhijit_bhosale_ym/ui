import {
  Component,
  OnInit,
  QueryList,
  ElementRef,
  ViewChildren,
  OnDestroy
} from '@angular/core';
import { CommonLibService } from '../../../../src/app/_services';
import { FetchApiDataService } from './fetch-api-data.service';
import { TreeNode } from 'primeng/api';
import { ToastService } from '../../_services/toast-notification/toast.service';
import { DialogService } from 'primeng/dynamicdialog';
import { AppConfigService } from '../../../../src/app/_services/app-config/app-config.service';
import { HtmltoimageService } from '../../../../src/app/_services/screencapture/htmltoimage.service';
import { ExportPptService } from '../../../../src/app/_services/export/export-ppt.service';
import { ExportdataService } from '../../../../src/app/_services/export/exportdata.service';
import * as moment from 'moment';
import { FormatNumPipe } from '../../../../src/app/_pipes/number-format.pipe';
import { Title } from '@angular/platform-browser';
import { Subscription } from 'rxjs';

import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
@AutoUnsubscribe()
@Component({
  selector: 'ym-revenue-compare-app',
  templateUrl: './revenue-compare-app.component.html',
  styleUrls: ['./revenue-compare-app.component.css']
})
export class RevenueCompareAppComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription = null;
  appConfig: object = {};

  orderByDefault = 1;

  noTableData = false;

  flatTableData: TreeNode[];
  flatTableColumnDef: any[];
  flatTableJson: Object;
  tblResData: any[];
  tableData: any[];
  time_key1: any;
  time_key2: any;
  sourceName: any;


  @ViewChildren('ttRev360') aggTableRef: QueryList<ElementRef>;
  @ViewChildren('ttFlat') flatTableRef: QueryList<ElementRef>;


  lastUpdatedOn: Date;
  /* ---------------------------------- Table --------------------------------- */

  flatTableReq: object;
  selectedChartName;
  filtersApplied: object = {};
  isExportReport = false;
  /* ---------------------------------- Table --------------------------------- */

  constructor(
    private appConfigService: AppConfigService,
    private _titleService: Title,
    private libServ: CommonLibService,
    private dataFetchServ: FetchApiDataService,
    private toastService: ToastService,
    private dialogService: DialogService,
    private htmltoimage: HtmltoimageService,
    private pptExport: ExportPptService,
    private exportService: ExportdataService,
    private formatNumPipe: FormatNumPipe
  ) { }

  ngOnInit() {

    this.flatTableColumnDef = [
      {
        field: 'website_tk1',
        displayName: 'Website',
        format: '',
        width: '270',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'country_tk1',
        displayName: 'Country',
        format: '',
        width: '130',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'dp_impressions_tk1',
        displayName: 'Imps',
        format: 'number',
        width: '70',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'dp_clicks_tk1',
        displayName: 'Clicks',
        format: 'number',
        width: '70',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'dp_revenue_tk1',
        displayName: 'Revenue',
        format: '$',
        width: '70',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'ctr_tk1',
        displayName: 'CTR',
        format: 'percentage',
        width: '50',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },


      {
        field: 'cpc_tk1',
        displayName: 'CPC',
        format: '$',
        width: '50',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },


      {
        field: 'cpm_tk1',
        displayName: 'CPM',
        format: '$',
        width: '50',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },


      {
        field: 'dp_impressions_tk2',
        displayName: 'Imps',
        format: 'number',
        width: '70',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'dp_clicks_tk2',
        displayName: 'Clicks',
        format: 'number',
        width: '70',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'dp_revenue_tk2',
        displayName: 'Revenue',
        format: '$',
        width: '70',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'ctr_tk2',
        displayName: 'CTR',
        format: 'percentage',
        width: '50',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'cpc_tk2',
        displayName: 'CPC',
        format: '$',
        width: '50',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'cpm_tk2',
        displayName: 'CPM',
        format: '$',
        width: '50',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },



      {
        field: 'rev_per',
        displayName: 'Revenue %',
        format: 'percentage',
        width: '110',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

    ];

    this.flatTableJson = {
      page_size: 50,
      page: 0,
      lazy: false,
      loading: false,
      export: true,
      reload: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '310px',
      totalRecords: 1000,
      columns: this.flatTableColumnDef.slice(2),
      selectedColumns: this.flatTableColumnDef.slice(2),
      frozenCols: [this.flatTableColumnDef[0], this.flatTableColumnDef[1]],
      // frozenWidth: '250px',
      frozenWidth:
        this.flatTableColumnDef
          .slice(0, 2)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',

      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        console.log('appConfig', this.appConfig);
        this.isExportReport = this.appConfig['permissions'].some(
          o => o.name === 'revenue-compare-export-reports'
        );
        this._titleService.setTitle(
          this.appConfig['displayName']
        );
        let startDate;
        let endDate;
        const date_config = this.appConfig['filter']['filterConfig']['filters'][
          'datePeriod'
        ][0];
        if ((date_config['defaultDate'][0]['value'] === 0 && date_config['defaultDate'][0]['period'] === 'month') && (moment().format('DD') === '01' || moment().format('DD') === '02')) {
          startDate = moment().subtract(1, 'months').startOf('month');
          endDate = moment().subtract(1, 'months').endOf('month');
        } else {
          if (
            date_config[
            'defaultDate'
            ][0]['startOf']
          ) {
            startDate = moment()
              .subtract(
                date_config['defaultDate'][0]['value'],
                date_config['defaultDate'][0]['period']
              )
              .startOf(
                date_config['defaultDate'][0]['period']
              );
          } else {
            startDate = moment().subtract(
              date_config['defaultDate'][0]['value'],
              date_config['defaultDate'][0]['period']
            );
          }
          endDate = moment().subtract(
            date_config[
            'defaultDate'
            ][1]['value'],
            date_config[
            'defaultDate'
            ][1]['period']
          );
        }

        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: startDate.format('YYYYMMDD'),
            time_key2: endDate.format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: this.appConfig['filter']['filterConfig']['groupBy'].filter(
            v => v.selected
          )
        };

        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: startDate.format('YYYYMMDD'),
            time_key2: endDate.format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: this.appConfig['filter']['filterConfig']['groupBy'].filter(
            v => v.selected
          )
        };
        this.initialLoading();
      }
    });
  }

  /* ------------------------------- Table Start ------------------------------ */

  initialLoading() {
    this.flatTableReq = {
      dimensions: ['source', 'publisher'],
      metrics: ['dp_revenue', 'dp_impressions', 'dp_clicks'],
      derived_metrics: ['ctr', 'cpm', 'cpc'],
      timeKeyFilter: {},
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [], // [{ key: 'source', opcode: 'desc' }],
      limit: '',
      offset: ''
    };
    // this.loadFlatTableData();
  }
  onLazyLoadAggTable(e: Event) {
    console.log('Lazy Agg', e);
  }


  loadFlatTableData() {
    this.flatTableJson['loading'] = true;
    this.flatTableReq['timeKeyFilter']['time_key1'] = this.libServ.deepCopy(this.filtersApplied['timeKeyFilter']['time_key1']);
    this.flatTableReq['timeKeyFilter']['time_key2'] = this.libServ.deepCopy(this.filtersApplied['timeKeyFilter']['time_key1']);

    let time_key1_data;
    let time_key2_data;
    const newColumns = [];

    this.dataFetchServ.getRevMgmtData(this.flatTableReq).subscribe(data => {
      if (data['status'] === 0) {
        // this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }
      const tableData = data['data'];
      time_key1_data = tableData;
      const publisher = Array.from(new Set(time_key1_data.map(s => s['website'])));

      this.flatTableReq['timeKeyFilter']['time_key1'] = this.libServ.deepCopy(this.filtersApplied['timeKeyFilter']['time_key2']);
      this.flatTableReq['timeKeyFilter']['time_key2'] = this.libServ.deepCopy(this.filtersApplied['timeKeyFilter']['time_key2']);

      this.dataFetchServ.getRevMgmtData(this.flatTableReq).subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        const tableData = data['data'];
        time_key2_data = tableData;

        const publisher1 = Array.from(new Set(time_key2_data.map(s => s['website'])));
        const c = publisher.concat(publisher1.filter((item) => publisher.indexOf(item) < 0));

        c.forEach(element => {
          const bk1 = time_key1_data.filter(item => item['website'] == element);
          const bk2 = time_key2_data.filter(item => item['website'] == element);
          let rev_per;

          if (bk1.length > 0 && bk2.length > 0) {

            const t = bk2[0]['dp_revenue'] - bk1[0]['dp_revenue'];
            rev_per = (t / bk1[0]['dp_revenue']) * 100;
          } else if (bk1.length.length == 0 && bk2.length > 0) {
            rev_per = bk2[0]['dp_revenue'];
          }
          if (isNaN(rev_per)) {
            rev_per = undefined;
          }
          if (!isFinite(rev_per)) {
            rev_per = undefined;
          }

          const obj = {};
          obj['website_tk1'] = element;
          obj['time_key1'] = this.time_key1;
          obj['time_key2'] = this.time_key2;

          if (bk1.length > 0) {
            obj['country_tk1'] = bk1[0]['country'];
            obj['dp_impressions_tk1'] = bk1[0]['dp_impressions'];
            obj['dp_clicks_tk1'] = bk1[0]['dp_clicks'];
            obj['ctr_tk1'] = bk1[0]['ctr'];
            obj['cpc_tk1'] = bk1[0]['cpc'];
            obj['cpm_tk1'] = bk1[0]['cpm'];
            obj['dp_revenue_tk1'] = bk1[0]['dp_revenue'];
          }
          if (bk2.length > 0) {
            obj['country_tk1'] = bk2[0]['country'];
            obj['dp_impressions_tk2'] = bk2[0]['dp_impressions'];
            obj['dp_clicks_tk2'] = bk2[0]['dp_clicks'];
            obj['ctr_tk2'] = bk2[0]['ctr'];
            obj['cpc_tk2'] = bk2[0]['cpc'];
            obj['cpm_tk2'] = bk2[0]['cpm'];
            obj['dp_revenue_tk2'] = bk2[0]['dp_revenue'];
          }
          obj['rev_per'] = rev_per;

          newColumns.push(obj);

        });

        const arr = [];
        this.tableData = newColumns;

        newColumns.forEach((row: object) => {
          const obj = {};
          obj['data'] = row;
          arr.push(obj);
        });

        this.flatTableData = <TreeNode[]>arr;
        this.flatTableJson['totalRecords'] = arr.length; // data['totalItems'];
        this.flatTableJson['loading'] = false;

      });

    });
  }



  resetPagination(tt1) { }

  reset(table) {
    table.reset();
    this.loadFlatTableData();
  }

  onLazyLoadFlatTable(e: Event) {
    if (typeof this.flatTableReq !== 'undefined') {
      const orderby = [];
      if (
        typeof e['multiSortMeta'] !== 'undefined' &&
        e['multiSortMeta'] !== null
      ) {
        e['multiSortMeta'].forEach(sort => {
          orderby.push({
            key: sort['field'],
            opcode: 'desc'
          });
          this.orderByDefault = sort['order'];
        });
      }

      this.flatTableReq['limit'] = 0;
      this.flatTableReq['offset'] = 0;
      this.flatTableReq['orderBy'] = orderby;
      this.loadFlatTableData();
    }
  }

  isHiddenColumn(col: Object) {
    return (
      this.flatTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.flatTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }


  /* --------------------- On Filter Apply Button Clicked --------------------- */

  onFiltersApplied(filterData: object) {

    this.filtersApplied['filters']['dimensions'] = [];

    // tslint:disable-next-line: forin
    for (const k in filterData['filter']['dimensions']) {
      if (filterData['filter']['dimensions'][k].length !== 0) {

        if (k == 'source') {
          this.sourceName = filterData['filter']['dimensions'][k];
        }

        if (Array.isArray(filterData['filter']['dimensions'][k])) {
          this.filtersApplied['filters']['dimensions'].push({
            key: k,
            values: filterData['filter']['dimensions'][k]
          });
        } else {
          this.filtersApplied['filters']['dimensions'].push({
            key: k,
            values: [filterData['filter']['dimensions'][k]]
          });
        }
      }
    }

    // this.orderByDefault = 1;
    // if (filterData['filter']['dimensions']) {
    //   if (filterData['filter']['dimensions'].length > 0) {
    //     this.selectedDimension =
    //       filterData['filter']['dimensions'];
    //   } else {
    //     this.selectedDimension = 'source';
    //   }
    // }

    // if (filterData['filter']['dimensions']['dimension']) {
    //   if (filterData['filter']['dimensions']['metric'].length > 0) {
    //     this.selectedMetric = filterData['filter']['dimensions']['metric'][0];
    //   } else {
    //     this.selectedMetric = 'dp_revenue';
    //   }
    // }

    // this.filtersApplied['groupby'] = filterData['groupby'];
    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];


    this.time_key1 = moment(filterData['date'][0], 'YYYYMMDD').format('MM-DD-YYYY');
    this.time_key2 = moment(filterData['date'][1], 'YYYYMMDD').format('MM-DD-YYYY');

    this.flatTableReq = {
      dimensions: ['country', 'source', 'website'],
      metrics: ['dp_revenue', 'dp_impressions', 'dp_clicks'],
      derived_metrics: ['ctr', 'cpm', 'cpc'],
      timeKeyFilter: {},
      // this.libServ.deepCopy(
      //   this.filtersApplied['timeKeyFilter']
      // ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['country', 'source', 'website'],
      orderBy: [], // [{ key: this.selectedMetric, opcode: 'desc' }],
      limit: 0,
      offset: 0
    };
    this.loadFlatTableData();
    // this.loadMetricChart();
  }

  showToast() {
    this.toastService.displayToast({
      severity: 'success',
      summary: 'Saple Toast',
      detail: 'Sample App Loaded'
    });
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['displayName']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    } else if (type === 'ppt') {
      const chartElements = document.getElementsByTagName('ym-charts');
      const slides = [];
      [].forEach.call(chartElements, (el, index) => {
        const c = el.getElementsByTagName('canvas')[0];

        slides.push({
          title: `${this.appConfig['displayName']}`,
          base64data: this.libServ.canvasToImage('white', c),
          width: 8,
          height: 3
        });

        if (index === chartElements.length - 1) {
          const pptConfig = {
            filename: 'Charts',
            logo: {
              visible: true,
              path: '/assets/images/ym.png',
              width: 0.53125,
              height: 0.458333
            },
            slides: slides
          };
          this.pptExport.exportPPT(pptConfig);
        }
      });
    }
    return false;
  }

  exportTable(fileFormat) {
    //   if (table === 'flat') {
    const columnDefs = this.libServ.deepCopy(this.flatTableColumnDef);
    const req = this.libServ.deepCopy(this.flatTableReq);
    req['limit'] = 0; // Max value of Int MySql
    req['offset'] = 0;
    req['orderBy'] = [{ key: 'dp_revenue', opcode: 'desc' }];
    req['isTable'] = false;
    // this.dataFetchServ.getRevMgmtData(req).subscribe(response => {
    //   const data = [];
    //   const res = response['data'];
    //   res.forEach((o: object) => {
    //     const tk = o['time_key'];
    //     delete o['time_key'];
    //     o['time_key'] = moment(tk, 'YYYYMMDD').format('MM-DD-YYYY');
    //   });



    columnDefs.splice(2, 0,
      {
        field: 'time_key1',
        displayName: 'Select Date',
        format: 'date',
        width: '120',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: false
        }
      }
    );

    columnDefs.splice(9, 0,
      {
        field: 'time_key2',
        displayName: 'Compare to Date',
        format: 'date',
        width: '120',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: false
        }
      }
    );

    const data = [];
    data.push({
      data: [
        {
          data: this.tableData,
          columnDefs: columnDefs
        }
      ],
      sheetname: 'Data'
    });
    this.exportService.exportReport(
      data,

      moment(
        this.filtersApplied['timeKeyFilter']['time_key1'],
        'YYYYMMDD'
      ).format('MM-DD-YYYY') +
      ' To ' +
      moment(
        this.filtersApplied['timeKeyFilter']['time_key2'],
        'YYYYMMDD'
      ).format('MM-DD-YYYY'),
      fileFormat,
      false
    );
    //      });
    // }
    return false;
  }

  getFooters(grpBy) {
    const tableReq1 = {
      dimensions: [],
      metrics: ['dp_revenue', 'dp_impressions', 'dp_clicks'],
      derived_metrics: ['cpm', 'cpc', 'ctr'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.filtersApplied['filters'],
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },

      gidGroupBy: grpBy, // grpBys,
      orderBy: [],
      limit: 0,
      offset: 0
    };

    this.dataFetchServ.getRevMgmtData(tableReq1).subscribe(data11 => {
      if (data11['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data11['status_msg']);
        return;
      }

      const data1 = data11['data'][0];
      this.flatTableJson['selectedColumns'].forEach(c => {
        if (typeof data1 !== 'undefined') {
          c['footerTotal'] = data1[c['field']];
        } else {
          c['footerTotal'] = 0;
        }
      });
    });
  }

  ngOnDestroy(): void {
    this.appConfigObs.unsubscribe();
  }
}
