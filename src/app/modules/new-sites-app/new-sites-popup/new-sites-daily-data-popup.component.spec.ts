import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewSitesDataPopupComponent } from './new-sites-daily-data-popup.component';

describe('Rev360DailyDataPopupComponent', () => {
  let component: NewSitesDataPopupComponent;
  let fixture: ComponentFixture<NewSitesDataPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NewSitesDataPopupComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewSitesDataPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
