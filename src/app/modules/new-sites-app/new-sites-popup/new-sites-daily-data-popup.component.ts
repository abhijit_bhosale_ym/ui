import { Component, OnInit, OnDestroy } from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { FetchApiDataService } from '../fetch-api-data.service';
import * as moment from 'moment';
import { DynamicDialogConfig} from 'primeng/dynamicdialog';
import { TreeNode } from 'primeng/api';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';

import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
@AutoUnsubscribe()
@Component({
  selector: 'ym-new-sites-daily-data-popup',
  templateUrl: './new-sites-daily-data-popup.component.html',
  styleUrls: ['./new-sites-daily-data-popup.component.scss']
})
export class NewSitesDataPopupComponent implements OnInit, OnDestroy {
  lineChartJson: object;
  showLineChart = false;
  filtersApplied: object;
  showMainPieChart = false;
  mainPieChartJson: object;
  tabChangedFlag = false;
  noDataMetricChart = false;
  tableDataReq: object;
  tableData: TreeNode[];
  tableColumnDef: any[];
  dimColDef: any[];
  tableJson: Object;
  isExportReport = false;
  constructor(
    private libServ: CommonLibService,
    private formatNumPipe: FormatNumPipe,
    private dataFetchServ: FetchApiDataService,
    private config: DynamicDialogConfig,
    private exportService: ExportdataService,
    private toastService: ToastService
  ) {
    this.filtersApplied = this.config.data['filters'];
  }

  ngOnInit() {
    this.isExportReport = this.config.data['isExportReport'];
    this.loadPieChart();

    this.tableColumnDef = [


      {
        field: 'time_key',
        displayName: 'Date',
        format: 'date',
        width: '120',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'publisher',
        displayName: 'Publisher',
        format: '',
        width: '230',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },

      {
        field: 'website',
        displayName: 'Publisher Site URL',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'dp_revenue',
        displayName: 'Revenue',
        format: '$',
        width: '130',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },

      {
        field: 'dp_impressions',
        displayName: 'Impressions',
        format: 'number',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        width: '130',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'dp_clicks',
        displayName: 'Clicks',
        format: 'number',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        width: '130',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'ctr',
        displayName: 'CTR',
        format: 'percentage',
        width: '85',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'cpm',
        displayName: 'CPM',
        format: '$',
        width: '85',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [3],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'cpc',
        displayName: 'CPC',
        format: '$',
        width: '85',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [3],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
    ];


    this.tableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      reload: true,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '300px',
      totalRecords: 1000,

      //      columns: this.tableColumnDef,
      //    selectedColumns: this.tableColumnDef,
      // frozenCols: [],
      // frozenWidth: '0px',
      columns: this.tableColumnDef.slice(1),
      selectedColumns: this.tableColumnDef.slice(1),
      frozenCols: [...this.tableColumnDef.slice(0, 1)],
      frozenWidth:
        this.tableColumnDef
          .slice(0, 1)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',

      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: false,
      columnSearch: false
    };


    console.log('filter applied', this.filtersApplied);



    this.tableDataReq = {
      dimensions: ['website', 'publisher', 'time_key'],
      metrics: ['dp_revenue', 'dp_impressions', 'dp_clicks'],
      derived_metrics: ['ctr', 'cpm', 'cpc'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: [
        'website', 'publisher'
      ],
      orderBy: [{ key: 'time_key', opcode: 'desc' }],
      limit: '',
      offset: ''
    };

    this.loadTableData();
  }


  loadTableData() {

    this.dataFetchServ.getRevMgmtData(this.tableDataReq).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }
      const tableData = data['data'];


      console.log('table data.....', tableData);


      const arr = [];
      tableData.forEach((row: object) => {
        const obj = {
          data: row
        };
        arr.push(obj);
      });

      this.tableData = <TreeNode[]>arr;
      this.tableJson['totalRecords'] = data['totalItems'];
      this.tableJson['loading'] = false;
    });
  }

  exportTablePopup(fileFormat) {
    const colDef = this.libServ.deepCopy(this.tableColumnDef);

    const tableReq = {
      dimensions: ['publisher', 'website', 'time_key'],
      metrics: ['dp_revenue', 'dp_impressions', 'dp_clicks'],
      derived_metrics: ['ctr', 'cpm', 'cpc'],

      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: ['publisher', 'website'], // grpBys.filter(v => v !== 'time_key'),
      orderBy: [{ key: 'time_key', opcode: 'desc' }],
      limit: '',
      offset: ''
    };

    tableReq['isTable'] = false;
    this.dataFetchServ.getRevMgmtData(tableReq).subscribe(success => {
      if (success['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(success['status_msg']);
        return;
      }
      const data = [];
      const dailyData = this.libServ.deepCopy(success['data']);

      dailyData.forEach((o: object) => {
        const tk = o['time_key'];
        delete o['time_key'];
        o['time_key'] = moment(tk, 'YYYYMMDD').format('MM-DD-YYYY');
      });

      data.push({
        data: [
          {
            data: dailyData,
            columnDefs: colDef
          }
        ],

        sheetname: 'Data Distribution'
      });

      this.exportService.exportReport(
        data,
        'Daily Data' +
        ' From ' +
        moment(
          this.filtersApplied['timeKeyFilter']['time_key1'],
          'YYYYMMDD'
        ).format('MM-DD-YYYY') +
        ' To ' +
        moment(
          this.filtersApplied['timeKeyFilter']['time_key2'],
          'YYYYMMDD'
        ).format('MM-DD-YYYY'),
        fileFormat,
        false
      );
    });

    return false;
  }



  loadPieChart() {

    let grpBys = this.filtersApplied['groupby']; // .map(e => e.key);

    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          this.filtersApplied['filters']['dimensions']
            .filter(f => f.values.length)
            .map(m => m.key)
            .concat(grpBys)
        )
      );
    }

    grpBys.push('source');

    const params = {
      dimensions: ['source'],
      metrics: ['dp_revenue'],
      derived_metrics: [],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.filtersApplied['filters'],
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },

      gidGroupBy: grpBys,
      orderBy: [],
      limit: '',
      offset: ''
    };

    this.mainPieChartJson = {
      chartTypes: [{ key: 'pie', label: 'Pie Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Revenue Distribution Across Sources'
        },
        legend: {
          display: false
        },
        elements: {
          arc: {
            borderWidth: 0
          }
        },
        plugins: {
          datalabels: {
            formatter: (value, ctx) => {
              let sum = 0;
              const dataArr = ctx.chart.data.datasets[0].data;
              dataArr.map(data => {
                sum += data;
              });
              const percentage = (value * 100) / sum;
              if (percentage > 10) {
                return `${this.formatNumPipe.transform(
                  value,
                  '$',
                  []
                )} `;
              } else {
                return '';
              }
            },
            color: 'black'
          }
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };
    this.mainPieChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];

          return `${
            data.labels[tooltipItem.index]
            } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
        }
      }
    };

    this.showMainPieChart = false;
    this.dataFetchServ.getRevMgmtData(params).subscribe(data => {
      const chartData = data['data'];
      this.noDataMetricChart = false;
      if (!chartData.length) {
        this.noDataMetricChart = true;
      }
      const sources = Array.from(new Set(chartData.map(s => s['source'])));
      const colors = this.libServ.dynamicColors(sources.length);
      this.mainPieChartJson['chartData']['labels'] = sources;
      this.mainPieChartJson['chartData']['datasets'][0]['data'] = Array.from(
        new Set(chartData.map(s => s['dp_revenue']))
      );

      setTimeout(() => {
        this.mainPieChartJson['chartData']['datasets'][0][
          'backgroundColor'
        ] = colors;
        this.showMainPieChart = true;
      }, 0);
    });
  }

  tabChanged(e) {
    if (!this.tabChangedFlag) {
      this.loadPieChart();
      this.tabChangedFlag = true;
    }
    // console.log('tab changed', e);
  }

  chartSelected(e) { }

  ngOnDestroy() {}
}
