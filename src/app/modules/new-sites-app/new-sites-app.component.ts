import {
  Component,
  OnInit,
  QueryList,
  ElementRef,
  ViewChildren,
  OnDestroy
} from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { FetchApiDataService } from './fetch-api-data.service';
import { TreeNode } from 'primeng/api';
import { ToastService } from '../../_services/toast-notification/toast.service';
import { DialogService } from 'primeng/dynamicdialog';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import * as moment from 'moment';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { Title } from '@angular/platform-browser';
import { DataShareService } from 'src/app/_services/data-share/data-share.service';
import { Subscription } from 'rxjs';
import { NewSitesDataPopupComponent } from './new-sites-popup/new-sites-daily-data-popup.component';
import { groupBy } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
@AutoUnsubscribe()
@Component({
  selector: 'ym-new-sites-app',
  templateUrl: './new-sites-app.component.html',
  styleUrls: ['./new-sites-app.component.css']
})
export class NewSitesAppComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;
  appConfig: object = {};

  showMainLineChart = false;
  noDataMainLineChart = false;
  showCharts = false;
  defaultChartsJson: object;
  mainLineChartJson: object;
  mainPieChartJson: object;
  revenueChartJson: object;
  cpcChartJson: object;
  ctrChartJson: object;
  cpmChartJson: object;
  clicksChartJson: object;
  impressionsChartJson: object;
  dimensionsDropdown: any[];
  selectedDimension: any[];
  metricsDropdown: any[];
  selectedMetric: any[];
  metricChartJson: object;
  noDataMetricChart = false;
  selectedTabIndex = 1;
  showStackChart: boolean;
  chartDefaultJson: object;
  chartJson: object;


  showMetricChart = false;
  isExportReport = false;

  @ViewChildren('ttAgg') aggTableRef: QueryList<ElementRef>;
  @ViewChildren('ttfranklypayout') flatTableRef: QueryList<ElementRef>;


  lastUpdatedOn: Date;
  nextUpdated: Date;
  dataUpdatedThrough: Date;
  /* ---------------------------------- Table --------------------------------- */

  selectedChartName;
  filtersApplied: object = {};


  aggTableData: TreeNode[];
  noTableData = false;
  dimColDef: any[];
  //  metricColDef: any[];
  aggTableColumnDef: any[];
  aggTableJson: Object;
  displayAggTable = true;

  timeZone = environment.timeZone;

  /* ---------------------------------- Table --------------------------------- */

  constructor(
    private appConfigService: AppConfigService,
    private _titleService: Title,
    private libServ: CommonLibService,
    private dataFetchServ: FetchApiDataService,
    private toastService: ToastService,
    private dialogService: DialogService,
    private htmltoimage: HtmltoimageService,
    private pptExport: ExportPptService,
    private exportService: ExportdataService,
    private formatNumPipe: FormatNumPipe
  ) { }

  ngOnInit() {



    /* --------------------------------- Charts --------------------------------- */


    this.dimColDef = [
      {
        field: 'publisher',
        displayName: 'Publisher',
        format: '',
        width: '230',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'

      },

      {
        field: 'website',
        displayName: 'Publisher Site URL',
        format: '',
        width: '200',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: '-'
      },
      {
        field: 'min_time_key',
        displayName: 'Date',
        format: 'date',
        width: '120',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: false
        },
        footerTotal: '-'

      },

      {
        field: 'dp_revenue',
        displayName: 'Revenue',
        format: '$',
        width: '130',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '0'

      },

      {
        field: 'dp_impressions',
        displayName: 'Impressions',
        format: 'number',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        width: '130',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '0'
      },
      {
        field: 'dp_clicks',
        displayName: 'Clicks',
        format: 'number',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        width: '130',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '0'
      },
      {
        field: 'ctr',
        displayName: 'CTR',
        format: 'percentage',
        width: '85',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '0'
      },
      {
        field: 'cpm',
        displayName: 'CPM',
        format: '$',
        width: '85',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [3],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '0'
      },
      {
        field: 'cpc',
        displayName: 'CPC',
        format: '$',
        width: '85',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [3],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '0'
      },
    ];
    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      reload: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,

      columns: this.libServ.deepCopy(this.dimColDef.slice(2, this.dimColDef.length)),
      selectedColumns: this.libServ.deepCopy(this.dimColDef.slice(2, this.dimColDef.length)),
      frozenCols: this.libServ.deepCopy([...this.dimColDef.slice(0, 2)]),
      frozenWidth:
        this.dimColDef
          .slice(0, 2)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: false,
      columnSearch: false
    };

    this.chartDefaultJson = {
      chartTypes: [
        { key: 'bar', label: 'Stack Trend', stacked: true }
      ],
      chartData: {
        labels: [],
        datasets: []
      },
      chartOptions: {
        title: {
          display: true,
          text: ''
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Dates'
              }
            }
          ],
          yAxes: [
            {
              display: true,
              scaleLabel: {
                display: true
              }
            }
          ]
        },
        pan: {
          enabled: true,
          mode: 'x'
        },
        zoom: {
          enabled: false,
          mode: 'x'
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };


    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        console.log('appConfig-----', this.appConfig);
        this.isExportReport = this.appConfig['permissions'].some(
          o => o.name === 'new-sites-export-reports'
        );
        this._titleService.setTitle(this.appConfig['displayName']);
        let startDate;
        if (
          this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
          'defaultDate'
          ][0]['startOf']
        ) {
          startDate = moment()
            .subtract(
              this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
                'defaultDate'
                ][0]['value'],
                this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
                  'defaultDate'
                  ][0]['period']
            )
            .startOf(
              this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
                'defaultDate'
                ][0]['period']
            );
        } else {
          startDate = moment().subtract(
            this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
              'defaultDate'
              ][0]['value'],
              this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
                'defaultDate'
                ][0]['period']
          );
        }
        const endDate = moment().subtract(
          this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
            'defaultDate'
            ][1]['value'],
            this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
              'defaultDate'
              ][1]['period']
        );

        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: startDate.format('YYYYMMDD'),
            time_key2: endDate.format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: this.appConfig["filter"]["filterConfig"]["groupBy"].filter(
            v => v.selected
          )
        };
        this.initialLoading();
      }
    });
  }

  /* ------------------------------- Table Start ------------------------------ */

  initialLoading() {
    console.log('this.appConfig last up', this.appConfig['id']);
    this.dataFetchServ
    .getLastUpdatedData(this.appConfig['id'])
    .subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }
      this.lastUpdatedOn = moment(data[0]['updated_at']).toDate();
      this.nextUpdated = moment(data[0]['next_run_at']).toDate();
      this.dataUpdatedThrough = moment(
        data[0]['source_updated_through'],
        'YYYYMMDD'
      ).toDate();
    });
    const flatTableReq = {
      dimensions: ['website', 'publisher', 'min_time_key'],
      metrics: ['dp_revenue', 'dp_impressions', 'dp_clicks'],
      derived_metrics: ['ctr', 'cpm', 'cpc'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: [
        'website', 'publisher'
      ],
      orderBy: [{ key: 'dp_revenue', opcode: 'desc' }],
      limit: 25,
      offset: 0
    };

    this.loadGraph();
    this.loadTableData(flatTableReq);
  }

  // loadFlatTableData(params: Object) {
  //   this.flatTableJson['loading'] = true;
  //   params['limit']='';
  //   params['orderBy']=[{ key: 'dp_revenue', opcode: 'desc' }];

  //   this.dataFetchServ.getRevMgmtData(params).subscribe(data => {
  //     console.log('data', data)
  //     // const arr = [];
  //     // for (const r of data['data']) {
  //     //   const obj = {};
  //     //   obj['data'] = r;
  //     //   arr.push(obj);
  //     // };


  //     const tableData = data['data'];

  //     const arr = [];
  //     tableData.forEach((row: object) => {
  //      // for (let i = 0; i < grpBysFilter.length; i++) {
  //      console.log('websites================',row['website']);

  //      if (row['website']== undefined) {
  //           row['website'] = 'All';
  //         }
  //       //}
  //       let obj = {};
  //       if (row['website']=='All') {
  //         obj = {
  //           data: row,
  //           children: [{ data: {} }]
  //         };


  //       } else {
  //         obj = {
  //           data: row
  //         };
  //       }
  //       arr.push(obj);
  //     });


  //     this.flatTableData = <TreeNode[]>arr;
  //     this.flatTableJson['totalRecords'] = 25;
  //     this.flatTableJson['loading'] = false;
  //   });
  // }

  loadTableData(params) {
    // this.reloadAggTable();
    this.aggTableJson['loading'] = true;

    //   params['dimensions'].push('accounting_key');

    this.dataFetchServ.getNewWebSiteData(params).subscribe(data => {
      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      } else {
        this.noTableData = false;
      }
      const grpBysFilter = ['publisher', 'website'];

      const tableData = data['data'];
      const arr = [];
      tableData.forEach((row: object) => {
        for (let i = 0; i < grpBysFilter.length; i++) {
          if (i !== grpBysFilter.length - 1) {
            row['website'] = 'All';
          }
        }
        let obj = {};
        if (grpBysFilter.length === 1) {
          obj = {
            data: row
          };
        } else {
          obj = {
            data: row,
            children: [{ data: {} }]
          };
        }

        // obj['data']['isExpanded'] = '0';
        arr.push(obj);
      });

      this.getFooters(grpBysFilter);

      this.aggTableData = <TreeNode[]>arr;
      this.aggTableJson['totalRecords'] = data['totalItems'];
      this.aggTableJson['loading'] = false;
    });
  }

  loadGraph() {
    const req = {
      dimensions: ['website', 'min_time_key'],
      metrics: ['dp_revenue'],
      derived_metrics: [],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: [
        'website', 'publisher'
      ],
      orderBy: [{ key: 'min_time_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };
    this.chartJson = this.libServ.deepCopy(this.chartDefaultJson);
    this.chartJson['chartOptions']['title']['text'] = 'Revenue Stack by new Publisher Site URL';
    this.chartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
      // Include a dollar sign in the ticks
      callback: (value, index, values) => {
        return this.formatNumPipe.transform(value, '$', [0]);
      }
    };
    this.chartJson['chartOptions']['scales']['yAxes'][0]['scaleLabel'][
      'labelString'
    ] = 'Revenue ($)';
    this.chartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return `${
            data.datasets[tooltipItem.datasetIndex].label
            } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
        }
      }

    };
    this.showStackChart = false;

    this.dataFetchServ.getNewWebSiteData(req).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      }
      const chartData = data['data'];
      this.noDataMetricChart = false;
  if (!chartData.length) {
    this.noDataMetricChart = true;
    return;
  }
      const datesArr = Array.from(new Set(chartData.map(r => r['min_time_key'])));
      const dimData = Array.from(new Set(chartData.map(s => s['website'])));
      this.chartJson['chartData']['labels'] = datesArr.map(d =>
        moment(d, 'YYYYMMDD').format('YYYY-MM-DD')
      );
      const colors = this.libServ.dynamicColors(dimData.length);
      setTimeout(() => {
      dimData.forEach((src, i) => {
        const revArr = [];
        datesArr.forEach(time_key => {
          let isavalable = false;
          chartData.forEach(r => {
            if (r['website'] === src && r['min_time_key'] === time_key) {
            revArr.push(r['dp_revenue']);
            isavalable = true;
            }
          });
          if (!isavalable) {
            revArr.push(null);
          }
        });
        this.chartJson['chartData']['datasets'].push({
          label: src,
          data: revArr,
          borderColor: colors[i],
          fill: false,
          backgroundColor: colors[i]
        });
      });
      this.showStackChart = true;
    }, 0);
    });

  }

  openPopup(row, field, rowData) {
    const grpBys = ['publisher'];
    const filtersApplied = this.libServ.deepCopy(this.filtersApplied);

    const isStationGrpAvail = filtersApplied['filters']['dimensions'].findIndex(
      item => item.key === 'publisher'
    );

    // filtersApplied['timeKeyFilter']['time_key1'] = rowData['accounting_key'];
    // filtersApplied['timeKeyFilter']['time_key2'] = moment(
    //   rowData['accounting_key'],
    //   'YYYYMMDD'
    // )
    //   .endOf('month')
    //   .format('YYYYMMDD');

    filtersApplied['filters']['dimensions'].splice(
      filtersApplied['filters']['dimensions'].findIndex(
        item => item.key === 'publisher'
      ),
      1
    );

    filtersApplied['filters']['dimensions'].push({
      key: 'publisher',
      values: [rowData['publisher']]
    });

    if (rowData['website'] !== 'All') {
      filtersApplied['filters']['dimensions'].splice(
        filtersApplied['filters']['dimensions'].findIndex(
          item => item.key === 'website'
        ),
        1
      );


      filtersApplied['filters']['dimensions'].push({
        key: 'publisher',
        values: [rowData['publisher']]
      });


      filtersApplied['filters']['dimensions'].push({
        key: 'website',
        values: [rowData['website']]
      });
    }

    filtersApplied['groupby'] = grpBys;
    const data = {
      filters: filtersApplied,
      field: row,
      isExportReport: this.isExportReport
    };
    const ref = this.dialogService.open(NewSitesDataPopupComponent, {
      header: row + ' Daily Data ',
      contentStyle: { width: '80vw', overflow: 'auto' },
      data: data
    });
    ref.onClose.subscribe((data1: string) => { });
  }

  getFooters(grpBys) {
    const tableReq1 = {
      dimensions: [],
      metrics: ['dp_revenue', 'dp_impressions', 'dp_clicks'],
      derived_metrics: ['cpm', 'cpc', 'ctr'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.filtersApplied['filters'],
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },

      gidGroupBy: grpBys, // grpBys,
      orderBy: [],
      limit: '',
      offset: ''
    };



    this.dataFetchServ.getNewWebSiteData(tableReq1).subscribe(data11 => {
      if (data11['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data11['status_msg']);
        return;
      }


      const data1 = data11['data'][0];
      this.aggTableJson['selectedColumns'].forEach(c => {
        if (typeof data1 !== 'undefined') {
          c['footerTotal'] = data1[c['field']];
        } else {
          c['footerTotal'] = 0;
        }
      });
    });
  }

  onTableDrill(e: Event) {

    console.log('Drilled', e);
    if (!e['node']['childLoaded']) {
      this.aggTableJson['loading'] = true;
      const tableReq = {
        dimensions: ['publisher', 'website', 'min_time_key'],
        metrics: ['dp_revenue', 'dp_impressions', 'dp_clicks'],
        derived_metrics: ['ctr', 'cpm', 'cpc'],

        timeKeyFilter: this.libServ.deepCopy(
          this.filtersApplied['timeKeyFilter']
        ),
        filters: this.libServ.deepCopy(this.filtersApplied['filters']),
        groupByTimeKey: {
          key: ['accounting_key', 'time_key'],
          interval: 'daily'
        },
        gidGroupBy: ['website'], // this.getGrpBys(),
        orderBy: [],
        limit: '',
        offset: ''
      };

      const grpBys = ['publisher', 'website']; // this.filtersApplied['groupby'].map(e => e.key);
      for (const g of grpBys) {
        if (e['node']['data'][g] !== 'All') {
          // tableReq['dimensions'].push(g);
          if (!tableReq['gidGroupBy'].includes(g)) {
            tableReq['gidGroupBy'].push(g);
          }
          if (
            tableReq['filters']['dimensions'].findIndex(e2 => e2.key === g) === -1
          ) {
            tableReq['filters']['dimensions'].push({
              key: g,
              values: [e['node']['data'][g]]
            });
          } else {
            tableReq['filters']['dimensions'].splice(
              tableReq['filters']['dimensions'].findIndex(e1 => e1.key === g),
              1
            );
            tableReq['filters']['dimensions'].push({
              key: g,
              values: [e['node']['data'][g]]
            });
            if (
              tableReq['filters']['dimensions']
                .find(e0 => e0.key === g)
              ['values'].findIndex(v => v === e['node']['data'][g]) === -1
            ) {
              tableReq['filters']['dimensions'].push(e['node']['data'][g]);
            }
          }
        }
        // else {
        //   tableReq['dimensions'].push(g);
        //   if (!tableReq['gidGroupBy'].includes(g)) {
        //     tableReq['gidGroupBy'].push(g);
        //   }
        //   break;
        // }
      }

      // tableReq['gidGroupBy'] = this.getGrpBys();
      // For Dev purpose
      // tableReq['gidGroupBy'] = [
      //   'ad_size',
      //   'ad_type',
      //   'ad_units',
      //   'bidding_type',
      //   'device_category',
      //   'geography',
      //   'sales_channel',
      //   'sales_type',
      //   'site',
      //   'source'
      // ];

      this.dataFetchServ.getRevMgmtData(tableReq).subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        const childData = data['data'];
        const arr = [];

        childData.forEach((row: object) => {
          this.aggTableJson['frozenCols'].forEach(r => {
            if (typeof row[r['field']] === 'undefined') {
              row[r['field']] = 'All';
            }
          });
          let obj = {};
          if (row[grpBys[grpBys.length - 1]] === 'All') {
            obj = {
              data: row,
              children: [{ data: {} }]
            };
          } else {
            obj = {
              data: row
            };
          }
          obj['data']['isExpanded'] = '1';
          arr.push(obj);
        });

        this.aggTableJson['loading'] = false;
        e['node']['children'] = <TreeNode[]>arr;
        this.aggTableData = [...this.aggTableData];
        e['node']['childLoaded'] = true;
      });
    }
  }


  resetPagination(tt1) {
    if (typeof tt1 !== 'undefined') {
      tt1.reset();
    }
  }

  reset(table) {
    table.reset();
    const flatTableReq = {
      dimensions: ['website', 'publisher', 'min_time_key'],
      metrics: ['dp_revenue', 'dp_impressions', 'dp_clicks'],
      derived_metrics: ['ctr', 'cpm', 'cpc'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: [
        'website', 'publisher'
      ],
      orderBy: [{ key: 'dp_revenue', opcode: 'desc' }],
      limit: 25,
      offset: 0
    };
    this.loadTableData(flatTableReq);
  }

  onLazyLoadAggTable(e: Event) {
    console.log('Lazy Agg', e);
  }

  isHiddenColumn(col: Object) {
    return (
      this.aggTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }

  /* ------------------------------- Load Charts ------------------------------ */

  chartSelected(data: Event) {
    console.log('Chart Selected', data);
  }


  /* --------------------- On Filter Apply Button Clicked --------------------- */

  onFiltersApplied(filterData: object) {
    const filters = [];
    // tslint:disable-next-line: forin
    this.filtersApplied['filters']['dimensions'] = [];

    // tslint:disable-next-line: forin
    for (const k in filterData['filter']['dimensions']) {
      if (filterData['filter']['dimensions'][k].length !== 0) {
        this.filtersApplied['filters']['dimensions'].push({
          key: k,
          values: filterData['filter']['dimensions'][k]
        });
      }
    }

    this.filtersApplied['groupby'] = filterData['groupby'];
    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];

    const flatTableReq = {
      dimensions: ['publisher', 'min_time_key'],
      metrics: ['dp_revenue', 'dp_impressions', 'dp_clicks'],
      derived_metrics: ['ctr', 'cpm', 'cpc'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy:
        this.filtersApplied['filters']['dimensions']
          .filter(f => f.values.length)
          .map(m => m.key),
      orderBy: [{ key: 'dp_revenue', opcode: 'desc' }],
      limit: 25,
      offset: 0
    };

    if (flatTableReq['gidGroupBy'].length === 0) {
      flatTableReq['gidGroupBy'] = ['publisher'];
    }

    this.loadTableData(flatTableReq);
    this.loadGraph();
  }

  showToast() {
    this.toastService.displayToast({
      severity: 'success',
      summary: 'Saple Toast',
      detail: 'Sample App Loaded'
    });
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['displayName']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    } else if (type === 'ppt') {
      const chartElements = document.getElementsByTagName('ym-charts');
      const slides = [];
      [].forEach.call(chartElements, (el, index) => {
        const c = el.getElementsByTagName('canvas')[0];

        slides.push({
          title: `${this.appConfig['displayName']}`,
          base64data: this.libServ.canvasToImage('white', c),
          width: 8,
          height: 3
        });

        if (index === chartElements.length - 1) {
          const pptConfig = {
            filename: 'Charts',
            logo: {
              visible: true,
              path: '/assets/images/ym.png',
              width: 0.53125,
              height: 0.458333
            },
            slides: slides
          };
          this.pptExport.exportPPT(pptConfig);
        }
      });
    }
    return false;
  }


  exportTable(table, fileFormat) {
    const colDef = this.libServ.deepCopy(this.dimColDef);

    const tableReq = {
      dimensions: ['publisher', 'website', 'min_time_key'],
      metrics: ['dp_revenue', 'dp_impressions', 'dp_clicks'],
      derived_metrics: ['ctr', 'cpm', 'cpc'],

      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: ['publisher', 'website'], // grpBys.filter(v => v !== 'time_key'),
      orderBy: [{ key: 'dp_revenue', opcode: 'desc' }],
      limit: '',
      offset: ''
    };

    tableReq['isTable'] = false;
    this.dataFetchServ.getNewWebSiteData(tableReq).subscribe(success => {
      if (success['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(success['status_msg']);
        return;
      }
      const data = [];

      const dailyData = this.libServ.deepCopy(success['data']);

      dailyData.forEach((o: object) => {
        const tk = o['min_time_key'];
        delete o['min_time_key'];
        o['min_time_key'] = moment(tk, 'YYYYMMDD').format('MM-DD-YYYY');
      });

      data.push({
        data: [
          {
            data: dailyData,
            columnDefs: colDef
          }
        ],

        sheetname: 'New Websites'
      });

      this.exportService.exportReport(
        data,
        'New Websites ' +
        ' From ' +
        moment(
          this.filtersApplied['timeKeyFilter']['time_key1'],
          'YYYYMMDD'
        ).format('MM-DD-YYYY') +
        ' To ' +
        moment(
          this.filtersApplied['timeKeyFilter']['time_key2'],
          'YYYYMMDD'
        ).format('MM-DD-YYYY'),
        fileFormat,
        false
      );
    });

    return false;
  }

  ngOnDestroy(): void {
    this.appConfigObs.unsubscribe();
  }
}
