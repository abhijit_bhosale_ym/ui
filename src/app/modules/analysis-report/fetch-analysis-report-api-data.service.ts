import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FetchAnalysisReportApiDataService {
  private BASE_URL: string = environment.baseUrl;
  public responseRevSubject = new BehaviorSubject({});
  private responseRevData = this.responseRevSubject.asObservable();

  public responseVastSubject = new BehaviorSubject({});
  private responseVastData = this.responseVastSubject.asObservable();

  public responseUnfilledSubject = new BehaviorSubject({});
  private responseUnfilledData = this.responseUnfilledSubject.asObservable();

  // public obsresponse = this.responseData.asObservable();

  constructor(private http: HttpClient) { }

  getAnalysisData(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/analysis-report/getData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
  }

  getLostRevenueOpportunityData(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/analysis-report/getLostRevenueOpportunityData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
  }
  getTableData(params: object, ispermission: boolean) {
    const url = `${this.BASE_URL}/frankly/v1/revmgmt/getData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    if (ispermission) {
      return this.http.post(url, params);
    } else {
      // this.responseData.next({data: [],totalItems: 0});
     // this.responseSubject.next({data: [], totalItems: 0});
      return this.responseRevData;
    }
    // return <TreeNode[]> json.data;
  }
  getunfilledData(params: object, ispermission: boolean) {
    const url = `${this.BASE_URL}/frankly/v1/unfilled-inventory/getData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    if (ispermission) {
      return this.http.post(url, params);
    } else {
      // this.responseSubject.next({data: [], totalItems: 0});
      return this.responseUnfilledData;

    }
  }
  getExportReportData(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/common/exportData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
  }
  getVastData(params: object, ispermission: boolean) {
    const url = `${this.BASE_URL}/frankly/v1/vast-errors/getData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    if (ispermission) {
      return this.http.post(url, params);
    } else {
     // this.responseSubject.next({data: [], totalItems: 0});
      return this.responseVastData;
    }
  }
}
