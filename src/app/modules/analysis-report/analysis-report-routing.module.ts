import { Routes } from '@angular/router';
import { AnalysisReportComponent } from './analysis-report.component';

export const AnalysisReportAppRoutesAppRoutes: Routes = [
  {
    path: '',
    component: AnalysisReportComponent
  }
];
