import { Routes } from '@angular/router';
import { DailyKpiLiteAppComponent } from './daily-kpi-lite-app.component';

export const DailyKpiLiteAppRoutes: Routes = [
  {
    path: '',
    component: DailyKpiLiteAppComponent
  }
];
