import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Rev360DailyDataPopupComponent } from './rev360-daily-data-popup.component';

describe('Rev360DailyDataPopupComponent', () => {
  let component: Rev360DailyDataPopupComponent;
  let fixture: ComponentFixture<Rev360DailyDataPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Rev360DailyDataPopupComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Rev360DailyDataPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
