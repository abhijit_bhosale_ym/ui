import { Routes } from '@angular/router';
import { Rev360AppComponent } from './rev360-app.component';

export const Rev360AppRoutes: Routes = [
  {
    path: '',
    component: Rev360AppComponent
  }
];
