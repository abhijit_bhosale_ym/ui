import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Rev360ExportPopupComponent } from './rev360-export-popup.component';

describe('Rev360ExportPopupComponent', () => {
  let component: Rev360ExportPopupComponent;
  let fixture: ComponentFixture<Rev360ExportPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Rev360ExportPopupComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Rev360ExportPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
