import { Component, OnInit, OnDestroy } from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { FetchApiDataService } from './fetch-api-data.service';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import * as moment from 'moment';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { Title } from '@angular/platform-browser';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { Subscription } from 'rxjs';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { SendEmailComponent } from '../send-email/send-email.component';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { environment } from 'src/environments/environment';
import { DialogService } from 'primeng';

@Component({
  selector: 'ym-rpm-app',
  templateUrl: './rpm-app.component.html',
  styleUrls: ['./rpm-app.component.scss']
})
export class RpmAppComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;
  timeZone = environment.timeZone;

  appConfig: object = {};
  lastUpdatedOn: Date;
  filtersApplied: object = {};

  chartColors: string[];
  cardsJson = [];
  showCards = true;
  nextUpdated: Date;
  dataUpdatedThrough: Date;

  /* ------------------------- Revenue RPM Daily Chart ------------------------ */

  showRevRpmDailyChart = false;
  revRpmDailyChartJson: object;
  noDataRevRpmDailyChart = false;

  /* ---------------------------- Page views Daily ---------------------------- */

  showPageViewsDailyChart = false;
  pageViewsDailyChartJson: object;
  noDataPageViewsDailyChart = false;

  /* ------------------------- Revenue RPM Monthly Chart ------------------------ */

  showRevRpmMonthlyChart = false;
  revRpmMonthlyChartJson: object;
  noDataRevRpmMonthlyChart = false;

  /* ---------------------------- Page views Monthly ---------------------------- */

  showPageViewsMonthlyChart = false;
  pageViewsMonthlyChartJson: object;
  noDataPageViewsMonthlyChart = false;

  /* ---------------------------------- Table --------------------------------- */

  noTableData = false;
  aggTableData: TreeNode[];
  aggTableColumnDef: any[];
  aggTableJson: object;
  jumpToSource: object = {
    totalSources: [],
    selected: []
  };
  aggreTableReq: object;
  sourcesJson = {
    titantv: {
      Columns: ['titantv_page_views', 'titantv_revenue', 'titantv_rpm'],
      DisplayName: 'TitanTv'
    },
    antennaweb: {
      Columns: [
        'antennaweb_page_views',
        'antennaweb_revenue',
        'antennaweb_rpm'
      ],
      DisplayName: 'Antennaweb'
    }
  };
  exportRequest: ExportRequest = <ExportRequest>{};
  isExportReport = false;

  appFeaturesConfigs = {};
  mainCardsConfig = {};

  constructor(
    private appConfigService: AppConfigService,
    private _titleService: Title,
    private libServ: CommonLibService,
    private dataFetchServ: FetchApiDataService,
    private formatNumPipe: FormatNumPipe,
    private htmltoimage: HtmltoimageService,
    private toastService: ToastService,
    private exportService: ExportdataService,
    private pptExport: ExportPptService,
    private dialogService: DialogService,
    private confirmationService: ConfirmationService
  ) { }

  ngOnInit() {
    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        this.isExportReport = this.appConfig['permissions'].some(
          o => o.name === 'rpm-export-report'
        );
        this._titleService.setTitle(this.appConfig['displayName']);
        let startDate;
        let endDate;
        const date_config = this.appConfig['filter']['filterConfig']['filters'][
          'datePeriod'
        ][0];
        if ((date_config['defaultDate'][0]['value'] === 0 && date_config['defaultDate'][0]['period'] === 'month') && (moment().format('DD') === '01' || moment().format('DD') === '02')) {
          startDate = moment().subtract(1, 'months').startOf('month');
          endDate = moment().subtract(1, 'months').endOf('month');
        } else {
          if (
            date_config[
            'defaultDate'
            ][0]['startOf']
          ) {
            startDate = moment()
              .subtract(
                date_config['defaultDate'][0]['value'],
                date_config['defaultDate'][0]['period']
              )
              .startOf(
                date_config['defaultDate'][0]['period']
              );
          } else {
            startDate = moment().subtract(
              date_config['defaultDate'][0]['value'],
              date_config['defaultDate'][0]['period']
            );
          }
          endDate = moment().subtract(
            date_config[
            'defaultDate'
            ][1]['value'],
            date_config[
            'defaultDate'
            ][1]['period']
          );
        }

        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: startDate.format('YYYYMMDD'),
            time_key2: endDate.format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: [
            { key: 'accounting_key' },
            { key: 'station_group' },
            { key: 'derived_station_rpt' }
          ]
        };
        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        this.initialLoading();
      }
    });

    this.aggTableColumnDef = [
      {
        field: 'time_key',
        displayName: 'Month-Year',
        format: 'date',
        formatConfig: ['MMM-YY'],
        width: '100',
        exportConfig: {
          format: 'date<<MMM-YY<<-',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'time_key1',
        displayName: 'Date',
        format: 'date',
        width: '100',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: ['MM-DD-YYYY'],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'page_views',
        displayName: 'Total Page Views',
        format: 'number',
        width: '100',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: 0
      },
      {
        field: 'revenue',
        displayName: 'Total Gross Revenue',
        width: '100',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: 0
      },
      {
        field: 'rpm',
        displayName: 'Total RPM',
        format: '$',
        width: '100',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: 0
      }
    ];
    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '300px',
      totalRecords: 1000,
      columns: this.aggTableColumnDef.slice(5),
      selectedColumns: this.aggTableColumnDef.slice(5),
      frozenCols: [...this.aggTableColumnDef.slice(0, 5)],
      frozenWidth:
        this.aggTableColumnDef
          .slice(0, 5)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: false,
      columnSearch: false
    };
  }

  initialLoading() {
    this.dataFetchServ
      .getLastUpdatedData(this.appConfig['id'])
      .subscribe(data => {
        this.lastUpdatedOn = moment(data[0]['updated_at']).toDate();
        this.nextUpdated = moment(data[0]['next_run_at']).toDate();
        this.dataUpdatedThrough = moment(
          data[0]['source_updated_through'],
          'YYYYMMDD'
        ).toDate();
      });

    this.libServ.getAppFeaturesConfigs(this.appConfig['id']).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: data['message']
        });
        console.log(data['message']);
        return;
      }
      this.appFeaturesConfigs = data['data'];
      this.appFeaturesConfigs['cardsConfig']['cards'].forEach(element => {
        if (element['name'] === 'main') {
          this.mainCardsConfig = element['config'];
          this.cardsJson = this.mainCardsConfig['list'];
          if (this.mainCardsConfig['display']) {
            this.loadCards();
          }
        }
      });
    });

    this.chartColors = this.libServ.dynamicColors(3);
    /* --------------------------------- CHARTS --------------------------------- */

    const revRpmDailyChartReq = {
      dimensions: ['time_key'],
      metrics: ['revenue', 'page_views'],
      derived_metrics: ['rpm'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: {
        dimensions: [],
        metrics: []
      },
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [
        {
          key: 'time_key',
          opcode: 'asc'
        }
      ],
      limit: '',
      offset: ''
    };

    // For dev purpose
    revRpmDailyChartReq['gidGroupBy'] = ['derived_station_rpt', 'source'];

    const pageViewsDailyChartReq = {
      dimensions: ['time_key'],
      metrics: ['revenue', 'page_views'],
      derived_metrics: ['rpm'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: {
        dimensions: [],
        metrics: []
      },
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [
        {
          key: 'time_key',
          opcode: 'asc'
        }
      ],
      limit: '',
      offset: ''
    };

    // For dev purpose
    pageViewsDailyChartReq['gidGroupBy'] = ['derived_station_rpt', 'source'];

    this.loadRevRpmDailyChart(revRpmDailyChartReq);

    pageViewsDailyChartReq['groupByTimeKey']['key'] = ['accounting_key'];
    revRpmDailyChartReq['groupByTimeKey']['key'] = ['accounting_key'];
    pageViewsDailyChartReq['dimensions'] = ['accounting_key'];
    revRpmDailyChartReq['dimensions'] = ['accounting_key'];
    pageViewsDailyChartReq['orderBy'] = [
      {
        key: 'accounting_key',
        opcode: 'asc'
      }
    ];
    revRpmDailyChartReq['orderBy'] = [
      {
        key: 'accounting_key',
        opcode: 'asc'
      }
    ];

    this.loadRevRpmMonthlyChart(pageViewsDailyChartReq);

    /* ---------------------------------- TABLE --------------------------------- */

    this.aggreTableReq = {
      dimensions: ['accounting_key'],
      metrics: ['revenue', 'page_views'],
      derived_metrics: ['rpm'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: {
        dimensions: [],
        metrics: []
      },
      groupByTimeKey: {
        key: ['accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [],
      limit: '',
      offset: ''
    };

    // For dev purpose
    this.aggreTableReq['gidGroupBy'] = ['derived_station_rpt', 'source'];
    this.loadAggTable(this.aggreTableReq);
  }

  loadCards() {
    this.showCards = false;
    const apiUrl = this.mainCardsConfig['api']['url'];
    const cardsReq = this.libServ.deepCopy(
      this.mainCardsConfig['api']['request']
    );
    cardsReq['timeKeyFilter'] = this.libServ.deepCopy(
      this.filtersApplied['timeKeyFilter']
    );
    cardsReq['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);

    this.libServ.getDataFromDynamicAPI(apiUrl, cardsReq).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error - Contact to administator',
          detail: data
        });
        console.log(data['status_msg']);
        return;
      }
      if (!this.libServ.isEmptyObj(data['data'])) {
        this.cardsJson.map(o => {
          o['value'] = data['data'][0][o['field']];
        });
      } else {
        this.cardsJson.map(o => {
          o['value'] = 0;
        });
      }
      this.showCards = this.mainCardsConfig['display'];
    });
  }

  loadRevRpmDailyChart(params) {
    const params2 = this.libServ.deepCopy(params);
    this.revRpmDailyChartJson = {
      chartTypes: [{ key: 'bar', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'Gross Revenue',
            type: 'line',
            yAxisID: 'y-axis-0',

            borderColor: this.chartColors[0],
            fill: false,
            backgroundColor: this.chartColors[0],
            data: []
          },
          {
            label: 'RPM',
            type: 'bar',
            yAxisID: 'y-axis-1',

            borderColor: this.chartColors[1],
            fill: false,
            backgroundColor: this.chartColors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'TitanTV and Antenna Web: Gross revenue and RPM (Daily)'
        },
        legend: {
          display: true,
          // onClick: e => e.stopPropagation()
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: 'Gross Revenue ($)'
              },
              position: 'right',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', [0]);
                }
              }
              // scaleFontColor: "rgba(151,137,200,0.8)"
            },
            {
              id: 'y-axis-1',
              scaleLabel: {
                display: true,
                labelString: 'RPM ($)'
              },
              position: 'left',
              name: '2',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', []);
                }
              }
              // scaleFontColor: "rgba(151,187,205,0.8)"
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,

          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    this.showRevRpmDailyChart = false;
    this.showPageViewsDailyChart = false;
    this.noDataPageViewsDailyChart = false;

    this.dataFetchServ.getTableData(params).subscribe(data => {
      const chartData = data['data'];

      this.noDataRevRpmDailyChart = false;
      if (typeof chartData === 'undefined' || !chartData.length) {
        this.noDataRevRpmDailyChart = true;
        this.noDataPageViewsDailyChart = true;
        return;
      } else {
        const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));

        this.revRpmDailyChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );

        const revArr = [];
        const rpmArr = [];
        datesArr.forEach(time_key => {
          chartData.forEach(r => {
            if (r['time_key'] === time_key) {
              revArr.push(r['revenue']);
              rpmArr.push(r['rpm']);
            }
          });
        });
        this.revRpmDailyChartJson['chartData']['datasets'][0]['data'] = revArr;
        this.revRpmDailyChartJson['chartData']['datasets'][1]['data'] = rpmArr;
        setTimeout(() => {
          this.showRevRpmDailyChart = true;
        }, 0);
        this.loadPageviewsDailyChart(params2);
      }
    });
  }

  loadPageviewsDailyChart(params) {
    params['gidGroupBy'] = ['derived_station_rpt'];
    (params['dimensions'] = ['time_key', 'derived_station_rpt']),
      this.dataFetchServ.getTableData(params).subscribe(data => {
        const chartData = data['data'];
        if (typeof chartData === 'undefined' || !chartData.length) {
          this.noDataPageViewsDailyChart = true;
          return;
        } else {
          this.pageViewsDailyChartJson = {
            chartTypes: [{ key: 'bar', label: 'Line Chart', stacked: true }],
            chartData: {
              labels: [],
              datasets: [
                {
                  label: 'Gross Revenue',
                  type: 'line',
                  yAxisID: 'y-axis-0',
                  borderColor: this.chartColors[0],
                  fill: false,
                  backgroundColor: this.chartColors[0],
                  data: []
                }
              ]
            },
            chartOptions: {
              title: {
                display: true,
                text: 'TitanTV and Antenna Web: Page Views (Daily)'
              },
              legend: {
                display: true,
                // onClick: e => e.stopPropagation()
              },
              scales: {
                xAxes: [
                  {
                    display: true,
                    scaleLabel: {
                      display: true,
                      labelString: 'Date'
                    }
                  }
                ],
                yAxes: [
                  {
                    id: 'y-axis-0',

                    scaleLabel: {
                      display: true,
                      labelString: 'Gross Revenue ($)'
                    },
                    position: 'right',
                    name: '1',
                    ticks: {
                      callback: (value, index, values) => {
                        return this.formatNumPipe.transform(value, '$', [0]);
                      }
                    }
                    // scaleFontColor: "rgba(151,137,200,0.8)"
                  },
                  {
                    id: 'y-axis-1',
                    stacked: true,
                    scaleLabel: {
                      display: true,
                      labelString: 'Page Views'
                    },
                    position: 'left',
                    name: '2',
                    ticks: {
                      callback: (value, index, values) => {
                        return this.formatNumPipe.transform(
                          value,
                          'number',
                          []
                        );
                      }
                    }
                    // scaleFontColor: "rgba(151,187,205,0.8)"
                  }
                ]
              },
              tooltips: {
                mode: 'index',
                intersect: false,

                callbacks: {
                  label: (tooltipItem, data) => {
                    const currentValue =
                      data.datasets[tooltipItem.datasetIndex].data[
                      tooltipItem.index
                      ];
                    if (tooltipItem.datasetIndex !== 0) {
                      return `${
                        data.datasets[tooltipItem.datasetIndex].label
                        } : ${this.formatNumPipe.transform(
                          currentValue,
                          'number',
                          []
                        )}`;
                    }
                    return `${
                      data.datasets[tooltipItem.datasetIndex].label
                      } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
                  }
                }
              },
              plugins: {
                datalabels: false
              }
            },
            zoomLabel: false,
            chartWidth: '',
            chartHeight: '300px'
          };

          // if (typeof chartData === 'undefined' || !chartData.length) {
          //   this.noDataPageViewsDailyChart = true;
          //   return;
          // }

          const datesArr = Array.from(
            new Set(chartData.map(r => r['time_key']))
          );

          this.pageViewsDailyChartJson['chartData'][
            'labels'
          ] = datesArr.map(d => moment(d, 'YYYYMMDD').format('MM-DD-YYYY'));

          const revArr = [];
          const pageViewsArr = {};
          for (const key in this.sourcesJson) {
            pageViewsArr[key] = [];
          }
          // const pageViewsantenawebArr = [];
          datesArr.forEach(time_key => {
            let sum = 0;
            chartData.forEach(r => {
              if (r['time_key'] === time_key) {
                sum = sum + r['revenue'];
                pageViewsArr[r['derived_station_rpt']].push(r['page_views']);
              }
            });
            revArr.push(sum);
          });
          this.pageViewsDailyChartJson['chartData']['datasets'][0][
            'data'
          ] = revArr;

          let i = 1;
          for (const key in this.sourcesJson) {
            this.pageViewsDailyChartJson['chartData']['datasets'].push({
              label: this.sourcesJson[key]['DisplayName'],
              type: 'bar',
              yAxisID: 'y-axis-1',
              borderColor: this.chartColors[i],
              fill: false,
              backgroundColor: this.chartColors[i],
              data: pageViewsArr[key]
            });
            i++;
          }
          setTimeout(() => {
            this.showPageViewsDailyChart = true;
          }, 0);
        }
      });
  }

  loadRevRpmMonthlyChart(params) {
    const params2 = this.libServ.deepCopy(params);
    this.revRpmMonthlyChartJson = {
      chartTypes: [{ key: 'bar', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'Gross Revenue',
            type: 'line',
            yAxisID: 'y-axis-0',

            borderColor: this.chartColors[0],
            fill: false,
            backgroundColor: this.chartColors[0],
            data: []
          },
          {
            label: 'RPM',
            type: 'bar',
            yAxisID: 'y-axis-1',

            borderColor: this.chartColors[1],
            fill: false,
            backgroundColor: this.chartColors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'TitanTV and Antenna Web: Gross revenue and RPM (Monthly)'
        },
        legend: {
          display: true,
          // onClick: e => e.stopPropagation()
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Month-Year'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: 'Gross Revenue ($)'
              },
              position: 'right',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', [0]);
                }
              }
              // scaleFontColor: "rgba(151,137,200,0.8)"
            },
            {
              id: 'y-axis-1',
              scaleLabel: {
                display: true,
                labelString: 'RPM ($)'
              },
              position: 'left',
              name: '2',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', []);
                }
              }
              // scaleFontColor: "rgba(151,187,205,0.8)"
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,

          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    this.showRevRpmMonthlyChart = false;
    this.showPageViewsMonthlyChart = false;
    this.noDataPageViewsMonthlyChart = false;

    this.dataFetchServ.getTableData(params).subscribe(data => {
      const chartData = data['data'];

      this.noDataRevRpmMonthlyChart = false;
      if (typeof chartData === 'undefined' || !chartData.length) {
        this.noDataRevRpmMonthlyChart = true;
        this.noDataPageViewsMonthlyChart = true;
        return;
      } else {
        const datesArr = Array.from(
          new Set(chartData.map(r => r['accounting_key']))
        );

        this.revRpmMonthlyChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MMM-YYYY')
        );

        const revArr = [];
        const rpmArr = [];
        datesArr.forEach(accounting_key => {
          chartData.forEach(r => {
            if (r['accounting_key'] === accounting_key) {
              revArr.push(r['revenue']);
              rpmArr.push(r['rpm']);
            }
          });
        });
        this.revRpmMonthlyChartJson['chartData']['datasets'][0][
          'data'
        ] = revArr;
        this.revRpmMonthlyChartJson['chartData']['datasets'][1][
          'data'
        ] = rpmArr;
        setTimeout(() => {
          this.showRevRpmMonthlyChart = true;
        }, 0);
        this.loadPageviewsMonthlyChart(params2);
      }
    });
  }

  loadPageviewsMonthlyChart(params) {
    params['gidGroupBy'] = ['derived_station_rpt'];
    (params['dimensions'] = ['accounting_key', 'derived_station_rpt']),
      this.dataFetchServ.getTableData(params).subscribe(data => {
        const chartData = data['data'];
        if (typeof chartData === 'undefined' || !chartData.length) {
          this.noDataPageViewsMonthlyChart = true;
          return;
        } else {
          this.pageViewsMonthlyChartJson = {
            chartTypes: [{ key: 'bar', label: 'Line Chart', stacked: true }],
            chartData: {
              labels: [],
              datasets: [
                {
                  label: 'Gross Revenue',
                  type: 'line',
                  yAxisID: 'y-axis-0',

                  borderColor: this.chartColors[0],
                  fill: false,
                  backgroundColor: this.chartColors[0],
                  data: []
                }
              ]
            },
            chartOptions: {
              title: {
                display: true,
                text:
                  'TitanTV and Antenna Web: Page Views with Gross Revenue (Monthly)'
              },
              legend: {
                display: true,
                // onClick: e => e.stopPropagation()
              },
              scales: {
                xAxes: [
                  {
                    display: true,
                    scaleLabel: {
                      display: true,
                      labelString: 'Month-Year'
                    }
                  }
                ],
                yAxes: [
                  {
                    id: 'y-axis-0',
                    scaleLabel: {
                      display: true,
                      labelString: 'Gross Revenue ($)'
                    },
                    position: 'right',
                    name: '1',
                    ticks: {
                      callback: (value, index, values) => {
                        return this.formatNumPipe.transform(value, '$', [0]);
                      }
                    }
                    // scaleFontColor: "rgba(151,137,200,0.8)"
                  },
                  {
                    id: 'y-axis-1',
                    stacked: true,
                    scaleLabel: {
                      display: true,
                      labelString: 'Page Views'
                    },
                    position: 'left',
                    name: '2',
                    ticks: {
                      callback: (value, index, values) => {
                        return this.formatNumPipe.transform(
                          value,
                          'number',
                          []
                        );
                      }
                    }
                    // scaleFontColor: "rgba(151,187,205,0.8)"
                  }
                ]
              },
              tooltips: {
                mode: 'index',
                intersect: false,

                callbacks: {
                  label: (tooltipItem, data) => {
                    const currentValue =
                      data.datasets[tooltipItem.datasetIndex].data[
                      tooltipItem.index
                      ];
                    if (tooltipItem.datasetIndex !== 0) {
                      return `${
                        data.datasets[tooltipItem.datasetIndex].label
                        } : ${this.formatNumPipe.transform(
                          currentValue,
                          'number',
                          []
                        )}`;
                    }
                    return `${
                      data.datasets[tooltipItem.datasetIndex].label
                      } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
                  }
                }
              },
              plugins: {
                datalabels: false
              }
            },
            zoomLabel: false,
            chartWidth: '',
            chartHeight: '300px'
          };

          // if (typeof chartData === 'undefined' || !chartData.length) {
          //   this.noDataPageViewsMonthlyChart = true;
          //   return;
          // }
          const datesArr = Array.from(
            new Set(chartData.map(r => r['accounting_key']))
          );

          this.pageViewsMonthlyChartJson['chartData'][
            'labels'
          ] = datesArr.map(d => moment(d, 'YYYYMMDD').format('MMM-YYYY'));

          const revArr = [];
          const pageViewsArr = {};
          for (const key in this.sourcesJson) {
            pageViewsArr[key] = [];
          }
          datesArr.forEach(accounting_key => {
            let sum = 0;
            chartData.forEach(r => {
              if (r['accounting_key'] === accounting_key) {
                sum = sum + r['revenue'];
                pageViewsArr[r['derived_station_rpt']].push(r['page_views']);
              }
            });

            revArr.push(sum);
          });
          this.pageViewsMonthlyChartJson['chartData']['datasets'][0][
            'data'
          ] = revArr;
          this.pageViewsMonthlyChartJson['chartData']['datasets'][0][
            'data'
          ] = revArr;

          let i = 1;
          for (const key in this.sourcesJson) {
            this.pageViewsMonthlyChartJson['chartData']['datasets'].push({
              label: this.sourcesJson[key]['DisplayName'],
              type: 'bar',
              yAxisID: 'y-axis-1',
              borderColor: this.chartColors[i],
              fill: false,
              backgroundColor: this.chartColors[i],
              data: pageViewsArr[key]
            });
            i++;
          }
          setTimeout(() => {
            this.showPageViewsMonthlyChart = true;
          }, 0);
        }
      });
  }

  chartSelected(data: Event) {
    console.log('Chart Selected', data);
  }

  /* ------------------------------- Table Code ------------------------------- */

  addSources(srcs) {
    this.aggTableColumnDef = this.libServ.deepCopy(
      this.aggTableColumnDef.slice(0, 5)
    );

    srcs.forEach((s: String) => {
      const src = s.toLowerCase().replace(/ /g, '_');
      if (this.sourcesJson.hasOwnProperty(s.toString())) {
        this.sourcesJson[s.toString()]['Columns'].forEach(element => {
          switch (element) {
            case src + '_page_views':
              this.aggTableColumnDef.push({
                field: src + '_page_views',
                displayName:
                  this.sourcesJson[s.toString()]['DisplayName'] + ' Page Views',
                format: 'number',
                width: '110',
                formatConfig: [],
                exportConfig: {
                  format: 'number',
                  styleinfo: {
                    thead: 'default',
                    tdata: 'white'
                  }
                },
                options: {
                  editable: false,
                  colSearch: false,
                  colSort: true,
                  resizable: true,
                  movable: true
                },
                footerTotal: 0
              });
              break;

            case src + '_revenue':
              this.aggTableColumnDef.push({
                field: src + '_revenue',
                displayName:
                  this.sourcesJson[s.toString()]['DisplayName'] +
                  ' Gross Revenue',
                width: '135',
                format: '$',
                exportConfig: {
                  format: 'currency',
                  styleinfo: {
                    thead: 'default',
                    tdata: 'white'
                  }
                },
                style: { color: '#007ad9' },
                formatConfig: [],
                options: {
                  editable: false,
                  colSearch: false,
                  colSort: true,
                  resizable: true,
                  movable: true
                },
                footerTotal: 0
              });
              break;

            case src + '_rpm':
              this.aggTableColumnDef.push({
                field: src + '_rpm',
                displayName:
                  this.sourcesJson[s.toString()]['DisplayName'] + ' RPM',
                format: '$',
                width: '110',
                exportConfig: {
                  format: 'currency',
                  styleinfo: {
                    thead: 'default',
                    tdata: 'white'
                  }
                },
                formatConfig: [],
                options: {
                  editable: false,
                  colSearch: false,
                  colSort: true,
                  resizable: true,
                  movable: true
                },
                footerTotal: 0
              });
              break;
          }
        });
      }
    });

    this.aggTableJson['columns'] = this.aggTableColumnDef.slice(5);
    this.aggTableJson['selectedColumns'] = this.aggTableColumnDef.slice(5);
  }

  onLazyLoadAggTable(e: Event) {
    console.log('Lazy Agg', e);
  }

  loadAggTable(reqJson) {
    const sourcesDatareq = {
      dimensions: ['derived_station_rpt', 'accounting_key'],
      metrics: ['revenue', 'page_views'],
      derived_metrics: ['rpm'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['derived_station_rpt'],
      orderBy: [],
      limit: '',
      offset: ''
    };

    // For dev purpose
    sourcesDatareq['gidGroupBy'] = ['derived_station_rpt'];

    const totalsRowreqSrc = {
      dimensions: ['derived_station_rpt'],
      metrics: ['revenue', 'page_views'],
      derived_metrics: ['rpm'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: ['derived_station_rpt'],
      orderBy: [],
      limit: '',
      offset: ''
    };

    // For dev purpose
    totalsRowreqSrc['gidGroupBy'] = ['derived_station_rpt'];

    this.dataFetchServ.getTableData(reqJson).subscribe(data => {
      this.dataFetchServ.getTableData(sourcesDatareq).subscribe(data1 => {
        if (data['status'] === 0) {
          this.noTableData = true;
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        } else {
          this.noTableData = false;
        }

        const totalsColData = data['data'];
        const srcWiseData = data1['data'];
        const srcs = Array.from(
          new Set(srcWiseData.map(s => s['derived_station_rpt']))
        );
        this.addSources(srcs);
        this.jumpToSource['totalSources'] = [];
        srcs.forEach(e => {
          this.jumpToSource['totalSources'].push({
            label: e,
            value: e
          });
        });
        // this.jumpToSource['selected'] = [srcs[0]];

        const map = this.dataMapping(totalsColData, srcWiseData);
        map.map(o => delete o['data']['time_key1']);

        this.aggTableData = map;
        this.aggTableJson['loading'] = true;

        this.dataFetchServ.getTableData(totalsRowreqSrc).subscribe(data1 => {
          const srcWiseData = data1['data'];

          srcWiseData.forEach(e => {
            const src = String(e['derived_station_rpt'])
              .toLowerCase()
              .replace(/ /g, '_');
            this.aggTableColumnDef.forEach(c => {
              if (src + '_revenue' === c['field']) {
                c['footerTotal'] = e['revenue'];
              } else if (src + '_page_views' === c['field']) {
                c['footerTotal'] = e['page_views'];
              } else if (src + '_rpm' === c['field']) {
                c['footerTotal'] = e['rpm'];
              }
            });
          });

          this.aggTableJson['frozenCols'][2]['footerTotal'] = this.cardsJson[0][
            'value'
          ];
          this.aggTableJson['frozenCols'][3]['footerTotal'] = this.cardsJson[1][
            'value'
          ];
          this.aggTableJson['frozenCols'][4]['footerTotal'] = this.cardsJson[2][
            'value'
          ];

          this.aggTableJson['totalRecords'] = map.length;
          this.aggTableJson['loading'] = false;
        });
      });
    });
  }

  onTableDrill(e: Event) {
    if (!e['node']['childLoaded']) {
      this.aggTableJson['loading'] = true;

      let time_key1 = moment(e['node']['data']['time_key'], 'YYYYMMDD')
        .startOf('month')
        .format('YYYYMMDD');
      if (time_key1 < this.filtersApplied['timeKeyFilter']['time_key1']) {
        time_key1 = this.filtersApplied['timeKeyFilter']['time_key1'];
      }
      let time_key2 = moment(e['node']['data']['time_key'], 'YYYYMMDD')
        .endOf('month')
        .format('YYYYMMDD');
      if (time_key2 > this.filtersApplied['timeKeyFilter']['time_key2']) {
        time_key2 = this.filtersApplied['timeKeyFilter']['time_key2'];
      }

      const timeKeyFilter = {
        time_key1: time_key1,
        time_key2: time_key2
      };

      const totalsColreq = {
        dimensions: ['time_key'],
        metrics: ['revenue', 'page_views'],
        derived_metrics: ['rpm'],
        timeKeyFilter: timeKeyFilter, // this.filtersApplied['timeKeyFilter'],
        filters: this.libServ.deepCopy(this.filtersApplied['filters']),
        groupByTimeKey: {
          key: ['time_key', 'accounting_key'],
          interval: 'daily'
        },
        gidGroupBy: ['derived_station_rpt'],
        orderBy: [],
        limit: '',
        offset: ''
      };

      totalsColreq['dimensions'] = ['accounting_key', 'time_key'];

      totalsColreq['gidGroupBy'] = ['derived_station_rpt'];

      // For dev purpose
      totalsColreq['gidGroupBy'] = ['derived_station_rpt'];

      const sourcesDatareq = {
        dimensions: ['derived_station_rpt', 'time_key', 'accounting_key'],
        metrics: ['revenue', 'page_views'],
        derived_metrics: ['rpm'],
        timeKeyFilter: timeKeyFilter, // this.filtersApplied['timeKeyFilter'],
        filters: this.libServ.deepCopy(this.filtersApplied['filters']),
        groupByTimeKey: {
          key: ['time_key', 'accounting_key'],
          interval: 'daily'
        },
        gidGroupBy: ['derived_station_rpt'],
        orderBy: [],
        limit: '',
        offset: ''
      };
      // For dev purpose
      sourcesDatareq['gidGroupBy'] = ['derived_station_rpt'];

      this.dataFetchServ.getTableData(totalsColreq).subscribe(data => {
        this.dataFetchServ.getTableData(sourcesDatareq).subscribe(data1 => {
          const totalsColData = data['data'];
          const srcWiseData = data1['data'];

          const map = this.dataMapping(totalsColData, srcWiseData);
          map.map(o => delete o['children']);
          map.map(o => delete o['data']['time_key']);

          this.aggTableJson['loading'] = false;

          e['node']['children'] = map;
          this.aggTableData = [...this.aggTableData];
          e['node']['childLoaded'] = true;
        });
      });
    }
  }

  dataMapping(totals: [], srcWise: []) {
    const arr = [];

    let tks = [];
    let timeKeyAcc = 'accounting_key';

    if (Array.from(new Set(totals.map(o => o['time_key'])))[0] === undefined) {
      tks = Array.from(new Set(totals.map(o => o['accounting_key'])));
    } else {
      tks = Array.from(new Set(totals.map(o => o['time_key'])));
      timeKeyAcc = 'time_key';
    }

    tks.forEach(t => {
      const obj = {
        data: {},
        children: [{ data: {} }]
      };
      totals.forEach(tot_obj => {
        if (tot_obj[timeKeyAcc] === t) {
          obj['data']['time_key'] = t;
          obj['data']['time_key1'] = t;
          obj['data']['revenue'] = tot_obj['revenue'];
          obj['data']['page_views'] = tot_obj['page_views'];
          obj['data']['rpm'] = tot_obj['rpm'];
        }
      });

      srcWise.forEach(s_obj => {
        if (s_obj[timeKeyAcc] === t) {
          const src = String(s_obj['derived_station_rpt'])
            .toLowerCase()
            .replace(/ /g, '_');

          obj['data'][src + '_revenue'] = s_obj['revenue'];
          obj['data'][src + '_page_views'] = s_obj['page_views'];
          obj['data'][src + '_rpm'] = s_obj['rpm'];
          // obj['data'][src + '_clicks'] = s_obj['clicks'];
          // obj['data'][src + '_fill_rate'] = s_obj['fill_rate'];
          // obj['data'][src + '_dfp_clicks'] = s_obj['dfp_clicks'];
          // obj['data'][src + '_dfp_ctr'] = s_obj['dfp_ctr'];
          // obj['data'][src + '_impressions'] = s_obj['impressions'];
          // obj['data'][src + '_source_impressions'] = s_obj['impressions'];
          // obj['data'][src + '_dfp_impressions'] = s_obj['dfp_impressions'];
          // obj['data'][src + '_total_clicks'] =s_obj['source_total_clicks'];
        }
      });
      arr.push(obj);
    });

    return <TreeNode[]>arr;
  }

  exportTable(table, fileFormat) {
    if (this.aggTableData.length == 0) {
      this.confirmationService.confirm({
        message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
        header: 'Information',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {

        },
        reject: () => {
          // this.msgs = [{severity:'info', summary:'Rejected', detail:'You have rejected'}];
        }
      });
    } else {
      if (this.exportRequest['sendEmail'].length > 0) {
        this.toastService.displayToast({
          severity: 'info',
          summary: 'Export Report',
          detail:
            'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
          life: 10000
        });
        const columnDefs = this.aggTableColumnDef;
        console.log('data', columnDefs);
        const totalsColreq = {
          dimensions: ['accounting_key'],
          metrics: ['revenue', 'page_views'],
          derived_metrics: ['rpm'],
          timeKeyFilter: this.filtersApplied['timeKeyFilter'],
          filters: this.libServ.deepCopy(this.filtersApplied['filters']),
          groupByTimeKey: {
            key: ['accounting_key'],
            interval: 'daily'
          },
          gidGroupBy: ['derived_station_rpt'],
          orderBy: [],
          limit: '',
          offset: '',
          isTable: true
        };
        // For dev purpose
        totalsColreq['gidGroupBy'] = ['derived_station_rpt'];

        const sourcesDatareq = {
          dimensions: ['derived_station_rpt', 'accounting_key'],
          metrics: ['revenue', 'page_views'],
          derived_metrics: ['rpm'],
          timeKeyFilter: this.filtersApplied['timeKeyFilter'],
          filters: this.libServ.deepCopy(this.filtersApplied['filters']),
          groupByTimeKey: {
            key: ['accounting_key'],
            interval: 'daily'
          },
          gidGroupBy: ['derived_station_rpt'],
          orderBy: [],
          limit: '',
          offset: '',
          isTable: true
        };
        // For dev purpose
        sourcesDatareq['gidGroupBy'] = ['derived_station_rpt'];

        const totalsDailyColreq = {
          dimensions: ['time_key'],
          metrics: ['revenue', 'page_views'],
          derived_metrics: ['rpm'],
          timeKeyFilter: this.filtersApplied['timeKeyFilter'],
          filters: this.libServ.deepCopy(this.filtersApplied['filters']),
          groupByTimeKey: {
            key: ['time_key', 'accounting_key'],
            interval: 'daily'
          },
          gidGroupBy: ['derived_station_rpt'],
          orderBy: [],
          limit: '',
          offset: '',
          isTable: true
        };

        // For dev purpose
        totalsDailyColreq['gidGroupBy'] = ['derived_station_rpt'];

        const sourcesDailyDatareq = {
          dimensions: ['derived_station_rpt', 'time_key'],
          metrics: ['revenue', 'page_views'],
          derived_metrics: ['rpm'],
          timeKeyFilter: this.filtersApplied['timeKeyFilter'],
          filters: this.libServ.deepCopy(this.filtersApplied['filters']),
          groupByTimeKey: {
            key: ['time_key', 'accounting_key'],
            interval: 'daily'
          },
          gidGroupBy: ['derived_station_rpt'],
          orderBy: [],
          limit: '',
          offset: '',
          isTable: true
        };

        // For dev purpose
        sourcesDailyDatareq['gidGroupBy'] = ['derived_station_rpt'];

        this.dataFetchServ.getTableData(totalsColreq).subscribe(data => {
          this.dataFetchServ.getTableData(sourcesDatareq).subscribe(data1 => {
            const totalsColData = data['data'];
            const srcWiseData = data1['data'];

            let mapMonthly = this.dataMapping(totalsColData, srcWiseData);
            // mapMonthly.map(o => delete o['data']['time_key']);
            mapMonthly = mapMonthly.map(o => o['data']);

            // mapMonthly.forEach((o: object) => {
            //   const tk = o['time_key1'];
            //   delete o['time_key'];

            //   o['time_key'] = moment(tk, 'YYYYMMDD').format('MMM-YYYY');
            // });
            this.dataFetchServ.getTableData(totalsDailyColreq).subscribe(data => {
              this.dataFetchServ
                .getTableData(sourcesDailyDatareq)
                .subscribe(data1 => {
                  const totalsDailyColData = data['data'];
                  const srcDailyWiseData = data1['data'];

                  let mapDaily = this.dataMapping(
                    totalsDailyColData,
                    srcDailyWiseData
                  );

                  // mapDaily.map(o => delete o['data']['time_key']);
                  mapDaily = mapDaily.map(o => o['data']);
                  // mapDaily.forEach((o: object) => {
                  //   const tk = o['time_key1'];
                  //   delete o['time_key1'];
                  //   o['time_key1'] = moment(tk, 'YYYYMMDD').format('MM-DD-YYYY');
                  // });
                  const totalObj = {};
                  columnDefs.forEach(ele => {
                    if ('footerTotal' in ele) {
                      totalObj[ele.field] = ele.footerTotal;
                    }
                  });
                  totalObj['page_views'] = this.cardsJson[0]['value'];
                  totalObj['revenue'] = this.cardsJson[1]['value'];
                  totalObj['rpm'] = this.cardsJson[2]['value'];
                  totalObj[columnDefs[0].field] = 'Total';
                  mapMonthly.push(totalObj);
                  mapDaily.push(totalObj);
                  const sheetDetailsarray = [];
                  const sheetDetails = {};
                  sheetDetails['columnDef'] = this.libServ
                    .deepCopy(columnDefs)
                    .splice(0, 1)
                    .concat(columnDefs.slice(2));
                  sheetDetails['data'] = mapMonthly;
                  sheetDetails['sheetName'] = 'Monthly Data Distribution';
                  sheetDetails['isRequest'] = false;
                  sheetDetails['request'] = {
                    url: '',
                    method: '',
                    param: {
                      timeKeyFilter: this.libServ.deepCopy(
                        this.filtersApplied['timeKeyFilter']
                      )
                    }
                  };
                  sheetDetails['disclaimer'] = [
                    {
                      position: 'bottom',
                      label: 'Note: Data present in the table may vary over a period of time.',
                      color: '#000000'
                    }
                  ];
                  sheetDetails['totalFooter'] = {
                    available: true,
                    custom: false
                  };
                  sheetDetails['tableTitle'] = {
                    available: false,
                    label: 'Monthly Data Distribution'
                  };
                  sheetDetails['image'] = [
                    {
                      available: true,
                      path: environment.exportConfig.exportLogo,
                      position: 'top'
                    }
                  ];

                  const sheetlineDetails = {};
                  sheetlineDetails['columnDef'] = this.libServ
                    .deepCopy(columnDefs)
                    .slice(1);
                  sheetlineDetails['data'] = mapDaily;
                  sheetlineDetails['sheetName'] = 'Daily Data Distribution';
                  sheetlineDetails['isRequest'] = false;
                  sheetlineDetails['request'] = {
                    url: '',
                    method: '',
                    param: {
                      timeKeyFilter: this.libServ.deepCopy(
                        this.filtersApplied['timeKeyFilter']
                      )
                    }
                  };
                  sheetlineDetails['disclaimer'] = [
                    {
                      position: 'bottom',
                      label: 'Note: Data present in the table may vary over a period of time.',
                      color: '#000000'
                    }
                  ];
                  sheetlineDetails['totalFooter'] = {
                    available: true,
                    custom: false
                  };
                  sheetlineDetails['tableTitle'] = {
                    available: false,
                    label: 'Daily Data Distribution'
                  };
                  sheetlineDetails['image'] = [
                    {
                      available: true,
                      path: environment.exportConfig.exportLogo,
                      position: 'top'
                    }
                  ];

                  sheetDetailsarray.push(sheetDetails, sheetlineDetails);
                  this.exportRequest['sheetDetails'] = <SheetDetails[]>(
                    sheetDetailsarray
                  );
                  this.exportRequest['fileName'] =
                    'RPM Trends Data ' +
                    moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
                  this.exportRequest['exportFormat'] = fileFormat;
                  this.exportRequest['exportConfig'] = environment.exportConfig;
                  // console.log('exportreport', this.exportRequest);
                  // return false;
                  this.dataFetchServ
                    .getExportReportData(this.exportRequest)
                    .subscribe(response => {
                      console.log(response);
                    });
                });
            });
          });
        });
      } else {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Export Report',
          detail:
            'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
          life: 10000
        });
      }
    }

    return false;
  }

  isHiddenColumn(col: Object) {
    return (
      this.aggTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }

  resetPagination(ttAgg) {
    if (typeof ttAgg !== 'undefined') {
      ttAgg.reset();
    }
  }

  jumpTo(table) {
    const body = table.containerViewChild.nativeElement.getElementsByClassName(
      'ui-treetable-scrollable-body'
    )[1];
    const scrollWidth = this.aggTableJson['columns']
      .slice(
        0,
        this.aggTableJson['columns'].findIndex(e =>
          e.displayName.includes(this.jumpToSource['selected'])
        )
      )
      .reduce((tot: any, v: any) => tot + parseInt(v.width, 10), 0);

    body.scrollLeft = scrollWidth;
  }

  /* --------------------- On Filter Apply Button Clicked --------------------- */

  onFiltersApplied(filterData: object) {
    this.filtersApplied['filters']['dimensions'] = [];

    // tslint:disable-next-line: forin
    for (const k in filterData['filter']['dimensions']) {
      if (filterData['filter']['dimensions'][k].length !== 0) {
        this.filtersApplied['filters']['dimensions'].push({
          key: k,
          values: filterData['filter']['dimensions'][k]
        });
      }
    }

    this.filtersApplied['groupby'] = filterData['groupby'];
    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];

    /* --------------------------------- CHARTS --------------------------------- */

    const revRpmDailyChartReq = {
      dimensions: ['time_key'],
      metrics: ['revenue', 'page_views'],
      derived_metrics: ['rpm'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [
        {
          key: 'time_key',
          opcode: 'asc'
        }
      ],
      limit: '',
      offset: ''
    };

    // For dev purpose
    revRpmDailyChartReq['gidGroupBy'] = ['derived_station_rpt', 'source'];

    const pageViewsDailyChartReq = {
      dimensions: ['time_key'],
      metrics: ['revenue', 'page_views'],
      derived_metrics: ['rpm'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [
        {
          key: 'time_key',
          opcode: 'asc'
        }
      ],
      limit: '',
      offset: ''
    };

    // For dev purpose
    pageViewsDailyChartReq['gidGroupBy'] = ['derived_station_rpt', 'source'];

    this.loadRevRpmDailyChart(revRpmDailyChartReq);

    pageViewsDailyChartReq['groupByTimeKey']['key'] = ['accounting_key'];
    revRpmDailyChartReq['groupByTimeKey']['key'] = ['accounting_key'];
    pageViewsDailyChartReq['dimensions'] = ['accounting_key'];
    revRpmDailyChartReq['dimensions'] = ['accounting_key'];
    pageViewsDailyChartReq['orderBy'] = [
      {
        key: 'accounting_key',
        opcode: 'asc'
      }
    ];
    revRpmDailyChartReq['orderBy'] = [
      {
        key: 'accounting_key',
        opcode: 'asc'
      }
    ];

    this.loadRevRpmMonthlyChart(pageViewsDailyChartReq);

    if (this.mainCardsConfig['display']) {
      this.loadCards();
    }

    /* ---------------------------------- TABLE --------------------------------- */
    this.aggreTableReq = {
      dimensions: ['accounting_key'],
      metrics: ['revenue', 'page_views'],
      derived_metrics: ['rpm'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [],
      limit: '',
      offset: ''
    };
    // For dev purpose
    this.aggreTableReq['gidGroupBy'] = ['derived_station_rpt', 'source'];
    this.loadAggTable(this.aggreTableReq);
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['name']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    } else if (type === 'ppt') {
      const chartElements = document.getElementsByTagName('ym-charts');
      const slides = [];
      [].forEach.call(chartElements, (el, index) => {
        const c = el.getElementsByTagName('canvas')[0];

        slides.push({
          title: `${this.appConfig['name']}`,
          base64data: this.libServ.canvasToImage('white', c),
          width: 8,
          height: 3
        });

        if (index === chartElements.length - 1) {
          const pptConfig = {
            filename: 'Charts',
            logo: {
              visible: true,
              path: '/assets/images/ym.png',
              width: 0.53125,
              height: 0.458333
            },
            slides: slides
          };
          this.pptExport.exportPPT(pptConfig);
        }
      });
    }
    return false;
  }

  /* -------------------------------------------------------------------------- */

  customTooltips = function (tooltip) {
    // Tooltip Element
    let tooltipEl = document.getElementById('chartjs-tooltip');
    if (!tooltipEl) {
      tooltipEl = document.createElement('div');
      tooltipEl.id = 'chartjs-tooltip';
      tooltipEl.innerHTML = '<table></table>';
      this._chart.canvas.parentNode.appendChild(tooltipEl);
    }
    // Hide if no tooltip
    if (tooltip.opacity === 0) {
      tooltipEl.style.opacity = 0 as any;
      return;
    }
    // Set caret Position
    tooltipEl.classList.remove('above', 'below', 'no-transform');
    if (tooltip.yAlign) {
      tooltipEl.classList.add(tooltip.yAlign);
    } else {
      tooltipEl.classList.add('no-transform');
    }
    function getBody(bodyItem) {
      return bodyItem.lines;
    }
    // Set Text
    if (tooltip.body) {
      const titleLines = tooltip.title || [];
      const bodyLines = tooltip.body.map(getBody);
      let innerHtml = '<thead>';
      titleLines.forEach(function (title) {
        innerHtml += '<tr><th>' + title + '</th></tr>';
      });
      innerHtml += '</thead><tbody>';
      bodyLines.forEach(function (body, i) {
        const colors = tooltip.labelColors[i];
        let style = 'background:' + colors.backgroundColor;
        style += '; border-color:' + colors.borderColor;
        style += '; border-width: 2px';
        const span =
          '<span class="chartjs-tooltip-key" style="' + style + '"></span>';
        innerHtml += '<tr><td>' + span + body + '</td></tr>';
      });
      innerHtml += '</tbody>';
      const tableRoot = tooltipEl.querySelector('table');
      tableRoot.innerHTML = innerHtml;
    }
    const positionY = this._chart.canvas.offsetTop;
    const positionX = this._chart.canvas.offsetLeft;
    // Display, position, and set styles for font
    tooltipEl.style.opacity = 1 as any;
    tooltipEl.style.left = positionX + tooltip.caretX + 'px';
    tooltipEl.style.top = positionY + tooltip.caretY + 'px';
    tooltipEl.style.fontFamily = tooltip._bodyFontFamily;
    tooltipEl.style.fontSize = tooltip.bodyFontSize + 'px';
    tooltipEl.style.fontStyle = tooltip._bodyFontStyle;
    tooltipEl.style.padding =
      tooltip.yPadding + 'px ' + tooltip.xPadding + 'px';
  };

  /* -------------------------------------------------------------------------- */

  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }

  sendEmail() {
    const imageConfig = {
      imgName: `${this.appConfig['name']}.png`,
      type: 'image/png'
    };

    this.htmltoimage
      .canvasImage(document.getElementById('capture'), imageConfig)
      .then(canvas => {
        this.dialogService.open(SendEmailComponent, {
          header: 'Email Screenshot',
          contentStyle: {
            'max-height': '80vh',
            width: '30vw',
            overflow: 'auto'
          },
          data: canvas.toDataURL('image/png')
        });
      });
  }
}
