import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RpmAppRoutingModule } from './rpm-app-routing.module';
import { RpmAppComponent } from './rpm-app.component';

import { FilterContainerModule } from '../common/filter-container/filter-container.module';
import { CardsModule } from '../common/cards/cards.module';
import { ChartsModule } from '../common/charts/charts.module';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { SharedModule } from 'src/app/_pipes/shared.module';
import { TreeTableModule } from 'primeng/treetable';
import { MultiSelectModule } from 'primeng/multiselect';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DropdownModule } from 'primeng/dropdown';
import { ConfirmDialogModule, ConfirmationService } from 'primeng';

@NgModule({
  declarations: [RpmAppComponent],
  imports: [
    CommonModule,
    RpmAppRoutingModule,
    FilterContainerModule,
    CardsModule,
    ChartsModule,
    SharedModule,
    TreeTableModule,
    MultiSelectModule,
    FormsModule,
    ReactiveFormsModule,
    DropdownModule, ConfirmDialogModule
  ],
  providers: [FormatNumPipe, ConfirmationService]
})
export class RpmAppModule { }
