import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RpmAppComponent } from './rpm-app.component';

const routes: Routes = [
  {
    path: '',
    component: RpmAppComponent
    // data: {
    //   breadcrumb: 'Yield Management'
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RpmAppRoutingModule {}
