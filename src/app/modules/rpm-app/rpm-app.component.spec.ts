import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RpmAppComponent } from './rpm-app.component';

describe('RpmAppComponent', () => {
  let component: RpmAppComponent;
  let fixture: ComponentFixture<RpmAppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RpmAppComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RpmAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
