import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FetchApiDataService {
  private BASE_URL: string = environment.baseUrl;
  private options = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
  constructor(private http: HttpClient) { }

  getTableData(params: object) {
    const url = `${this.BASE_URL}/unified/v1/pit-issues-tracker/getData`;
    return this.http.post(url, params);
  }

  getAvailableCampaigns() {
    const url = `${this.BASE_URL}/unified/v1/pit-issues-tracker/getAvailableCampaigns`;
    return this.http.post(url, {});
  }

  checkCIDAvailable(params: String) {
    const url = `${this.BASE_URL}/unified/v1/pit-issues-tracker/checkCIDAvailable`;
    return this.http.post(url, params);
  }

  createIssue(params: object) {
    const url = `${this.BASE_URL}/unified/v1/pit-issues-tracker/createIssue`;
    return this.http.post(url, params);
  }

  deleteIssue(params: object) {
    const url = `${this.BASE_URL}/unified/v1/pit-issues-tracker/deleteIssue`;
    return this.http.post(url, params);
  }

  getExportReportData(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/common/exportData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
  }

  getCommentsCounts() {
    const url = `${this.BASE_URL}/frankly/v1/campaign-progress-report/getCommentsCountData`;
    return this.http.get(url);
  }

  getFavorite() {
    const url = `${this.BASE_URL}/frankly/v1/campaign-progress-report/getFavoriteData`;
    return this.http.get(url);
  }

  setFavourite(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/campaign-progress-report/set-favorite`;
    return this.http.post(url, params);
  }

  setFavouriteLine(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/campaign-progress-report/set-favorite-line`;
    return this.http.post(url, params);
  }

  getCommentsData() {
    const url = `${this.BASE_URL}/frankly/v1/campaign-progress-report/getCommentsData`;
    return this.http.get(url);
  }

  getCommentsCountData() {
    const url = `${this.BASE_URL}/frankly/v1/campaign-progress-report/getCommentsCountData`;
    return this.http.get(url);
  }

  getLastUpdatedData(appId) {
    const url = `${this.BASE_URL}/frankly/v1/common/getLastUpdatedData/${appId}`;
    return this.http.get(url);
  }
}
