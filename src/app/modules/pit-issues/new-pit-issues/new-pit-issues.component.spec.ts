import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPitIssuesComponent } from './new-pit-issues.component';

describe('DailyDataComponent', () => {
  let component: NewPitIssuesComponent;
  let fixture: ComponentFixture<NewPitIssuesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NewPitIssuesComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPitIssuesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
