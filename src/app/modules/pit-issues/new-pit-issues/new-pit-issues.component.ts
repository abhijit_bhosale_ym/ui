import { Component, OnInit } from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { FetchApiDataService } from '../fetch-api-data.service';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { FilterDataService } from '../../../_services/filter-data/filter-data.service'
import { DialogService } from 'primeng/dynamicdialog';
import { SelectItem } from 'primeng/api/selectitem';

@Component({
  selector: 'ym-new-pit-issues',
  templateUrl: './new-pit-issues.component.html',
  styleUrls: ['./new-pit-issues.component.scss']
})
export class NewPitIssuesComponent implements OnInit {
  existingCampaignCheckboxFlag = true;
  descLimitInComment = 50;
  isCIDValid = true;
  campaignError = "";
  newIssueOnAddClicked = true;
  dummyCampaignName = "";
  selectedCampaign: Object = {};
  allAvailableCampaigns: any[];
  isEdit: boolean = false;
  typesPriority: SelectItem[];
  typesStatus: SelectItem[];
  selectedPriorityType: string;
  selectedStatusType: string;
  issueDesc: string;
  rowData: object;
  server_campaign_id = 'non_existing';
  constructor(
    private libServ: CommonLibService,
    private formatNumPipe: FormatNumPipe,
    private dataFetchServ: FetchApiDataService,
    private config: DynamicDialogConfig,
    private toastService: ToastService,
    private filterDataService: FilterDataService,
    private dialogService: DialogService,
    private dynamicDialogRef: DynamicDialogRef
  ) {
    this.isEdit = this.config.data['isEdit'];
    if (this.isEdit) {
      this.rowData = this.config.data['data'];
    }
  }

  ngOnInit() {
    this.typesPriority = [
      { label: 'REGULAR', value: 'regular' },
      { label: 'URGENT', value: 'urgent' }
    ];
    this.typesStatus = [
      { label: 'OPEN', value: 'open' },
      { label: 'CLOSED', value: 'closed' }
    ];
    if (!this.isEdit) {
      this.newIssueOnAddClicked = true;
      this.selectedPriorityType = 'regular';
      this.selectedStatusType = 'open';
      this.isCIDValid = false;
      this.getFilters();
    } else {
      if (this.rowData['server_campaign_id'] == 'non_existing') {
        this.existingCampaignCheckboxFlag = false;
      } else {
        this.existingCampaignCheckboxFlag = true;
      }
      this.newIssueOnAddClicked = false;
      this.selectedPriorityType = this.rowData['priority'].toLowerCase();
      this.selectedStatusType = this.rowData['status'].toLowerCase();
      this.issueDesc = this.rowData['issue_desc'];
      this.dummyCampaignName = this.rowData['campaign'];
      this.server_campaign_id = this.rowData['server_campaign_id'];
      this.selectedCampaign['server_campaign_id'] = this.rowData['server_campaign_id'];
    }    
  }

  getFilters() {
    this.dataFetchServ.getAvailableCampaigns().subscribe((data) => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      }
      this.allAvailableCampaigns = data['data'];
    });
  }


  changeCampaign() {
    let server_campaign_id = this.selectedCampaign['server_campaign_id'];
    this.dataFetchServ.checkCIDAvailable(server_campaign_id).subscribe(data => {
      let comData = data['data'] as [];
      if (comData.length > 0) {
        this.isCIDValid = false;
        this.campaignError = "This Campaign already exists"
      } else {
        this.isCIDValid = true;
        this.campaignError = "";
      }
    });
  }

  createUpdateIssues(value) {
    let params ={};
    if (this.existingCampaignCheckboxFlag && this.selectedCampaign['server_campaign_id'] !== 'non_existing' && this.selectedCampaign['server_campaign_id'] !== '' && this.selectedCampaign['server_campaign_id'] !== 'N/A') {
      params = {
        server_campaign_id: this.selectedCampaign['server_campaign_id'],
        issue_desc: this.issueDesc,
        status: this.selectedStatusType,
        priority: this.selectedPriorityType,
        existing_campaign: true
      }
    } else {
      params = {
        dummy_campaign_name : this.dummyCampaignName,
        server_campaign_id: this.server_campaign_id,
        issue_desc: this.issueDesc,
        status: this.selectedStatusType,
        priority: this.selectedPriorityType,
        existing_campaign: false
      }
      this.existingCampaignCheckboxFlag = false
    }
    if(value == 'update'){
      params = Object.assign(params, { issue_id: this.rowData['issue_id'] });
    }

    this.dataFetchServ.createIssue(params).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      }else {
        this.toastService.displayToast({
          severity: 'success',
          summary: 'Issue Create',
          detail: 'Issue Created Successfully'
        });
      }
    })
  }

  goBack() {
    this.dialogService.dialogComponentRef.destroy();
  }

  existingCampaignIssueToggle()
  {
    this.isCIDValid = !this.isCIDValid;
    console.log("demo called", this.selectedCampaign,this.isCIDValid,this.dummyCampaignName);
    
  }
}
