import { Routes } from '@angular/router';

import { PitIssuesAppComponent } from './pit-issues-app.component';
export const PitIssuesAppRoutes: Routes = [
  {
    path: '',
    component: PitIssuesAppComponent
  }
];
