import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PitIssuesAppComponent } from './pit-issues-app.component';

describe('CprComponent', () => {
  let component: PitIssuesAppComponent;
  let fixture: ComponentFixture<PitIssuesAppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PitIssuesAppComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PitIssuesAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
