import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, from } from 'rxjs';
import { Title } from '@angular/platform-browser';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import * as moment from 'moment';
import { CommonLibService, AppConfigService } from 'src/app/_services';
import { FetchApiDataService } from './fetch-api-data.service';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { NewPitIssuesComponent } from './new-pit-issues/new-pit-issues.component';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { ChatComponent } from '../common/chat/chat/chat.component';
import { CommentJson } from 'src/app/_interfaces/commentJson';
import { CommentService } from '../common/chat/comment.service';

@Component({
  selector: 'ym-pit-issues-app',
  templateUrl: './pit-issues-app.component.html',
  styleUrls: ['./pit-issues-app.component.scss']
})
export class PitIssuesAppComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;

  appConfig: object = {};
  lastUpdatedOn: Date;
  favoriteCollection = {};
  filtersApplied: object = {};
  allStatus: any;
  messages = [];
  unreadCount = 0;
  nextUpdated: Date;
  dataUpdatedThrough: Date;
  exportRequest: ExportRequest = <ExportRequest>{};
  flatTableReq: object;
  flatTableData: TreeNode[];
  flatTableColumnDef: any[];
  flatTableJson: Object;
  tableData: any[];
  displayAggTable = true;
  noTableData = false;
  tremorType = true;
  issueType = true;
  openIssueToggleFlag = 'open';
  tremorToggleFlag = 'tremor';
  commentData = [];
  chatcount = 0;
  commentJson: CommentJson = <CommentJson>{};
  bellIconLoaded = false;
  isExportReport = false;
  tableReq = {};
  searchvalue = "";
  constructor(
    private confirmationService: ConfirmationService,
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private toastService: ToastService,
    private dialogService: DialogService,
    private formatNumPipe: FormatNumPipe,
    private dataFetchServ: FetchApiDataService,
    private commentservice: CommentService
  ) {}

  ngOnInit() {

    this.flatTableColumnDef = [
      {
        field: 'server_campaign_id',
        displayName: 'CID',
        format: '',
        width: '70',
        value: '',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'campaign',
        displayName: 'Campaign Name',
        format: '',
        width: '200',
        value: '',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'flight_end_id',
        displayName: 'Flight End Date',
        format: '',
        width: '80',
        value: '',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'team_members',
        displayName: 'Primary Analyst',
        format: '',
        width: '120',
        value: '',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'bizops_team_members',
        displayName: 'Biz Ops',
        format: '',
        width: '120',
        value: '',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'trafficking_delivery_progress',
        displayName: 'Pacing (Beeswax)',
        format: 'percentage',
        width: '100',
        value: '',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'trafficking_total_vcr',
        displayName: 'VCR (Beeswax)',
        format: 'percentage',
        width: '100',
        value: '',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      }, {
        field: 'trafficking_total_ctr',
        displayName: 'CTR (Beeswax)',
        format: 'percentage',
        width: '100',
        value: '',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'c_budget_cost',
        displayName: 'IO Budget ($)',
        format: '$',
        width: '120',
        value: '',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'dropbox_url',
        displayName: 'Dealsheet Link',
        format: '',
        width: '80',
        value: '',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'issue_desc',
        displayName: 'Issue Description',
        format: '',
        width: '200',
        value: '',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'comments',
        displayName: 'Comments',
        format: '',
        width: '70',
        value: '',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'priority',
        displayName: 'Priority',
        format: '',
        width: '70',
        value: 'string',
        exportConfig: {
          format: '',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'status',
        displayName: 'Status',
        format: '',
        width: '100',
        value: '',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'created_at',
        displayName: 'Since First Entry',
        format: '',
        width: '150',
        value: '',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'edit',
        displayName: 'Edit Issue',
        format: '',
        width: '45',
        value: '',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'delete',
        displayName: 'Delete Issue',
        format: '',
        width: '55',
        value: '',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: false
        }
      }
    ];

    this.flatTableJson = {
      page_size: 10,
      page: 1,
      lazy: false,
      loading: true,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '300px',
      totalRecords: 1000,
      columns: this.libServ.deepCopy(this.flatTableColumnDef.slice(3)),
      selectedColumns: this.libServ.deepCopy(this.flatTableColumnDef.slice(3)),
      frozenCols: this.libServ.deepCopy([...this.flatTableColumnDef.slice(0, 3)]),
      frozenWidth:
        this.flatTableColumnDef
          .slice(0, 3)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      globalFilterFields: this.flatTableColumnDef.slice(0),
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: true
    };

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        console.log("appconfig", appConfig);
        this.isExportReport = this.appConfig['permissions'].some(
          o => o.name === 'pit-issues-export-reports'
        );
        this._titleService.setTitle(this.appConfig['displayName']);
        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');

        this.commentJson['app_id'] = this.appConfig['id'];
        this.commentJson['user_id'] = this.appConfig['user']['id'];
        this.commentJson['receiver'] = this.appConfig['route'];
        this.commentJson['user_name'] = this.appConfig['user']['userName'];
        this.initialLoading();
      }
    });
  }

  initialLoading() {
    this.UpdateBellIcon();
    // this.dataFetchServ
    // .getLastUpdatedData(this.appConfig['id'])
    // .subscribe(data => {
    //   if (data['status'] === 0) {
    //     this.toastService.displayToast({
    //       severity: 'error',
    //       summary: 'Server Error',
    //       detail: 'Please refresh the page'
    //     });
    //     return;
    //   }
    //   this.lastUpdatedOn = moment(data[0]['updated_at']).toDate();
    //   this.nextUpdated = moment(data[0]['next_run_at']).toDate();
    //   this.dataUpdatedThrough = moment(
    //     data[0]['source_updated_through'],
    //     'YYYYMMDD'
    //   ).toDate();
    // });

    this.tableReq = {
      account_type: 'tremor',
      status: 'open'
    }
    this.loadTableData(this.tableReq);
  }

  UpdateBellIcon() {
    const req1 = {
      user_id: this.appConfig['user']['id'],
      app_id: this.appConfig['id']
    };

    this.commentservice
      .getAppBellCommentData(req1)
      .subscribe(commentData => {
        if (commentData['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          return;
        }
        this.bellIconLoaded = false;
        this.commentData = commentData['data'];        
        this.chatcount = this.commentData.filter(x => x.is_read === 'N').length;
        this.bellIconLoaded = false;
        setTimeout(() => {
          this.bellIconLoaded = true;
        }, 0);
     });
    }

  clickOnComment(comment: object) {
    console.log('comment......', comment);
    const req1 = {
      user_id: this.appConfig['user']['id'],
      app_id: this.appConfig['id']
    };

    this.commentservice
      .getAppCommentData(req1)
      .subscribe(commentData => {
        if (commentData['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          return;
        }
        // this.commentData = commentData['data'];
        if (typeof comment['config'] == 'string') {
          this.commentJson['config'] = JSON.parse(comment['config']);
        } else {
          this.commentJson['config'] = comment['config'];
        }

        const data = {};
        const CommentSortData = [];
        commentData['data'].forEach(element => {
          const commentconfig = JSON.parse(element.config);
          if (this.commentJson['config']['id'] == commentconfig.id && this.commentJson['config']['type'] == commentconfig.type) {
            CommentSortData.push(element);
          }
        });
        data['commentJson'] = this.commentJson;
        data['commentData'] = CommentSortData;
        const chatSetting = {
          showChatMsg: false,
          title: `${this.commentJson['config']['comment_on']}`,
          showCommentTextArea: true,
          chatURL: '/cpr/send/message',
          data: data
        };
        console.log('chatSetting', chatSetting);

        const ref = this.dialogService.open(ChatComponent, {
          header: `Comment on ${this.commentJson['config']['comment_on']}`,
          contentStyle: { width: '30vw', height: '370px', overflow: 'auto' },
          data: chatSetting,
          width: '30vw'
        });

        ref.onClose.subscribe((data: string) => {
          // let UnreadCommentIdList = CommentSortData.filter(o => o.is_read == "N" && o.user_id != this.commentJson['user_id']).map(x => x.comment_id);

          // console.log('popup close', CommentSortData, UnreadCommentIdList);
          // if (UnreadCommentIdList.length > 0) {
          this.UpdateBellIcon();
          // }
        });
      });

  }

  addComment(row) {
    const req1 = {
      user_id: this.appConfig['user']['id'],
      app_id: this.appConfig['id']
    };

    this.commentservice
      .getAppCommentData(req1)
      .subscribe(commentData => {
        if (commentData['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          return;
        }
        const data = {};
        const CommentSortData = [];
        const config = {
          comment_on: 'campaign',
          id: row['server_campaign_id'],
          campaign : row['campaign']
        };
        commentData['data'].forEach(element => {
          const commentconfig = JSON.parse(element.config);
          if (config.id == commentconfig.id && config.campaign == commentconfig.campaign) {
            CommentSortData.push(element);
          }
        });

        this.commentJson['config'] = config;

        data['commentJson'] = this.commentJson;
        data['commentData'] = CommentSortData;
        const chatSetting = {
          showChatMsg: false,
          title: row['campaign'],
          showCommentTextArea: true,
          chatURL: '/cpr/send/message',
          data: data
        };

        const ref = this.dialogService.open(ChatComponent, {
          header: `Comment on ${row['campaign']}`,
          contentStyle: { width: '30vw', height: '370px', overflow: 'auto' },
          data: chatSetting,
          width: '30vw'
        });

        ref.onClose.subscribe((data: string) => {
          // let UnreadCommentIdList = CommentSortData.filter(o => o.is_read == "N" && o.user_id != this.commentJson['user_id']).map(x => x.comment_id);
          // console.log('popup close', CommentSortData,UnreadCommentIdList);
          // if (UnreadCommentIdList.length > 0) {
          this.UpdateBellIcon();
          // }
        });
      });

  }


  loadTableData(params) {
    this.flatTableJson['loading'] = true;
    this.dataFetchServ.getTableData(params).subscribe(data => {
      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      } else {
        this.noTableData = false;
      }
      const tableData = data['data'];
      this.tableData = data['data'];
      const arr = [];
      for (const r of tableData) {
        r['created_at'] = moment.tz(r['created_at'], "YYYY-MM-DD H:mm:ss", "America/New_York").tz(moment.tz.guess()).fromNow();
        r['status'] = r['status'].toUpperCase();
        r['priority'] = r['priority'].toUpperCase();
        const obj = {};
        obj['data'] = r;
        arr.push(obj);
      }
      this.flatTableData = <TreeNode[]>arr;
      this.flatTableJson['totalRecords'] = tableData.length;
      this.flatTableJson['loading'] = false;
    })
  }

  progressTemplateClass(row, col) {
    const regexp = /^[0-9]+(.[0-9]{1,9})?$/;
    let tagClass = 'badge badge-secondary ';
    if (regexp.test(row['campaign_duration_progress'])) {
      const cellValue = parseFloat(row[col]) - parseFloat(row['campaign_duration_progress']);
      if (col === 'trafficking_delivery_progress' && parseFloat(row[col]) === 0.00) tagClass = 'badge badge-secondary';
      else if (cellValue === 0.00) tagClass = 'badge badge-secondary';
      else if (cellValue < -5.00) tagClass = 'badge badge-danger';
      else if (cellValue >= -5.00 && cellValue < -3.00) tagClass = 'badge badge-warning';
      else if (cellValue >= -3.00 && cellValue < 5.00) tagClass = 'badge badge-success';
      else if (cellValue >= 5.00) tagClass = 'badge badge-primary';
      return tagClass;
  }
  return tagClass;
  }

 vcrTemplateClass(row, col){
    const cellValue = parseFloat(row[col]);
    let tagClass = 'badge badge-secondary';
    if(row['trafficking_total_vcr'] !== ''){
      if (cellValue === 0) tagClass = 'badge badge-secondary';
      else if (cellValue > 0 && cellValue < 80.00) tagClass = 'badge badge-danger';
      else if (cellValue >= 80.00 && cellValue < 85.00) tagClass = 'badge badge-warning';
      else if (cellValue >= 85.00) tagClass = 'badge badge-primary';
      else tagClass = 'badge badge-secondary'; 
    } else {
      row['trafficking_total_vcr']='N/A';
    }
    return tagClass; 
};

  isHiddenColumn(col: Object) {
    return (
      this.flatTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.flatTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }

  NewIssues() {
    const data = {
      isEdit: false
    };
    const ref = this.dialogService.open(NewPitIssuesComponent, {
      data: data,
      header: 'Add New Issue',
      contentStyle: { width: '48vw', height: '520px', overflow: 'auto' },
    });
    ref.onClose.subscribe((data: string) => {
      this.loadTableData(this.tableReq);
    })
  }
  changeOpenIssueToggleFlag() {
    this.openIssueToggleFlag = (this.openIssueToggleFlag === 'open') ? 'all' : 'open';
    this.tableReq['status'] = this.openIssueToggleFlag
    this.loadTableData(this.tableReq);
  }

  changetremorToggleFlag() {
    this.tremorToggleFlag = (this.tremorToggleFlag === 'non_tremor') ? 'tremor' : 'non_tremor';
    this.tableReq['account_type'] = this.tremorToggleFlag;
    this.loadTableData(this.tableReq);
  }

  exportTable(fileFormat) {
    if (this.flatTableData.length == 0) {
      this.confirmationService.confirm({
        message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
        header: 'Information',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
        },
        reject: () => {
        }
      });
    } else {
      if (this.exportRequest['sendEmail'].length > 0) {
        this.toastService.displayToast({
          severity: 'info',
          summary: 'Export Report',
          detail:
            'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
          life: 10000
        });

        const finalColDef = this.libServ.deepCopy(this.flatTableColumnDef);
        const sheetDetailsarray = [];
        const sheetDetails = {};
        sheetDetails['columnDef'] = finalColDef;
        sheetDetails['data'] = this.tableData;
        sheetDetails['sheetName'] = 'PIT Issues Data';
        sheetDetails['isRequest'] = false;
        sheetDetails['request'] = {
          url: '',
          method: '',
          param: ''
        };
        sheetDetails['disclaimer'] = [
          {
            position: 'top',
            label:
              'Data persent in below table is not final and may varies over period of time',
            color: '#3A37CF'
          },
          {
            position: 'bottom',
            label: 'Thank You',
            color: '#EC6A15' 
          }
        ];
        sheetDetails['totalFooter'] = {
          available: false,
          custom: false
        };
        sheetDetails['tableTitle'] = {
          available: true,
          label: 'PIT Issues '
        };
        sheetDetails['image'] = [
          {
            available: true,
            path: 'ExportImages/yuktaone-logo.png',
            position: 'top'
          }
        ];
        sheetDetailsarray.push(sheetDetails);
        this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
        this.exportRequest['fileName'] =
          'PIT Issues ' + moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
        this.exportRequest['exportFormat'] = fileFormat;
        this.dataFetchServ
          .getExportReportData(this.exportRequest)
          .subscribe(response => {
            console.log(response);
          });
      }
    }
  }

  deleteIssues(rowData) {
    const params={
      issue_id: rowData['issue_id'],
      existing_campaign :(rowData['server_campaign_id'] !== 'non_existing' && rowData['server_campaign_id'] !== '')
    }
    this.dataFetchServ.deleteIssue(params).subscribe(data => {
      if (data['status'] == 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      } else {
        this.toastService.displayToast({
          severity: 'success',
          summary: 'Delete Issue',
          detail: 'Issue Deleted Successfully'
        });
        this.loadTableData(this.tableReq);
      }
    });

  }

  editIssues(rowData) {
    const data = {
      isEdit: true,
      data: rowData
    };
    const ref = this.dialogService.open(NewPitIssuesComponent, {
      data: data,
      header: 'Update Issue',
      contentStyle: { width: '48vw', height: '520px', overflow: 'auto' },
    });
    ref.onClose.subscribe((data: string) => {
      this.loadTableData(this.tableReq);
    })
  }

  ConvertToColor(value, field) {
    let color = '';
    if (field == 'priority') {
      switch (value.toLowerCase()) {
        case "urgent":
          {
            return 'red';
          }
          break;
        case "regular":
          {
            return '#0275d8';
          }
          break;
        case "default":
          {
            return 'tag-default';
          }
          break
      }
    }
  }

  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }
}
