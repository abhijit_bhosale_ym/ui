import { Component, OnInit, OnDestroy } from '@angular/core';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { CommonLibService } from 'src/app/_services';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';
import { FetchApiDataService } from './fetch-api-data.service';
import { Rev360AsbnDailyDataPopupComponent } from './rev360-asbn-daily-data-popup/rev360-asbn-daily-data-popup.component';
import { Rev360AsbnExportPopupComponent } from './rev360-asbn-export-popup/rev360-asbn-export-popup.component';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { Subscription } from 'rxjs';
import { SendEmailComponent } from '../send-email/send-email.component';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';

@Component({
  selector: 'ym-rev360-asbn-app',
  templateUrl: './rev360-asbn-app.component.html',
  styleUrls: ['./rev360-asbn-app.component.scss']
})
export class Rev360AsbnAppComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;
  private appConfigObs1: Subscription;
  private appConfigObs2: Subscription;
  appConfig: object = {};
  lastUpdatedOn: Date;

  cardsJson = [];
  showCards = true;

  aggTableData: TreeNode[];
  noTableData = false;
  dimColDef: any[];
  metricColDef: any[];
  aggTableColumnDef: any[];
  aggTableJson: Object;
  displayAggTable = false;
  toggleView = true;
  filtersApplied: object = {};
  exportRequest: ExportRequest = <ExportRequest>{};
  showhide = true;
  tableReq: object;
  timeout: any;
  isExportReport = false;

  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private htmltoimage: HtmltoimageService,
    private exportService: ExportdataService,
    private pptExport: ExportPptService,
    private confirmationService: ConfirmationService
  ) { }

  ngOnInit() {
    this.cardsJson = [
      {
        field: 'impressions',
        displayName: 'Billable Imps.',
        value: 0,
        imageClass: 'fas fa-eye',
        format: 'number',
        config: [],
        // metaConfig: {
        //   icon: 'fas fa-arrow-down',
        //   avgValue: ''
        // },
        color: '#5cb85c'
      },
      {
        field: 'revenue',
        displayName: 'Revenue',
        value: 0,
        imageClass: 'fas fa-hand-holding-usd',
        format: '$',
        config: [],
        // metaConfig: {
        //   icon: 'fas fa-arrow-down',
        //   avgValue: ''
        // },
        color: '#5bc0de'
      },
      {
        field: 'cpm',
        displayName: 'eCPM',
        value: 0,
        imageClass: 'fas fa-dollar-sign',
        format: '$',
        config: [],
        // metaConfig: {
        //   icon: 'fas fa-arrow-down',
        //   avgValue: ''
        // },
        color: '#FFDC00'
      }
    ];

    this.dimColDef = [
      {
        field: 'source',
        displayName: 'Source',
        format: '',
        width: '180',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'media_group',
        displayName: 'Media Group',
        format: '',
        width: '180',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'property',
        displayName: 'Property',
        format: '',
        width: '180',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      }
    ];

    this.metricColDef = [
      {
        field: 'impressions',
        displayName: 'Billable Imps.',
        format: 'number',
        visible: false,
        width: '125',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      // {
      //   field: 'dfp_impressions',
      //   displayName: 'DFP Imps.',
      //   format: 'number',
      //   width: '170',
      //   exportConfig: {
      //     format: 'number',
      //     styleinfo: {
      //       thead: 'default',
      //       tdata: 'white'
      //     }
      //   },
      //   formatConfig: [],
      //   options: {
      //     editable: false,
      //     colSearch: false,
      //     colSort: true,
      //     resizable: true,
      //     movable: true
      //   },
      //   footerTotal: '-'
      // },
      {
        field: 'revenue',
        displayName: 'Revenue',
        width: '135',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'cpm',
        displayName: 'eCPM',
        format: '$',
        width: '120',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      }

      // {
      //   field: 'fill_rate',
      //   displayName: 'Fill Rate',
      //   format: 'percentage',
      //   width: '110',
      //   exportConfig: {
      //     format: 'percentage',
      //     styleinfo: {
      //       thead: 'default',
      //       tdata: 'white'
      //     }
      //   },
      //   formatConfig: [2],
      //   options: {
      //     editable: false,
      //     colSearch: false,
      //     colSort: true,
      //     resizable: true,
      //     movable: true
      //   }
      // },
    ];
    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      reload: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,

      // columns: this.aggTableColumnDef,
      // selectedColumns: this.aggTableColumnDef,
      // frozenCols: [],
      // frozenWidth: '0px',

      // columns: this.aggTableColumnDef.slice(5),
      // selectedColumns: this.aggTableColumnDef.slice(5),
      // frozenCols: [...this.aggTableColumnDef.slice(0, 5)],
      // frozenWidth:
      //   this.aggTableColumnDef
      //     .slice(0, 5)
      //     .reduce((tot, cur) => tot + parseInt(cur.width), 0) + 'px',

      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        console.log('appConfig', this.appConfig);
        this.isExportReport = this.appConfig['permissions'].some(
          o => o.name === 'adops360-asbn-export-reports'
        );
        this._titleService.setTitle( this.appConfig['displayName']);
        let startDate;
        let endDate;
        const date_config = this.appConfig['filter']['filterConfig']['filters'][
          'datePeriod'
        ][0];
        if ((date_config['defaultDate'][0]['value'] === 0 && date_config['defaultDate'][0]['period'] === 'month') && (moment().format('DD') === '01' || moment().format('DD') === '02')) {
          startDate = moment().subtract(1, 'months').startOf('month');
          endDate = moment().subtract(1, 'months').endOf('month');
        } else {
          if (
            date_config[
            'defaultDate'
            ][0]['startOf']
          ) {
            startDate = moment()
              .subtract(
                date_config['defaultDate'][0]['value'],
                date_config['defaultDate'][0]['period']
              )
              .startOf(
                date_config['defaultDate'][0]['period']
              );
          } else {
            startDate = moment().subtract(
              date_config['defaultDate'][0]['value'],
              date_config['defaultDate'][0]['period']
            );
          }
          endDate = moment().subtract(
            date_config[
            'defaultDate'
            ][1]['value'],
            date_config[
            'defaultDate'
            ][1]['period']
          );
        }

        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: startDate.format('YYYYMMDD'),
            time_key2: endDate.format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: this.appConfig['filter']['filterConfig']['groupBy'].filter(
            v => v.selected
          )
        };
        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        this.initialLoading();
      }
    });
  }

  initialLoading() {
    this.dataFetchServ
      .getLastUpdatedData(this.appConfig['id'])
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        this.lastUpdatedOn = moment(data).toDate();
      });

    this.tableReq = {
      dimensions: [],
      metrics: this.toggleView
        ? ['revenue', 'impressions', 'ad_requests']
        : [
          'revenue',
          'impressions',
          'house_ads_impressions',
          'unfilled_impressions',
          'total_code_served_count'
        ],
      derived_metrics: this.toggleView
        ? ['fill_rate', 'cpm']
        : ['cpm', 'fill_rate_house'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: { key: [], interval: 'daily' },
      gidGroupBy: [],
      orderBy: [
        { key: this.filtersApplied['groupby'][0]['key'], opcode: 'asc' }
      ],
      limit: '',
      offset: ''
    };
    this.loadTableData(this.tableReq);
    const cardsReq = {
      dimensions: [],
      metrics: ['revenue', 'impressions'],
      derived_metrics: ['cpm'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: { key: [], interval: 'daily' },
      gidGroupBy: ['source'],
      orderBy: [],
      limit: '',
      offset: '',
      isTable: false
    };
    this.loadCards(cardsReq);
  }

  resetPagination(tt1) {
    if (typeof tt1 !== 'undefined') {
      tt1.reset();
    }
  }

  reset(table) {
    table.reset();
    this.tableReq = {
      dimensions: [],
      metrics: this.toggleView
        ? ['revenue', 'impressions', 'ad_requests']
        : [
          'revenue',
          'impressions',

          'house_ads_impressions',
          'unfilled_impressions',
          'total_code_served_count'
        ],
      derived_metrics: this.toggleView
        ? [ 'fill_rate', 'cpm']
        : ['cpm', 'fill_rate_house'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: { key: [], interval: 'daily' },
      gidGroupBy: [],
      orderBy: [
        { key: this.filtersApplied['groupby'][0]['key'], opcode: 'asc' }
      ],
      limit: '',
      offset: ''
    };
    this.loadTableData(this.tableReq);
  }

  loadTableData(tableReq) {
    const finalColDef = this.libServ.deepCopy(this.metricColDef);

    if (this.toggleView) {
      if (
        !this.appConfig['filter']['filterConfig']['groupBy'].some(
          o => o.key == 'source'
        )
      ) {
        this.appConfig['filter']['filterConfig']['filters']['dimension'][
          'default'
        ].unshift({ key: 'source', label: 'Source', config_json: null });
        this.appConfig['filter']['filterConfig']['groupBy'].unshift({
          key: 'source',
          label: 'Source',
          selected: true
        });
        this.appConfigService.changeAppConfig(this.appConfig);
        this.showhide = false;
        setTimeout(() => {
          this.showhide = true;
        }, 1);
      }

      finalColDef.splice(
        finalColDef.length,
        0,
        {
          field: 'fill_rate',
          displayName: 'Fill Rate',
          format: 'percentage',
          width: '110',
          exportConfig: {
            format: 'percentage',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [2],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: true
          }
        }
      );
    } else {
      if (
        this.appConfig['filter']['filterConfig']['groupBy'].some(
          o => o.key == 'source'
        )
      ) {
        this.appConfig['filter']['filterConfig']['filters']['dimension'][
          'default'
        ].splice(0, 1);
        this.appConfig['filter']['filterConfig']['groupBy'].splice(0, 1);
        this.appConfigService.changeAppConfig(this.appConfig);
        this.showhide = false;
        setTimeout(() => {
          this.showhide = true;
        }, 1);
      }
      finalColDef.splice(
        finalColDef.length,
        0,
        // {
        //   field: 'fill_rate_house',
        //   displayName: 'Fill Rate',
        //   format: 'percentage',
        //   width: '110',
        //   exportConfig: {
        //     format: 'percentage',
        //     styleinfo: {
        //       thead: 'default',
        //       tdata: 'white'
        //     }
        //   },
        //   formatConfig: [2],
        //   options: {
        //     editable: false,
        //     colSearch: false,
        //     colSort: true,
        //     resizable: true,
        //     movable: true
        //   }
        // },
        {
          field: 'house_ads_impressions',
          displayName: 'House Ad',
          format: 'number',
          visible: false,
          width: '170',
          exportConfig: {
            format: 'number',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: true
          },
          footerTotal: '-'
        },
        {
          field: 'unfilled_impressions',
          displayName: 'Unfilled Imps.',
          format: 'number',
          visible: false,
          width: '170',
          exportConfig: {
            format: 'number',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: true
          },
          footerTotal: '-'
        }
      );
    }

    const grpBys = this.getGrpBys();

    tableReq['gidGroupBy'] = grpBys;
    tableReq['dimensions'] = [this.libServ.deepCopy(this.filtersApplied['groupby'][0]['key'])];
    tableReq['orderBy'] = [{ key: this.libServ.deepCopy(this.filtersApplied['groupby'][0]['key']), opcode: 'asc' }];
    tableReq['metrics'] = this.toggleView
      ? ['revenue', 'impressions', 'ad_requests']
      : [
        'revenue',
        'impressions',
        'house_ads_impressions',
        'unfilled_impressions',
        'total_code_served_count'
      ];
    tableReq['derived_metrics'] = this.toggleView
      ? [ 'fill_rate', 'cpm']
      : ['cpm', 'fill_rate_house'];

    const grpBysFilter = this.filtersApplied['groupby'].map(e => e.key);

    grpBysFilter.reverse().forEach(grp => {
      finalColDef.unshift(this.dimColDef.find(e => e.field === grp));
    });

    this.aggTableColumnDef = finalColDef.slice();

    this.aggTableJson['columns'] = this.aggTableColumnDef.slice(
      grpBysFilter.length
    );
    this.aggTableJson['selectedColumns'] = this.aggTableColumnDef.slice(
      grpBysFilter.length
    );
    this.aggTableJson['frozenCols'] = [
      ...this.aggTableColumnDef.slice(0, grpBysFilter.length)
    ];
    this.aggTableJson['frozenWidth'] =
      this.aggTableColumnDef
        .slice(0, grpBysFilter.length)
        .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px';

    this.reloadAggTable();

    this.aggTableJson['loading'] = true;
    // For Dev purpose
    // tableReq['gidGroupBy'] = [
    //   'ad_size',
    //   'ad_type',
    //   'ad_units',
    //   'bidding_type',
    //   'device_category',
    //   'geography',
    //   'sales_channel',
    //   'sales_type',
    //   'site',
    //   'source'
    // ];

    this.dataFetchServ.getRevMgmtData(tableReq).subscribe(data => {
      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      } else {
        this.noTableData = false;
      }
      const tableData = data['data'];
      const arr = [];
      tableData.forEach((row: object) => {
        for (let i = 0; i < grpBysFilter.length; i++) {
          if (i !== grpBysFilter.length - 1) {
            row[grpBysFilter[i]] = 'All';
          }
        }
        let obj = {};
        if (grpBysFilter.length === 1) {
          obj = {
            data: row
          };
        } else {
          obj = {
            data: row,
            children: [{ data: {} }]
          };
        }
        arr.push(obj);
      });

      this.aggTableData = <TreeNode[]>arr;
      this.aggTableJson['totalRecords'] = data['totalItems'];
      setTimeout(() => {
        this.aggTableJson['loading'] = false;
      }, 0);
    });
  }

  reloadAggTable() {
    this.displayAggTable = false;
    setTimeout(() => {
      this.displayAggTable = true;
    }, 0);
  }

  loadCards(cardsReq) {
    this.showCards = false;
    this.dataFetchServ.getRevMgmtData(cardsReq).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }
      if (!this.libServ.isEmptyObj(data['data'])) {

        this.cardsJson.map(o => {
          o['value'] = data['data'][0][o['field']];
        });
      } else {
        this.cardsJson.map(o => {
          o['value'] = 0;
        });
      }
      // this.getDaysAverage(cardsReq,data['data']);
      this.showCards = true;
    });
  }
  getDaysAverage(params, currentData) {
    const totalDataDateDiff = moment(params['timeKeyFilter']['time_key2'], 'YYYYMMDD').diff(
      moment(params['timeKeyFilter']['time_key1'], 'YYYYMMDD'),
      'days'
    );
    // const totalDataDateDiff = parseInt(params['timeKeyFilter']['time_key2'], 10) - parseInt(params['timeKeyFilter']['time_key1'], 10);
    const time_key1 = moment(params['timeKeyFilter']['time_key1'], 'YYYYMMDD')
      .subtract('days', 1)
      .format('YYYYMMDD');
    const time_key2 = moment(time_key1, 'YYYYMMDD')
      .subtract('days', totalDataDateDiff-1)
      .format('YYYYMMDD');
    params['timeKeyFilter'] = { 'time_key1': time_key2, 'time_key2': time_key1 };

    this.dataFetchServ.getRevMgmtData(params).subscribe(data => {
      if (data['data'].length &&  currentData.length) {
        const last7Days = data['data'][0];
        if (!this.libServ.isEmptyObj(last7Days) ) {
          this.cardsJson.map(o => {
            if(o['metaConfig']){
            let last7DaysAvg = 0;
            let totalAvg =  0;

            if (o.field == 'impressions' || o.field == 'revenue') {
              last7DaysAvg = parseFloat(last7Days[o['field']]) / totalDataDateDiff + 1 || 0;
              totalAvg = parseFloat(currentData[0][o['field']]) / (totalDataDateDiff + 1) || 0;
            }
            else {
              totalAvg = parseFloat(currentData[0][o['field']]);
              last7DaysAvg = parseFloat(last7Days[o['field']]);
            }
            o['metaConfig']['avgValue'] = ((totalAvg - last7DaysAvg) / totalAvg * 100).toFixed(2);
            o['metaConfig']['icon'] = totalAvg >= last7DaysAvg ? 'fas fa-arrow-up green' : 'fas fa-arrow-down red';
            o['metaConfig']['dateRange'] = `${moment(params['timeKeyFilter']['time_key1'], 'YYYYMMDD').format('MMM-DD-YYYY')} - ${moment(params['timeKeyFilter']['time_key2'], 'YYYYMMDD').format('MMM-DD-YYYY')}`;
            }
          });
        }
        this.showCards = true;
      } else {
        this.cardsJson.map(o => {
          if(o['metaConfig']){
          o['metaConfig']['avgValue'] = 'N/A';
          o['metaConfig']['icon'] = 'fas fa-minus';
          o['metaConfig']['dateRange'] = `${moment(params['timeKeyFilter']['time_key1'], 'YYYYMMDD').format('MMM-DD-YYYY')} - ${moment(params['timeKeyFilter']['time_key2'], 'YYYYMMDD').format('MMM-DD-YYYY')}`;
          }
        });
        this.showCards = true;
      }
    });
  }

  onTableDrill(e: Event) {
    if (!e['node']['childLoaded']) {
      this.aggTableJson['loading'] = true;
      const tableReq = {
        dimensions: [],
        metrics: this.toggleView
          ? ['revenue', 'impressions', 'ad_requests']
          : [
            'revenue',
            'impressions',
            'house_ads_impressions',
            'unfilled_impressions',
            'total_code_served_count'
          ],
        derived_metrics: this.toggleView
          ? [ 'fill_rate', 'cpm']
          : ['cpm', 'fill_rate_house'],
        timeKeyFilter: this.libServ.deepCopy(
          this.filtersApplied['timeKeyFilter']
        ),
        filters: this.libServ.deepCopy(this.filtersApplied['filters']),
        groupByTimeKey: { key: [], interval: 'daily' },
        gidGroupBy: this.getGrpBys(),
        orderBy: [],
        limit: '',
        offset: ''
      };
      const grpBys = this.filtersApplied['groupby'].map(e => e.key);
      for (const g of grpBys) {
        if (e['node']['data'][g] !== 'All') {
          tableReq['dimensions'].push(g);
          if (!tableReq['gidGroupBy'].includes(g)) {
            tableReq['gidGroupBy'].push(g);
          }
          if (
            tableReq['filters']['dimensions'].findIndex(e => e.key === g) === -1
          ) {
            tableReq['filters']['dimensions'].push({
              key: g,
              values: [e['node']['data'][g]]
            });
          } else {
            tableReq['filters']['dimensions'].splice(
              tableReq['filters']['dimensions'].findIndex(e => e.key === g),
              1
            );
            tableReq['filters']['dimensions'].push({
              key: g,
              values: [e['node']['data'][g]]
            });
            // if (
            //   tableReq['filters']['dimensions']
            //     .find(e => e.key === g)
            //     ['values'].findIndex(v => v === e['node']['data'][g]) === -1
            // ) {
            //   tableReq['filters']['dimensions'].push(e['node']['data'][g]);
            // }
          }
        } else {
          tableReq['dimensions'].push(g);
          if (!tableReq['gidGroupBy'].includes(g)) {
            tableReq['gidGroupBy'].push(g);
          }
          break;
        }
      }

      // tableReq['gidGroupBy'] = this.getGrpBys();
      // For Dev purpose
      // tableReq['gidGroupBy'] = [
      //   'ad_size',
      //   'ad_type',
      //   'ad_units',
      //   'bidding_type',
      //   'device_category',
      //   'geography',
      //   'sales_channel',
      //   'sales_type',
      //   'site',
      //   'source'
      // ];

      this.dataFetchServ.getRevMgmtData(tableReq).subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        const childData = data['data'];
        const arr = [];

        childData.forEach((row: object) => {
          this.aggTableJson['frozenCols'].forEach(r => {
            if (typeof row[r['field']] === 'undefined') {
              row[r['field']] = 'All';
            }
          });
          let obj = {};
          if (row[grpBys[grpBys.length - 1]] === 'All') {
            obj = {
              data: row,
              children: [{ data: {} }]
            };
          } else {
            obj = {
              data: row
            };
          }
          arr.push(obj);
        });
        this.aggTableJson['loading'] = false;
        e['node']['children'] = <TreeNode[]>arr;
        this.aggTableData = [...this.aggTableData];
        e['node']['childLoaded'] = true;
      });
    }
  }

  onLazyLoadAggTable(e: Event) {
    console.log('Lazy Agg', e);
  }

  openPopup(row, field) {
    const grpBys = [];
    const filtersApplied = this.libServ.deepCopy(this.filtersApplied);
    let str = '';

    const fieldIdx = filtersApplied['groupby'].findIndex(
      e => e['key'] === field
    );
    filtersApplied['groupby'].forEach((grp, i) => {
      if (i <= fieldIdx) {
        if (
          filtersApplied['filters']['dimensions'].findIndex(
            e => e.key === grp['key']
          ) !== -1
        ) {
          filtersApplied['filters']['dimensions'].splice(
            filtersApplied['filters']['dimensions'].findIndex(
              e => e.key === grp['key']
            ),
            1
          );
        }

        if (str === '') {
          str = str + row[grp['key']];
        } else {
          str = str + ' > ' + row[grp['key']];
        }
        filtersApplied['filters']['dimensions'].push({
          key: grp['key'],
          values: [row[grp['key']]]
        });
      }
    });

    filtersApplied['groupby'] = grpBys;
    const data = {
      filters: filtersApplied,
      toggleView: this.toggleView,
      title: str,
      heading: row[field],
      exportRequest: this.exportRequest,
      isExportReport: this.isExportReport
    };

    const ref = this.dialogService.open(Rev360AsbnDailyDataPopupComponent, {
      header: row[field] + ' Daily Distribution',
      contentStyle: { width: '80vw', overflow: 'auto' },
      data: data
    });
    ref.onClose.subscribe((data: string) => { });
  }

  exportTable(table, fileFormat) {
    if (this.aggTableData.length == 0) {
      this.confirmationService.confirm({
        message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
        header: 'Information',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
        },
        reject: () => {
        }
      });
    } else {
      if (this.exportRequest['sendEmail'].length > 0) {
        const data = {
          filters: this.filtersApplied,
          colDef: this.aggTableColumnDef,
          toggleView: this.toggleView,
          exportRequest: this.exportRequest,
          fileFormat: fileFormat
        };

        const ref = this.dialogService.open(Rev360AsbnExportPopupComponent, {
          header: 'Download Report',
          contentStyle: { 'max-height': '80vh', overflow: 'auto' },
          data: data
        });
        ref.onClose.subscribe((data: string) => { });
      } else {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Export Report',
          detail:
            'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
          life: 10000
        });
      }
    }
    return false;
  }

  isHiddenColumn(col: Object) {
    return (
      this.aggTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }

  /* --------------------- On Filter Apply Button Clicked --------------------- */

  onFiltersApplied(filterData: object) {
    this.filtersApplied['filters']['dimensions'] = [];

    // tslint:disable-next-line: forin
    for (const k in filterData['filter']['dimensions']) {
      if (filterData['filter']['dimensions'][k].length !== 0) {
        this.filtersApplied['filters']['dimensions'].push({
          key: k,
          values: filterData['filter']['dimensions'][k]
        });
      }
    }

    this.filtersApplied['groupby'] = filterData['groupby'];
    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];

    this.tableReq = {
      dimensions: [],
      metrics: [],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: { key: [], interval: 'daily' },
      gidGroupBy: [],
      orderBy: [
      ],
      limit: '',
      offset: ''
    };
    this.loadTableData(this.tableReq);

    const grpBys = this.getGrpBys();

    const cardsReq = {
      dimensions: [],
      metrics: this.toggleView
        ? ['revenue', 'impressions']
        : ['impressions', 'unfilled_impressions', 'house_ads_impressions'],
      derived_metrics: this.toggleView ? ['cpm'] : [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: { key: [], interval: 'daily' },
      gidGroupBy: grpBys,
      orderBy: [],
      limit: '',
      offset: '',
      isTable: false
    };
    // For Dev purpose
    // cardsReq['gidGroupBy'] = [
    //   'ad_size',
    //   'ad_type',
    //   'ad_units',
    //   'bidding_type',
    //   'device_category',
    //   'geography',
    //   'sales_channel',
    //   'sales_type',
    //   'site',
    //   'source'
    // ];
    this.loadCards(cardsReq);
  }

  getGrpBys() {
    //let grpBys = this.filtersApplied['groupby'].map(e => e.key);
    let grpBys = [this.filtersApplied['groupby'][0]['key']];
    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          grpBys.concat(
            this.filtersApplied['filters']['dimensions']
              .filter(f => f.values.length)
              .map(m => m.key)
          )
        )
      );
    }
    return grpBys;
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['name']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    }
    return false;
  }
  tabChanged(e) {
    this.ngOnDestroy();
    const index = e.index;
    if (index == 0) {
      this.toggleView = true;
      this.cardsJson = [
        {
          field: 'impressions',
          displayName: 'Billable Imps.',
          value: 0,
          imageClass: 'fas fa-eye',
          format: 'number',
          config: [],
          // metaConfig: {
          //   icon: 'fas fa-arrow-down',
          //   avgValue: ''
          // },
          color: '#5cb85c'
        },
        {
          field: 'revenue',
          displayName: 'Gross Revenue',
          value: 0,
          imageClass: 'fas fa-hand-holding-usd',
          format: '$',
          config: [],
          // metaConfig: {
          //   icon: 'fas fa-arrow-down',
          //   avgValue: ''
          // },
          color: '#5bc0de'
        },
        {
          field: 'cpm',
          displayName: 'Gross eCPM',
          value: 0,
          imageClass: 'fas fa-dollar-sign',
          format: '$',
          config: [],
          // metaConfig: {
          //   icon: 'fas fa-arrow-down',
          //   avgValue: ''
          // },
          color: '#FFDC00'
        }
      ];
      this.appConfigObs1 = this.appConfigService.appConfig.subscribe(
        appConfig => {
          moment
          if (!this.libServ.isEmptyObj(appConfig)) {
            this.appConfig = appConfig;
            this._titleService.setTitle( this.appConfig['displayName']);
            let startDate;
            if (
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['startOf']
            ) {
              startDate = moment()
                .subtract(
                  this.appConfig['filter']['filterConfig']['filters'][
                  'datePeriod'
                  ][0]['defaultDate'][0]['value'],
                  this.appConfig['filter']['filterConfig']['filters'][
                  'datePeriod'
                  ][0]['defaultDate'][0]['period']
                )
                .startOf(
                  this.appConfig['filter']['filterConfig']['filters'][
                  'datePeriod'
                  ][0]['defaultDate'][0]['period']
                );
            } else {
              startDate = moment().subtract(
                this.appConfig['filter']['filterConfig']['filters'][
                'datePeriod'
                ][0]['defaultDate'][0]['value'],
                this.appConfig['filter']['filterConfig']['filters'][
                'datePeriod'
                ][0]['defaultDate'][0]['period']
              );
            }
            const endDate = moment().subtract(
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][1]['value'],
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][1]['period']
            );

            this.filtersApplied = {
              timeKeyFilter: {
                time_key1: startDate.format('YYYYMMDD'),
                time_key2: endDate.format('YYYYMMDD')
              },
              filters: { dimensions: [], metrics: [] },
              groupby: this.appConfig['filter']['filterConfig'][
                'groupBy'
              ].filter(v => v.selected)
            };
            const cardsReq = {
              dimensions: [],
              metrics: [
                'revenue',
                'impressions'
              ],
              derived_metrics: ['cpm'],
              timeKeyFilter: this.libServ.deepCopy(
                this.filtersApplied['timeKeyFilter']
              ),
              filters: this.libServ.deepCopy(this.filtersApplied['filters']),
              groupByTimeKey: { key: [], interval: 'daily' },
              gidGroupBy: ['source'],
              orderBy: [],
              limit: '',
              offset: '',
              isTable: false
            };
            this.loadCards(cardsReq);

            this.tableReq = {
              dimensions: [],
              metrics: [],
              derived_metrics: [],
              timeKeyFilter: this.libServ.deepCopy(
                this.filtersApplied['timeKeyFilter']
              ),
              filters: this.libServ.deepCopy(this.filtersApplied['filters']),
              groupByTimeKey: { key: [], interval: 'daily' },
              gidGroupBy: [],
              orderBy: [
              ],
              limit: '',
              offset: ''
            };
          }
        }
      );
      this.loadTableData(this.tableReq);

    } else if (index == 1) {
      this.toggleView = false;
      this.cardsJson = [
        {
          field: 'unfilled_impressions',
          displayName: 'Unfilled Imps.',
          value: 0,
          imageClass: 'fas fa-eye',
          format: 'number',
          config: [],
          color: '#5cb85c'
        },
        {
          field: 'impressions',
          displayName: 'Billable Imps.',
          value: 0,
          imageClass: 'fas fa-eye',
          format: 'number',
          config: [],
          color: '#5bc0de'
        },
        {
          field: 'house_ads_impressions',
          displayName: 'House Ad Imps.',
          value: 0,
          imageClass: 'fas fa-eye',
          format: 'number',
          config: [],
          color: '#FFDC00'
        }
      ];

      this.appConfigObs2 = this.appConfigService.appConfig.subscribe(
        appConfig => {
          if (!this.libServ.isEmptyObj(appConfig)) {
            this.appConfig = appConfig;
            this._titleService.setTitle(this.appConfig['displayName']);
            let startDate;
            if (
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['startOf']
            ) {
              startDate = moment()
                .subtract(
                  this.appConfig['filter']['filterConfig']['filters'][
                  'datePeriod'
                  ][0]['defaultDate'][0]['value'],
                  this.appConfig['filter']['filterConfig']['filters'][
                  'datePeriod'
                  ][0]['defaultDate'][0]['period']
                )
                .startOf(
                  this.appConfig['filter']['filterConfig']['filters'][
                  'datePeriod'
                  ][0]['defaultDate'][0]['period']
                );
            } else {
              startDate = moment().subtract(
                this.appConfig['filter']['filterConfig']['filters'][
                'datePeriod'
                ][0]['defaultDate'][0]['value'],
                this.appConfig['filter']['filterConfig']['filters'][
                'datePeriod'
                ][0]['defaultDate'][0]['period']
              );
            }
            const endDate = moment().subtract(
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][1]['value'],
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][1]['period']
            );

            this.filtersApplied = {
              timeKeyFilter: {
                time_key1: startDate.format('YYYYMMDD'),
                time_key2: endDate.format('YYYYMMDD')
              },
              filters: { dimensions: [], metrics: [] },
              groupby: this.appConfig['filter']['filterConfig'][
                'groupBy'
              ].filter(v => v.selected)
            };
            const cardsReq = {
              dimensions: [],
              metrics: [
                'impressions',
                'unfilled_impressions',
                'house_ads_impressions'
              ],
              derived_metrics: [],
              timeKeyFilter: this.libServ.deepCopy(
                this.filtersApplied['timeKeyFilter']
              ),
              filters: this.libServ.deepCopy(this.filtersApplied['filters']),
              groupByTimeKey: { key: [], interval: 'daily' },
              gidGroupBy: ['source'],
              orderBy: [],
              limit: '',
              offset: '',
              isTable: false
            };
            this.loadCards(cardsReq);
            this.tableReq = {
              dimensions: [],
              metrics: [],
              derived_metrics: [],
              timeKeyFilter: this.libServ.deepCopy(
                this.filtersApplied['timeKeyFilter']
              ),
              filters: this.libServ.deepCopy(this.filtersApplied['filters']),
              groupByTimeKey: { key: [], interval: 'daily' },
              gidGroupBy: [],
              orderBy: [
              ],
              limit: '',
              offset: ''
            };
          }
        }
      );
      this.loadTableData(this.tableReq);
    }
  }

  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
    if (this.appConfigObs1 && !this.appConfigObs1.closed) {
      this.appConfigObs1.unsubscribe();
    }
    if (this.appConfigObs2 && !this.appConfigObs2.closed) {
      this.appConfigObs2.unsubscribe();
    }
  }

  sendEmail() {
    const imageConfig = {
      imgName: `${this.appConfig['name']}.png`,
      type: 'image/png'
    };

    this.htmltoimage
      .canvasImage(document.getElementById('capture'), imageConfig)
      .then(canvas => {
        this.dialogService.open(SendEmailComponent, {
          header: 'Email Screenshot',
          contentStyle: {
            'max-height': '80vh',
            width: '30vw',
            overflow: 'auto'
          },
          data: canvas.toDataURL('image/png')
        });
      });
  }

  onGlobalSearchChanged(searchValue, tableColDef) {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    const grpBys = this.getGrpBys();
    let col = grpBys[0];
    this.timeout = setTimeout(() => {
      this.tableReq['filters']['globalSearch'] = {
        dimensions: [],
        value: searchValue
      };
      tableColDef.forEach(element => {
        this.tableReq['filters']['globalSearch']['dimensions'].push(
          element.field
        );
      });
      this.tableReq['filters']['globalSearch']['dimensions'].push(col);
      this.loadTableData(this.tableReq);
    }, 3000);
  }
}
