import { Routes } from '@angular/router';
import { Rev360AsbnAppComponent } from './rev360-asbn-app.component';

export const Rev360AsbnAppRoutes: Routes = [
  {
    path: '',
    component: Rev360AsbnAppComponent
  }
];
