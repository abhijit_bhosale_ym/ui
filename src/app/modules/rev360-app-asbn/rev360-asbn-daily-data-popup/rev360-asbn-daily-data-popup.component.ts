import { Component, OnInit } from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { FetchApiDataService } from '../fetch-api-data.service';
import * as moment from 'moment';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-rev360-asbn-daily-data-popup',
  templateUrl: './rev360-asbn-daily-data-popup.component.html',
  styleUrls: ['./rev360-asbn-daily-data-popup.component.scss']
})
export class Rev360AsbnDailyDataPopupComponent implements OnInit {
  lineChartJson: object;
  showLineChart = false;
  toggleView = true;
  filtersApplied: object;
  noLineChart = false;
  tabChangedFlag = false;

  tableDataReq: object;
  tableData: TreeNode[];
  tableColumnDef: any[];
  dimColDef: any[];
  tableJson: Object;
  popupTitle = '';
  heading = '';
  exportRequest: ExportRequest = <ExportRequest>{};
  isExportReport = false;

  constructor(
    private libServ: CommonLibService,
    private formatNumPipe: FormatNumPipe,
    private dataFetchServ: FetchApiDataService,
    private config: DynamicDialogConfig,
    private exportService: ExportdataService,
    private toastService: ToastService,
    private confirmationService: ConfirmationService
  ) {
    this.filtersApplied = this.config.data['filters'];
    this.toggleView = this.config.data['toggleView'];
    this.popupTitle = this.config.data['title'];
    this.heading = this.config.data['heading'];
  }

  ngOnInit() {
    this.exportRequest = this.config.data['exportRequest'];
    this.isExportReport = this.config.data['isExportReport'];
    this.dimColDef = [
      {
        field: 'source',
        displayName: 'Source',
        format: '',
        width: '250',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'media_group',
        displayName: 'Media Group',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'property',
        displayName: 'Property',
        format: '',
        width: '140',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      }
    ];
    this.tableColumnDef = [
      {
        field: 'impressions',
        displayName: 'Paid Imps.',
        format: 'number',
        width: '170',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'revenue',
        displayName: 'Revenue',
        width: '155',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'cpm',
        displayName: 'eCPM',
        format: '$',
        width: '150',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      }
    ];
    this.tableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '300px',
      totalRecords: 1000,

      columns: this.tableColumnDef,
      selectedColumns: this.tableColumnDef,
      // frozenCols: [],
      // frozenWidth: '0px',
      // columns: this.tableColumnDef.slice(1),
      // selectedColumns: this.tableColumnDef.slice(1),
      // frozenCols: [...this.tableColumnDef.slice(0, 1)],
      // frozenWidth:
      //   this.tableColumnDef
      //     .slice(0, 1)
      //     .reduce((tot, cur) => tot + parseInt(cur.width), 0) + 'px',

      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: false,
      columnSearch: false
    };

    this.loadTable();
  }

  loadTable() {
    this.tableJson['loading'] = true;

    let grpBys = this.filtersApplied['groupby'].map(e => e.key);
    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          this.filtersApplied['filters']['dimensions']
            .filter(f => f.values.length)
            .map(m => m.key)
            .concat(grpBys)
        )
      );
    }

    this.dimColDef.reverse().forEach(c => {
      if (grpBys.includes(c.field)) {
        this.tableColumnDef.unshift(c);
      }
    });
    this.tableColumnDef.unshift({
      field: 'time_key',
      displayName: 'Date',
      format: 'date',
      width: '120',
      exportConfig: {
        format: 'date',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: false
      }
    });

    if (this.toggleView) {
      this.tableColumnDef.splice(this.tableColumnDef.length, 0,
        {
          field: 'fill_rate',
          displayName: 'Fill Rate',
          format: 'percentage',
          width: '110',
          exportConfig: {
            format: 'percentage',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [2],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: true
          }
        }
      );
    } else {
      this.tableColumnDef.splice(
        this.tableColumnDef.length,
        0,
        // {
        //   field: 'fill_rate_house',
        //   displayName: 'Fill Rate',
        //   format: 'percentage',
        //   width: '110',
        //   exportConfig: {
        //     format: 'percentage',
        //     styleinfo: {
        //       thead: 'default',
        //       tdata: 'white'
        //     }
        //   },
        //   formatConfig: [2],
        //   options: {
        //     editable: false,
        //     colSearch: false,
        //     colSort: true,
        //     resizable: true,
        //     movable: true
        //   }
        // },
        {
          field: 'house_ads_impressions',
          displayName: 'House Ad',
          format: 'number',
          visible: false,
          width: '170',
          exportConfig: {
            format: 'number',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: true
          },
          footerTotal: '-'
        },
        {
          field: 'unfilled_impressions',
          displayName: 'Unfilled Imps.',
          format: 'number',
          visible: false,
          width: '170',
          exportConfig: {
            format: 'number',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: true
          },
          footerTotal: '-'
        }
      );
    }
    grpBys.push('time_key');
    // this.aggTableColumnDef = finalColDef.slice();

    this.tableJson['columns'] = this.tableColumnDef.slice(1
    );
    this.tableJson['selectedColumns'] = this.tableColumnDef.slice(1);
    this.tableJson['frozenCols'] = [
      ...this.tableColumnDef.slice(0, 1)
    ];
    this.tableJson['frozenWidth'] =
      this.tableColumnDef
        .slice(0, 1)
        .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px';

    console.log("tableColumnDef=======", this.tableColumnDef);

    this.tableDataReq = {
      dimensions: grpBys,
      metrics: this.toggleView
        ? ['revenue', 'impressions', 'ad_requests']
        : [
          'revenue',
          'impressions',
          'dfp_adserver_impressions',
          'house_ads_impressions',
          'unfilled_impressions',
          'total_code_served_count'
        ],
      derived_metrics: this.toggleView
        ? ['fill_rate', 'cpm']
        : ['cpm', 'fill_rate_house'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: grpBys.filter(v => v !== 'time_key'),
      orderBy: [{ key: 'time_key', opcode: 'desc' }],
      limit: '',
      offset: ''
    };
    // For Dev purpose
    // this.tableDataReq['gidGroupBy'] = [
    //   'ad_size',
    //   'ad_type',
    //   'ad_units',
    //   'bidding_type',
    //   'device_category',
    //   'geography',
    //   'sales_channel',
    //   'sales_type',
    //   'site',
    //   'source'
    // ];

    this.dataFetchServ.getRevMgmtData(this.tableDataReq).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }
      const tableData = data['data'];

      const arr = [];
      tableData.forEach((row: object) => {
        const obj = {
          data: row
        };
        arr.push(obj);
      });

      this.tableData = <TreeNode[]>arr;
      this.tableJson['totalRecords'] = data['totalItems'];
      this.tableJson['loading'] = false;
    });
  }

  exportTablePopup(fileFormat) {

    if (this.tableData.length == 0) {
      this.confirmationService.confirm({
        message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
        header: 'Information',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
        },
        reject: () => {
        }
      });
    } else {
      let notIncludeColumn = this.toggleView
        ? ['fill_rate']
        : []
      if (this.exportRequest['sendEmail'].length > 0) {
        this.toastService.displayToast({
          severity: 'info',
          summary: 'Export Report',
          detail:
            'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
          life: 10000
        });
        const columnDefs = this.tableColumnDef;
        const req = this.libServ.deepCopy(this.tableDataReq);
        req['limit'] = '';
        req['offset'] = '';
        req['isTable'] = false;
        const sheetDetailsarray = [];
        const sheetDetails = {};
        sheetDetails['columnDef'] = this.libServ.deepCopy(columnDefs);
        sheetDetails['data'] = [];
        sheetDetails['sheetName'] = 'Daily Distribution';
        sheetDetails['isRequest'] = true;
        sheetDetails['request'] = {
          url: '/api/flask/payout_asbn/cassandradata',
          method: 'POST',
          param: req
        };
        sheetDetails['disclaimer'] = [
          {
            position: 'bottom',
            label: 'Note: Data present in the table may vary over a period of time.',
            color: '#000000'
          }
        ];
        sheetDetails['totalFooter'] = {
          available: true,
          custom: true,
          notIncludeColumn: notIncludeColumn
        };
        sheetDetails['tableTitle'] = {
          available: false,
          label: this.heading + ' Daily Distribution'
        };
        sheetDetails['image'] = [
          {
            available: true,
            path: environment.exportConfig.exportLogo,
            position: 'top'
          }
        ];
        sheetDetailsarray.push(sheetDetails);
        this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
        this.exportRequest['fileName'] = this.heading + ' Daily Distribution ' +
          moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
        this.exportRequest['exportFormat'] = fileFormat;
        this.exportRequest['exportConfig'] = environment.exportConfig;
        this.dataFetchServ
          .getExportReportData(this.exportRequest)
          .subscribe(response => {
            console.log(response);
          });

      } else {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Export Report',
          detail:
            'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email.Thanks!',
          life: 10000
        });
      }
    }
  }

  isHiddenColumn(col: Object) { }

  loadLineChart() {
    let grpBys = this.filtersApplied['groupby'].map(e => e.key);
    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          this.filtersApplied['filters']['dimensions']
            .filter(f => f.values.length)
            .map(m => m.key)
            .concat(grpBys)
        )
      );
    }

    const lineChartReq = {
      dimensions: ['time_key'],
      metrics: this.toggleView
        ? ['revenue', 'impressions', 'ad_requests']
        : [
          'revenue',
          'impressions',
          'dfp_adserver_impressions',
          'house_ads_impressions',
          'unfilled_impressions',
          'total_code_served_count'
        ],
      derived_metrics: this.toggleView
        ? ['fill_rate', 'cpm']
        : ['cpm', 'fill_rate_house'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: grpBys,
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };
    // For Dev purpose
    // lineChartReq['gidGroupBy'] = [
    //   'ad_size',
    //   'ad_type',
    //   'ad_units',
    //   'bidding_type',
    //   'device_category',
    //   'geography',
    //   'sales_channel',
    //   'sales_type',
    //   'site',
    //   'source'
    // ];
    const colors = this.libServ.dynamicColors(2);
    this.lineChartJson = {
      chartTypes: [{ key: 'bar', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'eCPM',
            type: 'line',
            yAxisID: 'y-axis-0',

            borderColor: colors[0],
            fill: false,
            backgroundColor: colors[0],
            data: []
          },
          {
            label: 'Revenue',
            type: 'bar',
            yAxisID: 'y-axis-1',

            borderColor: colors[1],
            fill: false,
            backgroundColor: colors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Revenue vs eCPM'
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: 'eCPM ($)'
              },
              position: 'right',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', []);
                }
              }
              // scaleFontColor: "rgba(151,137,200,0.8)"
            },
            {
              id: 'y-axis-1',
              scaleLabel: {
                display: true,
                labelString: 'Revenue ($)'
              },
              position: 'left',
              name: '2',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', []);
                }
              }
              // scaleFontColor: "rgba(151,187,205,0.8)"
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: true,
      chartWidth: '',
      chartHeight: '300px'
    };

    this.showLineChart = false;
    this.noLineChart = false;
    this.dataFetchServ.getRevMgmtData(lineChartReq).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }
      const chartData = data['data'];
      if (typeof chartData === 'undefined' || !chartData.length) {
        this.noLineChart = true;
      } else {
        const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));

        this.lineChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );

        const revArr = [];
        const ecpmArr = [];
        datesArr.forEach(time_key => {
          chartData.forEach(r => {
            if (r['time_key'] === time_key) {
              revArr.push(r['revenue']);
              ecpmArr.push(r['cpm']);
            }
          });
        });
        this.lineChartJson['chartData']['datasets'][0]['data'] = ecpmArr;
        this.lineChartJson['chartData']['datasets'][1]['data'] = revArr;
        this.showLineChart = true;
        this.noLineChart = false;
      }
    });
  }

  tabChanged(e) {
    if (!this.tabChangedFlag) {
      this.loadLineChart();
      this.tabChangedFlag = true;
    }
    // console.log('tab changed', e);
  }

  chartSelected(e) { }
}
