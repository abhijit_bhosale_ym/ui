import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Rev360AsbnDailyDataPopupComponent } from './rev360-asbn-daily-data-popup.component';

describe('Rev360AsbnDailyDataPopupComponent', () => {
  let component: Rev360AsbnDailyDataPopupComponent;
  let fixture: ComponentFixture<Rev360AsbnDailyDataPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Rev360AsbnDailyDataPopupComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Rev360AsbnDailyDataPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
