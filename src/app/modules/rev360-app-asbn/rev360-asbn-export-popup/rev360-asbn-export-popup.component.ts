import { Component, OnInit } from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { FetchApiDataService } from '../fetch-api-data.service';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import * as moment from 'moment';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-rev360-asbn-export-popup',
  templateUrl: './rev360-asbn-export-popup.component.html',
  styleUrls: ['./rev360-asbn-export-popup.component.scss']
})
export class Rev360AsbnExportPopupComponent implements OnInit {
  configData: object;
  exportRequest: ExportRequest = <ExportRequest>{};
  constructor(
    private libServ: CommonLibService,
    private dataFetchServ: FetchApiDataService,
    private exportService: ExportdataService,
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private toastService: ToastService
  ) {
    this.configData = this.config.data;
  }

  ngOnInit() {
    this.exportRequest = this.configData['exportRequest'];
  }

  async downloadData(daily) {
    this.toastService.displayToast({
      severity: 'info',
      summary: 'Export Report',
      detail:
        'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
      life: 10000
    });
    const filtersApplied = this.configData['filters'];
    const colDef = this.configData['colDef'];
    let grpBys = filtersApplied['groupby'].map(e => e.key);
    if (
      this.libServ.isEmptyObj(filtersApplied['filters']['dimensions']) ||
      filtersApplied['filters']['dimensions'].length === 0
    ) {
      filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          filtersApplied['filters']['dimensions']
            .filter(f => f.values.length)
            .map(m => m.key)
            .concat(grpBys)
        )
      );
    }

    if (daily) {
      grpBys.push('time_key');
      const tableReq = {
        dimensions: grpBys,
        metrics: this.configData['toggleView']
          ? ['revenue', 'impressions', 'ad_requests']
          : [
            'revenue',
            'impressions',
            'house_ads_impressions',
            'unfilled_impressions',
            'total_code_served_count'
          ],
        derived_metrics: this.configData['toggleView']
          ? ['fill_rate', 'cpm']
          : ['cpm', 'fill_rate_house'],
        timeKeyFilter: filtersApplied['timeKeyFilter'],
        filters: filtersApplied['filters'],
        groupByTimeKey: {
          key: ['accounting_key', 'time_key'],
          interval: 'daily'
        },
        gidGroupBy: grpBys.filter(v => v !== 'time_key'),
        orderBy: [{ key: 'time_key', opcode: 'desc' }],
        limit: '',
        offset: ''
      };

      tableReq['isTable'] = false;
      const obj = {
        field: 'time_key',
        displayName: 'Date',
        format: 'date',
        width: '150',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: false
        }
      };

      if (!colDef.some(o => o.field == 'time_key')) {
        colDef.splice(0, 0, obj);
      }

      const sheetDetails = {};
      sheetDetails['columnDef'] = colDef;
      sheetDetails['data'] = [];
      sheetDetails['sheetName'] = 'Daily Distribution';
      sheetDetails['isRequest'] = true;
      sheetDetails['request'] = {
        url: '/api/flask/payout_asbn/cassandradata',
        method: 'POST',
        param: tableReq
      };
      sheetDetails['disclaimer'] = [
        {
          position: 'bottom',
          label: 'Note: Data present in the table may vary over a period of time.',
          color: '#000000'
        }
      ];
      sheetDetails['totalFooter'] = {
        available: false,
        custom: false
      };
      sheetDetails['tableTitle'] = {
        available: false,
        label: 'Daily Distribution'
      };
      sheetDetails['image'] = [
        {
          available: true,
          path: environment.exportConfig.exportLogo,
          position: 'top'
        }
      ];
      const sheetDetailsarray = [];
      sheetDetailsarray.push(sheetDetails);
      this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
      this.exportRequest['fileName'] =
        'Daily Distribution ' + moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
      this.exportRequest['exportFormat'] = this.configData['fileFormat'];
      this.exportRequest['exportConfig'] = environment.exportConfig;

      // return false;
      this.dataFetchServ
        .getExportReportData(this.exportRequest)
        .subscribe(response => {
          console.log(response);
        });
    } else {
      const tableReq = {
        dimensions: grpBys,
        metrics: this.configData['toggleView']
          ? ['revenue', 'impressions', 'ad_requests']
          : [
            'revenue',
            'impressions',
            'house_ads_impressions',
            'unfilled_impressions',
            'total_code_served_count'
          ],
        derived_metrics: this.configData['toggleView']
          ? ['fill_rate', 'cpm']
          : ['cpm', 'fill_rate_house'],
        timeKeyFilter: filtersApplied['timeKeyFilter'],
        filters: filtersApplied['filters'],
        groupByTimeKey: {
          key: ['accounting_key', 'time_key'],
          interval: 'daily'
        },
        gidGroupBy: grpBys.filter(v => v !== 'time_key'),
        orderBy: [],
        limit: '',
        offset: ''
      };
      // For Dev purpose
      // tableReq['gidGroupBy'] = [
      //   'ad_size',
      //   'ad_type',
      //   'ad_units',
      //   'bidding_type',
      //   'device_category',
      //   'geography',
      //   'sales_channel',
      //   'sales_type',
      //   'site',
      //   'source'
      // ];

      let temp = 0;
      const data1 = [];
      const sheetDetailsarray = [];
      while (temp < grpBys.length) {
        const grpbyExport = [];
        for (let i = 0; i <= temp; i++) {
          grpbyExport.push(grpBys[i]);
          tableReq['orderBy'] = [{ key: grpBys[i], opcode: 'asc' }];
        }

        tableReq['gidGroupBy'] = this.getGrpBys(grpbyExport);
        tableReq['dimensions'] = grpbyExport;
        tableReq['isTable'] = false;

        // await this.dataFetchServ
        //   .getRevMgmtData(tableReq)
        //   .toPromise()
        //   .then(success => {
        // if (success['status'] === 0) {
        //   this.toastService.displayToast({
        //     severity: 'error',
        //     summary: 'Server Error',
        //     detail: 'Please refresh the page'
        //   });
        //   console.log(success['status_msg']);
        //   return;
        // }
        const colDefOptions = [];
        let i = 0;
        let sheetName = '';

        while (i < grpbyExport.length) {
          let displayName = '';
          if (grpbyExport[i] === 'source') {
            displayName = 'Source';
            if (i === 0) {
              sheetName = 'Source';
            } else {
              sheetName = '+Source';
            }
          } else if (grpbyExport[i] === 'media_group') {
            displayName = 'Media Group';
            if (i === 0) {
              sheetName = 'Media Group ';
            } else {
              sheetName = '+Media Group';
            }
          } else if (grpbyExport[i] === 'property') {
            displayName = 'Property';
            if (i === 0) {
              sheetName = 'Property';
            } else {
              sheetName = '+Property';
            }
          }
          colDefOptions.push({
            field: grpbyExport[i],
            displayName: displayName,
            visible: true,
            width: 150,
            format: 'string',
            exportConfig: {
              format: 'string',
              styleinfo: {
                thead: 'default',
                tdata: 'white'
              }
            }
          });
          i++;
        }

        colDefOptions.push(
          {
            field: 'impressions',
            displayName: 'Paid Imps.',
            format: 'number',
            width: '170',
            exportConfig: {
              format: 'number',
              styleinfo: {
                thead: 'default',
                tdata: 'white'
              }
            },
            formatConfig: [],
            options: {
              editable: false,
              colSearch: true,
              colSort: true,
              resizable: false
            },
            footerTotal: '-'
          },
          {
            field: 'revenue',
            displayName: 'Revenue',
            width: '155',
            exportConfig: {
              format: 'currency',
              styleinfo: {
                thead: 'default',
                tdata: 'white'
              }
            },
            format: '$',
            formatConfig: [],
            options: {
              editable: false,
              colSearch: true,
              colSort: true,
              resizable: false
            },
            footerTotal: '-'
          },
          {
            field: 'cpm',
            displayName: 'eCPM',
            format: '$',
            width: '135',
            exportConfig: {
              format: 'currency',
              styleinfo: {
                thead: 'default',
                tdata: 'white'
              }
            },
            formatConfig: [],
            options: {
              editable: false,
              colSearch: true,
              colSort: true,
              resizable: false
            },
            footerTotal: '-'
          }
        );

        if (this.configData['toggleView']) {
          colDefOptions.push(
            {
              field: 'fill_rate',
              displayName: 'Fill Rate',
              format: 'percentage',
              width: '110',
              exportConfig: {
                format: 'percentage',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              formatConfig: [2],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: true
              }
            }
          );
        } else {
          colDefOptions.push(
            // {
            //   field: 'fill_rate_house',
            //   displayName: 'Fill Rate',
            //   format: 'percentage',
            //   width: '110',
            //   exportConfig: {
            //     format: 'percentage',
            //     styleinfo: {
            //       thead: 'default',
            //       tdata: 'white'
            //     }
            //   },
            //   formatConfig: [2],
            //   options: {
            //     editable: false,
            //     colSearch: false,
            //     colSort: true,
            //     resizable: true,
            //     movable: true
            //   }
            // },
            {
              field: 'house_ads_impressions',
              displayName: 'House Ad',
              format: 'number',
              visible: false,
              width: '170',
              exportConfig: {
                format: 'number',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: true
              },
              footerTotal: '-'
            },
            {
              field: 'unfilled_impressions',
              displayName: 'Unfilled Imps',
              format: 'number',
              visible: false,
              width: '170',
              exportConfig: {
                format: 'number',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: true
              },
              footerTotal: '-'
            }
          );
        }
        const sheetDetails = {};
        let colDef = this.libServ.deepCopy(colDefOptions);
        sheetDetails['columnDef'] = colDef.slice(0, colDef.length);
        sheetDetails['data'] = [];
        sheetDetails['sheetName'] = sheetName;
        sheetDetails['isRequest'] = true;
        sheetDetails['request'] = {
          url: '/api/flask/payout_asbn/cassandradata',
          method: 'POST',
          param: this.libServ.deepCopy(tableReq)
        };
        sheetDetails['disclaimer'] = [
          {
            position: 'bottom',
            label: 'Note: Data present in the table may vary over a period of time.',
            color: '#000000'
          }
        ];
        sheetDetails['totalFooter'] = {
          available: true,
          custom: true
        };
        sheetDetails['tableTitle'] = {
          available: false,
          label: 'Programmatic Daily Data'
        };
        sheetDetails['image'] = [
          {
            available: true,
            path: environment.exportConfig.exportLogo,
            position: 'top'
          }
        ];

        sheetDetailsarray.push(sheetDetails);
        // return false;

        // const dailyData = this.libServ.deepCopy(success['data']);

        // data1.push({
        //   data: [
        //     {
        //       data: dailyData,
        //       columnDefs: colDefOptions
        //     }
        //   ],

        //   sheetname: sheetName
        // });
        // });

        temp++;
      }
      this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
      this.exportRequest['fileName'] =
        'Aggregated Distribution ' +
        moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
      this.exportRequest['exportFormat'] = this.configData['fileFormat'];
      this.exportRequest['exportConfig'] = environment.exportConfig;
      this.dataFetchServ
        .getExportReportData(this.exportRequest)
        .subscribe(response => {
          console.log(response);
        });
      // this.exportService.exportReport(
      //   data1,
      //   'Aggregated Distribution ' +
      //     moment(new Date()).format('MM-DD-YYYY HH:mm:ss'),
      //   'Excel',
      //   false
      // );
    }
  }

  getGrpBys(key) {
    // let grpBys = this.filtersApplied['groupby'].map(e => e.key);

    const filtersApplied = this.configData['filters'];
    let grpBys = key;
    if (
      this.libServ.isEmptyObj(filtersApplied['filters']['dimensions']) ||
      filtersApplied['filters']['dimensions'].length === 0
    ) {
      filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          grpBys.concat(
            filtersApplied['filters']['dimensions']
              .filter(f => f.values.length)
              .map(m => m.key)
          )
        )
      );
    }
    return grpBys;
  }

  cancel() {
    this.ref.close(null);
  }
}
