import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Rev360AsbnExportPopupComponent } from './rev360-asbn-export-popup.component';

describe('Rev360AsbnExportPopupComponent', () => {
  let component: Rev360AsbnExportPopupComponent;
  let fixture: ComponentFixture<Rev360AsbnExportPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Rev360AsbnExportPopupComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Rev360AsbnExportPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
