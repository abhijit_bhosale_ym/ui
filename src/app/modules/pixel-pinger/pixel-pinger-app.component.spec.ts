import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PixelPingerComponent } from './pixel-pinger-app.component';

describe('PixelPingerComponent', () => {
  let component: PixelPingerComponent;
  let fixture: ComponentFixture<PixelPingerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PixelPingerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PixelPingerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
