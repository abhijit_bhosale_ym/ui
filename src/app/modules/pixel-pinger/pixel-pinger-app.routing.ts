import { Routes } from '@angular/router';

import { PixelPingerComponent } from './pixel-pinger-app.component';
export const PixelPingerRoutes: Routes = [
  {
    path: '',
    component: PixelPingerComponent
  }
];
