import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, from } from 'rxjs';
import { Title } from '@angular/platform-browser';
import { TreeNode } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import * as moment from 'moment';
import { CommonLibService, AppConfigService } from 'src/app/_services';
import { FetchApiDataService } from './fetch-api-data.service';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SendEmailComponent } from '../send-email/send-email.component';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'ym-pixel-pinger',
  templateUrl: './pixel-pinger-app.component.html',
  styleUrls: ['./pixel-pinger-app.component.scss']
})
  export class PixelPingerComponent implements OnInit {
  private appConfigObs: Subscription;

  appConfig: object = {};
  filtersApplied: object = {};

  displayAggTable = true;
  noTableData = true;

  showMainLineChartSource = false;
  noDataMainLineChart = false;
  mainLineChartJson: object;
  noDataEngagementChart = false;
  mainEngagementChartJson: object;
  showEngagementChartSource = false;
  tableData: TreeNode[];
  tableColumnDef: any[];
  tableJson: Object;
  intervalHolder: any;
  exportRequest: ExportRequest = <ExportRequest>{};
  apiData : any[];  

  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private htmltoimage: HtmltoimageService,
    private pptExport: ExportPptService,
    private formatNumPipe: FormatNumPipe,
    private confirmationService: ConfirmationService
  ) { }


  ngOnInit(): void {
    
  this.intervalHolder = setInterval(() => {
      this.loadTableData({});
  }, 1000 * 120); 

    this.tableColumnDef = [
      {
        field: 'campaign_id',
        displayName: 'CID ',
        width: '60',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'line_item_id',
        displayName: 'LI ID',
        format: '',
        width: '60',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }

      },
      {
        field: 'campaign_name',
        displayName: 'Campaign Name',
        format: '',
        width: '160',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'line_item_name',
        displayName: 'Line Item ',
        format: '',
        width: '180',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
        {
          field: 'advertiser_name',
          displayName: 'Advertiser',
          format: '',
          width: '100',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
      },

      {
        field: 'start_date',
        displayName: 'Start Date',
        format: '',
        width: '85',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
    },
    {
      field: 'end_date',
      displayName: 'End Date',
      format: '',
      width: '85',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: false
      }
  },

  {
    field: 'campaign_budget',
    displayName: 'Campaign Budget',
    format: 'number',
    width: '90',
    exportConfig: {
      format: 'number',
      styleinfo: {
        thead: 'default',
        tdata: 'white'
      }
    },
    formatConfig: [],
    options: {
      editable: false,
      colSearch: false,
      colSort: true,
      resizable: true,
      movable: false
    }
},
{
  field: 'line_item_budget',
  displayName: 'Line Item Budget',
  format: 'number',
  width: '90',
  exportConfig: {
    format: 'number',
    styleinfo: {
      thead: 'default',
      tdata: 'white'
    }
  },
  formatConfig: [],
  options: {
    editable: false,
    colSearch: false,
    colSort: true,
    resizable: true,
    movable: false
  }
},

{
  field: 'total_bw_imp',
  displayName: 'BW Total Imps.',
  format: 'number',
  width: '85',
  exportConfig: {
    format: 'number',
    styleinfo: {
      thead: 'default',
      tdata: 'white'
    }
  },
  formatConfig: [],
  options: {
    editable: false,
    colSearch: false,
    colSort: true,
    resizable: true,
    movable: false
  }
},

{
  field: 'total_bw_completions',
  displayName: 'BW Total Compls.',
  format: 'number',
  width: '100',
  exportConfig: {
    format: 'number',
    styleinfo: {
      thead: 'default',
      tdata: 'white'
    }
  },
  formatConfig: [],
  options: {
    editable: false,
    colSearch: false,
    colSort: true,
    resizable: true,
    movable: false
  }
},
{
  field: 'totalBWComplRate',
  displayName: 'BW Total Compls. Rate',
  format: 'percentage',
  width: '100',
  exportConfig: {
    format: 'percentage',
    styleinfo: {
      thead: 'default',
      tdata: 'white'
    }
  },
  formatConfig: [2],
  options: {
    editable: false,
    colSearch: false,
    colSort: true,
    resizable: true,
    movable: false
  }
},

{
  field: 'campaign_daily_budget',
  displayName: 'Campaign Daily Budget',
  format: 'number',
  width: '100',
  exportConfig: {
    format: 'number',
    styleinfo: {
      thead: 'default',
      tdata: 'white'
    }
  },
  formatConfig: [],
  options: {
    editable: false,
    colSearch: false,
    colSort: true,
    resizable: true,
    movable: false
  }
},

{
  field: 'daily_budget',
  displayName: 'LI Daily Budget',
  format: 'number',
  width: '100',
  exportConfig: {
    format: 'number',
    styleinfo: {
      thead: 'default',
      tdata: 'white'
    }
  },
  formatConfig: [],
  options: {
    editable: false,
    colSearch: false,
    colSort: true,
    resizable: true,
    movable: false
  }
},

{
  field: 'today_bw_imp',
  displayName: 'BW Todays Imps.',
  format: 'number',
  width: '100',
  exportConfig: {
    format: 'number',
    styleinfo: {
      thead: 'default',
      tdata: 'white'
    }
  },
  formatConfig: [],
  options: {
    editable: false,
    colSearch: false,
    colSort: true,
    resizable: true,
    movable: false
  }
},

{
  field: 'today_bw_completions',
  displayName: 'BW Todays Compls.',
  format:'number',
  width: '100',
  exportConfig: {
    format: 'number',
    styleinfo: {
      thead: 'default',
      tdata: 'white'
    }
  },
  formatConfig: [],
  options: {
    editable: false,
    colSearch: false,
    colSort: true,
    resizable: true,
    movable: false
  }
},

{
  field: 'todaysBWComplRate',
  displayName: 'BW Todays Compls. Rate',
  format: 'percentage',
  width: '100',
  exportConfig: {
    format: 'percentage',
    styleinfo: {
      thead: 'default',
      tdata: 'white'
    }
  },
  formatConfig: [2],
  options: {
    editable: false,
    colSearch: false,
    colSort: true,
    resizable: true,
    movable: false
  }

},

{
  field: 'pp_imp',
  displayName: 'PP Imps.',
  format: 'number',
  width: '85',
  exportConfig: {
    format: 'number',
    styleinfo: {
      thead: 'default',
      tdata: 'white'
    }
  },
  formatConfig: [],
  options: {
    editable: false,
    colSearch: false,
    colSort: true,
    resizable: true,
    movable: false
  }

},

{
  field: 'pp_completions',
  displayName: 'PP Compls.',
  format: 'number',
  width: '90',
  exportConfig: {
    format: 'number',
    styleinfo: {
      thead: 'default',
      tdata: 'white'
    }
  },
  formatConfig: [],
  options: {
    editable: false,
    colSearch: false,
    colSort: true,
    resizable: true,
    movable: false
  }

},

{
  field: 'ppComplRate',
  displayName: 'PP Compls. Rate',
  format: 'percentage',
  width: '90',
  exportConfig: {
    format: 'percentage',
    styleinfo: {
      thead: 'default',
      tdata: 'white'
    }
  },
  formatConfig: [2],
  options: {
    editable: false,
    colSearch: false,
    colSort: true,
    resizable: true,
    movable: false
  }
 },
];
  this.tableJson = {
    page_size: 10,
    lazy: false,
    loading: false,
    export: true,
    sortMode: 'multiple',
    resizableColumns: true,
    columnResizeMode: 'fit',
    reorderableColumns: true,
    scrollHeight: '400px',
    totalRecords: 1000,
    columns: this.tableColumnDef,
    selectedColumns: this.tableColumnDef,
    scrollable: true,
    selectionMode: 'multiple',
    selectedColsModal: [],
    selectionDataKey: 'name',
    metaKeySelection: true,
    showHideCols: false,
    overallSearch: false,
    columnSearch: false
  };

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;

        this._titleService.setTitle(this.appConfig['displayName']);
        
        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        this.initialLoading();
      }
    });
  }

  initialLoading() {
    this.loadTableData({});
  }

  loadTableData(tableReq) {
    this.tableJson['loading'] = true;
    this.dataFetchServ.getTableData(tableReq).subscribe(data => {
      console.log('data---', data);

      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      } else {
        this.noTableData = false;
      }
      const tableData = data['data'];
      this.apiData = tableData;
      const arr = [];
      tableData.forEach((row: object) => {
        row['ppComplRate'] =  (parseFloat(row['pp_completions']) / parseFloat(row['pp_imp'])) * 100;
        row['todaysBWComplRate'] = (parseFloat(row['today_bw_completions']) / parseFloat(row['today_bw_imp'])) * 100;
        row['totalBWComplRate'] =  (parseFloat(row['total_bw_completions']) / parseFloat(row['total_bw_imp'])) * 100;
        arr.push({
          data: row
        });
      });

      this.tableData = <TreeNode[]>arr;
      this.tableJson['totalRecords'] = arr.length;
      this.tableJson['loading'] = false;
    });

  }

  onFiltersApplied(filterData: object) {
    let filterStr;

    for (let k in filterData['filter']['dimensions']) {
      if(k && filterData['filter']['dimensions'][k] != undefined && filterData['filter']['dimensions'][k].length > 0){
       
        let key = k;
        if(k == 'advertiser') key = "advertiser_name"    
        if(filterStr == undefined)
           filterStr = key+" in ("+filterData['filter']['dimensions'][k].map(x => "'" + x + "'").toString()+")" 
          else
            filterStr = filterStr+" and "+key+" in ("+filterData['filter']['dimensions'][k].map(x => "'" + x + "'").toString()+")"        
        }
      }    
    filterStr ? this.loadTableData({'filter' : filterStr}) : this.loadTableData({});
  }

    sendEmail() {
    const imageConfig = {
      imgName: `${this.appConfig['name']}.png`,
      type: 'image/png'
    };

    this.htmltoimage
      .canvasImage(document.getElementById('capture'), imageConfig)
      .then(canvas => {
        this.dialogService.open(SendEmailComponent, {
          header: 'Email Screenshot',
          contentStyle: { 'max-height': '80vh', overflow: 'auto' },
          data: canvas.toDataURL('image/png')
        });
      });
  }
  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['name']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    } else if (type === 'ppt') {
      const chartElements = document.getElementsByTagName('ym-charts');
      const slides = [];
      [].forEach.call(chartElements, (el, index) => {
        const c = el.getElementsByTagName('canvas')[0];

        slides.push({
          title: `${this.appConfig['name']}`,
          base64data: this.libServ.canvasToImage('white', c),
          width: 8,
          height: 3
        });

        if (index === chartElements.length - 1) {
          const pptConfig = {
            filename: 'Charts',
            logo: {
              visible: true,
              path: '/assets/images/ym.png',
              width: 0.53125,
              height: 0.458333
            },
            slides: slides
          };
          this.pptExport.exportPPT(pptConfig);
        }
      });
    }
    return false;
  }

  exportTable(fileFormat) {
 
    if (this.tableColumnDef.length == 0) {
      this.confirmationService.confirm({
        message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
        header: 'Information',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
        },
        reject: () => {
        }
      });
    } else {      
      if (this.exportRequest['sendEmail'].length > 0) {
        this.toastService.displayToast({
          severity: 'info',
          summary: 'Export Report',
          detail:
            'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
          life: 10000
        });
        const finalColDef = this.libServ.deepCopy(this.tableColumnDef);
            const tableData = this.tableData as [];
            const colDef = this.libServ.deepCopy(finalColDef);
            const sheetDetailsarray = [];
            const sheetDetails = {};
            sheetDetails['columnDef'] = colDef;
            sheetDetails['data'] = this.apiData;
            sheetDetails['sheetName'] = 'Campaign Data';
            sheetDetails['isRequest'] = false;
            sheetDetails['request'] = {
              url: '',
              method: '',
              param: ''
            };
            sheetDetails['disclaimer'] = [
              {
                position: 'top',
                label:    
                  'Data persent in below table is not final and may varies over period of time',
                color: '#3A37CF'
              },
              {
                position: 'bottom',
                label: 'Thank You',
                color: '#EC6A15'
              }
            ];
            sheetDetails['totalFooter'] = {
              available: false,
              custom: false
            };
            sheetDetails['tableTitle'] = {
              available: true,
              label: 'Campaign '
            };
            sheetDetails['image'] = [
              {
                available: true,
                path: 'ExportImages/yuktaone-logo.png',
                position: 'top'
              }
            ];
  
            sheetDetailsarray.push(sheetDetails);
  
           this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
           this.exportRequest['fileName'] =
             'Campaign Fulfilment ' + moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
           this.exportRequest['exportFormat'] = fileFormat;
           this.dataFetchServ
             .getExportReportData(this.exportRequest)
             .subscribe(response => {
               console.log(response);
             });
        }
    }
  }

}