import { Routes } from '@angular/router';
import { DataPipelineAppComponent } from './data-pipeline-app.component';

export const DataPipelineAppRoutesAppRoutes: Routes = [
  {
    path: '',
    component: DataPipelineAppComponent
  }
];
