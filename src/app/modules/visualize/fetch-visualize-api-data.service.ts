import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FetchApiDataService {
  private BASE_URL: string = environment.baseUrl;

  constructor(private http: HttpClient) { }
  getTableData() {
    const url = `${this.BASE_URL}/frankly/v1/common/getVisualizeData`;
    return this.http.get(url);
  }

  getKeyspaceData() {
    const url = `${this.BASE_URL}/frankly/v1/common/getKeyspaceData`;
    return this.http.get(url);
  }
  getKeyspaceById(keyspaceId: number) {
    return this.http.get(
      `${this.BASE_URL}/frankly/v1/visualize/get-keyspace-by-id/${keyspaceId}`
    );
  }

  getVisualizeById(visualizeId: number) {
    return this.http.get(
      `${this.BASE_URL}/frankly/v1/visualize/get-visualize-by-id/${visualizeId}`
    );
  }
  getChartData(params: object, api_url: string) {
    const url = `${this.BASE_URL}${api_url}`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
    // return <TreeNode[]> json.data;
  }

  savevisualize(req) {
    const url = `${this.BASE_URL}/frankly/v1/visualize/saveVisualize`;
    return this.http.post(url, req);
  }

  updatevisualize(req) {
    const url = `${this.BASE_URL}/frankly/v1/visualize/updateVisualize`;
    return this.http.post(url, req);
  }
  deleteVisualize(id) {
    const url = `${this.BASE_URL}/frankly/v1/visualize/deleteVisualize/${id}`;
    return this.http.post(url, id);
  }

  getExportReportData(params: object,backend_url:string) {
    const url = `${this.BASE_URL}${backend_url}`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
    // return <TreeNode[]> json.data;
  }
  getLastUpdatedData() {
    const url = `${this.BASE_URL}/frankly/v1/visualize/getVisualizeLastUpdated`;
    return this.http.get(url);
  }
}
