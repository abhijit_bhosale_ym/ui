import * as core from '@angular/core';
import { Subscription, from } from 'rxjs';
import { Title } from '@angular/platform-browser';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import * as moment from 'moment';

import { CommonLibService, AppConfigService } from 'src/app/_services';
import { FetchApiDataService } from './fetch-visualize-api-data.service';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { FilterDataService } from './../../../app/_services/filter-data/filter-data.service';
import { VisualizeChartPopComponent } from './visualize-chart-pop/visualize-chart-pop.component';
import { Router, ActivatedRoute } from '@angular/router';
import { VisualizeCreateComponent } from './visualize-create/visualize-create.component';


@core.Component({
  selector: 'ym-visualize-app',
  templateUrl: './visualize-app.component.html',
  styleUrls: ['./visualize-app.component.scss'],
  providers: [DialogService]
})
export class VisualizeAppComponent implements core.OnInit, core.OnDestroy {
  private appConfigObs: Subscription;

  appConfig: object = {};
  lastUpdatedOn: Date;
  tableRequestParam = {};
  favoriteCollection = {};
  filtersApplied: object = {};
  allStatus: any;
  messages = [];
  unreadCount = 0;
  nextUpdated: Date;
  dataUpdatedThrough: Date;
  exportRequest: ExportRequest = <ExportRequest>{};

  flatTableReq: object;
  flatTableData: TreeNode[];
  flatTableColumnDef: any[];
  flatTableJson: Object;
  tblResData: any[];
  displayAggTable = true;
  noTableData = false;
  isVisualiseCreate = false;

  constructor(
    private confirmationService: ConfirmationService,

    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private toastService: ToastService,
    private dialogService: DialogService,
    private formatNumPipe: FormatNumPipe,
    private dataFetchServ: FetchApiDataService,
    private exportService: ExportdataService,
    private currentActivatedRoute: ActivatedRoute,
    public router: Router
  ) {


  }

  ngOnInit() {

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {

      console.log('appconfig', appConfig);

      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        this._titleService.setTitle(this.appConfig['displayName']);


        this.filtersApplied = {
          timeKeyFilter: {
          },
          filters: { dimensions: [], metrics: [] },
          groupby: this.appConfig['filter']['filterConfig']['groupBy']
        };


        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        this.initialLoading();
      }
    });


    this.flatTableColumnDef = [
      {
        field: 'title',
        displayName: 'Chart Title',
        format: '',
        width: '250',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'chart_type',
        displayName: 'Chart Type',
        format: '',
        width: '150',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'description',
        displayName: 'Description',
        format: '',
        width: '300',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'keyspace',
        displayName: 'Key Space',
        format: '',
        width: '160',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      }, {
        field: 'view',
        displayName: 'View',
        format: '',
        width: '50',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'edit',
        displayName: 'Edit',
        format: '',
        width: '50',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: false
        }
      }, {
        field: 'delete',
        displayName: 'Delete',
        format: '',
        width: '55',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'name',
        displayName: 'Created By',
        format: '',
        width: '135',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      }, {
        field: 'insert_at',
        displayName: 'Creation Date',
        format: '',
        width: '170',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      }, {
        field: 'update_at',
        displayName: 'Last Updated',
        format: '',
        width: '170',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      }

    ];

    this.flatTableJson = {
      page_size: 10,
      page: 0,
      lazy: false,
      loading: false,
      export: false,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '450px',
      totalRecords: 1000,
      columns: this.flatTableColumnDef.slice(1),
      selectedColumns: this.flatTableColumnDef.slice(1),
      globalFilterFields: this.flatTableColumnDef.slice(0),
      frozenCols: [this.flatTableColumnDef[0]],
      // frozenWidth: '250px',
      frozenWidth:
        this.flatTableColumnDef
          .slice(0, 1)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      // columns: this.flatTableColumnDef.slice(this.flatTableColumnDef.length-1),
      // selectedColumns: [this.flatTableColumnDef],
      // frozenCols: [this.flatTableColumnDef[0]],
      // frozenWidth:
      //   this.flatTableColumnDef
      //     .slice(0, 1)
      //     .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: true
    };
    console.log('loooo', this.flatTableJson);
  }

  initialLoading() {
    this.dataFetchServ
      .getLastUpdatedData()
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        this.lastUpdatedOn = moment(data['data']).toDate();

      });
    if (localStorage.getItem('chartType') == 'update' || localStorage.getItem('chartType') == 'new' || localStorage.getItem('chartType') == 'updateInDashboard' ) {
      this.isVisualiseCreate = true;
    } else {
      this.isVisualiseCreate = false;
      this._titleService.setTitle(this.appConfig['displayName']);
      this.getTableData();
    }
  }


  getTableData() {

    this.dataFetchServ.getTableData().subscribe(data => {
      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      } else {
        this.noTableData = false;
      }
      const arr = [];
      const totalsColData = data['data'];
      for (const r of totalsColData) {
        const obj = {};
        obj['data'] = r;
        arr.push(obj);
      }
      // this.tblResData = arr;

      this.flatTableData = <TreeNode[]>arr;
      this.flatTableJson['totalRecords'] = totalsColData.length;
      this.flatTableJson['loading'] = false;
    });
  }



  reLoadTableData() {
    const tableReq = this.libServ.deepCopy(this.tableRequestParam);
    tableReq.filters = this.libServ.deepCopy(this.filtersApplied['filters']);
  }

  NewVisualizations() {
    // const data = {
    //   isEdit: false,
    //   data: this.flatTableData,
    //   table: this.noTableData
    // };

    // const ref = this.dialogService.open(VisualizeChartPopComponent, {
    //   data: data,
    //   header: 'Chart & Keyspace Selection',
    //   contentStyle: { width: '60vw', overflow: 'auto' },
    // });

    // ref.onClose.subscribe((data: boolean) => {
    //   console.log('data', data);
    //   this.isVisualiseCreate = data;
    // }
    // );

    localStorage.setItem('chartType', 'new');
    this.isVisualiseCreate = true;
  }

  exportTable(fileFormat) {
    const data = [];
    const finalColDef = this.libServ.deepCopy(this.flatTableColumnDef);

    const req = {
      'filters': this.filtersApplied['filters']['dimensions']
    };

    this.dataFetchServ
      .getTableData()
      .subscribe(response => {
        if (response['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          return;
        }
        data.push({
          data: [
            {
              data: response['data'],
              columnDefs: finalColDef
            }
          ],
          sheetname: 'RIO'
        });

        this.exportService.exportReport(
          data,
          `Received_IO_${moment(new Date()).format('MM-DD-YYYY HH:mm:ss')}`,
          fileFormat,
          false
        );

      });
  }

  onFiltersApplied(filterData: object) {
    this.filtersApplied['filters']['dimensions'] = [];
    for (const k in filterData['filter']['dimensions']) {
      if (filterData['filter']['dimensions'][k].length !== 0) {
        this.filtersApplied['filters']['dimensions'].push({
          key: k,
          values: filterData['filter']['dimensions'][k]
        });
      }
    }

    this.getTableData();
  }

  deleteVisualise(rowData) {

    const msg = 'Do you want to delete visualize graph?';

    this.confirmationService.confirm({
      message: msg,
      header: rowData['title'],
      accept: () => {
        this.dataFetchServ.deleteVisualize(rowData['id']).subscribe(data => {
          if (data['status']) {
            this.toastService.displayToast({
              severity: 'success',
              summary: 'Visualize graph Delete',
              detail: 'Visualize graph delete successfully'
            });
            this.getTableData();
          } else {
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Server Error',
              detail: 'Please refresh the page'
            });

          }
        });

      },
      reject: () => {

      }
    });

  }

  editVisualise(rowData) {
    localStorage.setItem('chartType', 'update');
    localStorage.setItem('keyspaceId', rowData['id']);
    this.isVisualiseCreate = true;

  }

  viewVisualise(rowData) {
    localStorage.setItem('chartType', 'view');
    localStorage.setItem('keyspaceId', rowData['id']);
    const ref = this.dialogService.open(VisualizeCreateComponent, {
      data: {},
      header: rowData.title,
      contentStyle: { width: '80vw', overflow: 'auto' },
    });

    ref.onClose.subscribe((data: string) => {
    });
  }

  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
      if (localStorage.getItem('chartType')) {
        localStorage.removeItem('chartType');
      }
      if (localStorage.getItem('keyspaceId')) {
        localStorage.removeItem('keyspaceId');
      }
    }
  }
}
