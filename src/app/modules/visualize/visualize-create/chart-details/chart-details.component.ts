import { Component, OnInit, OnDestroy } from '@angular/core';
import { SelectItemGroup, ConfirmationService } from 'primeng/api';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { FetchApiDataService } from './../../fetch-visualize-api-data.service';
import { CommonLibService } from 'src/app/_services';
import { DynamicDialogConfig, DynamicDialogRef, DialogService } from 'primeng/dynamicdialog';
@Component({
  selector: 'ym-chart-details',
  templateUrl: './chart-details.component.html',
  styleUrls: ['./chart-details.component.scss']
})
export class ChartDetailsComponent implements OnInit {
  // private appConfigObs: Subscription;

  appConfig: object = {};
  registerForm: FormGroup;
  isCreateRole = false;
  isCreateTeam = false;
  groupedDaterange: any[];
  fileName = '';
  paymentMethodLists: any[];
  fileData: File;
  userDetails: {
    name: '';
  };
  selectedRange: object = {};
  RowData: object = {};
  constructor(
    private formBuilder: FormBuilder,
    public router: Router,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private confirmationService: ConfirmationService,
    private libServ: CommonLibService,
    private config: DynamicDialogConfig,
    private dynamicDialogRef: DynamicDialogRef
  ) {
    this.RowData = this.config.data;
    // this.paymentMethodLists = this.RowData['paymentMethodLists'];
  }

  ngOnInit() {
    this.groupedDaterange = [
      { key: 'today', label: 'Today' },
      { key: 'yesterday', label: 'Yesterday' },
      { key: 'last_7_days', label: 'Last 7 days' },
      { key: 'last_week', label: 'Last Week' },
      { key: 'last_month', label: 'Last Month' },
      { key: 'last_30_days', label: 'Last 30 days' },
      { key: 'this_month', label: 'This Month' },
    ];
    this.registerForm = this.formBuilder.group({
      chartTitle: [this.RowData['title'], [Validators.required]],
      defaultDaterange: [this.groupedDaterange[
        this.groupedDaterange.findIndex(
          item => item['key'] == this.RowData['chart_config']['defualt_daterange']
        )
      ], [Validators.required]],
      description: [this.RowData['description']]
    });


    if (this.RowData['isUpdate']) {
      this.selectedRange = this.groupedDaterange[
        this.groupedDaterange.findIndex(
          item => item['key'] == this.RowData['chart_config']['defualt_daterange']
        )
      ];
    }
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {
    console.log('form...', this.registerForm)
    this.RowData['chart_config']['defualt_daterange'] = this.registerForm.value.defaultDaterange.key;
    this.RowData['chart_config'] = JSON.stringify(this.RowData['chart_config']);
    this.RowData['title'] = this.registerForm.value.chartTitle;
    this.RowData['description'] = this.registerForm.value.description;
    console.log('rowdata', this.RowData)
    if (this.RowData['isUpdate']) {
      this.dataFetchServ.updatevisualize(this.RowData).subscribe(data => {
        if (data['status']) {
          this.toastService.displayToast({
            severity: 'success',
            summary: 'Visualization Update',
            detail: 'Visualization updated successfully'
          });
          if (localStorage.getItem('chartType') == 'updateInDashboard') {
            if (localStorage.getItem('chartType')) {
              localStorage.removeItem('chartType');
            }
            if (localStorage.getItem('keyspaceId')) {
              localStorage.removeItem('keyspaceId');
            }
            this.dialogService.dialogComponentRef.destroy();
            this.router.navigate(['/ad-hoc-dashboard']);
          } else {
            if (localStorage.getItem('chartType')) {
              localStorage.removeItem('chartType');
            }
            if (localStorage.getItem('keyspaceId')) {
              localStorage.removeItem('keyspaceId');
            }
            this.dialogService.dialogComponentRef.destroy();
            this.router.navigate(['/visualize']);
          }

        } else {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
        }
      });

    } else {
      this.dataFetchServ.savevisualize(this.RowData).subscribe(data => {
        if (data['status']) {
          this.toastService.displayToast({
            severity: 'success',
            summary: 'Visualization Save',
            detail: 'Visualization saved successfully'
          });
          if (localStorage.getItem('chartType') == 'updateInDashboard') {
            if (localStorage.getItem('chartType')) {
              localStorage.removeItem('chartType');
            }
            if (localStorage.getItem('keyspaceId')) {
              localStorage.removeItem('keyspaceId');
            }
            this.dialogService.dialogComponentRef.destroy();
            this.router.navigate(['/ad-hoc-dashboard']);
          } else {
            if (localStorage.getItem('chartType')) {
              localStorage.removeItem('chartType');
            }
            if (localStorage.getItem('keyspaceId')) {
              localStorage.removeItem('keyspaceId');
            }
            this.dialogService.dialogComponentRef.destroy();
            this.router.navigate(['/visualize']);
          }
        } else {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });

        }
      });

    }


  }

  goBack() {
    // this.router.navigate(['/users']);
    this.dialogService.dialogComponentRef.destroy();
  }

}
