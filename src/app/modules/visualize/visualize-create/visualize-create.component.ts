import { Component, OnInit, OnDestroy } from '@angular/core';
import { SelectItemGroup, ConfirmationService } from 'primeng/api';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { FetchApiDataService } from '../fetch-visualize-api-data.service';
import { CommonLibService, AppConfigService } from 'src/app/_services';
import { DialogService } from 'primeng/dynamicdialog';
import { Title } from '@angular/platform-browser';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import * as moment from 'moment';
import { Subscription } from 'rxjs';
import { ChartDetailsComponent } from './chart-details/chart-details.component';
import { Swappable } from '@shopify/draggable';
import { numberToWords } from 'number-to-words';
import { CustomDateRangeComponent } from '../../ad-hoc-dashboard/dashboard-create/custom-daterange/custom-daterange.component';
import { TreeNode } from 'primeng/api';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
// import { setTimeout } from 'timers';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-visualize-create',
  templateUrl: './visualize-create.component.html',
  styleUrls: ['./visualize-create.component.scss'],
  providers: [DialogService]
})
export class VisualizeCreateComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;

  lastUpdatedOn: Date = new Date();
  appConfig: object = {};
  keySpaces: object = {};
  keySpacesOriginal: object = {};
  chartType: string;
  keyspaceId: number;
  showChart = false;
  noDataChart = true;
  chartJson: object;
  chartJsonMain: object;
  defualtChartJson: object;
  groupedAggregation: any[];
  groupedField: object = {};
  groupedAggregationBucket: any[];
  groupedFieldBucket: object = {};
  groupedOrderBy: any[];
  groupedTableOrderBy: any[];
  groupedOrder: any[];
  groupedLineMode: any[];
  groupedMode: any[];
  groupedChartType: any[];
  groupedValueAxis: any[];
  groupedLegendPosition: any[];
  groupedYaxisPosition: any[];
  groupedXaxisPosition: any[];
  groupedAxisMode: any[];
  groupedScaleType: any[];
  groupedAlign: any[];
  groupedYAxisLine: any[];
  groupedLineStyle: any[];
  groupedNumberPosition: any[];
  groupedOrderByMetrics: any[];
  listMatrics: any[];
  listDerivedMatrics: any[];
  listDate: any[];
  listDimension: any[];
  chatConfig: object = {};
  sampleChatConfig: object = {};
  defualtChatConfig: object = {};
  apiRequest: object = {};
  filtersApplied: object = {};
  isUpdate = false;
  isValid = false;
  isMetricValid: object = {};
  isBucketAggregationValid: object = {};
  isBucketFieldValid: object = {};
  isBucketFieldOrderValid: object = {};
  title = '';
  description = '';
  visualizeId: number;
  isChange = false;
  selectedCardPosition: string;
  hoverValue: object = {};
  VisaulisationType = 'new';
  sliceYAxis = 'y_axis_1';
  keySpacesData = [];
  index: number = 0;
  isCreate = true;
  chartTypeData = [{
    chart_type: 'bar',
    display_name: 'Vertical Bar',
    icon_class: 'fas fa-poll',
    info: 'A bar chart or bar graph is a chart that presents Grouped data with rectangular bars with lengths proportional to the values that they represent.'
  }, {
    chart_type: 'line',
    display_name: 'Line',
    icon_class: 'fas fa-chart-line',
    info: 'A line chart or line graph is a type of chart which displays information as a series of data points called \'markers\' connected by straight line segments.'
  },
  {
    chart_type: 'area',
    display_name: 'Area',
    icon_class: 'fas fa-chart-area',
    info: 'Emphasize the quantity beneath a line chart'
  },
  {
    chart_type: 'pie',
    display_name: 'Pie',
    icon_class: 'fas fa-chart-pie',
    info: 'A doughnut chart is a variant of the pie chart, with a blank center allowing for additional information about the data as a whole to be included.'
  },
  {
    chart_type: 'cards_panel',
    display_name: 'Cards Panel',
    icon_class: 'fas fa-id-card',
    info: 'Display a calculation as a single number'
  },
  {
    chart_type: 'polarArea',
    display_name: 'Polar Area',
    icon_class: 'fas fa-radiation-alt',
    info: 'Polar area charts are similar to pie charts, but each segment has the same angle - the radius of the segment differs depending on the value.'
  },
  {
    chart_type: 'radar',
    display_name: 'Radar',
    icon_class: 'fas fa-podcast',
    info: 'A radar chart is a graphical method of displaying multivariate data in the form of a two-dimensional chart of three or more quantitative variables represented on axes starting from the same point.'
  }, {
    chart_type: 'data_table',
    display_name: 'Data Table',
    icon_class: 'fas fa-table',
    info: 'Display value in table'
  }, {
    chart_type: 'horizontalBar',
    display_name: 'Horizontal Bar',
    icon_class: 'fas fa-poll-h',
    info: 'A bar chart or bar graph is a chart that presents Grouped data with rectangular bars with lengths proportional to the values that they represent.'
  }
  ];
  customize_daterange: object = {
    time_key1: '',
    time_key2: '',
    label: '',
    date_range: '',
    is_show: false,
    isremove: true
  };

  exportRequest: ExportRequest = <ExportRequest>{};

  constructor(
    private formBuilder: FormBuilder,
    public router: Router,
    private route: ActivatedRoute,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private confirmationService: ConfirmationService,
    public libServ: CommonLibService,
    private _titleService: Title,
    private formatNumPipe: FormatNumPipe,
    private appConfigService: AppConfigService
  ) {

  }

  ngOnInit() {
    this.VisaulisationType = localStorage.getItem('chartType');
    if (this.VisaulisationType == 'view') {
      this.isCreate = false;
    }
    this.hoverValue = {
      info: 'Start creating your visualization by selecting a type for that visualization.',
      display_name: 'Select a visualization type'
    };

    this.sampleChatConfig = {
      'line_bar_area': {
        'data': {
          'metrics': [{
            'name': 'Y-axis 1',
            'aggregation': 'sum',
            'field': '',
            'custom_label': '',
            'isOpen': true,
            'isVisible': true,
            'isAxisSetting': false,
            'axis_value': 'LeftAxis-1',
            'chart_type': 'bar',
            'mode': 'Normal',
            'legend_color': '#3366CC',
            'isSlice': true,
            'show_line': {
              'is_show': true,
              'line_mode': 'Smoothed',
              'line_width': '2'
            },
            'show_dot': true,
            'position': 'left',
            'scale_type': 'Linear',
            'show_axis_lines_label': {
              'is_show': true,
              'title': 'sum',
              'show_label': {
                'is_show': true,
                'align': { minRotation: 0, maxRotation: 0 },
                'truncate': 100,
                'isfilter_lable': false
              }
            },
            'custom_extend': {
              'is_show': false,
              'scale_to_bound': {
                'is_show': false,
                'margin': ''
              },
              'axis_extend': {
                'is_show': false,
                'min': '',
                'max': ''
              }
            },
          }, {
            'name': 'Y-axis 2',
            'aggregation': 'sum',
            'field': '',
            'custom_label': '',
            'isOpen': true,
            'isVisible': false,
            'isAxisSetting': false,
            'axis_value': 'RightAxis-1',
            'chart_type': 'line',
            'mode': 'Normal',
            'legend_color': '#7fdbff',
            'isSlice': false,
            'show_line': {
              'is_show': true,
              'line_mode': 'Smoothed',
              'line_width': '2'
            },
            'show_dot': true,
            'position': 'right',
            'scale_type': 'Linear',
            'show_axis_lines_label': {
              'is_show': true,
              'title': 'sum',
              'show_label': {
                'is_show': true,
                'align': { minRotation: 0, maxRotation: 0 },
                'truncate': 100,
                'isfilter_lable': false
              }
            },
            'custom_extend': {
              'is_show': false,
              'scale_to_bound': {
                'is_show': false,
                'range': ''
              },
              'axis_extend': {
                'is_show': false,
                'min': '',
                'max': ''
              }
            },
          }],
          'buckets': [{
            'aggregation': '',
            'field': '',
            'order_by': {
              'key': 0,
              'aggregation': 'sum',
              'field': '',
              'mode': 'desc',
              'size': ''
            },
            'custom_label': '',
            'advanced': {
              'is_show': false,
              'exclude': '',
              'include': ''
            },
            'isOpen': true,
            'isVisible': true,
            'isAxisSetting': false,

            'position': 'bottom',
            'show_axis_lines_label': {
              'is_show': true,
              'title': '',
              'show_label': {
                'is_show': true,
                'align': { minRotation: 0, maxRotation: 90 },
                'truncate': 100,
                'isfilter_lable': false
              }
            },
            'slice_data': {
              'is_show': false,
              'aggregation': '',
              'field': '',
              'order_by': {
                'key': 0,
                'aggregation': 'sum',
                'field': '',
                'mode': 'desc',
                'size': ''
              },
              'isStacked': true
            }
          }]
        },
        'panel_setting': {
          'settings': {
            'legend_position': 'top',
            'show_tooltips': true,
            'order_bucket_by_sum': false,
            'show_value_on_chart': false,
            'calculate_value': {
              'is_show': false,
              'min': false,
              'med': false,
              'max': false,
              'avg': false
            }
          },
          'grid': {
            'x_axis_lines': 'false',
            'y_axis_lines': 'Dont Show'
          },
          'threshold_line': {
            'is_show': false,
            'threshold_value': 1,
            'line_width': 1,
            'line_style': 'solid',
            'line_color': '#bf0720'
          }
        }

      },
      'pie': {
        'data': {
          'metrics': [{
            'aggregation': 'sum',
            'field': '',
            'custom_label': '',
            'isOpen': true,
            'isVisible': true
          }],
          'buckets': [{
            'aggregation': '',
            'field': '',
            'order_by': {
              'key': 0,
              'aggregation': 'sum',
              'field': '',
              'mode': 'desc',
              'size': ''
            },
            'group_separate_bucket': {
              'is_show': false,
              'label': 'other'
            },
            'missing_value': {
              'is_show': false,
              'label': 'missing'
            },
            'custom_label': '',
            'advanced': {
              'is_show': false,
              'exclude': '',
              'include': ''
            },
            'isOpen': true,
            'isVisible': true
          }]
        },
        'panel_setting': {
          'pie_settings': {
            'legend_position': 'top',
            'show_tooltips': true,
            'donut': false
          },
          'label_settings': {
            'show_labels': true,
            'show_grids': true,
            'show_values': true,
            'truncate': '100'
          },
        }

      },
      'cards_panel': {
        'data': {
          'metrics': [{
            'aggregation': 'sum',
            'field': '',
            'custom_label': '',
            'icon_class': 'fas fa-info-circle',
            'card_color': '#6795f0',
            'isVisible': true,
            'isOpen': true,

          }]
        }
      },
      'radar': {
        'data': {
          'metrics': [{
            'aggregation': 'sum',
            'field': '',
            'custom_label': '',
            'isOpen': true,
            'isVisible': true
          }],
          'buckets': [{
            'aggregation': '',
            'field': '',
            'order_by': {
              'key': 0,
              'aggregation': 'sum',
              'field': '',
              'mode': 'desc',
              'size': ''
            },
            'custom_label': '',
            'isOpen': true
          }]
        },
        'panel_setting': {
          'radar_settings': {
            'legend_position': 'top',
            'show_tooltips': true,
            'tick_show': true,
            'axis_extend': {
              'is_show': false,
              'min': '',
              'max': ''
            }
          },
          'label_settings': {
            'show_labels': true,
            'show_top_levels': true,
            'show_values': true,
            'truncate': '100'
          },
        }

      },
      'data_table': {
        'data': {
          'metrics': [{
            'aggregation': 'sum',
            'field': '',
            'custom_label': '',
            'width': '150',
            'options': {
              'editable': false,
              'colSearch': false,
              'colSort': true,
              'resizable': true,
              'movable': false
            },
            'isOpen': true,
            'isVisible': true
          }],
          'buckets': [{
            'aggregation': '',
            'field': '',
            'custom_label': '',
            'width': '150',
            'options': {
              'editable': false,
              'colSearch': false,
              'colSort': true,
              'resizable': true,
              'movable': false,
              'isfrozen': false
            },
            'isOpen': true,
            'isVisible': true
          }]
        },
        'table_setting': {
          'setting': {

            'export': true,
            'resizableColumns': true,
            'scrollHeight': '400',
            'showHideCols': true,
            'overallSearch': true,
            'columnSearch': false
          },
          'data_setting': {
            'page_size': 10,
            'limit': '',
            'order': []
          }
        }

      },
      'horizontalBar': {
        'data': {
          'metrics': [{
            'name': 'Y-axis 1',
            'aggregation': 'sum',
            'field': '',
            'custom_label': '',
            'isOpen': true,
            'isVisible': true,
            'isAxisSetting': false,
            'axis_value': 'LeftAxis-1',
            'mode': 'Normal',
            'legend_color': '#3366CC',
            'position': 'bottom',
            'show_axis_lines_label': {
              'is_show': true,
              'title': 'sum',
              'show_label': {
                'is_show': true,
                'align': {
                  minRotation: 0,
                  maxRotation: 90
                },
                'truncate': 100
              }
            },
            'custom_extend': {
              'is_show': false,
              'scale_to_bound': {
                'is_show': false,
                'margin': ''
              },
              'axis_extend': {
                'is_show': false,
                'min': '',
                'max': ''
              }
            },
          }],
          'buckets': [{
            'aggregation': '',
            'field': '',
            'order_by': {
              'key': 0,
              'aggregation': 'sum',
              'field': '',
              'mode': 'desc',
              'size': ''
            },
            'custom_label': '',
            'advanced': {
              'is_show': false,
              'exclude': '',
              'include': ''
            },
            'isOpen': true,
            'isVisible': true,
            'isAxisSetting': false,
            'position': 'left',
            'show_axis_lines_label': {
              'is_show': true,
              'title': '',
              'show_label': {
                'is_show': true,
                'align': {
                  minRotation: 0,
                  maxRotation: 0,
                },
                'truncate': 100,
                'isfilter_lable': false
              }
            },
            'slice_data': {
              'is_show': false,
              'aggregation': '',
              'field': '',
              'order_by': {
                'key': 0,
                'aggregation': 'sum',
                'field': '',
                'mode': 'desc',
                'size': ''
              },
              'isStacked': true
            }
          }]
        },
        'panel_setting': {
          'settings': {
            'legend_position': 'top',
            'show_tooltips': true,
            'order_bucket_by_sum': false,
            'show_value_on_chart': false,
            'calculate_value': {
              'is_show': false,
              'min': false,
              'med': false,
              'max': false,
              'avg': false
            }
          },
          'grid': {
            'x_axis_lines': false,
            'y_axis_lines': true
          },
          'threshold_line': {
            'is_show': false,
            'threshold_value': 1,
            'line_width': 1,
            'line_style': 'solid',
            'line_color': '#bf0720'
          }
        }

      }
    };

    this.defualtChatConfig = this.libServ.deepCopy(this.sampleChatConfig);
    this.getKeySpacesData();
    this.filtersApplied = {
      timeKeyFilter: {
        time_key1: moment().subtract(2, 'months').startOf('month').format('YYYYMMDD'),
        time_key2: moment().format('YYYYMMDD')
      },
      filters: { dimensions: [], metrics: [] },
    };
    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        // this._titleService.setTitle('Create Visualization');
        let startDate;
        let endDate;
        const date_config = this.appConfig['filter']['filterConfig']['filters'][
          'datePeriod'
        ][0];
        if ((date_config['defaultDate'][0]['value'] === 0 && date_config['defaultDate'][0]['period'] === 'month') && (moment().format('DD') === '01' || moment().format('DD') === '02')) {
          startDate = moment().subtract(1, 'months').startOf('month');
          endDate = moment().subtract(1, 'months').endOf('month');
        } else {
          if (
            date_config[
            'defaultDate'
            ][0]['startOf']
          ) {
            startDate = moment()
              .subtract(
                date_config['defaultDate'][0]['value'],
                date_config['defaultDate'][0]['period']
              )
              .startOf(
                date_config['defaultDate'][0]['period']
              );
          } else {
            startDate = moment().subtract(
              date_config['defaultDate'][0]['value'],
              date_config['defaultDate'][0]['period']
            );
          }
          endDate = moment().subtract(
            date_config[
            'defaultDate'
            ][1]['value'],
            date_config[
            'defaultDate'
            ][1]['period']
          );
        }

        // this.filtersApplied = {
        //   timeKeyFilter: {
        //     time_key1: startDate.format('YYYYMMDD'),
        //     time_key2: endDate.format('YYYYMMDD')
        //   },
        //   filters: { dimensions: [], metrics: [] },
        //   groupby: []
        // };
        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');

      }
    });

  }

  ngAfterViewInit() {
    const tableTh: any = document.querySelectorAll('.table-head');
    const tableTh1 = document.querySelector('.dashboard-table1 .ui-treetable-caption');
    const scrollHeight1: any = document.querySelectorAll('body .dashboard-table1 .ui-treetable-scrollable-body');
    let thArray = [40];
    for (const th of tableTh) {
      thArray.push(th.offsetHeight);
    }
    const maxheight = thArray.reduce((a, b) => Math.max(a, b));
    let extraHeight = 143;
    if (this.chatConfig['table_setting']['setting']['columnSearch']) {
      extraHeight = 184;
    }
    this.chartJson['flatTableJson']['scrollHeight'] = (526 - extraHeight - maxheight) + 'px !important';
    setTimeout(() => {
      for (const scrollHeight of scrollHeight1) {
        (scrollHeight as HTMLElement).style.cssText = 'max-height:' + (526 - extraHeight - maxheight) + 'px !important';
      }
      for (const th of tableTh) {
        (th as HTMLElement).style.cssText = 'height:' + maxheight + 'px !important';
        // console.log('th', th)
      }
    }, 0);
  }
  onColumnResize() {
    setTimeout(() => {
      this.ngAfterViewInit();
    }, 0);

  }
  getKeySpacesData() {
    this.dataFetchServ.getKeyspaceData().subscribe(data => {
      this.keySpacesData = data as [];
      this.initialLoading();
    });
  }
  mouseEnter(i) {
    this.hoverValue = this.chartTypeData[i];
  }
  mouseLeave() { 
    if (this.chartType) {
      this.hoverValue = this.chartTypeData.find(x => x.chart_type == this.chartType);
    } else {
      this.hoverValue = {
        info: 'Start creating your visualization by selecting a type for that visualization.',
        display_name: 'Select a visualization type'
      };
    }
  }
  keySpacesChange() {
    this.loadKeyspacesData();
    this.loadCharts().then((data) => {
      if (!this.libServ.isEmptyObj(this.chatConfig)) {
        this.index = 0;
        setTimeout(() => {
          this.index = 1;
        }, 0);
      }
    })

    this.showChart = false;
    this.noDataChart = true;
  }
  changeChartType(chart) {
    console.log('key', this.chartType);
    if (!this.libServ.isEmptyObj(this.chatConfig) && this.chartType != undefined) {
      this.confirmationService.confirm({
        message: 'Are you sure you want to change chart type?',
        accept: () => {
          this.selectType(chart);
        },
        reject: () => {

        }
      });
    } else {
      this.selectType(chart);
    }
  }

  changeKeySpaces(event) {
    console.log('key', this.keySpaces);
    if (!this.libServ.isEmptyObj(this.keySpacesOriginal)) {
      this.confirmationService.confirm({
        message: 'Are you sure you want to change keyspace?',
        accept: () => {
          this.keySpacesOriginal = this.keySpaces;
          this.keySpacesChange();
        },
        reject: () => {
          this.keySpaces = this.keySpacesOriginal;
        }
      });
    } else {
      this.keySpacesOriginal = this.keySpaces;
      this.keySpacesChange();
    }
  }
  selectType(chart) {
    console.log('keySpaces', this.keySpaces)
    if (!this.libServ.isEmptyObj(this.chatConfig)) {
      if (this.chartType == 'pie' || this.chartType == 'polarArea') {
        this.sampleChatConfig['pie'] = this.chatConfig;
      } else if (this.chartType == 'cards_panel') {
        this.sampleChatConfig['cards_panel'] = this.chatConfig;
      } else if (this.chartType == 'radar') {
        this.sampleChatConfig['radar'] = this.chatConfig;
      } else if (this.chartType == 'data_table') {
        this.sampleChatConfig['data_table'] = this.chatConfig;
      } else if (this.chartType == 'horizontalBar') {
        this.sampleChatConfig['horizontalBar'] = this.chatConfig;
      } else {
        this.sampleChatConfig['line_bar_area'] = this.chatConfig;
      }
    }
    this.chartType = chart.chart_type;
    if (!this.libServ.isEmptyObj(this.keySpaces)) {
      this.keySpacesChange();
      // setTimeout(() => {
      //   this.index = 1;
      // }, 0);
      this.showChart = false;
      this.noDataChart = true;
    }
  }


  loadCharts() {
    return new Promise(resolve => {
      this.groupedAggregation = this.chartType == 'cards_panel' ? [
        { label: 'Average', value: 'mean' },
        // { label: 'Count', value: 'count' },
        { label: 'Max', value: 'max' },
        { label: 'Min', value: 'min' },
        { label: 'Standard Deviation', value: 'std' },
        { label: 'Sum', value: 'sum' },
        { label: 'Variance', value: 'var' }
      ] : [
          { label: 'Average', value: 'mean' },
          { label: 'Count', value: 'count' },
          { label: 'Max', value: 'max' },
          // { label: 'Median', value: 'Median' },
          { label: 'Min', value: 'min' },
          // { label: 'Percentile Ranks', value: 'Percentile Ranks' },
          // { label: 'Percentiles', value: 'Percentiles' },
          { label: 'Standard Deviation', value: 'std' },
          { label: 'Sum', value: 'sum' },
          // { label: 'Top Hit', value: 'Top Hit' },
          // { label: 'Unique Count', value: 'Unique Count' }
          { label: 'Variance', value: 'var' },
          // { label: 'Size', value: 'size' }
        ];
      if (this.chartType == 'pie' || this.chartType == 'polarArea') {
        this.chatConfig = this.libServ.deepCopy(this.sampleChatConfig['pie']);
      } else if (this.chartType == 'cards_panel') {
        this.chatConfig = this.libServ.deepCopy(this.sampleChatConfig['cards_panel']);
      } else if (this.chartType == 'radar') {
        this.chatConfig = this.libServ.deepCopy(this.sampleChatConfig['radar']);
      } else if (this.chartType == 'data_table') {
        this.chatConfig = this.libServ.deepCopy(this.sampleChatConfig['data_table']);
      } else if (this.chartType == 'horizontalBar') {
        this.chatConfig = this.libServ.deepCopy(this.sampleChatConfig['horizontalBar']);
      } else {
        this.chatConfig = this.libServ.deepCopy(this.sampleChatConfig['line_bar_area']);
        if (this.isChange || !this.isUpdate) {
          this.chatConfig['data']['metrics'][0]['chart_type'] = this.chartType;
        }
      }
      this.chatConfig['data']['metrics'].forEach((ele, i) => {
        if (ele.aggregation != '') {
          this.aggregationChange(i);
        }
      });
      if (this.chartType == 'pie' || this.chartType == 'polarArea') {
        this.chatConfig['data']['buckets'].forEach((ele, i) => {
          if (ele.aggregation != '') {
            this.aggregationBucketChange(i);
          }
        });
        this.chatConfig = this.sampleChatConfig['pie'];
        // this.runPieChartQuery();

      } else if (this.chartType == 'cards_panel') {
        this.chatConfig = this.sampleChatConfig['cards_panel'];
        // this.runCardsPanelQuery();
      } else if (this.chartType == 'radar') {
        this.chatConfig['data']['buckets'].forEach((ele, i) => {
          if (ele.aggregation != '') {
            this.aggregationBucketChange(i);
          }
        });
        this.chatConfig = this.sampleChatConfig['radar'];
      } else if (this.chartType == 'data_table') {
        this.chatConfig['data']['buckets'].forEach((ele, i) => {
          if (ele.aggregation != '') {
            this.aggregationDimensionChange(i);
          }
        });
        this.chatConfig = this.sampleChatConfig['data_table'];
      } else {
        let bucketData = [];
        bucketData.push(this.chatConfig['data']['buckets'][0]);
        if (this.chatConfig['data']['buckets'][0]['slice_data']['is_show']) {
          bucketData.push(this.chatConfig['data']['buckets'][0]['slice_data']);
        }
        bucketData.forEach((ele, i) => {
          if (ele.aggregation != '') {
            this.aggregationBucketChange(i);
          }
        });
        if (this.chartType == 'horizontalBar') {
          this.chatConfig = this.sampleChatConfig['horizontalBar'];
        } else {
          this.chatConfig = this.sampleChatConfig['line_bar_area'];
          this.chatConfig['data']['metrics'][1]['isAxisSetting'] = false;
        }
        this.chatConfig['data']['metrics'][0]['isAxisSetting'] = false;
        this.chatConfig['data']['buckets'][0]['isAxisSetting'] = false;

        if (this.isChange || !this.isUpdate) {
          this.chatConfig['data']['metrics'][0]['chart_type'] = this.chartType;
        }
        // this.runChartQuery();
      }

      resolve('done');
    });
  }

  loadKeyspacesData() {
    let field: SelectItemGroup[] = [];
    this.listDate = [];
    this.listMatrics = [];
    this.listDimension = [];
    this.listDerivedMatrics = [];
    this.groupedFieldBucket = [];
    console.log('keySpaces', this.keySpaces);
    if (typeof this.keySpaces['field_config'] === 'string') {
      this.keySpaces['field_config'] = JSON.parse(this.keySpaces['field_config']);
    }
    this.keySpaces['field_config'].forEach(element => {
      if (element['columnType'] == 'date') {
        this.listDate.push({ label: element['displayName'], value: element['field'] });
      } else if (element['columnType'] == 'metrics') {
        this.listMatrics.push({ label: element['displayName'], value: element['field'] });
      } else if (element['columnType'] == 'derived_metrics') {
        this.listDerivedMatrics.push({ label: element['displayName'], value: element['field'] });
      } else if (element['columnType'] == 'dimensions') {
        this.listDimension.push({ label: element['displayName'], value: element['field'] });
      }
    });
    field.push({ label: 'Metrics', value: 'Metrics', items: this.listMatrics });
    if (this.listDerivedMatrics.length > 0) {
      field.push({ label: 'Derived Metric', value: 'Derived Metric', items: this.listDerivedMatrics });
    }
    // field.push({ label: 'Date', value: 'Date', items: this.listDate });
    this.groupedField[0] = field;
    this.groupedField[1] = field;
    this.groupedOrderByMetrics = field;
  }
  Validation() {
    this.isValid = true;
    this.chatConfig['data']['metrics'].forEach((element, i) => {
      this.isMetricValid[i] = false;
      if (element.isVisible && element.field == '') {
        this.isValid = false;
        this.isMetricValid[i] = true;
      }
    });
    if (this.chartType != 'cards_panel') {
      let bucketData = [];
      bucketData.push(this.chatConfig['data']['buckets'][0]);
      if (this.chartType == 'data_table') {
        bucketData = this.chatConfig['data']['buckets'];
      }

      if (this.chartType != 'pie' && this.chartType != 'polarArea' && this.chartType != 'radar' && this.chartType != 'data_table' && this.chatConfig['data']['buckets'][0]['slice_data']['is_show']) {
        bucketData.push(this.chatConfig['data']['buckets'][0]['slice_data']);
      }
      bucketData.forEach((element, i) => {
        this.isBucketAggregationValid[i] = false;
        this.isBucketFieldValid[i] = false;
        this.isBucketFieldOrderValid[i] = false;
        if (element.aggregation == '') {
          this.isValid = false;
          this.isBucketAggregationValid[i] = true;
        }
        if (element.field == '') {
          this.isValid = false;
          this.isBucketFieldValid[i] = true;
        }

        if (this.chartType != 'data_table' && element.order_by.key == 'Custom Metrics' && element.order_by.field == '') {
          this.isValid = false;
          this.isBucketFieldOrderValid[i] = true;
        }
      });
    }
  }

  customDateRange() {
    const ref = this.dialogService.open(CustomDateRangeComponent, {
      data: this.customize_daterange,
      header: 'Customize Date Range',
      contentStyle: { width: '40vw', overflow: 'auto' },
    });

    ref.onClose.subscribe((data: object) => {
      if (data) {
        this.customize_daterange = data;
        this.filtersApplied['timeKeyFilter'] = {
          time_key1: this.customize_daterange['time_key1'],
          time_key2: this.customize_daterange['time_key2']
        }
        this.runQuery();
      }
    });
  }
  initialLoading() {
    this.keySpaces = {};

    if (this.VisaulisationType == 'update' || this.VisaulisationType == 'updateInDashboard' || this.VisaulisationType == 'view') {
      this.isUpdate = true;
      let visualizationId = parseInt(localStorage.getItem('keyspaceId'));
      this.dataFetchServ.getVisualizeById(visualizationId).subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }

        this.lastUpdatedOn = moment(data['data']['update_at'], 'x').toDate();
        this.chatConfig = JSON.parse(data['data']['chart_config']);
        this.customize_daterange = this.changesDate(this.chatConfig['defualt_daterange'])
        this.chartType = this.chartTypeData.find(x => x.display_name == data['data']['chart_type']).chart_type;
        this.selectType({ chart_type: this.chartType });
        this.filtersApplied['timeKeyFilter'] = {
          time_key1: this.customize_daterange['time_key1'],
          time_key2: this.customize_daterange['time_key2']
        }
        this.title = data['data']['title'];
        this.description = data['data']['description'];
        this.visualizeId = data['data']['id'];
        this.keySpaces = this.keySpacesData.find(x => x.id == data['data']['keyspace_id']);
        this.keySpacesChange();
        // this.chatConfig['data']['metrics'][0]['chart_type'] = this.chartType;
        this.runQuery();
      });
    } else {
      this.customize_daterange = this.changesDate('this_month');
      this.filtersApplied['timeKeyFilter'] = {
        time_key1: this.customize_daterange['time_key1'],
        time_key2: this.customize_daterange['time_key2']
      }
    }

    this.groupedAggregationBucket = [
      { label: 'Date Histogram', value: 'Date Histogram' },
      { label: 'Terms', value: 'Terms' }
    ];

    this.groupedOrderBy = [
      { label: 'Custom Metrics', value: 'Custom Metrics' },
      { label: 'Alphabetical', value: 'Alphabetical' }
    ];

    this.groupedOrder = [
      { label: 'Descending', value: 'desc' },
      { label: 'Ascending', value: 'asc' }
    ];

    this.groupedLineMode = [

      { label: 'Smoothed', value: 'Smoothed' },
      { label: 'Straight', value: 'Straight' },
      { label: 'Stepped', value: 'Stepped' }
    ];
    this.groupedMode = [
      { label: 'Normal', value: 'Normal' },
      { label: 'Stacked', value: 'Stacked' }
    ];
    this.groupedChartType = [
      { label: 'Line', value: 'line' },
      { label: 'Area', value: 'area' },
      { label: 'Bar', value: 'bar' }
    ];

    this.groupedXaxisPosition = [
      { label: 'Top', value: 'top' },
      { label: 'Bottom', value: 'bottom' }
    ];

    this.groupedYaxisPosition = [
      { label: 'Left', value: 'left' },
      { label: 'Right', value: 'right' }
    ];

    this.groupedYAxisLine = [
      { label: 'LeftAxis-1', value: 'LeftAxis-1' },
      { label: 'RightAxis-1', value: 'RightAxis-1' },
      { label: 'Don\'t Show', value: 'Dont Show' }
    ];

    this.groupedLegendPosition = [
      { label: 'Top', value: 'top' },
      { label: 'Bottom', value: 'bottom' }
    ];
    // this.groupedAxisMode = [
    //   { label: 'Normal', value: 'Normal' },
    //   { label: 'Percentage', value: 'Percentage' },
    //   { label: 'Wiggle', value: 'Wiggle' },
    //   { label: 'Silhouette', value: 'Silhouette' }
    // ];
    // this.groupedScaleType = [
    //   { label: 'Linear', value: 'Linear' },
    //   { label: 'Log', value: 'Log' },
    //   { label: 'Square root', value: 'Square root' }
    // ];
    this.groupedAlign = [
      { label: 'Horizontal', value: { minRotation: 0, maxRotation: 0 } },
      { label: 'Vertical', value: { minRotation: 90, maxRotation: 90 } },
      { label: 'Angled', value: { minRotation: 0, maxRotation: 90 } }
    ];
    this.groupedLineStyle = [
      { label: 'Solid', value: 'solid' },
      { label: 'Dotted', value: 'dotted' },
      // { label: 'Dot-dashed', value: 'Dot-dashed' }
    ];

  }

  aggregationChange(i) {
    let field: SelectItemGroup[] = [];
    this.groupedField[i] = []
    let aggregation = this.chatConfig['data']['metrics'][i]['aggregation'];
    this.chatConfig['data']['metrics'][i]['field'] = '';
    if (aggregation == 'mean' || aggregation == 'Percentile Ranks' ||
      aggregation == 'std' || aggregation == 'sum' || aggregation == 'Top Hit' || aggregation == 'var' || aggregation == 'size') {
      field.push({ label: 'Metrics', value: 'Metrics', items: this.listMatrics });
      if (this.listDerivedMatrics.length > 0) {
        field.push({ label: 'Derived Metrics', value: 'Derived Metrics', items: this.listDerivedMatrics });
      }

      this.groupedField[i] = field;
    } else if (aggregation == 'max' || aggregation == 'Median' || aggregation == 'min' || aggregation == 'Percentiles') {
      field.push({ label: 'Metrics', value: 'Metrics', items: this.listMatrics });
      if (this.listDerivedMatrics.length > 0) {
        field.push({ label: 'Derived Metrics', value: 'Derived Metrics', items: this.listDerivedMatrics });
      }
      // field.push({ label: 'Date', value: 'Date', items: this.listDate });
      this.groupedField[i] = field;
    } else if (aggregation == 'count') {
      this.chatConfig['data']['metrics'][i]['field'] = this.listMatrics[0].value;
    } else {
      field.push({ label: 'Metrics', value: 'Metrics', items: this.listMatrics });
      if (this.listDerivedMatrics.length > 0) {
        field.push({ label: 'Derived Metrics', value: 'Derived Metrics', items: this.listDerivedMatrics });
      }
      // field.push({ label: 'Dimension', value: 'Dimension', items: this.listDimension });
      // field.push({ label: 'Date', value: 'Date', items: this.listDate });
      this.groupedField[i] = field;
    }

  }

  aggregationBucketChange(i) {
    let field: SelectItemGroup[] = [];
    this.groupedFieldBucket[i] = []
    let aggregation = '';
    if (i == 0) {
      aggregation = this.chatConfig['data']['buckets'][i]['aggregation'];
    } else {
      aggregation = this.chatConfig['data']['buckets'][i - 1]['slice_data']['aggregation'];
    }
    if (aggregation == 'Date Histogram') {

      // field.push({ label: 'Date Histogram', value: 'Date Histogram', items: this.listDate });
      this.groupedFieldBucket[i] = this.listDate;
      if (i == 0) {
        this.chatConfig['data']['buckets'][i]['order_by']['size'] = '';
      } else {
        this.chatConfig['data']['buckets'][i - 1]['slice_data']['order_by']['size'] = '';
      }

    } else {
      // field.push({ label: 'Matrics', value: 'Matrics', items: this.listMatrics });
      // field.push({ label: 'Derived Matrics', value: 'Derived Matrics', items: this.listDerivedMatrics });
      // field.push({ label: 'Dimension', value: 'Dimension', items: this.listDimension });
      // field.push({ label: 'Date', value: 'Date', items: this.listDate });
      this.groupedFieldBucket[i] = this.listDimension;
      if (i == 0) {
        this.chatConfig['data']['buckets'][i]['order_by']['size'] = '5';
      } else {
        this.chatConfig['data']['buckets'][i - 1]['slice_data']['order_by']['size'] = '5';
      }
    }

    if (i == 0) {
      this.chatConfig['data']['buckets'][i]['field'] = '';
    } else {
      this.chatConfig['data']['buckets'][i - 1]['slice_data']['field'] = '';
    }
  }

  aggregationDimensionChange(i) {
    let field: SelectItemGroup[] = [];
    this.groupedFieldBucket[i] = []
    let aggregation = '';
    aggregation = this.chatConfig['data']['buckets'][i]['aggregation'];
    if (aggregation == 'Date Histogram') {
      // field.push({ label: 'Date Histogram', value: 'Date Histogram', items: this.listDate });
      this.groupedFieldBucket[i] = this.listDate;
    } else {
      // field.push({ label: 'Dimension', value: 'Dimension', items: this.listDimension });
      this.groupedFieldBucket[i] = this.listDimension;
    }
  }


  fieldChange(i) {
    this.orderClick();
  }
  orderClick() {
    this.groupedOrderBy = [];
    this.chatConfig['data']['metrics'].forEach((element, i) => {
      if (element.isVisible) {
        if (element.custom_label == '') {
          this.groupedOrderBy.push({ label: 'metrics:' + this.getTitle(element), value: i });
        } else {
          this.groupedOrderBy.push({ label: 'metrics:' + element.custom_label, value: i });
        }
      }
    });
    this.groupedOrderBy.push({ label: 'Custom Metrics', value: 'Custom Metrics' },
      { label: 'Alphabetical', value: 'Alphabetical' })
  }

  tableOrderClick() {
    this.groupedTableOrderBy = [];
    this.chatConfig['data']['buckets'].forEach((element, i) => {
      if (element.isVisible) {
        this.groupedTableOrderBy.push({ label: this.getTitle(element), value: { type: 'D', index: i } });
      }
    });
    this.chatConfig['data']['metrics'].forEach((element, i) => {
      if (element.isVisible) {
        this.groupedTableOrderBy.push({ label: this.getTitle(element), value: { type: 'M', index: i } });
      }
    });
  }

  numberPositionClick(index) {
    this.groupedNumberPosition = [];
    this.chatConfig['data']['metrics'].forEach((element, i) => {
      if (i != index) {
        this.groupedNumberPosition.push({ label: this.firstLetterCap(numberToWords.toWordsOrdinal(i + 1)), value: i });
      }
    });
  }

  firstLetterCap(value) {
    if (typeof value !== 'string') {
      return '';
    } else {
      return value.charAt(0).toUpperCase() + value.slice(1);
    }
  }

  numberPositionBucketClick(index) {
    this.groupedNumberPosition = [];
    this.chatConfig['data']['buckets'].forEach((element, i) => {
      if (i != index) {
        this.groupedNumberPosition.push({ label: this.firstLetterCap(numberToWords.toWordsOrdinal(i + 1)), value: i });
      }
    });
  }
  changeCardPosition(index) {
    console.log('selected card', this.selectedCardPosition)
    let card = this.chatConfig['data']['metrics'][index];
    this.chatConfig['data']['metrics'].splice(index, 1);
    this.chatConfig['data']['metrics'].splice(this.selectedCardPosition, 0, card);
    this.selectedCardPosition = "";
  }

  changeBucketPosition(index) {
    console.log('selected card', this.selectedCardPosition)
    let card = this.chatConfig['data']['buckets'][index];
    let fields = this.groupedFieldBucket[index];
    this.groupedFieldBucket[index] = this.groupedFieldBucket[this.selectedCardPosition];
    this.groupedFieldBucket[this.selectedCardPosition] = fields;
    this.chatConfig['data']['buckets'].splice(index, 1);
    this.chatConfig['data']['buckets'].splice(this.selectedCardPosition, 0, card);
    this.selectedCardPosition = "";
  }

  addCards() {
    this.chatConfig['data']['metrics'].push({
      'aggregation': 'sum',
      'field': '',
      'custom_label': '',
      'icon_class': 'fas fa-info-circle',
      'card_color': this.getRandomColor(),
      'isVisible': true,
      'isOpen': true,
    });
    this.aggregationChange(this.chatConfig['data']['metrics'].length - 1);
  }
  addMetrics() {
    this.chatConfig['data']['metrics'].push({
      'aggregation': 'sum',
      'field': '',
      'custom_label': '',
      'isVisible': true,
      'isOpen': true,
    });
    this.aggregationChange(this.chatConfig['data']['metrics'].length - 1);
  }
  addTableMetrics() {
    this.chatConfig['data']['metrics'].push({
      'aggregation': 'sum',
      'field': '',
      'custom_label': '',
      'width': '150',
      'options': {
        'editable': false,
        'colSearch': false,
        'colSort': true,
        'resizable': true,
        'movable': false
      },
      'isOpen': true,
      'isVisible': true
    });
    this.aggregationChange(this.chatConfig['data']['metrics'].length - 1);
  }

  addTableDimension() {
    this.chatConfig['data']['buckets'].push({
      'aggregation': '',
      'field': '',
      'custom_label': '',
      'width': '150',
      'options': {
        'editable': false,
        'colSearch': false,
        'colSort': true,
        'resizable': true,
        'movable': false,
        'isfrozen': false
      },
      'isOpen': true,
      'isVisible': true
    });
    this.aggregationDimensionChange(this.chatConfig['data']['buckets'].length - 1);
  }


  getfielddata(field, returntype) {
    if (field && this.keySpaces['field_config']) {
      return this.keySpaces['field_config'].find(x => x.field == field)[returntype];
    } else {
      return '';
    }
  }

  checkMetricAggrigation(field, aggregation) {
    return (this.getfielddata(field, 'columnType') == 'derived_metrics' && aggregation == 'sum');
  }
  runChartQuery() {
    let colors = this.libServ.dynamicColors(10);
    console.log('chart', this.chatConfig);
    this.defualtChartJson = {
      chartTypes: [],
      chartData: {
        labels: [],
        datasets: []
      },
      chartOptions: {
        title: {
          display: true,
          text: this.title
        },
        legend: {
          display: true,
          position: 'top'
        },
        elements: {
          point: {
            display: false
          }
        },
        scales: {
          xAxes: [],
          yAxes: []
        },
        tooltips: {
          mode: this.chatConfig['data']['buckets'][0]['slice_data']['is_show'] ? 'nearest' : 'index',
          intersect: false,
          enabled: this.chatConfig['panel_setting']['settings']['show_tooltips'],
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, data.datasets[tooltipItem.datasetIndex].format, [])}`;
            }
          }
        },
        plugins: {
          datalabels: false,
          labels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '450px',
      calculate_value: this.chatConfig['panel_setting']['settings']['calculate_value'],
    }
    this.chartJson = this.libServ.deepCopy(this.defualtChartJson);
    this.chartJson['chartTypes'].push({ key: 'bar', label: 'Line Chart', stacked: false });
    this.chatConfig['data']['metrics'].forEach((element, i) => {
      if (element.isVisible) {
        if (element.chart_type == 'area') {
          this.chartJson['chartData']['datasets'].push({
            label: element.custom_label == '' ? this.getTitle(element) : element.custom_label,
            type: 'line',
            format: this.getfielddata(element.field, 'format'),
            yAxisID: element.axis_value,
            borderColor: element.legend_color,
            fill: 'origin',
            backgroundColor: element.legend_color,
            data: [],
            steppedLine: element.show_line.line_mode == 'Stepped' ? true : false,
            lineTension: element.show_line.line_mode == 'Straight' ? 0 : undefined
          });
        } else {
          this.chartJson['chartData']['datasets'].push({
            label: element.custom_label == '' ? this.getTitle(element) : element.custom_label,
            type: element.chart_type,
            yAxisID: element.axis_value,
            borderColor: element.legend_color,
            format: this.getfielddata(element.field, 'format'),
            fill: false,
            backgroundColor: element.legend_color,
            data: [],
            // borderDash: [2, 2],
            steppedLine: element.show_line.line_mode == 'Stepped' ? true : false,
            lineTension: element.show_line.line_mode == 'Straight' ? 0 : undefined,
            showLine: element.show_line.is_show,
            pointRadius: element.show_dot ? 2 : 0,
            borderWidth: element.show_line.line_width,
            trendlineLinear: (this.chatConfig['panel_setting']['threshold_line']['is_show']) ? {
              style: this.chatConfig['panel_setting']['threshold_line']['line_color'],
              lineStyle: this.chatConfig['panel_setting']['threshold_line']['line_style'],
              width: this.chatConfig['panel_setting']['threshold_line']['line_width']
            } : undefined,
            order: element.chart_type == 'line' ? 1 : 2
          });
        }
      }
    });

    const xAxes = this.chatConfig['data']['buckets'][0];
    this.chartJson['chartOptions']['scales']['xAxes'].push({
      type: 'category',
      display: true,
      scaleLabel: {
        display: xAxes.show_axis_lines_label.is_show,
        labelString: xAxes.custom_label == '' ? this.getTitle(xAxes) : xAxes.custom_label,
      },
      gridLines: {
        display: this.chatConfig['panel_setting']['grid']['x_axis_lines']
      },
      position: xAxes.position,
      stacked: true,
      ticks: {
        // autoRotate: true,
        minRotation: xAxes.show_axis_lines_label.show_label.align.minRotation,
        maxRotation: xAxes.show_axis_lines_label.show_label.align.maxRotation,
        display: xAxes.show_axis_lines_label.show_label.is_show,
        callback: (value, index, values) => {
          // return (value / this.max * 100).toFixed(0) + '%';
          return value.toString().substr(0, xAxes.show_axis_lines_label.show_label.truncate);
        }
      }
    });
    this.chatConfig['data']['metrics'].filter(x => x.isVisible).forEach((element, i) => {

      this.chartJson['chartOptions']['scales']['yAxes'].push({
        gridLines: {
          display: this.chatConfig['panel_setting']['grid']['y_axis_lines'] == element.axis_value
        },
        id: element.axis_value,
        display: true,
        scaleLabel: {
          display: element.show_axis_lines_label.is_show,
          labelString: element.custom_label == '' ? this.getTitle(element) : element.custom_label,
        },
        position: element.position,
        name: '1',
        stacked: this.chatConfig['data']['buckets'][0]['slice_data']['is_show'] && this.chatConfig['data']['buckets'][0]['slice_data']['isStacked'],
        format: this.getfielddata(element.field, 'format'),
        ticks: {
          minRotation: element.show_axis_lines_label.show_label.align.minRotation,
          maxRotation: element.show_axis_lines_label.show_label.align.maxRotation,
          min: element.custom_extend.axis_extend.min == '' ? undefined : element.custom_extend.axis_extend.min,
          max: element.custom_extend.axis_extend.max == '' ? undefined : element.custom_extend.axis_extend.max,
          stepSize: (element.custom_extend.scale_to_bound.is_show && element.custom_extend.scale_to_bound.range != '') ? element.custom_extend.scale_to_bound.range : undefined,
          display: element.show_axis_lines_label.show_label.is_show,
          callback: (value, index, values) => {
            // return (value / this.max * 100).toFixed(0) + '%';

            return this.formatNumPipe.transform(value, this.chartJson['chartOptions']['scales']['yAxes'][i].format, []).substr(0, element.show_axis_lines_label.show_label.truncate + 1);
          }
        }
        // scaleFontColor: "rgba(151,137,200,0.8)"
      });
    });
    this.chartJson['chartOptions']['legend']['position'] = this.chatConfig['panel_setting']['settings']['legend_position'];
    console.log(this.chartJson['chartOptions']['legend']['position'], this.chatConfig['panel_setting']['settings']['legend_position'])
    let dimensions = [];
    let metrics = [];
    let timekey = [];
    let groupbBy = [];
    let orderBy = [];
    let derived_metrics = [];
    let dashboardMetrics = {};
    let bucketData = [];
    let graphOrderBy = [];
    bucketData.push(this.chatConfig['data']['buckets'][0]);
    if (this.chatConfig['data']['buckets'][0]['slice_data']['is_show']) {
      bucketData.push(this.chatConfig['data']['buckets'][0]['slice_data']);
    }
    console.log('data....', bucketData, this.chatConfig['data']['buckets'][0]['slice_data']['is_show'])
    bucketData.forEach(bucket => {
      console.log('order', bucket.order_by.key);

      dimensions.push(bucket.field);
      if (bucket.field != 'time_key' && bucket.field != 'accounting_key') {
        groupbBy.push(bucket.field);
        if (bucket.order_by.key == 'Custom Metrics') {
          orderBy.push({ key: bucket.order_by.field, opcode: bucket.order_by.mode });
          if (dashboardMetrics[bucket.order_by.field] == undefined) {
            dashboardMetrics[bucket.order_by.field] = [];
          }
          dashboardMetrics[bucket.order_by.field].push('sum');

          if (this.getfielddata(bucket.order_by.field, 'columnType') == 'derived_metrics') {
            derived_metrics.push(bucket.order_by.field);
            let calculate_columns = [];
            calculate_columns = this.getfielddata(bucket.order_by.field, 'calculate_columns');
            metrics = metrics.concat(calculate_columns);
            console.log(derived_metrics, this.getfielddata(bucket.order_by.field, 'calculate_columns'));
          } else {
            if (!metrics.some(x => x == bucket.order_by.field)) {
              metrics.push(bucket.order_by.field);
            }
          }
        } else if (bucket.order_by.key == 'Alphabetical') {
          orderBy.push({ key: bucket.field, opcode: bucket.order_by.mode });
        } else {

          if (this.checkMetricAggrigation(this.chatConfig['data']['metrics'][bucket.order_by.key]['field'], this.chatConfig['data']['metrics'][bucket.order_by.key]['aggregation'])) {
            orderBy.push({
              key: this.chatConfig['data']['metrics'][bucket.order_by.key]['field'], opcode: bucket.order_by.mode
            });
          } else {
            orderBy.push({
              key: this.chatConfig['data']['metrics'][bucket.order_by.key]['field'] + '_'
                + this.chatConfig['data']['metrics'][bucket.order_by.key]['aggregation'], opcode: bucket.order_by.mode
            });
          }

        }
      } else {
        timekey.push(bucket.field);

        orderBy.push({ key: bucket.field, opcode: 'asc' })

      }
    });
    this.chatConfig['data']['metrics'].forEach(metric => {
      if (metric.isVisible) {
        if (dashboardMetrics[metric.field] == undefined) {
          dashboardMetrics[metric.field] = [];
        }
        dashboardMetrics[metric.field].push(metric.aggregation.toLowerCase());
        if (this.getfielddata(metric.field, 'columnType') == 'derived_metrics') {
          derived_metrics.push(metric.field);
          let calculate_columns = [];
          calculate_columns = this.getfielddata(metric.field, 'calculate_columns');
          metrics = metrics.concat(calculate_columns);
          console.log(derived_metrics, this.getfielddata(metric.field, 'calculate_columns'));
        } else {
          if (!metrics.some(x => x == metric.field)) {
            metrics.push(metric.field);
          }
        }
      }
    });
    if (this.chatConfig['data']['buckets'][0]['slice_data']['is_show']) {
      graphOrderBy.push({
        colmun_Name: this.chatConfig['data']['buckets'][0]['field'],
        order: orderBy[0]['opcode'],
        limit: this.chatConfig['data']['buckets'][0]['order_by']['size'] == null ? '' : this.chatConfig['data']['buckets'][0]['order_by']['size'],
        sort_column: orderBy[0]['key']
      }, {
        colmun_Name: this.chatConfig['data']['buckets'][0]['slice_data']['field'],
        order: orderBy[1]['opcode'],
        limit: this.chatConfig['data']['buckets'][0]['slice_data']['order_by']['size'] == null ? '' : this.chatConfig['data']['buckets'][0]['slice_data']['order_by']['size'],
        sort_column: orderBy[1]['key']
      })
      orderBy = [];
    }

    this.apiRequest = {
      dimensions: Array.from(new Set(dimensions.concat(['time_key']))),
      metrics: Array.from(new Set(metrics)),
      derived_metrics: Array.from(new Set(derived_metrics)),
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: { dimensions: [], metrics: [] },
      groupByTimeKey: {
        key: Array.from(new Set(timekey.concat(['time_key']))),
        interval: 'daily'
      },
      dashboardDetails: {

        isDashboard: 'True',
        dimensions: Array.from(new Set(dimensions)),
        // graphOrderBy: graphOrderBy.length > 0 ? graphOrderBy : null,
        metrics: [dashboardMetrics]
      },
      gidGroupBy: groupbBy.length ? Array.from(new Set(groupbBy)) : ['source'],
      orderBy: orderBy,
      limit: this.chatConfig['data']['buckets'][0]['slice_data']['is_show'] ? '' : this.chatConfig['data']['buckets'][0]['order_by']['size'],
      offset: '0'
    };
    if (graphOrderBy.length > 0) {
      this.apiRequest['dashboardDetails']['graphOrderBy'] = graphOrderBy;
    }
    this.showChart = false;
    this.noDataChart = false;
    this.dataFetchServ.getChartData(this.apiRequest, this.keySpaces['api_url']).subscribe(data => {
      const chartData = data['data'];
      this.noDataChart = false;
      if (!chartData.length) {
        this.noDataChart = true;
        return;
      }
      const labelArr = Array.from(new Set(chartData.map(r => r[this.chatConfig['data']['buckets'][0]['field']])));
      if (this.chatConfig['data']['buckets'][0]['field'] == 'time_key') {
        this.chartJson['chartData']['labels'] = labelArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );
      } else if (this.chatConfig['data']['buckets'][0]['field'] == 'accounting_key') {
        this.chartJson['chartData']['labels'] = labelArr.map(d =>
          moment(d, 'YYYYMMDD').format('MMM-YYYY')
        );
      } else {
        this.chartJson['chartData']['labels'] = labelArr;

      }
      if (this.chatConfig['data']['buckets'][0]['slice_data']['is_show']) {

        if (this.chatConfig['data']['buckets'][0]['slice_data']['isStacked']) {
          this.chartJson['chartTypes'][0]['key'] = 'bar';
          this.chartJson['chartTypes'][0]['stacked'] = true;
        }

        if (this.chatConfig['data']['metrics'].some(x => x.isVisible && !x.isSlice)) {
          let sliceindex = this.chatConfig['data']['metrics'].findIndex(x => x.isVisible && x.isSlice);
          this.chartJson['chartData']['datasets'].splice(sliceindex, 1);
          // this.chartJson['chartData']['datasets'][0]['borderColor'] = colors[0];
          // this.chartJson['chartData']['datasets'][0]['backgroundColor'] = colors[0];


          let dimensions = [];
          let metrics = [];
          let timekey = [];
          let groupbBy = [];
          let orderBy = [];
          let derived_metrics = [];
          let dashboardMetrics = {};
          let bucketData = [];
          bucketData.push(this.chatConfig['data']['buckets'][0]);
          bucketData.forEach(bucket => {
            console.log('order', bucket.order_by.key);
            dimensions.push(bucket.field);
            if (bucket.field != 'time_key' && bucket.field != 'accounting_key') {
              groupbBy.push(bucket.field);
              if (bucket.order_by.key == 'Custom Metrics') {
                orderBy.push({ key: bucket.order_by.field, opcode: bucket.order_by.mode });
                if (dashboardMetrics[bucket.order_by.field] == undefined) {
                  dashboardMetrics[bucket.order_by.field] = [];
                }
                dashboardMetrics[bucket.order_by.field].push('sum');
                if (this.getfielddata(bucket.order_by.field, 'columnType') == 'derived_metrics') {
                  derived_metrics.push(bucket.order_by.field);
                  let calculate_columns = [];
                  calculate_columns = this.getfielddata(bucket.order_by.field, 'calculate_columns');
                  metrics = metrics.concat(calculate_columns);
                  console.log(derived_metrics, this.getfielddata(bucket.order_by.field, 'calculate_columns'));
                } else {
                  if (!metrics.some(x => x == bucket.order_by.field)) {
                    metrics.push(bucket.order_by.field);
                  }
                }
              } else if (bucket.order_by.key == 'Alphabetical') {
                orderBy.push({ key: bucket.field, opcode: bucket.order_by.mode });
              } else {
                if (this.checkMetricAggrigation(this.chatConfig['data']['metrics'][bucket.order_by.key]['field'], this.chatConfig['data']['metrics'][bucket.order_by.key]['aggregation'])) {
                  orderBy.push({
                    key: this.chatConfig['data']['metrics'][bucket.order_by.key]['field'], opcode: bucket.order_by.mode
                  });
                } else {
                  orderBy.push({
                    key: this.chatConfig['data']['metrics'][bucket.order_by.key]['field'] + '_'
                      + this.chatConfig['data']['metrics'][bucket.order_by.key]['aggregation'], opcode: bucket.order_by.mode
                  });
                }
              }
            } else {
              timekey.push(bucket.field);
              orderBy.push({ key: bucket.field, opcode: 'asc' })
            }
          });
          this.chatConfig['data']['metrics'].forEach(metric => {
            if (metric.isVisible) {
              if (dashboardMetrics[metric.field] == undefined) {
                dashboardMetrics[metric.field] = [];
              }
              dashboardMetrics[metric.field].push(metric.aggregation.toLowerCase());
              if (this.getfielddata(metric.field, 'columnType') == 'derived_metrics') {
                derived_metrics.push(metric.field);
                let calculate_columns = [];
                calculate_columns = this.getfielddata(metric.field, 'calculate_columns');
                metrics = metrics.concat(calculate_columns);
                console.log(derived_metrics, this.getfielddata(metric.field, 'calculate_columns'));
              } else {
                if (!metrics.some(x => x == metric.field)) {
                  metrics.push(metric.field);
                }
              }
            }
          });

          let apiRequest = {
            dimensions: Array.from(new Set(dimensions.concat(['time_key']))),
            metrics: Array.from(new Set(metrics)),
            derived_metrics: Array.from(new Set(derived_metrics)),
            timeKeyFilter: this.filtersApplied['timeKeyFilter'],
            filters: { dimensions: [], metrics: [] },
            groupByTimeKey: {
              key: Array.from(new Set(timekey.concat(['time_key']))),
              interval: 'daily'
            },
            dashboardDetails: {
              isDashboard: 'True',
              dimensions: Array.from(new Set(dimensions)),
              // graphOrderBy: graphOrderBy.length > 0 ? graphOrderBy : null,
              metrics: [dashboardMetrics]
            },
            gidGroupBy: groupbBy.length ? Array.from(new Set(groupbBy)) : ['source'],
            orderBy: orderBy,
            limit: this.chatConfig['data']['buckets'][0]['order_by']['size'],
            offset: '0'
          };

          this.dataFetchServ.getChartData(apiRequest, this.keySpaces['api_url']).subscribe(data1 => {
            const chartData1 = data1['data'];
            let dataset = [];
            labelArr.forEach(label => {
              chartData1.forEach(r => {
                if (r[this.chatConfig['data']['buckets'][0]['field']] === label) {
                  this.chatConfig['data']['metrics'].forEach(element => {
                    if (element.isVisible && !element.isSlice) {
                      if (this.checkMetricAggrigation(element.field, element.aggregation)) {
                        dataset.push(r[element.field]);
                      } else {
                        dataset.push(r[element.field + '_' + element.aggregation]);
                      }
                    }
                  });
                }
              });
            });
            this.chartJson['chartData']['datasets'][0]['data'] = dataset;
            this.showChart = false;
            setTimeout(() => {
              this.showChart = true;
            }, 0);

          });
        } else {
          this.chartJson['chartData']['datasets'] = [];
        }
        const splitLabelArr = Array.from(new Set(chartData.map(r => r[this.chatConfig['data']['buckets'][0]['slice_data']['field']])));
        colors = this.libServ.dynamicColors(splitLabelArr.length + 2);
        splitLabelArr.forEach((splitLabel, i) => {
          let labelName = splitLabel;
          if (this.chatConfig['data']['buckets'][0]['slice_data']['field'] == 'time_key') {

            labelName = moment(splitLabel, 'YYYYMMDD').format('MM-DD-YYYY');
          } else if (this.chatConfig['data']['buckets'][0]['slice_data']['field'] == 'accounting_key') {
            labelName = moment(splitLabel, 'YYYYMMDD').format('MM-YYYY');
          } else {
            labelName = splitLabel;

          }
          const dataSets = {};
          this.chatConfig['data']['metrics'].forEach(element => {
            if (element.isVisible && element.isSlice) {
              if (this.checkMetricAggrigation(element.field, element.aggregation)) {
                dataSets[element.field] = [];
              } else {
                dataSets[element.field + '_' + element.aggregation] = [];
              }
            }
          });
          labelArr.forEach(label => {
            let isAvailable = true;
            chartData.forEach(r => {
              if (r[this.chatConfig['data']['buckets'][0]['field']] === label &&
                r[this.chatConfig['data']['buckets'][0]['slice_data']['field']] === splitLabel) {
                isAvailable = false;
                this.chatConfig['data']['metrics'].forEach(element => {
                  if (element.isVisible && element.isSlice) {
                    if (this.checkMetricAggrigation(element.field, element.aggregation)) {
                      dataSets[element.field].push(r[element.field]);
                    } else {
                      dataSets[element.field + '_' + element.aggregation].push(r[element.field + '_' + element.aggregation]);
                    }
                  }
                });
              }
            });
            if (isAvailable) {

              this.chatConfig['data']['metrics'].forEach(element => {
                if (element.isVisible && element.isSlice) {
                  if (this.checkMetricAggrigation(element.field, element.aggregation)) {
                    dataSets[element.field].push(null);
                  } else {
                    dataSets[element.field + '_' + element.aggregation].push(null);
                  }
                }
              });

            }
          });
          this.chatConfig['data']['metrics'].forEach((element) => {
            if (element.isVisible && element.isSlice) {
              if (element.chart_type == 'area') {
                this.chartJson['chartData']['datasets'].push({
                  label: labelName,
                  type: 'line',
                  format: this.getfielddata(element.field, 'format'),
                  yAxisID: element.axis_value,
                  borderColor: colors[i + 2],
                  fill: 'origin',
                  backgroundColor: colors[i + 2],
                  data: (this.checkMetricAggrigation(element.field, element.aggregation)) ?
                    dataSets[element.field]
                    : dataSets[element.field + '_' + element.aggregation],
                  steppedLine: element.show_line.line_mode == 'Stepped' ? true : false,
                  lineTension: element.show_line.line_mode == 'Straight' ? 0 : undefined

                });
              } else {
                this.chartJson['chartData']['datasets'].push({
                  label: labelName,
                  type: element.chart_type,
                  yAxisID: element.axis_value,
                  borderColor: colors[i + 2],
                  format: this.getfielddata(element.field, 'format'),
                  fill: false,
                  backgroundColor: colors[i + 2],
                  data: (this.checkMetricAggrigation(element.field, element.aggregation)) ?
                    dataSets[element.field]
                    : dataSets[element.field + '_' + element.aggregation],
                  steppedLine: element.show_line.line_mode == 'Stepped' ? true : false,
                  lineTension: element.show_line.line_mode == 'Straight' ? 0 : undefined,
                  showLine: element.show_line.is_show,
                  pointRadius: element.show_dot ? 2 : 0,
                  borderWidth: element.show_line.line_width,
                  order: element.chart_type == 'line' ? 1 : 2
                });
              }
            }
          });
        })
      } else {
        const dataSets = {};
        this.chatConfig['data']['metrics'].forEach(element => {
          if (element.isVisible) {
            if (this.checkMetricAggrigation(element.field, element.aggregation)) {
              dataSets[element.field] = [];
            } else {
              dataSets[element.field + '_' + element.aggregation] = [];
            }
          }
        });
        labelArr.forEach(label => {
          chartData.forEach(r => {
            if (r[this.chatConfig['data']['buckets'][0]['field']] === label) {
              this.chatConfig['data']['metrics'].forEach(element => {
                if (element.isVisible) {
                  if (this.checkMetricAggrigation(element.field, element.aggregation)) {
                    dataSets[element.field].push(r[element.field]);
                  } else {
                    dataSets[element.field + '_' + element.aggregation].push(r[element.field + '_' + element.aggregation]);
                  }
                }
              });

            }
          });
        });
        this.chatConfig['data']['metrics'].filter(x => x.isVisible).forEach((element, i) => {
          if (element.isVisible) {
            if (this.checkMetricAggrigation(element.field, element.aggregation)) {
              this.chartJson['chartData']['datasets'][i]['data'] = dataSets[element.field];
            } else {
              this.chartJson['chartData']['datasets'][i]['data'] = dataSets[element.field + '_' + element.aggregation];
            }
          }
        });
      }
      this.chartJsonMain = this.libServ.deepCopy(this.chartJson);
      this.isChange = true;
      console.log('chartjs', this.chartJson);
      this.showChart = true;
    });

  }

  runPieChartQuery() {
    let colors = this.libServ.dynamicColors(10);
    console.log('chart', this.chatConfig);
    let that = this;
    this.defualtChartJson = {
      chartTypes: [],
      chartData: {
        labels: [],
        datasets: []
      },
      chartOptions: {
        title: {
          display: true,
          text: this.title
        },
        legend: {
          display: true,
          position: 'top',

        },
        elements: {
          point: {
            display: false
          }
        },
        tooltips: {
          intersect: false,
          enabled: this.chatConfig['panel_setting']['pie_settings']['show_tooltips'],
          callbacks: {
            label: (tooltipItem, data) => {
              const dataset = data.datasets[tooltipItem.datasetIndex];
              if (dataset['_meta'][Object.keys(dataset._meta)[0]]['data'][tooltipItem.index]['hidden']) {
                return;
              }
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              let sum = 0;
              const dataArr = data.datasets[0].data;
              dataArr.forEach((data, index) => {
                if (!dataset['_meta'][Object.keys(dataset._meta)[0]]['data'][index]['hidden']) {
                  sum += data;
                }
              });
              const percentage = (currentValue * 100) / sum;
              return `${
                data.labels[tooltipItem.index]
                } : ${this.formatNumPipe.transform(
                  currentValue,
                  data.datasets[tooltipItem.datasetIndex].format,
                  []
                )} - ${percentage.toFixed(2)} %`;
            }
          }
        },
        plugins: {
          // labels: {
          //   render: function (args) {
          //     console.log('args', args)
          //     return '$' + args.value;
          //   },
          //   precision: 2
          // },
          labels: [
            {
              render: function (args) {
                return args.percentage > 10 && that.chatConfig['panel_setting']['label_settings']['show_labels'] ? args.label : '';
              },
              position: 'outside',
              fontStyle: 'bold',
              fontColor: '#000',

            },
            {
              render: function (args) {
                console.log(args);
                return args.percentage > 10 && that.chatConfig['panel_setting']['label_settings']['show_values'] ? args.percentage + '%' : '';
              },
              fontStyle: 'bold',
              fontColor: '#000',
              precision: 2
            }
          ],
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '450px'
    }

    this.chartJson = this.libServ.deepCopy(this.defualtChartJson);
    this.chartJson['chartTypes'].push({ key: this.chatConfig['panel_setting']['pie_settings']['donut'] ? 'doughnut' : this.chartType, label: 'Pie Chart', stacked: false });
    this.chartJson['chartOptions']['legend']['position'] = this.chatConfig['panel_setting']['pie_settings']['legend_position'];
    this.chatConfig['data']['buckets'].forEach((bucket, i) => {

      this.chartJson['chartData']['datasets'].push({
        backgroundColor: [],
        data: [],
        format: this.getfielddata(this.chatConfig['data']['metrics'][i].field, 'format'),
      });


      let dimensions = [];
      let metrics = [];
      let timekey = [];
      let groupbBy = [];
      let orderBy = [];
      let derived_metrics = [];
      let dashboardMetrics = {};
      console.log('order', bucket.order_by.key);
      if (bucket.isVisible) {
        dimensions.push(bucket.field);
        if (bucket.field != 'time_key' && bucket.field != 'accounting_key') {
          groupbBy.push(bucket.field);

          if (bucket.order_by.key == 'Custom Metrics') {
            orderBy.push({ key: bucket.order_by.field, opcode: bucket.order_by.mode });
            if (dashboardMetrics[bucket.order_by.field] == undefined) {
              dashboardMetrics[bucket.order_by.field] = [];
            }
            dashboardMetrics[bucket.order_by.field].push('sum');
            if (this.getfielddata(bucket.order_by.field, 'columnType') == 'derived_metrics') {
              derived_metrics.push(bucket.order_by.field);
              let calculate_columns = [];
              calculate_columns = this.getfielddata(bucket.order_by.field, 'calculate_columns');
              metrics = metrics.concat(calculate_columns);
              console.log(derived_metrics, this.getfielddata(bucket.order_by.field, 'calculate_columns'));
            } else {
              if (!metrics.some(x => x == bucket.order_by.field)) {
                metrics.push(bucket.order_by.field);
              }
            }
          } else if (bucket.order_by.key == 'Alphabetical') {
            orderBy.push({ key: bucket.field, opcode: bucket.order_by.mode });
          } else {
            if (this.checkMetricAggrigation(this.chatConfig['data']['metrics'][bucket.order_by.key]['field'], this.chatConfig['data']['metrics'][bucket.order_by.key]['aggregation'])) {
              orderBy.push({
                key: this.chatConfig['data']['metrics'][bucket.order_by.key]['field'], opcode: bucket.order_by.mode
              });
            } else {
              orderBy.push({
                key: this.chatConfig['data']['metrics'][bucket.order_by.key]['field'] + '_'
                  + this.chatConfig['data']['metrics'][bucket.order_by.key]['aggregation'], opcode: bucket.order_by.mode
              });
            }
          }
        } else {
          timekey.push(bucket.field);

          orderBy.push({ key: bucket.field, opcode: 'asc' })

        }
      }

      this.chatConfig['data']['metrics'].forEach(metric => {

        if (dashboardMetrics[metric.field] == undefined) {
          dashboardMetrics[metric.field] = [];
        }
        dashboardMetrics[metric.field].push(metric.aggregation.toLowerCase());
        if (this.getfielddata(metric.field, 'columnType') == 'derived_metrics') {
          derived_metrics.push(metric.field);
          let calculate_columns = [];
          calculate_columns = this.getfielddata(metric.field, 'calculate_columns');
          metrics = metrics.concat(calculate_columns);
          console.log(derived_metrics, this.getfielddata(metric.field, 'calculate_columns'));
        } else {
          if (!metrics.some(x => x == metric.field)) {
            metrics.push(metric.field);
          }

        }


      });
      this.apiRequest = {
        dimensions: Array.from(new Set(dimensions.concat(['time_key']))),
        metrics: Array.from(new Set(metrics)),
        derived_metrics: Array.from(new Set(derived_metrics)),
        timeKeyFilter: this.filtersApplied['timeKeyFilter'],
        filters: { dimensions: [], metrics: [] },
        groupByTimeKey: {
          key: Array.from(new Set(timekey.concat(['time_key']))),
          interval: 'daily'
        },
        dashboardDetails: {

          isDashboard: 'True',
          dimensions: Array.from(new Set(dimensions)),

          metrics: [dashboardMetrics]
        },
        gidGroupBy: groupbBy.length ? Array.from(new Set(groupbBy)) : ['source'],
        orderBy: orderBy,
        limit: bucket['order_by']['size'],
        offset: '0'
      };

      this.showChart = false;
      this.noDataChart = false;
      this.dataFetchServ.getChartData(this.apiRequest, this.keySpaces['api_url']).subscribe(data => {
        const chartData = data['data'];
        this.noDataChart = false;
        if (!chartData.length) {
          this.noDataChart = true;
          return;
        }
        let labelArr = Array.from(new Set(chartData.map(r => r[bucket['field']])));
        if (bucket['field'] == 'time_key') {
          labelArr = labelArr.map(d =>
            moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
          );
        } else if (bucket['field'] == 'accounting_key') {
          labelArr = labelArr.map(d =>
            moment(d, 'YYYYMMDD').format('MMM-YYYY')
          );
        }
        this.chartJson['chartData']['labels'] = this.chartJson['chartData']['labels'].concat(labelArr);
        colors = this.libServ.dynamicColors(this.chartJson['chartData']['labels'].length);
        const dataSets = [];
        chartData.forEach(r => {
          if (this.checkMetricAggrigation(this.chatConfig['data']['metrics'][0].field, this.chatConfig['data']['metrics'][0].aggregation)) {
            dataSets.push(r[this.chatConfig['data']['metrics'][0].field]);
          } else {
            dataSets.push(r[this.chatConfig['data']['metrics'][0].field + '_' + this.chatConfig['data']['metrics'][0].aggregation]);
          }
        });
        this.chartJson['chartData']['datasets'][i]['data'] = dataSets;
        this.chartJson['chartData']['datasets'][i]['backgroundColor'] =
          colors.slice(this.chartJson['chartData']['labels'].length - labelArr.length);
        this.chartJsonMain = this.libServ.deepCopy(this.chartJson);
        this.isChange = true;
        console.log('chartjs', this.chartJson);
        this.showChart = true;
      });
    });
  }


  runRadarChartQuery() {
    let colors = this.libServ.dynamicColors(10);
    console.log('chart', this.chatConfig);
    let that = this;
    this.defualtChartJson = {
      chartTypes: [],
      chartData: {
        labels: [],
        datasets: []
      },
      chartOptions: {
        title: {
          display: true,
          text: this.title
        },
        legend: {
          display: true,
          position: 'top',

        },

        scale: {
          gridLines: {
            display: this.chatConfig['panel_setting']['label_settings']['show_grids']
          },
          pointLabels: {
            callback: (pointLabel, index, labels) => {
              return this.chatConfig['panel_setting']['label_settings']['show_labels'] ? pointLabel.toString().substr(0, this.chatConfig['panel_setting']['label_settings']['truncate']) : '';
            }
          },
          ticks: {
            min: this.chatConfig['panel_setting']['radar_settings'].axis_extend.min == '' ? undefined : this.chatConfig['panel_setting']['radar_settings'].axis_extend.min,
            max: this.chatConfig['panel_setting']['radar_settings'].axis_extend.max == '' ? undefined : this.chatConfig['panel_setting']['radar_settings'].axis_extend.max,
            display: this.chatConfig['panel_setting']['radar_settings']['tick_show'],
          }
        },
        elements: {
          point: {
            display: false
          }
        },
        tooltips: {
          intersect: false,
          enabled: this.chatConfig['panel_setting']['radar_settings']['show_tooltips'],
          callbacks: {
            label: (tooltipItem, data) => {
              // console.log('tool', tooltipItem, data)
              // const dataset = data.datasets[tooltipItem.datasetIndex];
              // if (dataset['_meta'][Object.keys(dataset._meta)[0]]['data'][tooltipItem.index]['hidden']) {
              //   return;
              // }
              // const currentValue =
              //   data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              // let sum = 0;
              // const dataArr = data.datasets[0].data;
              // dataArr.forEach((data, index) => {
              //   if (!dataset['_meta'][Object.keys(dataset._meta)[0]]['data'][index]['hidden']) {
              //     sum += data;
              //   }
              // });
              // const percentage = (currentValue * 100) / sum;
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(
                  tooltipItem.value,
                  data.datasets[tooltipItem.datasetIndex].format,
                  []
                )}`;
            },
            title: function (tooltipItems, data) {
              //Return value for title



              return data.labels[tooltipItems[0].index];
            },
          }
        },
        plugins: {
          // labels: {
          //   render: function (args) {
          //     console.log('args', args)
          //     return '$' + args.value;
          //   },
          //   precision: 2
          // },
          labels: [
            {
              render: function (args) {
                return args.percentage > 10 && that.chatConfig['panel_setting']['label_settings']['show_labels'] ? args.label : '';
              },
              position: 'outside',
              fontStyle: 'bold',
              fontColor: '#000',

            },
            {
              render: function (args) {
                console.log(args);
                return args.percentage > 10 && that.chatConfig['panel_setting']['label_settings']['show_values'] ? args.percentage + '%' : '';
              },
              fontStyle: 'bold',
              fontColor: '#000',
              precision: 2
            }
          ],
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '450px'
    }

    this.chartJson = this.libServ.deepCopy(this.defualtChartJson);
    this.chartJson['chartTypes'].push({ key: 'radar', label: 'Radar', stacked: false });
    this.chartJson['chartOptions']['legend']['position'] = this.chatConfig['panel_setting']['radar_settings']['legend_position'];
    this.chatConfig['data']['metrics'].forEach((element, i) => {
      if (element.isVisible) {

        this.chartJson['chartData']['datasets'].push({
          label: element.custom_label == '' ? this.getTitle(element) : element.custom_label,
          type: element.chart_type,
          format: this.getfielddata(element.field, 'format'),
          backgroundColor: this.convertHexToRGBA(colors[i], 0.2),
          borderColor: this.convertHexToRGBA(colors[i], 1),
          data: [],

        });

      }
    });
    let dimensions = [];
    let metrics = [];
    let timekey = [];
    let groupbBy = [];
    let orderBy = [];
    let derived_metrics = [];
    let dashboardMetrics = {};
    this.chatConfig['data']['buckets'].forEach(bucket => {

      dimensions.push(bucket.field);
      if (bucket.field != 'time_key' && bucket.field != 'accounting_key') {
        groupbBy.push(bucket.field);
        if (bucket.order_by.key == 'Custom Metrics') {
          orderBy.push({ key: bucket.order_by.field, opcode: bucket.order_by.mode });
          if (dashboardMetrics[bucket.order_by.field] == undefined) {
            dashboardMetrics[bucket.order_by.field] = [];
          }
          dashboardMetrics[bucket.order_by.field].push('sum');
          if (this.getfielddata(bucket.order_by.field, 'columnType') == 'derived_metrics') {
            derived_metrics.push(bucket.order_by.field);
            let calculate_columns = [];
            calculate_columns = this.getfielddata(bucket.order_by.field, 'calculate_columns');
            metrics = metrics.concat(calculate_columns);
            console.log(derived_metrics, this.getfielddata(bucket.order_by.field, 'calculate_columns'));
          } else {
            if (!metrics.some(x => x == bucket.order_by.field)) {
              metrics.push(bucket.order_by.field);
            }
          }
        } else if (bucket.order_by.key == 'Alphabetical') {
          orderBy.push({ key: bucket.field, opcode: bucket.order_by.mode });
        } else {
          if (this.checkMetricAggrigation(this.chatConfig['data']['metrics'][bucket.order_by.key]['field'], this.chatConfig['data']['metrics'][bucket.order_by.key]['aggregation'])) {
            orderBy.push({
              key: this.chatConfig['data']['metrics'][bucket.order_by.key]['field'], opcode: bucket.order_by.mode
            });
          } else {
            orderBy.push({
              key: this.chatConfig['data']['metrics'][bucket.order_by.key]['field'] + '_'
                + this.chatConfig['data']['metrics'][bucket.order_by.key]['aggregation'], opcode: bucket.order_by.mode
            });
          }

        }
      } else {
        timekey.push(bucket.field);

        orderBy.push({ key: bucket.field, opcode: 'asc' })

      }
    });
    this.chatConfig['data']['metrics'].forEach(metric => {

      if (metric.isVisible) {
        if (dashboardMetrics[metric.field] == undefined) {
          dashboardMetrics[metric.field] = [];
        }
        dashboardMetrics[metric.field].push(metric.aggregation.toLowerCase());
        if (this.getfielddata(metric.field, 'columnType') == 'derived_metrics') {
          derived_metrics.push(metric.field);
          let calculate_columns = [];
          calculate_columns = this.getfielddata(metric.field, 'calculate_columns');
          metrics = metrics.concat(calculate_columns);
          console.log(derived_metrics, this.getfielddata(metric.field, 'calculate_columns'));
        } else {
          if (!metrics.some(x => x == metric.field)) {
            metrics.push(metric.field);
          }
        }
      }
    });
    this.apiRequest = {
      dimensions: Array.from(new Set(dimensions.concat(['time_key']))),
      metrics: Array.from(new Set(metrics)),
      derived_metrics: Array.from(new Set(derived_metrics)),
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: { dimensions: [], metrics: [] },
      groupByTimeKey: {
        key: Array.from(new Set(timekey.concat(['time_key']))),
        interval: 'daily'
      },
      dashboardDetails: {

        isDashboard: 'True',
        dimensions: Array.from(new Set(dimensions)),

        metrics: [dashboardMetrics]
      },
      gidGroupBy: groupbBy.length ? Array.from(new Set(groupbBy)) : ['source'],
      orderBy: orderBy,
      limit: this.chatConfig['data']['buckets'][0]['order_by']['size'],
      offset: '0'
    };

    this.showChart = false;
    this.noDataChart = false;
    this.dataFetchServ.getChartData(this.apiRequest, this.keySpaces['api_url']).subscribe(data => {
      const chartData = data['data'];
      this.noDataChart = false;
      if (!chartData.length) {
        this.noDataChart = true;
        return;
      }
      const labelArr = Array.from(new Set(chartData.map(r => r[this.chatConfig['data']['buckets'][0]['field']])));
      if (this.chatConfig['data']['buckets'][0]['field'] == 'time_key') {
        this.chartJson['chartData']['labels'] = labelArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );
      } else if (this.chatConfig['data']['buckets'][0]['field'] == 'accounting_key') {
        this.chartJson['chartData']['labels'] = labelArr.map(d =>
          moment(d, 'YYYYMMDD').format('MMM-YYYY')
        );
      } else {
        this.chartJson['chartData']['labels'] = labelArr;

      }

      colors = this.libServ.dynamicColors(this.chartJson['chartData']['labels'].length);
      const dataSets = {};
      this.chatConfig['data']['metrics'].forEach(element => {
        if (element.isVisible) {
          if (this.checkMetricAggrigation(element.field, element.aggregation)) {
            dataSets[element.field] = [];
          } else {
            dataSets[element.field + '_' + element.aggregation] = [];
          }
        }
      });
      labelArr.forEach(label => {
        chartData.forEach(r => {
          if (r[this.chatConfig['data']['buckets'][0]['field']] === label) {
            this.chatConfig['data']['metrics'].forEach(element => {
              if (element.isVisible) {
                if (this.checkMetricAggrigation(element.field, element.aggregation)) {
                  dataSets[element.field].push(r[element.field]);
                } else {
                  dataSets[element.field + '_' + element.aggregation].push(r[element.field + '_' + element.aggregation]);
                }
              }
            });

          }
        });
      });
      this.chatConfig['data']['metrics'].filter(x => x.isVisible).forEach((element, i) => {
        if (element.isVisible) {
          if (this.checkMetricAggrigation(element.field, element.aggregation)) {
            this.chartJson['chartData']['datasets'][i]['data'] = dataSets[element.field];
          } else {
            this.chartJson['chartData']['datasets'][i]['data'] = dataSets[element.field + '_' + element.aggregation];
          }
        }
      });

      this.chartJsonMain = this.libServ.deepCopy(this.chartJson);
      this.isChange = true;
      console.log('chartjs', this.chartJson);
      this.showChart = true;
    });
  }

  runCardsPanelQuery() {
    let colors = this.libServ.dynamicColors(10);
    console.log('chart', this.chatConfig);
    let that = this;
    this.defualtChartJson = {
      data: [
      ]
    };
    this.chartJson = this.libServ.deepCopy(this.defualtChartJson);

    this.chatConfig['data']['metrics'].forEach((metric, i) => {
      if (metric.isVisible) {
        this.chartJson['data'].push({
          field: this.checkMetricAggrigation(metric.field, metric.aggregation) ? metric.field : metric.field + '_' + metric.aggregation,
          displayName: metric.custom_label == '' ? this.getTitle(metric) : metric.custom_label,
          value: 0,
          imageClass: metric.icon_class,
          format: this.getfielddata(metric.field, 'format'),
          config: [],
          color: metric.card_color
        });
      }
    });
    let dimensions = [];
    let metrics = [];
    let timekey = [];
    let groupbBy = [];
    let orderBy = [];
    let derived_metrics = [];
    let dashboardMetrics = {};
    this.chatConfig['data']['metrics'].forEach(metric => {
      if (metric.isVisible) {
        if (dashboardMetrics[metric.field] == undefined) {
          dashboardMetrics[metric.field] = [];
        }
        dashboardMetrics[metric.field].push(metric.aggregation.toLowerCase());
        if (this.getfielddata(metric.field, 'columnType') == 'derived_metrics') {
          derived_metrics.push(metric.field);
          let calculate_columns = [];
          calculate_columns = this.getfielddata(metric.field, 'calculate_columns');
          metrics = metrics.concat(calculate_columns);
          console.log(derived_metrics, this.getfielddata(metric.field, 'calculate_columns'));
        } else {
          if (!metrics.some(x => x == metric.field)) {
            metrics.push(metric.field);
          }

        }
      }
    });
    this.apiRequest = {
      dimensions: Array.from(new Set(dimensions.concat(['time_key']))),
      metrics: Array.from(new Set(metrics)),
      derived_metrics: Array.from(new Set(derived_metrics)),
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: { dimensions: [], metrics: [] },
      groupByTimeKey: {
        key: Array.from(new Set(timekey.concat(['time_key']))),
        interval: 'daily'
      },
      dashboardDetails: {

        isDashboard: 'True',
        dimensions: Array.from(new Set(dimensions)),

        metrics: [dashboardMetrics]
      },
      gidGroupBy: groupbBy.length ? Array.from(new Set(groupbBy)) : ['source'],
      orderBy: orderBy,
      limit: '',
      offset: ''
    };

    this.showChart = false;
    this.noDataChart = false;
    this.dataFetchServ.getChartData(this.apiRequest, this.keySpaces['api_url']).subscribe(data => {
      const chartData = data['data'];
      this.noDataChart = false;
      if (!chartData.length) {
        this.noDataChart = true;
        return;
      }
      if (!this.libServ.isEmptyObj(data['data'])) {
        this.chartJson['data'].map(o => {
          o['value'] = data['data'][0][o['field']];
        });
      } else {
        this.chartJson['data'].map(o => {
          o['value'] = 0;
        });
      }
      this.chartJsonMain = this.libServ.deepCopy(this.chartJson);
      this.isChange = true;
      console.log('chartjs', this.chartJson);
      this.showChart = true;
    });

  }

  runDataTableQuery() {
    let colors = this.libServ.dynamicColors(10);
    console.log('chart', this.chatConfig);
    let that = this;
    this.defualtChartJson = {
      flatTableJson: {
        page_size: this.chatConfig['table_setting']['data_setting']['page_size'],
        page: 0,
        lazy: false,
        loading: false,
        export: this.chatConfig['table_setting']['setting']['export'],
        sortMode: 'multiple',
        resizableColumns: this.chatConfig['table_setting']['setting']['resizableColumns'],
        columnResizeMode: 'fit',
        reorderableColumns: true,
        scrollHeight: '350px',
        totalRecords: 1000,
        columns: [],
        selectedColumns: [],
        globalFilterFields: [],
        frozenCols: [],
        frozenWidth: '0px',
        scrollable: true,
        selectionMode: 'multiple',
        selectedColsModal: [],
        selectionDataKey: 'name',
        metaKeySelection: true,
        showHideCols: this.chatConfig['table_setting']['setting']['showHideCols'],
        overallSearch: this.chatConfig['table_setting']['setting']['overallSearch'],
        columnSearch: this.chatConfig['table_setting']['setting']['columnSearch']
      },
      flatTableData: []
    };
    this.chartJson = this.libServ.deepCopy(this.defualtChartJson);
    let coldef = [];
    let frozenCols = [];
    this.chatConfig['data']['buckets'].forEach((bucket, i) => {
      if (bucket.isVisible) {
        if (bucket.options.isfrozen) {
          frozenCols.push({
            field: bucket.field,
            displayName: bucket.custom_label == '' ? this.getTitle(bucket) : bucket.custom_label,
            format: this.getfielddata(bucket.field, 'format'),
            width: bucket.width,
            value: '',
            condition: '',
            columnType: 'dimensions',
            isClearSearch: true,
            exportConfig: {
              format: this.getfielddata(bucket.field, 'format'),
              styleinfo: {
                thead: 'default',
                tdata: 'white'
              }
            },
            formatConfig: [],
            options: {
              editable: bucket.options.editable,
              colSearch: bucket.options.colSearch,
              colSort: bucket.options.colSort,
              resizable: bucket.options.resizable,
              movable: bucket.options.movable
            }
          });
        } else {
          coldef.push({
            field: bucket.field,
            displayName: bucket.custom_label == '' ? this.getTitle(bucket) : bucket.custom_label,
            format: this.getfielddata(bucket.field, 'format'),
            width: bucket.width,
            value: '',
            condition: '',
            columnType: 'dimensions',
            isClearSearch: true,
            exportConfig: {
              format: this.getfielddata(bucket.field, 'format'),
              styleinfo: {
                thead: 'default',
                tdata: 'white'
              }
            },
            formatConfig: [],
            options: {
              editable: bucket.options.editable,
              colSearch: bucket.options.colSearch,
              colSort: bucket.options.colSort,
              resizable: bucket.options.resizable,
              movable: bucket.options.movable
            }
          });
        }
      }
    });

    this.chatConfig['data']['metrics'].forEach((metric, i) => {
      if (metric.isVisible) {
        coldef.push({
          field: this.checkMetricAggrigation(metric.field, metric.aggregation) ?
            metric.field : metric.field + '_' + metric.aggregation,
          displayName: metric.custom_label == '' ? this.getTitle(metric) : metric.custom_label,
          format: this.getfielddata(metric.field, 'format'),
          width: metric.width,
          value: '',
          condition: '',
          columnType: 'dimensions',
          isClearSearch: true,
          exportConfig: {
            format: this.getfielddata(metric.field, 'format') == '$' ? 'currency' : this.getfielddata(metric.field, 'format'),
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: metric.options.editable,
            colSearch: metric.options.colSearch,
            colSort: metric.options.colSort,
            resizable: metric.options.resizable,
            movable: metric.options.movable
          }
        });
      }
    });

    this.chartJson['flatTableJson']['columns'] = coldef;
    this.chartJson['flatTableJson']['selectedColumns'] = coldef;
    this.chartJson['flatTableJson']['globalFilterFields'] = frozenCols.concat(coldef);
    this.chartJson['flatTableJson']['frozenCols'] = frozenCols.length > 0 ? frozenCols : undefined;
    this.chartJson['flatTableJson']['frozenWidth'] = frozenCols.reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px';
    let dimensions = [];
    let metrics = [];
    let timekey = [];
    let groupbBy = [];
    let orderBy = [];
    let derived_metrics = [];
    let dashboardMetrics = {};
    this.chatConfig['data']['buckets'].forEach(bucket => {
      if (bucket.isVisible) {
        dimensions.push(bucket.field);
        if (bucket.field != 'time_key' && bucket.field != 'accounting_key') {
          groupbBy.push(bucket.field);
        } else {
          timekey.push(bucket.field);
        }
      }
    });
    this.chatConfig['data']['metrics'].forEach(metric => {

      if (metric.isVisible) {
        if (dashboardMetrics[metric.field] == undefined) {
          dashboardMetrics[metric.field] = [];
        }
        dashboardMetrics[metric.field].push(metric.aggregation.toLowerCase());
        if (this.getfielddata(metric.field, 'columnType') == 'derived_metrics') {
          derived_metrics.push(metric.field);
          let calculate_columns = [];
          calculate_columns = this.getfielddata(metric.field, 'calculate_columns');
          metrics = metrics.concat(calculate_columns);
        } else {
          if (!metrics.some(x => x == metric.field)) {
            metrics.push(metric.field);
          }
        }
      }
    });

    this.chatConfig['table_setting']['data_setting']['order'].forEach((element, idx) => {
      if (element.key.type == 'D') {
        orderBy.push({ key: this.chatConfig['data']['buckets'][element.key.index]['field'], opcode: element.opcode });
      } else {
        if (this.checkMetricAggrigation(this.chatConfig['data']['metrics'][element.key.index]['field'], this.chatConfig['data']['metrics'][element.key.index]['aggregation'])) {
          orderBy.push({
            key: this.chatConfig['data']['metrics'][element.key.index]['field']
          });
        } else {
          orderBy.push({
            key: this.chatConfig['data']['metrics'][element.key.index]['field'] + '_'
              + this.chatConfig['data']['metrics'][element.key.index]['aggregation'], opcode: element.opcode
          });
        }
      }
    });
    this.apiRequest = {
      dimensions: Array.from(new Set(dimensions.concat(['time_key']))),
      metrics: Array.from(new Set(metrics)),
      derived_metrics: Array.from(new Set(derived_metrics)),
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: { dimensions: [], metrics: [] },
      groupByTimeKey: {
        key: Array.from(new Set(timekey.concat(['time_key']))),
        interval: 'daily'
      },
      dashboardDetails: {

        isDashboard: 'True',
        dimensions: Array.from(new Set(dimensions)),

        metrics: [dashboardMetrics]
      },
      gidGroupBy: groupbBy.length ? Array.from(new Set(groupbBy)) : ['source'],
      orderBy: orderBy,
      limit: this.chatConfig['table_setting']['data_setting']['limit'],
      offset: '0'
    };
    this.noDataChart = false;
    this.showChart = false;
    // // this.noDataChart = false;
    // this.noDataChart = true;
    // this.showChart = true;
    this.dataFetchServ.getChartData(this.apiRequest, this.keySpaces['api_url']).subscribe(data => {
      if (data['status'] === 0) {

        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      } else {


      }
      const arr = [];
      const totalsColData = data['data'];
      for (const r of totalsColData) {
        const obj = {};
        obj['data'] = r;
        arr.push(obj);
      }
      // this.tblResData = arr;

      this.chartJson['flatTableData'] = <TreeNode[]>arr;
      this.chartJson['flatTableJson']['totalRecords'] = totalsColData.length;
      this.chartJson['flatTableJson']['loading'] = false;
      this.isChange = true;
      this.showChart = true;
      setTimeout(() => {
        this.ngAfterViewInit();
      }, 0);

      console.log('chartjson', this.chartJson)
    });

  }
  runHorizontalBarQuery() {
    let colors = this.libServ.dynamicColors(10);
    this.defualtChartJson = {
      chartTypes: [],
      chartData: {
        labels: [],
        datasets: []
      },
      chartOptions: {
        title: {
          display: true,
          text: this.title
        },
        legend: {
          display: true,
          position: 'top'
        },
        elements: {
          point: {
            display: false
          }
        },
        scales: {
          xAxes: [],
          yAxes: []
        },
        tooltips: {
          mode: this.chatConfig['data']['buckets'][0]['slice_data']['is_show'] ? 'nearest' : 'index',
          intersect: false,
          enabled: this.chatConfig['panel_setting']['settings']['show_tooltips'],
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, data.datasets[tooltipItem.datasetIndex].format, [])}`;
            }
          }
        },
        plugins: {
          datalabels: false,
          labels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '450px',
      calculate_value: this.chatConfig['panel_setting']['settings']['calculate_value'],
    }
    this.chartJson = this.libServ.deepCopy(this.defualtChartJson);
    this.chartJson['chartTypes'].push({ key: 'horizontalBar', label: 'Line Chart', stacked: false });
    this.chatConfig['data']['metrics'].forEach((element, i) => {
      if (element.isVisible) {

        this.chartJson['chartData']['datasets'].push({
          label: element.custom_label == '' ? this.getTitle(element) : element.custom_label,
          borderColor: element.legend_color,
          format: this.getfielddata(element.field, 'format'),
          fill: false,
          yAxisID: 'LeftAxis-1',
          backgroundColor: element.legend_color,
          data: [],
          trendlineLinear: (this.chatConfig['panel_setting']['threshold_line']['is_show']) ? {
            style: this.chatConfig['panel_setting']['threshold_line']['line_color'],
            lineStyle: this.chatConfig['panel_setting']['threshold_line']['line_style'],
            width: this.chatConfig['panel_setting']['threshold_line']['line_width']
          } : undefined
        });

      }
    });

    const xAxes = this.chatConfig['data']['buckets'][0];
    this.chartJson['chartOptions']['scales']['yAxes'].push({
      type: 'category',
      id: 'LeftAxis-1',
      display: true,
      scaleLabel: {
        display: xAxes.show_axis_lines_label.is_show,
        labelString: xAxes.custom_label == '' ? this.getTitle(xAxes) : xAxes.custom_label,
      },
      gridLines: {
        display: this.chatConfig['panel_setting']['grid']['y_axis_lines']
      },
      position: xAxes.position,
      stacked: true,
      ticks: {
        // autoRotate: true,
        minRotation: xAxes.show_axis_lines_label.show_label.align.minRotation,
        maxRotation: xAxes.show_axis_lines_label.show_label.align.maxRotation,
        display: xAxes.show_axis_lines_label.show_label.is_show,
        callback: (value, index, values) => {
          // return (value / this.max * 100).toFixed(0) + '%';
          return value.toString().substr(0, xAxes.show_axis_lines_label.show_label.truncate);
        }
      }
    });
    this.chatConfig['data']['metrics'].filter(x => x.isVisible).forEach((element, i) => {

      this.chartJson['chartOptions']['scales']['xAxes'].push({
        gridLines: {
          display: this.chatConfig['panel_setting']['grid']['x_axis_lines']
        },

        display: true,
        scaleLabel: {
          display: element.show_axis_lines_label.is_show,
          labelString: element.custom_label == '' ? this.getTitle(element) : element.custom_label,
        },
        position: element.position,
        name: '1',
        stacked: this.chatConfig['data']['buckets'][0]['slice_data']['is_show'] && this.chatConfig['data']['buckets'][0]['slice_data']['isStacked'],
        format: this.getfielddata(element.field, 'format'),
        ticks: {
          beginAtZero: true,
          minRotation: element.show_axis_lines_label.show_label.align.minRotation,
          maxRotation: element.show_axis_lines_label.show_label.align.maxRotation,
          min: element.custom_extend.axis_extend.min == '' ? undefined : element.custom_extend.axis_extend.min,
          max: element.custom_extend.axis_extend.max == '' ? undefined : element.custom_extend.axis_extend.max,
          stepSize: (element.custom_extend.scale_to_bound.is_show && element.custom_extend.scale_to_bound.range != '') ? element.custom_extend.scale_to_bound.range : undefined,
          display: element.show_axis_lines_label.show_label.is_show,
          callback: (value, index, values) => {
            // return (value / this.max * 100).toFixed(0) + '%';

            return this.formatNumPipe.transform(value, this.chartJson['chartOptions']['scales']['xAxes'][i].format, []).substr(0, element.show_axis_lines_label.show_label.truncate + 1);
          }
        }
        // scaleFontColor: "rgba(151,137,200,0.8)"
      });
    });
    this.chartJson['chartOptions']['legend']['position'] = this.chatConfig['panel_setting']['settings']['legend_position'];
    console.log(this.chartJson['chartOptions']['legend']['position'], this.chatConfig['panel_setting']['settings']['legend_position'])
    let dimensions = [];
    let metrics = [];
    let timekey = [];
    let groupbBy = [];
    let orderBy = [];
    let derived_metrics = [];
    let dashboardMetrics = {};
    let bucketData = [];
    let graphOrderBy = [];
    bucketData.push(this.chatConfig['data']['buckets'][0]);
    if (this.chatConfig['data']['buckets'][0]['slice_data']['is_show']) {
      bucketData.push(this.chatConfig['data']['buckets'][0]['slice_data']);
    }
    bucketData.forEach(bucket => {
      console.log('order', bucket.order_by.key);

      dimensions.push(bucket.field);
      if (bucket.field != 'time_key' && bucket.field != 'accounting_key') {
        groupbBy.push(bucket.field);
        if (bucket.order_by.key == 'Custom Metrics') {
          orderBy.push({ key: bucket.order_by.field, opcode: bucket.order_by.mode });
          if (dashboardMetrics[bucket.order_by.field] == undefined) {
            dashboardMetrics[bucket.order_by.field] = [];
          }
          dashboardMetrics[bucket.order_by.field].push('sum');
          if (this.getfielddata(bucket.order_by.field, 'columnType') == 'derived_metrics') {
            derived_metrics.push(bucket.order_by.field);
            let calculate_columns = [];
            calculate_columns = this.getfielddata(bucket.order_by.field, 'calculate_columns');
            metrics = metrics.concat(calculate_columns);
            console.log(derived_metrics, this.getfielddata(bucket.order_by.field, 'calculate_columns'));
          } else {
            if (!metrics.some(x => x == bucket.order_by.field)) {
              metrics.push(bucket.order_by.field);
            }
          }
        } else if (bucket.order_by.key == 'Alphabetical') {
          orderBy.push({ key: bucket.field, opcode: bucket.order_by.mode });
        } else {
          if (this.checkMetricAggrigation(this.chatConfig['data']['metrics'][bucket.order_by.key]['field'], this.chatConfig['data']['metrics'][bucket.order_by.key]['aggregation'])) {
            orderBy.push({
              key: this.chatConfig['data']['metrics'][bucket.order_by.key]['field'], opcode: bucket.order_by.mode
            });
          } else {
            orderBy.push({
              key: this.chatConfig['data']['metrics'][bucket.order_by.key]['field'] + '_'
                + this.chatConfig['data']['metrics'][bucket.order_by.key]['aggregation'], opcode: bucket.order_by.mode
            });
          }

        }
      } else {
        timekey.push(bucket.field);

        orderBy.push({ key: bucket.field, opcode: 'asc' })

      }
    });
    this.chatConfig['data']['metrics'].forEach(metric => {

      if (metric.isVisible) {
        if (dashboardMetrics[metric.field] == undefined) {
          dashboardMetrics[metric.field] = [];
        }
        dashboardMetrics[metric.field].push(metric.aggregation.toLowerCase());
        if (this.getfielddata(metric.field, 'columnType') == 'derived_metrics') {
          derived_metrics.push(metric.field);
          let calculate_columns = [];
          calculate_columns = this.getfielddata(metric.field, 'calculate_columns');
          metrics = metrics.concat(calculate_columns);
          console.log(derived_metrics, this.getfielddata(metric.field, 'calculate_columns'));
        } else {
          if (!metrics.some(x => x == metric.field)) {
            metrics.push(metric.field);
          }
        }
      }
    });
    if (this.chatConfig['data']['buckets'][0]['slice_data']['is_show']) {
      graphOrderBy.push({
        colmun_Name: this.chatConfig['data']['buckets'][0]['field'],
        order: orderBy[0]['opcode'],
        limit: this.chatConfig['data']['buckets'][0]['order_by']['size'] == null ? '' : this.chatConfig['data']['buckets'][0]['order_by']['size'],
        sort_column: orderBy[0]['key']
      }, {
        colmun_Name: this.chatConfig['data']['buckets'][0]['slice_data']['field'],
        order: orderBy[1]['opcode'],
        limit: this.chatConfig['data']['buckets'][0]['slice_data']['order_by']['size'] == null ? '' : this.chatConfig['data']['buckets'][0]['slice_data']['order_by']['size'],
        sort_column: orderBy[1]['key']
      })
      orderBy = [];
    }

    this.apiRequest = {
      dimensions: Array.from(new Set(dimensions.concat(['time_key']))),
      metrics: Array.from(new Set(metrics)),
      derived_metrics: Array.from(new Set(derived_metrics)),
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: { dimensions: [], metrics: [] },
      groupByTimeKey: {
        key: Array.from(new Set(timekey.concat(['time_key']))),
        interval: 'daily'
      },
      dashboardDetails: {

        isDashboard: 'True',
        dimensions: Array.from(new Set(dimensions)),
        // graphOrderBy: graphOrderBy.length > 0 ? graphOrderBy : null,
        metrics: [dashboardMetrics]
      },
      gidGroupBy: groupbBy.length ? Array.from(new Set(groupbBy)) : ['source'],
      orderBy: orderBy,
      limit: this.chatConfig['data']['buckets'][0]['slice_data']['is_show'] ? '' : this.chatConfig['data']['buckets'][0]['order_by']['size'],
      offset: '0'
    };
    if (graphOrderBy.length > 0) {
      this.apiRequest['dashboardDetails']['graphOrderBy'] = graphOrderBy;
    }
    this.showChart = false;
    this.noDataChart = false;
    this.dataFetchServ.getChartData(this.apiRequest, this.keySpaces['api_url']).subscribe(data => {
      const chartData = data['data'];
      this.noDataChart = false;
      if (!chartData.length) {
        this.noDataChart = true;
        return;
      }
      const labelArr = Array.from(new Set(chartData.map(r => r[this.chatConfig['data']['buckets'][0]['field']])));
      if (this.chatConfig['data']['buckets'][0]['field'] == 'time_key') {
        this.chartJson['chartData']['labels'] = labelArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );
      } else if (this.chatConfig['data']['buckets'][0]['field'] == 'accounting_key') {
        this.chartJson['chartData']['labels'] = labelArr.map(d =>
          moment(d, 'YYYYMMDD').format('MMM-YYYY')
        );
      } else {
        this.chartJson['chartData']['labels'] = labelArr;

      }
      if (this.chatConfig['data']['buckets'][0]['slice_data']['is_show']) {

        if (this.chatConfig['data']['buckets'][0]['slice_data']['isStacked']) {
          this.chartJson['chartTypes'][0]['key'] = 'horizontalBar';
          this.chartJson['chartTypes'][0]['stacked'] = true;
        }
        this.chartJson['chartData']['datasets'] = [];
        const splitLabelArr = Array.from(new Set(chartData.map(r => r[this.chatConfig['data']['buckets'][0]['slice_data']['field']])));
        colors = this.libServ.dynamicColors(splitLabelArr.length + 2);
        splitLabelArr.forEach((splitLabel, i) => {
          let labelName = splitLabel;
          if (this.chatConfig['data']['buckets'][0]['slice_data']['field'] == 'time_key') {

            labelName = moment(splitLabel, 'YYYYMMDD').format('MM-DD-YYYY');
          } else if (this.chatConfig['data']['buckets'][0]['slice_data']['field'] == 'accounting_key') {
            labelName = moment(splitLabel, 'YYYYMMDD').format('MM-YYYY');
          } else {
            labelName = splitLabel;

          }
          const dataSets = {};
          this.chatConfig['data']['metrics'].forEach(element => {
            if (element.isVisible) {
              if (this.checkMetricAggrigation(element.field, element.aggregation)) {
                dataSets[element.field] = [];
              } else {
                dataSets[element.field + '_' + element.aggregation] = [];
              }
            }
          });
          labelArr.forEach(label => {
            let isAvailable = true;
            chartData.forEach(r => {
              if (r[this.chatConfig['data']['buckets'][0]['field']] === label &&
                r[this.chatConfig['data']['buckets'][0]['slice_data']['field']] === splitLabel) {
                isAvailable = false;
                this.chatConfig['data']['metrics'].forEach(element => {
                  if (this.checkMetricAggrigation(element.field, element.aggregation)) {
                    dataSets[element.field].push(r[element.field]);
                  } else {
                    dataSets[element.field + '_' + element.aggregation].push(r[element.field + '_' + element.aggregation]);
                  }
                });
              }
            });
            if (isAvailable) {

              this.chatConfig['data']['metrics'].forEach(element => {
                if (this.checkMetricAggrigation(element.field, element.aggregation)) {
                  dataSets[element.field].push(null);
                } else {
                  dataSets[element.field + '_' + element.aggregation].push(null);
                }
              });

            }
          });
          this.chatConfig['data']['metrics'].forEach((element) => {
            if (element.isVisible) {
              this.chartJson['chartData']['datasets'].push({
                label: labelName,
                borderColor: colors[i + 2],
                yAxisID: 'LeftAxis-1',
                format: this.getfielddata(element.field, 'format'),
                fill: false,
                backgroundColor: colors[i + 2],
                data: (this.checkMetricAggrigation(element.field, element.aggregation)) ?
                  dataSets[element.field]
                  : dataSets[element.field + '_' + element.aggregation]
              });
            }
          });
        });
      } else {
        const dataSets = {};
        this.chatConfig['data']['metrics'].forEach(element => {
          if (element.isVisible) {
            if (this.checkMetricAggrigation(element.field, element.aggregation)) {
              dataSets[element.field] = [];
            } else {
              dataSets[element.field + '_' + element.aggregation] = [];
            }
          }
        });
        labelArr.forEach(label => {
          chartData.forEach(r => {
            if (r[this.chatConfig['data']['buckets'][0]['field']] === label) {
              this.chatConfig['data']['metrics'].forEach(element => {
                if (element.isVisible) {
                  if (this.checkMetricAggrigation(element.field, element.aggregation)) {
                    dataSets[element.field].push(r[element.field]);
                  } else {
                    dataSets[element.field + '_' + element.aggregation].push(r[element.field + '_' + element.aggregation]);
                  }
                }
              });

            }
          });
        });
        this.chatConfig['data']['metrics'].filter(x => x.isVisible).forEach((element, i) => {
          if (element.isVisible) {
            if (this.checkMetricAggrigation(element.field, element.aggregation)) {
              this.chartJson['chartData']['datasets'][i]['data'] = dataSets[element.field];
            } else {
              this.chartJson['chartData']['datasets'][i]['data'] = dataSets[element.field + '_' + element.aggregation];
            }
          }
        });
      }
      this.chartJsonMain = this.libServ.deepCopy(this.chartJson);
      this.isChange = true;
      console.log('chartjs', this.chartJson);
      this.showChart = true;
    });

  }


  removeElement(array, value) {
    const index = array.indexOf(value);
    if (index > -1) {
      array.splice(index, 1);
    }
    return array;
  }

  convertHexToRGBA = (hexCode, opacity) => {
    let hex = hexCode.replace('#', '');
    if (hex.length === 3) {
      hex += hex
    }
    const r = parseInt(hex.substring(0, 2), 16);
    const g = parseInt(hex.substring(2, 4), 16);
    const b = parseInt(hex.substring(4, 6), 16);
    return `rgba(${r},${g},${b},${opacity})`;
  };

  onFiltersApplied(filterData: object) {

    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];
    if (this.chartType == 'pie' || this.chartType == 'polarArea') {
      this.runPieChartQuery();
    } else if (this.chartType == 'cards_panel') {
      this.runRadarChartQuery();
    } else if (this.chartType == 'radar') {
      this.runCardsPanelQuery();
    } else if (this.chartType == 'data_table') {
      this.runDataTableQuery();
    } else if (this.chartType == 'horizontalBar') {
      this.runHorizontalBarQuery();
    } else {
      this.runChartQuery();
    }
  }
  clearChartQuery() {
    this.isChange = false;
    if (this.isUpdate) {
      this.initialLoading();
    } else {
      this.chartType = '';
      this.keySpaces = {};
      this.noDataChart = true;
      this.isValid = false;
      this.showChart = false;
      this.chatConfig = {};
      this.keySpacesOriginal = {};
      this.index = 0;
      this.sampleChatConfig = this.libServ.deepCopy(this.defualtChatConfig);
    }


  }

  runQuery() {
    this.Validation();
    if (this.isValid) {
      if (this.chartType == 'pie' || this.chartType == 'polarArea') {
        this.runPieChartQuery();
      } else if (this.chartType == 'cards_panel') {
        this.runCardsPanelQuery();
      } else if (this.chartType == 'radar') {
        this.runRadarChartQuery();
      } else if (this.chartType == 'data_table') {
        this.runDataTableQuery();
      } else if (this.chartType == 'horizontalBar') {
        this.runHorizontalBarQuery();
      } else {
        this.runChartQuery();
      }
    }
  }

  changeAxisType() {
    this.chatConfig['data']['metrics'][1]['isVisible'] = !this.chatConfig['data']['metrics'][1]['isVisible'];
    if (!this.chatConfig['data']['metrics'][1]['isVisible']) {
      this.chatConfig['data']['metrics'][0]['isSlice'] = true;
      this.chatConfig['data']['metrics'][1]['isSlice'] = false;
      this.sliceYAxis = 'y_axis_1';
    }
  }
  saveChartQuery() {
    const req = {
      user_id: this.appConfig['user']['id'],
      title: '',
      description: '',
      chart_type: this.chartTypeData.find(x => x.chart_type == this.chartType).display_name,
      chart_config: this.chatConfig,
      final_chart_config: JSON.stringify(this.chartJsonMain),
      api_request: JSON.stringify(this.apiRequest),
      keyspace_id: this.keySpaces['id'],
      isUpdate: false
    };
    const ref = this.dialogService.open(ChartDetailsComponent, {
      data: req,
      header: 'Chart Details',
      contentStyle: { width: '40vw', overflow: 'auto' },
    });

    ref.onClose.subscribe((data: string) => {
    });
  }
  updateChartQuery() {
    const req = {
      id: this.visualizeId,
      user_id: this.appConfig['user']['id'],
      title: this.title,
      description: this.description,
      chart_type: this.chartTypeData.find(x => x.chart_type == this.chartType).display_name,
      chart_config: this.chatConfig,
      final_chart_config: JSON.stringify(this.chartJsonMain),
      api_request: JSON.stringify(this.apiRequest),
      keyspace_id: this.keySpaces['id'],
      isUpdate: true,
    };
    const ref = this.dialogService.open(ChartDetailsComponent, {
      data: req,
      header: 'Chart Details',
      contentStyle: { width: '40vw', overflow: 'auto' },
    });

    ref.onClose.subscribe((data: string) => {
    });
  }
  getchbeckVisible() {
    var result = 0;
    this.chatConfig['data']['metrics'].forEach(element => {
      if (element.isVisible) {
        result++;
      }
    });
    // console.log(result);
    return result == 1;
  }

  getcheckBucketVisible() {
    var result = 0;
    this.chatConfig['data']['buckets'].forEach(element => {
      if (element.isVisible) {
        result++;
      }
    });
    // console.log(result);
    return result == 1;
  }
  backToHome() {
    if (localStorage.getItem('chartType') == 'updateInDashboard') {
      if (localStorage.getItem('chartType')) {
        localStorage.removeItem('chartType');
      }
      if (localStorage.getItem('keyspaceId')) {
        localStorage.removeItem('keyspaceId');
      }
      this.router.navigate(['/ad-hoc-dashboard']);
    } else {
      if (localStorage.getItem('chartType')) {
        localStorage.removeItem('chartType');
      }
      if (localStorage.getItem('keyspaceId')) {
        localStorage.removeItem('keyspaceId');
      }
      this.router.navigate(['/visualize']);
    }

  }
  changeAxisExtend(i) {
    if (this.chatConfig['data']['metrics'][i]['custom_extend']['axis_extend']['is_show']) {
      this.chatConfig['data']['metrics'][i]['custom_extend']['axis_extend']['min'] = '0';
      this.chatConfig['data']['metrics'][i]['custom_extend']['axis_extend']['max'] = '100';
    } else {
      this.chatConfig['data']['metrics'][i]['custom_extend']['axis_extend']['min'] = '';
      this.chatConfig['data']['metrics'][i]['custom_extend']['axis_extend']['max'] = '';
    }
  }

  changeTicksRange(i) {
    if (this.chatConfig['data']['metrics'][i]['custom_extend']['scale_to_bound']['is_show']) {
      this.chatConfig['data']['metrics'][i]['custom_extend']['scale_to_bound']['range'] = '';
    } else {
      this.chatConfig['data']['metrics'][i]['custom_extend']['scale_to_bound']['range'] = '';
    }
  }

  changeRadarAxisExtend(i) {
    if (this.chatConfig['panel_setting']['radar_settings']['axis_extend']['is_show']) {
      this.chatConfig['panel_setting']['radar_settings']['axis_extend']['min'] = 0;
      this.chatConfig['panel_setting']['radar_settings']['axis_extend']['max'] = 100;
    } else {
      this.chatConfig['panel_setting']['radar_settings']['axis_extend']['min'] = '';
      this.chatConfig['panel_setting']['radar_settings']['axis_extend']['max'] = '';
    }
  }
  chartSelected(event) {

  }
  changesDate(type) {

    switch (type) {
      case 'today':
        {

          this.customize_daterange['date_range'] = 'today';
          this.customize_daterange['time_key1'] = moment().format('YYYYMMDD');
          this.customize_daterange['time_key2'] = moment().format('YYYYMMDD');
          this.customize_daterange['label'] = 'Today';
          this.customize_daterange['is_show'] = true;
        }
        break;

      case 'yesterday':
        {
          this.customize_daterange['date_range'] = 'yesterday';
          this.customize_daterange['time_key1'] = moment().subtract(1, 'days').format('YYYYMMDD');
          this.customize_daterange['time_key2'] = moment().subtract(1, 'days').format('YYYYMMDD');
          this.customize_daterange['label'] = 'Yesterday';
          this.customize_daterange['is_show'] = true;
        }
        break;
      case 'last_7_days':
        {
          this.customize_daterange['date_range'] = 'last_7_days';
          this.customize_daterange['time_key1'] = moment().subtract(7, 'days').format('YYYYMMDD');
          this.customize_daterange['time_key2'] = moment().subtract(1, 'days').format('YYYYMMDD');
          this.customize_daterange['label'] = 'Last 7 days';
          this.customize_daterange['is_show'] = true;

        }
        break;
      case 'last_30_days':
        {
          this.customize_daterange['date_range'] = 'last_30_days';
          this.customize_daterange['time_key1'] = moment().subtract(30, 'days').format('YYYYMMDD');
          this.customize_daterange['time_key2'] = moment().subtract(1, 'days').format('YYYYMMDD');
          this.customize_daterange['label'] = 'Last 30 days';
          this.customize_daterange['is_show'] = true;
        }
        break;
      case 'this_month':
        {
          this.customize_daterange['date_range'] = 'this_month';
          this.customize_daterange['time_key1'] = moment().startOf('month').format('YYYYMMDD');
          this.customize_daterange['time_key2'] = moment().format('YYYYMMDD');
          this.customize_daterange['label'] = 'This Month';
          this.customize_daterange['is_show'] = true;
        }
        break;
      case 'last_month':
        {
          this.customize_daterange['date_range'] = 'last_month';
          this.customize_daterange['time_key1'] = moment().subtract(1, 'months').startOf('month').format('YYYYMMDD');
          this.customize_daterange['time_key2'] = moment().subtract(1, 'months').endOf('month').format('YYYYMMDD');
          this.customize_daterange['label'] = 'Last Month';
          this.customize_daterange['is_show'] = true;
        }
        break;

      case 'last_week':
        {

          this.customize_daterange['date_range'] = 'last_week';
          this.customize_daterange['time_key1'] = moment().subtract(1, 'weeks').startOf('isoWeek').format('YYYYMMDD');
          this.customize_daterange['time_key2'] = moment().subtract(1, 'weeks').endOf('isoWeek').format('YYYYMMDD');
          this.customize_daterange['label'] = 'Last Week';
          this.customize_daterange['is_show'] = true;
        }
        break;
    }
    return this.customize_daterange;
  }
  getIconDetails() {
    window.open("https://fontawesome.com/icons", "_blank");
  }

  getTitle(metrics) {
    if (metrics['field'] != 'countofdimension') {
      return metrics['custom_label'] == '' ? this.getAggrigationData(metrics['aggregation'], 'label') + ' of ' + this.getfielddata(metrics['field'], 'displayName') : metrics['custom_label']
    } else {
      return metrics['custom_label'] == '' ? this.getfielddata(metrics['field'], 'displayName') : metrics['custom_label'];
    }
  }

  getAggrigationData(field, returntype) {
    let groupedAggregation = [
      { label: 'Average', value: 'mean' },
      { label: 'Count', value: 'count' },
      { label: 'Max', value: 'max' },
      // { label: 'Median', value: 'Median' },
      { label: 'Min', value: 'min' },
      // { label: 'Percentile Ranks', value: 'Percentile Ranks' },
      // { label: 'Percentiles', value: 'Percentiles' },
      { label: 'Standard Deviation', value: 'std' },
      { label: 'Sum', value: 'sum' },
      { label: 'Sum', value: 'Sum' },
      // { label: 'Top Hit', value: 'Top Hit' },
      // { label: 'Unique Count', value: 'Unique Count' }
      { label: 'Variance', value: 'var' },
      // { label: 'Size', value: 'size' },
      { label: 'Date Histogram', value: 'Date Histogram' },
      { label: 'Terms', value: 'Terms' }
    ];
    if (field) {
      return groupedAggregation.find(x => x.value == field)[returntype];
    } else {
      return '';
    }
  }
  deleteMetric(i) {
    this.chatConfig['data']['metrics'].splice(i, 1);
    if (this.chatConfig['data']['metrics'].filter(x => x.isVisible).length == 0) {
      this.chatConfig['data']['metrics'][0]['isVisible'] = true;
    }
    if (this.chartType == 'data_table') {
      this.chatConfig['table_setting']['data_setting']['order'].forEach((element, idx) => {
        if (element.key.type == 'M' && element.key.index == i) {
          this.chatConfig['table_setting']['data_setting']['order'].splice(idx, 1);
        }
      });
    }
  }

  deleteDimension(i) {
    this.chatConfig['data']['buckets'].splice(i, 1);
    this.chatConfig['data']['buckets'].forEach((ele, i) => {
      if (ele.aggregation != '') {
        this.aggregationDimensionChange(i);
      }
    });
    if (this.chatConfig['data']['buckets'].filter(x => x.isVisible).length == 0) {
      this.chatConfig['data']['buckets'][0]['isVisible'] = true;
    }
    if (this.chartType == 'data_table') {
      this.chatConfig['table_setting']['data_setting']['order'].forEach((element, idx) => {
        if (element.key.type == 'D' && element.key.index == i) {
          this.chatConfig['table_setting']['data_setting']['order'].splice(idx, 1);
        }
      });
    }
  }

  changeSliceYAxis() {
    if (this.sliceYAxis == 'y_axis_1') {
      this.chatConfig['data']['metrics'][0]['isSlice'] = true;
      this.chatConfig['data']['metrics'][1]['isSlice'] = false;
    } else {
      this.chatConfig['data']['metrics'][0]['isSlice'] = false;
      this.chatConfig['data']['metrics'][1]['isSlice'] = true;
    }
  }
  changeDateTomoment(value) {
    if (value != '') {
      let splitData = value.split('-');
      if (splitData.length == 2) {
        value = splitData[0] + splitData[1];
      } else if (splitData.length == 3) {
        value = splitData[2] + splitData[0] + splitData[1];
      }
    }
    return value;
  }

  addOrderBy() {
    this.tableOrderClick();
    this.chatConfig['table_setting']['data_setting']['order'].push({ key: { type: 'D', index: 0 }, opcode: 'desc' });
  }

  deleteOrderBy(i) {
    this.chatConfig['table_setting']['data_setting']['order'].splice(i, 1);
  }
  exportTable(fileFormat) {
    this.exportRequest['appName'] = this.appConfig['name'].toString();
    this.exportRequest['sendEmail'] = this.appConfig['user']['contactEmail'].split(',');

    if (this.exportRequest['sendEmail'].length > 0) {
      if (this.chartJson['flatTableData'].length == 0) {
        this.confirmationService.confirm({
          message:
            'Your report is not generated because the data is not available for selected date range. Please select different date range to see the data.',
          header: 'Information',
          icon: 'pi pi-exclamation-triangle',
          accept: () => { },
          reject: () => { },
          key: 'exportDialog'
        });
      } else {
        this.toastService.displayToast({
          severity: 'info',
          summary: 'Export Report',
          detail:
            'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
          life: 10000
        });
        const columnDefs = this.chartJson['flatTableJson']['globalFilterFields'];
        const req = this.libServ.deepCopy(this.apiRequest);
        req['limit'] = '';
        req['offset'] = '';
        req['isTable'] = false;

        const sheetDetails = {};
        sheetDetails['columnDef'] = columnDefs;
        sheetDetails['data'] = [];
        sheetDetails['sheetName'] = 'visualise Data';
        sheetDetails['isRequest'] = true;
        sheetDetails['request'] = {
          url: this.keySpaces['export_url'],
          method: 'POST',
          param: req
        };
        sheetDetails['disclaimer'] = [
          {
            position: 'bottom',
            label: 'Note: Data present in the table may vary over a period of time.',
            color: '#000000'
          }
        ];
        sheetDetails['totalFooter'] = {
          available: false,
          custom: false
        };
        sheetDetails['tableTitle'] = {
          available: false,
          label: this.title
        };
        sheetDetails['image'] = [
          {
            available: true,
            path: environment.exportConfig.exportLogo,
            position: 'top'
          }
        ];
        const sheetDetailsarray = [];
        sheetDetailsarray.push(sheetDetails);
        this.exportRequest['sheetDetails'] = <SheetDetails[]>(
          sheetDetailsarray
        );
        this.exportRequest['fileName'] =
          this.title +
          moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
        this.exportRequest['exportFormat'] = fileFormat;
        this.exportRequest['exportAppName'] = this.keySpaces['name'];
        this.exportRequest['exportConfig'] = environment.exportConfig;
        // return false;
        this.dataFetchServ
          .getExportReportData(this.exportRequest,this.keySpaces['backend_url'])
          .subscribe(response => {
            console.log(response);
          });
      }
    } else {
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Export Report',
        detail:
          'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
        life: 10000
      });
    }
    return false;
  }
  getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }
}
