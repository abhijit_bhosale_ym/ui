import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizeChartPopComponent } from './visualize-chart-pop.component';

describe('VisualizeChartPopComponent', () => {
  let component: VisualizeChartPopComponent;
  let fixture: ComponentFixture<VisualizeChartPopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VisualizeChartPopComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizeChartPopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
