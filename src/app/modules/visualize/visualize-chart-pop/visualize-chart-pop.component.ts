import { Component, OnInit, OnDestroy } from '@angular/core';
import { SelectItemGroup, ConfirmationService } from 'primeng/api';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { FetchApiDataService } from '../fetch-visualize-api-data.service';
import { CommonLibService } from 'src/app/_services';
import { DynamicDialogConfig, DynamicDialogRef, DialogService } from 'primeng/dynamicdialog';
@Component({
  selector: 'ym-visualize-chart-pop',
  templateUrl: './visualize-chart-pop.component.html',
  styleUrls: ['./visualize-chart-pop.component.scss']
})
export class VisualizeChartPopComponent implements OnInit {
  // private appConfigObs: Subscription;

  appConfig: object = {};
  iskeyspace = false;
  RowData: object = {};
  chartType = [{
    chart_type: 'bar',
    display_name: 'Vertical Bar',
    icon_class: 'fas fa-poll',
    info: 'Assign a continous variable to each axis'
  }, {
    chart_type: 'line',
    display_name: 'Line',
    icon_class: 'fas fa-chart-line',
    info: 'Emphasize trends'
  },
  {
    chart_type: 'area',
    display_name: 'Area',
    icon_class: 'fas fa-chart-area',
    info: 'Emphasize the quantity beneath a line chart'
  },
  {
    chart_type: 'pie',
    display_name: 'Pie',
    icon_class: 'fas fa-chart-pie',
    info: 'Compare part of a whole'
  },
  {
    chart_type: 'cards_panel',
    display_name: 'Cards Panel',
    icon_class: 'fas fa-id-card',
    info: 'Display a calculation as a single number'
  }
  ];

  keySpaces = [];
  hoverValue: object = {};

  selectedChartType: string;
  selectedKeyspace: string;

  constructor(
    private formBuilder: FormBuilder,
    public router: Router,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private confirmationService: ConfirmationService,
    private libServ: CommonLibService,
    private config: DynamicDialogConfig,
    private dynamicDialogRef: DynamicDialogRef,
    private currentActivatedRoute: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    this.hoverValue = {
      info: 'Start creating your visualization by selecting a type for that visualization.',
      display_name: 'Select a visualization type'
    };
    // this.chartType = [{
    //   chart_type: 'vertical_bar',
    //   display_name: 'Vertical Bar',
    //   icon_class: 'far fa-chart-bar'
    // }, {
    //   chart_type: 'line',
    //   display_name: 'Line',
    //   icon_class: 'far fa-chart-line'
    // }, {
    //   chart_type: 'pie',
    //   display_name: 'Pie',
    //   icon_class: 'far fa-chart-pie'
    // }];


    // this.loadProperty();
    // this.getPaymentMethod();

  }
  mouseEnter(i) {
    console.log('i...........', i);
    this.hoverValue = this.chartType[i];
  }
  mouseLeave() {
    this.hoverValue = {
      info: 'Start creating your visualization by selecting a type for that visualization.',
      display_name: 'Select a visualization type'
    };
  }

  selectType(data) {

    this.selectedChartType = data['chart_type'];
    this.iskeyspace = true;
    this.dataFetchServ.getKeyspaceData().subscribe(data => {
      this.keySpaces = data as [];

    });
  }
  back() {
    this.iskeyspace = false;
  }
  onSelectionChange(e) {
    console.log('e..........', e)
    this.selectedKeyspace = e['value'][0]['id'];
    // this.router.navigate(['/visualize/create-visualize/' + this.selectedChartType + '/' + this.selectedKeyspace], {
    //   relativeTo: this.currentActivatedRoute
    // });
    localStorage.setItem('chartType', this.selectedChartType);
    localStorage.setItem('keyspaceId', this.selectedKeyspace);
    this.dynamicDialogRef.close(true);
  }
  onSubmit() {
    this.confirmationService.confirm({
      message: 'Are you sure you want to change update status?',
      accept: () => {

      },
      reject: () => {

      }
    });
  }

  goBack() {
    // this.router.navigate(['/users']);
    this.dynamicDialogRef.close(false);
  }
  // ngOnDestroy() {
  //   if (this.appConfigObs && !this.appConfigObs.closed) {
  //     this.appConfigObs.unsubscribe();
  //   }
  // }
}
