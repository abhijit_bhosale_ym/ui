import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectButtonModule } from 'primeng/selectbutton';
import { ButtonModule } from 'primeng/button';
import { TabViewModule } from 'primeng/tabview';
import { TreeTableModule } from 'primeng/treetable';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MultiSelectModule } from 'primeng/multiselect';
import { SharedModule } from 'src/app/_pipes/shared.module';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { FilterContainerModule } from '../common/filter-container/filter-container.module';
import { CardsModule } from '../common/cards/cards.module';
import { ChartsModule } from '../common/charts/charts.module';
import { RouterModule } from '@angular/router';
import { VisualizeAppRoutesAppRoutes } from './visualize-app-routing.module';
import { VisualizeAppComponent } from './visualize-app.component';
import { TableModule } from 'primeng/table';
import { ConfirmDialogModule, FileUploadModule, ColorPickerModule, OrderListModule, RadioButtonModule } from 'primeng';
import { DataViewModule } from 'primeng/dataview';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { VisualizeCreateComponent } from './visualize-create/visualize-create.component';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { DropdownModule } from 'primeng/dropdown';
import { InputSwitchModule } from 'primeng/inputswitch';
import { CheckboxModule } from 'primeng/checkbox';
@NgModule({
  declarations: [VisualizeAppComponent, VisualizeCreateComponent],
  imports: [
    CommonModule,
    SelectButtonModule,
    ButtonModule,
    TabViewModule,
    TreeTableModule,
    FormsModule,
    ReactiveFormsModule,
    MultiSelectModule,
    SharedModule,
    FilterContainerModule,
    CardsModule,
    ChartsModule,
    TableModule,
    ConfirmDialogModule,
    FileUploadModule,
    DataViewModule,
    ScrollPanelModule,
    ToggleButtonModule,
    DropdownModule,
    InputSwitchModule,
    ColorPickerModule,
    OrderListModule,
    RadioButtonModule,
    CheckboxModule,
    RouterModule.forChild(VisualizeAppRoutesAppRoutes)
  ],
  providers: [FormatNumPipe]
})
export class VisualizeAppRoutesAppModule { }
