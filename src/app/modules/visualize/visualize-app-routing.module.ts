import { Routes } from '@angular/router';
import { VisualizeAppComponent } from './visualize-app.component';
import { VisualizeCreateComponent } from './visualize-create/visualize-create.component';

export const VisualizeAppRoutesAppRoutes: Routes = [
  {
    path: '',
    component: VisualizeAppComponent
  },
  {
    path: 'create-visualize/:chartType/:keyspace',
    component: VisualizeCreateComponent
  }
];
