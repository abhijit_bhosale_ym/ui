import { Routes } from '@angular/router';

import { PrebidAppComponent } from './prebid-app.component';
export const PrebidAppRoutes: Routes = [
  {
    path: '',
    component: PrebidAppComponent
    // data: {
    //   breadcrumb: 'Yield Management'
    // }
  }
];
