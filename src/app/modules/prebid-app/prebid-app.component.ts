import { Component, OnInit, OnDestroy } from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { FetchApiDataService } from './fetch-api-data.service';
import { TreeNode } from 'primeng/api';
import { ToastService } from '../../_services/toast-notification/toast.service';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import * as moment from 'moment';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { Title } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { DialogService } from 'primeng';
import { DailyDataPopupComponent } from './daily-data-popup/daily-data-popup.component';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-prebid-app',
  templateUrl: './prebid-app.component.html',
  styleUrls: ['./prebid-app.component.css']
})
export class PrebidAppComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;

  appConfig: object = {};
  cardsJson = [];
  showCards = false;

  showMainLineChart = false;
  noDataMainLineChart = false;
  noDataMetricChart = false;
  mainLineChartJson: object;
  defaultChartsJson: object;
  metricChartJson: object;
  pagination = false;
  chartsMultiselectButton: object = {
    data: [
      {
        label: 'Revenue',
        value: 'revenue'
      },
      {
        label: 'eCPM',
        value: 'ecpm'
      },
      {
        label: 'Fill Rate',
        value: 'fillrate'
      },
      {
        label: 'Impressions',
        value: 'impressions'
      }
    ],
    model: ['revenue']
  };

  lastUpdatedOn: Date;
  nextUpdated: Date;
  dataUpdatedThrough: Date;
  dimensionsDropdown: any[];
  selectedDimension: any[];
  metricsDropdown: any[];
  selectedMetric: any[];
  showMetricChart = false;
  selectedTabIndex = 0;
  flatTableReq: object;
  flatTableData: TreeNode[];
  flatTableColumnDef: any[];
  flatTableJson: Object;
  isExportReport = false;
  filtersApplied: object = {};
  selectedDateRange: String;
  exportRequest: ExportRequest = <ExportRequest>{};

  appFeaturesConfigs = {};
  mainCardsConfig = {};

  constructor(
    private appConfigService: AppConfigService,
    private _titleService: Title,
    private libServ: CommonLibService,
    private dialogService: DialogService,
    private dataFetchServ: FetchApiDataService,
    private toastService: ToastService,
    private htmltoimage: HtmltoimageService,
    private pptExport: ExportPptService,
    private exportService: ExportdataService,
    private formatNumPipe: FormatNumPipe
  ) { }

  ngOnInit() {
    this.defaultChartsJson = {
      chartTypes: [
        { key: 'line', label: 'Line Chart' }
      ],
      chartData: {
        labels: [],
        datasets: []
      },
      chartOptions: {
        title: {
          display: true,
          text: ''
        },
        legend: {
          display: false
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              display: true,
              scaleLabel: {
                display: true
              }
            }
          ]
        },
        pan: {
          enabled: true,
          mode: 'x'
        },
        zoom: {
          enabled: true,
          mode: 'x'
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: true,
      chartWidth: '',
      chartHeight: '300px'
    };
    this.dimensionsDropdown = [
      {
        field: 'bidder',
        displayName: 'Bidder'
      },
      {
        field: 'publisher_name',
        displayName: 'Publisher'
      },

      {
        field: 'ad_unit',
        displayName: 'AdUnit'
      },
      {
        field: 'deal_id',
        displayName: 'Deal'
      },
      {
        field: 'media_type',
        displayName: 'Media Type'
      },
      {
        field: 'sizes',
        displayName: 'Sizes'
      },
      {
        field: 'bid_landscape',
        displayName: 'Bid Landscape'
      },
      {
        field: 'currency',
        displayName: 'Currency'
      },
      {
        field: 'device_os_name',
        displayName: 'OS'
      },
      {
        field: 'device_type',
        displayName: 'Device Type'
      },
      {
        field: 'browser_name',
        displayName: 'Browser'
      },
      {
        field: 'country_name',
        displayName: 'Country'
      },
      {
        field: 'region_name',
        displayName: 'Region'
      },
      {
        field: 'host',
        displayName: 'Host/Site'
      },
      {
        field: 'language',
        displayName: 'Language'
      },
      {
        field: 'path',
        displayName: 'Entry Page'
      },
      {
        field: 'referer_domain',
        displayName: 'Referer Domain'
      },
      {
        field: 'referrer_type',
        displayName: 'Referrer Type'
      },
      {
        field: 'referrer',
        displayName: 'Referrer'
      },
      {
        field: 'utm_campaign',
        displayName: 'UTM Campaign'
      },
      {
        field: 'utm_content',
        displayName: 'UTM Content'
      },
      {
        field: 'utm_medium',
        displayName: 'UTM Medium'
      },
      {
        field: 'utm_source',
        displayName: 'UTM Source'
      },
      {
        field: 'utm_term',
        displayName: 'UTM Term'
      },
      {
        field: 'prebid_version',
        displayName: 'Prebid Version'
      },
      {
        field: 'yuktamedia_analytics_version',
        displayName: 'Analytics Version'
      }
    ];
    this.selectedDimension = this.dimensionsDropdown[0];

    this.metricsDropdown = [
      {
        field: 'requests',
        displayName: 'Bid Requests',
        derived: false
      },
      {
        field: 'bids_count',
        displayName: 'Bid Responses',
        derived: false
      },
      {
        field: 'impressions',
        displayName: 'Paid Impressions',
        derived: false
      },
      {
        field: 'bid_rate',
        displayName: 'Bid Response Rate',
        derived: true,
        metrics: []
      },
      {
        field: 'win_rate',
        displayName: 'Bid Win Rate',
        derived: true,
        metrics: []
      },
      {
        field: 'fill_rate',
        displayName: 'Fill Rate',
        derived: true,
        metrics: []
      },
      {
        field: 'cpm',
        displayName: 'eCPM',
        derived: true,
        metrics: []
      },
      {
        field: 'revenue',
        displayName: 'Revenue',
        derived: false
      },
      {
        field: 'timeout_rate',
        displayName: 'Timeout Rate',
        derived: true,
        metrics: []
      },
      {
        field: 'won_rpm',
        displayName: 'Prebid Won RPM',
        derived: true,
        metrics: ['impressions', 'revenue']
      },
      {
        field: 'session_rpm',
        displayName: 'Prebid Session RPM',
        derived: true,
        metrics: []
      },
      {
        field: 'won_session_rpm',
        displayName: 'Prebid Won Session RPM',
        derived: true,
        metrics: []
      },
      {
        field: 'page_view_rpm',
        displayName: 'Prebid Page View RPM',
        derived: true,
        metrics: []
      },
      {
        field: 'ads_per_page_view',
        displayName: 'Ads Per Page View',
        derived: true,
        metrics: []
      },
      {
        field: 'page_views_per_session',
        displayName: 'Page View Per Session',
        derived: true,
        metrics: []
      }
    ];
    this.selectedMetric = this.metricsDropdown[0];

    /* ------------------------------- Table Start ------------------------------ */

    this.flatTableColumnDef = [
      {
        field: 'bidder',
        displayName: 'Bidder',
        format: '',
        width: '250',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'requests',
        displayName: 'Bid Request',
        format: 'number',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        width: '150',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'bids_count',
        displayName: 'Bid Response',
        format: 'number',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        width: '150',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'impressions',
        displayName: 'Paid Impressions',
        format: 'number',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        width: '170',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'pageviews',
        displayName: 'Page Views',
        format: 'number',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        width: '130',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'sessions',
        displayName: 'Sessions',
        format: 'number',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        width: '120',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'bid_rate',
        displayName: 'Bid Response Rate',
        format: 'percentage',
        width: '135',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'win_rate',
        displayName: 'Bid Win Rate',
        format: 'percentage',
        width: '110',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'fill_rate',
        displayName: 'Fill Rate',
        format: 'percentage',
        width: '110',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'revenue',
        displayName: 'Revenue',
        format: '$',
        width: '140',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'cpm',
        displayName: 'eCPM',
        format: '$',
        width: '110',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'timeout_rate',
        displayName: 'Timeout Rate',
        format: 'percentage',
        width: '155',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'won_rpm',
        displayName: 'Prebid Won RPM',
        format: '$',
        width: '115',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'session_rpm',
        displayName: 'Prebid Session RPM',
        format: '$',
        width: '140',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'won_session_rpm',
        displayName: 'Prebid Won Session RPM',
        format: '$',
        width: '155',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'page_view_rpm',
        displayName: 'Prebid Page View RPM',
        format: '$',
        width: '125',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'ads_per_page_view',
        displayName: 'Ads Per Page View',
        format: 'number',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        width: '125',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'page_views_per_session',
        displayName: 'Page View Per Session',
        format: 'number',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        width: '175',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      }
    ];
    this.flatTableJson = {
      page_size: 10,
      lazy: false,
      loading: true,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '300px',
      totalRecords: 1000,
      columns: this.flatTableColumnDef.slice(1),
      selectedColumns: this.flatTableColumnDef.slice(1),
      globalFilterFields: this.flatTableColumnDef.slice(0),
      frozenCols: [this.flatTableColumnDef[0]],
      // frozenWidth: '250px',
      frozenWidth:
        this.flatTableColumnDef
          .slice(0, 1)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: false,
      overallSearch: false,
      columnSearch: false
    };

    /* -------------------------------- Table End ------------------------------- */

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        console.log('appConfig', this.appConfig);
        this.isExportReport = this.appConfig['permissions'].some(
          o => o.name === 'prebid-export-reports'
        );
        this._titleService.setTitle(this.appConfig['displayName']);
        let startDate;
        let endDate;
        const date_config = this.appConfig['filter']['filterConfig']['filters'][
          'datePeriod'
        ][0];
        if ((date_config['defaultDate'][0]['value'] === 0 && date_config['defaultDate'][0]['period'] === 'month') && (moment().format('DD') === '01' || moment().format('DD') === '02')) {
          startDate = moment().subtract(1, 'months').startOf('month');
          endDate = moment().subtract(1, 'months').endOf('month');
        } else {
          if (
            date_config[
            'defaultDate'
            ][0]['startOf']
          ) {
            startDate = moment()
              .subtract(
                date_config['defaultDate'][0]['value'],
                date_config['defaultDate'][0]['period']
              )
              .startOf(
                date_config['defaultDate'][0]['period']
              );
          } else {
            startDate = moment().subtract(
              date_config['defaultDate'][0]['value'],
              date_config['defaultDate'][0]['period']
            );
          }
          endDate = moment().subtract(
            date_config[
            'defaultDate'
            ][1]['value'],
            date_config[
            'defaultDate'
            ][1]['period']
          );
        }

        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: startDate.format('YYYYMMDD'),
            time_key2: endDate.format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: this.appConfig['filter']['filterConfig']['groupBy'].filter(
            v => v.selected
          )
        };

        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        this.initialLoading();
      }
    });
  }

  /* ------------------------------- Table Start ------------------------------ */

  initialLoading() {
    this.dataFetchServ
      .getLastUpdatedData(this.appConfig['id'])
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        this.lastUpdatedOn = moment(data[0]['updated_at']).toDate();
        this.nextUpdated = moment(data[0]['next_run_at']).toDate();
        this.dataUpdatedThrough = moment(
          data[0]['source_updated_through'],
          'YYYYMMDD'
        ).toDate();
      });

    this.libServ.getAppFeaturesConfigs(this.appConfig['id']).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: data['message']
        });
        console.log(data['message']);
        return;
      }
      this.appFeaturesConfigs = data['data'];
      this.appFeaturesConfigs['cardsConfig']['cards'].forEach(element => {
        if (element['name'] === 'main') {
          this.mainCardsConfig = element['config'];
          this.cardsJson = this.mainCardsConfig['list'];
          if (this.mainCardsConfig['display']) {
            this.loadCards();
          }
        }
      });
    });

    this.selectedDateRange = `${moment(
      this.filtersApplied['timeKeyFilter']['time_key1'],
      'YYYYMMDD'
    ).format('MM-DD-YYYY')} - ${moment(
      this.filtersApplied['timeKeyFilter']['time_key2'],
      'YYYYMMDD'
    ).format('MM-DD-YYYY')}`;
    const mainLineChartReq = {
      dimensions: ['time_key'],
      metrics: ['revenue'],
      derived_metrics: ['cpm'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.filtersApplied['filters'],
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: [
        'bidder'
      ],
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: '',
      isCount: false
    };
    this.loadMainLineChart(mainLineChartReq);

    this.flatTableReq = {
      dimensions: ['bidder'],
      metrics: [
        'requests',
        'impressions',
        'revenue',
        'bids_count',
        'timeout_count'
      ],
      derived_metrics: [
        'bid_rate',
        'win_rate',
        'cpm',
        'timeout_rate',
        'fill_rate',
        'won_rpm',
        'session_rpm',
        'won_session_rpm',
        'page_view_rpm',
        'ads_per_page_view',
        'sessions',
        'page_views_per_session',
        'pageviews'
      ],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.filtersApplied['filters'],
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: [
        'bidder'
      ],
      orderBy: [{ key: 'bidder', opcode: 'asc' }],
      limit: 10,
      offset: 0,
      isCount: true
    };
    this.loadFlatTableData(this.flatTableReq);
    setTimeout(() => {
      this.flatTableJson['lazy'] = true;
    }, 0);
  }

  loadMainLineChart(params) {
    const colors = this.libServ.dynamicColors(2);
    this.mainLineChartJson = {
      chartTypes: [{ key: 'bar', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'eCPM',
            type: 'line',
            yAxisID: 'y-axis-0',

            borderColor: colors[0],
            fill: false,
            backgroundColor: colors[0],
            data: []
          },
          {
            label: 'Revenue',
            type: 'bar',
            yAxisID: 'y-axis-1',

            borderColor: colors[1],
            fill: false,
            backgroundColor: colors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Revenue vs eCPM'
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: 'eCPM ($)'
              },
              position: 'right',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', []);
                }
              }
            },
            {
              id: 'y-axis-1',
              scaleLabel: {
                display: true,
                labelString: 'Revenue ($)'
              },
              position: 'left',
              name: '2',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', []);
                }
              }
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    this.showMainLineChart = false;
    this.dataFetchServ.getTableData(params).subscribe(data => {
      const chartData = data as {};
      this.noDataMainLineChart = false;
      if (!chartData['data'].length) {
        this.noDataMainLineChart = true;
        return;
      }
      const datesArr = Array.from(
        new Set(chartData['data'].map(r => r['time_key']))
      );

      this.mainLineChartJson['chartData']['labels'] = datesArr.map(d =>
        moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
      );

      const revArr = [];
      const ecpmArr = [];
      datesArr.forEach(time_key => {
        chartData['data'].forEach(r => {
          if (r['time_key'] === time_key) {
            revArr.push(r['revenue']);
            ecpmArr.push(r['cpm']);
          }
        });
      });
      this.mainLineChartJson['chartData']['datasets'][0]['data'] = ecpmArr;
      this.mainLineChartJson['chartData']['datasets'][1]['data'] = revArr;
      this.showMainLineChart = true;
    });
  }

  loadCards() {
    this.showCards = false;
    const apiUrl = this.mainCardsConfig['api']['url'];
    const cardsReq = this.libServ.deepCopy(
      this.mainCardsConfig['api']['request']
    );
    cardsReq['timeKeyFilter'] = this.libServ.deepCopy(
      this.filtersApplied['timeKeyFilter']
    );

    cardsReq['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);
    const selDimention = this.selectedDimension['field'];
    const filtersArr = this.filtersApplied['filters']['dimensions'].map(x => x.key);
    const groupBy = Array.from(
      new Set([selDimention].concat(filtersArr))
    ).sort();
    cardsReq['gidGroupBy'] = groupBy;
    console.log(cardsReq, this.filtersApplied);

    this.libServ.getDataFromDynamicAPI(apiUrl, cardsReq).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error - Contact to administator',
          detail: data
        });
        console.log(data['status_msg']);
        return;
      }
      if (!this.libServ.isEmptyObj(data['data'])) {
        this.getLastSevenDaysAverage(cardsReq, data['data'][0]);
        this.cardsJson.map(o => {
          o['value'] = data['data'][0][o['field']];
        });
      } else {
        this.cardsJson.map(o => {
          o['value'] = 0;
        });
      }
      this.showCards = this.mainCardsConfig['display'];
    });
  }

  getLastSevenDaysAverage(params, currentData) {
    const totalDataDateDiff = moment(params['timeKeyFilter']['time_key2'], 'YYYYMMDD').diff(
      moment(params['timeKeyFilter']['time_key1'], 'YYYYMMDD'),
      'days'
    );
    // const totalDataDateDiff = parseInt(params['timeKeyFilter']['time_key2'], 10) - parseInt(params['timeKeyFilter']['time_key1'], 10);
    const time_key1 = moment(params['timeKeyFilter']['time_key1'], 'YYYYMMDD')
      .subtract('days', 1)
      .format('YYYYMMDD');
    const time_key2 = moment(time_key1, 'YYYYMMDD')
      .subtract('days', 6)
      .format('YYYYMMDD');
    params['timeKeyFilter'] = { 'time_key1': time_key2, 'time_key2': time_key1 };

    this.dataFetchServ.getTableData(params).subscribe(data => {
      if (data['data'].length) {
        const last7Days = data['data'][0];
        if (!this.libServ.isEmptyObj(last7Days)) {
          this.cardsJson.map(o => {
            const last7DaysAvg = parseFloat(last7Days[o['field']]) / 7 || 0;
            const totalAvg = parseFloat(currentData[o['field']]) / (totalDataDateDiff + 1) || 0;
            o['metaConfig']['avgValue'] = ((totalAvg - last7DaysAvg) / totalAvg * 100).toFixed(2);
            o['metaConfig']['icon'] = totalAvg >= last7DaysAvg ? 'fas fa-arrow-up green' : 'fas fa-arrow-down red';
            o['metaConfig']['dateRange'] = `${moment(params['timeKeyFilter']['time_key1'], 'YYYYMMDD').format('MMM-DD-YYYY')} - ${moment(params['timeKeyFilter']['time_key2'], 'YYYYMMDD').format('MMM-DD-YYYY')}`;
          });
        }
      } else {
        this.cardsJson.map(o => {
          o['metaConfig']['avgValue'] = 'N/A';
          o['metaConfig']['icon'] = 'fas fa-minus';
          o['metaConfig']['dateRange'] = `${moment(params['timeKeyFilter']['time_key1'], 'YYYYMMDD').format('MMM-DD-YYYY')} - ${moment(params['timeKeyFilter']['time_key2'], 'YYYYMMDD').format('MMM-DD-YYYY')}`;
        });
      }
    });
  }

  resetPagination(tt1) {
    if (typeof tt1 !== 'undefined') {
      tt1.reset();
    }
  }

  onLazyLoadFlatTable(e: Event) {
    // console.log('Lazy', e);
    if (typeof this.flatTableReq !== 'undefined') {
      let orderby = [];
      if (
        typeof e['multiSortMeta'] !== 'undefined' &&
        e['multiSortMeta'] !== null
      ) {
        e['multiSortMeta'].forEach(sort => {
          orderby.push({
            key: sort['field'],
            opcode: sort['order'] === -1 ? 'desc' : 'asc'
          });
        });
      } else {
        orderby = [];
      }
      this.flatTableReq['limit'] = e['rows'];
      this.flatTableReq['offset'] = e['first'];
      this.flatTableReq['orderBy'] = orderby;
      this.loadFlatTableData(this.flatTableReq);
    }
  }

  loadFlatTableData(params: Object) {
    this.flatTableJson['loading'] = true;
    this.flatTableJson['columns'] = this.flatTableColumnDef.slice(1);
    this.flatTableJson['selectedColumns'] = this.flatTableColumnDef.slice(1);
    this.flatTableJson['frozenCols'] = [this.flatTableColumnDef[0]];
    this.dataFetchServ.getTableData(params).subscribe(data => {
      const arr = [];
      const resData = data as {};
      for (const r of resData['data']) {
        const obj = {};
        // obj['children'] = [{}];
        obj['data'] = r;
        arr.push(obj);
      }
      this.flatTableData = <TreeNode[]>arr;
      this.pagination = true;
      this.flatTableJson['totalRecords'] = resData['totalItems'];
      this.flatTableJson['loading'] = false;
    });
  }

  isHiddenColumn(col: Object) {
    return (
      this.flatTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.flatTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }
  /* -------------------------------- Table End ------------------------------- */

  /* ------------------------------- Load Charts ------------------------------ */

  chartSelected(data: Event) {
    // console.log('Chart Selected', data);
  }

  isChartSelected(chartName) {
    return this.chartsMultiselectButton['model'].indexOf(chartName) !== -1;
  }

  /* --------------------- On Filter Apply Button Clicked --------------------- */

  onFiltersApplied(filterData: object) {
    this.selectedTabIndex = 0;
    const dim = {};
    this.filtersApplied['filters']['dimensions'] = [];
    for (const k in filterData['filter']['dimensions']) {
      if (filterData['filter']['dimensions'][k].length !== 0) {
        this.filtersApplied['filters']['dimensions'].push({
          key: k,
          values: filterData['filter']['dimensions'][k]
        });
      }
    }

    const selDimention = this.selectedDimension['field'];
    const filtersArr = this.filtersApplied['filters']['dimensions'].map(x => x.key);
    const groupBy = Array.from(
      new Set([selDimention].concat(filtersArr))
    ).sort();
    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];


    this.selectedDateRange = `${moment(
      this.filtersApplied['timeKeyFilter']['time_key1'],
      'YYYYMMDD'
    ).format('MM-DD-YYYY')} - ${moment(
      this.filtersApplied['timeKeyFilter']['time_key2'],
      'YYYYMMDD'
    ).format('MM-DD-YYYY')}`;

    if (this.mainCardsConfig['display']) {
      this.mainCardsConfig['api']['request']['gidGroupBy'] = groupBy;
      this.loadCards();
    }

    this.flatTableReq = {
      dimensions: [this.selectedDimension['field']],
      metrics: [
        'requests',
        'impressions',
        'revenue',
        'bids_count',
        'timeout_count'
      ],
      derived_metrics: [
        'bid_rate',
        'win_rate',
        'cpm',
        'timeout_rate',
        'fill_rate',
        'won_rpm',
        'session_rpm',
        'won_session_rpm',
        'page_view_rpm',
        'ads_per_page_view',
        'sessions',
        'page_views_per_session',
        'pageviews'
      ],
      timeKeyFilter: {
        time_key1: filterData['date'][0],
        time_key2: filterData['date'][1]
      },
      filters: this.filtersApplied['filters'],
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: groupBy,
      orderBy: [{ key: this.selectedDimension['field'], opcode: 'asc' }],
      limit: 10,
      offset: 0,
      isCount: true
    };
    this.loadFlatTableData(this.flatTableReq);
    const mainLineChartReq = {
      dimensions: ['time_key'],
      metrics: ['revenue'],
      derived_metrics: ['cpm'],
      timeKeyFilter: {
        time_key1: filterData['date'][0],
        time_key2: filterData['date'][1]
      },
      filters: this.filtersApplied['filters'],
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: groupBy,
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: '',
      isCount: false
    };
    this.loadMainLineChart(mainLineChartReq);
  }

  dimChanged() {
    if (this.selectedTabIndex === 0) {
      const cols = [
        {
          field: 'publisher_name',
          displayName: 'Publisher',
          format: '',
          width: '250',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
        },
        {
          field: 'bidder',
          displayName: 'Bidder',
          format: '',
          width: '250',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
        },
        {
          field: 'ad_unit',
          displayName: 'AdUnit',
          format: '',
          width: '250',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
        },
        {
          field: 'deal_id',
          displayName: 'Deal',
          format: '',
          width: '250',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
        },
        {
          field: 'media_type',
          displayName: 'Media Type',
          format: '',
          width: '250',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
        },
        {
          field: 'sizes',
          displayName: 'Sizes',
          format: '',
          width: '250',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
        },
        {
          field: 'bid_landscape',
          displayName: 'Bid Landscape',
          format: '',
          width: '250',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
        },
        {
          field: 'currency',
          displayName: 'Currency',
          format: '',
          width: '250',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
        },
        {
          field: 'device_os_name',
          displayName: 'OS',
          format: '',
          width: '250',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
        },
        {
          field: 'device_type',
          displayName: 'Device Type',
          format: '',
          width: '250',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
        },
        {
          field: 'browser_name',
          displayName: 'Browser',
          format: '',
          width: '250',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
        },
        {
          field: 'country_name',
          displayName: 'Country',
          format: '',
          width: '250',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
        },
        {
          field: 'region_name',
          displayName: 'Region',
          format: '',
          width: '250',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
        },
        {
          field: 'host',
          displayName: 'Host/Site',
          format: '',
          width: '250',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
        },
        {
          field: 'language',
          displayName: 'Language',
          format: '',
          width: '250',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
        },
        {
          field: 'path',
          displayName: 'Entry Page',
          format: '',
          width: '250',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
        },
        {
          field: 'referer_domain',
          displayName: 'Referer Domain',
          format: '',
          width: '250',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
        },
        {
          field: 'referrer_type',
          displayName: 'Referrer Type',
          format: '',
          width: '250',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
        },
        {
          field: 'referrer',
          displayName: 'Referrer',
          format: '',
          width: '250',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
        },
        {
          field: 'utm_campaign',
          displayName: 'UTM Campaign',
          format: '',
          width: '250',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
        },
        {
          field: 'utm_content',
          displayName: 'UTM Content',
          format: '',
          width: '250',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
        },
        {
          field: 'utm_medium',
          displayName: 'UTM Medium',
          format: '',
          width: '250',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
        },
        {
          field: 'utm_source',
          displayName: 'UTM Source',
          format: '',
          width: '250',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
        },
        {
          field: 'utm_term',
          displayName: 'UTM Term',
          format: '',
          width: '250',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
        },
        {
          field: 'prebid_version',
          displayName: 'Prebid Version',
          format: '',
          width: '250',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
        },
        {
          field: 'yuktamedia_analytics_version',
          displayName: 'Analytics Version',
          format: '',
          width: '250',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
        }
      ];

      this.flatTableColumnDef[0] = cols.find(e => {
        return e.field === this.selectedDimension['field'];
      });
      const selDimention = this.selectedDimension['field'];
      this.flatTableReq['dimensions'] = [selDimention];
      this.flatTableReq['orderBy'] = [{ key: selDimention, opcode: 'asc' }];
      const filtersArr = this.filtersApplied['filters']['dimensions'].map(x => x.key);
      this.flatTableReq['gidGroupBy'] = Array.from(
        new Set([selDimention].concat(filtersArr))
      ).sort();
      this.loadFlatTableData(this.flatTableReq);
    } else {
      this.loadMetricChart();
    }
  }

  metricChanged() {
    this.loadMetricChart();
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['displayName']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    } else if (type === 'ppt') {
      const chartElements = document.getElementsByTagName('ym-charts');
      const slides = [];
      [].forEach.call(chartElements, (el, index) => {
        const c = el.getElementsByTagName('canvas')[0];

        slides.push({
          title: `${this.appConfig['displayName']}`,
          base64data: this.libServ.canvasToImage('white', c),
          width: 8,
          height: 3
        });

        if (index === chartElements.length - 1) {
          const pptConfig = {
            filename: 'Charts',
            logo: {
              visible: true,
              path: '/assets/images/ym.png',
              width: 0.53125,
              height: 0.458333
            },
            slides: slides
          };
          this.pptExport.exportPPT(pptConfig);
        }
      });
    }
    return false;
  }

  tabChanged(e) {
    this.selectedTabIndex = e.index;
    if (this.selectedTabIndex === 0) {
      this.dimChanged();
    } else {
      this.loadMetricChart();
    }
  }

  loadMetricChart() {
    this.metricChartJson = this.libServ.deepCopy(this.defaultChartsJson);
    const selDimention = this.selectedDimension['field'];
    const filtersArr = this.filtersApplied['filters']['dimensions'].map(x => x.key);

    const req = {
      dimensions: ['time_key', selDimention],
      metrics: [],
      derived_metrics: [],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.filtersApplied['filters'],
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: Array.from(new Set([selDimention].concat(filtersArr))).sort(),
      orderBy: [{ key: this.selectedDimension['field'], opcode: 'asc' }],
      limit: '',
      offset: '',
      isCount: false
    };

    if (this.selectedMetric['derived']) {
      req['derived_metrics'] = [this.selectedMetric['field']];
      // req['metrics'] = this.selectedMetric['metrics'];
    } else {
      req['metrics'] = [this.selectedMetric['field']];
    }

    this.metricChartJson['chartOptions']['title'] = {
      display: true,
      text: `${this.selectedMetric['displayName']} Trends By ${this.selectedDimension['displayName']}`
    };

    this.metricChartJson['chartOptions']['scales'] = {
      xAxes: [
        {
          display: true,
          scaleLabel: {
            display: true,
            labelString: 'Date'
          }
        }
      ],
      yAxes: [
        {
          display: true,
          scaleLabel: {
            display: true,
            labelString: this.selectedMetric['displayName']
          },
          ticks: {
            callback: (label, index, labels) => {
              const field = this.flatTableColumnDef.find((item) => item.field == this.selectedMetric['field']);
              const value = this.formatNumPipe.transform(label, field.format, field.formatConfig);
              return value;
            }
          }
        }
      ]
    };

    this.metricChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          const label = data.datasets[tooltipItem.datasetIndex]['label'];
          const field = this.flatTableColumnDef.find((item) => item.field == this.selectedMetric['field']);
          const value = this.formatNumPipe.transform(currentValue, field.format, field.formatConfig);
          return `${label}: ${value}`;
        }
      }
    };

    this.metricChartJson['chartOptions']['legend'] = {
      display: true
    };

    this.showMetricChart = false;
    this.dataFetchServ.getTableData(req).subscribe(data => {
      const resData = data as {};
      const chartData = resData['data'];
      this.noDataMetricChart = false;
      if (!chartData.length) {
        this.noDataMetricChart = true;
        return;
      }
      const datesArr = Array.from(
        new Set(chartData.map(r => r['time_key']))
      ).sort();

      const dims = Array.from(
        new Set(chartData.map(s => s[this.selectedDimension['field']]))
      );
      const colors = this.libServ.dynamicColors(dims.length);
      this.metricChartJson['chartData']['labels'] = datesArr.map(d =>
        moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
      );

      dims.forEach((src, i) => {
        const revArr = [];
        datesArr.forEach(time_key => {
          chartData.forEach(r => {
            if (
              r[this.selectedDimension['field']] === src &&
              r['time_key'] === time_key
            ) {
              revArr.push(r[this.selectedMetric['field']]);
            }
          });
        });
        this.metricChartJson['chartData']['datasets'].push({
          label: src,
          data: revArr,
          borderColor: colors[i],
          fill: false,
          backgroundColor: colors[i]
        });
      });
      this.showMetricChart = true;
    });
  }

  openPopup(row, field) {
    const req = this.libServ.deepCopy(this.flatTableReq);
    req['dimensions'] = ['time_key'].concat(req['dimensions']);
    req['groupByTimeKey'] = {
      key: ['accounting_key', 'time_key'],
      interval: 'daily'
    };
    let isFieldPresent = false;
    const filterDim = req['filters']['dimensions'].map(filter => {
      if (filter.key === field) {
        filter.values = [row[field]];
        isFieldPresent = true;
      }
      return filter;
    });
    if (!isFieldPresent) { filterDim.push({ key: field, values: [row[field]] }) }
    req['filters']['dimensions'] = filterDim;
    req['orderBy'].unshift({ key: 'time_key', opcode: 'asc' });
    req['limit'] = '';
    req['offset'] = '';
    req['isTable'] = false;
    const columnDefs = this.libServ.deepCopy(this.flatTableColumnDef);
    columnDefs.splice(0, 0, {
      field: 'time_key',
      displayName: 'Date',
      format: 'date',
      width: '250',
      exportConfig: {
        format: 'date',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: false
      }
    });
    const data = {
      columnDef: columnDefs,
      field: row[field],
      request: req,
      appName: this.exportRequest['appName'],
      email: this.exportRequest['sendEmail']
    };
    const ref = this.dialogService.open(
      DailyDataPopupComponent,
      {
        header: row[field] + ' Daily Distribution',
        contentStyle: { width: '80vw', overflow: 'auto' },
        data: data
      }
    );
    ref.onClose.subscribe((data: string) => { });
  }

  exportTable(table, fileFormat) {
    if (this.exportRequest['sendEmail'].length > 0) {
      this.toastService.displayToast({
        severity: 'info',
        summary: 'Export Report',
        detail:
          'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
        life: 10000
      });
      if (table === 'flat') {
        const columnDefs = this.libServ.deepCopy(this.flatTableColumnDef);
        columnDefs.splice(0, 0, {
          field: 'time_key',
          displayName: 'Date',
          format: 'date',
          width: '250',
          exportConfig: {
            format: 'date',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
        });
        const req = this.libServ.deepCopy(this.flatTableReq);
        req['dimensions'] = ['time_key'].concat(req['dimensions']);
        req['groupByTimeKey'] = {
          key: ['accounting_key', 'time_key'],
          interval: 'daily'
        };
        req['orderBy'].unshift({ key: 'time_key', opcode: 'asc' });
        req['limit'] = '';
        req['offset'] = '';
        req['isTable'] = false;
        this.dataFetchServ.getTableData(req).subscribe(data => {
          const arr = [];
          const resData = data as {};

          const sheetDetails = {};
          sheetDetails['columnDef'] = columnDefs;
          sheetDetails['data'] = resData['data'];
          sheetDetails['sheetName'] = 'Prebid Data';
          sheetDetails['isRequest'] = false;
          sheetDetails['request'] = {
            url: '/api/flask/prebid/cassandradata',
            method: 'POST',
            param: req
          };
          sheetDetails['disclaimer'] = [
            {
              position: 'top',
              label:
                'Data persent in below table is not final and may varies over period of time',
              color: '#3A37CF'
            },
            {
              position: 'bottom',
              label: 'Thank You',
              color: '#EC6A15'
            }
          ];
          sheetDetails['totalFooter'] = {
            available: false,
            custom: false
          };
          sheetDetails['tableTitle'] = {
            available: true,
            label: 'Prebid Data'
          };
          sheetDetails['image'] = [
            {
              available: false,
              path: environment.exportConfig.exportLogo,
              position: 'top'
            }
          ];
          const sheetDetailsarray = [];
          sheetDetailsarray.push(sheetDetails);
          this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
          this.exportRequest['fileName'] =
            'Prebid Analytics Report ' +
            moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
          this.exportRequest['exportFormat'] = fileFormat;
          this.exportRequest['exportConfig'] = environment.exportConfig;
          // console.log('exportreport', this.exportRequest);
          this.dataFetchServ
            .getExportReportData(this.exportRequest)
            .subscribe(response => {
              console.log(response);
            });
        });
      } else if (table === 'agg') {
        const columnDefs = this.libServ.deepCopy(this.flatTableColumnDef);
        const req = this.libServ.deepCopy(this.flatTableReq);
        req['isTable'] = false;
        this.dataFetchServ.getTableData(req).subscribe(data => {
          const arr = [];
          const resData = data as {};

          const sheetDetails = {};
          sheetDetails['columnDef'] = columnDefs;
          sheetDetails['data'] = resData['data'];
          sheetDetails['sheetName'] = 'Prebid Data';
          sheetDetails['isRequest'] = false;
          sheetDetails['request'] = {
            url: '/api/flask/prebid/cassandradata',
            method: 'POST',
            param: req
          };
          sheetDetails['disclaimer'] = [
            {
              position: 'top',
              label:
                'Data persent in below table is not final and may varies over period of time',
              color: '#3A37CF'
            },
            {
              position: 'bottom',
              label: 'Thank You',
              color: '#EC6A15'
            }
          ];
          sheetDetails['totalFooter'] = {
            available: false,
            custom: false
          };
          sheetDetails['tableTitle'] = {
            available: true,
            label: 'Prebid Data'
          };
          sheetDetails['image'] = [
            {
              available: false,
              path: environment.exportConfig.exportLogo,
              position: 'top'
            }
          ];
          const sheetDetailsarray = [];
          sheetDetailsarray.push(sheetDetails);
          this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
          this.exportRequest['fileName'] =
            'Prebid Analytics Report ' +
            moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
          this.exportRequest['exportFormat'] = fileFormat;
          this.exportRequest['exportConfig'] = environment.exportConfig;
          // console.log('exportreport', this.exportRequest);
          this.dataFetchServ
            .getExportReportData(this.exportRequest)
            .subscribe(response => {
              console.log(response);
            });
        });
      }
    } else {
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Export Report',
        detail:
          'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
        life: 10000
      });
    }
    return false;
  }

  ngOnDestroy(): void {
    this.appConfigObs.unsubscribe();
  }
}
