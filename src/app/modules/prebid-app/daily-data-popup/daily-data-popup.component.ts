import { Component, OnInit } from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { FetchApiDataService } from '../fetch-api-data.service';
import * as moment from 'moment';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { DynamicDialogConfig } from 'primeng';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-daily-data-popup',
  templateUrl: './daily-data-popup.component.html',
  styleUrls: ['./daily-data-popup.component.scss']
})
export class DailyDataPopupComponent implements OnInit {
  flatTableReq: object;
  flatTableData: TreeNode[];
  flatTableColumnDef: any[];
  flatTableJson: Object;
  selectedDateRange: String;
  exportRequest: ExportRequest = <ExportRequest>{};
  currentField: string;
  pagination: boolean;
  appName: string;
  email: [];

  constructor(
    private libServ: CommonLibService,
    private dataFetchServ: FetchApiDataService,
    private toastService: ToastService,
    private config: DynamicDialogConfig,
    private exportService: ExportdataService,
    private formatNumPipe: FormatNumPipe
  ) {
    this.flatTableColumnDef = this.config.data['columnDef'];
    this.currentField = this.config.data['field'];
    this.flatTableReq = this.config.data['request'];
    this.appName = this.config.data['appName'];
    this.email = this.config.data['email'];
  }

  ngOnInit(): void {
    this.flatTableJson = {
      page_size: 10,
      lazy: false,
      loading: true,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '300px',
      totalRecords: 1000,
      columns: this.flatTableColumnDef.slice(1),
      selectedColumns: this.flatTableColumnDef.slice(1),
      globalFilterFields: this.flatTableColumnDef.slice(0),
      frozenCols: [this.flatTableColumnDef[0]],
      // frozenWidth: '250px',
      frozenWidth:
        this.flatTableColumnDef
          .slice(0, 1)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: false,
      overallSearch: false,
      columnSearch: false
    };
    this.loadFlatTableData(this.flatTableReq);
  }

  loadFlatTableData(params: Object) {
    this.flatTableJson['loading'] = true;
    this.flatTableJson['columns'] = this.flatTableColumnDef.slice(1);
    this.flatTableJson['selectedColumns'] = this.flatTableColumnDef.slice(1);
    this.flatTableJson['frozenCols'] = [this.flatTableColumnDef[0]];
    this.dataFetchServ.getTableData(params).subscribe(data => {
      const arr = [];
      const resData = data as {};
      for (const r of resData['data']) {
        const obj = {};
        // obj['children'] = [{}];
        obj['data'] = r;
        arr.push(obj);
      }
      this.flatTableData = <TreeNode[]>arr;
      this.pagination = true;
      this.flatTableJson['totalRecords'] = resData['totalItems'];
      this.flatTableJson['loading'] = false;
    });
  }
  resetPagination(tt1) {
    if (typeof tt1 !== 'undefined') {
      tt1.reset();
    }
  }

  onLazyLoadFlatTable(e: Event) {
    // console.log('Lazy', e);
    if (typeof this.flatTableReq !== 'undefined') {
      let orderby = [];
      if (
        typeof e['multiSortMeta'] !== 'undefined' &&
        e['multiSortMeta'] !== null
      ) {
        e['multiSortMeta'].forEach(sort => {
          orderby.push({
            key: sort['field'],
            opcode: sort['order'] === -1 ? 'desc' : 'asc'
          });
        });
      } else {
        orderby = [];
      }
      this.flatTableReq['limit'] = e['rows'];
      this.flatTableReq['offset'] = e['first'];
      this.flatTableReq['orderBy'] = orderby;
      this.loadFlatTableData(this.flatTableReq);
    }
  }

  exportTable(table, fileFormat) {
    this.exportRequest['sendEmail'] = this.email;
    this.exportRequest['appName'] = this.appName;
    if (this.exportRequest['sendEmail'].length > 0) {
      this.toastService.displayToast({
        severity: 'info',
        summary: 'Export Report',
        detail:
          'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
        life: 10000
      });
      if (table === 'flat') {
        const columnDefs = this.libServ.deepCopy(this.flatTableColumnDef);
        columnDefs.splice(0, 0, {
          field: 'time_key',
          displayName: 'Date',
          format: 'date',
          width: '250',
          exportConfig: {
            format: 'date',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
        });
        const req = this.libServ.deepCopy(this.flatTableReq);
        req['dimensions'] = ['time_key'].concat(req['dimensions']);
        req['groupByTimeKey'] = {
          key: ['accounting_key', 'time_key'],
          interval: 'daily'
        };
        req['orderBy'].unshift({ key: 'time_key', opcode: 'asc' });
        req['limit'] = '';
        req['offset'] = '';
        req['isTable'] = false;
        this.dataFetchServ.getTableData(req).subscribe(data => {
          const arr = [];
          const resData = data as {};

          const sheetDetails = {};
          sheetDetails['columnDef'] = columnDefs;
          sheetDetails['data'] = resData['data'];
          sheetDetails['sheetName'] = 'Prebid Data';
          sheetDetails['isRequest'] = false;
          sheetDetails['request'] = {
            url: '/api/flask/prebid/cassandradata',
            method: 'POST',
            param: req
          };
          sheetDetails['disclaimer'] = [
            {
              position: 'top',
              label:
                'Data persent in below table is not final and may varies over period of time',
              color: '#3A37CF'
            },
            {
              position: 'bottom',
              label: 'Thank You',
              color: '#EC6A15'
            }
          ];
          sheetDetails['totalFooter'] = {
            available: false,
            custom: false
          };
          sheetDetails['tableTitle'] = {
            available: true,
            label: 'Prebid Data'
          };
          sheetDetails['image'] = [
            {
              available: false,
              path: environment.exportConfig.exportLogo,
              position: 'top'
            }
          ];
          const sheetDetailsarray = [];
          sheetDetailsarray.push(sheetDetails);
          this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
          this.exportRequest['fileName'] =
            'Prebid Analytics Report ' +
            moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
          this.exportRequest['exportFormat'] = fileFormat;
          this.exportRequest['exportConfig'] = environment.exportConfig;
          // console.log('exportreport', this.exportRequest);
          this.dataFetchServ
            .getExportReportData(this.exportRequest)
            .subscribe(response => {
              console.log(response);
            });
        });
      } else if (table === 'agg') {
        const columnDefs = this.libServ.deepCopy(this.flatTableColumnDef);
        const req = this.libServ.deepCopy(this.flatTableReq);
        req['isTable'] = false;
        this.dataFetchServ.getTableData(req).subscribe(data => {
          const arr = [];
          const resData = data as {};

          const sheetDetails = {};
          sheetDetails['columnDef'] = columnDefs;
          sheetDetails['data'] = resData['data'];
          sheetDetails['sheetName'] = 'Prebid Data';
          sheetDetails['isRequest'] = false;
          sheetDetails['request'] = {
            url: '/api/flask/prebid/cassandradata',
            method: 'POST',
            param: req
          };
          sheetDetails['disclaimer'] = [
            {
              position: 'top',
              label:
                'Data persent in below table is not final and may varies over period of time',
              color: '#3A37CF'
            },
            {
              position: 'bottom',
              label: 'Thank You',
              color: '#EC6A15'
            }
          ];
          sheetDetails['totalFooter'] = {
            available: false,
            custom: false
          };
          sheetDetails['tableTitle'] = {
            available: true,
            label: 'Prebid Data'
          };
          sheetDetails['image'] = [
            {
              available: false,
              path: environment.exportConfig.exportLogo,
              position: 'top'
            }
          ];
          const sheetDetailsarray = [];
          sheetDetailsarray.push(sheetDetails);
          this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
          this.exportRequest['fileName'] =
            'Prebid Analytics Report ' +
            moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
          this.exportRequest['exportFormat'] = fileFormat;
          this.exportRequest['exportConfig'] = environment.exportConfig;
          // console.log('exportreport', this.exportRequest);
          this.dataFetchServ
            .getExportReportData(this.exportRequest)
            .subscribe(response => {
              console.log(response);
            });
        });
      }
    } else {
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Export Report',
        detail:
          'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
        life: 10000
      });
    }
    return false;
  }

}
