import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlatformPublisherDailyDataPopupComponent } from './platform-publisher-daily-data-popup.component';

describe('PlatformPublisherDailyDataPopupComponent', () => {
  let component: PlatformPublisherDailyDataPopupComponent;
  let fixture: ComponentFixture<PlatformPublisherDailyDataPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlatformPublisherDailyDataPopupComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlatformPublisherDailyDataPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
