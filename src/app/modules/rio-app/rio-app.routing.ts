import { Routes } from '@angular/router';

import { RioAppComponent } from './rio-app.component';
export const RIOAppRoutes: Routes = [
  {
    path: '',
    component: RioAppComponent
  }
];
