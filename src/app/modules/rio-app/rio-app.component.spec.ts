import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RioAppComponent } from './rio-app.component';

describe('CprComponent', () => {
  let component: RioAppComponent;
  let fixture: ComponentFixture<RioAppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RioAppComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RioAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
