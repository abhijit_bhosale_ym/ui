import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, from } from 'rxjs';
import { Title } from '@angular/platform-browser';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import * as moment from 'moment';

import { CommonLibService, AppConfigService } from 'src/app/_services';
import { FetchApiDataService } from './fetch-api-data.service';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';

import { NewRioComponent } from './new-io/new-rio.component';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { FilterDataService } from './../../../app/_services/filter-data/filter-data.service';



@Component({
  selector: 'ym-rio-app',
  templateUrl: './rio-app.component.html',
  styleUrls: ['./rio-app.component.scss']
})
export class RioAppComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;

  appConfig: object = {};
  lastUpdatedOn: Date;
  tableRequestParam = {};
  favoriteCollection = {};
  filtersApplied: object = {};
  allStatus: any;
  messages = [];
  unreadCount = 0;
  nextUpdated: Date;
  dataUpdatedThrough: Date;
  exportRequest: ExportRequest = <ExportRequest>{};

  flatTableReq: object;
  flatTableData: TreeNode[];
  flatTableColumnDef: any[];
  flatTableJson: Object;
  tblResData: any[];
  displayAggTable = true;
  noTableData = false;
  isExportReport = false;

  constructor(
    private confirmationService: ConfirmationService,

    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private toastService: ToastService,
    private dialogService: DialogService,
    private formatNumPipe: FormatNumPipe,
    private dataFetchServ: FetchApiDataService,
    private exportService: ExportdataService,
    private pptExport: ExportPptService,
    private htmltoimage: HtmltoimageService,
    private filterDataService: FilterDataService,

  ) {


  }

  ngOnInit() {

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        console.log('appconfig', appConfig);
        this.isExportReport = this.appConfig['permissions'].some(
          o => o.name === 'rio-export-reports'
        );
        this._titleService.setTitle(this.appConfig['displayName']);


        this.filtersApplied = {
          timeKeyFilter: {
          },
          filters: { dimensions: [], metrics: [] },
          groupby: this.appConfig['filter']['filterConfig']['groupBy']
        };


        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        this.initialLoading();
      }
    });


    this.flatTableColumnDef = [
      {
        field: 'sales_represntative',
        displayName: 'Sales Representative',
        format: '',
        width: '170',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'project_manager',
        displayName: 'Project Manager',
        format: '',
        width: '150',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'advertiser',
        displayName: 'Advertiser',
        format: '',
        width: '150',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'category',
        displayName: 'Category',
        format: '',
        width: '140',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'io_line_no_mapping',
        displayName: 'IO Line #',
        format: '',
        width: '100',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'billing_ad_server',
        displayName: 'Billing Ad Server',
        format: '',
        width: '120',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'campaign',
        displayName: 'Campaign',
        format: '',
        width: '200',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },


      {
        field: 'product',
        displayName: 'Product',
        format: '',
        width: '170',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },



      {
        field: 'placement',
        displayName: 'Placement',
        format: '',
        width: '200',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },



      {
        field: 'start_date',
        displayName: 'Start Date',
        format: '',
        width: '100',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'end_date',
        displayName: 'End Date',
        format: '',
        width: '100',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'rate_type',
        displayName: 'Rate Type',
        format: '',
        width: '100',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'rate',
        displayName: 'Rate',
        format: '$',
        width: '100',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'booked_qty',
        displayName: 'Booked Quantity',
        format: '',
        width: '150',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'total_cost',
        displayName: 'Total Cost',
        format: '$',
        width: '100',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'status',
        displayName: 'Status',
        format: '',
        width: '110',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'delete',
        displayName: 'Delete',
        format: '',
        width: '60',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'edit',
        displayName: 'Edit',
        format: '',
        width: '60',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: false
        }
      },
    ];

    this.flatTableJson = {
      page_size: 10,
      page: 0,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '290px',
      totalRecords: 1000,
      columns: this.flatTableColumnDef.slice(0),
      selectedColumns: this.flatTableColumnDef.slice(0),
      frozenCols: [this.flatTableColumnDef[0]],
      // frozenWidth: '250px',
      frozenWidth:
        this.flatTableColumnDef
          .slice(0, 0)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      // columns: this.flatTableColumnDef.slice(this.flatTableColumnDef.length-1),
      // selectedColumns: [this.flatTableColumnDef],
      // frozenCols: [this.flatTableColumnDef[0]],
      // frozenWidth:
      //   this.flatTableColumnDef
      //     .slice(0, 1)
      //     .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: true
    };
  }

  initialLoading() {
    this.dataFetchServ
    .getLastUpdatedData(this.appConfig['id'])
    .subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      }
      this.lastUpdatedOn = moment(data[0]['updated_at']).toDate();
      this.nextUpdated = moment(data[0]['next_run_at']).toDate();
      this.dataUpdatedThrough = moment(
        data[0]['source_updated_through'],
        'YYYYMMDD'
      ).toDate();
    });


    this.getTableData();
  }


  getTableData() {
    const req = {
      'filters': this.filtersApplied['filters']['dimensions']
    };

    this.dataFetchServ.getRioData(req).subscribe(data => {
      this.noTableData = false;
      const arr = [];
      for (const r of data['data']) {
        const obj = {};
        obj['data'] = r;
        arr.push(obj);
      }

      this.tblResData = arr;

      this.flatTableData = <TreeNode[]>arr;
      this.flatTableJson['totalRecords'] = data['totalItems'];
      this.flatTableJson['loading'] = false;
      setTimeout(() => {
        this.flatTableJson['lazy'] = false;
      }, 0);
    });
  }



  reLoadTableData() {
    const tableReq = this.libServ.deepCopy(this.tableRequestParam);
    tableReq.filters = this.libServ.deepCopy(this.filtersApplied['filters']);
  }

  openPopup(row, col) {

  }

  onLazyLoadAggTable(e: Event) {
    this.getTableData();
  }

  isHiddenColumn(col: Object) {
    return (
      this.flatTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.flatTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );

  }

  NewRio() {
    const data = {
      isEdit: false,
      data: this.flatTableData,
      table: this.noTableData
    };

    const ref = this.dialogService.open(NewRioComponent, {
      data: data,
      header: 'New RIO',
      contentStyle: { width: '80vw', height: '570px', overflow: 'auto' },
    });

    ref.onClose.subscribe((data: string) => {
      this.getTableData();
    }
    );
  }

  onLazyLoadFlatTable(e: Event) {
    if (typeof this.flatTableReq !== 'undefined' && this.flatTableJson['lazy']) {
      console.log('onLazyLoadFlatTable', this.flatTableJson['lazy']);
      let orderby = [];
      if (
        typeof e['multiSortMeta'] !== 'undefined' &&
        e['multiSortMeta'] !== null
      ) {
        e['multiSortMeta'].forEach(sort => {
          orderby.push({
            key: sort['field'],
            opcode: sort['order'] === -1 ? 'desc' : 'asc'
          });
        });
      } else {
        orderby = [];
      }
      // orderby = orderby.slice(0, orderby.length - 1);
      this.flatTableReq['limit'] = e['rows'];
      this.flatTableReq['offset'] = e['first'];
      // if (orderby[0]['key'].includes('fill_rate')) {
      //   return;
      // }
      this.flatTableReq['orderBy'] = orderby;
      this.getTableData();
    }
  }

  exportTable(fileFormat) {
    const data = [];
    const finalColDef = this.libServ.deepCopy(this.flatTableColumnDef);

    const req = {
      'filters': this.filtersApplied['filters']['dimensions']
    };

    this.dataFetchServ
      .getRioData(req)
      .subscribe(response => {
        if (response['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          return;
        }
        data.push({
          data: [
            {
              data: response['data'],
              columnDefs: finalColDef
            }
          ],
          sheetname: 'RIO'
        });

        this.exportService.exportReport(
          data,
          `Received_IO_${moment(new Date()).format('MM-DD-YYYY HH:mm:ss')}`,
          fileFormat,
          false
        );

      });
  }

  onFiltersApplied(filterData: object) {
    this.filtersApplied['filters']['dimensions'] = [];
    for (const k in filterData['filter']['dimensions']) {
      if (filterData['filter']['dimensions'][k].length !== 0) {
        this.filtersApplied['filters']['dimensions'].push({
          key: k,
          values: filterData['filter']['dimensions'][k]
        });
      }
    }

    this.getTableData();
  }

  deleteRio(rowData) {

    const msg = 'Do you want to delete ' + rowData['placement'];
    const toastMsg = 'Delete :  ' + rowData['placement'];
    this.confirmationService.confirm({
      message: msg,
      accept: () => {

        this.dataFetchServ.deleteInsertionOrder({ campaign: rowData['campaign'], placement: rowData['placement'] }).subscribe(data => {
          if (data['status']) {
            this.toastService.displayToast({
              severity: 'info',
              summary: 'Delete Placement',
              detail:
                rowData['placement'] + ' deleted succesfully',
              life: 10000
            });
          }
          this.getTableData();

        });
      },
      reject: () => {

      }
    });

  }

  editRio(rowData) {
    const data = {
      isEdit: true,
      data: rowData,
      table: this.noTableData
    };

    const ref = this.dialogService.open(NewRioComponent, {
      data: data,
      header: 'New RIO',
      contentStyle: { width: '80vw', height: '570px', overflow: 'auto' },
    });
  }

  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }
}
