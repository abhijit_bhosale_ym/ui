import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FetchApiDataService {
  private BASE_URL: string = environment.baseUrl;

  constructor(private http: HttpClient) {}

  getTableData(params: object, activeView: boolean) {
    let url = `${this.BASE_URL}/frankly/v1/campaign-progress-report/getData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    if (typeof activeView !== 'undefined') {
      url = `${url}${activeView ? '?status=active' : '?status=inactive'}`;
    }
    return this.http.post(url, params);
  }

  getFilterValues(params: string) {
    const url = `${this.BASE_URL}/afar/v1/rio/allfilter/${params}`;
    return this.http.get(url);
  }

  getExportReportData(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/common/exportData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
    // return <TreeNode[]> json.data;
  }

  getRioData(params: object) {
    const url = `${this.BASE_URL}/afar/v1/rio/get-insertion-order`;
    return this.http.post(url, params);
  }

  saveInsertionOrder(params) {
    const url = `${this.BASE_URL}/afar/v1/rio/save-insertion-order`;
    return this.http.post(url, params
);
  }

  updateInsertionOrder(params: object) {
    const url = `${this.BASE_URL}/afar/v1/rio/update-insertion-order`;
    return this.http.post(url, params);
  }

  deleteInsertionOrder(params: object) {
    const url = `${this.BASE_URL}/afar/v1/rio/delete-insertion-order`;
    return this.http.post(url, params);
  }


  getCommentsCounts() {
    const url = `${this.BASE_URL}/frankly/v1/campaign-progress-report/getCommentsCountData`;
    return this.http.get(url);
  }

  getFavorite() {
    const url = `${this.BASE_URL}/frankly/v1/campaign-progress-report/getFavoriteData`;
    return this.http.get(url);
  }

  setFavourite(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/campaign-progress-report/set-favorite`;
    return this.http.post(url, params);
  }

  setFavouriteLine(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/campaign-progress-report/set-favorite-line`;
    return this.http.post(url, params);
  }

  getCommentsData() {
    const url = `${this.BASE_URL}/frankly/v1/campaign-progress-report/getCommentsData`;
    return this.http.get(url);
  }

  getCommentsCountData() {
    const url = `${this.BASE_URL}/frankly/v1/campaign-progress-report/getCommentsCountData`;
    return this.http.get(url);
  }

  getLastUpdatedData(appId) {
    const url = `${this.BASE_URL}/frankly/v1/common/getLastUpdatedData/${appId}`;
    return this.http.get(url);
  }
}
