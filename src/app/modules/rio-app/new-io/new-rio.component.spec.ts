import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewRioComponent } from './new-rio.component';

describe('DailyDataComponent', () => {
  let component: NewRioComponent;
  let fixture: ComponentFixture<NewRioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NewRioComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewRioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
