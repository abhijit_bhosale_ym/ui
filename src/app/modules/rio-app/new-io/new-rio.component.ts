import { Component, OnInit } from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { FetchApiDataService } from '../fetch-api-data.service';
import * as moment from 'moment';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { environment } from 'src/environments/environment';
import { FilterDataService } from './../../../../app/_services/filter-data/filter-data.service';
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder
} from '@angular/forms';
import { DialogService } from 'primeng/dynamicdialog';
// import {ReplaySubject} from "rxjs/ReplaySubject";
// import {Observable} from "rxjs/Observable";

@Component({
  selector: 'ym-new-rio',
  templateUrl: './new-rio.component.html',
  styleUrls: ['./new-rio.component.scss']
})
export class NewRioComponent implements OnInit {
  BASE_URL: string = environment.baseUrl;
  filterUrl: string = this.BASE_URL + '/afar/v1/rio/filter';
  exportRequest: ExportRequest = <ExportRequest>{};
  formNewRioModel: FormGroup;
  salesRepOptions: object;
  projectMgrOptions: object;
  billingAdServersOptions: object;
  productOptions: object;
  rateTypeOptions: object;
  statusOptions: object;
  catagoryOptions: object;
  selectedSalesRep: object = {};
  selectedProjectMgmr: object = {};
  selectedCatagory: object = {};
  selectedBillingAdServer: object = {};
  selectedProdcut: object = {};
  selectedRateType: object = {};
  selectedStatus: object = {};
  advertiser: string;
  bookedQty: string;
  totalCost: string;
  ioLine: string;
  campaign: string;
  placement: string;
  startDate: string;
  endDate: string;
  camapaign: string;
  rate: string;
  editTableData: Object;
  isEdit = false;

  namefile: any[];
  update: any;
  deleteFile = false;
  uploadFile = false;
  downloadFile = false;
  frameUploader: any;
  files: any;
  tbleData: any[];
  table: any;

  constructor(
    private libServ: CommonLibService,
    private formatNumPipe: FormatNumPipe,
    private dataFetchServ: FetchApiDataService,
    private config: DynamicDialogConfig,
    private exportService: ExportdataService,
    private toastService: ToastService,
    private filterDataService: FilterDataService,
    private fb: FormBuilder,
    private dialogService: DialogService,
    private dynamicDialogRef: DynamicDialogRef
  ) {
    this.isEdit = this.config.data['isEdit'];

    if (this.isEdit) { this.editTableData = this.config.data['data']; } else {
      this.tbleData = this.config.data['data'];
      this.table = this.config.data['table'];
    }
  }

  ngOnInit() {
    this.formNewRioModel = this.fb.group({
      selectedSalesRep: new FormControl(),
      selectedProjectMgmr: new FormControl(),
      advertiser: new FormControl(),
      selectedCatagory: new FormControl(),
      selectedBillingAdServer: new FormControl(),
      camapaign: new FormControl('', Validators.required),
      ioLine: new FormControl(),
      selectedProdcut: new FormControl(),
      placement: new FormControl('', Validators.required),
      startDate: new FormControl(),
      endDate: new FormControl(),
      selectedRateType: new FormControl(),
      rate: new FormControl(),
      bookedQty: new FormControl(),
      totalCost: new FormControl(),
      selectedStatus: new FormControl(),
      files: ['']
    });

    this.getFilters('Sales Representative');
    this.getFilters('Project Manager');
    this.getDefaultFilters('billing_ad_server');
    this.getDefaultFilters('rate_type');
    this.getDefaultFilters('product');
    this.getDefaultFilters('status');
    this.getDefaultFilters('category');

    if (this.isEdit) {
      this.selectedSalesRep = {
        key: this.editTableData['sales_represntative'],
        label: this.editTableData['sales_represntative']
      };
      this.selectedProjectMgmr = {
        key: this.editTableData['project_manager'],
        label: this.editTableData['project_manager']
      };
      this.advertiser = this.editTableData['advertiser'];
      this.selectedCatagory = {
        key: this.editTableData['category'],
        label: this.editTableData['category']
      };
      this.ioLine = this.editTableData['io_line_no_mapping'];
      this.selectedBillingAdServer = {
        key: this.editTableData['billing_ad_server'],
        label: this.editTableData['billing_ad_server']
      };
      this.camapaign = this.editTableData['campaign'];
      this.selectedProdcut = {
        key: this.editTableData['product'],
        label: this.editTableData['product']
      };
      this.placement = this.editTableData['placement'];
      this.startDate = this.editTableData['start_date'];
      this.endDate = this.editTableData['end_date'];
      this.selectedRateType = {
        key: this.editTableData['rate_type'],
        label: this.editTableData['rate_type']
      };
      this.rate = this.editTableData['rate'];
      this.bookedQty = this.editTableData['booked_qty'];
      this.totalCost = this.editTableData['total_cost'];
      this.selectedStatus = {
        key: this.editTableData['status'],
        label: this.editTableData['status']
      };
    }
  }

  getFilters(params) {
    this.dataFetchServ.getFilterValues(params).subscribe(res => {
      res['data'].unshift({
        key: null,
        label: 'Select ' + this.prettify(params)
      });

      if (params == 'Sales Representative') { this.salesRepOptions = res['data']; } else if (params == 'Project Manager') {
        this.projectMgrOptions = res['data'];
           }
    });
  }

  getDefaultFilters(param) {
    this.filterDataService
      .getFilterDataList(`${this.filterUrl}/${param}`, {})
      .subscribe(res => {
        res['data'].unshift({
          key: null,
          label: 'Select ' + this.prettify(param)
        });
        if (param == 'billing_ad_server') {
          this.billingAdServersOptions = res['data'];
        } else if (param == 'product') { this.productOptions = res['data']; } else if (param == 'rate_type') { this.rateTypeOptions = res['data']; } else if (param == 'status') { this.statusOptions = res['data']; } else if (param == 'category') { this.catagoryOptions = res['data']; }
      });
  }

  upload = event => {
    for (let i = 0; i < event.target.files.length; i++) {
      this.files = event.target.files[i];
    }
  }

  submitHandler() {
    const newRioObj = {
      sales_represntative: this.selectedSalesRep['key'],
      project_manager: this.selectedProjectMgmr['key'],
      advertiser: this.advertiser,
      category: this.selectedCatagory['key'],
      io_line_no_mapping: this.ioLine,
      billing_ad_server: this.selectedBillingAdServer['key'],
      campaign: this.camapaign,
      product: this.selectedProdcut['key'],
      placement: this.placement,
      start_date: moment(this.startDate).format('MM/DD/YYYY'),
      end_date: moment(this.endDate).format('MM/DD/YYYY'),
      rate_type: this.selectedRateType['key'],
      rate: this.rate,
      booked_qty: this.bookedQty,
      total_cost: this.totalCost,
      status: this.selectedStatus['key'],
      files: this.files
    };
    if (this.isEdit) {
      this.dataFetchServ.updateInsertionOrder(newRioObj).subscribe(res => {
        if (res['status']) {
          this.toastService.displayToast({
            severity: 'info',
            summary: 'Update Placement',
            detail: this.placement + ' updated succesfully',
            life: 10000
          });

          this.dialogService.dialogComponentRef.destroy();

          this.editTableData['sales_represntative'] = this.selectedSalesRep[
            'label'
          ];
          this.editTableData['project_manager'] = this.selectedProjectMgmr[
            'label'
          ];
          this.editTableData['advertiser'] = this.advertiser;
          this.editTableData['category'] = this.selectedCatagory['label'];
          this.editTableData['io_line_no_mapping'] = this.ioLine;
          this.editTableData[
            'billing_ad_server'
          ] = this.selectedBillingAdServer['label'];
          this.editTableData['campaign'] = this.camapaign;
          this.editTableData['product'] = this.selectedProdcut['label'];
          this.editTableData['placement'] = this.placement;
          this.editTableData['start_date'] = this.startDate;
          this.editTableData['end_date'] = this.endDate;
          this.editTableData['rate_type'] = this.selectedRateType['label'];
          this.editTableData['rate'] = this.rate;
          this.editTableData['booked_qty'] = this.bookedQty;
          this.editTableData['total_cost'] = this.totalCost;
          this.editTableData['status'] = this.selectedStatus['label'];

          this.dialogService.dialogComponentRef.destroy();
        }
      });
    } else {
      this.table = false;
      this.dataFetchServ.saveInsertionOrder(newRioObj).subscribe(res => {
        if (res['status']) {
          this.toastService.displayToast({
            severity: 'info',
            summary: 'New Placement',
            detail: 'Placement added succesfully',
            life: 10000
          });

          const data = {
            data: {
              advertiser: this.advertiser,
              end_date: this.endDate,
              note: '',
              product: this.selectedProdcut['label'],
              sales_represntative: this.selectedSalesRep['label'],
              total_cost: this.totalCost,
              rate_type: this.selectedRateType['label'],
              billing_ad_server: this.selectedBillingAdServer['label'],
              booked_qty: this.bookedQty,
              updated_at: '',
              rate: this.rate,
              campaign: this.camapaign,
              project_manager: this.selectedProjectMgmr['label'],
              placement: this.placement,
              category: this.selectedCatagory['label'],
              io_line_no_mapping: this.ioLine,
              inserted_at: '',
              start_date: this.startDate,
              status: this.selectedStatus
            }
          };

          this.tbleData.unshift(data);
          this.table = true;
          this.dynamicDialogRef.close(null);
        }
      });
    }
  }

  onFileSelect(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.formNewRioModel.get('files').setValue(file);
    }
  }

  editRio() {}

  goBack() {
    this.dialogService.dialogComponentRef.destroy();
  }

  prettify(str) {
    return str.replace(/(?:_| |\b)(\w)/g, function($1) {
      return $1.toUpperCase().replace('_', ' ');
    });
  }

  isHiddenColumn(col) {}
}
