import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalysisReportFreecycleComponent } from './analysis-report-freecycle.component';

describe('AnalysisReportFreecycleComponent', () => {
  let component: AnalysisReportFreecycleComponent;
  let fixture: ComponentFixture<AnalysisReportFreecycleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnalysisReportFreecycleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalysisReportFreecycleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
