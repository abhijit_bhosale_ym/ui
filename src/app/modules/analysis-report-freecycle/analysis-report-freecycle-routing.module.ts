import { Routes } from '@angular/router';
import { AnalysisReportFreecycleComponent } from './analysis-report-freecycle.component';

export const AnalysisReportAppRoutesAppRoutes: Routes = [
  {
    path: '',
    component: AnalysisReportFreecycleComponent
  }
];
