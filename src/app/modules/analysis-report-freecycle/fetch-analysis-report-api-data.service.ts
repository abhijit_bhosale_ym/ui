import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FetchAnalysisReportApiDataService {
  private BASE_URL: string = environment.baseUrl;
  public responseRevSubject = new BehaviorSubject({});
  private responseRevData = this.responseRevSubject.asObservable();

  public responseVastSubject = new BehaviorSubject({});
  private responseVastData = this.responseVastSubject.asObservable();

  public responseUnfilledSubject = new BehaviorSubject({});
  private responseUnfilledData = this.responseUnfilledSubject.asObservable();

  // public obsresponse = this.responseData.asObservable();

  constructor(private http: HttpClient) { }

  getTableData(params: object, ispermission: boolean) {
    const url = `${this.BASE_URL}/freecycle/v1/revmgmt-fc/getData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    if (ispermission) {
      return this.http.post(url, params);
    } else {
      return this.responseRevData;
    }
  }
  getunfilledData(params: object, ispermission: boolean) {
    const url = `${this.BASE_URL}/freecycle/v1/unfilled-inventory-fc/getData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    if (ispermission) {
      return this.http.post(url, params);
    } else {
      return this.responseUnfilledData;
    }
  }
  getExportReportData(params: object) {
    const url = `${this.BASE_URL}/freecycle/v1/common/exportData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
  }
}
