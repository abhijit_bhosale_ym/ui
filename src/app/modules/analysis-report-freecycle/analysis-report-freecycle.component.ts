import { Component, OnInit, OnDestroy } from '@angular/core';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { CommonLibService, DataShareService } from 'src/app/_services';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';
import { FetchAnalysisReportApiDataService } from './fetch-analysis-report-api-data.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { groupBy, filter } from 'rxjs/operators';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { Subscription } from 'rxjs';
import { element } from 'protractor';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { DialogService } from 'primeng';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-analysis-report-freecycle',
  templateUrl: './analysis-report-freecycle.component.html',
  styleUrls: ['./analysis-report-freecycle.component.scss']
})
export class AnalysisReportFreecycleComponent implements OnInit,OnDestroy {
  private appConfigObs: Subscription;
  noTableData = false;
  columns: object[];
  arr: object[];
  status: boolean;
  filtersApplied: object = {};
  flatTableData: TreeNode[];
  flatTableColumnDef: any[];
  flatTableJson: Object;
  tableReq: Object;
  revenuetableReq: Object;
  data: any;
  data1: any;
  startD: any;
  endD: any;
  finalData: any[];

  revenueflatTableData: TreeNode[];
  revenueflatTableColumnDef: any[];
  revenueflatTableJson: Object;
  appConfig: object = {};
  exportRequest: ExportRequest = <ExportRequest>{};

  isRevenuemgt = false;
  isVast = false;
  isUnfilled = false;
  isExportReport = false;
  timeout: any;
  flatTableReq: object;
  isExport = false;

  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: FetchAnalysisReportApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private formatNumPipe: FormatNumPipe,
    private exportService: ExportdataService,
    private pptExport: ExportPptService,
    private confirmationService: ConfirmationService,
    private htmltoimage: HtmltoimageService // private exportService: ExportdataService
  ) { }

  ngOnInit() {
    this.flatTableColumnDef = [
      {
        field: 'data_source',
        displayName: 'Data Source',
        format: '',
        width: '175',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'kpis',
        displayName: 'KPIs',
        format: '',
        width: '200',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'yesterday_data',
        displayName: 'yesterday_data',
        format: '$',
        width: '175',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'seven_days_average',
        displayName: '7 Days Average',
        format: '$',
        width: '220',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'seven_days_average_vs_yesterday',
        displayName: '7 Days Average vs yesterday',
        format: '',
        width: '250',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: true
        }
      },

      {
        field: 'current_month_data',
        displayName: 'Current Month Data',
        format: '$',
        width: '220',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: true
        }
      }
    ];
    this.flatTableJson = {
      page_size: 10,
      page: 1,
      lazy: false,
      loading: true,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '200px',
      totalRecords: 1000,
      columns: this.flatTableColumnDef,
      selectedColumns: this.flatTableColumnDef,
      frozenCols: [this.flatTableColumnDef[0]],
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: false,
      columnSearch: false
    };
    this.revenueflatTableColumnDef = [
      {
        field: 'source',
        displayName: 'Demand Source',
        format: '',
        width: '175',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'dp_impressions',
        displayName: 'Last 7 Days Impressions',
        format: 'number',
        width: '175',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'rev_cpm',
        displayName: 'Last 7 Days eCPM',
        format: '$',
        width: '175',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'dp_revenue',
        displayName: 'Last 7 Days Revenue',
        format: '$',
        width: '175',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'seven_day_avg_ecpm',
        displayName: 'Last 7 Days Average eCPM across sources',
        format: '$',
        width: '175',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'lost_avg_revenue',
        displayName: 'Lost Revenue Opportunity for 7 Days',
        format: '$',
        width: '175',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      }
    ];
    this.revenueflatTableJson = {
      page_size: 10,
      page: 0,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '200px',
      totalRecords: 1000,
      columns: this.revenueflatTableColumnDef.slice(0),
      selectedColumns: this.revenueflatTableColumnDef.slice(0),
      // frozenCols: [this.revenueflatTableColumnDef[0]],
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: false,
      overallSearch: true,
      columnSearch: false
    };
    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        console.log('appConfig', appConfig);
        this.isExportReport = this.appConfig['permissions'].some(
          o => o.name === 'analysis-export-report-fc'
        );
        this.appConfig['user']['role']['appGroups'].forEach(appGroups => {
          if (appGroups['apps'].some(
            o => o.name === 'Revenue Management fc'
          )) {
            this.isRevenuemgt = true;
          }

          if (appGroups['apps'].some(
            o => o.name === 'Unfilled Inventory fc'
          )) {
            this.isUnfilled = true;
          }

          if (appGroups['apps'].some(
            o => o.name === 'Vast Errors'
          )) {
            this.isVast = true;
          }
        });


        this._titleService.setTitle(this.appConfig['displayName']);
        let startDate;
        if (
          this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
          'defaultDate'
          ][0]['startOf']
        ) {
          startDate = moment()
            .subtract(
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['value'],
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['period']
            )
            .startOf(
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['period']
            );
        } else {
          startDate = moment().subtract(
            this.appConfig['filter']['filterConfig']['filters'][
            'datePeriod'
            ][0]['defaultDate'][0]['value'],
            this.appConfig['filter']['filterConfig']['filters'][
            'datePeriod'
            ][0]['defaultDate'][0]['period']
          );
          AppConfigService;
        }
        const endDate = moment().subtract(
          this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
          'defaultDate'
          ][1]['value'],
          this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
          'defaultDate'
          ][1]['period']
        );

        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: startDate.format('YYYYMMDD'),
            time_key2: endDate.format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: this.appConfig['filter']['filterConfig']['groupBy']
        };
        this.data = moment().subtract(2, 'days');
        this.data1 = moment(
          this.filtersApplied['timeKeyFilter']['time_key1'],
          'YYYYMMDD'
        )
          .subtract(1, 'day')
          .format('DD MMM YY');
        this.startD = moment(
          this.filtersApplied['timeKeyFilter']['time_key1'],
          'YYYYMMDD'
        )
          .subtract(8, 'day')
          .format('DD MMM YY');
        this.endD = moment(
          this.filtersApplied['timeKeyFilter']['time_key1'],
          'YYYYMMDD'
        )
          .subtract(2, 'day')
          .format('DD MMM YY');
      }
      this.exportRequest['appName'] = this.appConfig['name'].toString();
      this.exportRequest['sendEmail'] = this.appConfig['user'][
        'contactEmail'
      ].split(',');


      this.initialLoading();
    });
  }

  initialLoading() {
    this.loadKPITableData();
    this.revenuetableReq = {
      dimensions: ['source'],
      metrics: ['dp_revenue', 'dp_impressions'],
      derived_metrics: ['rev_cpm', 'seven_day_avg_ecpm', 'lost_avg_revenue'],
      timeKeyFilter: {
        time_key1: moment(
          this.filtersApplied['timeKeyFilter']['time_key1'],
          'YYYYMMDD'
        )
          .subtract(7, 'days')
          .format('YYYYMMDD'),
        time_key2: moment(
          this.filtersApplied['timeKeyFilter']['time_key1'],
          'YYYYMMDD'
        )
          .subtract(1, 'days')
          .format('YYYYMMDD')
      },
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [],
      limit: '',
      offset: ''
    };
    this.loadLostRevenueTableData(this.revenuetableReq);
  }

  loadKPITableData() {
    this.isExport = false;
    this.tableReq = {
      dimensions: ['time_key'],
      metrics: ['dp_revenue', 'dp_impressions'],
      derived_metrics: [
        // 'this_day_revenue'
        // 'this_day_impressions',
        // 'last_sev_day_revenue_avg',
        // 'last_sev_day_impressions_avg',
        // 'last_sev_day_revenue_avg_vs_this',
        // 'last_sev_day_impressions_avg_vs_this',
        // 'current_month_revenue',
        // 'current_month_impressions'
      ],
      timeKeyFilter: {
        time_key1: moment(
          this.filtersApplied['timeKeyFilter']['time_key1'],
          'YYYYMMDD'
        )
          .subtract(30, 'day')
          .format('YYYYMMDD'),
        time_key2: this.filtersApplied['timeKeyFilter']['time_key1']
      },
      filters: this.filtersApplied['filters'],
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'], // grpBys,accounting_key
      orderBy: [],
      limit: '',
      offset: ''
    };

    const unfilled_req = {
      dimensions: ['time_key'],
      metrics: ['unfilled_impressions'],
      derived_metrics: [
        // 'this_day_unfilled_impressions',
        // 'last_sev_day_unfilled_impressions_avg',
        // 'last_sev_day_unfilled_impressions_avg_vs_this',
        // 'current_month_unfilled_impressions'
      ],
      timeKeyFilter: {
        time_key1: moment(
          this.filtersApplied['timeKeyFilter']['time_key1'],
          'YYYYMMDD'
        )
          .subtract(30, 'day')
          .format('YYYYMMDD'),
        time_key2: this.filtersApplied['timeKeyFilter']['time_key1']
      },
      filters: this.filtersApplied['filters'],
      groupByTimeKey: {
        key: ['time_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };

    this.flatTableJson['loading'] = true;
    this.isRevenuemgt === false ? this.dataFetchServ.responseRevSubject.next({ data: [], totalItems: 0 }) : '';
    this.appConfigObs = this.dataFetchServ.getTableData(this.tableReq, this.isRevenuemgt).subscribe(data => {
      if (!this.isRevenuemgt == false) {
        this.appConfigObs.unsubscribe();
      }
      console.log('dataRev.....', data);
      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });

        return;
      } else {
        this.noTableData = false;
      }

      const revdata = data['data'];
      const data2 = data as [];
      const objrev = {};
      const objimp = {};
      const objecpm = {};
      const objunfill = {};
      // const objvast = {};

      objrev['data_source'] = 'Programmatic';
      objimp['data_source'] = 'Programmatic';
      objecpm['data_source'] = 'Programmatic';
      objunfill['data_source'] = 'Prog. + Direct';
      // objvast['data_source'] = 'Prog. + Direct';
      objrev['kpis'] = 'Revenue';
      objimp['kpis'] = 'Total impressions';
      objecpm['kpis'] = 'eCPM';
      objunfill['kpis'] = 'Unfilled Impressions';
      // objvast['kpis'] = 'VAST Error Rate';
      const yesterday = moment(
        this.filtersApplied['timeKeyFilter']['time_key1'],
        'YYYYMMDD'
      )
        .subtract(1, 'day')
        .format('YYYYMMDD');
      const sevenday = moment(
        this.filtersApplied['timeKeyFilter']['time_key1'],
        'YYYYMMDD'
      )
        .subtract(8, 'day')
        .format('YYYYMMDD');

      const startOfMonth = moment(
        this.filtersApplied['timeKeyFilter']['time_key1'],
        'YYYYMMDD'
      )
        .startOf('month')
        .format('YYYYMMDD');
      const arr = [];

      for (const r of revdata) {
        if (r.time_key == yesterday) {
          objrev['yesterday_data'] = r.dp_revenue;
          objimp['yesterday_data'] = r.dp_impressions;
          // objecpm['yesterday_data'] = (r.r.dp_impressions * 1000) / r.dp_revenue;
        }

        if (r.time_key < yesterday && r.time_key >= sevenday) {
          objrev['seven_days_average'] =
            objrev['seven_days_average'] == undefined
              ? r.dp_revenue
              : objrev['seven_days_average'] + r.dp_revenue;
          objimp['seven_days_average'] =
            objimp['seven_days_average'] == undefined
              ? r.dp_impressions
              : objimp['seven_days_average'] + r.dp_impressions;
          // objecpm['seven_days_average'] = (r.r.dp_impressions * 1000) / r.dp_revenue;
        }
        if (r.time_key >= startOfMonth) {
          objrev['current_month_data'] =
            objrev['current_month_data'] == undefined
              ? r.dp_revenue
              : objrev['current_month_data'] + r.dp_revenue;
          objimp['current_month_data'] =
            objimp['current_month_data'] == undefined
              ? r.dp_impressions
              : objimp['current_month_data'] + r.dp_impressions;
        }
      }
      if (objimp['yesterday_data'] != undefined) {
        objecpm['yesterday_data'] =
          (objrev['yesterday_data'] * 1000) / objimp['yesterday_data'];
      }
      if (objimp['seven_days_average'] != undefined) {
        objecpm['seven_days_average'] =
          ((objrev['seven_days_average'] / 7) * 1000) /
          (objimp['seven_days_average'] / 7);
      }
      if (objimp['current_month_data'] != undefined) {
        objecpm['current_month_data'] =
          (objrev['current_month_data'] * 1000) / objimp['current_month_data'];
      }
      // this.isVast === false ? this.dataFetchServ.responseVastSubject.next({ data: [], totalItems: 0 }) : '';
      // this.appConfigObs = this.dataFetchServ.getVastData(vast_req, this.isVast).subscribe(data1 => {
        console.log('datavast.....', data2);
        // if (!this.isVast == false) {
        //   this.appConfigObs.unsubscribe();
        // }
        // const vastData = data1['data'];
        // for (const r of vastData) {
        //   if (r.time_key == yesterday) {
        //     objvast['yesterday_data'] = r.total_vast_rate;
        //   }

        //   if (r.time_key < yesterday && r.time_key >= sevenday) {
        //     objvast['seven_days_average'] =
        //       objvast['seven_days_average'] == undefined
        //         ? r.total_vast_rate
        //         : objvast['seven_days_average'] + r.total_vast_rate;
        //   }
        //   if (r.time_key >= startOfMonth) {
        //     objvast['current_month_data'] =
        //       objvast['current_month_data'] == undefined
        //         ? r.total_vast_rate
        //         : objvast['current_month_data'] + r.total_vast_rate;
        //   }
        // }
        this.isUnfilled === false ? this.dataFetchServ.responseUnfilledSubject.next({ data: [], totalItems: 0 }) : '';
        this.appConfigObs = this.dataFetchServ.getunfilledData(unfilled_req, this.isUnfilled).subscribe(data2 => {
          if (this.isUnfilled === false && this.appConfigObs && !this.appConfigObs.closed) {
            this.appConfigObs.unsubscribe();
          }
          console.log('data2unfiiled.....', data2);
          const unfilledData = data2['data'];
          for (const r of unfilledData) {
            if (r.time_key == yesterday) {
              objunfill['yesterday_data'] = r.unfilled_impressions;
            }

            if (r.time_key < yesterday && r.time_key >= sevenday) {
              objunfill['seven_days_average'] =
                objunfill['seven_days_average'] == undefined
                  ? r.unfilled_impressions
                  : objunfill['seven_days_average'] + r.unfilled_impressions;
            }
            if (r.time_key >= startOfMonth) {
              objunfill['current_month_data'] =
                objunfill['current_month_data'] == undefined
                  ? r.unfilled_impressions
                  : objunfill['current_month_data'] + r.unfilled_impressions;
            }
          }
          this.finalData = [];
          this.finalData.push(objrev, objecpm, objimp, objunfill);
          // console.log('finalData.......', this.finalData);
          this.finalData.forEach(r => {
            if (r.yesterday_data !== undefined || r.seven_days_average !== undefined || r.current_month_data !== undefined ||
              r.seven_days_average_vs_yesterday !== undefined) {
              this.isExport = true
            }
            if (
              r.yesterday_data != undefined &&
              r.seven_days_average != undefined
            ) {
              if (r.kpis == 'eCPM') {
                r.seven_days_average_vs_yesterday =
                  ((r.yesterday_data - r.seven_days_average) /
                    r.yesterday_data) *
                  100;
              } else {
                r.seven_days_average_vs_yesterday =
                  ((r.yesterday_data - r.seven_days_average / 7) /
                    r.yesterday_data) *
                  100;
              }
            }
            if (r.kpis == 'Revenue' || r.kpis == 'eCPM') {
              r.yesterday_data =
                r.yesterday_data != undefined
                  ? '$' +
                  Number(
                    parseFloat(r.yesterday_data).toFixed(2)
                  ).toLocaleString('en')
                  : 'N/A';

              if (r.seven_days_average != undefined) {
                if (r.kpis == 'eCPM') {
                  r.seven_days_average =
                    '$' +
                    Number(
                      parseFloat(r.seven_days_average).toFixed(2)
                    ).toLocaleString('en');
                } else {
                  const seven_days_average = r.seven_days_average / 7;
                  r.seven_days_average =
                    '$' +
                    Number(
                      parseFloat(seven_days_average.toString()).toFixed(2)
                    ).toLocaleString('en');
                }
              } else {
                r.seven_days_average = 'N/A';
              }

              r.current_month_data =
                r.current_month_data != undefined
                  ? '$' +
                  Number(
                    parseFloat(r.current_month_data).toFixed(2)
                  ).toLocaleString('en')
                  : 'N/A';
            } else {
              r.yesterday_data =
                r.yesterday_data != undefined
                  ? Number(r.yesterday_data).toLocaleString('en')
                  : 'N/A';

              if (r.seven_days_average != undefined) {
                const seven_days_average = parseInt(
                  (r.seven_days_average / 7).toString()
                );
                r.seven_days_average = Number(
                  seven_days_average
                ).toLocaleString('en');
              } else {
                r.seven_days_average = 'N/A';
              }
              r.current_month_data =
                r.current_month_data != undefined
                  ? Number(r.current_month_data).toLocaleString('en')
                  : 'N/A';
            }
            const obj = {};
            obj['data'] = r;
            arr.push(obj);
          });

          this.flatTableData = arr;
          this.flatTableJson['loading'] = false;
        });
      // });
    });
  }

  loadLostRevenueTableData(revenuetableReq) {
    this.revenueflatTableJson['loading'] = true;
    this.dataFetchServ.getTableData(revenuetableReq, this.isRevenuemgt).subscribe(data => {

      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      } else {
        this.noTableData = false;
      }

      // const data1 = data as [];
      const kpidata = data['data'];
      const data2 = data as [];

      const arr = [];

      for (const r of kpidata) {
        // let currdata = this.formatNumPipe.transform(
        //   r.dp_impressions,
        //   'number',
        //   []
        // );
        // r.dp_impressions = currdata;

        // let dd = r.lost_revenue_opportunity_seven_days.replace(',', '');
        // r.lost_revenue_opportunity_seven_days = dd;
        const obj = {};
        obj['data'] = r;
        arr.push(obj);
      }

      this.revenueflatTableData = <TreeNode[]>arr;
      this.revenueflatTableJson['loading'] = false;
    });
  }

  exportTable(table, fileFormat) {
    if (this.exportRequest['sendEmail'].length > 0) {
      if (table === 'flat') {
        if (this.isExport == false) {
          this.confirmationService.confirm({
            message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
            header: 'Information',
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
            },
            reject: () => {
            }
          });
        } else {
          this.toastService.displayToast({
            severity: 'info',
            summary: 'Export Report',
            detail:
              'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
            life: 10000
          });
          const columnDefs = this.flatTableColumnDef;
          const sheetDetailsarray = [];
          const sheetDetails = {};
          let col = this.libServ.deepCopy(columnDefs);
          const yesterday = moment(
            this.filtersApplied['timeKeyFilter']['time_key1'],
            'YYYYMMDD'
          )
            .subtract(1, 'day')
            .format('DD-MMM-YYYY');

          const Month = moment(
            this.filtersApplied['timeKeyFilter']['time_key1'],
            'YYYYMMDD'
          )
            .startOf('month')
            .format('MMM-YYYY');
          col.forEach(ele => {
            if (ele['field'] == 'yesterday_data') {
              ele['displayName'] = yesterday;
            }
            if (ele['field'] == 'current_month_data') {
              ele['displayName'] = Month;
            }
          });
          sheetDetails['columnDef'] = col;
          sheetDetails['data'] = this.finalData;
          sheetDetails['sheetName'] = 'Overall KPI Report Distribution';
          sheetDetails['isRequest'] = false;
          sheetDetails['request'] = {
            url: '',
            method: '',
            param: {
              timeKeyFilter: {
                time_key1: moment(
                  this.filtersApplied['timeKeyFilter']['time_key1'],
                  'YYYYMMDD'
                )
                  .subtract(30, 'day')
                  .format('YYYYMMDD'),
                time_key2: this.filtersApplied['timeKeyFilter']['time_key1']
              },
            }
          };
          sheetDetails['disclaimer'] = [
            {
              position: 'bottom',
              label: 'Note: Data present in the table may vary over a period of time.',
              color: '#000000'
            }
          ];
          sheetDetails['totalFooter'] = {
            available: false,
            custom: false
          };
          sheetDetails['tableTitle'] = {
            available: false,
            label: 'Overall KPI Report Distribution'
          };
          sheetDetails['image'] = [
            {
              available: true,
              path: environment.exportConfig.exportLogo,
              position: 'top'
            }
          ];
          sheetDetailsarray.push(sheetDetails);
          this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
          this.exportRequest['fileName'] =
            'Overall KPI Report Distribution ' +
            moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
          this.exportRequest['exportFormat'] = fileFormat;
          this.exportRequest['exportConfig'] = environment.exportConfig;
          this.dataFetchServ
            .getExportReportData(this.exportRequest)
            .subscribe(response => {
              console.log(response);
            });
        }

        // });
      } else if (table === 'lostRevenue') {
        if (this.revenueflatTableData.length == 0) {
          this.confirmationService.confirm({
            message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
            header: 'Information',
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
            },
            reject: () => {
            }
          });
        } else {
          this.toastService.displayToast({
            severity: 'info',
            summary: 'Export Report',
            detail:
              'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
            life: 10000
          });
          const columnDefs = this.revenueflatTableColumnDef;

          const req = this.libServ.deepCopy(this.revenuetableReq);
          req['limit'] = '';
          req['offset'] = '';
          req['isTable'] = false;

          const sheetDetailsarray = [];
          const sheetDetails = {};
          sheetDetails['columnDef'] = this.libServ.deepCopy(columnDefs);
          sheetDetails['data'] = [];
          sheetDetails['sheetName'] = 'Overall Lost Opportunity';
          sheetDetails['isRequest'] = true;
          sheetDetails['request'] = {
            url: '/api/flask/revmgmt/cassandradata',
            method: 'POST',
            param: req
          };
          sheetDetails['disclaimer'] = [
            {
              position: 'bottom',
              label: 'Note: Data present in the table may vary over a period of time.',
              color: '#000000'
            }
          ];
          sheetDetails['totalFooter'] = {
            available: false,
            custom: false
          };
          sheetDetails['tableTitle'] = {
            available: false,
            label: 'Overall Lost Opportunity'
          };
          sheetDetails['image'] = [
            {
              available: true,
              path: environment.exportConfig.exportLogo,
              position: 'top'
            }
          ];
          sheetDetailsarray.push(sheetDetails);
          this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
          this.exportRequest['fileName'] =
            'Overall Lost Opportunity Report Distribution ' +
            moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
          this.exportRequest['exportFormat'] = fileFormat;
          this.exportRequest['exportConfig'] = environment.exportConfig;
          this.dataFetchServ
            .getExportReportData(this.exportRequest)
            .subscribe(response => {
              console.log(response);
            });
        }
      }
    } else {
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Export Report',
        detail:
          'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email.Thanks!',
        life: 10000
      });
    }

    return false;
  }

  onFiltersApplied(filterData: object) {
    this.filtersApplied['filters']['dimensions'] = [];

    for (const k in filterData['filter']['dimensions']) {
      if (filterData['filter']['dimensions'][k].length !== 0) {
        this.filtersApplied['filters']['dimensions'].push({
          key: k,
          values: filterData['filter']['dimensions'][k]
        });
      }
    }

    this.filtersApplied['groupby'] = filterData['groupby'];

    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][0];
    this.data = this.filtersApplied['timeKeyFilter']['time_key1'];
    this.data1 = moment(
      this.filtersApplied['timeKeyFilter']['time_key1'],
      'YYYYMMDD'
    )
      .subtract(1, 'day')
      .format('DD MMM YY');
    this.startD = moment(
      this.filtersApplied['timeKeyFilter']['time_key1'],
      'YYYYMMDD'
    )
      .subtract(8, 'day')
      .format('DD MMM YY');
    this.endD = moment(
      this.filtersApplied['timeKeyFilter']['time_key1'],
      'YYYYMMDD'
    )
      .subtract(2, 'day')
      .format('DD MMM YY');

    this.loadKPITableData();
    this.revenuetableReq = {
      dimensions: ['source'],
      metrics: ['dp_revenue', 'dp_impressions'],
      derived_metrics: ['rev_cpm', 'seven_day_avg_ecpm', 'lost_avg_revenue'],
      timeKeyFilter: {
        time_key1: moment(
          this.filtersApplied['timeKeyFilter']['time_key1'],
          'YYYYMMDD'
        )
          .subtract(7, 'days')
          .format('YYYYMMDD'),
        time_key2: moment(
          this.filtersApplied['timeKeyFilter']['time_key1'],
          'YYYYMMDD'
        )
          .subtract(1, 'days')
          .format('YYYYMMDD')
      },
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [],
      limit: '',
      offset: ''
    };
    this.loadLostRevenueTableData(this.revenuetableReq);
  }
  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }

  onGlobalSearchChanged(searchValue, tableColDef) {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    this.timeout = setTimeout(() => {
      this.revenuetableReq['filters']['globalSearch'] = {
        dimensions: [],
        value: searchValue
      };
      tableColDef.forEach(element => {
        this.revenuetableReq['filters']['globalSearch']['dimensions'].push(
          element.field
        );
      });
      this.loadLostRevenueTableData(this.revenuetableReq);
    }, 3000);
  }

}

