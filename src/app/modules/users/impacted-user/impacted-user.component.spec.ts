import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImpactedUserComponent } from './impacted-user.component';

describe('ImpactedUserComponent', () => {
  let component: ImpactedUserComponent;
  let fixture: ComponentFixture<ImpactedUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImpactedUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImpactedUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
