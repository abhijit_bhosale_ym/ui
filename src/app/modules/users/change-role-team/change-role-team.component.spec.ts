import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeRoleTeamComponent } from './change-role-team.component';

describe('ChangeRoleTeamComponent', () => {
  let component: ChangeRoleTeamComponent;
  let fixture: ComponentFixture<ChangeRoleTeamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ChangeRoleTeamComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeRoleTeamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
