import { Component, OnInit, OnDestroy } from '@angular/core';
import { SelectItemGroup } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RoleService } from 'src/app/_services/users/role.service';
import { TeamService } from 'src/app/_services/users/team.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { UsersService } from 'src/app/_services';
import { TeamDetailsComponent } from '../team-details/team-details.component';
import { RoleDetailsComponent } from '../role-details/role-details.component';
// import { Subscription } from 'rxjs';
import { CommonLibService } from 'src/app/_services';
// import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ImpactedUserComponent } from './../impacted-user/impacted-user.component';
@Component({
  selector: 'ym-change-role-team',
  templateUrl: './change-role-team.component.html',
  styleUrls: ['./change-role-team.component.scss'],
  providers: [DialogService]
})
export class ChangeRoleTeamComponent implements OnInit {
  // private appConfigObs: Subscription;

  appConfig: object = {};
  registerForm: FormGroup;
  isCreateRole = false;
  isCreateTeam = false;
  groupedRoles: SelectItemGroup[];
  groupedTeams: SelectItemGroup[];
  groupedApps: SelectItemGroup[];
  userDetails: {
    name: '';
  };
  constructor(
    private formBuilder: FormBuilder,
    public router: Router,
    private roleServ: RoleService,
    private teamServ: TeamService,
    private userService: UsersService,
    private dialogService: DialogService,
    private toastService: ToastService,
    // private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private config: DynamicDialogConfig,
    private dynamicDialogRef: DynamicDialogRef
  ) {
    this.userDetails = this.config.data;
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      role: ['', [Validators.required]],
      team: ['', [Validators.required]],
      default_app: ['', [Validators.required]]
    });

    this.loadRolesAndTeams();
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  loadRolesAndTeams() {
    this.roleServ.getRoles().subscribe(res => {
      this.groupedRoles = res['data'];

      this.registerForm.controls.role.setValue(
        this.groupedRoles.find(x => x['id'] == this.userDetails['role']['id'])
      );
      const role = this.registerForm.controls.role.value;
      this.roleServ.getRoleDetails(role['id']).subscribe(res => {
        this.groupedApps = res['data']['permissions']
          .filter(e => e.permissionType === 'Application')
          .map(e => {
            e['id'] = e['appId'];
            return e;
          });
        this.registerForm.controls.default_app.setValue(
          this.groupedApps.find(x => x['id'] == this.userDetails['defaultApp'])
        );
      });
      this.teamServ.getTeamsAssociatedWithRole(role['id']).subscribe(res => {
        this.groupedTeams = res['data'];
        this.registerForm.controls.team.setValue(
          this.groupedTeams.find(x => x['id'] == this.userDetails['team']['id'])
        );
      });
    });
  }

  roleChanged() {
    const role = this.registerForm.controls.role.value;
    this.roleServ.getRoleDetails(role['id']).subscribe(res => {
      this.groupedApps = res['data']['permissions']
        .filter(e => e.permissionType === 'Application')
        .map(e => {
          e['id'] = e['appId'];
          return e;
        });
      this.registerForm.controls.default_app.reset();
    });
    this.teamServ.getTeamsAssociatedWithRole(role['id']).subscribe(res => {
      this.groupedTeams = res['data'];
      this.registerForm.controls.team.reset();
    });
  }

  roleDetails() {
    const ref = this.dialogService.open(RoleDetailsComponent, {
      header: ' Role Details ',
      contentStyle: { 'max-height': '80vh', width: '80vw', overflow: 'auto' },
      data: this.registerForm.value.role
    });
    ref.onClose.subscribe((data1: string) => { });
  }

  teamDetails() {
    const ref = this.dialogService.open(TeamDetailsComponent, {
      header: ' Team Details ',
      contentStyle: { 'max-height': '80vh', width: '60vw', overflow: 'auto' },
      data: this.registerForm.value.team
    });
    ref.onClose.subscribe((data1: string) => { });
  }

  onSubmit() {
    // stop here if form is invalid
    console.log('userdetails', this.userDetails, this.registerForm.value);
    let changeStatus = 'nochange';
    if (
      this.userDetails['role']['id'] === this.registerForm.value.role.id &&
      this.userDetails['team']['id'] === this.registerForm.value.team.id &&
      this.userDetails['defaultApp'] === this.registerForm.value.default_app.id
    ) {
      changeStatus = 'nochange';
    } else if (
      this.userDetails['role']['id'] === this.registerForm.value.role.id &&
      this.userDetails['team']['id'] === this.registerForm.value.team.id
    ) {
      changeStatus = 'changedefualtapp';
    } else {
      changeStatus = 'changeall';
    }
    if (changeStatus === 'nochange') {
      this.toastService.displayToast({
        severity: 'warning',
        summary: 'Change Role/Team',
        detail: 'Please change any fields'
      });
      return;
    }

    const req = {
      userid: this.userDetails['id'],
      roleid: this.registerForm.value.role.id,
      teamid: this.registerForm.value.team.id,
      default_app: this.registerForm.value.default_app.id
    };
    if (changeStatus === 'changedefualtapp') {
      this.userService.updateUserRoleTeam(req).subscribe(res => {
        if (res['status']) {
          this.toastService.displayToast({
            severity: 'success',
            summary: 'Change Role/Team',
            detail: 'Change defualt app successfully'
          });
          // this.router.navigate(['/users']);
          this.dynamicDialogRef.close();
        } else {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Something went wrong',
            detail: 'Error Changing Role/Team'
          });
        }
      });
    } else {
      const siblingReq = {
        owner_team_id: this.userDetails['team']['id'],
        owner_role_id: this.userDetails['role']['id']
      };
      this.userService.getSiblingUser(siblingReq).subscribe(siblingRes => {
        console.log('siblingRes', siblingRes);
        if (siblingRes['data'].length > 1) {

          this.userService.updateUserRoleTeam(req).subscribe(res => {
            if (res['status']) {
              this.toastService.displayToast({
                severity: 'success',
                summary: 'Change Role/Team',
                detail: 'Change defualt app successfully'
              });
              // this.router.navigate(['/users']);
              this.userDetails['team'] = this.registerForm.value.team;
              this.userDetails['role'] = this.registerForm.value.role;
              this.dynamicDialogRef.close();
            } else {
              this.toastService.displayToast({
                severity: 'error',
                summary: 'Something went wrong',
                detail: 'Error Changing Role/Team'
              });
            }
          });
        } else {
          const impactedReq = {
            team_id: this.userDetails['team']['id'],
            role_id: this.userDetails['role']['id']
          };
          this.userService.getImpactedUser(impactedReq).subscribe(res => {
            const impactedUser = res;
            console.log('response impact user---', res);
            if (impactedUser['data'].length > 0) {
              const data = {
                data: impactedUser['data'],
              };
              this.dynamicDialogRef.close();
              const ref = this.dialogService.open(ImpactedUserComponent, {
                header: 'Impacted User List',
                contentStyle: {
                  'min-height': '65vh',
                  'max-height': '65vh',
                  width: '60vw',
                  overflow: 'auto'
                },
                data: data
              });
              ref.onClose.subscribe((data: string) => {
                console.log('data', data);
                if (data) {
                  const updateImpactedreq = {
                    team_id: this.userDetails['team']['id'],
                    role_id: this.userDetails['role']['id']
                  };

                  this.userService.updateImpactedUser(updateImpactedreq).subscribe(res => {
                    this.userService.updateUserRoleTeam(req).subscribe(res => {
                      if (res['status']) {
                        this.toastService.displayToast({
                          severity: 'success',
                          summary: 'Change Role/Team',
                          detail: 'Change defualt app successfully'
                        });
                        // this.router.navigate(['/users']);
                        this.userDetails['team'] = this.registerForm.value.team;
                        this.userDetails['role'] = this.registerForm.value.role;
                        this.dynamicDialogRef.close(null);
                        // this.dialogService.dialogComponentRef.destroy();

                      } else {
                        this.toastService.displayToast({
                          severity: 'error',
                          summary: 'Something went wrong',
                          detail: 'Error Changing Role/Team'
                        });
                      }
                    });
                  });
                } else {

                }
              });
            } else {
              this.userService.updateUserRoleTeam(req).subscribe(res => {
                if (res['status']) {
                  this.toastService.displayToast({
                    severity: 'success',
                    summary: 'Change Role/Team',
                    detail: 'Change defualt app successfully'
                  });
                  // this.router.navigate(['/users']);
                  this.userDetails['team'] = this.registerForm.value.team;
                  this.userDetails['role'] = this.registerForm.value.role;
                  this.dynamicDialogRef.close();
                } else {
                  this.toastService.displayToast({
                    severity: 'error',
                    summary: 'Something went wrong',
                    detail: 'Error Changing Role/Team'
                  });
                }
              });
            }
          });
        }
      });

      // this.userService
      //   .getImpactedUser(this.userDetails['id'])
      //   .subscribe(res => {
      //     // this.impactedUser = res;
      //     console.log('impacted user---', res);
      //   });
      // setTimeout(() => {
      //   const data = {
      //     status: this.impactedUser['status'],
      //     request: this.req,
      //     msg: toastMsg,
      //     owner_team_id: user['ownerTeamId'],
      //     owner_role_id: user['ownerRoleId']
      //   };

      //   if (!user.status && this.impactedUser['status'].length > 0) {
      //     const ref = this.dialogService.open(ImpactedUserComponent, {
      //       header: 'Impacted User List',
      //       contentStyle: {
      //         'min-height': '65vh',
      //         'max-height': '65vh',
      //         width: '60vw',
      //         overflow: 'auto'
      //       },
      //       data: data
      //     });
      //     ref.onClose.subscribe((data: string) => {});
      //   } else {
      //     this.usersService.updateStatus(this.req).subscribe(res => {
      //       console.log('response---', res);
      //       if (res['status'] === 0) {
      //         this.toastService.displayToast({
      //           severity: 'error',
      //           summary: 'Server Error',
      //           detail: 'Please refresh the page'
      //         });
      //         console.log(res['status_msg']);
      //         return;
      //       } else {
      //         this.toastService.displayToast({
      //           severity: 'success',
      //           summary: 'User status',
      //           detail: toastMsg
      //         });
      //       }
      //     });
      //   }
      // }, 1000);
    }
  }

  goBack() {
    // this.router.navigate(['/users']);
    this.dynamicDialogRef.close();
  }
  // ngOnDestroy() {
  //   if (this.appConfigObs && !this.appConfigObs.closed) {
  //     this.appConfigObs.unsubscribe();
  //   }
  // }
}
