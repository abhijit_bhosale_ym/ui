import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'ym-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent {
  constructor(public router: Router) {}

  onSubmit() {}

  goBack() {
    this.router.navigate(['/users']);
  }
}
