import {
  Component,
  OnInit,
  OnDestroy,
  Output,
  EventEmitter
} from '@angular/core';
import { IUser } from 'src/app/_interfaces/users';
import { UsersService } from '../../_services';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import {
  Router,
  ActivatedRoute,
  NavigationExtras,
  NavigationEnd,
  RouterEvent
} from '@angular/router';

import { ConfirmationService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { RoleDetailsComponent } from './role-details/role-details.component';
import { TeamDetailsComponent } from './team-details/team-details.component';
import { Title } from '@angular/platform-browser';
import { ChangeRoleTeamComponent } from './change-role-team/change-role-team.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ImpersonateService } from 'src/app/_services/impersonate/impersonate.service';
import {
  filter,
  takeUntil,
  pairwise,
  startWith,
  timeout
} from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Subscription } from 'rxjs';
import { CommonLibService } from 'src/app/_services';
import { ImpactedUserComponent } from './impacted-user/impacted-user.component';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { log } from 'util';
import { AuthService } from '../../_services';

@Component({
  selector: 'ym-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, OnDestroy {
  @Output() changed = new EventEmitter<boolean>();
  private appConfigObs: Subscription;
  isCreateUser = false;
  isChangesRoleTeam = false;

  appConfig: object = {};
  users: IUser[];
  columns: any[];
  status: boolean;
  public destroyed = new Subject<any>();
  req: object;
  impactedUser: {};
  checked: boolean;

  constructor(
    private toastService: ToastService,
    private usersService: UsersService,
    private _titleService: Title,
    private router: Router,
    private dialogService: DialogService,
    private impersonateServ: ImpersonateService,
    private currentActivatedRoute: ActivatedRoute,
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private confirmationService: ConfirmationService,
    private authService: AuthService
  ) {
    this.router.events
      .pipe(
        filter((event: RouterEvent) => event instanceof NavigationEnd),
        pairwise(),
        filter((events: RouterEvent[]) => events[0].url === events[1].url),
        startWith('Initial call'),
        takeUntil(this.destroyed)
      )

      .subscribe(() => {
        // If it is a NavigationEnd event re-initalise the component
        this.usersService.getUsersList().subscribe(res => {
          console.log('res-----', res);

          this.users = <IUser[]>res['data'];
        });
      });
  }

  ngOnInit() {
    this.columns = [
      {
        field: 'name',
        header: 'Name',
        width: '180'
      },
      {
        field: 'email',
        header: 'Email',
        width: '180'
      },
      // {
      //   field: 'contact_email',
      //   header: 'Contact Email',
      //   width: '245'
      // },

      // {
      //   field: 'status',
      //   header: 'Status',
      //   width: '100'
      // },
      // {
      //   field: 'edit',
      //   header: 'Edit',
      //   width: '100'
      // },
      {
        field: 'role.name',
        header: 'Role',
        width: '160'
      },
      {
        field: 'team.name',
        header: 'Team',
        width: '160'
      },
      {
        field: 'impersonate',
        header: 'Impersonate',
        width: '90'
      },
      // {
      //   field: 'change',
      //   header: 'Edit',
      //   width: '100'
      // },
      {
        field: 'resetPassword',
        header: 'Reset Password',
        width: '80'
      },
      {
        field: 'clearSession',
        header: 'Clear Session',
        width: '70'
      },
      {
        field: 'parentUserName',
        header: 'Created By',
        width: '125'
      },
      {
        field: 'createdAt',
        header: 'Created At',
        width: '150'
      },
      {
        field: 'lastLoginAt',
        header: 'Last Login',
        width: '150'
      },
      {
        field: 'status',
        header: 'Status',
        width: '100'
      }
    ];
    // this.usersService.getUsersList().subscribe(res => {
    //   this.users = <IUser[]>res['data'];
    // });
    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        console.log('appConfig', this.appConfig);
        this._titleService.setTitle(this.appConfig['displayName']);
        this.isCreateUser = this.appConfig['permissions'].some(
          o => o.name === 'um-create-user'
        );
        this.isChangesRoleTeam = this.appConfig['permissions'].some(
          o => o.name === 'um-update-role-team'
        );
        if (!((this.columns.some(x => x.field === 'change')))) {
          this.isChangesRoleTeam ? this.columns.splice(5, 0, { field: 'change', header: 'Edit', width: '100' }) : '';
        }
        // this.isCreateTeam = this.appConfig['permissions'].some(
        //   o => o.name == 'um-create-team'
        // );
      }
    });
  }

  emailString(mailId: string) {
    return `mailto:${mailId}`;
  }

  statusChanged(e: Event, user: IUser) {
    console.log('checked', e['checked']);
    console.log('user', user);

    // CONFIRM DEACTIVATE/ACTIVATE
    // const ref = this.dialogService.open(UserDetailsComponent, {
    //   header: ' User Details ',
    //   contentStyle: { 'max-height': '80vh', width: '80vw', overflow: 'auto' },
    //   data: user
    // });
    // ref.onClose.subscribe((data1: string) => {});
  }

  addUser() {
    this.router.navigate(['/users/add-user'], {
      relativeTo: this.currentActivatedRoute
    });
  }

  editUser(user) {
    const navExtras: NavigationExtras = {
      queryParams: user
    };

    // this.router.navigate(['/users/edit-user'], navExtras);
  }

  roleDetails(role) {
    const ref = this.dialogService.open(RoleDetailsComponent, {
      header: ' Role Details ',
      contentStyle: {
        'min-height': '80vh',
        'max-height': '80vh',
        width: '60vw',
        overflow: 'auto'
      },
      data: role
    });
    ref.onClose.subscribe((data1: string) => { });
  }

  teamDetails(team) {
    const ref = this.dialogService.open(TeamDetailsComponent, {
      header: ' Team Details ',
      contentStyle: {
        'min-height': '65vh',
        'max-height': '65vh',
        width: '60vw',
        overflow: 'auto'
      },
      data: team
    });
    ref.onClose.subscribe((data1: string) => { });
  }

  changeRoleTeam(user) {
    const ref = this.dialogService.open(ChangeRoleTeamComponent, {
      header: ' Change Role/Team Details ',
      contentStyle: {
        'min-height': '65vh',
        'max-height': '65vh',
        width: '50vw',
        overflow: 'auto'
      },
      data: user
    });
    ref.onClose.subscribe((data1: string) => {
      ref.close();
    });
  }

  resetPassword(user) {
    const ref = this.dialogService.open(ResetPasswordComponent, {
      header: ' Reset Password ',
      contentStyle: {
        'min-height': '20vh',
        'max-height': '75vh',
        width: '50vw',
        overflow: 'auto'
      },
      data: user
    });
    ref.onClose.subscribe((data1: string) => { });


  }
  decreaseSearchWidth(width) {
    return width - 30;
  }

  impersonate(user) {
    this.impersonateServ.startImpersonate(user);
  }

  activateDeactivate(user) {
    console.log('users', user);

    this.req = {
      status: user.status,
      id: user.id,
      email: user.email
    };
    const msg = `Do you want to ${user.status ? 'Activate' : 'Deactivate'} user?`;
    const toastMsg = `User ${
      user.status ? 'Activated' : 'Deactivated'
      } successfully.`;
    this.confirmationService.confirm({
      message: msg,
      accept: () => {
        const siblingReq = {
          owner_team_id: user.team.id,
          owner_role_id: user.role.id
        };
        if (user.status) {
          this.usersService.updateStatus(this.req).subscribe(res => {
            console.log('response direct active', res);
            if (res['status'] === 0) {
              user.status = !user.status;
              this.toastService.displayToast({
                severity: 'error',
                summary: 'Server Error',
                detail: 'Please refresh the page'
              });
              console.log(res['status_msg']);
              return;
            } else {

              this.toastService.displayToast({
                severity: 'success',
                summary: 'User status',
                detail: toastMsg
              });
            }

            // this.dialogService.dialogComponentRef.destroy();
          });
        } else {
          this.usersService.getSiblingUser(siblingReq).subscribe(res => {
            console.log('response sibling .....', res);
            if (res['data'].length > 1) {

              this.usersService.updateStatus(this.req).subscribe(res => {
                console.log('response sibling update---', res);
                if (res['status'] === 0) {
                  user.status = !user.status;
                  this.toastService.displayToast({
                    severity: 'error',
                    summary: 'Server Error',
                    detail: 'Please refresh the page'
                  });
                  console.log(res['status_msg']);
                  return;
                } else {

                  this.toastService.displayToast({
                    severity: 'success',
                    summary: 'User status',
                    detail: toastMsg
                  });
                }

                // this.dialogService.dialogComponentRef.destroy();
              });
            } else {
              const impactedReq = {
                team_id: user.team.id,
                role_id: user.role.id
              };
              this.usersService.getImpactedUser(impactedReq).subscribe(res => {
                this.impactedUser = res;
                console.log('response impact user---', res);
                if (this.impactedUser['data'].length > 0) {
                  const data = {
                    data: this.impactedUser['data']

                  };
                  const ref = this.dialogService.open(ImpactedUserComponent, {
                    header: 'Impacted User List',
                    contentStyle: {
                      'min-height': '65vh',
                      'max-height': '65vh',
                      width: '60vw',
                      overflow: 'auto'
                    },
                    data: data
                  });
                  ref.onClose.subscribe((data: string) => {
                    console.log('data', data);
                    if (data) {
                      const req = {
                        team_id: user.team.id,
                        role_id: user.role.id
                      };

                      this.usersService.updateImpactedUser(req).subscribe(res => {
                        this.usersService.updateStatus(this.req).subscribe(res => {
                          if (res['status'] === 0) {
                            user.status = !user.status;
                            this.toastService.displayToast({
                              severity: 'error',
                              summary: 'Server Error',
                              detail: 'Please refresh the page'
                            });
                            console.log(res['status_msg']);
                            return;
                          } else {
                            this.router.navigate(['/users']);
                            this.toastService.displayToast({
                              severity: 'success',
                              summary: 'User status',
                              detail: toastMsg
                            });
                          }

                          // this.dialogService.dialogComponentRef.destroy();
                        });
                      });
                    } else {
                      user.status = !user.status;
                    }
                  });
                } else {
                  this.usersService.updateStatus(this.req).subscribe(res => {
                    console.log('response---', res);
                    if (res['status'] === 0) {
                      user.status = !user.status;
                      this.toastService.displayToast({
                        severity: 'error',
                        summary: 'Server Error',
                        detail: 'Please refresh the page'
                      });
                      console.log(res['status_msg']);
                      return;
                    } else {

                      this.toastService.displayToast({
                        severity: 'success',
                        summary: 'User status',
                        detail: toastMsg
                      });
                    }
                  });
                }
              });
            }
          });
        }
      },
      reject: () => {
        user.status = !user.status;
      }
    });
  }

  clearSession(user) {
    this.req = {
      email: user.email
    };
    this.confirmationService.confirm({
      message: 'Do you want to clear session?',
      accept: () => {
        this.usersService.clearSession(this.req).subscribe(res => {
          if (res['status'] === 0) {
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Server Error',
              detail: 'Please refresh the page'
            });
            console.log(res['status_msg']);
            return;
          } else {
            this.toastService.displayToast({
              severity: 'success',
              summary: 'User Clear Session',
              detail: 'User Session Clear Successfully'
            });
          }
        });
      },
      reject: () => {
      }
    });
  }
  ngOnDestroy() {
    this.destroyed.next();
    this.destroyed.complete();
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }
}
