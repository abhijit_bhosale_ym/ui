import { Component, OnInit } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { RoleService } from 'src/app/_services/users/role.service';

interface IRole {
  id: number;
  name: string;
  ownerRoleId: number;
  ownerUserId: number;
  description: string;
  createdAt: number;
  updatedAt: number;
}

interface IRoleDetails {
  role: IRole;
  permissions: [];
}

@Component({
  selector: 'ym-role-details',
  templateUrl: './role-details.component.html',
  styleUrls: ['./role-details.component.scss']
})
export class RoleDetailsComponent implements OnInit {
  private role: object;
  public roleDetails: IRoleDetails;
  public detailsLoaded = false;
  permissionsColumnDef: any[];

  constructor(
    private config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private roleServ: RoleService
  ) {
    this.role = this.config.data;
  }

  ngOnInit() {
    this.roleServ.getRoleDetails(this.role['id']).subscribe(res => {
      this.roleDetails = res['data'];
      this.detailsLoaded = true;
    });

    this.permissionsColumnDef = [
      {
        field: 'displayName',
        header: 'Name',
        width: '175'
      },
      {
        field: 'appDisplayName',
        header: 'Application',
        width: '175'
      },
      {
        field: 'permissionType',
        header: 'Permission Type',
        width: '140'
      },
      {
        field: 'description',
        header: 'Description',
        width: '200'
      }
    ];
  }

  decreaseSearchWidth(width) {
    return width - 30;
  }
}
