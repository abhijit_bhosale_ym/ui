import { Component, OnInit } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { TeamService } from 'src/app/_services/users/team.service';
import { SearchStringPipe } from 'src/app/_pipes/search-string.pipe';
import { environment } from 'src/environments/environment';

interface ITeamDetails {
  id: number;
  name: string;
  ownerRoleId: number;
  ownerTeamId: number;
  ownerUserId: number;
  resources: string;
  createdAt: number;
  updatedAt: number;
}

@Component({
  selector: 'ym-team-details',
  templateUrl: './team-details.component.html',
  styleUrls: ['./team-details.component.scss']
})
export class TeamDetailsComponent implements OnInit {
  public team: ITeamDetails;
  public detailsLoaded = false;
  public resources: any[];
  defaultDimensions = [];
  private BASE_URL: string = environment.baseUrl;
  constructor(
    private config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private teamServ: TeamService,
    private searchStringPipe: SearchStringPipe
  ) {
    this.team = this.config.data;
  }

  ngOnInit() {
    console.log('team data---', this.config.data);
    this.defaultDimensions = [
      {
        label: 'Payout Resources',
        key: 'payout_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/common/filter`,
        enable: false,
        permissionNames: [
          'Publisher Payouts',
          'Frankly Payout',
          'Revenue Management 360',
          '3P Statements',
          'analysis_report',
          'Revenue Dashboard',
          'Finance Audit',
          'payment-contact-info'
        ],
        value: [
          {
            label: 'Station Group',
            filter_key: 'dim_station_group',
            key: 'station_group',
            config_json: null,
            value: ['User have access to All Station Group']
          },
          {
            label: 'Station',
            filter_key: 'dim_payout_station',
            key: 'derived_station_rpt',
            config_json: null,
            value: ['User have access to All Station']
          },
          {
            label: 'Source',
            filter_key: 'dim_payout_source',
            key: 'source',
            config_json: null,
            value: ['User have access to All Source']
          }
        ]
      },
      {
        label: 'Revenue Resources',
        key: 'revenue_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/revmgmt/filter`,
        enable: false,
        permissionNames: ['Revenue Management'],
        value: [
          {
            label: 'Source',
            key: 'rev_source',
            filter_key: 'dim_rev_source',
            config_json: null,
            value: ['User have access to All Source']
          }
        ]
      },
      {
        label: 'Unfilled Resources',
        key: 'unfilled_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/unfilled-inventory/filter`,
        enable: false,
        permissionNames: [
          'Unfilled',
          'analysis_report',
          'Revenue Dashboard'
        ],
        value: [
          {
            label: 'Stations',
            key: 'station',
            filter_key: 'dim_unfilled_station',
            config_json: null,
            value: ['User have access to All Unfilled Resources']
          }
        ]
      },
      {
        label: 'Vast Resources',
        key: 'vast_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/vast-errors/filter`,
        enable: false,
        permissionNames: [
          'Vast Errors',
          'analysis_report',
          'Revenue Dashboard'
        ],
        value: [
          {
            label: 'Advertiser',
            key: 'advertiser',
            filter_key: 'dim_advertiser',
            config_json: null,
            value: ['User have access to All Advertiser']
          }
        ]
      },
      {
        label: 'CPR Resources',
        key: 'cpr_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/campaign-progress-report/filter`,
        enable: false,
        permissionNames: ['CPR'],
        value: [
          {
            label: 'Order',
            key: 'order_name',
            filter_key: 'dim_order_name',
            config_json: null,
            value: ['User have access to All Order']
          },
          {
            label: 'Line Item',
            key: 'lineitem',
            filter_key: 'dim_line_item',
            config_json: null,
            value: ['User have access to All Line Item']
          }
        ]
      },
      {
        label: 'Revenue Resources Freecycle',
        key: 'revenue_resources_fc',
        filterUrl: `${this.BASE_URL}/freecycle/v1/revmgmt-fc/filter`,
        enable: false,
        permissionNames: ['Revenue Management FreeCycle',
        'Analysis Report FreeCycle',
        'Revenue Dashboard FreeCycle'],
        value: [
          {
            label: 'Source',
            key: 'source',
            filter_key: 'dim_source',
            config_json: null,
            value: ['User have access to All Source']
          }
        ]
      },
      {
        label: 'Unfilled Resources Freecycle',
        key: 'unfilled_resources_fc',
        filterUrl: `${this.BASE_URL}/freecycle/v1/unfilled-inventory-fc/filter`,
        enable: false,
        permissionNames: [
          'Unfilled Inventory FreeCycle',
          'Analysis Report FreeCycle',
          'Revenue Dashboard FreeCycle'
        ],
        value: [
          {
            label: 'Ad Unit',
            key: 'ad_unit',
            filter_key: 'dim_ad_unit',
            config_json: null,
            value: ['User have access to All Line Item']
          }
        ]
      },
      {
        label: 'Prebid Resources',
        key: 'prebid_publisher_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/prebid/filter`,
        enable: false,
        permissionNames: ['Prebid Analytics'],
        value: [
          {
            label: 'Publisher Name',
            key: 'publisher_name',
            filter_key: 'dim_prebid_publisher',
            config_json: null,
            value: 'Publisher Name'
          }
        ]
      },
      {
        label: 'ASBN Programmatic Revenue Resources',
        key: 'asbn_prog_rev_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/asbn-revmgmt/filter`,
        enable: false,
        permissionNames: [
          'Revenue Management Asbn'
        ],
        value: [
          {
            label: 'Source',
            filter_key: 'dim_prog_rev_source',
            key: 'source',
            config_json: null,
            value: 'Source'
          }
        ]
      },
      {
        label: 'ASBN Revenue Management Resources',
        key: 'asbn_rev_360_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/asbn-rev360/filter`,
        enable: false,
        permissionNames: [
          'Revenue Management 360 Asbn'
        ],
        value: [
          {
            label: 'Station Group',
            filter_key: 'dim_rev_360_media_group',
            key: 'media_group',
            config_json: null,
            value: []
          },
          {
            label: 'Station',
            filter_key: 'dim_rev_360_property',
            key: 'property',
            config_json: null,
            value: []
          },
          {
            label: 'Source',
            filter_key: 'dim_rev_360_source',
            key: 'source',
            config_json: null,
            value: []
          }
        ]
      }
    ];




    if (typeof this.team.resources !== 'string') {
      this.team.resources = JSON.stringify(this.team.resources);
    }
    let resources: any = [];
    try {
      resources = JSON.parse(this.team.resources);
    } catch (error) {
      resources = '';
      console.log(error);
    }
    if (resources !== '') {
      if (resources.length) {
        this.resources = JSON.parse(this.team.resources).filter(
          v => v.value.length > 0
        );
        console.log('res--', this.resources);
        this.resources.forEach(resource => {
          this.defaultDimensions.forEach(dim => {
            dim.value.forEach(element => {
              // value.forEach(element => {
              if (element['filter_key'] == resource['key']) {
                dim.enable = true;
                element['value'] = resource['value'];
              }
              // });
            });
          });
        });


      } else {
        this.resources = [];
        this.defaultDimensions.forEach(dim => {
              dim.enable = true;
        });
      }
      // else {
      //   this.resources = [
      //     {
      //       key: 'dim_payout_source',
      //       display_name: 'Payout Source',
      //       value: [
      //         'User have access to All Payout Sources'
      //       ]
      //     },
      //     {
      //       key: 'dim_rev_source',
      //       display_name: 'RevOps Source',
      //       value: [
      //         'User have access to All RevOps Sources'
      //       ]
      //     },
      //     {
      //       key: 'dim_payout_station',
      //       display_name: 'Payout Station',
      //       value: [
      //         'User have access to All Payout Station'
      //       ]
      //     },
      //     {
      //       key: 'dim_unfilled_station',
      //       display_name: 'Unfilled Station',
      //       value: [
      //         'User have access to All Unfilled Stations'
      //       ]
      //     },
      //     {
      //       key: 'dim_order_name',
      //       display_name: 'Order',
      //       value: [
      //         'User have access to All Orders'
      //       ]
      //     },
      //     {
      //       key: 'dim_line_item',
      //       display_name: 'Line Item',
      //       value: [
      //         'User have access to All Line Items'
      //       ]
      //     },
      //     {
      //       key: 'dim_advertiser',
      //       display_name: 'Advertiser',
      //       value: [
      //         'User have access to All Advertisers'
      //       ]
      //     },
      //     {
      //       key: 'dim_station_group',
      //       display_name: 'Station group',
      //       value: [
      //         'User have access to All Station Groups'
      //       ]
      //     }
      //   ];
      // }
    } else {
      this.resources = [];
      this.defaultDimensions.forEach(dim => {
        dim.enable = true;
  });
    }


    this.detailsLoaded = true;



    this.defaultDimensions = this.defaultDimensions.filter(x => x.enable);
    // Instead of calling getTeamDetails API, using platformConfig
    // this.teamServ.getTeamDetails(this.team['id']).subscribe(res => {
    //   this.team = res['data'];
    //   if (JSON.parse(this.team.resources) !== '') {
    //     this.team.resources = JSON.parse(this.team.resources).filter(
    //       v => v.value.length > 0
    //     );
    //   } else {
    //     this.team['resources'] = '';
    //   }
    //   this.detailsLoaded = true;
    // });
  }
}
