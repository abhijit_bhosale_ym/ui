import { Component, OnInit, OnDestroy } from '@angular/core';
import { SelectItemGroup, MenuItem, TreeNode } from 'primeng/api';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { RoleService } from 'src/app/_services/users/role.service';
import { TeamService } from 'src/app/_services/users/team.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { UsersService, PlatformConfigService } from 'src/app/_services';
import { TeamDetailsComponent } from '../team-details/team-details.component';
import { RoleDetailsComponent } from '../role-details/role-details.component';
import { Subscription } from 'rxjs';
import { CommonLibService } from 'src/app/_services';
import { environment } from 'src/environments/environment';
import { DialogService } from 'primeng';
@Component({
  selector: 'ym-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;
  private BASE_URL: string = environment.baseUrl;

  appConfig: object = {};
  userFlow: MenuItem[];
  activeStepIdx: any;
  isCreateRole = false;
  isCreateTeam = false;
  isRoleExists = false;
  isTeamExists = false;
  userdetails: object;
  userRegisterForm: FormGroup;

  roleRegisterForm: FormGroup;
  groupedRoles: SelectItemGroup[];
  childRoles: object;
  selectedRole: object;
  formRoleModel = {
    roleName: '',
    roleDescription: '',
    selectedPermissions: []
  };
  customRole: boolean;
  noPermissionsFlag = false;
  permCols: any[];
  permissionsData: TreeNode[];

  teamRegisterForm: FormGroup;
  groupedTeams: SelectItemGroup[];
  customTeam: boolean;
  formTeamModel = {
    teamName: '',
    selectedDims: []
  };
  defaultDimensions = [];
  availableDimensions = [];

  defaultAppRegisterForm: FormGroup;
  groupedApps: SelectItemGroup[];

  constructor(
    private formBuilder: FormBuilder,
    public router: Router,
    private roleServ: RoleService,
    private teamServ: TeamService,
    private userService: UsersService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private platformConfigService: PlatformConfigService,
    private libServ: CommonLibService
  ) { }

  ngOnInit() {
    this.userFlow = [
      {
        label: 'Fill User Details',
        command: (event: any) => {
          this.activeStepIdx = 0;
        }
      },
      {
        label: 'Fill Roles Details',
        command: (event: any) => {
          this.activeStepIdx = 1;
        }
      },
      {
        label: 'Fill Team Details',
        command: (event: any) => {
          this.activeStepIdx = 2;
        }
      },
      {
        label: 'Select Default App',
        command: (event: any) => {
          this.activeStepIdx = 3;
        }
      }
    ];

    this.activeStepIdx = 0;

    this.userRegisterForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      contact_email: ['']
    });
    this.roleRegisterForm = this.formBuilder.group({
      role: ['', [Validators.required]]
    });
    this.teamRegisterForm = this.formBuilder.group({
      team: ['', [Validators.required]]
    });
    this.defaultAppRegisterForm = this.formBuilder.group({
      default_app: ['', [Validators.required]]
    });
    this.customRole = false;
    this.customTeam = false;
    this.appConfigObs = this.platformConfigService.obsConfig.subscribe(res => {
      if (!this.libServ.isEmptyObj(res)) {
        const appGroups = res['user']['role']['appGroups'];
        for (const appGrpIdx of Object.keys(appGroups)) {
          for (const appIdx of Object.keys(
            appGroups[appGrpIdx]['apps']
          )) {
            if (
              appGroups[appGrpIdx]['apps'][appIdx]['route'] === `/${window.location.pathname.split('/')[1]}`
            ) {
              this.appConfig = Object.assign({}, appGroups[appGrpIdx]['apps'][appIdx], { user: res['user'] });
            }
          }
        }
        this.isCreateRole = this.appConfig['permissions'].some(
          o => o.name === 'um-create-role'
        );
        this.isCreateTeam = this.appConfig['permissions'].some(
          o => o.name === 'um-create-team'
        );
      }
    });

    this.defaultDimensions = [
      {
        label: 'Payout Resources',
        key: 'payout_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/common/filter`,
        enable: false,
        permissionNames: [
          'Publisher Payouts',
          'Frankly Payout',
          'Revenue Management 360',
          '3P Statements',
          'analysis_report',
          'Revenue Dashboard',
          'Finance Audit',
          'payment-contact-info'
        ],
        value: [
          {
            label: 'Station Group',
            filter_key: 'dim_station_group',
            key: 'station_group',
            config_json: null,
            value: 'Station Group'
          },
          {
            label: 'Station',
            filter_key: 'dim_payout_station',
            key: 'derived_station_rpt',
            config_json: null,
            value: 'Station'
          },
          {
            label: 'Source',
            filter_key: 'dim_payout_source',
            key: 'source',
            config_json: null,
            value: 'Source'
          }
        ]
      },
      {
        label: 'Revenue Resources',
        key: 'revenue_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/revmgmt/filter`,
        enable: false,
        permissionNames: ['Revenue Management'],
        value: [
          {
            label: 'Source',
            key: 'rev_source',
            filter_key: 'dim_rev_source',
            config_json: null,
            value: 'Rev Source'
          }
        ]
      },
      {
        label: 'Unfilled Resources',
        key: 'unfilled_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/unfilled-inventory/filter`,
        enable: false,
        permissionNames: [
          'Unfilled',
          'analysis_report',
          'Revenue Dashboard'
        ],
        value: [
          {
            label: 'Stations',
            key: 'station',
            filter_key: 'dim_unfilled_station',
            config_json: null,
            value: 'Unfilled Station'
          }
        ]
      },
      {
        label: 'Vast Resources',
        key: 'vast_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/vast-errors/filter`,
        enable: false,
        permissionNames: [
          'Vast Errors',
          'analysis_report',
          'Revenue Dashboard'
        ],
        value: [
          {
            label: 'Advertiser',
            key: 'advertiser',
            filter_key: 'dim_advertiser',
            config_json: null,
            value: 'Advertiser'
          }
        ]
      },
      {
        label: 'CPR Resources',
        key: 'cpr_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/campaign-progress-report/filter`,
        enable: false,
        permissionNames: ['CPR'],
        value: [
          {
            label: 'Order',
            key: 'order_name',
            filter_key: 'dim_order_name',
            config_json: null,
            value: 'Order'
          },
          {
            label: 'Line Item',
            key: 'lineitem',
            filter_key: 'dim_line_item',
            config_json: null,
            value: 'Line Item'
          }
        ]
      },
      {
        label: 'KPI Resources',
        key: 'kpi_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/daily-kpi-lite-app/filter`,
        enable: false,
        permissionNames: [
          'daily-kpi',
          'daily-kpi-lite'
        ],
        value: [
          {
            label: 'Property',
            key: 'derived_station_rpt',
            filter_key: 'dim_derived_station_rpt',
            config_json: null,
            value: []
          }
        ]
      },
      {
        label: 'Camapign Management Resources',
        key: 'campaign_resources',
        filterUrl: `${this.BASE_URL}/unified/v1/campaign-delivery-dashboard`,
        enable: false,
        permissionNames: [
          'Campaign Management'
        ],
        value: [
          {
            label: 'Advertiser',
            filter_key: 'advertisers',
            key: 'getAdvertiserList',
            config_json: null,
            value: []
          },
          {
            label: 'Connector',
            filter_key: 'connector',
            key: 'getConnectionList',
            config_json: null,
            value: 'Connector'
          },
        ]
      },
      {
        label: 'AMP Resources',
        key: 'amp_resources',
        filterUrl: `${this.BASE_URL}/unified/v1/digital-property-analytics/filter`,
        enable: false,
        permissionNames: [
          'digital-property-analytics'
        ],
        value: [
          {
            label: 'Publisher Name',
            filter_key: 'dim_publisher_name',
            key: 'publisher_name',
            config_json: null,
            value: []
          },
        ]
      },
      {
        label: 'Revenue Resources Freecycle',
        key: 'revenue_resources_fc',
        filterUrl: `${this.BASE_URL}/freecycle/v1/revmgmt-fc/filter`,
        enable: false,
        permissionNames: ['Revenue Management FreeCycle',
        'Analysis Report FreeCycle',
        'Revenue Dashboard FreeCycle'],
        value: [
          {
            label: 'Source',
            key: 'source',
            filter_key: 'dim_source',
            config_json: null,
            value: 'Source'
          }
        ]
      },
      {
        label: 'Unfilled Resources Freecycle',
        key: 'unfilled_resources_fc',
        filterUrl: `${this.BASE_URL}/freecycle/v1/unfilled-inventory-fc/filter`,
        enable: false,
        permissionNames: [
          'Unfilled Inventory FreeCycle',
          'Analysis Report FreeCycle',
          'Revenue Dashboard FreeCycle'
        ],
        value: [
          {
            label: 'Ad Unit',
            key: 'ad_unit',
            filter_key: 'dim_ad_unit',
            config_json: null,
            value: 'Ad Unit'
          }
        ]
      },{
        label: 'Prebid Resources',
        key: 'prebid_publisher_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/prebid/filter`,
        enable: false,
        permissionNames: ['Prebid Analytics'],
        value: [
          {
            label: 'Publisher Name',
            key: 'publisher_name',
            filter_key: 'dim_prebid_publisher',
            config_json: null,
            value: 'Publisher Name'
          }
        ]
      },
      {
        label: 'ASBN Programmatic Revenue Resources',
        key: 'asbn_prog_rev_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/asbn-revmgmt/filter`,
        enable: false,
        permissionNames: [
          'Revenue Management Asbn'
        ],
        value: [
          {
            label: 'Source',
            filter_key: 'dim_prog_rev_source',
            key: 'source',
            config_json: null,
            value: 'Source'
          }
        ]
      },
      {
        label: 'ASBN Revenue Management Resources',
        key: 'asbn_rev_360_resources',
        filterUrl: `${this.BASE_URL}/frankly/v1/asbn-rev360/filter`,
        enable: false,
        permissionNames: [
          'Revenue Management 360 Asbn'
        ],
        value: [
          {
            label: 'Station Group',
            filter_key: 'dim_rev_360_media_group',
            key: 'media_group',
            config_json: null,
            value: 'Station Group'
          },
          {
            label: 'Station',
            filter_key: 'dim_rev_360_property',
            key: 'property',
            config_json: null,
            value: 'Station'
          },
          {
            label: 'Source',
            filter_key: 'dim_rev_360_source',
            key: 'source',
            config_json: null,
            value: 'Source'
          }
        ]
      }
    ];

    this.permCols = [
      { field: 'displayName', header: 'Permission' },
      { field: 'appName', header: 'Application' }
    ];
  }

  // convenience getter for easy access to form fields
  get u() {
    return this.userRegisterForm.controls;
  }

  get r() {
    return this.roleRegisterForm.controls;
  }

  get t() {
    return this.teamRegisterForm.controls;
  }

  get a() {
    return this.defaultAppRegisterForm.controls;
  }

  loadRoles() {
    this.roleServ.getRoles().subscribe(res => {
      const roles: any[] = this.libServ.deepCopy(res['data']);
      roles.splice(0, 0, { id: 0, name: 'None' });
      this.groupedRoles = res['data'];
      this.roleRegisterForm.controls.role.setValue(res['data'][0]);
      this.childRoles = roles;
      this.selectedRole = roles[0];
      this.roleChanged();
    });
    this.roleServ
      .getRoleDetails(this.appConfig['user']['role']['id'])
      .subscribe(res1 => {
        let permissionsData1 = res1['data']['permissions'];
        let arr = []
        permissionsData1.filter(x => x.permissionType == 'Application').forEach(element => {
          arr.push({
            key: element.appId,
            label: element.displayName,
            data: element.id,
            icon: element.iconClass,
            appId: element.appId,
            appGroupId: element.appGroupId,
            name: element.name,
            appName: element.appName,
            permissionType: element.permissionType,
            displayName:element.displayName,
            children: []
          });
        });
        permissionsData1.forEach(element => {
          console.log('data', element)
          arr.find(x => x.key == element.appId)['children'].push({
            key: element.appId + '-' + element.id,
            label: element.displayName,
            data: element.id,
            appId: element.appId,
            appGroupId: element.appGroupId,
            name: element.name,
            appName: element.appName,
            permissionType: element.permissionType,
            displayName:element.displayName,
            icon: "pi pi-fw pi-inbox"
          });

        });
        this.permissionsData = <TreeNode[]>arr;
      });
  }

  customTeamSelected() {
    if (this.customTeam) {
      if (this.groupedApps.length === 0) {
        this.roleServ.getRoleDetails(this.userdetails['roleId']).subscribe(res => {
          this.availableDimensions = this.libServ.deepCopy(this.defaultDimensions);
          this.groupedApps = res['data']['permissions']
            .filter(e => e.permissionType === 'Application')
            .map(e => {
              const app = this.libServ.deepCopy(e);
              app['id'] = app['appId'];
              this.availableDimensions.forEach(element => {
                element['permissionNames'].includes(e.name) ? element['enable'] = true : '';
              });
              return app;
            });
        });
      }
    }
  }

  loadTeams() {
    if (!this.customRole) {
      this.customTeam = false;
      this.teamServ.getTeamsAssociatedWithRole(this.userdetails['roleId']).subscribe(res => {
        this.groupedTeams = res['data'];
        this.teamRegisterForm.controls.team.setValue(res['data'][0]);
      });
    } else {
      this.groupedTeams = [];
      this.customTeam = true;
      this.availableDimensions = this.libServ.deepCopy(this.defaultDimensions);
      this.groupedApps = this.userdetails['role']['permissions']
        .filter(e => e.permissionType === 'Application')
        .map(e => {
          const app = this.libServ.deepCopy(e);
          app['id'] = app['appId'];
          this.availableDimensions.forEach(element => {
            element['permissionNames'].includes(e.name) ? element['enable'] = true : '';
          });
          return app;
        });
    }
  }

  loadDefaultApp() {
    if (!this.customRole) {
      if (this.groupedApps.length === 0) {
        this.roleServ.getRoleDetails(this.userdetails['roleId']).subscribe(res => {
          this.groupedApps = res['data']['permissions']
            .filter(e => e.permissionType === 'Application')
            .map(e => {
              const app = this.libServ.deepCopy(e);
              app['id'] = app['appId'];
              return app;
            });
          this.defaultAppRegisterForm.controls.default_app.setValue(this.groupedApps[0]);
        });
      } else {
        this.defaultAppRegisterForm.controls.default_app.setValue(this.groupedApps[0]);
      }
    } else {
      this.defaultAppRegisterForm.controls.default_app.setValue(this.groupedApps[0]);
    }
  }

  roleChanged() {
    if (this.selectedRole['id'] === 0) {
      this.formRoleModel.selectedPermissions = [];
    } else {
      this.roleServ.getRoleDetails(this.selectedRole['id']).subscribe(res => {
        // this.formRoleModel.selectedPermissions = res['data']['permissions'];
        let selectedPermissions1 = res['data']['permissions'];
        let arr1 = [];
        selectedPermissions1.forEach(element => {
          arr1.push({
            key: element.appId + '-' + element.id,
            label: element.displayName,
            data: element.id,
            icon: "pi pi-fw pi-inbox",
            appId: element.appId,
            appGroupId: element.appGroupId,
            name: element.name,
            appName: element.appName,
            permissionType: element.permissionType,
            partialSelected: true,
            displayName:element.displayName,
            parent: this.permissionsData.find(x => x.key == element.appId)['partialSelected'] = true
          });
        });
        let arr3 = [];
        this.permissionsData.forEach(element => {
          if (element.children.length == arr1.filter(x => x.key.split('-')[0] == element.key).length) {
            element['partialSelected'] = false;
            arr3.push(element);
          }
        });
        arr1 = arr1.concat(arr3);
        this.formRoleModel.selectedPermissions = <TreeNode[]>arr1;
        this.noPermissionsFlag = false;
      });
    }
  }

  onPermissionSelect(event) {
    this.noPermissionsFlag = false;
  }

  onPermissionUnselect(event) {
    if (this.formRoleModel.selectedPermissions.length < 1) {
      this.noPermissionsFlag = true;
    }
  }

  roleDetails() {
    console.log('this.userRegisterForm.value.role', this.roleRegisterForm);
    const ref = this.dialogService.open(RoleDetailsComponent, {
      header: ' Role Details ',
      contentStyle: { 'max-height': '80vh', width: '80vw', overflow: 'auto' },
      data: this.roleRegisterForm.value.role
    });
    ref.onClose.subscribe((data1: string) => { });
  }

  teamDetails() {
    const ref = this.dialogService.open(TeamDetailsComponent, {
      header: ' Team Details ',
      contentStyle: { 'max-height': '80vh', width: '60vw', overflow: 'auto' },
      data: this.teamRegisterForm.value.team
    });
    ref.onClose.subscribe((data1: string) => { });
  }

  onUserDetailsSubmit() {
    // stop here if form is invalid
    if (this.userRegisterForm.invalid) {
      return;
    }
    this.userdetails = {
      name: this.userRegisterForm.value.name,
      email: this.userRegisterForm.value.email,
      contactEmails: this.userRegisterForm.value.contact_email,
      roleId: null,
      teamId: null,
      defaultAppId: null
    };

    this.activeStepIdx = 1;
    this.loadRoles();
  }

  onRoleDetailsSubmit() {
    if (!this.customRole) {
      this.userdetails['roleId'] = this.roleRegisterForm.value.role.id;
      this.userdetails['role'] = null;
    } else {
      let selectedValues = this.formRoleModel.selectedPermissions.map(function (e) {
        return {
          id: e.data, appId: e['appId'],
          appGroupId: e['appGroupId'],
          permissionType: e['permissionType'],
          name: e['name'],
          appName: e['appName'],
          displayName:e['displayName']
        };
      });
      selectedValues = Array.from(new Set(selectedValues.map(a => a.id)))
        .map(id => {
          return selectedValues.find(a => a.id === id)
        });
      this.userdetails['role'] = {
        name: this.formRoleModel.roleName,
        description: this.formRoleModel.roleDescription,
        permissions: selectedValues
      };
      this.userdetails['roleId'] = null;
    }
    this.activeStepIdx = 2;
    this.groupedApps = [];
    this.loadTeams();
  }

  onTeamDetailsSubmit() {
    if (this.customTeam) {
      const resources = [];
      for (const key of Object.keys(this.formTeamModel.selectedDims)) {
        const defaultD = this.availableDimensions.find(grp => grp.key === key);
        defaultD.value.forEach(valueObj => {
          const obj = {
            key: valueObj['filter_key'],
            display_name: valueObj['label'],
            value: this.formTeamModel.selectedDims[key][valueObj.filter_key]
          };
          resources.push(obj);
        });
      }
      this.userdetails['team'] = {
        name: this.formTeamModel.teamName,
        resources: JSON.stringify(resources),
        relatedRoleId: this.userdetails['roleId']
      };
      this.userdetails['teamId'] = null;
    } else {
      this.userdetails['teamId'] = this.teamRegisterForm.value.team.id;
      this.userdetails['team'] = null;
    }
    this.activeStepIdx = 3;
    this.loadDefaultApp();
  }

  onDefaultAppSubmit() {
    this.userdetails['defaultAppId'] = this.defaultAppRegisterForm.value.default_app.appId;
    if (this.customRole) {
      this.roleServ.createRole(this.userdetails['role']).subscribe(res => {
        if (res['status']) {
          this.toastService.displayToast({
            severity: 'success',
            summary: 'Role Created',
            detail: 'Role created successfully'
          });
          this.userdetails['roleId'] = res['data'];
          this.userdetails['team']['relatedRoleId'] = res['data'];
          this.teamServ.createTeam(this.userdetails['team']).subscribe(res1 => {
            if (res1['status']) {
              this.toastService.displayToast({
                severity: 'success',
                summary: 'Team Created',
                detail: 'Team created successfully'
              });
              this.userdetails['teamId'] = res1['data'];
              const req = {
                name: this.userdetails['name'],
                email: this.userdetails['email'],
                contactEmails: this.userdetails['contactEmails'],
                roleId: this.userdetails['roleId'],
                teamId: this.userdetails['teamId'],
                defaultAppId: this.userdetails['defaultAppId']
              };
              this.createUser(req);
            } else {
              this.toastService.displayToast({
                severity: 'error',
                summary: 'Something went wrong',
                detail: 'Error creating Team'
              });
            }
          });
        } else {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Something went wrong',
            detail: 'Error creating Role'
          });
        }
      });
    } else if (!this.customRole && this.customTeam) {

      this.teamServ.createTeam(this.userdetails['team']).subscribe(res1 => {
        if (res1['status']) {
          this.toastService.displayToast({
            severity: 'success',
            summary: 'Team Created',
            detail: 'Team created successfully'
          });
          this.userdetails['teamId'] = res1['data'];
          const req = {
            name: this.userdetails['name'],
            email: this.userdetails['email'],
            contactEmails: this.userdetails['contactEmails'],
            roleId: this.userdetails['roleId'],
            teamId: this.userdetails['teamId'],
            defaultAppId: this.userdetails['defaultAppId']
          };
          this.createUser(req);
        } else {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Something went wrong',
            detail: 'Error creating Team'
          });
        }
      });
    } else {
      const req = {
        name: this.userdetails['name'],
        email: this.userdetails['email'],
        contactEmails: this.userdetails['contactEmails'],
        roleId: this.userdetails['roleId'],
        teamId: this.userdetails['teamId'],
        defaultAppId: this.userdetails['defaultAppId']
      };
      this.createUser(req);
    }
  }

  createUser(details) {
    this.userService.createUser(details).subscribe(res => {
      if (res['status']) {
        this.toastService.displayToast({
          severity: 'success',
          summary: 'User Created',
          detail: 'User created successfully'
        });
        this.router.navigate(['/users']);
      } else {
        if (res['message'].includes('Duplicate entry')) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Duplicate Entry',
            detail: 'This email/user already exists'
          });
        } else {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Something went wrong',
            detail: 'Error creating User'
          });
        }
      }
    });
  }

  goBack() {
    this.router.navigate(['/users']);
  }

  ngOnDestroy() {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }

  validateField(control: AbstractControl) {
    if (control.value) {
      control.patchValue(control.value.trim())
    }
  }
  validateEmailAlreadyExist(emailControl) {
    //  console.log("email   :", emailControl.value)
    if (emailControl.value) {
      this.userService.emailAlreadyExist(emailControl.value).subscribe(result => {
        if (result) {
          emailControl.setErrors({
            'exists': true
          })
        }
        else {
          emailControl.clearValidators();
        }
        //    console.log("result   :", result)
      })
    }
  }

  validateRoleAlreadyExist(roleControl) {
    if (roleControl.value) {
      this.roleServ.roleAlreadyExist(roleControl.value).subscribe(result => {
        if (result) {
          this.isRoleExists = true;
        }
        else {
          this.isRoleExists = false;
        }
      });
    }
  }

  validateTeamAlreadyExist(teamControl) {
    console.log("team   :", teamControl.value)
    if (teamControl.value) {
      this.teamServ.teamAlreadyExist(teamControl.value).subscribe(result => {
        if (result) {
          this.isTeamExists = true;
        }
        else {
          this.isTeamExists = false;
        }
      })
    }
  }
}
