import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { AddUserComponent } from './add-user/add-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { TeamResourcesComponent } from './team-resources/team-resources.component';
import { FilterContainerModule } from '../common/filter-container/filter-container.module';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { DropdownModule } from 'primeng/dropdown';
import { ButtonModule } from 'primeng/button';
import { MultiSelectModule } from 'primeng/multiselect';
import { CheckboxModule } from 'primeng/checkbox';
import { InputSwitchModule } from 'primeng/inputswitch';
import { StepsModule } from 'primeng/steps';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableModule } from 'primeng/table';
import { RadioButtonModule } from 'primeng/radiobutton';
import { SharedModule } from 'src/app/_pipes/shared.module';
import { SearchStringPipe } from 'src/app/_pipes/search-string.pipe';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { TreeModule } from 'primeng/tree';

@NgModule({
  declarations: [
    UsersComponent,
    AddUserComponent,
    EditUserComponent,
    TeamResourcesComponent
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    FilterContainerModule,
    SharedModule,
    ToggleButtonModule,
    ConfirmDialogModule,
    RadioButtonModule,
    TableModule,
    InputTextareaModule,
    DropdownModule,
    ButtonModule,
    MultiSelectModule,
    CheckboxModule,
    InputSwitchModule,
    StepsModule,
    TreeModule
  ],
  providers: [SearchStringPipe, ConfirmationService]
})
export class UsersModule {}
