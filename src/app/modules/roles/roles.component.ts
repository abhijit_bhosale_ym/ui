import { Component, OnInit, OnDestroy } from '@angular/core';
import { IUser } from 'src/app/_interfaces/users';
import { RoleService } from '../../_services/users/role.service';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import {
  Router,
  ActivatedRoute,
  NavigationExtras,
  NavigationEnd,
  RouterEvent
} from '@angular/router';
import { DialogService } from 'primeng/dynamicdialog';
import { RoleDetailComponent } from './role-details/role-details.component';
import { ImpersonateService } from 'src/app/_services/impersonate/impersonate.service';
import { filter, takeUntil, pairwise, startWith } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';
import { CommonLibService } from 'src/app/_services';
import { IRoles } from 'src/app/_interfaces/roles';
import { EditRoleComponent } from './edit-role/edit-role.component';

@Component({
  selector: 'ym-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss'],
  providers: [DialogService]
})


export class RolesComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;
  private isEditPermission = false;
  appConfig: object = {};
  users: IUser[];
  roles: IRoles[];
  columns: any[];
  status: boolean;
  public destroyed = new Subject<any>();

  constructor(
    private roleService: RoleService,
    private router: Router,
    private dialogService: DialogService,
    private impersonateServ: ImpersonateService,
    private _titleService: Title,
    private currentActivatedRoute: ActivatedRoute,
    private appConfigService: AppConfigService,
    private libServ: CommonLibService
  ) {
    this.router.events
      .pipe(
        filter((event: RouterEvent) => event instanceof NavigationEnd),
        pairwise(),
        filter((events: RouterEvent[]) => events[0].url === events[1].url),
        startWith('Initial call'),
        takeUntil(this.destroyed)
      )

      .subscribe(() => {
        roleService.getRoles().subscribe(res => {
          this.roles = <IRoles[]>res['data'];
        });
      });
  }

  ngOnInit() {
    this.columns = [
      {
        field: 'name',
        header: 'Name',
        width: '245'
      },
      {
        field: 'description',
        header: 'Description',
        width: '245'
      },
      {
        field: 'info',
        header: 'Details',
        width: '120'
      },
      // {
      //   field: 'edit',
      //   header: 'Edit',
      //   width: '60'
      // },
      {
        field: 'createdAt',
        header: 'Created at',
        width: '215'
      },
      {
        field: 'upatedAt',
        header: 'Updated at',
        width: '215'
      },
    ];
    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        console.log('appConfig', this.appConfig);
        this._titleService.setTitle(this.appConfig['displayName']);
        this.isEditPermission = this.appConfig['permissions'].some(
          o => o.name === 'um-update-role'
        );
        if (!((this.columns.some(x => x.field == 'edit')))) {
          this.isEditPermission ? this.columns.push({ field: 'edit', header: 'Edit', width: '60' }) : '';
        }
      }
    });
  }

  editRole(roleDtls) {
    const data = {
      role: roleDtls,
      userRoleId: this.appConfig['user']['role']['id']
    };

    const ref = this.dialogService.open(EditRoleComponent, {
      header: ' Edit Role ',
      contentStyle: { 'min-height': '80vh', 'max-height': '80vh', width: '60vw', overflow: 'auto' },
      data: data
    });
    ref.onClose.subscribe((data1: string) => {
      // this.dialogService.dialogComponentRef.destroy();
    });
  }

  getRoleInfo(role) {
    const ref = this.dialogService.open(RoleDetailComponent, {
      header: ' Role Details ',
      contentStyle: { 'min-height': '80vh', 'max-height': '80vh', width: '60vw', overflow: 'auto' },
      data: role
    });
    ref.onClose.subscribe((data1: string) => { });
  }

  decreaseSearchWidth(width) {
    return width - 30;
  }

  ngOnDestroy() {
    this.destroyed.next();
    this.destroyed.complete();
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }
}
