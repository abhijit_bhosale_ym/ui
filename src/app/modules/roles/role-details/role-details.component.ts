import { Component, OnInit } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { RoleService } from 'src/app/_services/users/role.service';

@Component({
  selector: 'ym-role-details',
  templateUrl: './role-details.component.html',
  styleUrls: ['./role-details.component.scss']
})
export class RoleDetailComponent implements OnInit {
  public role: object;
  public roleDetails: [];

  constructor(
    private config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private roleServ: RoleService
  ) {
    this.role = this.config.data;
  }

  ngOnInit() {
    this.roleServ.getUserTeamDetails(this.role['id']).subscribe(res => {
      if (res['data'] != null) {
      this.roleDetails = res['data'];
      }
    });
  }

  decreaseSearchWidth(width) {
    return width - 30;
  }
}
