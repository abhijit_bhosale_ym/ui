import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RolesRoutingModule } from './roles-routing.module';
import { RolesComponent } from '../roles/roles.component';
// import { AddUserComponent } from './add-user/add-user.component';
// import { EditUserComponent } from './edit-user/edit-user.component';
import { FilterContainerModule } from '../common/filter-container/filter-container.module';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from 'primeng/button';
import { TableModule } from 'primeng/table';
import { InputSwitchModule } from 'primeng/inputswitch';
import { DropdownModule } from 'primeng/dropdown';
import { SharedModule } from 'src/app/_pipes/shared.module';
import { SearchStringPipe } from 'src/app/_pipes/search-string.pipe';
import { TreeModule } from 'primeng/tree';
import { DialogService } from 'primeng';
@NgModule({
//  declarations: [UsersComponent, AddUserComponent, EditUserComponent],
declarations: [RolesComponent],
  imports: [
    CommonModule,
    RolesRoutingModule,
    ButtonModule,
    TableModule,
    ReactiveFormsModule,
    InputSwitchModule,
    DropdownModule,
    FilterContainerModule,
    SharedModule,
    TreeModule
  ],
  providers: [SearchStringPipe,DialogService]
})
export class RoleModule {}
