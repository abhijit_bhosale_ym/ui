import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { PlatformConfigService, CommonLibService } from 'src/app/_services';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { RoleService } from '../../../_services/users/role.service';
import { Subscription } from 'rxjs';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { DialogService } from 'primeng/dynamicdialog';
import { ImpactedUserComponent } from '../../users/impacted-user/impacted-user.component';
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder
} from '@angular/forms';
import { TreeNode, ConfirmationService } from 'primeng';

@Component({
  selector: 'ym-edit-role',
  templateUrl: './edit-role.component.html',
  styleUrls: ['./edit-role.component.scss'],
  providers: [DialogService]
})
export class EditRoleComponent implements OnInit, OnDestroy {
  private platConfigObs: Subscription;

  isRoleExists = false;
  roleName: string;
  description: string;
  selectedPermissions: TreeNode[];
  originalPermissions: any[];
  userform: FormGroup;
  noPermissionsFlag = false;
  permCols: any[];
  permissionsData: TreeNode[];
  role: object;
  userRoleId: any;
  files3: TreeNode[];
  selectedFiles2: TreeNode[];
  constructor(
    private roleServ: RoleService,
    private dialogService: DialogService,
    private platformConfigService: PlatformConfigService,
    public libServ: CommonLibService,
    private toastService: ToastService,
    private config: DynamicDialogConfig,
    private fb: FormBuilder,
    public dynamicDialogRef: DynamicDialogRef,
    private confirmationService: ConfirmationService,
    private router: Router) {
    this.role = this.config['data']['role'];
  }
  ngOnInit() {

    this.userform = this.fb.group({
      roleName: new FormControl('', Validators.required),
      description: new FormControl(),
      permissionsData: new FormControl('', Validators.minLength(1)),
      selectedPermissions: new FormControl()
    });

    this.roleName = this.role['name'];
    this.description = this.role['description'];
    this.userRoleId = this.config['data']['userRoleId'];

    this.roleServ
      .getRoleDetails(this.userRoleId)
      .subscribe(res1 => {
        let permissionsData1 = res1['data']['permissions'];
        let arr = []
        permissionsData1.filter(x => x.permissionType == 'Application').forEach(element => {
          arr.push({
            key: element.appId,
            label: element.displayName,
            data: element.id,
            icon: element.iconClass,
            appId: element.appId,
            appGroupId: element.appGroupId,
            permissionType: element.permissionType,
            children: []
          });
        });
        permissionsData1.forEach(element => {
          arr.find(x => x.key == element.appId)['children'].push({
            key: element.appId + '-' + element.id,
            label: element.displayName,
            data: element.id,
            appId: element.appId,
            appGroupId: element.appGroupId,
            permissionType: element.permissionType,
            icon: "pi pi-fw pi-inbox"
          });

        });
        this.permissionsData = <TreeNode[]>arr;
        this.roleServ
          .getRoleDetails(this.role['id'])
          .subscribe(res2 => {
            let selectedPermissions1 = res2['data']['permissions'];
            let arr1 = [];
            selectedPermissions1.forEach(element => {
              arr1.push({
                key: element.appId + '-' + element.id,
                label: element.displayName,
                data: element.id,
                icon: "pi pi-fw pi-inbox",
                appId: element.appId,
                appGroupId: element.appGroupId,
                permissionType: element.permissionType,
                partialSelected: true,
                parent: this.permissionsData.find(x => x.key == element.appId)['partialSelected'] = true
              });
            });
            let arr3 = [];
            this.permissionsData.forEach(element => {
              if (element.children.length == arr1.filter(x => x.key.split('-')[0] == element.key).length) {
                element['partialSelected'] = false;
                arr3.push(element);
              }
            });
            arr1 = arr1.concat(arr3);
            this.selectedPermissions = <TreeNode[]>arr1;
            this.originalPermissions = this.libServ.deepCopy(selectedPermissions1);
          });
      });

    this.permCols = [
      { field: 'name', header: 'Permission' },
      { field: 'description', header: 'Application' }
    ];
  }

  onPermissionSelect(event) {
    this.noPermissionsFlag = false;
  }

  onPermissionUnselect(event) {
    if (this.selectedPermissions.length == 0) {
      this.noPermissionsFlag = true;
    }
  }
  submitHandler1() {
    console.log('this.selectedFiles2', this.selectedFiles2);
  }
  submitHandler() {
    this.confirmationService.confirm({
      message: 'If you edit any role then you need to update the team as well.',
      header: 'Team Warning',
      accept: () => {
        let selectedValues = this.selectedPermissions.map(function (e) {
          return { id: e.data, appId: e['appId'], appGroupId: e['appGroupId'], permissionType: e['permissionType'], }
        })
        selectedValues = Array.from(new Set(selectedValues.map(a => a.id)))
          .map(id => {
            return selectedValues.find(a => a.id === id)
          })
        console.log('this.selectedPermissions11', selectedValues);
        const originalPermissionsmap = [];

        this.originalPermissions.forEach(ele => {
          if (!(selectedValues.some(x => x.id == ele.id))) {
            originalPermissionsmap.push(ele);
          }
        });
        if (!this.noPermissionsFlag) {
          const reqData = {
            name: this.roleName,
            description: this.description,
            permissions: selectedValues.map(x => x.id),
            permissionsDetails: selectedValues
          };

          if (originalPermissionsmap.length > 0) {
            this.roleServ
              .getUsersByRole(this.role['id'])
              .subscribe(res1 => {
                const userData = res1 as [];

                if (userData.length > 0) {
                  const data = {
                    data: userData
                  };
                  // this.dialogService.dialogComponentRef.destroy();
                  const ref = this.dialogService.open(ImpactedUserComponent, {
                    header: 'May Be Impacted User List',
                    contentStyle: {
                      'min-height': '65vh',
                      'max-height': '65vh',
                      width: '60vw',
                      overflow: 'auto'
                    },
                    data: data
                  });
                  ref.onClose.subscribe((data: string) => {
                    console.log('data.............role', data);
                    if (data) {
                      const reqUpdateData = {
                        removepermission: originalPermissionsmap.map(x => x.id),
                        removepermissionsDetails: originalPermissionsmap
                      };
                      this.roleServ.updateImpactedRole(this.role['id'], reqUpdateData).subscribe(res => {
                        this.roleServ
                          .updateRole(this.role['id'], reqData)
                          .subscribe(res => {
                            if (res['status'] === 0) {
                              // user.status = !user.status;
                              this.toastService.displayToast({
                                severity: 'error',
                                summary: 'Server Error',
                                detail: 'Please refresh the page'
                              });
                              console.log(res['status_msg']);
                              return;
                            } else {
                              this.router.navigate(['/roles']);
                              this.toastService.displayToast({
                                severity: 'success',
                                summary: 'Role Updated',
                                detail: 'Role updated successfully'
                              });
                            }
                            // console.log('destroy')
                            // setTimeout(() => {
                            // this.dialogService.dialogComponentRef.destroy();
                            this.dynamicDialogRef.close(null);
                            // }, 0);
                          });
                      });
                    } else {

                    }

                  });
                } else {


                  const reqUpdateData = {
                    removepermission: originalPermissionsmap.map(x => x.id),
                    removepermissionsDetails: originalPermissionsmap
                  };
                  this.roleServ.updateImpactedRole(this.role['id'], reqUpdateData).subscribe(res => {
                    this.roleServ
                      .updateRole(this.role['id'], reqData)
                      .subscribe(res => {
                        if (res['status'] === 0) {
                          // user.status = !user.status;
                          this.toastService.displayToast({
                            severity: 'error',
                            summary: 'Server Error',
                            detail: 'Please refresh the page'
                          });
                          console.log(res['status_msg']);
                          return;
                        } else {
                          this.router.navigate(['/roles']);
                          this.toastService.displayToast({
                            severity: 'success',
                            summary: 'Role Updated',
                            detail: 'Role updated successfully'
                          });
                        }
                        setTimeout(() => {
                          // this.dialogService.dialogComponentRef.destroy();
                          this.dynamicDialogRef.close(null);
                        }, 0);

                      });
                  });

                }
              });
          } else {
            this.roleServ
              .updateRole(this.role['id'], reqData)
              .subscribe(res1 => {
                if (res1['status']) {

                  this.role['name'] = this.roleName;
                  this.role['description'] = this.description;

                  this.dynamicDialogRef.close(null);
                  this.toastService.displayToast({
                    severity: 'success',
                    summary: 'Role Updated',
                    detail: 'Role updated successfully'
                  });
                } else {
                  this.toastService.displayToast({
                    severity: 'error',
                    summary: 'Something went wrong',
                    detail: 'Error updating Role'
                  });
                }
              });

          }
        }
      }
    });
  }
  validateRoleAlreadyExist(roleControl) {
    this.isRoleExists = false;
    if (roleControl.value && this.role['name'].toLowerCase() != roleControl.value.toLowerCase()) {
      this.roleServ.roleAlreadyExist(roleControl.value).subscribe(result => {
        if (result) {
          this.isRoleExists = true;
        }
        else {
          this.isRoleExists = false;
        }
      });
    }
  }

  ngOnDestroy(): void {
    if (this.platConfigObs && !this.platConfigObs.closed) {
      this.platConfigObs.unsubscribe();
    }
  }

  goBack() {
    this.dynamicDialogRef.close(null);
  }
}
