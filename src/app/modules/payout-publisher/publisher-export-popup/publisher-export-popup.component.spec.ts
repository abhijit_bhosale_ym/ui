import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublisherExportPopupComponent } from './publisher-export-popup.component';

describe('Rev360ExportPopupComponent', () => {
  let component: PublisherExportPopupComponent;
  let fixture: ComponentFixture<PublisherExportPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PublisherExportPopupComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublisherExportPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
