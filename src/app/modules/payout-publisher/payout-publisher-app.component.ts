import { Component, OnInit, OnDestroy } from '@angular/core';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { CommonLibService } from 'src/app/_services';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';
import { FetchApiDataService } from './fetch-api-data.service';
import { PayoutPublisherDailyDataPopupComponent } from './payout-publisher-daily-data-popup/payout-publisher-daily-data-popup.component';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { Subscription } from 'rxjs';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { SendEmailComponent } from '../send-email/send-email.component';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { PublisherExportPopupComponent } from './publisher-export-popup/publisher-export-popup.component';
import { environment } from 'src/environments/environment';
import { DialogService } from 'primeng';

@Component({
  selector: 'ym-payout-publisher-app',
  templateUrl: './payout-publisher-app.component.html',
  styleUrls: ['./payout-publisher-app.component.scss']
})
export class PayoutPublisherAppComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;

  timeZone = environment.timeZone;
  public reportType = 'isSummary';
  appConfig: object = {};
  appConfig1: object = {};
  lastUpdatedOn: Date;
  collapsedCustom = false;
  cardsJson = [];
  showCards = true;
  chartColors: string[];
  selectedDates: Date[];

  noDataImpressionsLineChart = false;
  showImpressionsLineChart = false;
  ImpressionsLineChartJson: object;

  noDataRevenueLineChart = false;
  showRevenueLineChart = false;
  RevenueLineChartJson: object;

  noDataEcpmLineChart = false;
  showEcpmLineChart = false;
  EcpmLineChartJson: object;


  showMonthlyChart = false;
  MonthlyBarChartJson: object;
  MonthlyStackChartJson: object;
  noDataMonthlyChart = false;
  nextUpdated: Date;
  dataUpdatedThrough: Date;
  isExportReport = false;
  timeout: any;
  tableReq: object;

  sourcesJson = {
    titantv: {
      Columns: ['titantv_page_views', 'titantv_revenue', 'titantv_rpm'],
      DisplayName: 'TitanTv'
    },
    antennaweb: {
      Columns: [
        'antennaweb_page_views',
        'antennaweb_revenue',
        'antennaweb_rpm'
      ],
      DisplayName: 'Antennaweb'
    }
  };

  aggTableData: TreeNode[];
  noTableData = false;
  tableDimColDef: any[];
  tableMatColDef: any[];
  aggTableColumnDef: any[];
  aggTableJson: Object;
  displayAggTable = false;
  toggleView = true;
  filtersApplied: object = {};
  exportReportData: object = {};
  exportRequest: ExportRequest = <ExportRequest>{};
  ispubGraph = false;

  appFeaturesConfigs = {};
  mainCardsConfig = {};

  constructor(
    private appConfigService: AppConfigService,
    public libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private htmltoimage: HtmltoimageService,
    private exportService: ExportdataService,
    private pptExport: ExportPptService,
    private formatNumPipe: FormatNumPipe,
    private confirmationService: ConfirmationService
  ) { }

  ngOnInit() {
    this.tableDimColDef = [{
      field: 'time_key',
      displayName: 'Date',
      format: '',
      width: '100',
      exportConfig: {
        format: 'date',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: false
      },
      footerTotal: '-'
    }, {
      field: 'accounting_key',
      displayName: 'Month',
      format: '',
      width: '110',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: false
      },
      footerTotal: '-'
    },
    {
      field: 'station_group',
      displayName: 'Media Group',
      format: '',
      width: '230',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: false
      },
      footerTotal: '-'
    },

    {
      field: 'derived_station_rpt',
      displayName: 'Property',
      format: '',
      width: '170',
      exportConfig: {
        format: 'string',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: false
      },
      footerTotal: '-'
    }];
    this.tableMatColDef = [
      {
        field: 'dp_impressions',
        displayName: 'Billable Imps.',
        format: 'number',
        width: '125',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '0'
      },
      // {
      //   field: 'dfp_impressions',
      //   displayName: 'DFP Imps.',
      //   format: 'number',
      //   width: '170',
      //   exportConfig: {
      //     format: 'number',
      //     styleinfo: {
      //       thead: 'default',
      //       tdata: 'white'
      //     }
      //   },
      //   formatConfig: [],
      //   options: {
      //     editable: false,
      //     colSearch: false,
      //     colSort: true
      //     resizable: true,
      //     movable: true
      //   },
      //   footerTotal: '-'
      // },
      {
        field: 'gross_revenue',
        displayName: 'Gross Revenue',
        width: '133',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '0'
      },
      {
        field: 'gross_ecpm',
        displayName: 'Gross eCPM',
        format: '$',
        width: '120',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '0'
      },
      {
        field: 'station_revenue_share_distribution',
        displayName: 'Client Net Revenue Share($)',
        format: '$',
        width: '210',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '0'
      },
      {
        field: 'client_net_ecpm',
        displayName: 'Client Net eCPM',
        format: '$',
        width: '140',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '0'
      },
      {
        field: 'client_net_share',
        displayName: 'Client Net Share(%)',
        format: 'percentage',
        width: '160',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '0'
      }
    ];
    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,

      columns: this.tableDimColDef,
      // selectedColumns: this.aggTableColumnDef,
      // frozenCols: [],
      // frozenWidth: '0px',

      // columns: this.aggTableColumnDef.slice(5),
      // selectedColumns: this.aggTableColumnDef.slice(5),
      // frozenCols: [...this.aggTableColumnDef.slice(0, 5)],
      // frozenWidth:
      //   this.aggTableColumnDef
      //     .slice(0, 5)
      //     .reduce((tot, cur) => tot + parseInt(cur.width), 0) + 'px',


      // footerColumns: this.tableMatColDef,

      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: false,
      overallSearch: true,
      columnSearch: false
    };

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        this.isExportReport = this.appConfig['permissions'].some(
          o => o.name === 'pub-payout-export-reports'
        );
        this._titleService.setTitle(this.appConfig['displayName']);
        console.log("appconfig", this.appConfig['permissions']);

        this.ispubGraph = this.appConfig['permissions'].some(
          o => o.name === 'pub-payout-view-chart'
        );
        let startDate;
        let endDate;
        const date_config = this.appConfig['filter']['filterConfig']['filters'][
          'datePeriod'
        ][0];
        if ((date_config['defaultDate'][0]['value'] === 0 && date_config['defaultDate'][0]['period'] === 'month') && (moment().format('DD') === '01' || moment().format('DD') === '02')) {
          startDate = moment().subtract(1, 'months').startOf('month');
          endDate = moment().subtract(1, 'months').endOf('month');
        } else {
          if (
            date_config[
            'defaultDate'
            ][0]['startOf']
          ) {
            startDate = moment()
              .subtract(
                date_config['defaultDate'][0]['value'],
                date_config['defaultDate'][0]['period']
              )
              .startOf(
                date_config['defaultDate'][0]['period']
              );
          } else {
            startDate = moment().subtract(
              date_config['defaultDate'][0]['value'],
              date_config['defaultDate'][0]['period']
            );
          }
          endDate = moment().subtract(
            date_config[
            'defaultDate'
            ][1]['value'],
            date_config[
            'defaultDate'
            ][1]['period']
          );
        }

        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: startDate.format('YYYYMMDD'),
            time_key2: endDate.format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: this.appConfig['filter']['filterConfig']['groupBy'].filter(
            v => v.selected
          )
        };
        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        this.appConfig1 = this.appConfig;
        this.initialLoading();
      }
    });
  }

  initialLoading() {
    this.chartColors = this.libServ.dynamicColors(5);
    this.dataFetchServ
      .getLastUpdatedData(this.appConfig['id'])
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        this.lastUpdatedOn = moment(data[0]['updated_at']).toDate();
        this.nextUpdated = moment(data[0]['next_run_at']).toDate();
        this.dataUpdatedThrough = moment(
          data[0]['source_updated_through'],
          'YYYYMMDD'
        ).toDate();
      });

    this.libServ.getAppFeaturesConfigs(this.appConfig['id']).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: data['message']
        });
        console.log(data['message']);
        return;
      }
      this.appFeaturesConfigs = data['data'];
      this.appFeaturesConfigs['cardsConfig']['cards'].forEach(element => {
        if (element['name'] === 'main') {
          this.mainCardsConfig = element['config'];
          this.cardsJson = this.mainCardsConfig['list'];
          if (this.mainCardsConfig['display']) {
            this.loadCards();
          }
        }
      });
    });

    this.tableReq = {
      dimensions: [],
      metrics: [
        'gross_revenue',
        'dp_impressions',
        'dfp_adserver_impressions',
        'station_revenue_share_distribution',
        'total_recoupable_expenses'
      ],
      derived_metrics: ['gross_ecpm', 'client_net_ecpm', 'client_net_share'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: this.reportType === 'isDetailed' ? ['time_key', 'accounting_key'] : ['accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys(),
      orderBy: [
        { key: 'accounting_key', opcode: 'asc' }

        // { key: 'derived_station_rpt', opcode: 'asc' }
      ],
      limit: '',
      offset: ''
    };

    this.loadTableData(this.tableReq);

    if (this.ispubGraph) {
      const MonthlyChartReq = {
        dimensions: ['accounting_key', 'derived_station_rpt'],
        metrics: ['gross_revenue'],
        derived_metrics: [],
        timeKeyFilter: this.libServ.deepCopy(
          this.filtersApplied['timeKeyFilter']
        ),
        filters: this.libServ.deepCopy(this.filtersApplied['filters']),
        groupByTimeKey: { key: ['accounting_key'], interval: 'daily' },
        gidGroupBy: ['station_group', 'derived_station_rpt'],
        orderBy: [{ key: 'accounting_key', opcode: 'asc' }],
        limit: '',
        offset: ''
      };
      this.loadMonthlyChart(MonthlyChartReq);
    }
    // if (this.ispubGraph) {
    const loadGraph = {
      dimensions: ['time_key'],
      metrics: [
        'gross_revenue',
        'dp_impressions',
        'dfp_adserver_impressions',
        'station_revenue_share_distribution',
        'total_recoupable_expenses'
      ],
      derived_metrics: ['gross_ecpm', 'client_net_ecpm', 'client_net_share'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: { key: ['time_key'], interval: 'daily' },
      gidGroupBy: ['source'],
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: '',
      isTable: false
    };
    this.loadMainLineChart(loadGraph);
  }

  resetPagination(tt1) {
    if (typeof tt1 !== 'undefined') {
      tt1.reset();
    }
  }

  loadTableData(param) {
    let grpBysFilter = [];
    let tableDimColDef = [];
    if (this.reportType == 'isDetailed') {
      grpBysFilter.push('time_key');
      //   if (!(this.tableDimColDef.some(o => o.field == 'time_key'))) {
      //     this.tableDimColDef.unshift({
      //       field: 'time_key',
      //       displayName: 'Date',
      //       format: '',
      //       width: '100',
      //       exportConfig: {
      //         format: 'string',
      //         styleinfo: {
      //           thead: 'default',
      //           tdata: 'white'
      //         }
      //       },
      //       formatConfig: [],
      //       options: {
      //         editable: false,
      //         colSearch: false,
      //         colSort: true,
      //         resizable: true,
      //         movable: false
      //       },
      //       footerTotal: '-'
      //     });
      //   }
      // } else {
      //   if (this.tableDimColDef.some(o => o.field == 'time_key')) {
      //     this.tableDimColDef.splice(0, 1);
      //   }
    }
    grpBysFilter.push('accounting_key');
    grpBysFilter = grpBysFilter.concat(this.filtersApplied['groupby'].map(e => e.key));

    grpBysFilter.forEach(ele => {
      tableDimColDef.push(this.tableDimColDef.find(x => x.field == ele));
    });
    param['dimensions'] = grpBysFilter;
    param['groupByTimeKey'] = {
      key: this.reportType == 'isDetailed' ? ['time_key', 'accounting_key'] : ['accounting_key'],
      interval: 'daily'
    }

    tableDimColDef = tableDimColDef.concat(this.tableMatColDef);
    console.log('tableDimColDef', tableDimColDef);
    this.aggTableJson['columns'] = tableDimColDef.slice(
      grpBysFilter.length
    );
    this.aggTableJson['selectedColumns'] = tableDimColDef.slice(
      grpBysFilter.length
    );
    this.aggTableJson['frozenCols'] = [
      ...tableDimColDef.slice(0, grpBysFilter.length)
    ];
    this.aggTableJson['frozenWidth'] = tableDimColDef
      .slice(0, grpBysFilter.length)
      .reduce((tot, cur) => tot + parseInt(cur.width), 0) + 'px',
      this.aggTableJson['footerColumns'] = tableDimColDef;
    this.reloadAggTable();

    this.aggTableJson['loading'] = true;

    // this.tableReq = {
    //   dimensions: grpBysFilter,
    //   metrics: [
    //     'gross_revenue',
    //     'dp_impressions',
    //     'dfp_adserver_impressions',
    //     'station_revenue_share_distribution',
    //     'total_recoupable_expenses'
    //   ],
    //   derived_metrics: ['gross_ecpm', 'client_net_ecpm', 'client_net_share'],
    //   timeKeyFilter: this.libServ.deepCopy(
    //     this.filtersApplied['timeKeyFilter']
    //   ),
    //   filters: this.libServ.deepCopy(this.filtersApplied['filters']),
    //   groupByTimeKey: {
    //     key: this.reportType == 'isDetailed' ? ['time_key', 'accounting_key'] : ['accounting_key'],
    //     interval: 'daily'
    //   },
    //   gidGroupBy: this.getGrpBys(),
    //   orderBy: [
    //     { key: 'accounting_key', opcode: 'asc' }

    //     // { key: 'derived_station_rpt', opcode: 'asc' }
    //   ],
    //   limit: '',
    //   offset: ''
    // };


    const TotalFooterReq = {
      dimensions: [],
      metrics: [
        'gross_revenue',
        'dp_impressions',
        'dfp_adserver_impressions',
        'station_revenue_share_distribution',
        'total_recoupable_expenses'
      ],
      derived_metrics: ['gross_ecpm', 'client_net_ecpm', 'client_net_share'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: this.reportType == 'isDetailed' ? ['time_key', 'accounting_key'] : ['accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys(),
      orderBy: [],
      limit: '',
      offset: ''
    };
    this.dataFetchServ.getRevMgmtData(param).subscribe(data => {
      this.dataFetchServ.getRevMgmtData(TotalFooterReq).subscribe(dataTotal => {
        if (data['status'] === 0) {
          this.noTableData = true;
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        } else {
          this.noTableData = false;
        }

        const tableData = data['data'];
        const totalFooterData = dataTotal['data'];

        const arr = [];
        tableData.forEach((row: object) => {
          const ak = row['accounting_key'];
          delete row['accounting_key'];
          row['accounting_key'] = moment(ak, 'YYYYMMDD').format('MMMM-YYYY');
          if (this.reportType == 'isDetailed') {
            const tk = row['time_key'];
            delete row['time_key'];
            row['time_key'] = moment(tk, 'YYYYMMDD').format('MM-DD-YYYY');
          }
          let obj = {};
          obj = {
            data: row
          };
          arr.push(obj);
        });
        if (totalFooterData[0] !== undefined) {
          for (const key in totalFooterData[0]) {
            if (this.tableMatColDef.some(o => o.field == key)) {

              this.tableMatColDef.find(x => x.field == key).footerTotal =
                totalFooterData[0][key];

            }
          }
        } else {
          this.aggTableJson['footerColumns'].forEach(c => {
            if (this.tableMatColDef.some(o => o.field == c.field)) {
              c['footerTotal'] = 0;
            }
          });
        }
        this.aggTableData = <TreeNode[]>arr;
        this.aggTableJson['totalRecords'] = tableData.length;
        this.aggTableJson['loading'] = false;
        // this.addReportTotal(tableData);
      });
    });
  }

  exportTable(table, fileFormat) {
    if (this.aggTableData.length == 0) {
      this.confirmationService.confirm({
        message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
        header: 'Information',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
        },
        reject: () => {
        }
      });
    } else {
      if (this.exportRequest['sendEmail'].length > 0) {
        this.toastService.displayToast({
          severity: 'info',
          summary: 'Export Report',
          detail:
            'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
          life: 10000
        });
        const reportTotals = {};
        const totalData = {};
        const grpBysFilter = [];

        if (this.reportType == 'isDetailed') {
          grpBysFilter.push('time_key');

        }
        grpBysFilter.push('accounting_key', 'station_group', 'derived_station_rpt');
        // grpBysFilter = grpBysFilter.concat(this.filtersApplied['groupby'].map(e => e.key));

        const tableReq = {
          dimensions: grpBysFilter,
          metrics: [
            'gross_revenue',
            'dp_impressions',
            'dfp_adserver_impressions',
            'station_revenue_share_distribution',
            'total_recoupable_expenses'
          ],
          derived_metrics: ['gross_ecpm', 'client_net_ecpm', 'client_net_share'],
          timeKeyFilter: this.libServ.deepCopy(
            this.filtersApplied['timeKeyFilter']
          ),
          filters: this.libServ.deepCopy(this.filtersApplied['filters']),
          groupByTimeKey: {
            key: this.reportType == 'isDetailed' ? ['time_key', 'accounting_key'] : ['accounting_key'],
            interval: 'daily'
          },
          gidGroupBy: ['station_group', 'derived_station_rpt'],
          orderBy: [
            { key: 'accounting_key', opcode: 'asc' }

            // { key: 'derived_station_rpt', opcode: 'asc' }
          ],
          limit: '',
          offset: ''
        };


        this.dataFetchServ.getRevMgmtData(tableReq).subscribe(data => {
          if (data['status'] === 0) {
            this.noTableData = true;
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Server Error',
              detail: 'Please refresh the page'
            });
            console.log(data['status_msg']);
            return;
          } else {
            this.noTableData = false;
          }

          const tableData = data['data'];

          tableData.forEach(response => {

            const tk = response['accounting_key'];
            delete response['accounting_key'];
            response['accounting_key'] = moment(tk, 'YYYYMMDD').format('MMMM/YYYY');

            const key = response['station_group'];

            if (reportTotals.hasOwnProperty(key)) {
              totalData[key].push(response);

              reportTotals[key]['dp_impressions'] =
                reportTotals[key]['dp_impressions'] +
                (response['dp_impressions'] ? response['dp_impressions'] : 0);
              reportTotals[key]['station_revenue_share_distribution'] =
                reportTotals[key]['station_revenue_share_distribution'] +
                (response['station_revenue_share_distribution']
                  ? response['station_revenue_share_distribution']
                  : 0);

              reportTotals[key]['client_net_ecpm'] = (reportTotals[key]['dp_impressions']
                ? (reportTotals[key]['station_revenue_share_distribution'] /
                  reportTotals[key]['dp_impressions']) *
                1000
                : 0);
              reportTotals[key]['gross_revenue'] =
                reportTotals[key]['gross_revenue'] +
                (response['gross_revenue'] ? response['gross_revenue'] : 0);
              // reportTotals[key]['gross_ecpm'] = reportTotals[key]['gross_ecpm'] + (response['gross_ecpm'] ? response['gross_ecpm'] : 0);
              reportTotals[key]['gross_ecpm'] = reportTotals[key]['dp_impressions']
                ? (reportTotals[key]['gross_revenue'] /
                  reportTotals[key]['dp_impressions']) *
                1000
                : 0;
              reportTotals[key]['total_recoupable_expenses'] =
                reportTotals[key]['total_recoupable_expenses'] +
                (response['total_recoupable_expenses']
                  ? response['total_recoupable_expenses']
                  : 0);
            } else {
              totalData[key] = [];
              totalData[key].push(response);
              reportTotals[key] = {
                accounting_key: 'Report Total',
                dp_impressions: response['dp_impressions'],
                station_revenue_share_distribution:
                  response['station_revenue_share_distribution'],
                client_net_ecpm: response['client_net_ecpm'],
                // client_net_share: response['client_net_share']
                //   ? response['client_net_share']
                //   : 0,
                client_net_share: response['client_net_share'],

                gross_revenue: response['gross_revenue'],
                gross_ecpm: response['gross_ecpm'],
                total_recoupable_expenses: response['total_recoupable_expenses']
              };
            }
          });

          // this.exportReportData = {
          //   data: totalData,
          //   footer: reportTotals
          // };
          const reportData = totalData;
          let columnDefs = this.libServ.deepCopy(this.tableDimColDef);
          columnDefs = columnDefs.concat(this.libServ.deepCopy(this.tableMatColDef));
          if (this.reportType == 'isSummary') {
            columnDefs.splice(0, 1);
          }
          const sheetDetailsarray = [];

          for (const key in reportData) {

            reportTotals[key]['client_net_share'] =
              (reportTotals[key]['station_revenue_share_distribution']
                ? ((reportTotals[key]['station_revenue_share_distribution'] /
                  (reportTotals[key]['gross_revenue'] - reportTotals[key]['total_recoupable_expenses'])) *
                  100)
                : 0);

            const keyData = this.libServ.deepCopy(reportData[key]);
            keyData.push(
              this.libServ.deepCopy(reportTotals[key])
            );
            const sheetDetails = {};
            sheetDetails['columnDef'] = columnDefs;
            sheetDetails['data'] = keyData;
            sheetDetails['sheetName'] = key.substring(0, 31);
            sheetDetails['isRequest'] = false;
            sheetDetails['request'] = {
              url: '',
              method: '',
              param: {
                timeKeyFilter: this.libServ.deepCopy(
                  this.filtersApplied['timeKeyFilter']
                )
              }
            };
            sheetDetails['disclaimer'] = [
              {
                position: 'bottom',
                label: 'Note: Data present in the table may vary over a period of time.',
                color: '#000000'
              }
            ];
            sheetDetails['totalFooter'] = {
              available: true,
              custom: false
            };
            sheetDetails['tableTitle'] = {
              available: false,
              label: 'Overall Distribution'
            };
            sheetDetails['image'] = [
              {
                available: true,
                path: environment.exportConfig.exportLogo,
                position: 'top'
              }
            ];

            sheetDetailsarray.push(sheetDetails);
          }
          this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
          this.exportRequest['fileName'] =
            'Overall Distribution ' + moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
          this.exportRequest['exportFormat'] = fileFormat;
          this.exportRequest['exportConfig'] = environment.exportConfig;
          this.dataFetchServ
            .getExportReportData(this.exportRequest)
            .subscribe(response => {
              console.log('response of table', response);
            });
        });
      } else {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Export Report',
          detail:
            'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
          life: 10000
        });
      }
    }

  }

  reloadAggTable() {
    this.displayAggTable = false;
    setTimeout(() => {
      this.displayAggTable = true;
    }, 0);
  }

  loadCards() {
    this.showCards = false;
    const apiUrl = this.mainCardsConfig['api']['url'];
    const cardsReq = this.libServ.deepCopy(
      this.mainCardsConfig['api']['request']
    );
    cardsReq['timeKeyFilter'] = this.libServ.deepCopy(
      this.filtersApplied['timeKeyFilter']
    );
    cardsReq['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);

    this.libServ.getDataFromDynamicAPI(apiUrl, cardsReq).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error - Contact to administator',
          detail: data
        });
        console.log(data['status_msg']);
        return;
      }
      if (!this.libServ.isEmptyObj(data['data'])) {
        this.cardsJson.map(o => {
          o['value'] = data['data'][0][o['field']];
        });
      } else {
        this.cardsJson.map(o => {
          o['value'] = 0;
        });
      }
      this.showCards = this.mainCardsConfig['display'];
    });
  }

  onLazyLoadAggTable(e: Event) {
    console.log('Lazy Agg', e);
  }

  openPopup(row, field) {
    const grpBys = [];
    const filtersApplied = this.libServ.deepCopy(this.filtersApplied);
    const fieldIdx = filtersApplied['groupby'].findIndex(
      e => e['key'] === field
    );
    filtersApplied['groupby'].forEach((grp, i) => {
      if (i <= fieldIdx) {
        if (
          filtersApplied['filters']['dimensions'].findIndex(
            e => e.key === grp['key']
          ) !== -1
        ) {
          filtersApplied['filters']['dimensions'].splice(
            filtersApplied['filters']['dimensions'].findIndex(
              e => e.key === grp['key']
            ),
            1
          );
        }
        filtersApplied['filters']['dimensions'].push({
          key: grp['key'],
          values:
            grp['key'] == 'accounting_key'
              ? [moment(row[grp['key']]).format('YYYYMMDD')]
              : [row[grp['key']]]
        });
      }
    });

    filtersApplied['groupby'] = grpBys;
    const data = {
      filters: filtersApplied,
      field: row[field],
      appName: this.exportRequest['appName'],
      email: this.exportRequest['sendEmail'],
      isExportReport: this.isExportReport
    };
    const ref = this.dialogService.open(
      PayoutPublisherDailyDataPopupComponent,
      {
        header: row[field] + ' Daily Distribution',
        contentStyle: { width: '80vw', overflow: 'auto' },
        data: data
      }
    );
    ref.onClose.subscribe((data: string) => { });
  }

  isHiddenColumn(col: Object) {
    return (
      this.aggTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }

  loadMonthlyChart(params) {
    params['filters']['dimensions'] = [
      { key: 'station_group', values: ['TitanTV Inc'] },
      { key: 'derived_station_rpt', values: ['antennaweb', 'titantv'] }
    ];
    this.showMonthlyChart = false;
    this.noDataMonthlyChart = false;
    this.dataFetchServ.getRevMgmtData(params).subscribe(data => {
      const chartData = data['data'];
      if (typeof chartData === 'undefined' || !chartData.length) {
        this.noDataMonthlyChart = true;
        return;
      } else {
        this.MonthlyBarChartJson = {
          chartTypes: [
            { key: 'bar', label: 'Bar Chart' },
            { key: 'bar', label: 'Stack Chart', stacked: true }
          ],
          chartData: {
            labels: [],
            datasets: []
          },
          chartOptions: {
            title: {
              display: true,
              text: 'Gross Revenue ($) by Month by Property'
            },
            legend: {
              display: true,
              // onClick: e => e.stopPropagation()
            },
            scales: {
              xAxes: [
                {
                  display: true,
                  scaleLabel: {
                    display: true,
                    labelString: 'Month-Year'
                  }
                }
              ],
              yAxes: [
                {
                  id: 'y-axis-0',

                  scaleLabel: {
                    display: true,
                    labelString: 'Gross Revenue'
                  },
                  position: 'left',
                  name: '2',
                  ticks: {
                    callback: (value, index, values) => {
                      return this.formatNumPipe.transform(value, '$', [0]);
                    }
                  }
                  // scaleFontColor: 'rgba(151,187,205,0.8)'
                }
              ]
            },
            tooltips: {
              mode: 'index',
              intersect: false,

              callbacks: {
                label: (tooltipItem, data) => {
                  const currentValue =
                    data.datasets[tooltipItem.datasetIndex].data[
                    tooltipItem.index
                    ];
                  // if (tooltipItem.datasetIndex !== 0) {
                  //   return `${
                  //     data.datasets[tooltipItem.datasetIndex].label
                  //     } : ${this.formatNumPipe.transform(
                  //       currentValue,
                  //       'number',
                  //       []
                  //     )}`;
                  // }

                  return `${
                    data.datasets[tooltipItem.datasetIndex].label
                    } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
                }
              }
            },
            plugins: {
              datalabels: false
            }
          },
          zoomLabel: false,
          chartWidth: '',
          chartHeight: '300px'
        };

        // if (typeof chartData === 'undefined' || !chartData.length) {
        //   this.noDataPageViewsMonthlyChart = true;
        //   return;
        // }
        const datesArr = Array.from(
          new Set(chartData.map(r => r['accounting_key']))
        );

        this.MonthlyBarChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MMM-YYYY')
        );
        const revArr = {};
        for (const key in this.sourcesJson) {
          revArr[key] = [];
        }
        datesArr.forEach(accounting_key => {
          chartData.forEach(r => {
            if (r['accounting_key'] === accounting_key) {
              revArr[r['derived_station_rpt']].push(r['gross_revenue']);
            }
          });
        });

        let i = 1;
        for (const key in this.sourcesJson) {
          this.MonthlyBarChartJson['chartData']['datasets'].push({
            label: this.sourcesJson[key]['DisplayName'],
            type: 'bar',
            yAxisID: 'y-axis-0',
            borderColor: this.chartColors[i],
            fill: false,
            backgroundColor: this.chartColors[i],
            data: revArr[key]
          });
          i++;
        }

        // this.MonthlyStackChartJson = this.libServ.deepCopy(
        //   this.MonthlyBarChartJson
        // );
        // this.MonthlyStackChartJson['chartTypes'] = [
        //   { key: 'bar', label: 'Line Chart', stacked: true }
        // ];
        // this.MonthlyStackChartJson['chartOptions']['scales']['yAxes'][
        //   'stacked'
        // ] = true;
        setTimeout(() => {
          this.showMonthlyChart = true;
        }, 0);
      }
    });
  }

  /* --------------------- On Filter Apply Button Clicked --------------------- */

  onFiltersApplied(filterData: object) {
    console.log('filterData', filterData);
    this.filtersApplied['filters']['dimensions'] = [];

    // tslint:disable-next-line: forin
    for (const k in filterData['filter']['dimensions']) {
      if (filterData['filter']['dimensions'][k].length !== 0) {
        this.filtersApplied['filters']['dimensions'].push({
          key: k,
          values: filterData['filter']['dimensions'][k]
        });
      }
    }

    this.filtersApplied['groupby'] = filterData['groupby'];
    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];

    this.tableReq = {
      dimensions: [],
      metrics: [
        'gross_revenue',
        'dp_impressions',
        'dfp_adserver_impressions',
        'station_revenue_share_distribution',
        'total_recoupable_expenses'
      ],
      derived_metrics: ['gross_ecpm', 'client_net_ecpm', 'client_net_share'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: this.reportType == 'isDetailed' ? ['time_key', 'accounting_key'] : ['accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys(),
      orderBy: [
        { key: 'accounting_key', opcode: 'asc' }

        // { key: 'derived_station_rpt', opcode: 'asc' }
      ],
      limit: '',
      offset: ''
    };
    this.loadTableData(this.tableReq);

    const grpBys = this.getGrpBys();

    if (this.mainCardsConfig['display']) {
      this.mainCardsConfig['api']['request']['gidGroupBy'] = grpBys;
      this.loadCards();
    }

    const loadGraph = {
      dimensions: ['time_key'],
      metrics: [
        'gross_revenue',
        'dp_impressions',
        'dfp_adserver_impressions',
        'station_revenue_share_distribution',
        'total_recoupable_expenses'
      ],
      derived_metrics: ['gross_ecpm', 'client_net_ecpm', 'client_net_share'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: { key: ['time_key'], interval: 'daily' },
      gidGroupBy: grpBys.length > 0 ? grpBys : ['source'],
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: '',
      isTable: false
    };

    this.loadMainLineChart(loadGraph);
  }

  onGlobalSearchChanged(searchValue, tableColDef) {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    this.timeout = setTimeout(() => {
      this.tableReq['filters']['globalSearch'] = {
        dimensions: [],
        value: searchValue
      };
      tableColDef.forEach(element => {
        this.tableReq['filters']['globalSearch']['dimensions'].push(
          element.field
        );
      });
      let grpBysFilter = [];
      if (this.reportType == 'isDetailed') {
        grpBysFilter.push('time_key');
      }
      grpBysFilter.push('accounting_key');
      grpBysFilter = grpBysFilter.concat(this.filtersApplied['groupby'].map(e => e.key));

      grpBysFilter.forEach(ele => {
        this.tableReq['filters']['globalSearch']['dimensions'].push(
          ele
        );
      });
      this.loadTableData(this.tableReq);
    }, 3000);
  }

  onApplyFilterClicked(filterData: any[]) {
    if (typeof filterData != 'undefined') {
      const MonthlyChartReq = {
        dimensions: ['accounting_key', 'derived_station_rpt'],
        metrics: ['gross_revenue'],
        derived_metrics: [],
        timeKeyFilter: {
          time_key1: moment(filterData[0]).format('YYYYMMDD'),
          time_key2: moment(filterData[1]).format('YYYYMMDD')
        },
        filters: this.libServ.deepCopy(this.filtersApplied['filters']),
        groupByTimeKey: { key: ['accounting_key'], interval: 'daily' },
        gidGroupBy: ['station_group', 'derived_station_rpt'],
        orderBy: [{ key: 'accounting_key', opcode: 'asc' }],
        limit: '',
        offset: ''
      };
      this.loadMonthlyChart(MonthlyChartReq);
    }
  }

  getGrpBys() {
    let grpBys = this.filtersApplied['groupby'].map(e => e.key);
    // let grpBys = [];
    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          grpBys.concat(
            this.filtersApplied['filters']['dimensions']
              .filter(f => f.values.length)
              .map(m => m.key)
          )
        )
      );
    }
    return grpBys;
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['name']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    } else if (type === 'ppt') {
      const chartElements = document.getElementsByTagName('ym-charts');
      const slides = [];
      [].forEach.call(chartElements, (el, index) => {
        const c = el.getElementsByTagName('canvas')[0];

        slides.push({
          title: `${this.appConfig['name']}`,
          base64data: this.libServ.canvasToImage('white', c),
          width: 8,
          height: 3
        });

        if (index === chartElements.length - 1) {
          const pptConfig = {
            filename: 'Charts',
            logo: {
              visible: true,
              path: '/assets/images/ym.png',
              width: 0.53125,
              height: 0.458333
            },
            slides: slides
          };
          this.pptExport.exportPPT(pptConfig);
        }
      });
    }
    return false;
  }

  loadMainLineChart(params) {
    const colors = this.libServ.dynamicColors(2);
    const defaultJson = {
      chartTypes: [{ key: 'line', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'VAST Errors',
            type: 'line',
            borderColor: colors[1],
            fill: false,
            backgroundColor: colors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: ''
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: ''
              },
              position: 'left',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', []);
                }
              }
            }
          ]
        },
        tooltips: {
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };
    this.ImpressionsLineChartJson = this.libServ.deepCopy(defaultJson);
    this.RevenueLineChartJson = this.libServ.deepCopy(defaultJson);
    this.EcpmLineChartJson = this.libServ.deepCopy(defaultJson);

    this.showImpressionsLineChart = false;
    this.showRevenueLineChart = false;
    this.showEcpmLineChart = false;
    this.dataFetchServ.getRevMgmtData(params).subscribe(data => {
      const chartData = data['data'] as [];
      if (typeof chartData === 'undefined' || !chartData.length) {
        this.noDataImpressionsLineChart = true;
        this.noDataRevenueLineChart = true;
        this.noDataEcpmLineChart = true;
      } else {
        const datesArr = Array.from(
          new Set(chartData.map(r => r['time_key']))
        );

        this.ImpressionsLineChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );

        this.RevenueLineChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );

        this.EcpmLineChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );

        const impArr = [];
        const revArr = [];
        const ecpmArr = [];
        datesArr.forEach(time_key => {
          chartData.forEach(r => {
            if (r['time_key'] === time_key) {
              impArr.push(r['dp_impressions']);
              revArr.push(r['station_revenue_share_distribution']);
              ecpmArr.push(r['client_net_ecpm']);
            }
          });
        });

        this.ImpressionsLineChartJson['chartOptions']['tooltips'] = {
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
            }
          }
        };

        this.ImpressionsLineChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
          callback: (value, index, values) => {
            return this.formatNumPipe.transform(value, 'number', []);
          }
        };

        this.ImpressionsLineChartJson['chartData']['datasets'][0]['label'] = 'Billable Impressions';
        this.RevenueLineChartJson['chartData']['datasets'][0]['label'] = 'Client Net Revenue Share';
        this.EcpmLineChartJson['chartData']['datasets'][0]['label'] = 'Client Net eCPM';

        this.ImpressionsLineChartJson['chartOptions']['title']['text'] = 'Billable Impressions';
        this.RevenueLineChartJson['chartOptions']['title']['text'] = 'Client Net Revenue Share';
        this.EcpmLineChartJson['chartOptions']['title']['text'] = 'Client Net eCPM';

        this.ImpressionsLineChartJson['chartOptions']['scales']['yAxes'][0]['scaleLabel']['labelString'] = 'Billable Impressions';
        this.RevenueLineChartJson['chartOptions']['scales']['yAxes'][0]['scaleLabel']['labelString'] = 'Client Net Revenue Share';
        this.EcpmLineChartJson['chartOptions']['scales']['yAxes'][0]['scaleLabel']['labelString'] = 'Client Net eCPM';



        this.ImpressionsLineChartJson['chartData']['datasets'][0]['data'] = impArr;
        this.RevenueLineChartJson['chartData']['datasets'][0]['data'] = revArr;
        this.EcpmLineChartJson['chartData']['datasets'][0]['data'] = ecpmArr;

        this.showImpressionsLineChart = true;
        this.showRevenueLineChart = true;
        this.showEcpmLineChart = true;
        this.noDataImpressionsLineChart = false;
        this.noDataRevenueLineChart = false;
        this.noDataEcpmLineChart = false;
      }
    });
  }

  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }

  sendEmail() {
    const imageConfig = {
      imgName: `${this.appConfig['name']}.png`,
      type: 'image/png'
    };

    this.htmltoimage
      .canvasImage(document.getElementById('capture'), imageConfig)
      .then(canvas => {
        this.dialogService.open(SendEmailComponent, {
          header: 'Email Screenshot',
          contentStyle: {
            'max-height': '80vh',
            width: '30vw',
            overflow: 'auto'
          },
          data: canvas.toDataURL('image/png')
        });
      });
  }

  changeReport() {
    console.log('report', this.reportType);
    this.tableReq = {
      dimensions: [],
      metrics: [
        'gross_revenue',
        'dp_impressions',
        'dfp_adserver_impressions',
        'station_revenue_share_distribution',
        'total_recoupable_expenses'
      ],
      derived_metrics: ['gross_ecpm', 'client_net_ecpm', 'client_net_share'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: this.reportType == 'isDetailed' ? ['time_key', 'accounting_key'] : ['accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys(),
      orderBy: [
        { key: 'accounting_key', opcode: 'asc' }

        // { key: 'derived_station_rpt', opcode: 'asc' }
      ],
      limit: '',
      offset: ''
    };
    this.loadTableData(this.tableReq);
  }

  openExportPopup(row, field) {

    const grpBys = [];
    const filtersApplied = this.libServ.deepCopy(this.filtersApplied);
    const fieldIdx = filtersApplied['groupby'].findIndex(
      e => e['key'] === field
    );
    filtersApplied['groupby'].forEach((grp, i) => {
      if (i <= fieldIdx) {
        if (
          filtersApplied['filters']['dimensions'].findIndex(
            e => e.key === grp['key']
          ) !== -1
        ) {
          filtersApplied['filters']['dimensions'].splice(
            filtersApplied['filters']['dimensions'].findIndex(
              e => e.key === grp['key']
            ),
            1
          );
        }
        filtersApplied['filters']['dimensions'].push({
          key: grp['key'],
          values:
            grp['key'] == 'accounting_key'
              ? [moment(row[grp['key']]).format('YYYYMMDD')]
              : [row[grp['key']]]
        });
      }
    });

    filtersApplied['groupby'] = grpBys;
    const data = {
      filters: filtersApplied,
      field: row[field],
      appName: this.exportRequest['appName'],
      email: this.exportRequest['sendEmail'],
      isExportReport: this.isExportReport
    };
    const ref = this.dialogService.open(PublisherExportPopupComponent, {
      header: row[field] + ' Daily Distribution',
      contentStyle: { width: '80vw', overflow: 'auto' },
      data: data
    });
    ref.onClose.subscribe((data: string) => { });

    return false;
  }
}
