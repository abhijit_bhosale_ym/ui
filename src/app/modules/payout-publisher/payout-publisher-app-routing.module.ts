import { Routes } from '@angular/router';
import { PayoutPublisherAppComponent } from './payout-publisher-app.component';

export const PayoutPublisherAppRoutes: Routes = [
  {
    path: '',
    component: PayoutPublisherAppComponent
  }
];
