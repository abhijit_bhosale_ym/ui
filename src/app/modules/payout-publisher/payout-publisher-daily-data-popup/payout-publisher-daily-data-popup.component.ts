import { Component, OnInit } from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { FetchApiDataService } from '../fetch-api-data.service';
import * as moment from 'moment';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { DynamicDialogConfig } from 'primeng';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-payout-publisher-daily-data-popup',
  templateUrl: './payout-publisher-daily-data-popup.component.html',
  styleUrls: ['./payout-publisher-daily-data-popup.component.scss']
})
export class PayoutPublisherDailyDataPopupComponent implements OnInit {
  lineChartJson: object;
  showLineChart = false;
  filtersApplied: object;
  appName: string;
  email: [];
  tabChangedFlag = false;

  tableDataReq: object;
  tableData: TreeNode[];
  tableColumnDef: any[];
  dimColDef: any[];
  tableJson: Object;
  timeout: any;
  exportRequest: ExportRequest = <ExportRequest>{};
  isExportReport = false;
  constructor(
    private libServ: CommonLibService,
    private formatNumPipe: FormatNumPipe,
    private dataFetchServ: FetchApiDataService,
    private config: DynamicDialogConfig,
    private exportService: ExportdataService,
    private toastService: ToastService,
    private confirmationService: ConfirmationService
  ) {
    this.filtersApplied = this.config.data['filters'];
    this.appName = this.config.data['appName'];
    this.email = this.config.data['email'];
  }

  ngOnInit() {
    this.isExportReport = this.config.data['isExportReport'];
    this.dimColDef = [
      {
        field: 'time_key',
        displayName: 'Date',
        format: 'date',
        width: '150',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'station_group',
        displayName: 'Media Group',
        format: '',
        width: '200',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'derived_station_rpt',
        displayName: 'Property',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'dp_impressions',
        displayName: 'Billable Imps.',
        format: 'number',
        width: '170',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      // {
      //   field: 'dfp_impressions',
      //   displayName: 'DFP Imps.',
      //   format: 'number',
      //   width: '170',
      //   exportConfig: {
      //     format: 'number',
      //     styleinfo: {
      //       thead: 'default',
      //       tdata: 'white'
      //     }
      //   },
      //   formatConfig: [],
      //   options: {
      //     editable: false,
      //     colSearch: false,
      //     colSort: true,
      //     resizable: true,
      //     movable: true
      //   },
      //   footerTotal: '-'
      // },
      {
        field: 'gross_revenue',
        displayName: 'Gross Revenue',
        width: '155',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'gross_ecpm',
        displayName: 'Gross eCPM',
        format: '$',
        width: '150',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'station_revenue_share_distribution',
        displayName: 'Client Net Revenue share',
        format: '$',
        width: '225',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'client_net_ecpm',
        displayName: 'Client Net eCPM',
        format: '$',
        width: '165',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'client_net_share',
        displayName: 'Client Net Share',
        format: 'percentage',
        width: '165',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      }
    ];
    this.tableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '400px',
      totalRecords: 1000,

      columns: this.dimColDef,
      selectedColumns: this.dimColDef.slice(1),
      frozenCols: [...this.dimColDef.slice(0, 1)],
      frozenWidth:
        this.dimColDef
          .slice(0, 1)
          .reduce((tot, cur) => tot + parseInt(cur.width), 0) + 'px',

      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: false,
      overallSearch: true,
      columnSearch: false
    };

    this.tableDataReq = {
      dimensions: ['station_group', 'time_key', 'derived_station_rpt'],
      metrics: [
        'gross_revenue',
        'dp_impressions',
        'dfp_adserver_impressions',
        'station_revenue_share_distribution',
        'total_recoupable_expenses'
      ],
      derived_metrics: ['gross_ecpm', 'client_net_ecpm', 'client_net_share'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['station_group', 'derived_station_rpt'],
      orderBy: [{ key: 'time_key', opcode: 'desc' }],
      limit: '',
      offset: ''
    };
    this.loadTable(this.tableDataReq);
  }

  loadTable(tableDataReq) {
    this.tableJson['loading'] = true;

    // let grpBys = this.filtersApplied['groupby'].map(e => e.key);
    // if (
    //   this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
    //   this.filtersApplied['filters']['dimensions'].length === 0
    // ) {
    //   this.filtersApplied['filters']['dimensions'] = [];
    // } else {
    //   grpBys = Array.from(
    //     new Set(
    //       this.filtersApplied['filters']['dimensions']
    //         .filter(f => f.values.length)
    //         .map(m => m.key)
    //         .concat(grpBys)
    //     )
    //   );
    // }

    // this.dimColDef.reverse().forEach(c => {
    //   if (grpBys.includes(c.field)) {
    //     this.tableColumnDef.unshift(c);
    //   }
    // });
    // this.tableColumnDef.unshift({
    //   field: 'time_key',
    //   displayName: 'Date',
    //   format: 'date',
    //   width: '250',
    //   exportConfig: {
    //     format: 'string',
    //     styleinfo: {
    //       thead: 'default',
    //       tdata: 'white'
    //     }
    //   },
    //   formatConfig: [],
    //   options: {
    //     editable: false,
    //     colSearch: false,
    //     colSort: true,
    //     resizable: true,
    //     movable: false
    //   }
    // });
    // grpBys.push('time_key');
    // For Dev purpose
    // this.tableDataReq['gidGroupBy'] = [
    //   'ad_size',
    //   'ad_type',
    //   'ad_units',
    //   'bidding_type',
    //   'device_category',
    //   'geography',
    //   'sales_channel',
    //   'sales_type',
    //   'site',
    //   'source'
    // ];

    this.dataFetchServ.getRevMgmtData(tableDataReq).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }
      const tableData = data['data'];

      const arr = [];
      tableData.forEach((row: object) => {
        const obj = {
          data: row
        };
        arr.push(obj);
      });

      this.tableData = <TreeNode[]>arr;
      this.tableJson['totalRecords'] = data['totalItems'];
      this.tableJson['loading'] = false;
    });
  }

  exportTablePopup(fileFormat) {
    const req = this.libServ.deepCopy(this.tableDataReq);
    this.exportRequest['sendEmail'] = this.email;
    this.exportRequest['appName'] = this.appName;
    if (this.tableData.length == 0) {
      this.confirmationService.confirm({
        message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
        header: 'Information',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
        },
        reject: () => {
        }
      });
    } else {
      if (this.exportRequest['sendEmail'].length > 0) {
        this.toastService.displayToast({
          severity: 'info',
          summary: 'Export Report',
          detail:
            'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
          life: 10000
        });
        const columnDefs = this.libServ.deepCopy(this.dimColDef);

        req['limit'] = '';
        req['offset'] = '';
        req['isTable'] = false;

        const sheetDetails = {};
        sheetDetails['columnDef'] = columnDefs;
        sheetDetails['data'] = [];
        sheetDetails['sheetName'] = 'Daily Data Distribution';
        sheetDetails['isRequest'] = true;
        sheetDetails['request'] = {
          url: '/api/flask/payout/cassandradata',
          method: 'POST',
          param: req
        };
        sheetDetails['disclaimer'] = [
          {
            position: 'bottom',
            label: 'Note: Data present in the table may vary over a period of time.',
            color: '#000000'
          }
        ];
        sheetDetails['totalFooter'] = {
          available: false,
          custom: false
        };
        sheetDetails['tableTitle'] = {
          available: false,
          label: 'Daily Data Distribution'
        };
        sheetDetails['image'] = [
          {
            available: true,
            path: environment.exportConfig.exportLogo,
            position: 'top'
          }
        ];
        const sheetDetailsarray = [];
        sheetDetailsarray.push(sheetDetails);
        this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
        this.exportRequest['fileName'] =
          this.config.data['field'] +
          ' Daily Distribution ' +
          moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
        this.exportRequest['exportFormat'] = fileFormat;
        this.exportRequest['exportConfig'] = environment.exportConfig;
        this.dataFetchServ
          .getExportReportData(this.exportRequest)
          .subscribe(response => {
            console.log('response of table', response);
          });
      } else {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Export Report',
          detail:
            'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
          life: 10000
        });
      }
    }
    return false;
  }

  isHiddenColumn(col: Object) { }

  loadLineChart() {
    // let grpBys = this.filtersApplied['groupby'].map(e => e.key);
    // if (
    //   this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
    //   this.filtersApplied['filters']['dimensions'].length === 0
    // ) {
    //   this.filtersApplied['filters']['dimensions'] = [];
    // } else {
    //   grpBys = Array.from(
    //     new Set(
    //       this.filtersApplied['filters']['dimensions']
    //         .filter(f => f.values.length)
    //         .map(m => m.key)
    //         .concat(grpBys)
    //     )
    //   );
    // }

    const lineChartReq = {
      dimensions: ['time_key'],
      metrics: ['dp_impressions', 'station_revenue_share_distribution'],
      derived_metrics: ['client_net_ecpm'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['station_group', 'derived_station_rpt'],
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };
    // For Dev purpose
    // lineChartReq['gidGroupBy'] = [
    //   'ad_size',
    //   'ad_type',
    //   'ad_units',
    //   'bidding_type',
    //   'device_category',
    //   'geography',
    //   'sales_channel',
    //   'sales_type',
    //   'site',
    //   'source'
    // ];
    const colors = this.libServ.dynamicColors(2);
    this.lineChartJson = {
      chartTypes: [{ key: 'bar', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'eCPM',
            type: 'line',
            yAxisID: 'y-axis-0',

            borderColor: colors[0],
            fill: false,
            backgroundColor: colors[0],
            data: []
          },
          {
            label: 'Revenue',
            type: 'bar',
            yAxisID: 'y-axis-1',

            borderColor: colors[1],
            fill: false,
            backgroundColor: colors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: ' Client Net Revenue Share vs Client Net eCPM'
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: 'Client Net eCPM ($)'
              },
              position: 'right',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', []);
                }
              }
              // scaleFontColor: "rgba(151,137,200,0.8)"
            },
            {
              id: 'y-axis-1',
              scaleLabel: {
                display: true,
                labelString: 'Client Net Revenue Share ($)'
              },
              position: 'left',
              name: '2',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', []);
                }
              }
              // scaleFontColor: "rgba(151,187,205,0.8)"
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    this.showLineChart = false;
    this.dataFetchServ.getRevMgmtData(lineChartReq).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }
      const chartData = data['data'];

      const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));

      this.lineChartJson['chartData']['labels'] = datesArr.map(d =>
        moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
      );

      const revArr = [];
      const ecpmArr = [];
      datesArr.forEach(time_key => {
        chartData.forEach(r => {
          if (r['time_key'] === time_key) {
            revArr.push(r['station_revenue_share_distribution']);
            ecpmArr.push(r['client_net_ecpm']);
          }
        });
      });
      this.lineChartJson['chartData']['datasets'][0]['data'] = ecpmArr;
      this.lineChartJson['chartData']['datasets'][1]['data'] = revArr;
      this.showLineChart = true;
    });
  }

  tabChanged(e) {
    if (!this.tabChangedFlag) {
      this.loadLineChart();
      this.tabChangedFlag = true;
    }
  }

  onGlobalSearchChanged(searchValue, tableColDef) {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    this.timeout = setTimeout(() => {
      this.tableDataReq['filters']['globalSearch'] = {
        dimensions: [],
        value: searchValue
      };
      tableColDef.forEach(element => {
        this.tableDataReq['filters']['globalSearch']['dimensions'].push(
          element.field
        );
      });
      this.loadTable(this.tableDataReq);
    }, 3000);
  }


  chartSelected(e) { }
}
