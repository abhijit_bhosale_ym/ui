import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayoutPublisherDailyDataPopupComponent } from './payout-publisher-daily-data-popup.component';

describe('PayoutPublisherDailyDataPopupComponent', () => {
  let component: PayoutPublisherDailyDataPopupComponent;
  let fixture: ComponentFixture<PayoutPublisherDailyDataPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PayoutPublisherDailyDataPopupComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayoutPublisherDailyDataPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
