import { Component, OnInit, OnDestroy } from '@angular/core';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { CommonLibService } from 'src/app/_services';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';
import { FetchApiDataService } from './fetch-api-data.service';
import { SupplyPerformanceDailyDataPopupComponent } from './supply-performance-daily-data-popup/supply-performance-daily-data-popup.component';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { Subscription } from 'rxjs';
import { SendEmailComponent } from '../send-email/send-email.component';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';

@Component({
  selector: 'ym-supply-performance-app',
  templateUrl: './supply-performance-app.component.html',
  styleUrls: ['./supply-performance-app.component.scss']
})
export class SupplyPerformanceAppComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;
  private appConfigObs1: Subscription;
  private appConfigObs2: Subscription;
  appConfig: object = {};
  lastUpdatedOn: Date;

  cardsJson = [];
  showCards = true;

  aggTableData: TreeNode[];
  noTableData = false;
  dimColDef: any[];
  metricColDef: any[];
  aggTableColumnDef: any[];
  aggTableJson: Object;
  displayAggTable = false;
  toggleView = true;
  filtersApplied: object = {};
  exportRequest: ExportRequest = <ExportRequest>{};
  showhide = true;
  tableReq: object;
  timeout: any;
  isExportReport = false;

  showMainLineChart = false;
  noDataMainLineChart = false;
  noDataMetricChart = false;
  mainLineChartJson: object;
  defaultChartsJson: object;
  metricChartJson: object;
  dimensionsDropdown: any[];
  selectedDimension: any[];
  metricsDropdown: any[];
  selectedMetric: any[];
  showMetricChart = false;


  chartsMultiselectButton: object = {
    data: [
      {
        label: 'Paid Impressions',
        value: 'paid_impressions'
      },
      {
        label: 'Clicks',
        value: 'clicks'
      },
      {
        label: 'CTR',
        value: 'ctr'
      },
      {
        label: 'Viewability',
        value: 'viewability'
      },
      {
        label: 'Audience Count',
        value: 'audience_count'
      },
    ],
    model: ['paid_impressions']
  };

  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private htmltoimage: HtmltoimageService,
    private exportService: ExportdataService,
    private pptExport: ExportPptService,
    private confirmationService: ConfirmationService,
    private formatNumPipe: FormatNumPipe

  ) { }

  ngOnInit() {

    this.defaultChartsJson = {
      chartTypes: [
        { key: 'line', label: 'Line Chart' }
      ],
      chartData: {
        labels: [],
        datasets: []
      },
      chartOptions: {
        title: {
          display: true,
          text: ''
        },
        legend: {
          display: false
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              display: true,
              scaleLabel: {
                display: true
              }
            }
          ]
        },
        pan: {
          enabled: true,
          mode: 'x'
        },
        zoom: {
          enabled: true,
          mode: 'x'
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: true,
      chartWidth: '',
      chartHeight: '300px'
    };
    this.dimensionsDropdown = [
      {
        field: 'campaign',
        displayName: 'Campaign'
      },
      {
        field: 'advertiser',
        displayName: 'Advertiser'
      },
      {
        field: 'advertiser_category',
        displayName: 'Advertiser Category'
      },
      {
        field: 'ad_type',
        displayName: 'Ad type'
      },
      {
        field: 'Ad Unit Name',
        displayName: 'ad_unit_name'
      },
      {
        field: 'ad_size',
        displayName: 'Ad size'
      },
      {
        field: 'device_type',
        displayName: 'Device Type'
      },
      {
        field: 'audience',
        displayName: 'audience'
      }
    ];
    this.selectedDimension = this.dimensionsDropdown[0];

    this.metricsDropdown = [
      {
        field: 'total_impressions',
        displayName: 'Total Impressions',
        derived: false
      },
      {
        field: 'paid_impressions',
        displayName: 'Paid Impressions',
        derived: false
      },
      {
        field: 'clicks',
        displayName: 'Clicks',
        derived: false
      },
      {
        field: 'ctr',
        displayName: 'CTR',
        derived: false,
        metrics: []
      },
      {
        field: 'viewability',
        displayName: 'Viewability',
        derived: false,
        metrics: []
      },
      {
        field: 'audience_count',
        displayName: 'Audience Count',
        derived: true,
        metrics: []
      }
    ];
    this.selectedMetric = this.metricsDropdown[0];


    this.dimColDef = [
      {
        field: 'campaign',
        displayName: 'Campaign',
        format: '',
        width: '250',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'advertiser',
        displayName: 'Advertiser',
        format: '',
        width: '140',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'advertiser_category',
        displayName: 'Advertiser Category',
        format: '',
        width: '165',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'ad_type',
        displayName: 'Ad type',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'ad_type',
        displayName: 'Ad type',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'ad_unit_name',
        displayName: 'Ad Unit Name',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'ad_size',
        displayName: 'Ad Size',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'device_type',
        displayName: 'Device Type',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
        {
          field: 'audience',
          displayName: 'Audience',
          format: '',
          width: '150',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
      }

    ];

    this.metricColDef = [
      {
        field: 'total_impressions',
        displayName: 'Total Impressions',
        format: 'number',
        visible: false,
        width: '170',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'paid_impressions',
        displayName: 'Paid Impressions',
        format: 'number',
        width: '170',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'clicks',
        displayName: 'Clicks',
        format: 'number',
        width: '170',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
          field: 'ctr',
          displayName: 'CTR',
          format: 'percentage',
          width: '110',
          exportConfig: {
            format: 'percentage',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [2],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: true
          }
        },
        {
          field: 'viewability',
          displayName: 'Viewability',
          format: 'percentage',
          width: '110',
          exportConfig: {
            format: 'percentage',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [2],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: true
          }
        },

        {
          field: 'video_completion_rate',
          displayName: 'Video-Completion Rate',
          format: 'percentage',
          width: '110',
          exportConfig: {
            format: 'percentage',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [2],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: true
          }
        },

        {
          field: 'audience_count',
          displayName: 'Audience Count',
          format: 'number',
          width: '170',
          exportConfig: {
            format: 'number',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: true
          },
          footerTotal: '-'
        }
    ];
    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      reload: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '200px',
      totalRecords: 1000,

      // columns: this.aggTableColumnDef,
      // selectedColumns: this.aggTableColumnDef,
      // frozenCols: [],
      // frozenWidth: '0px',

      // columns: this.aggTableColumnDef.slice(5),
      // selectedColumns: this.aggTableColumnDef.slice(5),
      // frozenCols: [...this.aggTableColumnDef.slice(0, 5)],
      // frozenWidth:
      //   this.aggTableColumnDef
      //     .slice(0, 5)
      //     .reduce((tot, cur) => tot + parseInt(cur.width), 0) + 'px',

      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        console.log('appConfig', this.appConfig);
        this.isExportReport = this.appConfig['permissions'].some(
          o => o.name === 'adops360-export-reports'
        );
        this._titleService.setTitle( this.appConfig['displayName']);
        let startDate;
        let endDate;
        const date_config = this.appConfig['filter']['filterConfig']['filters'][
          'datePeriod'
        ][0];
        if ((date_config['defaultDate'][0]['value'] === 0 && date_config['defaultDate'][0]['period'] === 'month') && (moment().format('DD') === '01' || moment().format('DD') === '02')) {
          startDate = moment().subtract(1, 'months').startOf('month');
          endDate = moment().subtract(1, 'months').endOf('month');
        } else {
          if (
            date_config[
            'defaultDate'
            ][0]['startOf']
          ) {
            startDate = moment()
              .subtract(
                date_config['defaultDate'][0]['value'],
                date_config['defaultDate'][0]['period']
              )
              .startOf(
                date_config['defaultDate'][0]['period']
              );
          } else {
            startDate = moment().subtract(
              date_config['defaultDate'][0]['value'],
              date_config['defaultDate'][0]['period']
            );
          }
          endDate = moment().subtract(
            date_config[
            'defaultDate'
            ][1]['value'],
            date_config[
            'defaultDate'
            ][1]['period']
          );
        }

        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: startDate.format('YYYYMMDD'),
            time_key2: endDate.format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: this.appConfig['filter']['filterConfig']['groupBy'].filter(
            v => v.selected
          )
        };
        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        this.initialLoading();
      }
    });
  }

  initialLoading() {
    this.dataFetchServ
      .getLastUpdatedData(this.appConfig['id'])
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        this.lastUpdatedOn = moment(data).toDate();
      });

    this.tableReq = {
      dimensions: [],
      metrics: this.toggleView
        ? ['gross_revenue', 'dp_impressions', 'dfp_adserver_impressions', 'dp_total_requests']
        : [
          'gross_revenue',
          'dp_impressions',
          'dfp_adserver_impressions',
          'house_ads_impressions',
          'unfilled_impressions',
          'total_code_served_count'
        ],
      derived_metrics: this.toggleView
        ? ['discrepancy_adops', 'fill_rate_adops', 'cpm']
        : ['cpm', 'fill_rate_house'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: { key: [], interval: 'daily' },
      gidGroupBy: [],
      orderBy: [
        { key: this.filtersApplied['groupby'][0]['key'], opcode: 'asc' }
      ],
      limit: '',
      offset: ''
    };
    this.loadTableData(this.tableReq);
    const cardsReq = {
      dimensions: [],
      metrics: ['gross_revenue', 'dp_impressions', 'dfp_adserver_impressions'],
      derived_metrics: ['cpm'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: { key: [], interval: 'daily' },
      gidGroupBy: ['source'],
      orderBy: [],
      limit: '',
      offset: '',
      isTable: false
    };
//    this.loadCards(cardsReq);
this.loadMetricChart();
  }

  resetPagination(tt1) {
    if (typeof tt1 !== 'undefined') {
      tt1.reset();
    }
  }

  chartSelected(data: Event) {
    // console.log('Chart Selected', data);
  }

  isChartSelected(chartName) {
    return this.chartsMultiselectButton['model'].indexOf(chartName) !== -1;
  }

  metricChanged() {
    this.loadMetricChart();
  }

  reset(table) {
    table.reset();
    this.tableReq = {
      dimensions: [],
      metrics: this.toggleView
        ? ['gross_revenue', 'dp_impressions', 'dfp_adserver_impressions', 'dp_total_requests']
        : [
          'gross_revenue',
          'dp_impressions',
          'dfp_adserver_impressions',
          'house_ads_impressions',
          'unfilled_impressions',
          'total_code_served_count'
        ],
      derived_metrics: this.toggleView
        ? ['discrepancy_adops', 'fill_rate_adops', 'cpm']
        : ['cpm', 'fill_rate_house'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: { key: [], interval: 'daily' },
      gidGroupBy: [],
      orderBy: [
        { key: this.filtersApplied['groupby'][0]['key'], opcode: 'asc' }
      ],
      limit: '',
      offset: ''
    };
    this.loadTableData(this.tableReq);
  }

  loadTableData(tableReq) {
    const finalColDef = this.libServ.deepCopy(this.metricColDef);

    const grpBys = this.getGrpBys();

    tableReq['gidGroupBy'] = grpBys;
    tableReq['dimensions'] = [this.libServ.deepCopy(this.filtersApplied['groupby'][0]['key'])];
    tableReq['orderBy'] = [{ key: this.libServ.deepCopy(this.filtersApplied['groupby'][0]['key']), opcode: 'asc' }];
    tableReq['metrics'] = this.toggleView
      ? ['gross_revenue', 'dp_impressions', 'dfp_adserver_impressions', 'dp_total_requests']
      : [
        'gross_revenue',
        'dp_impressions',
        'dfp_adserver_impressions',
        'house_ads_impressions',
        'unfilled_impressions',
        'total_code_served_count'
      ];
    tableReq['derived_metrics'] = this.toggleView
      ? ['discrepancy_adops', 'fill_rate_adops', 'cpm']
      : ['cpm', 'fill_rate_house'];

    const grpBysFilter = this.filtersApplied['groupby'].map(e => e.key);

    grpBysFilter.reverse().forEach(grp => {
      finalColDef.unshift(this.dimColDef.find(e => e.field === grp));
    });

    this.aggTableColumnDef = finalColDef.slice();

    this.aggTableJson['columns'] = this.aggTableColumnDef.slice(
      grpBysFilter.length
    );
    this.aggTableJson['selectedColumns'] = this.aggTableColumnDef.slice(
      grpBysFilter.length
    );
    this.aggTableJson['frozenCols'] = [
      ...this.aggTableColumnDef.slice(0, grpBysFilter.length)
    ];
    this.aggTableJson['frozenWidth'] =
      this.aggTableColumnDef
        .slice(0, grpBysFilter.length)
        .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px';

    this.reloadAggTable();


    let data = {
      "data": [                        {
        "campaign": "Crate & Barrel Q4 2020",
        "advertiser": "Crate & Barrel",
        "advertiser_category": "Jewellery",
        "ad_type": "Display",
        "ad_unit_name": "AV",
        "ad_size": "300 x 600",
        "device _type": "Desktop",
        "audience": "2564",
        "total_impressions": " 96,318 ",
        "paid_impressions": " 94,218 ",
        "clicks": " 3,034 ",
        "ctr": "3.22",
        "viewability": "87%",
        "video_completion_rate": "67%",
        "audience_count": " 94,218 "
    },

    {
        "campaign": "Mariott Q4 2020",
        "advertiser": "Mariott",
        "advertiser_category": "Travel",
        "ad_type": "Display",
        "ad_unit_name": "FB AV",
        "ad_size": "300 x 250",
        "device _type": "Desktop",
        "audience": "3423",
        "total_impressions": " 185,237 ",
        "paid_impressions": " 155,237 ",
        "clicks": " 72,594 ",
        "ctr": "46.76",
        "viewability": "64%",
        "video_completion_rate": "40%",
        "audience_count": " 155,237 "
    },

    {
        "campaign": "Adidas Q4 2020",
        "advertiser": "Adidas",
        "advertiser_category": "Sports",
        "ad_type": "Rich Media",
        "ad_unit_name": "RON",
        "ad_size": "300 x 600",
        "device _type": "Desktop",
        "audience": "6985",
        "total_impressions": " 17,473 ",
        "paid_impressions": " 17,473 ",
        "clicks": " 6,173 ",
        "ctr": "35.33",
        "viewability": "59%",
        "video_completion_rate": "64%",
        "audience_count": " 17,473 "
    },
    {
        "campaign": "Volkswagen Q3 2020",
        "advertiser": "Volkswagen",
        "advertiser_category": "Automobile",
        "ad_type": "Interactive",
        "ad_unit_name": "RON",
        "ad_size": "300 x 250",
        "device _type": "Desktop",
        "audience": "1254",
        "total_impressions": " 361,298 ",
        "paid_impressions": " 351,298 ",
        "clicks": " 162,023 ",
        "ctr": "46.12",
        "viewability": "62%",
        "video_completion_rate": "82%",
        "audience_count": " 351,298 "
    }
],
      "totalItems": 30
    }
    const tableData = data['data'];
      const arr = [];
      tableData.forEach((row: object) => {
        for (let i = 0; i < grpBysFilter.length; i++) {
          if (i !== grpBysFilter.length - 1) {
            row[grpBysFilter[i]] = 'All';
          }
        }
        let obj = {};
        if (grpBysFilter.length === 1) {
          obj = {
            data: row
          };
        } else {
          obj = {
            data: row,
            children: [{ data: {} }]
          };
        }
        arr.push(obj);
      });

      this.aggTableData = <TreeNode[]>arr;
      this.aggTableJson['totalRecords'] = data['totalItems'];
      // setTimeout(() => {
      //   this.aggTableJson['loading'] = false;
      // }, 0);

//    this.aggTableJson['loading'] = true;
    // For Dev purpose
    // tableReq['gidGroupBy'] = [
    //   'ad_size',
    //   'ad_type',
    //   'ad_units',
    //   'bidding_type',
    //   'device_category',
    //   'geography',
    //   'sales_channel',
    //   'sales_type',
    //   'site',
    //   'source'
    // ];

    // this.dataFetchServ.getRevMgmtData(tableReq).subscribe(data => {
    //   if (data['status'] === 0) {
    //     this.noTableData = true;
    //     this.toastService.displayToast({
    //       severity: 'error',
    //       summary: 'Server Error',
    //       detail: 'Please refresh the page'
    //     });
    //     console.log(data['status_msg']);
    //     return;
    //   } else {
    //     this.noTableData = false;
    //   }
    //   const tableData = data['data'];
    //   const arr = [];
    //   tableData.forEach((row: object) => {
    //     for (let i = 0; i < grpBysFilter.length; i++) {
    //       if (i !== grpBysFilter.length - 1) {
    //         row[grpBysFilter[i]] = 'All';
    //       }
    //     }
    //     let obj = {};
    //     if (grpBysFilter.length === 1) {
    //       obj = {
    //         data: row
    //       };
    //     } else {
    //       obj = {
    //         data: row,
    //         children: [{ data: {} }]
    //       };
    //     }
    //     arr.push(obj);
    //   });

    //   this.aggTableData = <TreeNode[]>arr;
    //   this.aggTableJson['totalRecords'] = data['totalItems'];
    //   setTimeout(() => {
    //     this.aggTableJson['loading'] = false;
    //   }, 0);
    // });
  }

  reloadAggTable() {
    this.displayAggTable = false;
    setTimeout(() => {
      this.displayAggTable = true;
    }, 0);
  }

  loadMetricChart() {
    this.metricChartJson = this.libServ.deepCopy(this.defaultChartsJson);
    const selDimention = this.selectedDimension['field'];
    const filtersArr = this.filtersApplied['filters']['dimensions'].map(x => x.key);

  //  if (this.selectedMetric['derived']) {

    this.metricChartJson['chartOptions']['title'] = {
      display: true,
      text: `${this.selectedMetric['displayName']} Trends By ${this.selectedDimension['displayName']}`
    };

    this.metricChartJson['chartOptions']['scales'] = {
      xAxes: [
        {
          display: true,
          scaleLabel: {
            display: true,
            labelString: 'Date'
          }
        }
      ],
      yAxes: [
        {
          display: true,
          scaleLabel: {
            display: true,
            labelString: this.selectedMetric['displayName']
          },
          ticks: {
            callback: (label, index, labels) => {
              console.log("this.metricColDef.find((item) => item.field == this.selectedMetric['field']",this.metricColDef.find((item) => item.field == this.selectedMetric['field']));

              const field = this.metricColDef.find((item) => item.field == this.selectedMetric['field']);
              const value = this.formatNumPipe.transform(label, field.format, field.formatConfig);
              return value;
            }
          }
        }
      ]
    };

    this.metricChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue = data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          const label = data.datasets[tooltipItem.datasetIndex]['label'];
          const field = this.metricColDef.find((item) => item.field == this.selectedMetric['field']);
          const value = this.formatNumPipe.transform(currentValue, field.format, field.formatConfig);
          return `${label}: ${value}`;
        }
      }
    };

    this.metricChartJson['chartOptions']['legend'] = {
      display: true
    };

    this.showMetricChart = false;
    // this.dataFetchServ.getTableData(req).subscribe(data => {
    //   const resData = data as {};
    //   const chartData = resData['data'];
    //   this.noDataMetricChart = false;
    //   if (!chartData.length) {
    //     this.noDataMetricChart = true;
    //     return;
    //   }
    this.noDataMetricChart = false;
    let data = {
        "data": [
          {
            "campaign": "Crate & Barrel Q4 2020",
            "audience": "2564",
            "total_impressions": 96118,
            "paid_impressions": " 94,218 ",
            "clicks": " 3,034 ",
            "ctr": "3.22",
            "viewability": "87%",
            "video_completion_rate": "67%",
            "audience_count": " 94,218 ",
            "time_key" : 20201011
        },
        {
          "campaign": "Crate & Barrel Q4 2020",

          "audience": "2564",
          "total_impressions": 9618,
          "paid_impressions": " 94,218 ",
          "clicks": " 3,034 ",
          "ctr": "3.22",
          "viewability": "87%",
          "video_completion_rate": "67%",
          "audience_count": " 94,218 ",
          "time_key" : 20201012
      },
      {
        "campaign": "Crate & Barrel Q4 2020",

        "audience": "2564",
        "total_impressions": 96428,
        "paid_impressions": " 94,218 ",
        "clicks": " 3,034 ",
        "ctr": "3.22",
        "viewability": "87%",
        "video_completion_rate": "67%",
        "audience_count": " 94,218 ",
        "time_key" : 20201013
    },
    {
      "campaign": "Crate & Barrel Q4 2020",

      "audience": "2564",
      "total_impressions": 94318 ,
      "paid_impressions": " 94,218 ",
      "clicks": " 3,034 ",
      "ctr": "3.22",
      "viewability": "87%",
      "video_completion_rate": "67%",
      "audience_count": " 94,218 ",
      "time_key" : 20201014
  },
  {
    "campaign": "Crate & Barrel Q4 2020",

    "audience": "2564",
    "total_impressions": 86318,
    "paid_impressions": " 94,218 ",
    "clicks": " 3,034 ",
    "ctr": "3.22",
    "viewability": "87%",
    "video_completion_rate": "67%",
    "audience_count": " 94,218 ",
    "time_key" : 20201015
  },
  {
    "campaign": "Crate & Barrel Q4 2020",

    "audience": "2564",
    "total_impressions": 36318,
    "paid_impressions": " 94,218 ",
    "clicks": " 3,034 ",
    "ctr": "3.22",
    "viewability": "87%",
    "video_completion_rate": "67%",
    "audience_count": " 94,218 ",
    "time_key" : 20201016
  },


  {
    "campaign": "Mariott Q4 2020",
    "audience": "2564",
    "total_impressions": 8658,
    "paid_impressions": " 94,218 ",
    "clicks": " 3,034 ",
    "ctr": "3.22",
    "viewability": "87%",
    "video_completion_rate": "67%",
    "audience_count": " 94,218 ",
    "time_key" : 20201011
},
{
  "campaign": "Mariott Q4 2020",

  "audience": "2564",
  "total_impressions": 16118,
  "paid_impressions": " 94,218 ",
  "clicks": " 3,034 ",
  "ctr": "3.22",
  "viewability": "87%",
  "video_completion_rate": "67%",
  "audience_count": " 94,218 ",
  "time_key" : 20201012
},
{
"campaign": "Mariott Q4 2020",

"audience": "2564",
"total_impressions": 76428,
"paid_impressions": " 94,218 ",
"clicks": " 3,034 ",
"ctr": "3.22",
"viewability": "87%",
"video_completion_rate": "67%",
"audience_count": " 94,218 ",
"time_key" : 20201013
},
{
"campaign": "Mariott Q4 2020",

"audience": "2564",
"total_impressions": 54314 ,
"paid_impressions": " 94,218 ",
"clicks": " 3,034 ",
"ctr": "3.22",
"viewability": "87%",
"video_completion_rate": "67%",
"audience_count": " 94,218 ",
"time_key" : 20201014
},
{
"campaign": "Mariott Q4 2020",

"audience": "2564",
"total_impressions": 36318,
"paid_impressions": " 94,218 ",
"clicks": " 3,034 ",
"ctr": "3.22",
"viewability": "87%",
"video_completion_rate": "67%",
"audience_count": " 94,218 ",
"time_key" : 20201015
},
{
"campaign": "Mariott Q4 2020",

"audience": "2564",
"total_impressions": 36318,
"paid_impressions": " 94,218 ",
"clicks": " 3,034 ",
"ctr": "3.22",
"viewability": "87%",
"video_completion_rate": "67%",
"audience_count": " 94,218 ",
"time_key" : 20201016
},



{
  "campaign": "Adidas Q4 2020",
  "audience": "2564",
  "total_impressions": 6358,
  "paid_impressions": " 94,218 ",
  "clicks": " 3,034 ",
  "ctr": "3.22",
  "viewability": "87%",
  "video_completion_rate": "67%",
  "audience_count": " 94,218 ",
  "time_key" : 20201011
},
{
"campaign": "Adidas Q4 2020",

"audience": "2564",
"total_impressions": 3188,
"paid_impressions": " 94,218 ",
"clicks": " 3,034 ",
"ctr": "3.22",
"viewability": "87%",
"video_completion_rate": "67%",
"audience_count": " 94,218 ",
"time_key" : 20201012
},
{
"campaign": "Adidas Q4 2020",

"audience": "2564",
"total_impressions": 2348,
"paid_impressions": " 94,218 ",
"clicks": " 3,034 ",
"ctr": "3.22",
"viewability": "87%",
"video_completion_rate": "67%",
"audience_count": " 94,218 ",
"time_key" : 20201013
},
{
"campaign": "Adidas Q4 2020",

"audience": "2564",
"total_impressions": 86314 ,
"paid_impressions": " 94,218 ",
"clicks": " 3,034 ",
"ctr": "3.22",
"viewability": "87%",
"video_completion_rate": "67%",
"audience_count": " 94,218 ",
"time_key" : 20201014
},
{
"campaign": "Adidas Q4 2020",

"audience": "2564",
"total_impressions": 46318,
"paid_impressions": " 94,218 ",
"clicks": " 3,034 ",
"ctr": "3.22",
"viewability": "87%",
"video_completion_rate": "67%",
"audience_count": " 94,218 ",
"time_key" : 20201015
},
{
"campaign": "Adidas Q4 2020",

"audience": "2564",
"total_impressions": 23318,
"paid_impressions": " 94,218 ",
"clicks": " 3,034 ",
"ctr": "3.22",
"viewability": "87%",
"video_completion_rate": "67%",
"audience_count": " 94,218 ",
"time_key" : 20201016
}



      ],
      "totalItems": 30
    }
     const chartData = data['data'] as [];
     this.showMetricChart = false;

      const datesArr = Array.from(
        new Set(chartData.map(r => r['time_key']))
      ).sort();

      const dims = Array.from(
        new Set(chartData.map(s => s['campaign']))
      );
      const colors = this.libServ.dynamicColors(dims.length);
      this.metricChartJson['chartData']['labels'] = datesArr.map(d =>
        moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
      );

      console.log("this.selectedMetric['field']",this.selectedMetric['field']);
      console.log("dims",dims);
      console.log("this.selectedDimension['field']",this.selectedDimension['field']);


      dims.forEach((src, i) => {
        const revArr = [];
        datesArr.forEach(time_key => {
          chartData.forEach(r => {
            if (
              r['campaign'] === src &&
              r['time_key'] === time_key
            ) {
              revArr.push(r['total_impressions']);
            }
          });
        });
        console.log("revArr",revArr);
        console.log("src",src);


        this.metricChartJson['chartData']['datasets'].push({
          label: src,
          data: revArr,
          borderColor: colors[i],
          fill: false,
          backgroundColor: colors[i]
        });
      });
      console.log("this.metricChartJson",this.metricChartJson);

      this.showMetricChart = true;

  //  }
  }

  onTableDrill(e: Event) {
    if (!e['node']['childLoaded']) {
      this.aggTableJson['loading'] = true;
      const tableReq = {
        dimensions: [],
        metrics: this.toggleView
          ? ['gross_revenue', 'dp_impressions', 'dfp_adserver_impressions', 'dp_total_requests']
          : [
            'gross_revenue',
            'dp_impressions',
            'dfp_adserver_impressions',
            'house_ads_impressions',
            'unfilled_impressions',
            'total_code_served_count'
          ],
        derived_metrics: this.toggleView
          ? ['discrepancy_adops', 'fill_rate_adops', 'cpm']
          : ['cpm', 'fill_rate_house'],
        timeKeyFilter: this.libServ.deepCopy(
          this.filtersApplied['timeKeyFilter']
        ),
        filters: this.libServ.deepCopy(this.filtersApplied['filters']),
        groupByTimeKey: { key: [], interval: 'daily' },
        gidGroupBy: this.getGrpBys(),
        orderBy: [],
        limit: '',
        offset: ''
      };
      const grpBys = this.filtersApplied['groupby'].map(e => e.key);
      console.log('grp',grpBys);
      console.log("e",e);


      for (const g of grpBys) {
        if (e['node']['data'][g] !== 'All') {
          tableReq['dimensions'].push(g);
          if (!tableReq['gidGroupBy'].includes(g)) {
            tableReq['gidGroupBy'].push(g);
          }
          if (
            tableReq['filters']['dimensions'].findIndex(e => e.key === g) === -1
          ) {
            tableReq['filters']['dimensions'].push({
              key: g,
              values: [e['node']['data'][g]]
            });
          } else {
            tableReq['filters']['dimensions'].splice(
              tableReq['filters']['dimensions'].findIndex(e => e.key === g),
              1
            );
            tableReq['filters']['dimensions'].push({
              key: g,
              values: [e['node']['data'][g]]
            });
            // if (
            //   tableReq['filters']['dimensions']
            //     .find(e => e.key === g)
            //     ['values'].findIndex(v => v === e['node']['data'][g]) === -1
            // ) {
            //   tableReq['filters']['dimensions'].push(e['node']['data'][g]);
            // }
          }
        } else {
          tableReq['dimensions'].push(g);
          if (!tableReq['gidGroupBy'].includes(g)) {
            tableReq['gidGroupBy'].push(g);
          }
          break;
        }
      }

      // tableReq['gidGroupBy'] = this.getGrpBys();
      // For Dev purpose
      // tableReq['gidGroupBy'] = [
      //   'ad_size',
      //   'ad_type',
      //   'ad_units',
      //   'bidding_type',
      //   'device_category',
      //   'geography',
      //   'sales_channel',
      //   'sales_type',
      //   'site',
      //   'source'
      // ];

//      this.dataFetchServ.getRevMgmtData(tableReq).subscribe(data => {
        // if (data['status'] === 0) {
        //   this.toastService.displayToast({
        //     severity: 'error',
        //     summary: 'Server Error',
        //     detail: 'Please refresh the page'
        //   });
        //   console.log(data['status_msg']);
        //   return;
        // }
        let data = {
          "data": [
            {
              "campaign": "Crate & Barrel Q4 2020",
              "advertiser": "Crate & Barrel",
              "advertiser_category": "Jewellery",
              "ad_type": "Display",
              "ad_unit_name": "AV",
              "ad_size": "300 x 600",
              "device_type": "Desktop",
              "audience": "2564",
              "total_impressions": " 96,318 ",
              "paid_impressions": " 94,218 ",
              "clicks": " 3,034 ",
              "ctr": "3.22",
              "viewability": "87%",
              "video_completion_rate": "67%",
              "audience_count": " 94,218 "
          },
          {
              "campaign": "Crate & Barrel Q4 2020",
              "advertiser": "Crate & Barrel",
              "advertiser_category": "Jewellery",
              "ad_type": "Display",
              "ad_unit_name": "FB",
              "ad_size": "728 x 90",
              "device_type": "Mobile",
              "audience": "3254",
              "total_impressions": " 215,957 ",
              "paid_impressions": " 205,957 ",
              "clicks": " 4,420 ",
              "ctr": "2.15",
              "viewability": "68%",
              "video_completion_rate": "62%",
              "audience_count": " 205,957 "
          },
          {
              "campaign": "Mariott Q4 2020",
              "advertiser": "Crate & Barrel",
              "advertiser_category": "Jewellery",
              "ad_type": "Display",
              "ad_unit_name": "FB",
              "ad_size": "728 x 90",
              "device_type": "Mobile",
              "audience": "3423",
              "total_impressions": " 185,237 ",
              "paid_impressions": " 155,237 ",
              "clicks": " 72,594 ",
              "ctr": "46.76",
              "viewability": "64%",
              "video_completion_rate": "40%",
              "audience_count": " 155,237 "
          },
          {
              "campaign": "Mariott Q4 2020",
              "advertiser": "Mariott",
              "advertiser_category": "Travel",
              "ad_type": "Display",
              "ad_unit_name": "FB AV",
              "ad_size": "300 x 250",
              "device_type": "Desktop",
              "audience": "25698",
              "total_impressions": " 6,794 ",
              "paid_impressions": " 6,194 ",
              "clicks": " 1,298 ",
              "ctr": "20.96",
              "viewability": "74%",
              "video_completion_rate": "56%",
              "audience_count": " 6,194 "
          },
          {
              "campaign": "Mariott Q4 2020",
              "advertiser": "Mariott",
              "advertiser_category": "Travel",
              "ad_type": "Display",
              "ad_unit_name": "Mobile",
              "ad_size": "300 x 250",
              "device_type": "Mobile",
              "audience": "6842",
              "total_impressions": " 599 ",
              "paid_impressions": " 510 ",
              "clicks": " 63 ",
              "ctr": "12.35",
              "viewability": "85%",
              "video_completion_rate": "87%",
              "audience_count": " 510 "
          },
          {
              "campaign": "Mariott Q4 2020",
              "advertiser": "Mariott",
              "advertiser_category": "Travel",
              "ad_type": "Interactive",
              "ad_unit_name": "Mobile Scroller",
              "ad_size": "300 x 250",
              "device_type": "Smartphone",
              "audience": "9875",
              "total_impressions": " 17,945 ",
              "paid_impressions": " 17,145 ",
              "clicks": " 4,405 ",
              "ctr": "25.69",
              "viewability": "82%",
              "video_completion_rate": "68%",
              "audience_count": " 17,145 "
          },
          {
              "campaign": "Mariott Q4 2020",
              "advertiser": "Mariott",
              "advertiser_category": "Travel",
              "ad_type": "Interactive",
              "ad_unit_name": "Mobile Scroller",
              "ad_size": "300 x 250",
              "device_type": "Smartphone",
              "audience": "2154",
              "total_impressions": " 1,686 ",
              "paid_impressions": " 1,286 ",
              "clicks": " 453 ",
              "ctr": "35.23",
              "viewability": "89%",
              "video_completion_rate": "58%",
              "audience_count": " 1,286 "
          },
          // {
          //     "campaign": "Adidas Q4 2020",

          //     "audience": "6985",
          //     "total_impressions": " 17,473 ",
          //     "paid_impressions": " 17,473 ",
          //     "clicks": " 6,173 ",
          //     "ctr": "35.33",
          //     "viewability": "59%",
          //     "video_completion_rate": "64%",
          //     "audience_count": " 17,473 "
          // },
          // {
          //     "campaign": "Adidas Q4 2020",

          //     "audience": "5486",
          //     "total_impressions": " 247,619 ",
          //     "paid_impressions": " 241,619 ",
          //     "clicks": " 5,445 ",
          //     "ctr": "2.25",
          //     "viewability": "58%",
          //     "video_completion_rate": "74%",
          //     "audience_count": " 241,619 "
          // },
          // {
          //     "campaign": "Adidas Q4 2020",

          //     "audience": "9852",
          //     "total_impressions": " 126,529 ",
          //     "paid_impressions": " 121,529 ",
          //     "clicks": " 45,078 ",
          //     "ctr": "37.09",
          //     "viewability": "67%",
          //     "video_completion_rate": "85%",
          //     "audience_count": " 121,529 "
          // },
          // {
          //     "campaign": "Volkswagen Q3 2020",

          //     "audience": "1254",
          //     "total_impressions": " 361,298 ",
          //     "paid_impressions": " 351,298 ",
          //     "clicks": " 162,023 ",
          //     "ctr": "46.12",
          //     "viewability": "62%",
          //     "video_completion_rate": "82%",
          //     "audience_count": " 351,298 "
          // },
          // {
          //     "campaign": "Volkswagen Q3 2020",

          //     "audience": "9874",
          //     "total_impressions": " 66,283 ",
          //     "paid_impressions": " 65,283 ",
          //     "clicks": " 43,607 ",
          //     "ctr": "66.80",
          //     "viewability": "89%",
          //     "video_completion_rate": "89%",
          //     "audience_count": " 65,283 "
          // },
          // {
          //     "campaign": "Volkswagen Q3 2020",

          //     "audience": "9652",
          //     "total_impressions": " 96,318 ",
          //     "paid_impressions": " 91,318 ",
          //     "clicks": " 25,769 ",
          //     "ctr": "28.22",
          //     "viewability": "40%",
          //     "video_completion_rate": "82%",
          //     "audience_count": " 91,318 "
          // },
          // {
          //     "campaign": "Volkswagen Q3 2020",

          //     "audience": "2135",
          //     "total_impressions": " 215,957 ",
          //     "paid_impressions": " 205,957 ",
          //     "clicks": " 185,237 ",
          //     "ctr": "89.94",
          //     "viewability": "56%",
          //     "video_completion_rate": "89%",
          //     "audience_count": " 205,957 "
          // }
          ],
          "totalItems": 30
        }
        const childData = data['data'];

        const arr = [];

        console.log("this.aggTableJson['frozenCols']",this.aggTableJson['frozenCols']);
        const grpBysFilter = this.filtersApplied['groupby'].map(e => e.key);

        childData.forEach((row: object) => {
          this.aggTableJson['frozenCols'].forEach(r => {


             if(grpBysFilter.length >2 && r['field'] == grpBysFilter[grpBysFilter.length-1] && !e['node']['parent']){
             row[r['field']] = 'All';
           }
          });
          let obj = {};
          if (row[grpBys[grpBys.length - 1]] === 'All'  ) {
            obj = {
              data: row,
              children: [{ data: {} }]
            };
          } else {
            obj = {
              data: row
            };
          }
          arr.push(obj);
        });
        this.aggTableJson['loading'] = false;
        e['node']['children'] = <TreeNode[]>arr;
        this.aggTableData = [...this.aggTableData];
        e['node']['childLoaded'] = true;


//        const grpBysFilter = this.filtersApplied['groupby'].map(e => e.key);

      //   childData.forEach((row: object) => {
      //   for (let i = 0; i < grpBysFilter.length; i++) {
      //     if (i !== grpBysFilter.length - 2 && i != 0) {
      //       row[grpBysFilter[i]] = 'All';
      //     }
      //   }
      //   let obj = {};
      //   if (grpBysFilter.length === 1) {
      //     obj = {
      //       data: row
      //     };
      //   } else {
      //     obj = {
      //       data: row,
      //       children: [{ data: {} }]
      //     };
      //   }
      //   arr.push(obj);
      // });
        this.aggTableJson['loading'] = false;
        e['node']['children'] = <TreeNode[]>arr;
        this.aggTableData = [...this.aggTableData];
        e['node']['childLoaded'] = true;
     // });
    }
  }

  dimChanged() {
    this.loadMetricChart();
  }
  onLazyLoadAggTable(e: Event) {
    console.log('Lazy Agg', e);
  }

  openPopup(row, field) {
    const grpBys = [];
    const filtersApplied = this.libServ.deepCopy(this.filtersApplied);
    let str = '';

    const fieldIdx = filtersApplied['groupby'].findIndex(
      e => e['key'] === field
    );
    filtersApplied['groupby'].forEach((grp, i) => {
      if (i <= fieldIdx) {
        if (
          filtersApplied['filters']['dimensions'].findIndex(
            e => e.key === grp['key']
          ) !== -1
        ) {
          filtersApplied['filters']['dimensions'].splice(
            filtersApplied['filters']['dimensions'].findIndex(
              e => e.key === grp['key']
            ),
            1
          );
        }

        if (str === '') {
          str = str + row[grp['key']];
        } else {
          str = str + ' > ' + row[grp['key']];
        }
        filtersApplied['filters']['dimensions'].push({
          key: grp['key'],
          values: [row[grp['key']]]
        });
      }
    });

    filtersApplied['groupby'] = grpBys;
    const data = {
      filters: filtersApplied,
      toggleView: this.toggleView,
      title: str,
      heading: row[field],
      exportRequest: this.exportRequest,
      isExportReport: this.isExportReport
    };

    const ref = this.dialogService.open(SupplyPerformanceDailyDataPopupComponent, {
      header: row[field] + ' Daily Distribution',
      contentStyle: { width: '80vw', overflow: 'auto' },
      data: data
    });
    ref.onClose.subscribe((data: string) => { });
  }

  exportTable(table, fileFormat) {
    if (this.aggTableData.length == 0) {
      this.confirmationService.confirm({
        message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
        header: 'Information',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
        },
        reject: () => {
        }
      });
    } else {
      if (this.exportRequest['sendEmail'].length > 0) {
        const data = {
          filters: this.filtersApplied,
          colDef: this.aggTableColumnDef,
          toggleView: this.toggleView,
          exportRequest: this.exportRequest,
          fileFormat: fileFormat
        };

      //   const ref = this.dialogService.open(Rev360ExportPopupComponent, {
      //     header: 'Download Report',
      //     contentStyle: { 'max-height': '80vh', overflow: 'auto' },
      //     data: data
      //   });
      //   ref.onClose.subscribe((data: string) => { });
      // } else {
      //   this.toastService.displayToast({
      //     severity: 'error',
      //     summary: 'Export Report',
      //     detail:
      //       'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
      //     life: 10000
      //   });
       }
    }
    return false;
  }

  isHiddenColumn(col: Object) {
    return (
      this.aggTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }

  /* --------------------- On Filter Apply Button Clicked --------------------- */

  onFiltersApplied(filterData: object) {
    this.filtersApplied['filters']['dimensions'] = [];

    // tslint:disable-next-line: forin
    for (const k in filterData['filter']['dimensions']) {
      if (filterData['filter']['dimensions'][k].length !== 0) {
        this.filtersApplied['filters']['dimensions'].push({
          key: k,
          values: filterData['filter']['dimensions'][k]
        });
      }
    }

    this.filtersApplied['groupby'] = filterData['groupby'];
    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];

    this.tableReq = {
      dimensions: [],
      metrics: [],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: { key: [], interval: 'daily' },
      gidGroupBy: [],
      orderBy: [
      ],
      limit: '',
      offset: ''
    };
    this.loadTableData(this.tableReq);

    const grpBys = this.getGrpBys();

    const cardsReq = {
      dimensions: [],
      metrics: this.toggleView
        ? ['gross_revenue', 'dp_impressions', 'dfp_adserver_impressions']
        : ['dp_impressions', 'unfilled_impressions', 'house_ads_impressions'],
      derived_metrics: this.toggleView ? ['cpm'] : [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: { key: [], interval: 'daily' },
      gidGroupBy: grpBys,
      orderBy: [],
      limit: '',
      offset: '',
      isTable: false
    };
    // For Dev purpose
    // cardsReq['gidGroupBy'] = [
    //   'ad_size',
    //   'ad_type',
    //   'ad_units',
    //   'bidding_type',
    //   'device_category',
    //   'geography',
    //   'sales_channel',
    //   'sales_type',
    //   'site',
    //   'source'
    // ];
//    this.loadCards(cardsReq);
  }

  getGrpBys() {
    //let grpBys = this.filtersApplied['groupby'].map(e => e.key);
    let grpBys = [this.filtersApplied['groupby'][0]['key']];
    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          grpBys.concat(
            this.filtersApplied['filters']['dimensions']
              .filter(f => f.values.length)
              .map(m => m.key)
          )
        )
      );
    }
    return grpBys;
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['name']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    }
    return false;
  }
  tabChanged(e) {
    this.ngOnDestroy();
    const index = e.index;
    if (index == 0) {
      this.toggleView = true;
      this.cardsJson = [
        {
          field: 'dp_impressions',
          displayName: 'Billable Imps.',
          value: 0,
          imageClass: 'fas fa-eye',
          format: 'number',
          config: [],
          color: '#5cb85c'
        },
        {
          field: 'gross_revenue',
          displayName: 'Gross Revenue',
          value: 0,
          imageClass: 'fas fa-hand-holding-usd',
          format: '$',
          config: [],
          color: '#5bc0de'
        },
        {
          field: 'cpm',
          displayName: 'Gross eCPM',
          value: 0,
          imageClass: 'fas fa-dollar-sign',
          format: '$',
          config: [],
          color: '#FFDC00'
        }
      ];
      this.appConfigObs1 = this.appConfigService.appConfig.subscribe(
        appConfig => {
          moment
          if (!this.libServ.isEmptyObj(appConfig)) {
            this.appConfig = appConfig;
            this._titleService.setTitle( this.appConfig['name']);
            let startDate;
            if (
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['startOf']
            ) {
              startDate = moment()
                .subtract(
                  this.appConfig['filter']['filterConfig']['filters'][
                  'datePeriod'
                  ][0]['defaultDate'][0]['value'],
                  this.appConfig['filter']['filterConfig']['filters'][
                  'datePeriod'
                  ][0]['defaultDate'][0]['period']
                )
                .startOf(
                  this.appConfig['filter']['filterConfig']['filters'][
                  'datePeriod'
                  ][0]['defaultDate'][0]['period']
                );
            } else {
              startDate = moment().subtract(
                this.appConfig['filter']['filterConfig']['filters'][
                'datePeriod'
                ][0]['defaultDate'][0]['value'],
                this.appConfig['filter']['filterConfig']['filters'][
                'datePeriod'
                ][0]['defaultDate'][0]['period']
              );
            }
            const endDate = moment().subtract(
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][1]['value'],
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][1]['period']
            );

            this.filtersApplied = {
              timeKeyFilter: {
                time_key1: startDate.format('YYYYMMDD'),
                time_key2: endDate.format('YYYYMMDD')
              },
              filters: { dimensions: [], metrics: [] },
              groupby: this.appConfig['filter']['filterConfig'][
                'groupBy'
              ].filter(v => v.selected)
            };
            const cardsReq = {
              dimensions: [],
              metrics: [
                'gross_revenue',
                'dp_impressions',
                'dfp_adserver_impressions'
              ],
              derived_metrics: ['cpm'],
              timeKeyFilter: this.libServ.deepCopy(
                this.filtersApplied['timeKeyFilter']
              ),
              filters: this.libServ.deepCopy(this.filtersApplied['filters']),
              groupByTimeKey: { key: [], interval: 'daily' },
              gidGroupBy: ['source'],
              orderBy: [],
              limit: '',
              offset: '',
              isTable: false
            };
//            this.loadCards(cardsReq);

            this.tableReq = {
              dimensions: [],
              metrics: [],
              derived_metrics: [],
              timeKeyFilter: this.libServ.deepCopy(
                this.filtersApplied['timeKeyFilter']
              ),
              filters: this.libServ.deepCopy(this.filtersApplied['filters']),
              groupByTimeKey: { key: [], interval: 'daily' },
              gidGroupBy: [],
              orderBy: [
              ],
              limit: '',
              offset: ''
            };
          }
        }
      );
      this.loadTableData(this.tableReq);

    } else if (index == 1) {
      this.toggleView = false;
      this.cardsJson = [
        {
          field: 'unfilled_impressions',
          displayName: 'Unfilled Imps.',
          value: 0,
          imageClass: 'fas fa-eye',
          format: 'number',
          config: [],
          color: '#5cb85c'
        },
        {
          field: 'dp_impressions',
          displayName: 'Billable Imps.',
          value: 0,
          imageClass: 'fas fa-eye',
          format: 'number',
          config: [],
          color: '#5bc0de'
        },
        {
          field: 'house_ads_impressions',
          displayName: 'House Ad Imps.',
          value: 0,
          imageClass: 'fas fa-eye',
          format: 'number',
          config: [],
          color: '#FFDC00'
        }
      ];

      this.appConfigObs2 = this.appConfigService.appConfig.subscribe(
        appConfig => {
          if (!this.libServ.isEmptyObj(appConfig)) {
            this.appConfig = appConfig;
            this._titleService.setTitle(this.appConfig['name']);
            let startDate;
            if (
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['startOf']
            ) {
              startDate = moment()
                .subtract(
                  this.appConfig['filter']['filterConfig']['filters'][
                  'datePeriod'
                  ][0]['defaultDate'][0]['value'],
                  this.appConfig['filter']['filterConfig']['filters'][
                  'datePeriod'
                  ][0]['defaultDate'][0]['period']
                )
                .startOf(
                  this.appConfig['filter']['filterConfig']['filters'][
                  'datePeriod'
                  ][0]['defaultDate'][0]['period']
                );
            } else {
              startDate = moment().subtract(
                this.appConfig['filter']['filterConfig']['filters'][
                'datePeriod'
                ][0]['defaultDate'][0]['value'],
                this.appConfig['filter']['filterConfig']['filters'][
                'datePeriod'
                ][0]['defaultDate'][0]['period']
              );
            }
            const endDate = moment().subtract(
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][1]['value'],
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][1]['period']
            );

            this.filtersApplied = {
              timeKeyFilter: {
                time_key1: startDate.format('YYYYMMDD'),
                time_key2: endDate.format('YYYYMMDD')
              },
              filters: { dimensions: [], metrics: [] },
              groupby: this.appConfig['filter']['filterConfig'][
                'groupBy'
              ].filter(v => v.selected)
            };
            const cardsReq = {
              dimensions: [],
              metrics: [
                'dp_impressions',
                'unfilled_impressions',
                'house_ads_impressions'
              ],
              derived_metrics: [],
              timeKeyFilter: this.libServ.deepCopy(
                this.filtersApplied['timeKeyFilter']
              ),
              filters: this.libServ.deepCopy(this.filtersApplied['filters']),
              groupByTimeKey: { key: [], interval: 'daily' },
              gidGroupBy: ['source'],
              orderBy: [],
              limit: '',
              offset: '',
              isTable: false
            };
            //this.loadCards(cardsReq);
            this.tableReq = {
              dimensions: [],
              metrics: [],
              derived_metrics: [],
              timeKeyFilter: this.libServ.deepCopy(
                this.filtersApplied['timeKeyFilter']
              ),
              filters: this.libServ.deepCopy(this.filtersApplied['filters']),
              groupByTimeKey: { key: [], interval: 'daily' },
              gidGroupBy: [],
              orderBy: [
              ],
              limit: '',
              offset: ''
            };
          }
        }
      );
      this.loadTableData(this.tableReq);
    }
  }

  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
    if (this.appConfigObs1 && !this.appConfigObs1.closed) {
      this.appConfigObs1.unsubscribe();
    }
    if (this.appConfigObs2 && !this.appConfigObs2.closed) {
      this.appConfigObs2.unsubscribe();
    }
  }

  sendEmail() {
    const imageConfig = {
      imgName: `${this.appConfig['name']}.png`,
      type: 'image/png'
    };

    this.htmltoimage
      .canvasImage(document.getElementById('capture'), imageConfig)
      .then(canvas => {
        this.dialogService.open(SendEmailComponent, {
          header: 'Email Screenshot',
          contentStyle: {
            'max-height': '80vh',
            width: '30vw',
            overflow: 'auto'
          },
          data: canvas.toDataURL('image/png')
        });
      });
  }

  onGlobalSearchChanged(searchValue, tableColDef) {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    const grpBys = this.getGrpBys();
    let col = grpBys[0];
    this.timeout = setTimeout(() => {
      this.tableReq['filters']['globalSearch'] = {
        dimensions: [],
        value: searchValue
      };
      tableColDef.forEach(element => {
        this.tableReq['filters']['globalSearch']['dimensions'].push(
          element.field
        );
      });
      this.tableReq['filters']['globalSearch']['dimensions'].push(col);
      this.loadTableData(this.tableReq);
    }, 3000);
  }
}
