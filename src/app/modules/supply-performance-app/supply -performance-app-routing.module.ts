import { Routes } from '@angular/router';
import { SupplyPerformanceAppComponent } from './supply-performance.component';

export const SupplyPerformanceAppRoutes: Routes = [
  {
    path: '',
    component: SupplyPerformanceAppComponent
  }
];
