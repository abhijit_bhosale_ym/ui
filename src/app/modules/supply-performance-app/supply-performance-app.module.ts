import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectButtonModule } from 'primeng/selectbutton';
import { ButtonModule } from 'primeng/button';
import { TabViewModule } from 'primeng/tabview';
import { TreeTableModule } from 'primeng/treetable';
import { InputSwitchModule } from 'primeng/inputswitch';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MultiSelectModule } from 'primeng/multiselect';

import { SharedModule } from 'src/app/_pipes/shared.module';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { FilterContainerModule } from '../common/filter-container/filter-container.module';
import { CardsModule } from '../common/cards/cards.module';
import { ChartsModule } from '../common/charts/charts.module';
import { RouterModule } from '@angular/router';
import { SupplyPerformanceAppRoutes } from './supply -performance-app-routing.module';
import { SupplyPerformanceAppComponent } from './supply-performance.component';
import { ConfirmDialogModule, ConfirmationService } from 'primeng';
import { DropdownModule } from 'primeng/dropdown';


@NgModule({
  declarations: [SupplyPerformanceAppComponent],
  imports: [
    CommonModule,
    SelectButtonModule,
    ButtonModule,
    TabViewModule,
    TreeTableModule,
    InputSwitchModule,
    FormsModule,
    ReactiveFormsModule,
    MultiSelectModule,
    SharedModule,
    FilterContainerModule,
    CardsModule,
    ChartsModule,
    ConfirmDialogModule,
    DropdownModule,
    RouterModule.forChild(SupplyPerformanceAppRoutes)
  ],
  providers: [FormatNumPipe,ConfirmationService]
})
export class SupplyPerformanceAppModule {}
