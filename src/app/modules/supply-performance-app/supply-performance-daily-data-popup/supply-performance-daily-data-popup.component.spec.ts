import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplyPerformanceDailyDataPopupComponent } from './supply-performance-daily-data-popup.component';

describe('SupplyPerformanceDailyDataPopupComponent', () => {
  let component: SupplyPerformanceDailyDataPopupComponent;
  let fixture: ComponentFixture<SupplyPerformanceDailyDataPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SupplyPerformanceDailyDataPopupComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplyPerformanceDailyDataPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
