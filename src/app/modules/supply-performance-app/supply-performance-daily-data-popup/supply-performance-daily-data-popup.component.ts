import { Component, OnInit } from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { FetchApiDataService } from '../fetch-api-data.service';
import * as moment from 'moment';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { DropdownModule } from 'primeng/dropdown';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-supply-performance-daily-data-popup',
  templateUrl: './supply-performance-daily-data-popup.component.html',
  styleUrls: ['./supply-performance-daily-data-popup.component.scss']
})
export class SupplyPerformanceDailyDataPopupComponent implements OnInit {
  lineChartJson: object;
  showLineChart = false;
  toggleView = true;
  filtersApplied: object;
  noLineChart = false;
  tabChangedFlag = false;

  tableDataReq: object;
  tableData: TreeNode[];
  tableColumnDef: any[];
  dimColDef: any[];
  tableJson: Object;
  popupTitle = '';
  heading = '';
  exportRequest: ExportRequest = <ExportRequest>{};
  isExportReport = false;
  mainLineChartJson: object;
  showMainLineChart = false;
  showMainPieChart = false;
  noDataMainLineChart = false;
  mainLineChartJsonView: object;
  showMainLineChartView = false;
  showMainPieChartView = false;
  noDataMainLineChartView = false;
  lineChartJsonCon: object;
  showLineChartCon = false;
  noLineChartCon = false;

  constructor(
    private libServ: CommonLibService,
    private formatNumPipe: FormatNumPipe,
    private dataFetchServ: FetchApiDataService,
    private config: DynamicDialogConfig,
    private exportService: ExportdataService,
    private toastService: ToastService,
    private confirmationService: ConfirmationService
  ) {
    this.filtersApplied = this.config.data['filters'];
    this.toggleView = this.config.data['toggleView'];
    this.popupTitle = this.config.data['title'];
    this.heading = this.config.data['heading'];
  }

  ngOnInit() {
    this.exportRequest = this.config.data['exportRequest'];
    this.isExportReport = this.config.data['isExportReport'];
    this.dimColDef = [
      {
        field: 'campaign',
        displayName: 'Campaign',
        format: '',
        width: '250',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'advertiser',
        displayName: 'Advertiser',
        format: '',
        width: '140',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'advertiser_category',
        displayName: 'Advertiser Category',
        format: '',
        width: '165',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'ad_type',
        displayName: 'Ad type',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'ad_type',
        displayName: 'Ad type',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'ad_unit_name',
        displayName: 'Ad Unit Name',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'ad_size',
        displayName: 'Ad Size',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'device_type',
        displayName: 'Device Type',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
        {
          field: 'audience',
          displayName: 'Audience',
          format: '',
          width: '150',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
      }
    ];
    this.tableColumnDef = [
      {
        field: 'total_impressions',
        displayName: 'Total Impressions',
        format: 'number',
        visible: false,
        width: '170',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'paid_impressions',
        displayName: 'Paid Impressions',
        format: 'number',
        width: '170',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'clicks',
        displayName: 'Clicks',
        format: 'number',
        width: '170',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
          field: 'ctr',
          displayName: 'CTR',
          format: 'percentage',
          width: '110',
          exportConfig: {
            format: 'percentage',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [2],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: true
          }
        },
        {
          field: 'viewability',
          displayName: 'Viewability',
          format: 'percentage',
          width: '110',
          exportConfig: {
            format: 'percentage',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [2],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: true
          }
        },

        {
          field: 'video_completion_rate',
          displayName: 'Video-Completion Rate',
          format: 'percentage',
          width: '110',
          exportConfig: {
            format: 'percentage',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [2],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: true
          }
        },

        {
          field: 'audience_count',
          displayName: 'Audience Count',
          format: 'number',
          width: '170',
          exportConfig: {
            format: 'number',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: true
          },
          footerTotal: '-'
        }
    ];
    this.tableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '200px',
      totalRecords: 1000,

      columns: this.tableColumnDef,
      selectedColumns: this.tableColumnDef,
      // frozenCols: [],
      // frozenWidth: '0px',
      // columns: this.tableColumnDef.slice(1),
      // selectedColumns: this.tableColumnDef.slice(1),
      // frozenCols: [...this.tableColumnDef.slice(0, 1)],
      // frozenWidth:
      //   this.tableColumnDef
      //     .slice(0, 1)
      //     .reduce((tot, cur) => tot + parseInt(cur.width), 0) + 'px',

      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: false,
      columnSearch: false
    };

    this.loadTable();
  }

  loadTable() {
    this.tableJson['loading'] = true;

    let grpBys = this.filtersApplied['groupby'].map(e => e.key);
    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          this.filtersApplied['filters']['dimensions']
            .filter(f => f.values.length)
            .map(m => m.key)
            .concat(grpBys)
        )
      );
    }

    this.dimColDef.reverse().forEach(c => {
      if (grpBys.includes(c.field)) {
        this.tableColumnDef.unshift(c);
      }
    });
    this.tableColumnDef.unshift({
      field: 'time_key',
      displayName: 'Date',
      format: 'date',
      width: '120',
      exportConfig: {
        format: 'date',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: false
      }
    });

    grpBys.push('time_key');
    // this.aggTableColumnDef = finalColDef.slice();

    this.tableJson['columns'] = this.tableColumnDef.slice(1
    );
    this.tableJson['selectedColumns'] = this.tableColumnDef.slice(1);
    this.tableJson['frozenCols'] = [
      ...this.tableColumnDef.slice(0, 1)
    ];
    this.tableJson['frozenWidth'] =
      this.tableColumnDef
        .slice(0, 1)
        .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px';

    console.log("tableColumnDef=======", this.tableColumnDef);

    this.tableDataReq = {
      dimensions: grpBys,
      metrics: this.toggleView
        ? ['gross_revenue', 'dp_impressions', 'dfp_adserver_impressions', 'dp_total_requests']
        : [
          'gross_revenue',
          'dp_impressions',
          'dfp_adserver_impressions',
          'house_ads_impressions',
          'unfilled_impressions',
          'total_code_served_count'
        ],
      derived_metrics: this.toggleView
        ? ['discrepancy_adops', 'fill_rate_adops', 'cpm']
        : ['cpm', 'fill_rate_house'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: grpBys.filter(v => v !== 'time_key'),
      orderBy: [{ key: 'time_key', opcode: 'desc' }],
      limit: '',
      offset: ''
    };
    // For Dev purpose
    // this.tableDataReq['gidGroupBy'] = [
    //   'ad_size',
    //   'ad_type',
    //   'ad_units',
    //   'bidding_type',
    //   'device_category',
    //   'geography',
    //   'sales_channel',
    //   'sales_type',
    //   'site',
    //   'source'
    // ];

    // this.dataFetchServ.getRevMgmtData(this.tableDataReq).subscribe(data => {
    //   if (data['status'] === 0) {
    //     this.toastService.displayToast({
    //       severity: 'error',
    //       summary: 'Server Error',
    //       detail: 'Please refresh the page'
    //     });
    //     console.log(data['status_msg']);
    //     return;
    //   }
   let data ={ "data": [                        {
      "campaign": "Crate & Barrel Q4 2020",
      "advertiser": "Crate & Barrel",
      "advertiser_category": "Jewellery",
      "ad_type": "Display",
      "ad_unit_name": "AV",
      "ad_size": "300 x 600",
      "device _type": "Desktop",
      "audience": "2564",
      "total_impressions": " 96,318 ",
      "paid_impressions": " 94,218 ",
      "clicks": " 3,034 ",
      "ctr": "3.22",
      "viewability": "87%",
      "video_completion_rate": "67%",
      "audience_count": " 94,218 ",
      "time_key" : "20201212"
  },

  {
      "campaign": "Mariott Q4 2020",
      "advertiser": "Mariott",
      "advertiser_category": "Travel",
      "ad_type": "Display",
      "ad_unit_name": "FB AV",
      "ad_size": "300 x 250",
      "device _type": "Desktop",
      "audience": "3423",
      "total_impressions": " 185,237 ",
      "paid_impressions": " 155,237 ",
      "clicks": " 72,594 ",
      "ctr": "46.76",
      "viewability": "64%",
      "video_completion_rate": "40%",
      "audience_count": " 155,237 ",
      "time_key" : "20201213"
  },

  {
      "campaign": "Adidas Q4 2020",
      "advertiser": "Adidas",
      "advertiser_category": "Sports",
      "ad_type": "Rich Media",
      "ad_unit_name": "RON",
      "ad_size": "300 x 600",
      "device _type": "Desktop",
      "audience": "6985",
      "total_impressions": " 17,473 ",
      "paid_impressions": " 17,473 ",
      "clicks": " 6,173 ",
      "ctr": "35.33",
      "viewability": "59%",
      "video_completion_rate": "64%",
      "audience_count": " 17,473 ",
      "time_key" : "20201213"
  },
  {
      "campaign": "Volkswagen Q3 2020",
      "advertiser": "Volkswagen",
      "advertiser_category": "Automobile",
      "ad_type": "Interactive",
      "ad_unit_name": "RON",
      "ad_size": "300 x 250",
      "device _type": "Desktop",
      "audience": "1254",
      "total_impressions": " 361,298 ",
      "paid_impressions": " 351,298 ",
      "clicks": " 162,023 ",
      "ctr": "46.12",
      "viewability": "62%",
      "video_completion_rate": "82%",
      "audience_count": " 351,298 ",
      "time_key" : "20201214"
  }
]
}

      const tableData = data['data'];

      const arr = [];
      tableData.forEach((row: object) => {
        const obj = {
          data: row
        };
        arr.push(obj);
      });             

      this.tableData = <TreeNode[]>arr;
      this.tableJson['totalRecords'] = data['totalItems'];
      this.tableJson['loading'] = false;
    // });
  }

  exportTablePopup(fileFormat) {

    if (this.tableData.length == 0) {
      this.confirmationService.confirm({
        message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
        header: 'Information',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
        },
        reject: () => {
        }
      });
    } else {
      let notIncludeColumn = this.toggleView
        ? ['fill_rate_adops', 'discrepancy_adops']
        : []
      if (this.exportRequest['sendEmail'].length > 0) {
        this.toastService.displayToast({
          severity: 'info',
          summary: 'Export Report',
          detail:
            'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
          life: 10000
        });
        const columnDefs = this.tableColumnDef;
        const req = this.libServ.deepCopy(this.tableDataReq);
        req['limit'] = '';
        req['offset'] = '';
        req['isTable'] = false;
        const sheetDetailsarray = [];
        const sheetDetails = {};
        sheetDetails['columnDef'] = this.libServ.deepCopy(columnDefs);
        sheetDetails['data'] = [];
        sheetDetails['sheetName'] = 'Daily Distribution';
        sheetDetails['isRequest'] = true;
        sheetDetails['request'] = {
          url: '/api/flask/payout/cassandradata',
          method: 'POST',
          param: req
        };
        sheetDetails['disclaimer'] = [
          {
            position: 'bottom',
            label: 'Note: Data present in the table may vary over a period of time.',
            color: '#000000'
          }
        ];
        sheetDetails['totalFooter'] = {
          available: true,
          custom: true,
          notIncludeColumn: notIncludeColumn
        };
        sheetDetails['tableTitle'] = {
          available: false,
          label: this.heading + ' Daily Distribution'
        };
        sheetDetails['image'] = [
          {
            available: true,
            path: environment.exportConfig.exportLogo,
            position: 'top'
          }
        ];
        sheetDetailsarray.push(sheetDetails);
        this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
        this.exportRequest['fileName'] = this.heading + ' Daily Distribution ' +
          moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
        this.exportRequest['exportFormat'] = fileFormat;
        this.exportRequest['exportConfig'] = environment.exportConfig;
        this.dataFetchServ
          .getExportReportData(this.exportRequest)
          .subscribe(response => {
            console.log(response);
          });

      } else {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Export Report',
          detail:
            'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email.Thanks!',
          life: 10000
        });
      }
    }
  }

  isHiddenColumn(col: Object) { }

  loadLineChart() {
    let grpBys = this.filtersApplied['groupby'].map(e => e.key);
    this.showLineChart = false;
    
    const colors = this.libServ.dynamicColors(2);
    this.mainLineChartJson = {
      chartTypes: [{ key: 'line', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'CTR',
            type: 'line',
            borderColor: colors[1],
            fill: false,
            backgroundColor: colors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'CTR'
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Dates'
              }
            }
          ],
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: 'CTR'
              },
              position: 'left',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, 'percentage', [2]);
                }
              }
            }
          ]
        },
        tooltips: {
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, 'percentage', [2])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

   let data = {
    "data": [
      {
      "time_key": 20200601,
      "ctr": 24.8
    }, {
      "time_key": 20201002,
      "ctr": 17.78
    }, {
      "time_key": 20201003,
      "ctr": 12.3
    },
    {
      "time_key": 20201004,
      "ctr": 39.8
    },
    {
      "time_key": 20201005,
      "ctr": 22.8
    },
    {
      "time_key": 20201006,
      "ctr": 58.4
    },
    {
      "time_key": 20201007,
      "ctr": 29.0
        },
    {
      "time_key": 20201008,
      "ctr": 23.23
    },
    {
      "time_key": 20201009,
      "ctr": 27.8
    },
    {
      "time_key": 20201010,
      "ctr": 28.2
    }
  ],
    "totalItems": 10
  }
  const chartData = data['data'] as [];
        const datesArr = Array.from(
          new Set(chartData.map(r => r['time_key']))
        );

        this.mainLineChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );

        const revArr = [];
        datesArr.forEach(time_key => {
          chartData.forEach(r => {
            if (r['time_key'] === time_key) {
              revArr.push(r['ctr']);
            }
          });
        });
        this.mainLineChartJson['chartData']['datasets'][0]['data'] = revArr;
        this.showMainLineChart = true;
        this.noDataMainLineChart = false;
  }

  loadLineChartView() {
    let grpBys = this.filtersApplied['groupby'].map(e => e.key);
    this.showMainLineChartView = false;
    
    this.mainLineChartJsonView = {      
      chartTypes: [
      { key: 'line', label: 'Line Chart' }
    ],
    chartData: {
      labels: [],
      datasets: []
    },
    chartOptions: {
      title: {
        display: true,
        text: ''
      },
      legend: {
        display: false
      },
      scales: {
        xAxes: [
          {
            display: true,
            scaleLabel: {
              display: true,
              labelString: 'Date'
            }
          }
        ],
        yAxes: [
          {
            display: true,
            scaleLabel: {
              display: true
            }
          }
        ]
      },
      pan: {
        enabled: true,
        mode: 'x'
      },
      zoom: {
        enabled: true,
        mode: 'x'
      },
      plugins: {
        datalabels: false
      }
    },
    zoomLabel: true,
    chartWidth: '',
    chartHeight: '300px'
  };

    this.mainLineChartJsonView['chartOptions']['title'] = {
      display: true,
      text: 'Viewability Trend by Ad Units'
    };

    this.mainLineChartJsonView['chartOptions']['scales'] = {
      xAxes: [
        {
          display: true,
          scaleLabel: {
            display: true,
            labelString: 'Date'
          }
        }
      ],
      yAxes: [
        {
          display: true,
          scaleLabel: {
            display: true,
            labelString: 'Viewability'
          },
          ticks: {
                callback: (value, index1, values) => {
                  return this.formatNumPipe.transform(value, 'percentage', [2]);
            }
          }
        }
      ]
    };

  this.mainLineChartJsonView['chartOptions']['tooltips'] = {
    callbacks: {
      label: (tooltipItem, data) => {
        const currentValue =
          data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
        return `${
          data.datasets[tooltipItem.datasetIndex].label
          } : ${this.formatNumPipe.transform(currentValue, 'percentage', [2])}`;
      }
    }
  };

    this.mainLineChartJsonView['chartOptions']['legend'] = {
      display: true
    };

    this.showMainLineChartView = false;
    // this.dataFetchServ.getTableData(req).subscribe(data => {
    //   const resData = data as {};
    //   const chartData = resData['data'];
    //   this.noDataMetricChart = false;
    //   if (!chartData.length) {
    //     this.noDataMetricChart = true;
    //     return;
    //   }
    this.noDataMainLineChartView = false;
    let data = {
        "data": [
          {
            "ad_unit": "YuktaNews_Home_TopLeaderboard",
            "viewability": 23,
            "time_key" : 20201011
        },
        {
          "ad_unit": "YuktaNews_Home_TopLeaderboard",
          "viewability": 43,
          "time_key" : 20201012
      },
      {
        "ad_unit": "YuktaNews_Home_TopLeaderboard",        
        "viewability": 55,
        "time_key" : 20201013
    },
    {
      "ad_unit": "YuktaNews_Home_TopLeaderboard",
      "viewability": 34,
      "time_key" : 20201014
  },
  {
    "ad_unit": "YuktaNews_Home_TopLeaderboard",
    "viewability": 81,
     "time_key" : 20201015
  },
  {
    "ad_unit": "YuktaNews_Home_TopLeaderboard",
    "viewability": 27,
    "time_key" : 20201016
  },

  {
    "ad_unit": "YuktaNews_Home_MPU",
    "viewability": 86,
    "time_key" : 20201011
},
{
  "ad_unit": "YuktaNews_Home_MPU",
  "viewability": 23,
  "time_key" : 20201012
},
{
"ad_unit": "YuktaNews_Home_MPU",        
"viewability": 52,
"time_key" : 20201013
},
{
"ad_unit": "YuktaNews_Home_MPU",
"viewability": 43,
"time_key" : 20201014
},
{
"ad_unit": "YuktaNews_Home_MPU",
"viewability": 18,
"time_key" : 20201015
},
{
"ad_unit": "YuktaNews_Home_MPU",
"viewability": 7,
"time_key" : 20201016
},

{
  "ad_unit": "YuktaNews_Category_TopLeaderboard",
  "viewability": 47.5,
  "time_key" : 20201011
},
{
"ad_unit": "YuktaNews_Category_TopLeaderboard",
"viewability": 57.3,
"time_key" : 20201012
},
{
"ad_unit": "YuktaNews_Category_TopLeaderboard",        
"viewability": 29.2,
"time_key" : 20201013
},
{
"ad_unit": "YuktaNews_Category_TopLeaderboard",
"viewability": 32.23,
"time_key" : 20201014
},
{
"ad_unit": "YuktaNews_Category_TopLeaderboard",
"viewability": 43.45,
"time_key" : 20201015
},
{
"ad_unit": "YuktaNews_Category_TopLeaderboard",
"viewability": 17,
"time_key" : 20201016
}

      ],
      "totalItems": 30
    }
     const chartData = data['data'] as [];
     this.showMainLineChartView = false;

      const datesArr = Array.from(
        new Set(chartData.map(r => r['time_key']))
      ).sort();

      const dims = Array.from(
        new Set(chartData.map(s => s['ad_unit']))
      );
      const colors = this.libServ.dynamicColors(dims.length);
      this.mainLineChartJsonView['chartData']['labels'] = datesArr.map(d =>
        moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
      );
      

      dims.forEach((src, i) => {
        const revArr = [];
        datesArr.forEach(time_key => {
          chartData.forEach(r => {
            if (
              r['ad_unit'] === src &&
              r['time_key'] === time_key
            ) {
              revArr.push(r['viewability']);
            }
          });
        });
        console.log("revArr",revArr);
        console.log("src",src);
        
        
        this.mainLineChartJsonView['chartData']['datasets'].push({
          label: src,
          data: revArr,
          borderColor: colors[i],
          fill: false,
          backgroundColor: colors[i]
        });
      });
      
      this.showMainLineChartView = true;


  //   this.mainLineChartJsonView = {
  //     chartTypes: [{ key: 'bar', label: 'Stack Trend', stacked: true }],
  //     chartData: {
  //       labels: [],
  //       datasets: []
  //     },
  //     chartOptions: {
  //       title: {
  //         display: true,
  //         text: ''
  //       },
  //       legend: {
  //         display: true
  //       },
  //       scales: {
  //         xAxes: [
  //           {
  //             display: true,
  //             scaleLabel: {
  //               display: true,
  //               labelString: 'Date'
  //             }
  //           }
  //         ],
  //         yAxes: [
  //           {
  //             display: true,
  //             scaleLabel: {
  //               display: true
  //             }
  //           }
  //         ]
  //       },
  //       pan: {
  //         enabled: true,
  //         mode: 'x'
  //       },
  //       zoom: {
  //         enabled: true,
  //         mode: 'x'
  //       },
  //       plugins: {
  //         datalabels: false
  //       }
  //     },
  //     zoomLabel: true,
  //     chartWidth: '',
  //     chartHeight: '300px'
  //   };

  //  let data = {
  //   "data": [{
  //     "ad_unit": "Ad Unit1",
  //     "viewability": 24,
  //     "time_key": 20200601,

  //   }, {
  //     "ad_unit": "Ad Unit2",
  //     "viewability": 12,
  //     "time_key": 20200601,

  //   }, {
  //     "ad_unit": "Ad Unit3",
  //     "viewability": 44,
  //     "time_key": 20200601,

  //   },
  //   {
  //     "ad_unit": "Ad Unit4",
  //     "viewability": 14,    
  //     "time_key": 20200602,

  //   },
  //   {
  //     "ad_unit": "Ad Unit5",
  //     "viewability": 38,
  //     "time_key": 20200602,
   
  //    },
  //    {
  //     "ad_unit": "Ad Unit1",
  //     "viewability": 24,
  //     "time_key": 20200602,

  //   },
  //   {
  //     "ad_unit": "Ad Unit6",
  //     "viewability": 53,
  //     "time_key": 20200602,

  //   },
  //   {
  //     "ad_unit": "Ad Unit7",
  //     "viewability": 35,
  //     "time_key": 20200602,

  //       },
  //   {
  //     "ad_unit": "Ad Unit8",
  //     "viewability": 32,
  //     "time_key": 20200603,

  //   },
  //   {
  //     "ad_unit": "Ad Unit9",
  //     "viewability": 16,
  //     "time_key": 20200603,

  //   },
  //   {
  //     "ad_unit": "Ad Unit10",
  //     "viewability": 28,
  //     "time_key": 20200603,

  //   },
  //   {
  //     "ad_unit": "Ad Unit1",
  //     "viewability": 24,
  //     "time_key": 20200601,
  //   }
  // ],
  //   "totalItems": 10
  // }
  // const chartData = data['data'] as [];
  
  // this.mainLineChartJsonView = this.libServ.deepCopy(this.mainLineChartJsonView);
  // this.mainLineChartJsonView['chartOptions']['title']['text'] = "Viewability Stack by Ad Units";
  // this.mainLineChartJsonView['chartOptions']['scales']['yAxes'][0]['ticks'] = {
  //   // Include a dollar sign in the ticks
  //   callback: (value, index1, values) => {
  //     return this.formatNumPipe.transform(value, 'number', []);
  //   }
  // };
  // this.mainLineChartJsonView['chartOptions']['scales']['yAxes'][0]['scaleLabel'][
  //   'labelString'
  // ] = 'Viewability';
  // this.mainLineChartJsonView['chartOptions']['tooltips'] = {
  //   callbacks: {
  //     label: (tooltipItem, data) => {
  //       const currentValue =
  //         data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
  //       return `${
  //         data.datasets[tooltipItem.datasetIndex].label
  //         } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
  //     }
  //   }
  // };

  // const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));

  // const dimData = Array.from(
  //   new Set(chartData.map(s => s['ad_unit']))
  // );
  // this.mainLineChartJsonView['chartData']['labels'] = datesArr.map(d =>
  //   moment(d, 'YYYYMMDD').format('YYYY-MM-DD')
  // );
  // const colors = this.libServ.dynamicColors(dimData.length);

  // setTimeout(() => {
  //   dimData.forEach((src, i) => {
  //     const revArr = [];
  //     datesArr.forEach(time_key => {
  //       let isavalable = false;
  //       chartData.forEach(r => {
  //         if (
  //           r["ad_unit"] === src &&
  //           r['time_key'] === time_key
  //         ) {
  //           revArr.push(r['viewability']);
  //           isavalable = true;
  //         }
  //       });
  //       if (!isavalable) {
  //         revArr.push(null);
  //       }
  //     });

  //     this.mainLineChartJsonView['chartData']['datasets'].push({
  //       label: src,
  //       data: revArr,
  //       borderColor: colors[i],
  //       fill: false,
  //       backgroundColor: colors[i]
  //     });
  //   });
  //       this.showMainLineChartView = true;
  //       this.noDataMainLineChartView = false;
  //   }, 0);
//});

//  this.showStackChart = false;
  }

  loadLineChartCon(){
    const colors = this.libServ.dynamicColors(2);
    this.lineChartJsonCon = {
      chartTypes: [{ key: 'bar', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'Conversion',
            type: 'line',
            yAxisID: 'y-axis-0',

            borderColor: colors[0],
            fill: false,
            backgroundColor: colors[0],
            data: []
          },
          {
            label: 'Conversion %',
            type: 'bar',
            yAxisID: 'y-axis-1',

            borderColor: colors[1],
            fill: false,
            backgroundColor: colors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Conversion vs Conversion %'
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Ad Units'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: 'Conversion (%)'
              },
              position: 'right',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, 'percentage', [2]);
                }
              }
              // scaleFontColor: "rgba(151,137,200,0.8)"
            },
            {
              id: 'y-axis-1',
              scaleLabel: {
                display: true,
                labelString: 'Conversion'
              },
              position: 'left',
              name: '2',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, 'number', []);
                }
              }
              // scaleFontColor: "rgba(151,187,205,0.8)"
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: true,
      chartWidth: '',
      chartHeight: '300px'
    };

    this.showLineChartCon = false;
    this.noLineChartCon = false;
    // this.dataFetchServ.getRevMgmtData(lineChartReq).subscribe(data => {
    //   if (data['status'] === 0) {
    //     this.toastService.displayToast({
    //       severity: 'error',
    //       summary: 'Server Error',
    //       detail: 'Please refresh the page'
    //     });
    //     console.log(data['status_msg']);
    //     return;
    //   }
    let data = {
      "data": [{
        "ad_unit": "YuktaNews_Home_TopLeaderboard",
        "actual_con": 24,
        "per_con": 20,
  
      }, {
        "ad_unit": "YuktaNews_Home_MidLeaderboard",
        "actual_con": 14,
        "per_con": 50,
   
      }, {
        "ad_unit": "YuktaNews_Home_FooterLeaderboard",
        "actual_con": 54,
        "per_con": 30,
      },
      {
        "ad_unit": "YuktaNews_Home_MPU",
        "actual_con": 44,
        "per_con": 70,
 
  
      },
      {
        "ad_unit": "YuktaNews_Category_TopLeaderboard",
        "actual_con": 62,
        "per_con": 60,     
       },
      {
        "ad_unit": "YuktaNews_Category_FooterLeaderboard",
        "actual_con": 74,
        "per_con": 80,  
      },
      {
        "ad_unit": "YuktaNews_Article_TopLeaderboard",
        "actual_con": 54,
        "per_con": 40,
      },
      {
        "ad_unit": "YuktaNews_Article_FooterLeaderboard",
        "actual_co6n": 45,
        "per_con": 2,
 
      },
      {
        "ad_unit": "YuktaNews_Video_TopLeaderboard",
        "actual_con": 54,
        "per_con": 24,
 
  
      },
      {
        "ad_unit": "YuktaNews_Video_FooterLeaderboard",
        "actual_con": 34,
        "per_con": 0,
   
      }
    ],
      "totalItems": 10
    }
      const chartData = data['data'];

        const datesArr = Array.from(new Set(chartData.map(r => r['ad_unit'])));

        this.lineChartJsonCon['chartData']['labels'] = datesArr.map(d =>
          d
        );

        const revArr = [];
        const ecpmArr = [];
        datesArr.forEach(time_key => {
          chartData.forEach(r => {
            if (r['ad_unit'] === time_key) {
              revArr.push(r['actual_con']);
              ecpmArr.push(r['per_con']);
            }
          });
        });
        this.lineChartJsonCon['chartData']['datasets'][0]['data'] = ecpmArr;
        this.lineChartJsonCon['chartData']['datasets'][1]['data'] = revArr;
        this.showLineChartCon = true;
        this.noLineChartCon = false;
      
  }

  tabChanged(e) {
    if (!this.tabChangedFlag) {
      this.loadLineChart();
      this.loadLineChartView();
      this.loadLineChartCon();
      this.tabChangedFlag = true;
    }
    // console.log('tab changed', e);
  }

  chartSelected(e) { }
}
