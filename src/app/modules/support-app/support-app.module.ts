import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { TabViewModule } from 'primeng/tabview';
import { TreeTableModule } from 'primeng/treetable';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MultiSelectModule } from 'primeng/multiselect';
import { DropdownModule } from 'primeng/dropdown';

import { SharedModule } from 'src/app/_pipes/shared.module';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { SupportAppComponent } from './support-app.component';
import { FilterContainerModule } from '../common/filter-container/filter-container.module';
import { CardsModule } from '../common/cards/cards.module';
import { ChartsModule } from '../common/charts/charts.module';
import { RouterModule } from '@angular/router';
import { SupportAppRoutes } from './support-app.routing.module';
import {CardModule} from 'primeng/card';
import {PanelModule} from 'primeng/panel';

@NgModule({
  declarations: [SupportAppComponent],
  // exports: [SampleAppComponent],
  imports: [
    CommonModule,
    ButtonModule,
    TabViewModule,
    TreeTableModule,
    FormsModule,
    ReactiveFormsModule,
    MultiSelectModule,
    DropdownModule,

    SharedModule,
    PanelModule,
    FilterContainerModule,
    CardsModule,
    ChartsModule,
    CardModule,
    RouterModule.forChild(SupportAppRoutes)
  ],
  providers: [FormatNumPipe]
})
export class SupportAppModule {}
