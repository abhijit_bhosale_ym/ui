import {
  Component,
  OnInit,
  Input,
  QueryList,
  ElementRef,
  ViewChildren,
  OnDestroy
} from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { FetchApiDataService } from './fetch-api-data.service';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';
import {CreateTicketComponent} from './create-ticket/create-ticket.component'
import { DialogService } from 'primeng/dynamicdialog';
import { Validators,FormControl,FormGroup,FormBuilder } from '@angular/forms';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';

@Component({
  selector: 'ym-support-app',
  templateUrl: './support-app.component.html',
  styleUrls: ['./support-app.component.scss']
})
export class SupportAppComponent implements OnInit {
  private appConfigObs: Subscription;
  @Input() isSaveView:object;
  appConfig: object = {};
  tickettitledata = [];
  userId="";
  replay="";
  public jobSearch: String = '';
  public ticketdetails : any ;
  public ticketreplies : any ;
  public ticket_id: number;
  public emptyreplyErr = '';
  public reply: any;
  public userid:any;
  public selectedid:any;
  public isGod: boolean = false;
  public displayName : string ='';
  commentForm : FormGroup;
  public getAllTicket = false;
  public createTicket = false;
  public userEmail = ""

  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private dataFetchServ: FetchApiDataService,
    private _titleService: Title,
    private dialogService: DialogService,
    private formbuilder: FormBuilder,
    private toastService: ToastService,

   ) { }

  ngOnInit(){
    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        console.log("appconfig ",this.appConfig);
        this.userId = this.appConfig['user']['id'];
        this._titleService.setTitle(this.appConfig['displayName']);
        const permission_arr = this.appConfig['permissions'];
        for (let i = 0; i < permission_arr.length; i++) {
          if (
            permission_arr[i].name == 'get-all-tickets') {
            this.getAllTicket = true;
          }

          if (
            permission_arr[i].name == 'create-ticket') {
            this.createTicket = true;
          }
        }
        this.initialLoading();
      }
    });

    this.commentForm = this.formbuilder.group({
      reply:['']
    })
  }

  initialLoading()
  {
    if(this.getAllTicket == true)
    {
      this.dataFetchServ.getallticketlist().subscribe(data => {
        this.tickettitledata = data as [];
        this.getticketdetails(this.tickettitledata[0]['id']);
      });
    }
    else {
      const req = {
        userId:this.userId,
        permission :this.getAllTicket
      };
      this.dataFetchServ.getticketlist(req).subscribe(data => {
        this.tickettitledata = data as [];
        this.getticketdetails(this.tickettitledata[0]['id']);
      });
    }
  }

  getticketdetails(id){
    // this.loaderService.start();
    this.dataFetchServ.getticketsingledata(id).subscribe(
      data => {
        this.selectedid =id;
        this.ticketdetails = data['details'][0];
        this.ticketreplies = data['replies'];
        this.userEmail = data['details'][0]['email'];
      });
  }

  sendChatMessage()
  {
    const ticketParam = {
      ticket_id: this.ticketdetails.id,
      reply: this.replay,
      title:this.ticketdetails.ticketTitle,
      displayName:this.appConfig['user']['name'],
      userId : this.userId,
      email : this.userEmail
    };
    
    if (this.replay === '' || typeof this.replay === 'undefined') {
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Error',
        detail: 'Error! Reply must be not empty!'
      });  
    }
    else{
    this.dataFetchServ.createTicketReply(ticketParam).subscribe(response => {
      
      if (response['status']) {
        // this.loaderService.complete();
        this.toastService.displayToast({
          severity: 'success',
          summary: 'Success',
          detail: 'Thank you for Reply, we will get back to you soon!!'
        });  
        this.getticketdetails(this.ticketdetails.id);
        this.replay = '';
      } else {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'error',
          detail: 'Error! failed to add and save reply'
        });  
        this.replay = '';
      }

    },
      error => {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'error',
          detail: 'Opps! Something went wrong, Please try again'
        });  
        this.reply = '';
        // this.loaderService.complete();
      }
    );
    }
    
  }

  createdTicket(e)
  {
   
  }

  openPopup() {
    const data =
    {
      userId : this.appConfig['user']['id'],
      name : this.appConfig['user']['userName'],
      email:this.appConfig['user']['email']
    }
    const ref = this.dialogService.open(CreateTicketComponent, {
      header: "Create New Ticket",
      contentStyle: { width: '50vw', overflow: 'auto' },
      data: data
    });
    ref.onClose.subscribe((data: string) => { 
      this.initialLoading();
    });
  }

  onSelect(id)
  {
    this.getticketdetails(id);
  }

}
