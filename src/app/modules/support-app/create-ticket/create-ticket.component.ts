import { Component, OnInit, OnDestroy, Output, EventEmitter, } from '@angular/core';
import { Router } from '@angular/router';
import { PlatformConfigService, CommonLibService } from 'src/app/_services';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { RoleService } from '../../../_services/users/role.service';
import { Subscription } from 'rxjs';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { DialogService } from 'primeng/dynamicdialog';
import { FetchApiDataService } from '../fetch-api-data.service';
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder
} from '@angular/forms';

@Component({
  selector: 'ym-create-ticket',
  templateUrl: './create-ticket.component.html',
  styleUrls: ['./create-ticket.component.scss']
})
export class CreateTicketComponent implements OnInit, OnDestroy {
  @Output() ticketList = new EventEmitter<object>();
  private platConfigObs: Subscription;
  ticketOfList = {}
  subject: string;
  description: string;
  selectedPermissions: any[];
  originalPermissions: any[];
  ticketForm: FormGroup;
  noPermissionsFlag = false;
  permCols: any[];
  permissionsData = [];
  userId = "";
  userName = "";
  userRoleId: any;
  email = "";

  // types:any [];
  severity: string;
  types = [
    { label: 'Highest', value: 'Highest' },
    { label: 'Medium', value: 'Medium' },
    { label: 'Lowest', value: 'Lowest' }
  ];


  constructor(
    private roleServ: RoleService,
    private dialogService: DialogService,
    private platformConfigService: PlatformConfigService,
    public libServ: CommonLibService,
    private toastService: ToastService,
    private config: DynamicDialogConfig,
    private fb: FormBuilder,
    private router: Router,
    private dataFetchServ: FetchApiDataService,
    public ref: DynamicDialogRef
  ) {
    this.userId = this.config['data']['userId'];
    this.userName = this.config['data']['name'];
    this.email = this.config['data']['userEmail']
  }
  ngOnInit() {

    this.ticketForm = this.fb.group({
      subject: [''],
      severity: [''],
      description: ['']
    });
  }

  ngOnDestroy(): void {

    if (this.platConfigObs && !this.platConfigObs.closed) {
      this.platConfigObs.unsubscribe();
    }
  }

  goBack() {
    this.dialogService.dialogComponentRef.destroy();
  }

  createTicket() {
    const ticketParam = {
      Title: this.subject,
      Data: this.description,
      userId: this.userId,
      displayName: this.userName,
      severity: this.severity['value'],
      email:this.email
    };

    console.log("in create ticket", ticketParam);
    this.dataFetchServ.createSupportTicket(ticketParam).subscribe(response => {
      console.log("response - ", response);

      if (response['status']) {
        this.ticketOfList = ticketParam;
        this.toastService.displayToast({
          severity: 'success',
          summary: 'success',
          detail: 'Thank you for reaching us, we will get back to you soon!!'
        });
        setTimeout(() => {
          this.ticketList.emit({
            ticketList: ticketParam
          });
        }, 0);
        this.ref.close(ticketParam);
        this.subject = '';
        this.description = '';
      } else {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Error! failed to add and save ticket'
        });
        setTimeout(() => {
          //  this.formDisable = false;
          this.subject = '';
          this.description = '';
        }, 0);
      }
      // this.loaderService.complete();
    },
      error => {

        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Opps! Something went wrong, Please try again'
        });

        //  this.formDisable = false;
      }
    );

  }

}
