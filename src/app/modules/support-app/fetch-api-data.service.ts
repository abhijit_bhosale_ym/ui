import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FetchApiDataService {
  private BASE_URL: string = environment.baseUrl;

  constructor(private http: HttpClient) {}

  // getTableData(params: object) {
  //   const url = `${this.BASE_URL}/prebid-data/mainTable`;
  //   if (!('isTable' in params)) {
  //     params = Object.assign(params, { isTable: true });
  //   }
  //   return this.http.post(url, params);
  // }

  createSupportTicket(params: object) {
    const url= `${this.BASE_URL}/daas/v1/daas-support/create-ticket`;
    return this.http.post(url, params);
  }

  getticketlist(params: object) {
    const url = `${this.BASE_URL}/daas/v1/daas-support/all-tickets`;
    return this.http.post(url,params);
  }
  getallticketlist() {
    const url= `${this.BASE_URL}/daas/v1/daas-support/god-all-tickets`;
    return this.http.get(url);
  }
  getticketsingledata(id) {
    const url= `${this.BASE_URL}/daas/v1/daas-support/single-tickets/${id}`;
    return this.http.get(url);
  }
  createTicketReply(ticketParam) {
    const url = `${this.BASE_URL}/daas/v1/daas-support/ticket-reply`;
    return this.http.post(url, ticketParam);
  }

  getExportReportData(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/common/exportData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
    // return <TreeNode[]> json.data;
  }
  getUPRData(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/upr/getData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
  }

  getLastUpdatedData(appId) {
    const url = `${this.BASE_URL}/frankly/v1/common/getLastUpdatedData/${appId}`;
    return this.http.get(url);
  }
}
