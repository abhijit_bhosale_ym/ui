import { Routes } from '@angular/router';

import { SupportAppComponent } from './support-app.component';
export const SupportAppRoutes: Routes = [
  {
    path: '',
    component: SupportAppComponent
    // data: {
    //   breadcrumb: 'Yield Management'
    // }
  }
];
