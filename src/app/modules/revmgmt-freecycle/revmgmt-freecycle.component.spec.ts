import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RevmgmtFreecycleComponent } from './revmgmt-freecycle.component';

describe('RevmgmtFreecycleComponent', () => {
  let component: RevmgmtFreecycleComponent;
  let fixture: ComponentFixture<RevmgmtFreecycleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RevmgmtFreecycleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RevmgmtFreecycleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
