import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DummyRevmgmtCompComponent } from './dummy-revmgmt-comp.component';

describe('DummyRevmgmtCompComponent', () => {
  let component: DummyRevmgmtCompComponent;
  let fixture: ComponentFixture<DummyRevmgmtCompComponent>;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DummyRevmgmtCompComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DummyRevmgmtCompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
