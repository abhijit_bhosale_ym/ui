import { Component, OnInit, OnDestroy } from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { FetchApiDataService } from '../fetch-api-data.service';
import { ToastService } from '../../../_services/toast-notification/toast.service';
import { DialogService } from 'primeng/dynamicdialog';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { DataShareService } from 'src/app/_services/data-share/data-share.service';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
import { Subscription } from 'rxjs';

@Component({
  selector: 'ym-dummy-revmgmt-comp',
  templateUrl: './dummy-revmgmt-comp.component.html',
  styleUrls: ['./dummy-revmgmt-comp.component.scss']
})
export class DummyRevmgmtCompComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;
  popupPieChartJson: object;
  mainLineChartJson: object;
  time_key: String;
  showMainLineChart = false;
  showMainPieChart = false;
  filtersApplied: object;
  noDataMainLineChart = false;
  noDataPieChart = false;

  constructor(
    private appConfigService: AppConfigService,

    private libServ: CommonLibService,
    private dataFetchServ: FetchApiDataService,
    private toastService: ToastService,
    private dialogService: DialogService,
    private htmltoimage: HtmltoimageService,
    private pptExport: ExportPptService,
    private exportService: ExportdataService,
    private formatNumPipe: FormatNumPipe,
    private dataShareService: DataShareService,
    public config: DynamicDialogConfig
  ) { }

  ngOnInit() {
    this.time_key = this.config.data['time_key'];
    this.filtersApplied = this.config.data['filterData'];

    const mainLineChartReq = {
      dimensions: ['source'],
      metrics: ['dp_revenue', 'dp_impressions'],
      derived_metrics: ['rev_cpm'],
      timeKeyFilter: this.time_key,
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [],
      limit: '',
      offset: ''
    };
    // For dev purpose
    // mainLineChartReq['gidGroupBy'] = [
    //   'ad_size',
    //   'ad_type',
    //   'ad_units',
    //   'bidding_type',
    //   'device_category',
    //   'geography',
    //   'sales_channel',
    //   'sales_type',
    //   'site',
    //   'source'
    // ];
    this.loadMainLineChart(mainLineChartReq);
  }

  loadMainLineChart(params) {
    const colors = this.libServ.dynamicColors(2);
    this.mainLineChartJson = {
      chartTypes: [{ key: 'bar', label: 'Bar Chart', stacked: false }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'Revenue',
            type: 'bar',
            yAxisID: 'y-axis-1',

            borderColor: colors[1],
            fill: false,
            backgroundColor: colors[1],
            data: []
          },
          {
            label: 'Impressions',
            type: 'bar',
            yAxisID: 'y-axis-0',

            borderColor: colors[0],
            fill: false,
            backgroundColor: colors[0],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Impression Trend Column-Chart'
        },
        legend: {
          display: true,
          // onClick: e => e.stopPropagation()
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Sources'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: 'Impressions'
              },
              position: 'left',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, 'number', []);
                }
              }
              // scaleFontColor: "rgba(151,137,200,0.8)"
            },
            {
              id: 'y-axis-1',
              scaleLabel: {
                display: true,
                labelString: 'Revenue ($)'
              },
              position: 'right',
              name: '2',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', [0]);
                }
              }
              // scaleFontColor: "rgba(151,187,205,0.8)"
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              if (tooltipItem.datasetIndex !== 0) {
                return `${
                  data.datasets[tooltipItem.datasetIndex].label
                  } : ${this.formatNumPipe.transform(
                    currentValue,
                    'number',
                    []
                  )}`;
              }
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '450px'
    };

    this.showMainLineChart = false;
    this.noDataMainLineChart = false;
    this.appConfigObs = this.dataFetchServ.getTableData(params).subscribe(data => {
      const chartData = data['data'];
      if (typeof chartData === 'undefined' || !chartData.length) {
        this.noDataMainLineChart = true;
      } else {
        const datesArr = Array.from(
          new Set(chartData.map(r => r['source']))
        );

        this.mainLineChartJson['chartData']['labels'] = datesArr;
        const revArr = [];
        const ecpmArr = [];
        datesArr.forEach(time_key => {
          chartData.forEach(r => {
            if (r['source'] === time_key) {
              revArr.push(r['dp_revenue']);
              ecpmArr.push(r['dp_impressions']);
            }
          });
        });
        this.mainLineChartJson['chartData']['datasets'][0]['data'] = revArr;
        this.mainLineChartJson['chartData']['datasets'][1]['data'] = ecpmArr;
        this.showMainLineChart = true;
        this.noDataMainLineChart = false;
      }
    });
  }

  loadMainPieChart(params) {
    this.popupPieChartJson = {
      chartTypes: [{ key: 'pie', label: 'Pie Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Demand Partner Mix'
        },
        legend: {
          display: false
        },
        elements: {
          arc: {
            borderWidth: 0
          }
        },
        plugins: {
          datalabels: {
            formatter: (value, ctx) => {
              let sum = 0;
              const dataArr = ctx.chart.data.datasets[0].data;
              dataArr.map(data => {
                sum += data;
              });
              const percentage = (value * 100) / sum;
              if (percentage > 10) {
                return `${percentage.toFixed(2)} %`;
                // ${this.formatNumPipe.transform(
                //   value,
                //   '$',
                //   []
                // )} - ${percentage.toFixed(2)} %`;
              } else {
                return '';
              }
            },
            color: 'black'
          }
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };
    this.popupPieChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          let sum = 0;
          const dataArr = data.datasets[0].data;
          dataArr.map(data => {
            sum += data;
          });
          const percentage = (currentValue * 100) / sum;
          return `${
            data.labels[tooltipItem.index]
            } : ${this.formatNumPipe.transform(
              currentValue,
              '$',
              []
            )} - ${percentage.toFixed(2)} %`;
        }
      }
    };
    this.showMainPieChart = false;
    this.noDataPieChart = false;
    params['orderBy'] = [{ key: 'dp_revenue', opcode: 'desc' }];

    this.dataFetchServ.getTableData(params).subscribe(data => {
      const chartData = data['data'];
      if (typeof chartData === 'undefined' || !chartData.length) {
        this.noDataPieChart = true;
      } else {
        const sources = Array.from(
          new Set(chartData.map(s => s['source']))
        );
        const colors = this.libServ.dynamicColors(sources.length);
        this.popupPieChartJson['chartData']['labels'] = sources;
        this.popupPieChartJson['chartData']['datasets'][0]['data'] = Array.from(
          new Set(chartData.map(s => s['dp_revenue']))
        );

        this.popupPieChartJson['chartData']['datasets'][0][
          'backgroundColor'
        ] = colors;
        this.showMainPieChart = true;
        this.noDataPieChart = false;
      }
    });
  }

  tabChanged(e) {
    const popupPieChartReq = {
      dimensions: ['source'],
      metrics: ['dp_revenue'],
      derived_metrics: [],
      timeKeyFilter: this.time_key, // {'time_key1': "20190801", 'time_key2': "20190808"},//this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [],
      limit: '5',
      offset: 0
    };
    this.loadMainPieChart(popupPieChartReq);
    // console.log('tab changed', e);
  }
  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }

  chartSelected(e) { }
}
