import {
  Component,
  OnInit,
  QueryList,
  ElementRef,
  ViewChildren,
  OnDestroy
} from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { FetchApiDataService } from './fetch-api-data.service';
import { ToastService } from '../../_services/toast-notification/toast.service';
import { ConfirmationService, TreeNode } from 'primeng/api';
import { DummyRevmgmtCompComponent } from './dummy-revmgmt-comp/dummy-revmgmt-comp.component';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import * as moment from 'moment';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { Title } from '@angular/platform-browser';
import { DataShareService } from 'src/app/_services/data-share/data-share.service';
import { Subscription } from 'rxjs';
import { float } from 'html2canvas/dist/types/css/property-descriptors/float';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { SendEmailComponent } from '../send-email/send-email.component';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { environment } from 'src/environments/environment';
import { DialogService } from 'primeng';

@Component({
  selector: 'ym-revmgmt-freecycle',
  templateUrl: './revmgmt-freecycle.component.html',
  styleUrls: ['./revmgmt-freecycle.component.scss']
})
export class RevmgmtFreecycleComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;
  timeZone = environment.timeZone;
  appConfig: object = {};
  cardsJson = [];
  showCards = false;
  noDataPieChart = false;
  showMainLineChart = false;
  noDataMainLineChart = false;
  showMainPieChart = false;
  showCharts = false;
  defaultChartsJson: object;
  mainLineChartJson: object;
  mainPieChartJson: object;
  revenueChartJson: object;
  ecpmChartJson: object;
  fillrateChartJson: object;
  impressionsChartJson: object;
  searchInput: any;
  cols: any[];
  conditionNames: any[];
  timeout: any;
  fieldName: any;
  value: any;
  isExportReport = false;
  @ViewChildren('ttAgg') aggTableRef: QueryList<ElementRef>;
  @ViewChildren('ttFlat') flatTableRef: QueryList<ElementRef>;

  /* ---------------------------------- Aggreagated Table JSON --------------------------------- */
  sourcesJson = {
    'ADX': {
      columns: [
        'adx_dp_revenue',
        'adx_rev_cpm',
        'adx_dp_impressions',
        'adx_fill_rate',
      ],
      displayName: 'ADX',
      defaultView: true,
      width: 150
    },
    OpenX: {
      columns: [
        'openx_dp_revenue',
        'openx_rev_cpm',
        'openx_fill_rate',
        'openx_ctr',
        'openx_dp_impressions',
        'openx_dp_clicks'
      ],
      displayName: 'OpenX',
      defaultView: true,
      width: 150
    },
    'Sovrn': {
      columns: [
        'sovrn_dp_revenue',
        'sovrn_rev_cpm',
        'sovrn_fill_rate',
        'sovrn_dp_impressions',
      ],
      displayName: 'Sovrn',
      defaultView: true,
      width: 160
    },
  };

  chartsMultiselectButton: object = {
    data: [
      {
        label: 'Revenue',
        value: 'revenue'
      },
      {
        label: 'eCPM',
        value: 'ecpm'
      },
      {
        label: 'Fill Rate',
        value: 'fillrate'
      },
      {
        label: 'Impressions',
        value: 'impressions'
      }
    ],
    model: ['revenue']
  };

  lastUpdatedOn: Date;
  /* ---------------------------------- Table --------------------------------- */

  flatTableReq: object;
  aggreTableReq: object;
  flatTableData: TreeNode[];
  flatTableColumnDef: any[];
  flatTableJson: Object;

  aggTableData: TreeNode[];
  aggTableColumnDef: any[];
  aggTableJson: object;
  jumpToSource: object = {
    totalSources: [],
    selected: []
  };
  showJumpTo = true;
  nextUpdated: Date;
  dataUpdatedThrough: Date;

  selectedChartName;
  filtersApplied: object = {};
  exportRequest: ExportRequest = <ExportRequest>{};

  appFeaturesConfigs = {};
  mainCardsConfig = {};

  /* ---------------------------------- Table --------------------------------- */

  constructor(
    private appConfigService: AppConfigService,
    private _titleService: Title,
    private libServ: CommonLibService,
    private dataFetchServ: FetchApiDataService,
    private toastService: ToastService,
    private dialogService: DialogService,
    private htmltoimage: HtmltoimageService,
    private pptExport: ExportPptService,
    private exportService: ExportdataService,
    private formatNumPipe: FormatNumPipe,
    private confirmationService: ConfirmationService
  ) { }

  ngOnInit() {
    /* --------------------------------- Charts --------------------------------- */

    this.defaultChartsJson = {
      chartTypes: [
        { key: 'line', label: 'Linear Trend' },
        { key: 'bar', label: 'Stack Trend', stacked: true }
        // { key: 'pie', label: 'Pie Chart' },
        // { key: 'radar', label: 'Radar Chart' },
        // { key: 'doughnut', label: 'Doughnut Chart' },
        // { key: 'polarArea', label: 'PolarArea Chart' },
        // { key: 'bubble', label: 'Bubble Chart' }
      ],
      chartData: {
        labels: [],
        datasets: []
      },
      chartOptions: {
        title: {
          display: true,
          text: ''
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              display: true,
              scaleLabel: {
                display: true
              }
            }
          ]
        },
        pan: {
          enabled: true,
          mode: 'x'
        },
        zoom: {
          enabled: true,
          mode: 'x'
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    /* ------------------------------- Table Start ------------------------------ */

    this.flatTableColumnDef = [
      {
        field: 'time_key',
        displayName: 'Date',
        format: 'date',
        width: '150',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'source',
        displayName: 'Source',
        format: '',
        value: '',
        condition: 'like',
        columnType: 'dimensions',
        isClearSearch: true,
        width: '175',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'dp_revenue',
        displayName: 'Revenue',
        format: '$',
        width: '175',
        value: '',
        condition: 'GTE',
        columnType: 'metrics',
        isClearSearch: true,
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'rev_cpm',
        displayName: 'eCPM',
        format: '$',
        width: '175',
        value: '',
        condition: 'GTE',
        columnType: 'metrics',
        isClearSearch: true,
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'fill_rate',
        displayName: 'Fill Rate',
        format: 'percentage',
        width: '175',
        value: '',
        condition: 'GTE',
        columnType: 'metrics',
        isClearSearch: true,
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'dp_impressions',
        displayName: 'Paid Impressions',
        format: 'number',
        value: '',
        condition: 'GTE',
        columnType: 'metrics',
        isClearSearch: true,
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        width: '175',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      }
    ];
    this.flatTableJson = {
      page_size: 10,
      page: 0,
      lazy: false,
      loading: true,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '200px',
      totalRecords: 1000,
      columns: this.flatTableColumnDef.slice(1),
      selectedColumns: this.flatTableColumnDef.slice(1),
      frozenCols: [this.flatTableColumnDef[0]],
      // frozenWidth: '250px',
      frozenWidth:
        this.flatTableColumnDef
          .slice(0, 1)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: true
    };
    this.aggTableColumnDef = [
      {
        field: 'time_key',
        displayName: 'Month-Year',
        format: 'date',
        formatConfig: ['MMM-YY'],
        width: '145',
        exportConfig: {
          format: 'date<<MMM-YY<<-',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'time_key1',
        displayName: 'Date',
        format: 'date',
        width: '125',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: ['MM-DD-YYYY'],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'source_revenue',
        displayName: 'Total Revenue',
        width: '125',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'source_rev_cpm',
        displayName: 'Total eCPM',
        format: '$',
        width: '125',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'source_impressions',
        displayName: 'Total Impressions',
        format: 'number',
        width: '135',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      }
    ];
    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '300px',
      totalRecords: 1000,
      columns: this.aggTableColumnDef.slice(5),
      selectedColumns: this.aggTableColumnDef.slice(5),
      frozenCols: [...this.aggTableColumnDef.slice(0, 5)],
      frozenWidth:
        this.aggTableColumnDef
          .slice(0, 5)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: false,
      columnSearch: false
    };

    this.conditionNames = [
      { label: 'GTE', value: '<<GTE' },
      { label: 'LTE', value: '<<LTE' }
      // { label: 'Equal', value: '' },
      // { label: 'like', value: '<<like' }
    ];

    this.searchInput = '>';
    // this.fieldName='revenue';
    this.value = this.conditionNames[0].value;

    /* -------------------------------- Table End ------------------------------- */

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        console.log('appConfig', this.appConfig);
        this.isExportReport = this.appConfig['permissions'].some(
          o => o.name === 'rev-mgmt-export-reports-fc'
        );
        this._titleService.setTitle(this.appConfig['displayName']);
        let startDate;
        let endDate;
        const date_config = this.appConfig['filter']['filterConfig']['filters'][
          'datePeriod'
        ][0];

        if (
          date_config['defaultDate'][0]['value'] === 0 &&
          date_config['defaultDate'][0]['period'] === 'month' &&
          (moment().format('DD') === '01' || moment().format('DD') === '02')
        ) {
          startDate = moment()
            .subtract(1, 'months')
            .startOf('month');
          endDate = moment()
            .subtract(1, 'months')
            .endOf('month');
        } else {
          if (date_config['defaultDate'][0]['startOf']) {
            startDate = moment()
              .subtract(
                date_config['defaultDate'][0]['value'],
                date_config['defaultDate'][0]['period']
              )
              .startOf(date_config['defaultDate'][0]['period']);
          } else {
            startDate = moment().subtract(
              date_config['defaultDate'][0]['value'],
              date_config['defaultDate'][0]['period']
            );
          }
          endDate = moment().subtract(
            date_config['defaultDate'][1]['value'],
            date_config['defaultDate'][1]['period']
          );
        }
        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: startDate.format('YYYYMMDD'),
            time_key2: endDate.format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: this.appConfig['filter']['filterConfig']['groupBy'].filter(
            v => v.selected
          )
        };
        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        this.initialLoading();
      }
    });
  }

  /* ------------------------------- Table Start ------------------------------ */

  initialLoading() {
    console.log('this.appConfig last up', this.appConfig['id']);
    this.dataFetchServ
      .getLastUpdatedData(this.appConfig['id'])
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        this.lastUpdatedOn = moment(data[0]['updated_at']).toDate();
        this.nextUpdated = moment(data[0]['next_run_at']).toDate();
        this.dataUpdatedThrough = moment(
          data[0]['source_updated_through'],
          'YYYYMMDD'
        ).toDate();
      });

    this.libServ.getAppFeaturesConfigs(this.appConfig['id']).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: data['message']
        });
        console.log(data['message']);
        return;
      }
      this.appFeaturesConfigs = data['data'];
      this.appFeaturesConfigs['cardsConfig']['cards'].forEach(element => {
        if (element['name'] === 'main') {
          this.mainCardsConfig = element['config'];
          this.cardsJson = this.mainCardsConfig['list'];
          if (this.mainCardsConfig['display']) {
            this.loadCards();
          }
        }
      });
    });

    const mainLineChartReq = {
      dimensions: ['time_key'],
      metrics: ['dp_revenue', 'dp_impressions'],
      derived_metrics: ['rev_cpm'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };

    this.loadMainLineChart(mainLineChartReq);
    const mainPieChartReq = {
      dimensions: ['source'],
      metrics: ['dp_revenue'],
      derived_metrics: [],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [],
      limit: '',
      offset: ''
    };
    this.loadMainPieChart(mainPieChartReq);

    const chartReq = {
      dimensions: ['source', 'time_key'],
      metrics: [
        'dp_revenue',
        'dp_impressions',
        'dp_clicks',
        'dp_total_requests'
      ],
      derived_metrics: ['rev_cpm', 'fill_rate'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };
    this.loadCharts(chartReq);
    this.flatTableReq = {
      dimensions: ['source', 'time_key'],
      metrics: [
        'dp_revenue',
        'dp_impressions',
        'dp_total_requests',
      ],
      derived_metrics: ['rev_cpm', 'fill_rate'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [],
      limit: '',
      offset: ''
    };

    this.loadFlatTableData(this.flatTableReq);

    this.aggreTableReq = {
      dimensions: ['accounting_key'],
      metrics: [
        'dp_revenue',
        'dp_impressions',
        'dp_clicks',
        // 'rev_dfp_adserver_impressions',
        'dp_total_requests'
      ],
      derived_metrics: ['rev_cpm', 'ctr', 'fill_rate'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [],
      limit: '',
      offset: ''
    };
  }

  loadMainLineChart(params) {
    const colors = this.libServ.dynamicColors(2);
    this.mainLineChartJson = {
      chartTypes: [{ key: 'bar', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'eCPM',
            type: 'line',
            yAxisID: 'y-axis-0',

            borderColor: colors[0],
            fill: false,
            backgroundColor: colors[0],
            data: []
          },
          {
            label: 'Revenue',
            type: 'bar',
            yAxisID: 'y-axis-1',

            borderColor: colors[1],
            fill: false,
            backgroundColor: colors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Revenue vs eCPM'
        },
        legend: {
          display: true,
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: 'eCPM ($)'
              },
              position: 'right',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', []);
                }
              }
              // scaleFontColor: 'rgba(151,137,200,0.8)'
            },
            {
              id: 'y-axis-1',
              scaleLabel: {
                display: true,
                labelString: 'Revenue ($)'
              },
              position: 'left',
              name: '2',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', [0]);
                }
              }
              // scaleFontColor: 'rgba(151,187,205,0.8)'
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    this.showMainLineChart = false;
    this.dataFetchServ.getTableData(params).subscribe(data => {
      const chartData = data['data'];
      this.noDataMainLineChart = false;
      if (!chartData.length) {
        this.noDataMainLineChart = true;
        return;
      }
      const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));

      this.mainLineChartJson['chartData']['labels'] = datesArr.map(d =>
        moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
      );

      const revArr = [];
      const ecpmArr = [];
      datesArr.forEach(time_key => {
        chartData.forEach(r => {
          if (r['time_key'] === time_key) {
            revArr.push(r['dp_revenue']);
            ecpmArr.push(r['rev_cpm']);
          }
        });
      });
      this.mainLineChartJson['chartData']['datasets'][0]['data'] = ecpmArr;
      this.mainLineChartJson['chartData']['datasets'][1]['data'] = revArr;
      this.showMainLineChart = true;
    });
  }

  loadMainPieChart(params) {
    params['orderBy'] = [{ key: 'dp_revenue', opcode: 'desc' }];

    this.mainPieChartJson = {
      chartTypes: [{ key: 'pie', label: 'Pie Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Revenue Distribution Across Partners'
        },
        legend: {
          display: false
        },
        elements: {
          arc: {
            borderWidth: 0
          }
        },
        plugins: {
          datalabels: {
            formatter: (value, ctx) => {
              let sum = 0;
              const dataArr = ctx.chart.data.datasets[0].data;
              dataArr.map(data => {
                sum += data;
              });
              const percentage = (value * 100) / sum;
              if (percentage > 10) {
                return `${percentage.toFixed(2)} %`;
              } else {
                return '';
              }
            },
            color: 'black'
          }
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };
    this.mainPieChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          let sum = 0;
          const dataArr = data.datasets[0].data;
          dataArr.map(data => {
            sum += data;
          });
          const percentage = (currentValue * 100) / sum;
          return `${
            data.labels[tooltipItem.index]
            } : ${this.formatNumPipe.transform(
              currentValue,
              '$',
              []
            )} - ${percentage.toFixed(2)} %`;
        }
      }
    };
    this.showMainPieChart = false;
    this.dataFetchServ.getTableData(params).subscribe(data => {
      const chartData = data['data'];
      if (typeof chartData === 'undefined' || !chartData.length) {
        this.noDataPieChart = true;
      } else {
        const sources = Array.from(
          new Set(chartData.map(s => s['source']))
        );
        const colors = this.libServ.dynamicColors(sources.length);
        this.mainPieChartJson['chartData']['labels'] = sources;
        this.mainPieChartJson['chartData']['datasets'][0]['data'] = Array.from(
          new Set(chartData.map(s => s['dp_revenue']))
        );
        this.mainPieChartJson['chartData']['datasets'][0][
          'backgroundColor'
        ] = colors;
        this.showMainPieChart = true;
        this.noDataPieChart = false;
      }
    });
  }

  resetPagination(tt1, tt2) {
    if (typeof tt1 !== 'undefined') {
      tt1.reset();
    }
    if (typeof tt2 !== 'undefined') {
      tt2.reset();
    }
  }

  loadCards() {
    this.showCards = false;
    const apiUrl = this.mainCardsConfig['api']['url'];
    const cardsReq = this.libServ.deepCopy(
      this.mainCardsConfig['api']['request']
    );
    cardsReq['timeKeyFilter'] = this.libServ.deepCopy(
      this.filtersApplied['timeKeyFilter']
    );
    cardsReq['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);

    this.libServ.getDataFromDynamicAPI(apiUrl, cardsReq).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error - Contact to administator',
          detail: data
        });
        console.log(data['status_msg']);
        return;
      }
      if (!this.libServ.isEmptyObj(data['data'])) {
        let projected_revenue = this.loadCardsOld(data['data'][0]['dp_revenue']);
        if(projected_revenue != 'N/A')
        data['data'][0]['projected_revenue'] = this.loadCardsOld(data['data'][0]['dp_revenue']);
        this.cardsJson.map(o => {
          o['value'] = data['data'][0][o['field']];          
        });
      } else {
        this.cardsJson.map(o => {
          o['value'] = 0;
        });
      }
      this.showCards = this.mainCardsConfig['display'];
    });
  }

  loadCardsOld(dp_revenue) {
    const endDate = moment(
      this.filtersApplied['timeKeyFilter']['time_key2']
    ).isAfter(moment())
      ? moment().format('YYYYMMDD')
      : moment(this.filtersApplied['timeKeyFilter']['time_key2']).format(
        'YYYYMMDD'
      );
    const startDate = moment(
      this.filtersApplied['timeKeyFilter']['time_key1']
    ).isAfter(moment())
      ? moment()
        .startOf('month')
        .format('YYYYMMDD')
      : moment(this.filtersApplied['timeKeyFilter']['time_key1']).format(
        'YYYYMMDD'
      );
    const newEndDate = moment(this.dataUpdatedThrough).format('YYYYMMDD');
    const monthEnd = moment(startDate)
      .endOf('month')
      .format('YYYYMMDD');
    let dateDiff = parseInt(newEndDate) - (parseInt(startDate) - 1);
    if (
      dateDiff &&
      !moment(
        this.filtersApplied['timeKeyFilter']['time_key1'],
        'YYYYMMDD'
      ).isBefore(moment().startOf('month'))
    ) {
      const dailyRev = parseFloat(dp_revenue) / dateDiff;
      dateDiff = parseInt(monthEnd) - (parseInt(startDate) - 1);
      console.log("monthDiff",dateDiff);
      
      const expectedRev = dailyRev * dateDiff;      
      return expectedRev;
    } else {
      return 'N/A';
    }
  }

  loadCharts(params: Object) {
    // Reset Data
    this.revenueChartJson = this.libServ.deepCopy(this.defaultChartsJson);
    this.revenueChartJson['chartOptions']['title']['text'] = 'Revenue Trend';
    this.revenueChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
      // Include a dollar sign in the ticks
      callback: (value, index, values) => {
        return this.formatNumPipe.transform(value, '$', []);
      }
    };
    this.revenueChartJson['chartOptions']['scales']['yAxes'][0]['scaleLabel'][
      'labelString'
    ] = 'Revenue ($)';
    this.revenueChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return `${
            data.datasets[tooltipItem.datasetIndex].label
            } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
        }
      }
    };
    this.ecpmChartJson = this.libServ.deepCopy(this.defaultChartsJson);
    this.ecpmChartJson['chartTypes'] = [
      { key: 'line', label: 'Linear Trend' }
    ]
    this.ecpmChartJson['chartOptions']['title']['text'] = 'eCPM Trend';
    this.ecpmChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
      // Include a dollar sign in the ticks
      callback: (value, index, values) => {
        return this.formatNumPipe.transform(value, '$', []);
      }
    };
    this.ecpmChartJson['chartOptions']['scales']['yAxes'][0]['scaleLabel'][
      'labelString'
    ] = 'eCPM ($)';
    this.ecpmChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return `${
            data.datasets[tooltipItem.datasetIndex].label
            } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
        }
      }
    };
    this.fillrateChartJson = this.libServ.deepCopy(this.defaultChartsJson);
    this.fillrateChartJson['chartTypes'] = [
      { key: 'line', label: 'Linear Trend' }
    ]
    this.fillrateChartJson['chartOptions']['title']['text'] = 'Fill Rate Trend';
    this.fillrateChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
      // Include a dollar sign in the ticks
      callback: (value, index, values) => {
        return this.formatNumPipe.transform(value, 'percentage', [2]);
      }
    };
    this.fillrateChartJson['chartOptions']['scales']['yAxes'][0]['scaleLabel'][
      'labelString'
    ] = 'Fill Rate (%)';
    this.fillrateChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return `${
            data.datasets[tooltipItem.datasetIndex].label
            } : ${this.formatNumPipe.transform(currentValue, 'percentage', [2])}`;
        }
      }
    };
    this.impressionsChartJson = this.libServ.deepCopy(this.defaultChartsJson);
    this.impressionsChartJson['chartOptions']['title']['text'] =
      'Paid Impressions Trend';
    this.impressionsChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
      // Include a dollar sign in the ticks
      callback: (value, index, values) => {
        return this.formatNumPipe.transform(value, 'number', []);
      }
    };
    this.impressionsChartJson['chartOptions']['scales']['yAxes'][0][
      'scaleLabel'
    ]['labelString'] = 'Impressions';
    this.impressionsChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return `${
            data.datasets[tooltipItem.datasetIndex].label
            } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
        }
      }
    };
    this.showCharts = false;
    this.dataFetchServ.getTableData(params).subscribe(data => {
      const chartData = data['data'];

      if (typeof chartData === 'undefined' || !chartData.length) {
        this.showCharts = false;
      } else {
        const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));

        const sources = Array.from(
          new Set(chartData.map(s => s['source']))
        );
        const colors = this.libServ.dynamicColors(sources.length);
        this.revenueChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );
        this.ecpmChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );
        this.fillrateChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );
        this.impressionsChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );

        sources.forEach((src, i) => {
          const revArr = [];
          const ecpmArr = [];
          const fillrateArr = [];
          const impArr = [];
          datesArr.forEach(time_key => {
            chartData.forEach(r => {
              if (r['source'] === src && r['time_key'] === time_key) {
                revArr.push(r['dp_revenue']);
                ecpmArr.push(r['rev_cpm']);
                fillrateArr.push(r['fill_rate']);
                impArr.push(r['dp_impressions']);
              }
            });
          });

          this.revenueChartJson['chartData']['datasets'].push({
            label: src,
            data: revArr,
            borderColor: colors[i],
            fill: false,
            backgroundColor: colors[i]
          });
          this.ecpmChartJson['chartData']['datasets'].push({
            label: src,
            data: ecpmArr,
            borderColor: colors[i],
            fill: false,
            backgroundColor: colors[i]
          });
          this.fillrateChartJson['chartData']['datasets'].push({
            label: src,
            data: fillrateArr,
            borderColor: colors[i],
            fill: false,
            backgroundColor: colors[i]
          });
          this.impressionsChartJson['chartData']['datasets'].push({
            label: src,
            data: impArr,
            borderColor: colors[i],
            fill: false,
            backgroundColor: colors[i]
          });
        });
        this.showCharts = true;
      }
    });
  }

  onLazyLoadFlatTable(e: Event) {
    if (
      typeof this.flatTableReq !== 'undefined' &&
      this.flatTableJson['lazy']
    ) {
      console.log('onLazyLoadFlatTable', this.flatTableJson['lazy']);
      let orderby = [];
      if (
        typeof e['multiSortMeta'] !== 'undefined' &&
        e['multiSortMeta'] !== null
      ) {
        e['multiSortMeta'].forEach(sort => {
          orderby.push({
            key: sort['field'],
            opcode: sort['order'] === -1 ? 'desc' : 'asc'
          });
        });
      } else {
        orderby = [{ key: 'time_key', opcode: 'ASC' }];
      }
      // orderby = orderby.slice(0, orderby.length - 1);
      this.flatTableReq['limit'] = e['rows'];
      this.flatTableReq['offset'] = e['first'];
      this.flatTableReq['orderBy'] = orderby;
      this.loadFlatTableData(this.flatTableReq);
    }
  }

  onLazyLoadAggTable(e: Event) {
    console.log('Lazy Agg', e);
  }

  loadFlatTableData(params: Object) {
    this.flatTableJson['loading'] = true;

    this.dataFetchServ.getTableData(params).subscribe(data => {
      const arr = [];
      for (const r of data['data']) {
        const obj = {};
        obj['data'] = r;
        arr.push(obj);
      }
      this.flatTableData = <TreeNode[]>arr;
      this.flatTableJson['totalRecords'] = data['totalItems'];
      this.flatTableJson['loading'] = false;
      setTimeout(() => {
        this.flatTableJson['lazy'] = true;
      }, 0);
    });
  }

  isHiddenColumn(col: Object) {
    return (
      this.flatTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.flatTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }

  onTableDrill(e: Event) {
    if (!e['node']['childLoaded']) {
      this.aggTableJson['loading'] = true;

      let time_key1 = moment(e['node']['data']['time_key'], 'YYYYMMDD')
        .startOf('month')
        .format('YYYYMMDD');
      if (time_key1 < this.filtersApplied['timeKeyFilter']['time_key1']) {
        time_key1 = this.filtersApplied['timeKeyFilter']['time_key1'];
      }
      let time_key2 = moment(e['node']['data']['time_key'], 'YYYYMMDD')
        .endOf('month')
        .format('YYYYMMDD');
      if (time_key2 > this.filtersApplied['timeKeyFilter']['time_key2']) {
        time_key2 = this.filtersApplied['timeKeyFilter']['time_key2'];
      }

      const timeKeyFilter = {
        time_key1: time_key1,
        time_key2: time_key2
      };

      const totalsColreq = {
        dimensions: ['time_key'],
        metrics: ['dp_revenue', 'dp_impressions'],
        derived_metrics: ['rev_cpm'],
        timeKeyFilter: timeKeyFilter, // this.filtersApplied['timeKeyFilter'],
        filters: this.libServ.deepCopy(this.filtersApplied['filters']),
        groupByTimeKey: {
          key: ['time_key', 'accounting_key'],
          interval: 'daily'
        },
        gidGroupBy: ['source'],
        orderBy: [],
        limit: '',
        offset: ''
      };

      const grpBys = this.filtersApplied['groupby'].map(e => e.key);
      for (const g of grpBys) {
        if (e['node']['data'][g] !== 'All') {
          totalsColreq['dimensions'].push(g);

          if (
            totalsColreq['filters']['dimensions'].findIndex(
              e => e.key === g
            ) === -1
          ) {
            totalsColreq['filters']['dimensions'].push({
              key: g,
              values: [e['node']['data'][g]]
            });
          } else {
            if (
              totalsColreq['filters']['dimensions']
                .find(e => e.key === g)
              ['values'].findIndex(v => v === e['node']['data'][g]) === -1
            ) {
              totalsColreq['filters']['dimensions'].push(e['node']['data'][g]);
            }
          }
        } else {
          totalsColreq['dimensions'].push(g);
          break;
        }
      }

      totalsColreq['gidGroupBy'] = ['source'];
      const sourcesDatareq = {
        dimensions: ['source', 'time_key', 'accounting_key'],
        metrics: [
          'dp_revenue',
          'dp_impressions',
          'dp_clicks',
          'dfp_adserver_impressions',
          'dp_total_requests'
        ],
        derived_metrics: ['rev_cpm', 'fill_rate', 'ctr'],
        timeKeyFilter: timeKeyFilter, // this.filtersApplied['timeKeyFilter'],
        filters: this.libServ.deepCopy(this.filtersApplied['filters']),
        groupByTimeKey: {
          key: ['time_key', 'accounting_key'],
          interval: 'daily'
        },
        gidGroupBy: ['source'],
        orderBy: [],
        limit: '',
        offset: ''
      };

      this.dataFetchServ.getTableData(totalsColreq).subscribe(data => {
        this.dataFetchServ.getTableData(sourcesDatareq).subscribe(data1 => {
          const totalsColData = data['data'];
          const srcWiseData = data1['data'];

          const map = this.dataMapping(totalsColData, srcWiseData);
          map.map(o => delete o['children']);
          map.map(o => delete o['data']['time_key']);

          this.aggTableJson['loading'] = false;

          e['node']['children'] = map;
          this.aggTableData = [...this.aggTableData];
          e['node']['childLoaded'] = true;
        });
      });
    }
  }

  /* -------------------------------- Table End ------------------------------- */

  /* ------------------------------- Load Charts ------------------------------ */

  chartSelected(data: Event) {
    console.log('Chart Selected', data);
  }

  getChartJson(chartName) {
    switch (chartName) {
      case 'revenue':
        return this.revenueChartJson;
      case 'ecpm':
        return this.ecpmChartJson;
      case 'fillrate':
        return this.fillrateChartJson;
      case 'impressions':
        return this.impressionsChartJson;
      default:
        break;
    }
  }

  /* --------------------- On Filter Apply Button Clicked --------------------- */

  onFiltersApplied(filterData: object) {
    this.filtersApplied['filters']['dimensions'] = [];
    if (filterData['filter']['dimensions']['source'] != undefined) {
      // for (const source in this.sourcesJson) {
      //   this.sourcesJson[source]['defaultView'] = false;
      // }
      filterData['filter']['dimensions']['source'].forEach(item => {
        if (this.sourcesJson[item] != undefined) {
          this.sourcesJson[item]['defaultView'] = true;
        }
      });

      for (const k in filterData['filter']['dimensions']) {
        if (filterData['filter']['dimensions'][k].length !== 0) {
          this.filtersApplied['filters']['dimensions'].push({
            key: k,
            values: filterData['filter']['dimensions'][k]
          });
        }
      }

      this.filtersApplied['groupby'] = filterData['groupby'];
    }
    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];

    if (this.mainCardsConfig['display']) {
      this.loadCards();
    }

    const chartReq = {
      dimensions: ['source', 'time_key'],
      metrics: [
        'dp_revenue',
        'dp_impressions',
        'dp_clicks',
        'dp_total_requests'
      ],
      derived_metrics: ['rev_cpm', 'fill_rate'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };

    this.loadCharts(chartReq);

    this.flatTableReq = {
      dimensions: ['source', 'time_key'],
      metrics: [
        'dp_revenue',
        'dp_impressions',
        'dp_total_requests',
      ],
      derived_metrics: ['rev_cpm', 'fill_rate'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [],
      limit: '',
      offset: ''
    };
    this.flatTableJson['lazy'] = false;
    setTimeout(() => {
      this.loadFlatTableData(this.flatTableReq);
    }, 0);

    const mainLineChartReq = {
      dimensions: ['time_key'],
      metrics: ['dp_revenue', 'dp_impressions'],
      derived_metrics: ['rev_cpm'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };

    this.loadMainLineChart(mainLineChartReq);
    const mainPieChartReq = {
      dimensions: ['source'],
      metrics: ['dp_revenue'],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [],
      limit: '',
      offset: ''
    };
    this.loadMainPieChart(mainPieChartReq);

    this.aggreTableReq = {
      dimensions: ['accounting_key'],
      metrics: [
        'dp_revenue',
        'dp_impressions',
        'dp_clicks',
        // 'rev_dfp_adserver_impressions',
        'dp_total_requests'
      ],
      derived_metrics: ['rev_cpm', 'fill_rate', 'ctr'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [],
      limit: '',
      offset: ''
    };

    this.aggTableJson['loading'] = true;
    this.loadAggTable(this.aggreTableReq);
  }

  getGrpBys() {
    let grpBys = this.filtersApplied['groupby'].map(e => e.key);

    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          grpBys.concat(
            this.filtersApplied['filters']['dimensions']
              .filter(f => f.values.length)
              .map(m => m.key)
          )
        )
      );
    }
    return grpBys;
  }

  openDialog(selectedTimkey, fieldName) {
    let tabHeading;
    let dialogData;

    if (fieldName === 'time_key') {
      const time_key1 = moment(selectedTimkey, 'YYYYMMDD')
        .startOf('month')
        .format('YYYYMMDD');
      const time_key2 = moment(selectedTimkey, 'YYYYMMDD')
        .endOf('month')
        .format('YYYYMMDD');

      dialogData = {
        time_key: {
          time_key1: time_key1,
          time_key2: time_key2
        },
        filterData: this.filtersApplied
      };
      tabHeading =
        ' ( Date Range : ' +
        moment(time_key1, 'YYYYMMDD').format('MM-DD-YYYY') +
        ' - ' +
        moment(time_key2, 'YYYYMMDD').format('MM-DD-YYYY') +
        ' )';
    } else {
      dialogData = {
        time_key: {
          time_key1: selectedTimkey,
          time_key2: selectedTimkey
        },
        filterData: this.filtersApplied
      };
      tabHeading =
        '( Day : ' +
        moment(selectedTimkey, 'YYYYMMDD').format('MM-DD-YYYY') +
        ' )';
    }

    this.dialogService.open(DummyRevmgmtCompComponent, {
      header: 'Sources Trend ' + tabHeading,
      width: '70%',
      data: dialogData,
      styleClass: 'ui-dialog-custom-class'
    });
  }

  showToast() {
    this.toastService.displayToast({
      severity: 'success',
      summary: 'Saple Toast',
      detail: 'Sample App Loaded'
    });
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['name']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    } else if (type === 'ppt') {
      const chartElements = document.getElementsByTagName('ym-charts');
      const slides = [];
      [].forEach.call(chartElements, (el, index) => {
        const c = el.getElementsByTagName('canvas')[0];

        slides.push({
          title: `${this.appConfig['name']}`,
          base64data: this.libServ.canvasToImage('white', c),
          width: 8,
          height: 3
        });

        if (index === chartElements.length - 1) {
          const pptConfig = {
            filename: 'Charts',
            logo: {
              visible: true,
              path: '/assets/images/ym.png',
              width: 0.53125,
              height: 0.458333
            },
            slides: slides
          };
          this.pptExport.exportPPT(pptConfig);
        }
      });
    }
    return false;
  }

  loadAggTable(timeKeyFilter) {
    const sourcesDatareq = {
      dimensions: ['source', 'accounting_key'], // derived_sales_type_name
      metrics: [
        'dp_revenue',
        'dp_impressions',
        'dp_clicks',
        // 'rev_dfp_adserver_impressions',
        'dp_total_requests'
      ],
      derived_metrics: ['rev_cpm', 'fill_rate', 'ctr'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'], // derived_sales_type_name
      orderBy: [],
      limit: '',
      offset: ''
    };

    const totalsRowreqSrc = {
      dimensions: ['source'],
      metrics: [
        'dp_revenue',
        'dp_impressions',
        'dp_clicks',
        // 'rev_dfp_adserver_impressions',
        'dp_total_requests'
      ],
      derived_metrics: ['rev_cpm', 'fill_rate', 'ctr'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [],
      limit: '',
      offset: ''
    };

    this.dataFetchServ.getTableData(timeKeyFilter).subscribe(data => {
      this.dataFetchServ.getTableData(sourcesDatareq).subscribe(data1 => {
        const totalsColData = data['data'];
        const srcWiseData = data1['data'];

        const srcs = Array.from(new Set(srcWiseData.map(s => s['source'])));
        this.addSources(srcs);
        this.jumpToSource['totalSources'] = [];
        srcs.forEach(e => {
          this.jumpToSource['totalSources'].push({
            label: e,
            value: e
          });
        });
        // this.jumpToSource['selected'] = [srcs[0]];

        const map = this.dataMapping(totalsColData, srcWiseData);
        // map.map(o => delete o['data']['time_key1']);
        map.forEach(el => {
          el['data']['time_key1'] = 'All';
        });
        this.aggTableData = map;

        this.dataFetchServ.getTableData(totalsRowreqSrc).subscribe(data1 => {
          const srcWiseData = data1['data'];

          srcWiseData.forEach(e => {
            const src = String(e['source'])
              .toLowerCase()
              .replace(/ /g, '_');
            this.aggTableColumnDef.forEach(c => {

              if (src + '_dp_revenue' === c['field']) {
                c['footerTotal'] = e['dp_revenue'];
              } else if (src + '_rev_cpm' === c['field']) {
                c['footerTotal'] = e['rev_cpm'];
              } else if (src + '_ctr' === c['field']) {
                c['footerTotal'] = e['ctr'];
              } else if (src + '_discrepancy' === c['field']) {
                if (src + '_discrepancy' === '33across_discrepancy') {
                }
                c['footerTotal'] = e['discrepancy'];
              } else if (src + '_rev_dfp_adserver_impressions' === c['field']) {
                c['footerTotal'] = e['rev_dfp_adserver_impressions'];
              } else if (src + '_dp_impressions' === c['field']) {
                c['footerTotal'] = e['dp_impressions'];
              } else if (src + '_dp_clicks' === c['field']) {
                c['footerTotal'] = e['dp_clicks'];
              } else if (src + '_fill_rate' === c['field']) {
                c['footerTotal'] = e['fill_rate'];
              }
            });
          });

          this.aggTableJson['frozenCols'][2]['footerTotal'] = this.cardsJson[0][
            'value'
          ];
          this.aggTableJson['frozenCols'][3]['footerTotal'] = this.cardsJson[2][
            'value'
          ];
          this.aggTableJson['frozenCols'][4]['footerTotal'] = this.cardsJson[3][
            'value'
          ];
          this.aggTableJson['totalRecords'] = map.length;
          this.aggTableJson['loading'] = false;
        });
      });
    });
  }

  dataMapping(totals: [], srcWise: []) {
    const arr = [];

    let tks = [];
    let timeKeyAcc = 'accounting_key';

    if (Array.from(new Set(totals.map(o => o['time_key'])))[0] === undefined) {
      tks = Array.from(new Set(totals.map(o => o['accounting_key'])));
    } else {
      tks = Array.from(new Set(totals.map(o => o['time_key'])));
      timeKeyAcc = 'time_key';
    }

    tks.forEach(t => {
      const obj = {
        data: {},
        children: [{ data: {} }]
      };
      totals.forEach(tot_obj => {
        if (tot_obj[timeKeyAcc] === t) {
          obj['data']['time_key'] = t;
          obj['data']['time_key1'] = t;
          obj['data']['source_revenue'] = tot_obj['dp_revenue'];
          obj['data']['source_rev_cpm'] = tot_obj['rev_cpm'];
          obj['data']['source_impressions'] = tot_obj['dp_impressions'];
        }
      });

      srcWise.forEach(s_obj => {
        if (s_obj[timeKeyAcc] === t) {
          const src = String(s_obj['source'])
            .toLowerCase()
            .replace(/ /g, '_');

          obj['data'][src + '_dp_revenue'] = s_obj['dp_revenue'];
          obj['data'][src + '_rev_cpm'] = s_obj['rev_cpm'];
          obj['data'][src + '_ctr'] = s_obj['ctr'];
          obj['data'][src + '_dp_clicks'] = s_obj['dp_clicks'];
          // obj['data'][src + '_rev_dfp_adserver_impressions'] =
          //   s_obj['rev_dfp_adserver_impressions'];
          // if (
          //   typeof s_obj['dfp_impressions'] !== 'undefined' &&
          //   s_obj['dfp_impressions'] > 0
          // ) {
          //   obj['data'][src + '_fill_rate'] =
          //     (s_obj['impressions'] * 100) / s_obj['dfp_impressions'];
          // } else {
          //   obj['data'][src + '_fill_rate'] =
          //     s_obj['request'] > 0
          //       ? (s_obj['impressions'] * 100) / s_obj['request']
          //       : s_obj['fill_rate'];
          // }
          // obj['data'][src + 'rev_dfp_adserver_impressions'] =
          //   s_obj['rev_dfp_adserver_impressions'];
          // obj['data'][src + '_dp_ctr'] = s_obj['dp_ctr'];
          obj['data'][src + '_dp_impressions'] = s_obj['dp_impressions'];
          obj['data'][src + '_dp_total_requests'] = s_obj['dp_total_requests'];
          obj['data'][src + '_fill_rate'] = s_obj['fill_rate'];
        }
      });
      arr.push(obj);
    });

    return <TreeNode[]>arr;
  }

  addSources(srcs) {
    this.aggTableColumnDef = this.libServ.deepCopy(
      this.aggTableColumnDef.slice(0, 5)
    );

    srcs.forEach((s: String) => {
      const src = s.toLowerCase().replace(/ /g, '_');

      if (this.sourcesJson.hasOwnProperty(s.toString())) {
        this.sourcesJson[s.toString()]['columns'].forEach(element => {
          if (this.sourcesJson[s.toString()]['defaultView']) {

            switch (element) {
              case src + '_dp_revenue':
                this.aggTableColumnDef.push({
                  field: src + '_dp_revenue',
                  displayName:
                    this.sourcesJson[s.toString()]['displayName'] + ' Revenue',
                  width: this.sourcesJson[s.toString()]['width'] + 10,
                  format: '$',
                  exportConfig: {
                    format: 'currency',
                    styleinfo: {
                      thead: 'default',
                      tdata: 'white'
                    }
                  },
                  style: { color: '#007ad9' },
                  formatConfig: [],
                  options: {
                    editable: false,
                    colSearch: false,
                    colSort: true,
                    resizable: true,
                    movable: true
                  },
                  footerTotal: 0
                });
                break;

              case src + '_rev_cpm':
                this.aggTableColumnDef.push({
                  field: src + '_rev_cpm',
                  displayName:
                    this.sourcesJson[s.toString()]['displayName'] + ' eCPM',
                  width: this.sourcesJson[s.toString()]['width'] + 10,
                  format: '$',
                  exportConfig: {
                    format: 'currency',
                    styleinfo: {
                      thead: 'default',
                      tdata: 'white'
                    }
                  },
                  formatConfig: [],
                  options: {
                    editable: false,
                    colSearch: false,
                    colSort: true,
                    resizable: true,
                    movable: true
                  },
                  footerTotal: 0
                });
                break;

              case src + '_dp_impressions':
                this.aggTableColumnDef.push({
                  field: src + '_dp_impressions',
                  displayName:
                    this.sourcesJson[s.toString()]['displayName'] +
                    ' Paid Impressions',
                  width: this.sourcesJson[s.toString()]['width'] + 30,
                  format: 'number',
                  formatConfig: [],
                  exportConfig: {
                    format: 'number',
                    styleinfo: {
                      thead: 'default',
                      tdata: 'white'
                    }
                  },
                  options: {
                    editable: false,
                    colSearch: false,
                    colSort: true,
                    resizable: true,
                    movable: true
                  },
                  footerTotal: 0
                });
                break;

              case src + '_dp_clicks':
                this.aggTableColumnDef.push({
                  field: src + '_dp_clicks',
                  displayName:
                    this.sourcesJson[s.toString()]['displayName'] + ' Clicks',
                  width: this.sourcesJson[s.toString()]['width'] + 10,
                  format: 'number',
                  formatConfig: [],
                  exportConfig: {
                    format: 'number',
                    styleinfo: {
                      thead: 'default',
                      tdata: 'white'
                    }
                  },
                  options: {
                    editable: false,
                    colSearch: false,
                    colSort: true,
                    resizable: true,
                    movable: true
                  },
                  footerTotal: 0
                });
                break;
              case src + '_ctr':
                this.aggTableColumnDef.push({
                  field: src + '_ctr',
                  displayName:
                    this.sourcesJson[s.toString()]['displayName'] + ' CTR',
                  width: this.sourcesJson[s.toString()]['width'] + 10,
                  format: 'percentage',
                  formatConfig: [2],
                  exportConfig: {
                    format: 'percentage',
                    styleinfo: {
                      thead: 'default',
                      tdata: 'white'
                    }
                  },
                  options: {
                    editable: false,
                    colSearch: false,
                    colSort: true,
                    resizable: true,
                    movable: true
                  },
                  footerTotal: 0
                });
                break;

              case src + '_fill_rate':
                this.aggTableColumnDef.push({
                  field: src + '_fill_rate',
                  displayName:
                    this.sourcesJson[s.toString()]['displayName'] +
                    ' Fill Rate',
                  width: this.sourcesJson[s.toString()]['width'] + 20,
                  format: 'percentage',
                  formatConfig: [2],
                  exportConfig: {
                    format: 'percentage',
                    styleinfo: {
                      thead: 'default',
                      tdata: 'white'
                    }
                  },
                  options: {
                    editable: false,
                    colSearch: false,
                    colSort: true,
                    resizable: true,
                    movable: true
                  },
                  footerTotal: 0
                });
                break;

              case src + '_rev_dfp_adserver_impressions':
                this.aggTableColumnDef.push({
                  field: src + '_rev_dfp_adserver_impressions',
                  displayName:
                    this.sourcesJson[s.toString()]['displayName'] +
                    ' DFP Impressions',
                  width: this.sourcesJson[s.toString()]['width'] + 30,
                  format: 'number',
                  formatConfig: [],
                  exportConfig: {
                    format: 'number',
                    styleinfo: {
                      thead: 'default',
                      tdata: 'white'
                    }
                  },
                  options: {
                    editable: false,
                    colSearch: false,
                    colSort: true,
                    resizable: true,
                    movable: true
                  },
                  footerTotal: 0
                });
                break;

              case src + '_discrepancy':
                this.aggTableColumnDef.push({
                  field: src + '_discrepancy',
                  displayName:
                    this.sourcesJson[s.toString()]['displayName'] +
                    ' Discrepancy',
                  width: this.sourcesJson[s.toString()]['width'] + 10,
                  format: 'percentage',
                  formatConfig: [2],
                  exportConfig: {
                    format: 'percentage',
                    styleinfo: {
                      thead: 'default',
                      tdata: 'white'
                    }
                  },
                  options: {
                    editable: false,
                    colSearch: false,
                    colSort: true,
                    resizable: true,
                    movable: true
                  },
                  footerTotal: 0
                });
                break;
            }
          }
        });
      }
    });

    this.aggTableJson['columns'] = this.aggTableColumnDef.slice(5);
    this.aggTableJson['selectedColumns'] = this.aggTableColumnDef.slice(5);
  }

  tabChanged(e) {
    if (e.index === 1 && this.aggTableData === undefined) {
      this.aggTableJson['loading'] = true;
      this.loadAggTable(this.aggreTableReq);
    }
  }

  exportTable(table, fileFormat) {
    if (this.exportRequest['sendEmail'].length > 0) {
      if (table === 'agg') {
        if (this.aggTableData.length == 0) {
          this.confirmationService.confirm({
            message:
              'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
            header: 'Information',
            icon: 'pi pi-exclamation-triangle',
            accept: () => { },
            reject: () => { }
          });
        } else {
          this.toastService.displayToast({
            severity: 'info',
            summary: 'Export Report',
            detail:
              'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
            life: 10000
          });

          const columnDefs = this.aggTableColumnDef;

          const totalsColreq = {
            dimensions: ['accounting_key'],
            metrics: ['dp_revenue', 'dp_impressions'],
            derived_metrics: ['rev_cpm'],
            timeKeyFilter: this.filtersApplied['timeKeyFilter'],
            filters: this.libServ.deepCopy(this.filtersApplied['filters']),
            groupByTimeKey: {
              key: ['accounting_key'],
              interval: 'daily'
            },
            gidGroupBy: ['source'],
            orderBy: [],
            limit: '',
            offset: '',
            isTable: true
          };

          const sourcesDatareq = {
            dimensions: ['source', 'accounting_key'],
            metrics: [
              'dp_revenue',
              'dp_impressions',
              'dp_clicks',
              'dfp_adserver_impressions',
              'dp_total_requests'
            ],
            derived_metrics: ['rev_cpm', 'fill_rate', 'ctr'],
            timeKeyFilter: this.filtersApplied['timeKeyFilter'],
            filters: this.libServ.deepCopy(this.filtersApplied['filters']),
            groupByTimeKey: {
              key: ['accounting_key'],
              interval: 'daily'
            },
            gidGroupBy: ['source'],
            orderBy: [],
            limit: '',
            offset: '',
            isTable: true
          };

          const totalsDailyColreq = {
            dimensions: ['time_key'],
            metrics: [
              'dp_revenue',
              'dp_impressions',
              'dp_clicks',
              'dfp_adserver_impressions',
              'dp_total_requests'
            ],
            derived_metrics: ['rev_cpm', 'fill_rate', 'ctr'],
            timeKeyFilter: this.filtersApplied['timeKeyFilter'],
            filters: this.libServ.deepCopy(this.filtersApplied['filters']),
            groupByTimeKey: {
              key: ['time_key', 'accounting_key'],
              interval: 'daily'
            },
            gidGroupBy: ['source'],
            orderBy: [],
            limit: '',
            offset: '',
            isTable: true
          };

          const sourcesDailyDatareq = {
            dimensions: ['source', 'time_key'],
            metrics: [
              'dp_revenue',
              'dp_impressions',
              'dp_clicks',
              'dfp_adserver_impressions',
              'dp_total_requests'
            ],
            derived_metrics: ['rev_cpm', 'fill_rate', 'ctr'],
            timeKeyFilter: this.filtersApplied['timeKeyFilter'],
            filters: this.libServ.deepCopy(this.filtersApplied['filters']),
            groupByTimeKey: {
              key: ['time_key', 'accounting_key'],
              interval: 'daily'
            },
            gidGroupBy: [
              'source'
            ],
            orderBy: [],
            limit: '',
            offset: '',
            isTable: true
          };

          this.dataFetchServ.getTableData(totalsColreq).subscribe(data => {
            this.dataFetchServ.getTableData(sourcesDatareq).subscribe(data1 => {
              const totalsColData = data['data'];
              const srcWiseData = data1['data'];

              let mapMonthly = this.dataMapping(totalsColData, srcWiseData);
              mapMonthly = mapMonthly.map(o => o['data']);
              this.dataFetchServ
                .getTableData(totalsDailyColreq)
                .subscribe(data => {
                  this.dataFetchServ
                    .getTableData(sourcesDailyDatareq)
                    .subscribe(data1 => {
                      const totalsDailyColData = data['data'];
                      const srcDailyWiseData = data1['data'];

                      let mapDaily = this.dataMapping(
                        totalsDailyColData,
                        srcDailyWiseData
                      );

                      mapDaily = mapDaily.map(o => o['data']);
                      const totalObj = {};
                      columnDefs.forEach(ele => {
                        if ('footerTotal' in ele) {
                          totalObj[ele.field] = ele.footerTotal;
                        }
                      });
                      totalObj[columnDefs[0].field] = 'Total';
                      totalObj['source_revenue'] = this.cardsJson[0]['value'];
                      totalObj['source_rev_cpm'] = this.cardsJson[2]['value'];
                      totalObj['source_impressions'] = this.cardsJson[3][
                        'value'
                      ];
                      mapMonthly.push(totalObj);
                      mapDaily.push(totalObj);
                      const sheetDetailsarray = [];
                      const sheetDetails = {};
                      sheetDetails['columnDef'] = this.libServ
                        .deepCopy(columnDefs)
                        .splice(0, 1)
                        .concat(columnDefs.slice(2));
                      sheetDetails['data'] = mapMonthly;
                      sheetDetails['sheetName'] = 'Monthly Data Distribution';
                      sheetDetails['isRequest'] = false;
                      sheetDetails['request'] = {
                        url: '',
                        method: '',
                        param: {
                          timeKeyFilter: this.libServ.deepCopy(
                            this.filtersApplied['timeKeyFilter']
                          )
                        }
                      };
                      sheetDetails['disclaimer'] = [
                        {
                          position: 'bottom',
                          label: 'Note: Data present in the table may vary over a period of time.',
                          color: '#000000'
                        }
                      ];
                      sheetDetails['totalFooter'] = {
                        available: true,
                        custom: false
                      };
                      sheetDetails['tableTitle'] = {
                        available: false,
                        label: 'Monthly Data Distribution'
                      };
                      sheetDetails['image'] = [
                        {
                          available: true,
                          path: environment.exportConfig.exportLogo,
                          position: 'top'
                        }
                      ];

                      const sheetlineDetails = {};
                      sheetlineDetails['columnDef'] = this.libServ
                        .deepCopy(columnDefs)
                        .slice(1);
                      sheetlineDetails['data'] = mapDaily;
                      sheetlineDetails['sheetName'] = 'Daily Data Distribution';
                      sheetlineDetails['isRequest'] = false;
                      sheetlineDetails['request'] = {
                        url: '',
                        method: '',
                        param: {}
                      };
                      sheetlineDetails['disclaimer'] = [
                        {
                          position: 'bottom',
                          label: 'Note: Data present in the table may vary over a period of time.',
                          color: '#000000'
                        }
                      ];
                      sheetlineDetails['totalFooter'] = {
                        available: true,
                        custom: false
                      };
                      sheetlineDetails['tableTitle'] = {
                        available: false,
                        label: 'Daily Data Distribution'
                      };
                      sheetlineDetails['image'] = [
                        {
                          available: true,
                          path: environment.exportConfig.exportLogo,
                          position: 'top'
                        }
                      ];

                      sheetDetailsarray.push(sheetDetails, sheetlineDetails);
                      this.exportRequest['sheetDetails'] = <SheetDetails[]>(
                        sheetDetailsarray
                      );
                      this.exportRequest['fileName'] =
                        'Programmatic Revenue Data ' +
                        moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
                      this.exportRequest['exportFormat'] = fileFormat;
                      this.exportRequest['exportConfig'] = environment.exportConfig;
                      // console.log('exportreport', this.exportRequest);
                      // return false;
                      this.dataFetchServ
                        .getExportReportData(this.exportRequest)
                        .subscribe(response => {
                          console.log(response);
                        });
                    });
                });
            });
          });
        }
      } else if (table === 'flat') {
        if (this.flatTableData.length == 0) {
          this.confirmationService.confirm({
            message:
              'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
            header: 'Information',
            icon: 'pi pi-exclamation-triangle',
            accept: () => { },
            reject: () => { }
          });
        } else {
          this.toastService.displayToast({
            severity: 'info',
            summary: 'Export Report',
            detail:
              'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
            life: 10000
          });
          const columnDefs = this.flatTableColumnDef;
          const req = this.libServ.deepCopy(this.flatTableReq);
          req['limit'] = '';
          req['offset'] = '';
          req['isTable'] = false;

          const sheetDetails = {};
          sheetDetails['columnDef'] = columnDefs;
          sheetDetails['data'] = [];
          sheetDetails['sheetName'] = 'Programmatic Daily Data';
          sheetDetails['isRequest'] = true;
          sheetDetails['request'] = {
            url: '/api/flask/revmgmt/cassandradata',
            method: 'POST',
            param: req
          };
          sheetDetails['disclaimer'] = [
            {
              position: 'bottom',
              label: 'Note: Data present in the table may vary over a period of time.',
              color: '#000000'
            }
          ];
          sheetDetails['totalFooter'] = {
            available: true,
            custom: true,
            notIncludeColumn: ['fill_rate']
          };
          sheetDetails['tableTitle'] = {
            available: false,
            label: 'Programmatic Daily Data'
          };
          sheetDetails['image'] = [
            {
              available: true,
              path: environment.exportConfig.exportLogo,
              position: 'top'
            }
          ];
          const sheetDetailsarray = [];
          sheetDetailsarray.push(sheetDetails);
          this.exportRequest['sheetDetails'] = <SheetDetails[]>(
            sheetDetailsarray
          );
          this.exportRequest['fileName'] =
            'Programmatic Daily Trends ' +
            moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
          this.exportRequest['exportFormat'] = fileFormat;
          this.exportRequest['exportConfig'] = environment.exportConfig;
          // console.log('exportreport', this.exportRequest);
          // return false;
          this.dataFetchServ
            .getExportReportData(this.exportRequest)
            .subscribe(response => {
              console.log(response);
            });
        }
      }
    } else {
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Export Report',
        detail:
          'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
        life: 10000
      });
    }
    return false;
  }

  colShowHide() {
    if (
      this.aggTableJson['columns'].length !==
      this.aggTableJson['selectedColumns'].length
    ) {
      this.showJumpTo = false;
    } else {
      this.showJumpTo = true;
    }
  }

  jumpTo(table) {
    const body = table.containerViewChild.nativeElement.getElementsByClassName(
      'ui-treetable-scrollable-body'
    )[1];
    const scrollWidth = this.aggTableJson['columns']
      .slice(
        0,
        this.aggTableJson['columns'].findIndex(e =>
          e.displayName.startsWith(this.jumpToSource['selected'])
        )
      )
      .reduce((tot: any, v: any) => tot + parseInt(v.width, 10), 0);

    body.scrollLeft = scrollWidth;
  }

  customSearch(value, fieldName, inputValue, tableColDef) {
    tableColDef[
      tableColDef.findIndex(x => x.field == fieldName)
    ].condition = this.conditionNames[
      this.conditionNames.findIndex(x => x.value == value)
    ].label;

    if (value != '') {
      this.onSearchChanged(tableColDef);
    }
  }

  onGlobalSearchChanged(searchValue, tableColDef) {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    this.timeout = setTimeout(() => {
      this.flatTableReq['filters']['globalSearch'] = {
        dimensions: [],
        value: searchValue
      };
      tableColDef.forEach(element => {
        this.flatTableReq['filters']['globalSearch']['dimensions'].push(
          element.field
        );
      });
      this.loadFlatTableData(this.flatTableReq);
    }, 3000);
  }

  onSearchChanged(tableColDef) {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    this.timeout = setTimeout(() => {
      this.flatTableReq['filters']['dimensions'].forEach(element => {
        if (element.key.split('<<').length > 0) {
          this.flatTableReq['filters']['dimensions'].splice(
            this.flatTableReq['filters']['dimensions'].findIndex(
              x => x.key === element.key
            ),
            1
          );
        }
      });

      this.flatTableReq['filters']['metrics'] = [];
      tableColDef.forEach(element => {
        if (element.value !== '' && element.value != null) {
          if (element.condition === 'Equal') {
            this.flatTableReq['filters'][element.columnType].push({
              key: element.field,
              values: [parseFloat(element.value)]
            });
          } else {
            this.flatTableReq['filters'][element.columnType].push({
              key: `${element.field}<<${element.condition}`,
              values:
                element.columnType === 'metrics'
                  ? [parseFloat(element.value)]
                  : [element.value.toString()]
            });
          }
        }
      });
      this.loadFlatTableData(this.flatTableReq);
    }, 3000);
  }

  resetSearch(fieldName, tableColDef) {
    console.log('reset', fieldName, tableColDef);
    tableColDef.find(o => o.field === fieldName).value = '';
    this.onSearchChanged(tableColDef);
  }

  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }

  sendEmail() {
    const imageConfig = {
      imgName: `${this.appConfig['name']}.png`,
      type: 'image/png'
    };

    this.htmltoimage
      .canvasImage(document.getElementById('capture'), imageConfig)
      .then(canvas => {
        this.dialogService.open(SendEmailComponent, {
          header: 'Email Screenshot',
          contentStyle: {
            'max-height': '80vh',
            width: '30vw',
            overflow: 'auto'
          },
          data: canvas.toDataURL('image/png')
        });
      });
  }
}

