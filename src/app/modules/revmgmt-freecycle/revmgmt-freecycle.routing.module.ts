import { Routes } from '@angular/router';

import { RevmgmtFreecycleComponent } from './revmgmt-freecycle.component';
export const RevmgmtFreecycleAppRoutes: Routes = [
  {
    path: '',
    component: RevmgmtFreecycleComponent
    // data: {
    //   breadcrumb: 'Yield Management'
    // }
  }
];
