import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdHocDashboardPopComponent } from './ad-hoc-dashboard-pop.component';

describe('AdHocDashboardPopComponent', () => {
  let component: AdHocDashboardPopComponent;
  let fixture: ComponentFixture<AdHocDashboardPopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AdHocDashboardPopComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdHocDashboardPopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
