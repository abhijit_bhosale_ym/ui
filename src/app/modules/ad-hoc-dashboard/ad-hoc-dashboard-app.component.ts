import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, from } from 'rxjs';
import { Title } from '@angular/platform-browser';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import * as moment from 'moment';

import { CommonLibService, AppConfigService } from 'src/app/_services';
import { FetchApiDataService } from './fetch-ad-hoc-dashboard-api-data.service';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { FilterDataService } from '../../_services/filter-data/filter-data.service'
import { AdHocDashboardPopComponent } from './ad-hoc-dashboard-pop/ad-hoc-dashboard-pop.component';
import { Router, ActivatedRoute } from '@angular/router';
import { ShareDashboardPopComponent } from './share-dashboard-pop/share-dashboard-pop.component';


@Component({
  selector: 'ym-ad-hoc-dashboard-app',
  templateUrl: './ad-hoc-dashboard-app.component.html',
  styleUrls: ['./ad-hoc-dashboard-app.component.scss'],
  providers: [DialogService]
})
export class AdHocDashboardAppComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;

  appConfig: object = {};
  lastUpdatedOn: Date;
  tableRequestParam = {};
  favoriteCollection = {};
  filtersApplied: object = {};
  allStatus: any;
  messages = [];
  unreadCount = 0;
  nextUpdated: Date;
  dataUpdatedThrough: Date;
  exportRequest: ExportRequest = <ExportRequest>{};

  flatTableReq: object;
  flatTableData: TreeNode[];
  flatTableColumnDef: any[];
  flatTableJson: Object;
  tblResData: any[];
  displayAggTable = true;
  noTableData = false;
  isDashboardCreate = false;

  constructor(
    private confirmationService: ConfirmationService,

    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private toastService: ToastService,
    private dialogService: DialogService,
    private formatNumPipe: FormatNumPipe,
    private dataFetchServ: FetchApiDataService,
    private exportService: ExportdataService,
    private currentActivatedRoute: ActivatedRoute,
    public router: Router
  ) {


  }

  ngOnInit() {

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {

      console.log('appconfig', appConfig);

      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        this._titleService.setTitle(this.appConfig['displayName']);


        this.filtersApplied = {
          timeKeyFilter: {
          },
          filters: { dimensions: [], metrics: [] },
          groupby: this.appConfig['filter']['filterConfig']['groupBy']
        };


        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        this.initialLoading();
      }
    });


    this.flatTableColumnDef = [
      {
        field: 'dashboard_title',
        displayName: 'Dashboard Title',
        format: '',
        width: '250',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'dashboard_description',
        displayName: 'Description',
        format: '',
        width: '260',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'edit',
        displayName: 'View',
        format: '',
        width: '50',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'delete',
        displayName: 'Delete',
        format: '',
        width: '55',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'share',
        displayName: 'Share',
        format: '',
        width: '55',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'type',
        displayName: 'Type',
        format: '',
        width: '60',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'name',
        displayName: 'Created By',
        format: '',
        width: '125',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      }, {
        field: 'insert_at',
        displayName: 'Creation Date',
        format: '',
        width: '160',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      }, {
        field: 'update_at',
        displayName: 'Last Updated',
        format: '',
        width: '160',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      }
    ];

    this.flatTableJson = {
      page_size: 10,
      page: 0,
      lazy: false,
      loading: false,
      export: false,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '450px',
      totalRecords: 0,
      columns: this.flatTableColumnDef.slice(1),
      selectedColumns: this.flatTableColumnDef.slice(1),
      globalFilterFields: this.flatTableColumnDef.slice(0),
      frozenCols: [this.flatTableColumnDef[0]],
      // frozenWidth: '250px',
      frozenWidth:
        this.flatTableColumnDef
          .slice(0, 1)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      // columns: this.flatTableColumnDef.slice(this.flatTableColumnDef.length-1),
      // selectedColumns: [this.flatTableColumnDef],
      // frozenCols: [this.flatTableColumnDef[0]],
      // frozenWidth:
      //   this.flatTableColumnDef
      //     .slice(0, 1)
      //     .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: true
    };
  }

  initialLoading() {
    this.dataFetchServ
      .getLastUpdatedData()
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        this.lastUpdatedOn = moment(data['data']).toDate();

      });

    if (localStorage.getItem('type')) {
      this.isDashboardCreate = true;
    } else {
      this.isDashboardCreate = false;
      this.getTableData();
    }
  }


  getTableData() {

    this.dataFetchServ.getTableData().subscribe(data => {
      this.dataFetchServ.getShareTableData().subscribe(shareData => {
        if (data['status'] === 0) {
          this.noTableData = true;
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        } else {
          this.noTableData = false;
        }
        const arr = [];
        const totalsColData = data['data'];
        const shareTableData = shareData['data'];
        for (const r of totalsColData) {
          const obj = {};
          r['type'] = 'Own';
          obj['data'] = r;
          arr.push(obj);
        }
        // this.tblResData = arr;
        for (const r of shareTableData) {
          const obj = {};
          r['type'] = 'Shared';
          obj['data'] = r;
          arr.push(obj);
        }


        this.flatTableData = <TreeNode[]>arr;
        this.flatTableJson['totalRecords'] = totalsColData.length;
        this.flatTableJson['loading'] = false;
      });
    });
  }



  reLoadTableData() {
    const tableReq = this.libServ.deepCopy(this.tableRequestParam);
    tableReq.filters = this.libServ.deepCopy(this.filtersApplied['filters']);
  }

  NewDashboard() {
    const data = {
      isEdit: false,
      isSingle: false
    };

    const ref = this.dialogService.open(AdHocDashboardPopComponent, {
      data: data,
      header: 'Visualize Selection',
      contentStyle: { width: '60vw', overflow: 'auto' },
    });

    ref.onClose.subscribe((data: boolean) => {
      console.log('data', data);
      this.isDashboardCreate = data;
    }
    );
  }

  exportTable(fileFormat) {
    const data = [];
    const finalColDef = this.libServ.deepCopy(this.flatTableColumnDef);

    const req = {
      'filters': this.filtersApplied['filters']['dimensions']
    };

    this.dataFetchServ
      .getTableData()
      .subscribe(response => {
        if (response['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          return;
        }
        data.push({
          data: [
            {
              data: response['data'],
              columnDefs: finalColDef
            }
          ],
          sheetname: 'RIO'
        });

        this.exportService.exportReport(
          data,
          `Received_IO_${moment(new Date()).format('MM-DD-YYYY HH:mm:ss')}`,
          fileFormat,
          false
        );

      });
  }

  onFiltersApplied(filterData: object) {
    this.filtersApplied['filters']['dimensions'] = [];
    for (const k in filterData['filter']['dimensions']) {
      if (filterData['filter']['dimensions'][k].length !== 0) {
        this.filtersApplied['filters']['dimensions'].push({
          key: k,
          values: filterData['filter']['dimensions'][k]
        });
      }
    }

    this.getTableData();
  }

  deleteDashboard(rowData) {
    if (rowData['type'] == 'Own') {
      const msg = 'Do you want to delete dashboard?';
      this.confirmationService.confirm({
        message: msg,
        header: rowData['dashboard_title'],
        accept: () => {
          this.dataFetchServ.deleteDashboard(rowData['id']).subscribe(data => {
            if (data['status']) {
              this.toastService.displayToast({
                severity: 'success',
                summary: 'Dashbard Delete',
                detail: 'Dashoard delete successfully'
              });
              this.getTableData();
            } else {
              this.toastService.displayToast({
                severity: 'error',
                summary: 'Server Error',
                detail: 'Please refresh the page'
              });

            }
          });

        },
        reject: () => {

        }
      });
    } else {

      const msg = 'Do you want to remove dashboard?';
      this.confirmationService.confirm({
        message: msg,
        header: rowData['dashboard_title'],
        accept: () => {
          this.dataFetchServ.removeDashboard(rowData['id']).subscribe(data => {
            if (data['status']) {
              this.toastService.displayToast({
                severity: 'success',
                summary: 'Dashbard Remove',
                detail: 'Dashoard remove successfully'
              });
              this.getTableData();
            } else {
              this.toastService.displayToast({
                severity: 'error',
                summary: 'Server Error',
                detail: 'Please refresh the page'
              });

            }
          });

        },
        reject: () => {

        }
      });

    }
  }
  shareDashboard(rowdata) {
    // const data = {
    //   isEdit: false,
    //   isSingle: false
    // };
    const data = this.libServ.deepCopy(rowdata);
    data['user_id'] = this.appConfig['user']['id'];
    const ref = this.dialogService.open(ShareDashboardPopComponent, {
      data: rowdata,
      header: 'Share User Selection',
      contentStyle: { width: '75vw', overflow: 'auto' },
    });

    ref.onClose.subscribe((data: boolean) => {
      console.log('data', data);
      // this.isDashboardCreate = data;
    }
    );
  }
  editDashboard(rowData) {
    if (rowData['type'] == 'Own') {
      localStorage.setItem('type', 'update');
      localStorage.setItem('visualizeIds', rowData.id);
      this.isDashboardCreate = true;
    } else {
      localStorage.setItem('type', 'view');
      localStorage.setItem('visualizeIds', rowData.id);
      this.isDashboardCreate = true;
    }
  }

  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
      if (localStorage.getItem('chartType') != 'updateInDashboard') {
        if (localStorage.getItem('type')) {
          localStorage.removeItem('type');
        }
        if (localStorage.getItem('visualizeIds')) {
          localStorage.removeItem('visualizeIds');
        }
      }
    }
  }
}
