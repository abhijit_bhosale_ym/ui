import { Routes } from '@angular/router';
import { AdHocDashboardAppComponent } from './ad-hoc-dashboard-app.component';
import { DashboardCreateComponent } from './dashboard-create/dashboard-create.component';

export const AdHocDashboardAppRoutes: Routes = [
  {
    path: '',
    component: AdHocDashboardAppComponent
  },
  {
    path: 'create-dashboard/:chartType/:keyspace',
    component: DashboardCreateComponent
  }
];
