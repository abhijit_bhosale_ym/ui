import { Component, OnInit, OnDestroy } from '@angular/core';
import { SelectItemGroup, ConfirmationService, SelectItem } from 'primeng/api';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { FetchApiDataService } from '../fetch-ad-hoc-dashboard-api-data.service';
import { CommonLibService } from 'src/app/_services';
import { DynamicDialogConfig, DynamicDialogRef, DialogService } from 'primeng/dynamicdialog';
@Component({
  selector: 'ym-share-dashboard-pop',
  templateUrl: './share-dashboard-pop.component.html',
  styleUrls: ['./share-dashboard-pop.component.scss']
})
export class ShareDashboardPopComponent implements OnInit {
  // private appConfigObs: Subscription;

  appConfig: object = {};
  iskeyspace = false;
  RowData: object = {};
  userList: SelectItem[];
  selectedUserList: any[] = [];
  sharedUserList: SelectItem[];
  selectedSharedUserList: any[] = [];
  constructor(
    private formBuilder: FormBuilder,
    public router: Router,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private confirmationService: ConfirmationService,
    private libServ: CommonLibService,
    private config: DynamicDialogConfig,
    private dynamicDialogRef: DynamicDialogRef,
    private currentActivatedRoute: ActivatedRoute,
  ) {
    this.RowData = this.config.data;
  }

  ngOnInit() {
    this.selectedUserList = [];
    this.selectedSharedUserList = [];
    this.getShareUserData();
    this.getSharedUserData();
  }

  back() {
    this.iskeyspace = false;
  }

  getShareUserData() {
    let req = {
      dashboard_id: this.RowData['id']
    }
    this.dataFetchServ.getShareUserData(req).subscribe(data => {
      let datalist = data['data'];
      this.userList = [];
      datalist.forEach(element => {
        this.userList.push({ label: element.name, value: element });
      });
    });
  }
  getSharedUserData() {
    let req = {
      dashboard_id: this.RowData['id']
    }
    this.dataFetchServ.getSharedUserData(req).subscribe(data => {
      let datalist = data['data'];
      this.sharedUserList = [];
      datalist.forEach(element => {
        this.sharedUserList.push({ label: element.name, value: element });
      });
    });
  }
  onSubmit() {

    let req1 = {
      dashboard_id: this.RowData['id'],
      role_ids: this.selectedUserList.map(x => x.role_id)
    }
    this.dataFetchServ.getRoleWiseGraphCount(req1).subscribe(roleData => {
      let roleWiseData = roleData['data'];
      let isPermmit = true;
      roleWiseData.forEach(element => {
        if (element.total_graph != element.permmit_graph) {
          isPermmit = false;
        }
      });
      let confirmationMsg = isPermmit ? 'Do you want to share dashboard?' : 'Do you want to share dashboard? &#013; Please note: While sharing the Dashboard, please check if the User has the permissions to View graphs. If user has no permissions then he will not be able to see the graphs.';
      this.confirmationService.confirm({
        message: confirmationMsg,
        header: 'Share Dashboard Comfirmation',
        accept: () => {
          let req = {
            dashboard_id: this.RowData['id'],
            share_from_user_id: this.RowData['user_id'],
            share_to_user_id: this.selectedUserList.map(function (x) {
              return { id: x.id, email: x.email, name: x.name };
            })
          }
          this.dataFetchServ.saveShareDashboard(req).subscribe(data => {
            if (data['status']) {
              this.toastService.displayToast({
                severity: 'success',
                summary: 'Dashboard Share',
                detail: 'Dashboard share successfully'
              });
              this.ngOnInit()
            } else {
              this.toastService.displayToast({
                severity: 'error',
                summary: 'Server Error',
                detail: 'Please refresh the page'
              });

            }
          });
        },
        reject: () => {

        }
      });
    });
  }

  shareRemove() {

    this.confirmationService.confirm({
      message: 'Do you want to remove dashboard?',
      header: 'Share Dashboard Comfirmation',
      accept: () => {
        let req = {
          dashboard_id: this.RowData['id'],
          remove_user_id: this.selectedSharedUserList.map(function (x) {
            return { id: x.id, email: x.email, name: x.name };
          })
        }
        this.dataFetchServ.removeShareDashboard(req).subscribe(data => {
          if (data['status']) {
            this.toastService.displayToast({
              severity: 'success',
              summary: 'Dashboard Remove',
              detail: 'Dashboard Remove successfully'
            });
            // this.dialogService.dialogComponentRef.destroy();
            this.ngOnInit();
          } else {
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Server Error',
              detail: 'Please refresh the page'
            });

          }
        });
      },
      reject: () => {

      }
    });

  }

  goBack() {
    // this.router.navigate(['/users']);
    this.dialogService.dialogComponentRef.destroy();
  }
  // ngOnDestroy() {
  //   if (this.appConfigObs && !this.appConfigObs.closed) {
  //     this.appConfigObs.unsubscribe();
  //   }
  // }
}
