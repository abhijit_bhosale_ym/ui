import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShareDashboardPopComponent } from './share-dashboard-pop.component';

describe('ShareDashboardPopComponent', () => {
  let component: ShareDashboardPopComponent;
  let fixture: ComponentFixture<ShareDashboardPopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ShareDashboardPopComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShareDashboardPopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
