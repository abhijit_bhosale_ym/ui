import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FetchApiDataService {
  private BASE_URL: string = environment.baseUrl;

  constructor(private http: HttpClient) { }
  getTableData() {
    const url = `${this.BASE_URL}/frankly/v1/common/getDashboardData`;
    return this.http.get(url);
  }

  getShareTableData() {
    const url = `${this.BASE_URL}/frankly/v1/common/getShareDashboardData`;
    return this.http.get(url);
  }

  getVisaulizeData() {
    const url = `${this.BASE_URL}/frankly/v1/common/getVisualizeData`;
    return this.http.get(url);
  }
  getKeyspaceData() {
    const url = `${this.BASE_URL}/frankly/v1/common/getKeyspaceData`;
    return this.http.get(url);
  }
  getKeyspaceById(keyspaceId: number) {
    return this.http.get(
      `${this.BASE_URL}/frankly/v1/visualize/get-keyspace-by-id/${keyspaceId}`
    );
  }
  getVisualizeByMultipleIds(req: object) {
    const url = `${this.BASE_URL}/frankly/v1/ad-hoc-dashboard/get-visualize-by-multiple-ids`;
    return this.http.post(url, req);
  }
  getVisualizeById(visualizeId: number) {
    return this.http.get(
      `${this.BASE_URL}/frankly/v1/visualize/get-visualize-by-id/${visualizeId}`
    );
  }
  getDashboardById(dashboardId: number) {
    return this.http.get(
      `${this.BASE_URL}/frankly/v1/ad-hoc-dashboard/get-dashboard-by-id/${dashboardId}`
    );
  }
  getChartData(params: object, api_url: string) {
    const url = `${this.BASE_URL}${api_url}`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
    // return <TreeNode[]> json.data;
  }

  saveDashboard(req) {
    const url = `${this.BASE_URL}/frankly/v1/ad-hoc-dashboard/saveDashboard`;
    return this.http.post(url, req);
  }
  updateDashboard(req) {
    const url = `${this.BASE_URL}/frankly/v1/ad-hoc-dashboard/updateDashboard`;
    return this.http.post(url, req);
  }
  updatevisualize(req) {
    const url = `${this.BASE_URL}/frankly/v1/visualize/updateVisualize`;
    return this.http.post(url, req);
  }
  deleteVisualize(id) {
    const url = `${this.BASE_URL}/frankly/v1/visualize/deleteVisualize/${id}`;
    return this.http.post(url, id);
  }

  deleteDashboard(id) {
    const url = `${this.BASE_URL}/frankly/v1/ad-hoc-dashboard/deleteDashboard/${id}`;
    return this.http.post(url, id);
  }

  removeDashboard(id) {
    const url = `${this.BASE_URL}/frankly/v1/ad-hoc-dashboard/removeDashboard/${id}`;
    return this.http.post(url, id);
  }


  getExportReportData(params: object,backend_url:string) {
    const url = `${this.BASE_URL}${backend_url}`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
    // return <TreeNode[]> json.data;
  }

  getExportData(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/common/exportData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
    // return <TreeNode[]> json.data;
  }

  getLastUpdatedData() {
    const url = `${this.BASE_URL}/frankly/v1/ad-hoc-dashboard/getDashboardLastUpdated`;
    return this.http.get(url);
  }
  getShareUserData(req) {
    const url = `${this.BASE_URL}/frankly/v1/ad-hoc-dashboard/getShareUserData`;
    return this.http.post(url, req);
  }

  getSharedUserData(req) {
    const url = `${this.BASE_URL}/frankly/v1/ad-hoc-dashboard/getSharedUserData`;
    return this.http.post(url, req);
  }

  saveShareDashboard(req) {
    const url = `${this.BASE_URL}/frankly/v1/ad-hoc-dashboard/saveShareDashboard`;
    return this.http.post(url, req);
  }

  getRoleWiseGraphCount(req) {
    const url = `${this.BASE_URL}/frankly/v1/ad-hoc-dashboard/getRoleWiseGraphCount`;
    return this.http.post(url, req);
  }

  removeShareDashboard(req) {
    const url = `${this.BASE_URL}/frankly/v1/ad-hoc-dashboard/removeShareDashboard`;
    return this.http.post(url, req);
  }

}
