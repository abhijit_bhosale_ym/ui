import { Component, OnInit, OnDestroy } from '@angular/core';
import { SelectItemGroup, ConfirmationService } from 'primeng/api';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { FetchApiDataService } from '../fetch-ad-hoc-dashboard-api-data.service';
import { CommonLibService, AppConfigService } from 'src/app/_services';
import { DialogService } from 'primeng/dynamicdialog';
import { Title } from '@angular/platform-browser';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import * as moment from 'moment';
import { Subscription } from 'rxjs';
import { DashboardDetailsComponent } from './dashboard-details/dashboard-details.component';
import { TabularDataPopupComponent } from './tabular-data/tabular-data-popup.component';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { CustomDateRangeComponent } from './custom-daterange/custom-daterange.component';
import { AdHocDashboardPopComponent } from '../ad-hoc-dashboard-pop/ad-hoc-dashboard-pop.component';
import { CustomPanelComponent } from './custom-panel/custom-panel.component';
import { Swappable } from '@shopify/draggable';
import { ListDashboardGraphComponent } from './list-dashboard-graph/list-dashboard-graph.component';
import { TreeNode } from 'primeng/api';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-dashboard-create',
  templateUrl: './dashboard-create.component.html',
  styleUrls: ['./dashboard-create.component.scss']
})


export class DashboardCreateComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;

  appConfig: object = {};
  chartJson: object = {};
  chartJsonMain: object;
  defualtChartJson: object;
  dashboardType: string;
  visualizeIds: any[];
  visualizeData: any[];
  filtersApplied: object = {};
  isEdit = false;
  isShare = false;
  isUpdate = false;
  title = '';
  showChart: object = {};
  noDataChart: object = {};
  description = '';
  visualizeId: number;
  defualtChartConfig: object = {};
  keySpaces: object = {};
  chatConfig: object = {};
  apiRequest: object = {};
  inBounds = true;
  position: object = { x: 0, y: 0 };
  positionA = { x: 0, y: 0 };
  isResize = true;
  isDrag = false;
  chartType: string;
  lastUpdatedOn: Date = new Date();
  edge = {
    top: true,
    bottom: true,
    left: true,
    right: true
  };
  chartTypeData = [{
    chart_type: 'bar',
    display_name: 'Vertical Bar',
    icon_class: 'fas fa-poll',
    info: 'A bar chart or bar graph is a chart that presents Grouped data with rectangular bars with lengths proportional to the values that they represent.'
  }, {
    chart_type: 'line',
    display_name: 'Line',
    icon_class: 'fas fa-chart-line',
    info: 'A line chart or line graph is a type of chart which displays information as a series of data points called \'markers\' connected by straight line segments.'
  },
  {
    chart_type: 'area',
    display_name: 'Area',
    icon_class: 'fas fa-chart-area',
    info: 'Emphasize the quantity beneath a line chart'
  },
  {
    chart_type: 'pie',
    display_name: 'Pie',
    icon_class: 'fas fa-chart-pie',
    info: 'A doughnut chart is a variant of the pie chart, with a blank center allowing for additional information about the data as a whole to be included.'
  },
  {
    chart_type: 'cards_panel',
    display_name: 'Cards Panel',
    icon_class: 'fas fa-id-card',
    info: 'Display a calculation as a single number'
  },
  {
    chart_type: 'polarArea',
    display_name: 'Polar Area',
    icon_class: 'fas fa-radiation-alt',
    info: 'Polar area charts are similar to pie charts, but each segment has the same angle - the radius of the segment differs depending on the value.'
  },
  {
    chart_type: 'radar',
    display_name: 'Radar',
    icon_class: 'fas fa-podcast',
    info: 'A radar chart is a graphical method of displaying multivariate data in the form of a two-dimensional chart of three or more quantitative variables represented on axes starting from the same point.'
  }, {
    chart_type: 'data_table',
    display_name: 'Data Table',
    icon_class: 'fas fa-table',
    info: 'Display value in table'
  }, {
    chart_type: 'horizontalBar',
    display_name: 'Horizontal Bar',
    icon_class: 'fas fa-poll-h',
    info: 'A bar chart or bar graph is a chart that presents Grouped data with rectangular bars with lengths proportional to the values that they represent.'
  }
  ];

  exportRequest: ExportRequest = <ExportRequest>{};
  constructor(
    private formBuilder: FormBuilder,
    public router: Router,
    private route: ActivatedRoute,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private confirmationService: ConfirmationService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private formatNumPipe: FormatNumPipe,
    private appConfigService: AppConfigService
  ) {

  }

  ngOnInit() {
    this.filtersApplied = {
      timeKeyFilter: {
        time_key1: moment().startOf('month').format('YYYYMMDD'),
        time_key2: moment().format('YYYYMMDD')
      },
      filters: { dimensions: [], metrics: [] },
    };

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        // this._titleService.setTitle('Dashboard');
        let startDate;
        let endDate;
        const date_config = this.appConfig['filter']['filterConfig']['filters'][
          'datePeriod'
        ][0];
        if ((date_config['defaultDate'][0]['value'] === 0 && date_config['defaultDate'][0]['period'] === 'month') && (moment().format('DD') === '01' || moment().format('DD') === '02')) {
          startDate = moment().subtract(1, 'months').startOf('month');
          endDate = moment().subtract(1, 'months').endOf('month');
        } else {
          if (
            date_config[
            'defaultDate'
            ][0]['startOf']
          ) {
            startDate = moment()
              .subtract(
                date_config['defaultDate'][0]['value'],
                date_config['defaultDate'][0]['period']
              )
              .startOf(
                date_config['defaultDate'][0]['period']
              );
          } else {
            startDate = moment().subtract(
              date_config['defaultDate'][0]['value'],
              date_config['defaultDate'][0]['period']
            );
          }
          endDate = moment().subtract(
            date_config[
            'defaultDate'
            ][1]['value'],
            date_config[
            'defaultDate'
            ][1]['period']
          );
        }
        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        // this.filtersApplied = {
        //   timeKeyFilter: {
        //     time_key1: startDate.format('YYYYMMDD'),
        //     time_key2: endDate.format('YYYYMMDD')
        //   },
        //   filters: { dimensions: [], metrics: [] },
        //   groupby: []
        // };


      }
    });



    this.initialLoading();
    const swappable = new Swappable(document.querySelectorAll('#container'), {
      draggable: '.draggable'
    });
    // swappable.on('swappable:start', () => console.log('swappable:start'));
    // swappable.on('swappable:swapped', (event) => console.log('swappable:swapped', event));
    swappable.on('swappable:swapped', (event) => {
      if (event.data) {
        const swappedData = event.data;
        let dragId = swappedData.dragEvent.source.id;
        let swapId = swappedData.swappedElement.id;
        console.log('swpppppp', dragId, swapId)
        if (dragId) {
          let dragIndex = dragId.split('_')[1];
          if (swapId) {

            let swapIndex = swapId.split('_')[1];
            if (dragIndex && swapIndex) {
              this.swap(dragIndex, swapIndex);
            }
          }
        }

      }
      console.log('swappable:swapped', event)
    });

  }
  adjustTableHeight(idx) {
    let dropdownElement = document.querySelector('#dragElement_' + idx + ' .dropdown')['offsetHeight'] ? document.querySelector('#dragElement_' + idx + ' .dropdown')['offsetHeight'] : 23
    let panelHeight = this.visualizeData[idx]['dashboard_config']['height'];
    let paginationHeight = document.querySelector('.dashboard-table_' + idx + ' .ui-paginator-bottom')['offsetHeight'];
    const tableTh: any = document.querySelectorAll('.dashboard-table_' + idx + ' .table-head');
    const tableTh1 = document.querySelector('.dashboard-table_' + idx + ' .ui-treetable-caption')['offsetHeight'];
    const scrollHeight1: any = document.querySelectorAll('body .dashboard-table_' + idx + ' .ui-treetable-scrollable-body');
    let thArray = [40];
    for (const th of tableTh) {
      thArray.push(th.offsetHeight);
    }
    console.log('dropdownElement + tableTh1 + paginationHeight', dropdownElement, tableTh1, paginationHeight)
    const maxheight = thArray.reduce((a, b) => Math.max(a, b));
    let extraHeight = dropdownElement + tableTh1 + paginationHeight + 25;
    if (this.chatConfig[idx]['table_setting']['setting']['columnSearch']) {
      extraHeight = extraHeight + 40;
    }
    this.chartJson[idx]['flatTableJson']['scrollHeight'] = (panelHeight - extraHeight - maxheight) + 'px !important';
    setTimeout(() => {
      for (const scrollHeight of scrollHeight1) {
        (scrollHeight as HTMLElement).style.cssText = 'max-height:' + (panelHeight - extraHeight - maxheight) + 'px !important';
      }
      for (const th of tableTh) {
        (th as HTMLElement).style.cssText = 'height:' + maxheight + 'px !important';
      }
    }, 0);
  }
  onColumnResize(idx) {
    this.adjustTableHeight(idx);
  }
  initialLoading() {
    // this.route.params.subscribe(res => (this.chartType = res.chartType));
    // this.route.params.subscribe(res => (this.keyspaceId = res.keyspace));

    this.visualizeIds = [];
    this.dashboardType = localStorage.getItem('type');
    this.visualizeIds = (localStorage.getItem('visualizeIds')).split(',');
    if (this.dashboardType == 'insert') {

      console.log('type.........', this.dashboardType, this.visualizeIds)
      if (this.visualizeIds.length > 0) {

        this.isUpdate = false;
        this.isEdit = true;
        let req = {
          visualizeId: this.visualizeIds
        }
        this.dataFetchServ.getVisualizeByMultipleIds(req).subscribe(data => {

          if (data['status'] === 0) {
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Server Error',
              detail: 'Please refresh the page'
            });
            console.log(data['status_msg']);
            return;
          }

          this.visualizeData = data['data'];
          this.visualizeData.forEach((element, i) => {
            // this.position[i] = { x: 0, y: 0 };
            element['visualize_id'] = element['id'];
            this.chatConfig[i] = JSON.parse(element['chart_config']);
            this.keySpaces[i] = {
              field_config: JSON.parse(element['field_config']),
              api_url: element['api_url'],
              export_url: element['export_url'],
              name: element['name'],
              backend_url:element['backend_url'],
              logo_url:element['logo_url'],
            };
            element['dashboard_config'] = {
              chartPosition: i,
              width: '48',
              height: '300',
              panel_title: {
                is_show: true,
                title: element.title
              },
              customize_daterange: {
                time_key1: '',
                time_key2: '',
                label: '',
                date_range: '',
                is_show: false
              }
            }
            element['dashboard_config']['customize_daterange'] = this.changesDate(this.chatConfig[i]['defualt_daterange'])
            console.log(element);
            element['chart_type'] = this.chartTypeData.find(x => x.display_name == element['chart_type']).chart_type;
            if (element['chart_type'] == 'pie' || element['chart_type'] == 'polarArea') {

              this.runPieChartQuery(i);

            } else if (element['chart_type'] == 'cards_panel') {
              this.runCardsPanelQuery(i);
            } else if (element['chart_type'] == 'radar') {
              this.runRadarChartQuery(i);
            } else if (element['chart_type'] == 'data_table') {
              this.runDataTableQuery(i);
            } else if (element['chart_type'] == 'horizontalBar') {
              this.runHorizontalBarQuery(i);
            } else {
              this.runChartQuery(i).then(res => {

              });
            }
          });
        });
      }
    } else {
      if (this.dashboardType == 'view') {
        this.isShare = true;
      }
      this.isUpdate = true;
      this.isEdit = false;
      this.dataFetchServ.getDashboardById(this.visualizeIds[0]).subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        this.lastUpdatedOn = moment(data['data'][0]['update_at'], 'x').toDate();
        this.visualizeData = data['data'];
        this.title = this.visualizeData[0]['dashboard_title'];
        this.description = this.visualizeData[0]['dashboard_description'];

        this.visualizeData.forEach((element, i) => {

          this.chatConfig[i] = JSON.parse(element['chart_config']);
          element['dashboard_config'] = JSON.parse(element['dashboard_config']);
          if (element['dashboard_config']['customize_daterange']['is_show'] && element['dashboard_config']['customize_daterange']['date_range'] != 'custom') {
            element['dashboard_config']['customize_daterange'] = this.changesDate(element['dashboard_config']['customize_daterange']['date_range']);
          }
          this.keySpaces[i] = {
            field_config: JSON.parse(element['field_config']),
            api_url: element['api_url'],
            export_url: element['export_url'],
            name: element['name'],
            backend_url:element['backend_url'],
            logo_url:element['logo_url'],
          };
          // element.dashboard_config = JSON.parse(element['dashboard_config']);
          // element['dashboard_config']['width'] = '600px';
          // element['dashboard_config']['height'] = '400px';
          // let rowindex = Math.round((i + 1) / 2) - 1;
          // console.log('row', rowindex)
          // if (i % 2 != 0) {
          //   element['dashboard_config']['top'] = (425 * rowindex).toString() + 'px';
          //   element['dashboard_config']['left'] = '51%';
          // } else {
          //   element['dashboard_config']['top'] = (425 * rowindex).toString() + 'px';
          //   element['dashboard_config']['left'] = '1%';
          // }
          element['chart_type'] = this.chartTypeData.find(x => x.display_name == element['chart_type']).chart_type;

          if (element['chart_type'] == 'pie' || element['chart_type'] == 'polarArea') {

            this.runPieChartQuery(i);

          } else if (element['chart_type'] == 'cards_panel') {
            this.runCardsPanelQuery(i);
          } else if (element['chart_type'] == 'radar') {
            this.runRadarChartQuery(i);
          } else if (element['chart_type'] == 'data_table') {
            this.runDataTableQuery(i);
          } else if (element['chart_type'] == 'horizontalBar') {
            this.runHorizontalBarQuery(i);
          } else {
            this.runChartQuery(i).then(res => {

            });
          }
        });
      });
    }


  }
  getWidth(width: number) {
    let containerWidth = document.getElementById('container').clientWidth;
    return 100 * width / (containerWidth - 18);
  }
  checkEdge(event) {
    this.edge = event;
    // console.log('edge:', event);
  }
  getfielddata(field, returntype, index) {
    if (field && this.keySpaces[index]['field_config']) {
      return this.keySpaces[index]['field_config'].find(x => x.field == field)[returntype];
    } else {
      return '';
    }
    // if (returntype == 'displayName') {
    //   return this.keySpaces['field_config'].find(x => x.field == field).displayName;
    // } else if (returntype == 'columnType') {
    //   return this.keySpaces['field_config'].find(x => x.field == field).columnType;
    // } else if (returntype == 'format') {
    //   return this.keySpaces['field_config'].find(x => x.field == field).format;
    // } else if (returntype == 'calculate_columns') {
    //   return this.keySpaces['field_config'].find(x => x.field == field).format;
    // }
  }

  checkMetricAggrigation(field, aggregation, index) {
    return (this.getfielddata(field, 'columnType', index) == 'derived_metrics' && aggregation == 'sum');
  }

  runChartQuery(idx) {
    return new Promise((resolve, reject) => {
      let colors = this.libServ.dynamicColors(10);
      console.log('chart', this.chatConfig[idx]);
      this.defualtChartJson = {
        chartTypes: [],
        chartData: {
          labels: [],
          datasets: []
        },
        chartOptions: {
          // responsive: true,
          maintainAspectRatio: false,
          title: {
            display: true,
            text: this.visualizeData[idx]['title']
          },
          legend: {
            display: true,
            position: 'top'
          },
          elements: {
            point: {
              display: false
            }
          },
          scales: {
            xAxes: [],
            yAxes: []
          },
          tooltips: {
            mode: this.chatConfig[idx]['data']['buckets'][0]['slice_data']['is_show'] ? 'nearest' : 'index',
            intersect: false,
            enabled: this.chatConfig[idx]['panel_setting']['settings']['show_tooltips'],
            callbacks: {
              label: (tooltipItem, data) => {
                const currentValue =
                  data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                return `${
                  data.datasets[tooltipItem.datasetIndex].label
                  } : ${this.formatNumPipe.transform(currentValue, data.datasets[tooltipItem.datasetIndex].format, [])}`;
              }
            }
          },
          plugins: {
            datalabels: false
          }
        },
        zoomLabel: false,
        chartWidth: '',
        chartHeight: this.visualizeData[idx]['dashboard_config']['height'] - 100 + 'px',
        calculate_value: this.chatConfig[idx]['panel_setting']['settings']['calculate_value'],
      }

      this.chartJson[idx] = this.libServ.deepCopy(this.defualtChartJson);
      this.chartJson[idx]['chartTypes'].push({ key: 'bar', label: 'Line Chart', stacked: false });
      this.chatConfig[idx]['data']['metrics'].forEach((element, i) => {
        if (element.isVisible) {
          if (element.chart_type == 'area') {
            this.chartJson[idx]['chartData']['datasets'].push({
              label: element.custom_label == '' ? this.getTitle(element, idx) : element.custom_label,
              type: 'line',
              format: this.getfielddata(element.field, 'format', idx),
              yAxisID: element.axis_value,
              borderColor: element.legend_color,
              fill: 'origin',
              backgroundColor: element.legend_color,
              data: [],
              steppedLine: element.show_line.line_mode == 'Stepped' ? true : false,
              lineTension: element.show_line.line_mode == 'Straight' ? 0 : undefined

            });
          } else {
            this.chartJson[idx]['chartData']['datasets'].push({
              label: element.custom_label == '' ? this.getTitle(element, idx) : element.custom_label,
              type: element.chart_type,
              yAxisID: element.axis_value,
              borderColor: element.legend_color,
              format: this.getfielddata(element.field, 'format', idx),
              fill: false,
              backgroundColor: element.legend_color,
              data: [],
              // borderDash: [2, 2],
              steppedLine: element.show_line.line_mode == 'Stepped' ? true : false,
              lineTension: element.show_line.line_mode == 'Straight' ? 0 : undefined,
              showLine: element.show_line.is_show,
              pointRadius: element.show_dot ? 2 : 0,
              borderWidth: element.show_line.line_width,
              trendlineLinear: (this.chatConfig[idx]['panel_setting']['threshold_line']['is_show']) ? {
                style: this.chatConfig[idx]['panel_setting']['threshold_line']['line_color'],
                lineStyle: this.chatConfig[idx]['panel_setting']['threshold_line']['line_style'],
                width: this.chatConfig[idx]['panel_setting']['threshold_line']['line_width']
              } : undefined,
              order: element.chart_type == 'line' ? 1 : 2
            });
          }
        }
      });

      const xAxes = this.chatConfig[idx]['data']['buckets'][0];
      this.chartJson[idx]['chartOptions']['scales']['xAxes'].push({
        type: 'category',
        display: true,
        scaleLabel: {
          display: xAxes.show_axis_lines_label.is_show,
          labelString: xAxes.custom_label == '' ? this.getTitle(xAxes, idx) : xAxes.custom_label,
        },
        gridLines: {
          display: this.chatConfig[idx]['panel_setting']['grid']['x_axis_lines']
        },
        position: xAxes.position,
        stacked: true,
        ticks: {
          // autoRotate: true,
          display: xAxes.show_axis_lines_label.show_label.is_show,
          minRotation: xAxes.show_axis_lines_label.show_label.align.minRotation,
          maxRotation: xAxes.show_axis_lines_label.show_label.align.maxRotation,
          callback: (value, index, values) => {
            // return (value / this.max * 100).toFixed(0) + '%';
            return value.toString().substr(0, xAxes.show_axis_lines_label.show_label.truncate);
          }
        }
      });
      this.chatConfig[idx]['data']['metrics'].filter(x => x.isVisible).forEach((element, i) => {

        this.chartJson[idx]['chartOptions']['scales']['yAxes'].push({
          gridLines: {
            display: this.chatConfig[idx]['panel_setting']['grid']['y_axis_lines'] == element.axis_value
          },
          id: element.axis_value,
          display: true,
          scaleLabel: {
            display: element.show_axis_lines_label.is_show,
            labelString: element.custom_label == '' ? this.getTitle(element, idx) : element.custom_label,
          },
          position: element.position,
          name: '1',
          stacked: element.mode == 'Stacked',
          format: this.getfielddata(element.field, 'format', idx),
          ticks: {
            minRotation: element.show_axis_lines_label.show_label.align.minRotation,
            maxRotation: element.show_axis_lines_label.show_label.align.maxRotation,
            min: element.custom_extend.axis_extend.min == '' ? undefined : element.custom_extend.axis_extend.min,
            max: element.custom_extend.axis_extend.max == '' ? undefined : element.custom_extend.axis_extend.max,
            stepSize: (element.custom_extend.scale_to_bound.is_show && element.custom_extend.scale_to_bound.range != '') ? element.custom_extend.scale_to_bound.range : undefined,
            display: element.show_axis_lines_label.show_label.is_show,
            callback: (value, index, values) => {
              // return (value / this.max * 100).toFixed(0) + '%';
              return this.formatNumPipe.transform(value, this.chartJson[idx]['chartOptions']['scales']['yAxes'][i].format, []).substr(0, element.show_axis_lines_label.show_label.truncate + 1);
            }
          }
          // scaleFontColor: "rgba(151,137,200,0.8)"
        });
      });
      this.chartJson[idx]['chartOptions']['legend']['position'] = this.chatConfig[idx]['panel_setting']['settings']['legend_position'];
      // console.log(this.chartJson[idx]['chartOptions']['legend']['position'], this.chatConfig[idx]['panel_setting']['settings']['legend_position'])
      let dimensions = [];
      let metrics = [];
      let timekey = [];
      let groupbBy = [];
      let orderBy = [];
      let derived_metrics = [];
      let timeKeyFilter = {};
      let dashboardMetrics = {};
      let bucketData = [];
      let graphOrderBy = [];
      bucketData.push(this.chatConfig[idx]['data']['buckets'][0]);
      if (this.chatConfig[idx]['data']['buckets'][0]['slice_data']['is_show']) {
        bucketData.push(this.chatConfig[idx]['data']['buckets'][0]['slice_data']);
      }
      bucketData.forEach(bucket => {
        dimensions.push(bucket.field);
        if (bucket.field != 'time_key' && bucket.field != 'accounting_key') {
          groupbBy.push(bucket.field);
          if (bucket.order_by.key == 'Custom Metrics') {
            orderBy.push({ key: bucket.order_by.field, opcode: bucket.order_by.mode });
            if (dashboardMetrics[bucket.order_by.field] == undefined) {
              dashboardMetrics[bucket.order_by.field] = [];
            }
            dashboardMetrics[bucket.order_by.field].push('sum');
            if (this.getfielddata(bucket.order_by.field, 'columnType', idx) == 'derived_metrics') {
              derived_metrics.push(bucket.order_by.field);
              let calculate_columns = [];
              calculate_columns = this.getfielddata(bucket.order_by.field, 'calculate_columns', idx);
              metrics = metrics.concat(calculate_columns);
              console.log(derived_metrics, this.getfielddata(bucket.order_by.field, 'calculate_columns', idx));
            } else {
              if (!metrics.some(x => x == bucket.order_by.field)) {
                metrics.push(bucket.order_by.field);
              }
            }
          } else if (bucket.order_by.key == 'Alphabetical') {
            orderBy.push({ key: bucket.field, opcode: bucket.order_by.mode });
          } else {
            if (this.checkMetricAggrigation(this.chatConfig[idx]['data']['metrics'][bucket.order_by.key]['field'], this.chatConfig[idx]['data']['metrics'][bucket.order_by.key]['aggregation'], idx)) {
              orderBy.push({
                key: this.chatConfig[idx]['data']['metrics'][bucket.order_by.key]['field'], opcode: bucket.order_by.mode
              });
            } else {
              orderBy.push({
                key: this.chatConfig[idx]['data']['metrics'][bucket.order_by.key]['field'] + '_'
                  + this.chatConfig[idx]['data']['metrics'][bucket.order_by.key]['aggregation'], opcode: bucket.order_by.mode
              });
            }
          }
        } else {
          timekey.push(bucket.field);

          orderBy.push({ key: bucket.field, opcode: 'asc' })

        }
      });
      this.chatConfig[idx]['data']['metrics'].forEach(metric => {
        if (metric.isVisible) {
          if (dashboardMetrics[metric.field] == undefined) {
            dashboardMetrics[metric.field] = [];
          }
          dashboardMetrics[metric.field].push(metric.aggregation.toLowerCase());
          if (this.getfielddata(metric.field, 'columnType', idx) == 'derived_metrics') {
            derived_metrics.push(metric.field);
            let calculate_columns = [];
            calculate_columns = this.getfielddata(metric.field, 'calculate_columns', idx);
            metrics = metrics.concat(calculate_columns);
            console.log(derived_metrics, this.getfielddata(metric.field, 'calculate_columns', idx));
          } else {
            if (!metrics.some(x => x == metric.field)) {
              metrics.push(metric.field);
            }

          }

        }
      });

      if (this.chatConfig[idx]['data']['buckets'][0]['slice_data']['is_show']) {

        graphOrderBy.push({
          colmun_Name: this.chatConfig[idx]['data']['buckets'][0]['field'],
          order: orderBy[0]['opcode'],
          limit: this.chatConfig[idx]['data']['buckets'][0]['order_by']['size'] == null ? '' : this.chatConfig[idx]['data']['buckets'][0]['order_by']['size'],
          sort_column: orderBy[0]['key']
        }, {
          colmun_Name: this.chatConfig[idx]['data']['buckets'][0]['slice_data']['field'],
          order: orderBy[1]['opcode'],
          limit: this.chatConfig[idx]['data']['buckets'][0]['slice_data']['order_by']['size'] == null ? '' : this.chatConfig[idx]['data']['buckets'][0]['slice_data']['order_by']['size'],
          sort_column: orderBy[1]['key']
        })
        orderBy = [];
      }

      if (this.visualizeData[idx]['dashboard_config']['customize_daterange']['is_show']) {
        timeKeyFilter = {
          time_key1: this.visualizeData[idx]['dashboard_config']['customize_daterange']['time_key1'],
          time_key2: this.visualizeData[idx]['dashboard_config']['customize_daterange']['time_key2']
        };
      } else {
        timeKeyFilter = this.filtersApplied['timeKeyFilter'];
      }
      this.apiRequest = {
        dimensions: Array.from(new Set(dimensions.concat(['time_key']))),
        metrics: Array.from(new Set(metrics)),
        derived_metrics: Array.from(new Set(derived_metrics)),
        timeKeyFilter: timeKeyFilter,
        filters: { dimensions: [], metrics: [] },
        groupByTimeKey: {
          key: Array.from(new Set(timekey.concat(['time_key']))),
          interval: 'daily'
        },
        dashboardDetails: {

          isDashboard: 'True',
          dimensions: Array.from(new Set(dimensions)),

          metrics: [dashboardMetrics]
        },
        gidGroupBy: groupbBy.length ? Array.from(new Set(groupbBy)) : ['source'],
        orderBy: orderBy,
        limit: this.chatConfig[idx]['data']['buckets'][0]['order_by']['size'],
        offset: '0'
      };
      if (graphOrderBy.length > 0) {
        this.apiRequest['dashboardDetails']['graphOrderBy'] = graphOrderBy;
      }
      this.showChart[idx] = false;
      this.noDataChart[idx] = false;
      this.dataFetchServ.getChartData(this.apiRequest, this.keySpaces[idx]['api_url']).subscribe(data => {
        const chartData = data['data'];
        this.noDataChart[idx] = false;
        if (!chartData.length) {
          this.noDataChart[idx] = true;
          return;
        }
        const labelArr = Array.from(new Set(chartData.map(r => r[this.chatConfig[idx]['data']['buckets'][0]['field']])));
        if (this.chatConfig[idx]['data']['buckets'][0]['field'] == 'time_key') {
          this.chartJson[idx]['chartData']['labels'] = labelArr.map(d =>
            moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
          );
        } else if (this.chatConfig[idx]['data']['buckets'][0]['field'] == 'accounting_key') {
          this.chartJson[idx]['chartData']['labels'] = labelArr.map(d =>
            moment(d, 'YYYYMMDD').format('MMM-YYYY')
          );
        } else {
          this.chartJson[idx]['chartData']['labels'] = labelArr;

        }
        if (this.chatConfig[idx]['data']['buckets'][0]['slice_data']['is_show']) {
          // this.chartJson[idx]['chartData']['datasets'] = [];

          if (this.chatConfig[idx]['data']['buckets'][0]['slice_data']['isStacked']) {
            this.chartJson[idx]['chartTypes'][0]['key'] = 'bar';
            this.chartJson[idx]['chartTypes'][0]['stacked'] = true;
          }

          if (this.chatConfig[idx]['data']['metrics'].some(x => x.isVisible && !x.isSlice)) {
            let sliceindex = this.chatConfig[idx]['data']['metrics'].findIndex(x => x.isVisible && x.isSlice);
            this.chartJson[idx]['chartData']['datasets'].splice(sliceindex, 1);
            this.chartJson[idx]['chartData']['datasets'][0]['borderColor'] = colors[0];
            this.chartJson[idx]['chartData']['datasets'][0]['backgroundColor'] = colors[0];

            let dimensions = [];
            let metrics = [];
            let timekey = [];
            let groupbBy = [];
            let orderBy = [];
            let derived_metrics = [];
            let dashboardMetrics = {};
            let bucketData = [];
            bucketData.push(this.chatConfig[idx]['data']['buckets'][0]);
            bucketData.forEach(bucket => {
              console.log('order', bucket.order_by.key);
              dimensions.push(bucket.field);
              if (bucket.field != 'time_key' && bucket.field != 'accounting_key') {
                groupbBy.push(bucket.field);
                if (bucket.order_by.key == 'Custom Metrics') {
                  orderBy.push({ key: bucket.order_by.field, opcode: bucket.order_by.mode });
                  if (dashboardMetrics[bucket.order_by.field] == undefined) {
                    dashboardMetrics[bucket.order_by.field] = [];
                  }
                  dashboardMetrics[bucket.order_by.field].push('sum');
                  if (this.getfielddata(bucket.order_by.field, 'columnType', idx) == 'derived_metrics') {
                    derived_metrics.push(bucket.order_by.field);
                    let calculate_columns = [];
                    calculate_columns = this.getfielddata(bucket.order_by.field, 'calculate_columns', idx);
                    metrics = metrics.concat(calculate_columns);
                    console.log(derived_metrics, this.getfielddata(bucket.order_by.field, 'calculate_columns', idx));
                  } else {
                    if (!metrics.some(x => x == bucket.order_by.field)) {
                      metrics.push(bucket.order_by.field);
                    }
                  }
                } else if (bucket.order_by.key == 'Alphabetical') {
                  orderBy.push({ key: bucket.field, opcode: bucket.order_by.mode });
                } else {
                  if (this.checkMetricAggrigation(this.chatConfig[idx]['data']['metrics'][bucket.order_by.key]['field'], this.chatConfig[idx]['data']['metrics'][bucket.order_by.key]['aggregation'], idx)) {
                    orderBy.push({
                      key: this.chatConfig[idx]['data']['metrics'][bucket.order_by.key]['field'], opcode: bucket.order_by.mode
                    });
                  } else {
                    orderBy.push({
                      key: this.chatConfig[idx]['data']['metrics'][bucket.order_by.key]['field'] + '_'
                        + this.chatConfig[idx]['data']['metrics'][bucket.order_by.key]['aggregation'], opcode: bucket.order_by.mode
                    });
                  }
                }
              } else {
                timekey.push(bucket.field);
                orderBy.push({ key: bucket.field, opcode: 'asc' })
              }
            });
            this.chatConfig[idx]['data']['metrics'].forEach(metric => {
              if (metric.isVisible) {
                if (dashboardMetrics[metric.field] == undefined) {
                  dashboardMetrics[metric.field] = [];
                }
                dashboardMetrics[metric.field].push(metric.aggregation.toLowerCase());
                if (this.getfielddata(metric.field, 'columnType', idx) == 'derived_metrics') {
                  derived_metrics.push(metric.field);
                  let calculate_columns = [];
                  calculate_columns = this.getfielddata(metric.field, 'calculate_columns', idx);
                  metrics = metrics.concat(calculate_columns);
                  console.log(derived_metrics, this.getfielddata(metric.field, 'calculate_columns', idx));
                } else {
                  if (!metrics.some(x => x == metric.field)) {
                    metrics.push(metric.field);
                  }
                }
              }
            });

            this.apiRequest = {
              dimensions: Array.from(new Set(dimensions.concat(['time_key']))),
              metrics: Array.from(new Set(metrics)),
              derived_metrics: Array.from(new Set(derived_metrics)),
              timeKeyFilter: timeKeyFilter,
              filters: { dimensions: [], metrics: [] },
              groupByTimeKey: {
                key: Array.from(new Set(timekey.concat(['time_key']))),
                interval: 'daily'
              },
              dashboardDetails: {

                isDashboard: 'True',
                dimensions: Array.from(new Set(dimensions)),

                metrics: [dashboardMetrics]
              },
              gidGroupBy: groupbBy.length ? Array.from(new Set(groupbBy)) : ['source'],
              orderBy: orderBy,
              limit: this.chatConfig[idx]['data']['buckets'][0]['order_by']['size'],
              offset: '0'
            };

            this.dataFetchServ.getChartData(this.apiRequest, this.keySpaces[idx]['api_url']).subscribe(data1 => {
              const chartData1 = data1['data'];
              let dataset = [];
              labelArr.forEach(label => {
                chartData1.forEach(r => {
                  if (r[this.chatConfig[idx]['data']['buckets'][0]['field']] === label) {
                    this.chatConfig[idx]['data']['metrics'].forEach(element => {
                      if (this.checkMetricAggrigation(element.field, element.aggregation, idx)) {
                        dataset.push(r[element.field]);
                      } else {
                        dataset.push(r[element.field + '_' + element.aggregation]);
                      }
                    });
                  }
                });
              });
              this.chartJson[idx]['chartData']['datasets'][0]['data'] = dataset;
              this.showChart[idx] = false;
              setTimeout(() => {
                this.showChart[idx] = true;
              }, 0);

            });
          } else {
            this.chartJson[idx]['chartData']['datasets'] = [];
          }
          const splitLabelArr = Array.from(new Set(chartData.map(r => r[this.chatConfig[idx]['data']['buckets'][0]['slice_data']['field']])));
          colors = this.libServ.dynamicColors(splitLabelArr.length);
          splitLabelArr.forEach((splitLabel, i) => {
            let labelName = splitLabel;
            if (this.chatConfig[idx]['data']['buckets'][0]['slice_data']['field'] == 'time_key') {

              labelName = moment(splitLabel, 'YYYYMMDD').format('MM-DD-YYYY');
            } else if (this.chatConfig[idx]['data']['buckets'][0]['slice_data']['field'] == 'accounting_key') {
              labelName = moment(splitLabel, 'YYYYMMDD').format('MM-YYYY');
            } else {
              labelName = splitLabel;

            }
            const dataSets = {};
            this.chatConfig[idx]['data']['metrics'].forEach(element => {
              if (element.isVisible && element.isSlice) {
                if (this.checkMetricAggrigation(element.field, element.aggregation, idx)) {
                  dataSets[element.field] = [];
                } else {
                  dataSets[element.field + '_' + element.aggregation] = [];
                }
              }
            });
            labelArr.forEach(label => {
              let isAvailable = true;
              chartData.forEach(r => {
                if (r[this.chatConfig[idx]['data']['buckets'][0]['field']] === label &&
                  r[this.chatConfig[idx]['data']['buckets'][0]['slice_data']['field']] === splitLabel) {
                  isAvailable = false;
                  this.chatConfig[idx]['data']['metrics'].forEach(element => {
                    if (element.isVisible && element.isSlice) {
                      if (this.checkMetricAggrigation(element.field, element.aggregation, idx)) {
                        dataSets[element.field].push(r[element.field]);
                      } else {
                        dataSets[element.field + '_' + element.aggregation].push(r[element.field + '_' + element.aggregation]);
                      }
                    }
                  });
                }
              });
              if (isAvailable) {

                this.chatConfig[idx]['data']['metrics'].forEach(element => {
                  if (element.isVisible && element.isSlice) {
                    if (this.checkMetricAggrigation(element.field, element.aggregation, idx)) {
                      dataSets[element.field].push(null);
                    } else {
                      dataSets[element.field + '_' + element.aggregation].push(null);
                    }
                  }
                });

              }
            });
            this.chatConfig[idx]['data']['metrics'].forEach((element) => {
              if (element.isVisible && element.isSlice) {
                if (element.chart_type == 'area') {
                  this.chartJson[idx]['chartData']['datasets'].push({
                    label: labelName,
                    type: 'line',
                    format: this.getfielddata(element.field, 'format', idx),
                    yAxisID: element.axis_value,
                    borderColor: colors[i + 1],
                    fill: 'origin',
                    backgroundColor: colors[i + 1],
                    data: (this.checkMetricAggrigation(element.field, element.aggregation, idx)) ?
                      dataSets[element.field]
                      : dataSets[element.field + '_' + element.aggregation],
                    steppedLine: element.show_line.line_mode == 'Stepped' ? true : false,
                    lineTension: element.show_line.line_mode == 'Straight' ? 0 : undefined

                  });
                } else {
                  this.chartJson[idx]['chartData']['datasets'].push({
                    label: labelName,
                    type: element.chart_type,
                    yAxisID: element.axis_value,
                    borderColor: colors[i + 1],
                    format: this.getfielddata(element.field, 'format', idx),
                    fill: false,
                    backgroundColor: colors[i + 1],
                    data: (this.checkMetricAggrigation(element.field, element.aggregation, idx)) ?
                      dataSets[element.field]
                      : dataSets[element.field + '_' + element.aggregation],
                    steppedLine: element.show_line.line_mode == 'Stepped' ? true : false,
                    lineTension: element.show_line.line_mode == 'Straight' ? 0 : undefined,
                    showLine: element.show_line.is_show,
                    pointRadius: element.show_dot ? 2 : 0,
                    borderWidth: element.show_line.line_width,
                    order: element.chart_type == 'line' ? 1 : 2
                  });
                }
              }
            });
          })
        } else {
          const dataSets = {};
          this.chatConfig[idx]['data']['metrics'].forEach(element => {
            if (element.isVisible) {
              if (this.checkMetricAggrigation(element.field, element.aggregation, idx)) {
                dataSets[element.field] = [];
              } else {
                dataSets[element.field + '_' + element.aggregation] = [];
              }
            }
          });
          labelArr.forEach(label => {
            chartData.forEach(r => {
              if (r[this.chatConfig[idx]['data']['buckets'][0]['field']] === label) {
                this.chatConfig[idx]['data']['metrics'].forEach(element => {
                  if (element.isVisible) {
                    if (this.checkMetricAggrigation(element.field, element.aggregation, idx)) {
                      dataSets[element.field].push(r[element.field]);
                    } else {
                      dataSets[element.field + '_' + element.aggregation].push(r[element.field + '_' + element.aggregation]);
                    }
                  }
                });

              }
            });
          });
          this.chatConfig[idx]['data']['metrics'].filter(x => x.isVisible).forEach((element, i) => {
            if (element.isVisible) {
              if (this.checkMetricAggrigation(element.field, element.aggregation, idx)) {
                this.chartJson[idx]['chartData']['datasets'][i]['data'] = dataSets[element.field];
              } else {
                this.chartJson[idx]['chartData']['datasets'][i]['data'] = dataSets[element.field + '_' + element.aggregation];
              }
            }
          });
        }
        this.showChart[idx] = true;
        this.positionA = { x: 0, y: 0 };
      });
    });
  }

  runPieChartQuery(idx) {
    return new Promise((resolve, reject) => {
      let colors = this.libServ.dynamicColors(10);
      console.log('chart', this.chatConfig);
      let that = this;
      this.defualtChartJson = {
        chartTypes: [],
        chartData: {
          labels: [],
          datasets: []
        },
        chartOptions: {
          title: {
            display: true,
            text: this.visualizeData[idx]['title']
          },
          legend: {
            display: true,
            position: 'top',

          },
          elements: {
            point: {
              display: false
            }
          },
          tooltips: {
            intersect: false,
            enabled: this.chatConfig[idx]['panel_setting']['pie_settings']['show_tooltips'],
            callbacks: {
              label: (tooltipItem, data) => {
                const dataset = data.datasets[tooltipItem.datasetIndex];
                if (dataset['_meta'][Object.keys(dataset._meta)[0]]['data'][tooltipItem.index]['hidden']) {
                  return;
                }
                const currentValue =
                  data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                let sum = 0;
                const dataArr = data.datasets[0].data;
                dataArr.forEach((data, index) => {
                  if (!dataset['_meta'][Object.keys(dataset._meta)[0]]['data'][index]['hidden']) {
                    sum += data;
                  }
                });
                const percentage = (currentValue * 100) / sum;
                return `${
                  data.labels[tooltipItem.index]
                  } : ${this.formatNumPipe.transform(
                    currentValue,
                    data.datasets[tooltipItem.datasetIndex].format,
                    []
                  )} - ${percentage.toFixed(2)} %`;
              }
            }
          },
          plugins: {
            // labels: {
            //   render: function (args) {
            //     console.log('args', args)
            //     return '$' + args.value;
            //   },
            //   precision: 2
            // },
            labels: [
              {
                render: (args) => {
                  return args.percentage > 10 && this.chatConfig[idx]['panel_setting']['label_settings']['show_labels'] ? args.label : '';
                },
                position: 'outside',
                fontStyle: 'bold',
                fontColor: '#000',

              },
              {
                render: (args) => {

                  return args.percentage > 10 && this.chatConfig[idx]['panel_setting']['label_settings']['show_values'] ? args.percentage + '%' : '';
                },
                fontStyle: 'bold',
                fontColor: '#000',
                precision: 2
              }
            ],
            datalabels: false,
            // datalabels: {
            //   formatter: (value, ctx) => {
            //     let sum = 0;
            //     const dataArr = ctx.chart.data.datasets[0].data;
            //     dataArr.map(data => {
            //       sum += data;
            //     });
            //     const percentage = (value * 100) / sum;
            //     if (percentage > 10) {
            //       return `${percentage.toFixed(2)} %`;
            //       // ${this.formatNumPipe.transform(
            //       //   value,
            //       //   '$',
            //       //   []
            //       // )} - ${percentage.toFixed(2)} %`;
            //     } else {
            //       return '';
            //     }
            //   },
            //   color: 'black'
            // }
          }
        },
        zoomLabel: false,
        chartWidth: '',
        chartHeight: this.visualizeData[idx]['dashboard_config']['height'] - 100 + 'px'
      }

      this.chartJson[idx] = this.libServ.deepCopy(this.defualtChartJson);
      this.chartJson[idx]['chartTypes'].push({ key: this.chatConfig[idx]['panel_setting']['pie_settings']['donut'] ? 'doughnut' : this.visualizeData[idx]['chart_type'], label: 'Pie Chart', stacked: false });
      this.chartJson[idx]['chartOptions']['legend']['position'] = this.chatConfig[idx]['panel_setting']['pie_settings']['legend_position'];
      this.chatConfig[idx]['data']['buckets'].forEach((bucket, i) => {

        this.chartJson[idx]['chartData']['datasets'].push({
          backgroundColor: [],
          data: [],
          format: this.getfielddata(this.chatConfig[idx]['data']['metrics'][i].field, 'format', idx)
        });





        let dimensions = [];
        let metrics = [];
        let timekey = [];
        let groupbBy = [];
        let orderBy = [];
        let derived_metrics = [];
        let timeKeyFilter = {};
        let dashboardMetrics = {};
        console.log('order', bucket.order_by.key);
        if (bucket.isVisible) {
          dimensions.push(bucket.field);
          if (bucket.field != 'time_key' && bucket.field != 'accounting_key') {
            groupbBy.push(bucket.field);

            if (bucket.order_by.key == 'Custom Metrics') {
              orderBy.push({ key: bucket.order_by.field, opcode: bucket.order_by.mode });
              if (dashboardMetrics[bucket.order_by.field] == undefined) {
                dashboardMetrics[bucket.order_by.field] = [];
              }
              dashboardMetrics[bucket.order_by.field].push('sum');
              if (this.getfielddata(bucket.order_by.field, 'columnType', idx) == 'derived_metrics') {
                derived_metrics.push(bucket.order_by.field);
                let calculate_columns = [];
                calculate_columns = this.getfielddata(bucket.order_by.field, 'calculate_columns', idx);
                metrics = metrics.concat(calculate_columns);
              } else {
                if (!metrics.some(x => x == bucket.order_by.field)) {
                  metrics.push(bucket.order_by.field);
                }
              }
            } else if (bucket.order_by.key == 'Alphabetical') {
              orderBy.push({ key: bucket.field, opcode: bucket.order_by.mode });
            } else {
              if (this.checkMetricAggrigation(this.chatConfig[idx]['data']['metrics'][bucket.order_by.key]['field'], this.chatConfig[idx]['data']['metrics'][bucket.order_by.key]['aggregation'], idx)) {
                orderBy.push({
                  key: this.chatConfig[idx]['data']['metrics'][bucket.order_by.key]['field'], opcode: bucket.order_by.mode
                });
              } else {
                orderBy.push({
                  key: this.chatConfig[idx]['data']['metrics'][bucket.order_by.key]['field'] + '_'
                    + this.chatConfig[idx]['data']['metrics'][bucket.order_by.key]['aggregation'], opcode: bucket.order_by.mode
                });
              }
            }
          } else {
            timekey.push(bucket.field);

            orderBy.push({ key: bucket.field, opcode: 'asc' })

          }
        }

        this.chatConfig[idx]['data']['metrics'].forEach(metric => {
          if (dashboardMetrics[metric.field] == undefined) {
            dashboardMetrics[metric.field] = [];
          }
          dashboardMetrics[metric.field].push(metric.aggregation.toLowerCase());
          if (this.getfielddata(metric.field, 'columnType', idx) == 'derived_metrics') {
            derived_metrics.push(metric.field);
            let calculate_columns = [];
            calculate_columns = this.getfielddata(metric.field, 'calculate_columns', idx);
            metrics = metrics.concat(calculate_columns);
            console.log(derived_metrics, this.getfielddata(metric.field, 'calculate_columns', idx));
          } else {
            if (!metrics.some(x => x == metric.field)) {
              metrics.push(metric.field);
            }
          }
        });

        if (this.visualizeData[idx]['dashboard_config']['customize_daterange']['is_show']) {
          timeKeyFilter = {
            time_key1: this.visualizeData[idx]['dashboard_config']['customize_daterange']['time_key1'],
            time_key2: this.visualizeData[idx]['dashboard_config']['customize_daterange']['time_key2']
          };
        } else {
          timeKeyFilter = this.filtersApplied['timeKeyFilter'];
        }
        this.apiRequest = {
          dimensions: Array.from(new Set(dimensions.concat(['time_key']))),
          metrics: Array.from(new Set(metrics)),
          derived_metrics: Array.from(new Set(derived_metrics)),
          timeKeyFilter: timeKeyFilter,
          filters: { dimensions: [], metrics: [] },
          groupByTimeKey: {
            key: Array.from(new Set(timekey.concat(['time_key']))),
            interval: 'daily'
          },
          dashboardDetails: {

            isDashboard: 'True',
            dimensions: Array.from(new Set(dimensions)),

            metrics: [dashboardMetrics]
          },
          gidGroupBy: groupbBy.length ? Array.from(new Set(groupbBy)) : ['source'],
          orderBy: orderBy,
          limit: bucket['order_by']['size'],
          offset: '0'
        };

        this.showChart[idx] = false;
        this.noDataChart[idx] = false;
        this.dataFetchServ.getChartData(this.apiRequest, this.keySpaces[idx]['api_url']).subscribe(data => {
          const chartData = data['data'];
          this.noDataChart[idx] = false;
          if (!chartData.length) {
            this.noDataChart[idx] = true;
            return;
          }
          let labelArr = Array.from(new Set(chartData.map(r => r[bucket['field']])));
          if (bucket['field'] == 'time_key') {
            labelArr = labelArr.map(d =>
              moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
            );
          } else if (bucket['field'] == 'accounting_key') {
            labelArr = labelArr.map(d =>
              moment(d, 'YYYYMMDD').format('MMM-YYYY')
            );
          }
          this.chartJson[idx]['chartData']['labels'] = this.chartJson[idx]['chartData']['labels'].concat(labelArr);
          colors = this.libServ.dynamicColors(this.chartJson[idx]['chartData']['labels'].length);
          const dataSets = [];
          chartData.forEach(r => {
            if (this.checkMetricAggrigation(this.chatConfig[idx]['data']['metrics'][0].field, this.chatConfig[idx]['data']['metrics'][0].aggregation, idx)) {
              dataSets.push(r[this.chatConfig[idx]['data']['metrics'][0].field]);
            } else {
              dataSets.push(r[this.chatConfig[idx]['data']['metrics'][0].field + '_' + this.chatConfig[idx]['data']['metrics'][0].aggregation]);
            }
          });
          this.chartJson[idx]['chartData']['datasets'][i]['data'] = dataSets;
          this.chartJson[idx]['chartData']['datasets'][i]['backgroundColor'] =
            colors.slice(this.chartJson[idx]['chartData']['labels'].length - labelArr.length);
          console.log('chartjs', this.chartJson[idx]);
          this.showChart[idx] = true;
        });
      });
    });

  }

  runCardsPanelQuery(idx) {

    return new Promise((resolve, reject) => {
      let colors = this.libServ.dynamicColors(10);
      console.log('chart', this.chatConfig);
      let that = this;
      this.defualtChartJson = {
        data: [
        ]
      };
      this.chartJson[idx] = this.libServ.deepCopy(this.defualtChartJson);

      this.chatConfig[idx]['data']['metrics'].forEach((metric, i) => {
        if (metric.isVisible) {
          this.chartJson[idx]['data'].push({
            field: this.checkMetricAggrigation(metric.field, metric.aggregation, idx) ? metric.field : metric.field + '_' + metric.aggregation,
            displayName: metric.custom_label == '' ? this.getTitle(metric, idx) : metric.custom_label,
            value: 0,
            imageClass: metric.icon_class,
            format: this.getfielddata(metric.field, 'format', idx),
            config: [],
            color: metric.card_color
          });
        }
      });




      let dimensions = [];
      let metrics = [];
      let timekey = [];
      let groupbBy = [];
      let orderBy = [];
      let derived_metrics = [];
      let timeKeyFilter = {};
      let dashboardMetrics = {};
      this.chatConfig[idx]['data']['metrics'].forEach(metric => {
        if (metric.isVisible) {
          if (dashboardMetrics[metric.field] == undefined) {
            dashboardMetrics[metric.field] = [];
          }
          dashboardMetrics[metric.field].push(metric.aggregation.toLowerCase());
          if (this.getfielddata(metric.field, 'columnType', idx) == 'derived_metrics') {
            derived_metrics.push(metric.field);
            let calculate_columns = [];
            calculate_columns = this.getfielddata(metric.field, 'calculate_columns', idx);
            metrics = metrics.concat(calculate_columns);
            console.log(derived_metrics, this.getfielddata(metric.field, 'calculate_columns', idx));
          } else {
            if (!metrics.some(x => x == metric.field)) {
              metrics.push(metric.field);
            }

          }

        }
      });
      if (this.visualizeData[idx]['dashboard_config']['customize_daterange']['is_show']) {
        timeKeyFilter = {
          time_key1: this.visualizeData[idx]['dashboard_config']['customize_daterange']['time_key1'],
          time_key2: this.visualizeData[idx]['dashboard_config']['customize_daterange']['time_key2']
        };
      } else {
        timeKeyFilter = this.filtersApplied['timeKeyFilter'];
      }
      this.apiRequest = {
        dimensions: Array.from(new Set(dimensions.concat(['time_key']))),
        metrics: Array.from(new Set(metrics)),
        derived_metrics: Array.from(new Set(derived_metrics)),
        timeKeyFilter: timeKeyFilter,
        filters: { dimensions: [], metrics: [] },
        groupByTimeKey: {
          key: Array.from(new Set(timekey.concat(['time_key']))),
          interval: 'daily'
        },
        dashboardDetails: {

          isDashboard: 'True',
          dimensions: Array.from(new Set(dimensions)),

          metrics: [dashboardMetrics]
        },
        gidGroupBy: groupbBy.length ? Array.from(new Set(groupbBy)) : ['source'],
        orderBy: orderBy,
        limit: '',
        offset: ''
      };

      this.showChart[idx] = false;
      this.noDataChart[idx] = false;
      this.dataFetchServ.getChartData(this.apiRequest, this.keySpaces[idx]['api_url']).subscribe(data => {
        const chartData = data['data'];
        this.noDataChart[idx] = false;
        if (!chartData.length) {
          this.noDataChart[idx] = true;
          return;
        }
        if (!this.libServ.isEmptyObj(data['data'])) {
          this.chartJson[idx]['data'].map(o => {
            o['value'] = data['data'][0][o['field']];
          });
        } else {
          this.chartJson[idx]['data'].map(o => {
            o['value'] = 0;
          });
        }
        console.log('chartjs', this.chartJson[idx]);
        this.showChart[idx] = true;
      });
    })
  }

  runRadarChartQuery(idx) {
    let colors = this.libServ.dynamicColors(10);
    this.defualtChartJson = {
      chartTypes: [],
      chartData: {
        labels: [],
        datasets: []
      },
      chartOptions: {
        title: {
          display: true,
          text: this.visualizeData[idx]['title']
        },
        legend: {
          display: true,
          position: 'top',
        },
        scale: {
          gridLines: {
            display: this.chatConfig[idx]['panel_setting']['label_settings']['show_grids']
          },
          pointLabels: {
            callback: (pointLabel, index, labels) => {
              return this.chatConfig[idx]['panel_setting']['label_settings']['show_labels'] ? pointLabel.toString().substr(0, this.chatConfig[idx]['panel_setting']['label_settings']['truncate']) : '';
            }
          },
          ticks: {
            min: this.chatConfig[idx]['panel_setting']['radar_settings'].axis_extend.min == '' ? undefined : this.chatConfig[idx]['panel_setting']['radar_settings'].axis_extend.min,
            max: this.chatConfig[idx]['panel_setting']['radar_settings'].axis_extend.max == '' ? undefined : this.chatConfig[idx]['panel_setting']['radar_settings'].axis_extend.max,
            display: this.chatConfig[idx]['panel_setting']['radar_settings']['tick_show'],
          }
        },
        elements: {
          point: {
            display: false
          }
        },
        tooltips: {
          intersect: false,
          enabled: this.chatConfig[idx]['panel_setting']['radar_settings']['show_tooltips'],
          callbacks: {
            label: (tooltipItem, data) => {
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(
                  tooltipItem.value,
                  data.datasets[tooltipItem.datasetIndex].format,
                  []
                )}`;
            },
            title: function (tooltipItems, data) {
              return data.labels[tooltipItems[0].index];
            },
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: this.visualizeData[idx]['dashboard_config']['height'] - 100 + 'px'
    }

    this.chartJson[idx] = this.libServ.deepCopy(this.defualtChartJson);
    this.chartJson[idx]['chartTypes'].push({ key: 'radar', label: 'Radar', stacked: false });
    this.chartJson[idx]['chartOptions']['legend']['position'] = this.chatConfig[idx]['panel_setting']['radar_settings']['legend_position'];
    this.chatConfig[idx]['data']['metrics'].forEach((element, i) => {
      if (element.isVisible) {

        this.chartJson[idx]['chartData']['datasets'].push({
          label: element.custom_label == '' ? this.getTitle(element, idx) : element.custom_label,
          type: element.chart_type,
          format: this.getfielddata(element.field, 'format', idx),
          backgroundColor: this.convertHexToRGBA(colors[i], 0.2),
          borderColor: this.convertHexToRGBA(colors[i], 1),
          data: [],
        });
      }
    });
    let dimensions = [];
    let metrics = [];
    let timekey = [];
    let groupbBy = [];
    let orderBy = [];
    let derived_metrics = [];
    let dashboardMetrics = {};
    this.chatConfig[idx]['data']['buckets'].forEach(bucket => {

      dimensions.push(bucket.field);
      if (bucket.field != 'time_key' && bucket.field != 'accounting_key') {
        groupbBy.push(bucket.field);
        if (bucket.order_by.key == 'Custom Metrics') {
          orderBy.push({ key: bucket.order_by.field, opcode: bucket.order_by.mode });
          if (dashboardMetrics[bucket.order_by.field] == undefined) {
            dashboardMetrics[bucket.order_by.field] = [];
          }
          dashboardMetrics[bucket.order_by.field].push('sum');
          if (this.getfielddata(bucket.order_by.field, 'columnType', idx) == 'derived_metrics') {
            derived_metrics.push(bucket.order_by.field);
            let calculate_columns = [];
            calculate_columns = this.getfielddata(bucket.order_by.field, 'calculate_columns', idx);
            metrics = metrics.concat(calculate_columns);
            console.log(derived_metrics, this.getfielddata(bucket.order_by.field, 'calculate_columns', idx));
          } else {
            if (!metrics.some(x => x == bucket.order_by.field)) {
              metrics.push(bucket.order_by.field);
            }
          }
        } else if (bucket.order_by.key == 'Alphabetical') {
          orderBy.push({ key: bucket.field, opcode: bucket.order_by.mode });
        } else {
          if (this.checkMetricAggrigation(this.chatConfig[idx]['data']['metrics'][bucket.order_by.key]['field'], this.chatConfig[idx]['data']['metrics'][bucket.order_by.key]['aggregation'], idx)) {
            orderBy.push({
              key: this.chatConfig[idx]['data']['metrics'][bucket.order_by.key]['field'], opcode: bucket.order_by.mode
            });
          } else {
            orderBy.push({
              key: this.chatConfig[idx]['data']['metrics'][bucket.order_by.key]['field'] + '_'
                + this.chatConfig[idx]['data']['metrics'][bucket.order_by.key]['aggregation'], opcode: bucket.order_by.mode
            });
          }
        }
      } else {
        timekey.push(bucket.field);

        orderBy.push({ key: bucket.field, opcode: 'asc' })

      }
    });
    this.chatConfig[idx]['data']['metrics'].forEach(metric => {

      if (metric.isVisible) {
        if (dashboardMetrics[metric.field] == undefined) {
          dashboardMetrics[metric.field] = [];
        }
        dashboardMetrics[metric.field].push(metric.aggregation.toLowerCase());
        if (this.getfielddata(metric.field, 'columnType', idx) == 'derived_metrics') {
          derived_metrics.push(metric.field);
          let calculate_columns = [];
          calculate_columns = this.getfielddata(metric.field, 'calculate_columns', idx);
          metrics = metrics.concat(calculate_columns);
          console.log(derived_metrics, this.getfielddata(metric.field, 'calculate_columns', idx));
        } else {
          if (!metrics.some(x => x == metric.field)) {
            metrics.push(metric.field);
          }
        }
      }
    });
    let timeKeyFilter = {};
    if (this.visualizeData[idx]['dashboard_config']['customize_daterange']['is_show']) {
      timeKeyFilter = {
        time_key1: this.visualizeData[idx]['dashboard_config']['customize_daterange']['time_key1'],
        time_key2: this.visualizeData[idx]['dashboard_config']['customize_daterange']['time_key2']
      };
    } else {
      timeKeyFilter = this.filtersApplied['timeKeyFilter'];
    }
    this.apiRequest = {
      dimensions: Array.from(new Set(dimensions.concat(['time_key']))),
      metrics: Array.from(new Set(metrics)),
      derived_metrics: Array.from(new Set(derived_metrics)),
      timeKeyFilter: timeKeyFilter,
      filters: { dimensions: [], metrics: [] },
      groupByTimeKey: {
        key: Array.from(new Set(timekey.concat(['time_key']))),
        interval: 'daily'
      },
      dashboardDetails: {

        isDashboard: 'True',
        dimensions: Array.from(new Set(dimensions)),

        metrics: [dashboardMetrics]
      },
      gidGroupBy: groupbBy.length ? Array.from(new Set(groupbBy)) : ['source'],
      orderBy: orderBy,
      limit: this.chatConfig[idx]['data']['buckets'][0]['order_by']['size'],
      offset: '0'
    };

    this.showChart[idx] = false;
    this.noDataChart[idx] = false;
    this.dataFetchServ.getChartData(this.apiRequest, this.keySpaces[idx]['api_url']).subscribe(data => {
      const chartData = data['data'];
      this.noDataChart[idx] = false;
      if (!chartData.length) {
        this.noDataChart[idx] = true;
        return;
      }
      const labelArr = Array.from(new Set(chartData.map(r => r[this.chatConfig[idx]['data']['buckets'][0]['field']])));
      if (this.chatConfig[idx]['data']['buckets'][0]['field'] == 'time_key') {
        this.chartJson[idx]['chartData']['labels'] = labelArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );
      } else if (this.chatConfig[idx]['data']['buckets'][0]['field'] == 'accounting_key') {
        this.chartJson[idx]['chartData']['labels'] = labelArr.map(d =>
          moment(d, 'YYYYMMDD').format('MMM-YYYY')
        );
      } else {
        this.chartJson[idx]['chartData']['labels'] = labelArr;

      }

      colors = this.libServ.dynamicColors(this.chartJson[idx]['chartData']['labels'].length);
      const dataSets = {};
      this.chatConfig[idx]['data']['metrics'].forEach(element => {
        if (element.isVisible) {
          if (this.checkMetricAggrigation(element.field, element.aggregation, idx)) {
            dataSets[element.field] = [];
          } else {
            dataSets[element.field + '_' + element.aggregation] = [];
          }
        }
      });
      labelArr.forEach(label => {
        chartData.forEach(r => {
          if (r[this.chatConfig[idx]['data']['buckets'][0]['field']] === label) {
            this.chatConfig[idx]['data']['metrics'].forEach(element => {
              if (element.isVisible) {
                if (this.checkMetricAggrigation(element.field, element.aggregation, idx)) {
                  dataSets[element.field].push(r[element.field]);
                } else {
                  dataSets[element.field + '_' + element.aggregation].push(r[element.field + '_' + element.aggregation]);
                }
              }
            });

          }
        });
      });
      this.chatConfig[idx]['data']['metrics'].filter(x => x.isVisible).forEach((element, i) => {
        if (element.isVisible) {
          if (this.checkMetricAggrigation(element.field, element.aggregation, idx)) {
            this.chartJson[idx]['chartData']['datasets'][i]['data'] = dataSets[element.field];
          } else {
            this.chartJson[idx]['chartData']['datasets'][i]['data'] = dataSets[element.field + '_' + element.aggregation];
          }
        }
      });
      this.showChart[idx] = true;
    });
  }
  runDataTableQuery(idx) {
    let colors = this.libServ.dynamicColors(10);
    console.log('chart', this.chatConfig[idx]);
    let that = this;
    this.defualtChartJson = {
      flatTableJson: {
        page_size: this.chatConfig[idx]['table_setting']['data_setting']['page_size'],
        page: 0,
        lazy: false,
        loading: false,
        export: this.chatConfig[idx]['table_setting']['setting']['export'],
        sortMode: 'multiple',
        resizableColumns: this.chatConfig[idx]['table_setting']['setting']['resizableColumns'],
        columnResizeMode: 'fit',
        reorderableColumns: true,
        scrollHeight: '350px',
        totalRecords: 1000,
        columns: [],
        selectedColumns: [],
        globalFilterFields: [],
        frozenCols: [],
        frozenWidth: '0px',
        scrollable: true,
        selectionMode: 'multiple',
        selectedColsModal: [],
        selectionDataKey: 'name',
        metaKeySelection: true,
        showHideCols: this.chatConfig[idx]['table_setting']['setting']['showHideCols'],
        overallSearch: this.chatConfig[idx]['table_setting']['setting']['overallSearch'],
        columnSearch: this.chatConfig[idx]['table_setting']['setting']['columnSearch']
      },
      flatTableData: []
    };
    this.chartJson[idx] = this.libServ.deepCopy(this.defualtChartJson);
    let coldef = [];
    let frozenCols = [];
    this.chatConfig[idx]['data']['buckets'].forEach((bucket, i) => {
      if (bucket.isVisible) {
        if (bucket.options.isfrozen) {
          frozenCols.push({
            field: bucket.field,
            displayName: bucket.custom_label == '' ? this.getTitle(bucket, idx) : bucket.custom_label,
            format: this.getfielddata(bucket.field, 'format', idx),
            width: bucket.width,
            value: '',
            condition: '',
            columnType: 'dimensions',
            isClearSearch: true,
            exportConfig: {
              format: this.getfielddata(bucket.field, 'format', idx),
              styleinfo: {
                thead: 'default',
                tdata: 'white'
              }
            },
            formatConfig: [],
            options: {
              editable: bucket.options.editable,
              colSearch: bucket.options.colSearch,
              colSort: bucket.options.colSort,
              resizable: bucket.options.resizable,
              movable: bucket.options.movable
            }
          });
        } else {
          coldef.push({
            field: bucket.field,
            displayName: bucket.custom_label == '' ? this.getTitle(bucket, idx) : bucket.custom_label,
            format: this.getfielddata(bucket.field, 'format', idx),
            width: bucket.width,
            value: '',
            condition: '',
            columnType: 'dimensions',
            isClearSearch: true,
            exportConfig: {
              format: this.getfielddata(bucket.field, 'format', idx),
              styleinfo: {
                thead: 'default',
                tdata: 'white'
              }
            },
            formatConfig: [],
            options: {
              editable: bucket.options.editable,
              colSearch: bucket.options.colSearch,
              colSort: bucket.options.colSort,
              resizable: bucket.options.resizable,
              movable: bucket.options.movable
            }
          });
        }
      }
    });

    this.chatConfig[idx]['data']['metrics'].forEach((metric, i) => {
      if (metric.isVisible) {
        coldef.push({
          field: this.checkMetricAggrigation(metric.field, metric.aggregation, idx) ?
            metric.field : metric.field + '_' + metric.aggregation,
          displayName: metric.custom_label == '' ? this.getTitle(metric, idx) : metric.custom_label,
          format: this.getfielddata(metric.field, 'format', idx),
          width: metric.width,
          value: '',
          condition: '',
          columnType: 'dimensions',
          isClearSearch: true,
          exportConfig: {
            format: this.getfielddata(metric.field, 'format', idx) == '$' ? 'currency' : this.getfielddata(metric.field, 'format', idx),
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: metric.options.editable,
            colSearch: metric.options.colSearch,
            colSort: metric.options.colSort,
            resizable: metric.options.resizable,
            movable: metric.options.movable
          }
        });
      }
    });

    this.chartJson[idx]['flatTableJson']['columns'] = coldef;
    this.chartJson[idx]['flatTableJson']['selectedColumns'] = coldef;
    this.chartJson[idx]['flatTableJson']['globalFilterFields'] = frozenCols.concat(coldef);
    this.chartJson[idx]['flatTableJson']['frozenCols'] = frozenCols.length > 0 ? frozenCols : undefined;
    this.chartJson[idx]['flatTableJson']['frozenWidth'] = frozenCols.reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px';
    let dimensions = [];
    let metrics = [];
    let timekey = [];
    let groupbBy = [];
    let orderBy = [];
    let derived_metrics = [];
    let dashboardMetrics = {};
    this.chatConfig[idx]['data']['buckets'].forEach(bucket => {
      dimensions.push(bucket.field);
      if (bucket.field != 'time_key' && bucket.field != 'accounting_key') {
        groupbBy.push(bucket.field);
      } else {
        timekey.push(bucket.field);
      }
    });
    this.chatConfig[idx]['data']['metrics'].forEach(metric => {

      if (metric.isVisible) {
        if (dashboardMetrics[metric.field] == undefined) {
          dashboardMetrics[metric.field] = [];
        }
        dashboardMetrics[metric.field].push(metric.aggregation.toLowerCase());
        if (this.getfielddata(metric.field, 'columnType', idx) == 'derived_metrics') {
          derived_metrics.push(metric.field);
          let calculate_columns = [];
          calculate_columns = this.getfielddata(metric.field, 'calculate_columns', idx);
          metrics = metrics.concat(calculate_columns);
        } else {
          if (!metrics.some(x => x == metric.field)) {
            metrics.push(metric.field);
          }
        }
      }
    });

    this.chatConfig[idx]['table_setting']['data_setting']['order'].forEach(element => {
      if (element.key.type == 'D') {
        orderBy.push({ key: this.chatConfig[idx]['data']['buckets'][element.key.index]['field'], opcode: element.opcode });
      } else {
        if (this.checkMetricAggrigation(this.chatConfig['data']['metrics'][element.key.index]['field'], this.chatConfig['data']['metrics'][element.key.index]['aggregation'], idx)) {
          orderBy.push({
            key: this.chatConfig[idx]['data']['metrics'][element.key.index]['field']
          });
        } else {
          orderBy.push({
            key: this.chatConfig[idx]['data']['metrics'][element.key.index]['field'] + '_'
              + this.chatConfig[idx]['data']['metrics'][element.key.index]['aggregation'], opcode: element.opcode
          });
        }
      }
    });
    let timeKeyFilter = {};
    if (this.visualizeData[idx]['dashboard_config']['customize_daterange']['is_show']) {
      timeKeyFilter = {
        time_key1: this.visualizeData[idx]['dashboard_config']['customize_daterange']['time_key1'],
        time_key2: this.visualizeData[idx]['dashboard_config']['customize_daterange']['time_key2']
      };
    } else {
      timeKeyFilter = this.filtersApplied['timeKeyFilter'];
    }
    this.apiRequest = {
      dimensions: Array.from(new Set(dimensions.concat(['time_key']))),
      metrics: Array.from(new Set(metrics)),
      derived_metrics: Array.from(new Set(derived_metrics)),
      timeKeyFilter: timeKeyFilter,
      filters: { dimensions: [], metrics: [] },
      groupByTimeKey: {
        key: Array.from(new Set(timekey.concat(['time_key']))),
        interval: 'daily'
      },
      dashboardDetails: {

        isDashboard: 'True',
        dimensions: Array.from(new Set(dimensions)),

        metrics: [dashboardMetrics]
      },
      gidGroupBy: groupbBy.length ? Array.from(new Set(groupbBy)) : ['source'],
      orderBy: orderBy,
      limit: this.chatConfig[idx]['table_setting']['data_setting']['limit'],
      offset: '0'
    };
    this.noDataChart[idx] = false;
    this.showChart[idx] = false;
    // // this.noDataChart = false;
    // this.noDataChart = true;
    // this.showChart = true;
    this.dataFetchServ.getChartData(this.apiRequest, this.keySpaces[idx]['api_url']).subscribe(data => {
      if (data['status'] === 0) {

        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      } else {


      }
      const arr = [];
      const totalsColData = data['data'];
      for (const r of totalsColData) {
        const obj = {};
        obj['data'] = r;
        arr.push(obj);
      }
      // this.tblResData = arr;

      this.chartJson[idx]['flatTableData'] = <TreeNode[]>arr;
      this.chartJson[idx]['flatTableJson']['totalRecords'] = totalsColData.length;
      this.chartJson[idx]['flatTableJson']['loading'] = false;
      // this.isChange[idx] = true;
      this.showChart[idx] = true;
      setTimeout(() => {
        this.adjustTableHeight(idx);
      }, 0);

      console.log('chartjson[idx]', this.chartJson[idx])
    });

  }

  runHorizontalBarQuery(idx) {
    let colors = this.libServ.dynamicColors(10);
    console.log('chart', this.chatConfig[idx]);
    // let dropdownElement = (document.querySelector('#dragElement_' + idx + ' .dropdown')['offsetHeight'] ? document.querySelector('#dragElement_' + idx + ' .dropdown')['offsetHeight'] : 23) - 23;
    this.defualtChartJson = {
      chartTypes: [],
      chartData: {
        labels: [],
        datasets: []
      },
      chartOptions: {
        title: {
          display: true,
          text: this.title
        },
        legend: {
          display: true,
          position: 'top'
        },
        elements: {
          point: {
            display: false
          }
        },
        scales: {
          xAxes: [],
          yAxes: []
        },
        tooltips: {
          mode: this.chatConfig[idx]['data']['buckets'][0]['slice_data']['is_show'] ? 'nearest' : 'index',
          intersect: false,
          enabled: this.chatConfig[idx]['panel_setting']['settings']['show_tooltips'],
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, data.datasets[tooltipItem.datasetIndex].format, [])}`;
            }
          }
        },
        plugins: {
          datalabels: false,
          labels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: this.visualizeData[idx]['dashboard_config']['height'] - 100 + 'px',
      calculate_value: this.chatConfig[idx]['panel_setting']['settings']['calculate_value'],
    }
    this.chartJson[idx] = this.libServ.deepCopy(this.defualtChartJson);
    this.chartJson[idx]['chartTypes'].push({ key: 'horizontalBar', label: 'Line Chart', stacked: false });
    this.chatConfig[idx]['data']['metrics'].forEach((element, i) => {
      if (element.isVisible) {

        this.chartJson[idx]['chartData']['datasets'].push({
          label: element.custom_label == '' ? this.getTitle(element, idx) : element.custom_label,
          borderColor: element.legend_color,
          format: this.getfielddata(element.field, 'format', idx),
          fill: false,
          yAxisID: 'LeftAxis-1',
          backgroundColor: element.legend_color,
          data: [],
          trendlineLinear: (this.chatConfig[idx]['panel_setting']['threshold_line']['is_show']) ? {
            style: this.chatConfig[idx]['panel_setting']['threshold_line']['line_color'],
            lineStyle: this.chatConfig[idx]['panel_setting']['threshold_line']['line_style'],
            width: this.chatConfig[idx]['panel_setting']['threshold_line']['line_width']
          } : undefined
        });

      }
    });

    const xAxes = this.chatConfig[idx]['data']['buckets'][0];
    this.chartJson[idx]['chartOptions']['scales']['yAxes'].push({
      type: 'category',
      id: 'LeftAxis-1',
      display: true,
      scaleLabel: {
        display: xAxes.show_axis_lines_label.is_show,
        labelString: xAxes.custom_label == '' ? this.getTitle(xAxes, idx) : xAxes.custom_label,
      },
      gridLines: {
        display: this.chatConfig[idx]['panel_setting']['grid']['y_axis_lines']
      },
      position: xAxes.position,
      stacked: true,
      ticks: {
        // autoRotate: true,
        minRotation: xAxes.show_axis_lines_label.show_label.align.minRotation,
        maxRotation: xAxes.show_axis_lines_label.show_label.align.maxRotation,
        display: xAxes.show_axis_lines_label.show_label.is_show,
        callback: (value, index, values) => {
          // return (value / this.max * 100).toFixed(0) + '%';
          return value.toString().substr(0, xAxes.show_axis_lines_label.show_label.truncate);
        }
      }
    });
    this.chatConfig[idx]['data']['metrics'].filter(x => x.isVisible).forEach((element, i) => {

      this.chartJson[idx]['chartOptions']['scales']['xAxes'].push({
        gridLines: {
          display: this.chatConfig[idx]['panel_setting']['grid']['x_axis_lines']
        },

        display: true,
        scaleLabel: {
          display: element.show_axis_lines_label.is_show,
          labelString: element.custom_label == '' ? this.getTitle(element, idx) : element.custom_label,
        },
        position: element.position,
        name: '1',
        stacked: this.chatConfig[idx]['data']['buckets'][0]['slice_data']['is_show'] && this.chatConfig[idx]['data']['buckets'][0]['slice_data']['isStacked'],
        format: this.getfielddata(element.field, 'format', idx),
        ticks: {
          minRotation: element.show_axis_lines_label.show_label.align.minRotation,
          maxRotation: element.show_axis_lines_label.show_label.align.maxRotation,
          min: element.custom_extend.axis_extend.min == '' ? undefined : element.custom_extend.axis_extend.min,
          max: element.custom_extend.axis_extend.max == '' ? undefined : element.custom_extend.axis_extend.max,
          stepSize: (element.custom_extend.scale_to_bound.is_show && element.custom_extend.scale_to_bound.range != '') ? element.custom_extend.scale_to_bound.range : undefined,
          display: element.show_axis_lines_label.show_label.is_show,
          callback: (value, index, values) => {
            // return (value / this.max * 100).toFixed(0) + '%';

            return this.formatNumPipe.transform(value, this.chartJson[idx]['chartOptions']['scales']['xAxes'][i].format, []).substr(0, element.show_axis_lines_label.show_label.truncate + 1);
          }
        }
        // scaleFontColor: "rgba(151,137,200,0.8)"
      });
    });
    this.chartJson[idx]['chartOptions']['legend']['position'] = this.chatConfig[idx]['panel_setting']['settings']['legend_position'];
    console.log(this.chartJson[idx]['chartOptions']['legend']['position'], this.chatConfig[idx]['panel_setting']['settings']['legend_position'])
    let dimensions = [];
    let metrics = [];
    let timekey = [];
    let groupbBy = [];
    let orderBy = [];
    let derived_metrics = [];
    let dashboardMetrics = {};
    let bucketData = [];
    let graphOrderBy = [];
    let timeKeyFilter = {};
    bucketData.push(this.chatConfig[idx]['data']['buckets'][0]);
    if (this.chatConfig[idx]['data']['buckets'][0]['slice_data']['is_show']) {
      bucketData.push(this.chatConfig[idx]['data']['buckets'][0]['slice_data']);
    }
    bucketData.forEach(bucket => {
      console.log('order', bucket.order_by.key);

      dimensions.push(bucket.field);
      if (bucket.field != 'time_key' && bucket.field != 'accounting_key') {
        groupbBy.push(bucket.field);
        if (bucket.order_by.key == 'Custom Metrics') {
          orderBy.push({ key: bucket.order_by.field, opcode: bucket.order_by.mode });
          if (dashboardMetrics[bucket.order_by.field] == undefined) {
            dashboardMetrics[bucket.order_by.field] = [];
          }
          dashboardMetrics[bucket.order_by.field].push('sum');
          if (this.getfielddata(bucket.order_by.field, 'columnType', idx) == 'derived_metrics') {
            derived_metrics.push(bucket.order_by.field);
            let calculate_columns = [];
            calculate_columns = this.getfielddata(bucket.order_by.field, 'calculate_columns', idx);
            metrics = metrics.concat(calculate_columns);
            console.log(derived_metrics, this.getfielddata(bucket.order_by.field, 'calculate_columns', idx));
          } else {
            if (!metrics.some(x => x == bucket.order_by.field)) {
              metrics.push(bucket.order_by.field);
            }
          }
        } else if (bucket.order_by.key == 'Alphabetical') {
          orderBy.push({ key: bucket.field, opcode: bucket.order_by.mode });
        } else {
          if (this.checkMetricAggrigation(this.chatConfig[idx]['data']['metrics'][bucket.order_by.key]['field'], this.chatConfig[idx]['data']['metrics'][bucket.order_by.key]['aggregation'], idx)) {
            orderBy.push({
              key: this.chatConfig[idx]['data']['metrics'][bucket.order_by.key]['field'], opcode: bucket.order_by.mode
            });
          } else {
            orderBy.push({
              key: this.chatConfig[idx]['data']['metrics'][bucket.order_by.key]['field'] + '_'
                + this.chatConfig[idx]['data']['metrics'][bucket.order_by.key]['aggregation'], opcode: bucket.order_by.mode
            });
          }
        }
      } else {
        timekey.push(bucket.field);

        orderBy.push({ key: bucket.field, opcode: 'asc' })

      }
    });
    this.chatConfig[idx]['data']['metrics'].forEach(metric => {

      if (metric.isVisible) {
        if (dashboardMetrics[metric.field] == undefined) {
          dashboardMetrics[metric.field] = [];
        }
        dashboardMetrics[metric.field].push(metric.aggregation.toLowerCase());
        if (this.getfielddata(metric.field, 'columnType', idx) == 'derived_metrics') {
          derived_metrics.push(metric.field);
          let calculate_columns = [];
          calculate_columns = this.getfielddata(metric.field, 'calculate_columns', idx);
          metrics = metrics.concat(calculate_columns);
          console.log(derived_metrics, this.getfielddata(metric.field, 'calculate_columns', idx));
        } else {
          if (!metrics.some(x => x == metric.field)) {
            metrics.push(metric.field);
          }
        }
      }
    });
    if (this.chatConfig[idx]['data']['buckets'][0]['slice_data']['is_show']) {
      graphOrderBy.push({
        colmun_Name: this.chatConfig[idx]['data']['buckets'][0]['field'],
        order: orderBy[0]['opcode'],
        limit: this.chatConfig[idx]['data']['buckets'][0]['order_by']['size'] == null ? '' : this.chatConfig[idx]['data']['buckets'][0]['order_by']['size'],
        sort_column: orderBy[0]['key']
      }, {
        colmun_Name: this.chatConfig[idx]['data']['buckets'][0]['slice_data']['field'],
        order: orderBy[1]['opcode'],
        limit: this.chatConfig[idx]['data']['buckets'][0]['slice_data']['order_by']['size'] == null ? '' : this.chatConfig[idx]['data']['buckets'][0]['slice_data']['order_by']['size'],
        sort_column: orderBy[1]['key']
      })
      orderBy = [];
    }

    if (this.visualizeData[idx]['dashboard_config']['customize_daterange']['is_show']) {
      timeKeyFilter = {
        time_key1: this.visualizeData[idx]['dashboard_config']['customize_daterange']['time_key1'],
        time_key2: this.visualizeData[idx]['dashboard_config']['customize_daterange']['time_key2']
      };
    } else {
      timeKeyFilter = this.filtersApplied['timeKeyFilter'];
    }

    this.apiRequest = {
      dimensions: Array.from(new Set(dimensions.concat(['time_key']))),
      metrics: Array.from(new Set(metrics)),
      derived_metrics: Array.from(new Set(derived_metrics)),
      timeKeyFilter: timeKeyFilter,
      filters: { dimensions: [], metrics: [] },
      groupByTimeKey: {
        key: Array.from(new Set(timekey.concat(['time_key']))),
        interval: 'daily'
      },
      dashboardDetails: {

        isDashboard: 'True',
        dimensions: Array.from(new Set(dimensions)),
        // graphOrderBy: graphOrderBy.length > 0 ? graphOrderBy : null,
        metrics: [dashboardMetrics]
      },
      gidGroupBy: groupbBy.length ? Array.from(new Set(groupbBy)) : ['source'],
      orderBy: orderBy,
      limit: this.chatConfig[idx]['data']['buckets'][0]['slice_data']['is_show'] ? '' : this.chatConfig[idx]['data']['buckets'][0]['order_by']['size'],
      offset: '0'
    };
    if (graphOrderBy.length > 0) {
      this.apiRequest['dashboardDetails']['graphOrderBy'] = graphOrderBy;
    }
    this.showChart[idx] = false;
    this.noDataChart[idx] = false;
    this.dataFetchServ.getChartData(this.apiRequest, this.keySpaces[idx]['api_url']).subscribe(data => {
      const chartData = data['data'];
      this.noDataChart[idx] = false;
      if (!chartData.length) {
        this.noDataChart[idx] = true;
        return;
      }
      const labelArr = Array.from(new Set(chartData.map(r => r[this.chatConfig[idx]['data']['buckets'][0]['field']])));
      if (this.chatConfig[idx]['data']['buckets'][0]['field'] == 'time_key') {
        this.chartJson[idx]['chartData']['labels'] = labelArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );
      } else if (this.chatConfig[idx]['data']['buckets'][0]['field'] == 'accounting_key') {
        this.chartJson[idx]['chartData']['labels'] = labelArr.map(d =>
          moment(d, 'YYYYMMDD').format('MMM-YYYY')
        );
      } else {
        this.chartJson[idx]['chartData']['labels'] = labelArr;

      }
      if (this.chatConfig[idx]['data']['buckets'][0]['slice_data']['is_show']) {

        if (this.chatConfig[idx]['data']['buckets'][0]['slice_data']['isStacked']) {
          this.chartJson[idx]['chartTypes'][0]['key'] = 'horizontalBar';
          this.chartJson[idx]['chartTypes'][0]['stacked'] = true;
        }
        this.chartJson[idx]['chartData']['datasets'] = [];
        const splitLabelArr = Array.from(new Set(chartData.map(r => r[this.chatConfig[idx]['data']['buckets'][0]['slice_data']['field']])));
        colors = this.libServ.dynamicColors(splitLabelArr.length + 2);
        splitLabelArr.forEach((splitLabel, i) => {
          let labelName = splitLabel;
          if (this.chatConfig[idx]['data']['buckets'][0]['slice_data']['field'] == 'time_key') {

            labelName = moment(splitLabel, 'YYYYMMDD').format('MM-DD-YYYY');
          } else if (this.chatConfig[idx]['data']['buckets'][0]['slice_data']['field'] == 'accounting_key') {
            labelName = moment(splitLabel, 'YYYYMMDD').format('MM-YYYY');
          } else {
            labelName = splitLabel;

          }
          const dataSets = {};
          this.chatConfig[idx]['data']['metrics'].forEach(element => {
            if (element.isVisible) {
              if (this.checkMetricAggrigation(element.field, element.aggregation, idx)) {
                dataSets[element.field] = [];
              } else {
                dataSets[element.field + '_' + element.aggregation] = [];
              }
            }
          });
          labelArr.forEach(label => {
            let isAvailable = true;
            chartData.forEach(r => {
              if (r[this.chatConfig[idx]['data']['buckets'][0]['field']] === label &&
                r[this.chatConfig[idx]['data']['buckets'][0]['slice_data']['field']] === splitLabel) {
                isAvailable = false;
                this.chatConfig[idx]['data']['metrics'].forEach(element => {
                  if (element.isVisible) {
                    if (this.checkMetricAggrigation(element.field, element.aggregation, idx)) {
                      dataSets[element.field].push(r[element.field]);
                    } else {
                      dataSets[element.field + '_' + element.aggregation].push(r[element.field + '_' + element.aggregation]);
                    }
                  }
                });
              }
            });
            if (isAvailable) {

              this.chatConfig[idx]['data']['metrics'].forEach(element => {
                if (element.isVisible) {
                  if (this.checkMetricAggrigation(element.field, element.aggregation, idx)) {
                    dataSets[element.field].push(null);
                  } else {
                    dataSets[element.field + '_' + element.aggregation].push(null);
                  }
                }
              });

            }
          });
          this.chatConfig[idx]['data']['metrics'].forEach((element) => {
            if (element.isVisible) {
              this.chartJson[idx]['chartData']['datasets'].push({
                label: labelName,
                borderColor: colors[i + 2],
                yAxisID: 'LeftAxis-1',
                format: this.getfielddata(element.field, 'format', idx),
                fill: false,
                backgroundColor: colors[i + 2],
                data: (this.checkMetricAggrigation(element.field, element.aggregation, idx)) ?
                  dataSets[element.field]
                  : dataSets[element.field + '_' + element.aggregation]
              });
            }
          });
        });
      } else {
        const dataSets = {};
        this.chatConfig[idx]['data']['metrics'].forEach(element => {
          if (element.isVisible) {
            if (this.checkMetricAggrigation(element.field, element.aggregation, idx)) {
              dataSets[element.field] = [];
            } else {
              dataSets[element.field + '_' + element.aggregation] = [];
            }
          }
        });
        labelArr.forEach(label => {
          chartData.forEach(r => {
            if (r[this.chatConfig[idx]['data']['buckets'][0]['field']] === label) {
              this.chatConfig[idx]['data']['metrics'].forEach(element => {
                if (element.isVisible) {
                  if (this.checkMetricAggrigation(element.field, element.aggregation, idx)) {
                    dataSets[element.field].push(r[element.field]);
                  } else {
                    dataSets[element.field + '_' + element.aggregation].push(r[element.field + '_' + element.aggregation]);
                  }
                }
              });

            }
          });
        });
        this.chatConfig[idx]['data']['metrics'].filter(x => x.isVisible).forEach((element, i) => {
          if (element.isVisible) {
            if (this.checkMetricAggrigation(element.field, element.aggregation, idx)) {
              this.chartJson[idx]['chartData']['datasets'][i]['data'] = dataSets[element.field];
            } else {
              this.chartJson[idx]['chartData']['datasets'][i]['data'] = dataSets[element.field + '_' + element.aggregation];
            }
          }
        });
      }
      this.chartJsonMain = this.libServ.deepCopy(this.chartJson[idx]);
      console.log('chartjs', this.chartJson[idx]);
      this.showChart[idx] = true;
      setTimeout(() => {
        let dropdownElement = (document.querySelector('#dragElement_' + idx + ' .dropdown')['offsetHeight'] ? document.querySelector('#dragElement_' + idx + ' .dropdown')['offsetHeight'] : 23) - 23;
        this.chartJson[idx]['chartHeight'] = (this.visualizeData[idx]['dashboard_config']['height'] - 100 - dropdownElement) + 'px';
        if (this.showChart[idx]) {
          this.showChart[idx] = false;
          setTimeout(() => {
            this.showChart[idx] = true;
          }, 0);
        }
      }, 0);
    });

  }


  convertHexToRGBA = (hexCode, opacity) => {
    let hex = hexCode.replace('#', '');
    if (hex.length === 3) {
      hex += hex
    }
    const r = parseInt(hex.substring(0, 2), 16);
    const g = parseInt(hex.substring(2, 4), 16);
    const b = parseInt(hex.substring(4, 6), 16);
    return `rgba(${r},${g},${b},${opacity})`;
  };

  deleteMetric(i) {
    this.chatConfig[i]['data']['metrics'].splice(i, 1);
  }

  deleteBucket(i) {
    this.chatConfig[i]['data']['buckets'].splice(i, 1);
  }
  onFiltersApplied(filterData: object) {
    // this.filtersApplied['filters']['dimensions'] = [];
    // for (const k in filterData['filter']['dimensions']) {
    //   if (filterData['filter']['dimensions'][k].length !== 0) {
    //     this.filtersApplied['filters']['dimensions'].push({
    //       key: k,
    //       values: filterData['filter']['dimensions'][k]
    //     });
    //   }
    // }

    // this.getTableData();
    let graphList = [];
    this.visualizeData.forEach((element, i) => {
      graphList.push({
        label: element['dashboard_config']['panel_title']['title'],
        value: { id: i, chart_type: element['chart_type'] }
      })

    });
    console.log('filterData', filterData);
    const ref = this.dialogService.open(ListDashboardGraphComponent, {
      data: graphList,
      header: 'Select graphs to apply date range',
      contentStyle: { width: '60vw', overflow: 'auto' },
    });

    ref.onClose.subscribe((data: any[]) => {
      console.log('data', data);
      if (data) {
        data.forEach(element => {
          this.visualizeData[element]['dashboard_config']['customize_daterange'] = this.dateToCustomizeDaterange(filterData['date'][0], filterData['date'][1])
          if (this.visualizeData[element]['chart_type'] == 'pie' || this.visualizeData[element]['chart_type'] == 'polarArea') {

            this.runPieChartQuery(element);

          } else if (this.visualizeData[element]['chart_type'] == 'cards_panel') {
            this.runCardsPanelQuery(element);
          } else if (this.visualizeData[element]['chart_type'] == 'radar') {
            this.runRadarChartQuery(element);
          } else if (this.visualizeData[element]['chart_type'] == 'data_table') {
            this.runDataTableQuery(element);
          } else if (this.visualizeData[element]['chart_type'] == 'horizontalBar') {
            this.runHorizontalBarQuery(element);
          } else {
            this.runChartQuery(element).then(res => {

            });
          }
        });
      }
    }
    );
    // this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    // this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];
    // this.visualizeData.forEach((element, i) => {
    //   this.chatConfig[i] = JSON.parse(element['chart_config']);
    //   this.keySpaces[i] = {
    //     field_config: JSON.parse(element['field_config']),
    //     api_url: element['api_url']
    //   };

    // element['chart_type'] = this.chartTypeData.find(x => x.display_name == element['chart_type']).chart_type;

    //   if (element['chart_type'] == 'pie' || element['chart_type'] == 'polarArea') {

    //     this.runPieChartQuery(i);

    //   } else if (element['chart_type'] == 'cards_panel') {
    //     this.runCardsPanelQuery(i);
    //   } else if (element['chart_type'] == 'radar') {
    //     this.runRadarChartQuery(i);
    //   } else {
    //     this.runChartQuery(i).then(res => {

    //     });
    //   }
    // });
  }
  clearChartQuery() {
    // this.chatConfig[idx] = this.libServ.deepCopy(this.defualtChartConfig);
    this.isEdit = false;
    if (this.isUpdate) {
      this.initialLoading();
    } else {
      this.backToHome();
    }

  }
  saveChartQuery() {
    let dashboard_visualize_details = [];
    this.visualizeData.forEach((element, i) => {
      dashboard_visualize_details.push({
        visualizeId: element.visualize_id,
        dashboard_config: JSON.stringify(element['dashboard_config'])
      })
    });
    const req = {
      user_id: this.appConfig['user']['id'],
      title: '',
      description: '',
      // visualizeData: this.visualizeData,
      dashboard_visualize_details: dashboard_visualize_details,
      isUpdate: false
    };
    console.log('req', req)
    const ref = this.dialogService.open(DashboardDetailsComponent, {
      data: req,
      header: 'Dashboard Details',
      contentStyle: { width: '40vw', overflow: 'auto' },
    });

    ref.onClose.subscribe((data: string) => {
    });
  }
  updateChartQuery() {
    let dashboard_visualize_details = [];
    console.log('this.visualizeData', this.visualizeData);
    this.visualizeData.forEach((element, i) => {
      dashboard_visualize_details.push({
        visualizeId: element.visualize_id,
        dashboard_config: JSON.stringify(element.dashboard_config)
      })
    });
    const req = {
      id: this.visualizeIds[0],
      user_id: this.appConfig['user']['id'],
      title: this.title,
      description: this.description,
      // visualizeData: this.visualizeData,
      dashboard_visualize_details: dashboard_visualize_details,
      isUpdate: true
    };
    console.log('req', req)
    const ref = this.dialogService.open(DashboardDetailsComponent, {
      data: req,
      header: 'Dashboard Details',
      contentStyle: { width: '40vw', overflow: 'auto' },
    });

    ref.onClose.subscribe((data: string) => {
      this.initialLoading();
    });
  }
  // getchbeckVisible() {
  //   var result = 0;
  //   this.chatConfig[idx]['data']['metrics'].forEach(element => {
  //     if (element.isVisible) {
  //       result++;
  //     }
  //   });
  //   // console.log(result);
  //   return result == 1;
  // }
  backToHome() {
    if (localStorage.getItem('type')) {
      localStorage.removeItem('type');
    }
    if (localStorage.getItem('visualizeIds')) {
      localStorage.removeItem('visualizeIds');
    }
    this.router.navigate(['/ad-hoc-dashboard']);
  }
  editChartQuery() {
    this.isEdit = true;
  }
  addVisaualize() {
    const data = {
      isEdit: true,
      isSingle: false
    };

    const ref = this.dialogService.open(AdHocDashboardPopComponent, {
      data: data,
      header: 'Visualize Selection',
      contentStyle: { width: '60vw', overflow: 'auto' },
    });

    ref.onClose.subscribe((data: any[]) => {
      console.log('data', data);
      if (data.length > 0) {
        let req = {
          visualizeId: data
        }
        this.dataFetchServ.getVisualizeByMultipleIds(req).subscribe(data => {

          if (data['status'] === 0) {
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Server Error',
              detail: 'Please refresh the page'
            });
            console.log(data['status_msg']);
            return;
          }
          let addidx = this.visualizeData.length;
          let VData = data['data'];

          VData.forEach((element, i) => {
            element['visualize_id'] = element['id'];
            this.chatConfig[i + addidx] = JSON.parse(element['chart_config']);
            this.keySpaces[i + addidx] = {
              field_config: JSON.parse(element['field_config']),
              api_url: element['api_url']
            };
            element['dashboard_config'] = {
              chartPosition: i + addidx,
              width: '48',
              height: '300',
              panel_title: {
                is_show: true,
                title: element.title
              },
              customize_daterange: {
                time_key1: '',
                time_key2: '',
                label: '',
                date_range: '',
                is_show: false
              }
            }
            element['dashboard_config']['customize_daterange'] = this.changesDate(this.chatConfig[i + addidx]['defualt_daterange'])
            element['chart_type'] = this.chartTypeData.find(x => x.display_name == element['chart_type']).chart_type;
            this.visualizeData.push(element);


            if (element['chart_type'] == 'pie' || element['chart_type'] == 'polarArea') {

              this.runPieChartQuery(i + addidx);

            } else if (element['chart_type'] == 'cards_panel') {
              this.runCardsPanelQuery(i + addidx);
            } else if (element['chart_type'] == 'radar') {
              this.runRadarChartQuery(i + addidx);
            } else if (element['chart_type'] == 'data_table') {
              this.runDataTableQuery(i + addidx);
            } else if (element['chart_type'] == 'horizontalBar') {
              this.runHorizontalBarQuery(i + addidx);
            } else {
              this.runChartQuery(i + addidx).then(res => {

              });
            }


          });
        });
      }
    }
    );
  }

  replacePanel(chartdetails, addidx) {
    const data = {
      isEdit: true,
      isSingle: true
    };

    const ref = this.dialogService.open(AdHocDashboardPopComponent, {
      data: data,
      header: 'Visualize Selection',
      contentStyle: { width: '60vw', overflow: 'auto' },
    });

    ref.onClose.subscribe((data: any[]) => {
      console.log('data', data);
      if (data.length > 0) {
        let req = {
          visualizeId: data
        }
        this.dataFetchServ.getVisualizeByMultipleIds(req).subscribe(data => {

          if (data['status'] === 0) {
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Server Error',
              detail: 'Please refresh the page'
            });
            console.log(data['status_msg']);
            return;
          }

          let VData = data['data'];

          VData.forEach((element, i) => {
            element['visualize_id'] = element['id'];
            this.chatConfig[addidx] = JSON.parse(element['chart_config']);
            this.keySpaces[i] = {
              field_config: JSON.parse(element['field_config']),
              api_url: element['api_url']
            };
            element['dashboard_config'] = this.visualizeData[addidx]['dashboard_config'];
            element['dashboard_config']['panel_title'] = {
              is_show: true,
              title: element.title
            }
            element['dashboard_config']['customize_daterange'] = this.changesDate(this.chatConfig[i]['defualt_daterange'])
            element['chart_type'] = this.chartTypeData.find(x => x.display_name == element['chart_type']).chart_type;
            this.visualizeData[addidx] = element;

            if (element['chart_type'] == 'pie' || element['chart_type'] == 'polarArea') {

              this.runPieChartQuery(addidx);

            } else if (element['chart_type'] == 'cards_panel') {
              this.runCardsPanelQuery(addidx);
            } else if (element['chart_type'] == 'radar') {
              this.runRadarChartQuery(addidx);
            } else if (element['chart_type'] == 'data_table') {
              this.runDataTableQuery(addidx);
            } else if (element['chart_type'] == 'horizontalBar') {
              this.runHorizontalBarQuery(addidx);
            } else {
              this.runChartQuery(addidx).then(res => {

              });
            }
          });
        });
      }
    }
    );
  }
  tabularData(chartdetails) {
    let field_config = JSON.parse(chartdetails['field_config']);
    let api_url = chartdetails['api_url'];
    let name = chartdetails['name'];
    let req = JSON.parse(chartdetails['api_request']);
    let fieldslistData = [];
    req['dashboardDetails']['dimensions'].forEach(field => {
      fieldslistData.push(field_config.find(x => x.field == field));
    });
    for (const key in req['dashboardDetails']['metrics'][0]) {
      const fieldData = field_config.find(x => x.field == key);
      req['dashboardDetails']['metrics'][0][key].forEach(element => {
        fieldData['field'] = key + '_' + element;
        fieldslistData.push(fieldData);
      });
    }
    let timeKeyFilter = {};
    if (chartdetails['dashboard_config']['customize_daterange']['is_show']) {
      timeKeyFilter = {
        time_key1: chartdetails['dashboard_config']['customize_daterange']['time_key1'],
        time_key2: chartdetails['dashboard_config']['customize_daterange']['time_key2']
      };
    } else {
      timeKeyFilter = this.filtersApplied['timeKeyFilter'];
    }
    req['timeKeyFilter'] = timeKeyFilter;
    const ref = this.dialogService.open(TabularDataPopupComponent, {
      data: {
        request: req,
        fieldslist: fieldslistData,
        view: 'tabular',
        exportRequest: this.exportRequest,
        title: chartdetails.title,
        api_url: api_url,
        name: name,
        logo_url:chartdetails['logo_url'],
        backend_url:chartdetails['backend_url'],

      },
      header: chartdetails.title,
      contentStyle: { width: '80vw', overflow: 'auto' },
    });

    ref.onClose.subscribe((data: string) => {
    });
  }

  fullScreenData(chartJson, chartdetails) {

    const ref = this.dialogService.open(TabularDataPopupComponent, {
      data: {
        chartJson: chartJson,
        view: 'graphical',
        title: chartdetails.title,
        chart_type: chartdetails.chart_type
      },
      header: chartdetails.title,
      contentStyle: { width: '80vw', overflow: 'auto' },
    });

    ref.onClose.subscribe((data: string) => {
    });
  }

  customDateRange(chartdetails, index) {
    console.log('chartdetails', chartdetails)
    let detail = this.libServ.deepCopy(chartdetails['dashboard_config']['customize_daterange']);
    const ref = this.dialogService.open(CustomDateRangeComponent, {
      data: detail,
      header: 'Customize Date Range',
      contentStyle: { width: '40vw', overflow: 'auto' },
    });

    ref.onClose.subscribe((data: object) => {
      if (data) {
        this.visualizeData[index]['dashboard_config']['customize_daterange'] = data;
        if (this.visualizeData[index]['chart_type'] == 'pie' || this.visualizeData[index]['chart_type'] == 'polarArea') {

          this.runPieChartQuery(index);

        } else if (this.visualizeData[index]['chart_type'] == 'cards_panel') {
          this.runCardsPanelQuery(index);
        } else if (this.visualizeData[index]['chart_type'] == 'radar') {
          this.runRadarChartQuery(index);
        } else if (this.visualizeData[index]['chart_type'] == 'data_table') {
          this.runDataTableQuery(index);
        } else if (this.visualizeData[index]['chart_type'] == 'horizontalBar') {
          this.runHorizontalBarQuery(index);
        } else {
          this.runChartQuery(index).then(res => {

          });
        }
      }
    });
  }
  costomizePanel(chartdetails, index) {
    let detail = this.libServ.deepCopy(chartdetails['dashboard_config']['panel_title']);
    const ref = this.dialogService.open(CustomPanelComponent, {
      data: detail,
      header: 'Customise Panel',
      contentStyle: { width: '40vw', overflow: 'auto' },
    });

    ref.onClose.subscribe((data: object) => {
      this.visualizeData[index]['dashboard_config']['panel_title'] = data;
    });
  }

  deleteFromDashboard(index) {
    this.visualizeData.splice(index, 1);
  }
  changesDate(type) {
    let customize_daterange = {
      time_key1: '',
      time_key2: '',
      label: '',
      date_range: '',
      is_show: false
    }

    switch (type) {
      case 'today':
        {

          customize_daterange['date_range'] = 'today';
          customize_daterange['time_key1'] = moment().format('YYYYMMDD');
          customize_daterange['time_key2'] = moment().format('YYYYMMDD');
          customize_daterange['label'] = 'Today';
          customize_daterange['is_show'] = true;
        }
        break;

      case 'yesterday':
        {
          customize_daterange['date_range'] = 'yesterday';
          customize_daterange['time_key1'] = moment().subtract(1, 'days').format('YYYYMMDD');
          customize_daterange['time_key2'] = moment().subtract(1, 'days').format('YYYYMMDD');
          customize_daterange['label'] = 'Yesterday';
          customize_daterange['is_show'] = true;
        }
        break;
      case 'last_7_days':
        {
          customize_daterange['date_range'] = 'last_7_days';
          customize_daterange['time_key1'] = moment().subtract(7, 'days').format('YYYYMMDD');
          customize_daterange['time_key2'] = moment().subtract(1, 'days').format('YYYYMMDD');
          customize_daterange['label'] = 'Last 7 days';
          customize_daterange['is_show'] = true;

        }
        break;
      case 'last_30_days':
        {
          customize_daterange['date_range'] = 'last_30_days';
          customize_daterange['time_key1'] = moment().subtract(30, 'days').format('YYYYMMDD');
          customize_daterange['time_key2'] = moment().subtract(1, 'days').format('YYYYMMDD');
          customize_daterange['label'] = 'Last 30 days';
          customize_daterange['is_show'] = true;
        }
        break;
      case 'this_month':
        {
          customize_daterange['date_range'] = 'this_month';
          customize_daterange['time_key1'] = moment().startOf('month').format('YYYYMMDD');
          customize_daterange['time_key2'] = moment().format('YYYYMMDD');
          customize_daterange['label'] = 'This Month';
          customize_daterange['is_show'] = true;
        }
        break;
      case 'last_month':
        {
          customize_daterange['date_range'] = 'last_month';
          customize_daterange['time_key1'] = moment().subtract(1, 'months').startOf('month').format('YYYYMMDD');
          customize_daterange['time_key2'] = moment().subtract(1, 'months').endOf('month').format('YYYYMMDD');
          customize_daterange['label'] = 'Last Month';
          customize_daterange['is_show'] = true;
        }
        break;

      case 'last_week':
        {

          customize_daterange['date_range'] = 'last_week';
          customize_daterange['time_key1'] = moment().subtract(1, 'weeks').startOf('isoWeek').format('YYYYMMDD');
          customize_daterange['time_key2'] = moment().subtract(1, 'weeks').endOf('isoWeek').format('YYYYMMDD');
          customize_daterange['label'] = 'Last Week';
          customize_daterange['is_show'] = true;
        }
        break;
    }
    return customize_daterange;
  }

  dateToCustomizeDaterange(startDate, endDate) {
    let customize_daterange = {
      time_key1: '',
      time_key2: '',
      label: '',
      date_range: '',
      is_show: false
    }

    if (moment().format('YYYYMMDD') == startDate && moment().format('YYYYMMDD') == endDate) {

      customize_daterange['date_range'] = 'today';
      customize_daterange['time_key1'] = moment().format('YYYYMMDD');
      customize_daterange['time_key2'] = moment().format('YYYYMMDD');
      customize_daterange['label'] = 'Today';
      customize_daterange['is_show'] = true;
    }
    else if (moment().subtract(1, 'days').format('YYYYMMDD') == startDate
      && moment().subtract(1, 'days').format('YYYYMMDD') == endDate) {
      customize_daterange['date_range'] = 'yesterday';
      customize_daterange['time_key1'] = moment().subtract(1, 'days').format('YYYYMMDD');
      customize_daterange['time_key2'] = moment().subtract(1, 'days').format('YYYYMMDD');
      customize_daterange['label'] = 'Yesterday';
      customize_daterange['is_show'] = true;
    }
    else if (moment().subtract(7, 'days').format('YYYYMMDD') == startDate
      && moment().subtract(1, 'days').format('YYYYMMDD') == endDate) {
      customize_daterange['date_range'] = 'last_7_days';
      customize_daterange['time_key1'] = moment().subtract(7, 'days').format('YYYYMMDD');
      customize_daterange['time_key2'] = moment().subtract(1, 'days').format('YYYYMMDD');
      customize_daterange['label'] = 'Last 7 days';
      customize_daterange['is_show'] = true;

    }
    else if (moment().subtract(30, 'days').format('YYYYMMDD') == startDate
      && moment().subtract(1, 'days').format('YYYYMMDD') == endDate) {
      customize_daterange['date_range'] = 'last_30_days';
      customize_daterange['time_key1'] = moment().subtract(30, 'days').format('YYYYMMDD');
      customize_daterange['time_key2'] = moment().subtract(1, 'days').format('YYYYMMDD');
      customize_daterange['label'] = 'Last 30 days';
      customize_daterange['is_show'] = true;
    }
    else if (moment().startOf('month').format('YYYYMMDD') == startDate
      && moment().format('YYYYMMDD') == endDate) {
      customize_daterange['date_range'] = 'this_month';
      customize_daterange['time_key1'] = moment().startOf('month').format('YYYYMMDD');
      customize_daterange['time_key2'] = moment().format('YYYYMMDD');
      customize_daterange['label'] = 'This Month';
      customize_daterange['is_show'] = true;
    }
    else if (moment().subtract(1, 'months').startOf('month').format('YYYYMMDD') == startDate
      && moment().subtract(1, 'months').endOf('month').format('YYYYMMDD') == endDate) {
      customize_daterange['date_range'] = 'last_month';
      customize_daterange['time_key1'] = moment().subtract(1, 'months').startOf('month').format('YYYYMMDD');
      customize_daterange['time_key2'] = moment().subtract(1, 'months').endOf('month').format('YYYYMMDD');
      customize_daterange['label'] = 'Last Month';
      customize_daterange['is_show'] = true;
    }
    else if (moment().subtract(1, 'weeks').startOf('isoWeek').format('YYYYMMDD') == startDate
      && moment().subtract(1, 'weeks').endOf('isoWeek').format('YYYYMMDD') == endDate) {

      customize_daterange['date_range'] = 'last_week';
      customize_daterange['time_key1'] = moment().subtract(1, 'weeks').startOf('isoWeek').format('YYYYMMDD');
      customize_daterange['time_key2'] = moment().subtract(1, 'weeks').endOf('isoWeek').format('YYYYMMDD');
      customize_daterange['label'] = 'Last Week';
      customize_daterange['is_show'] = true;
    }
    else {

      customize_daterange['date_range'] = 'custom';
      customize_daterange['time_key1'] = startDate;
      customize_daterange['time_key2'] = endDate;
      customize_daterange['label'] = `${moment(startDate, 'YYYYMMDD').format('MM-DD-YYYY')} to ${moment(endDate, 'YYYYMMDD').format('MM-DD-YYYY')}`;
      customize_daterange['is_show'] = true;

    }
    return customize_daterange;
  }


  editVisualization(chartdetails) {
    localStorage.setItem('chartType', 'updateInDashboard');
    localStorage.setItem('keyspaceId', chartdetails['visualize_id']);
    this.router.navigate(['/visualize']);

  }
  onMoving(event, id, index) {
    var div = document.getElementById(id);
    var divOffset = this.offset(div);
    let listOfElement = [];
    this.visualizeData.forEach((element, i) => {
      if (i != index) {
        let ele = document.getElementById('dragElement_' + i);
        listOfElement.push(this.offset(ele))
      }
    });
    let findElement = listOfElement.map(x => x.left).reduce((a, b) => {
      return Math.abs(b - divOffset.left) < Math.abs(a - divOffset.left) ? b : a;
    });
    let findElementTop = listOfElement.map(x => x.top).reduce((a, b) => {
      return Math.abs(b - divOffset.top) < Math.abs(a - divOffset.top) ? b : a;
    });

    let final = listOfElement.find(x => x.left == findElement && x.top == findElementTop);
    let scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
      scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    var element = document.elementFromPoint(divOffset.left, divOffset.top);
    if (element) {
      let findElementid = element.id.split('_')[1];
      console.log('findElementid', findElementid)
      if (findElementid) {
        // let temp = this.visualizeData[findElementid];
        // this.visualizeData[findElementid] = this.visualizeData[index];
        // this.visualizeData[index] = temp;
        this.swap(findElementid, index);
        // this.swap(this.chatConfig[findElementid], this.chatConfig[index]);

      }
    }
    console.log(divOffset, findElement, findElementTop);
    console.log('moving', event, listOfElement, final);
    console.log('finding', element)
  }


  swap(first, second) {

    let tempChart = this.chatConfig[first];
    this.chatConfig[first] = this.chatConfig[second];
    this.chatConfig[second] = tempChart;

    let tempChartjson = this.chartJson[first];
    this.chartJson[first] = this.chartJson[second];
    this.chartJson[second] = tempChartjson;

    let tempkeySpaces = this.keySpaces[first];
    this.keySpaces[first] = this.keySpaces[second];
    this.keySpaces[second] = tempkeySpaces;

    let temp = this.visualizeData[first];
    this.visualizeData[first] = this.visualizeData[second];
    this.visualizeData[second] = temp;




    if (this.visualizeData[first]['chart_type'] == 'pie' || this.visualizeData[first]['chart_type'] == 'polarArea') {

      this.runPieChartQuery(first);

    } else if (this.visualizeData[first]['chart_type'] == 'cards_panel') {
      this.runCardsPanelQuery(first);
    } else if (this.visualizeData[first]['chart_type'] == 'radar') {
      this.runRadarChartQuery(first);
    } else if (this.visualizeData[first]['chart_type'] == 'data_table') {
      this.runDataTableQuery(first);
    } else if (this.visualizeData[first]['chart_type'] == 'horizontalBar') {
      this.runHorizontalBarQuery(first);
    } else {
      this.runChartQuery(first).then(res => {

      });
    }


    if (this.visualizeData[second]['chart_type'] == 'pie' || this.visualizeData[second]['chart_type'] == 'polarArea') {

      this.runPieChartQuery(second);

    } else if (this.visualizeData[second]['chart_type'] == 'cards_panel') {
      this.runCardsPanelQuery(second);
    } else if (this.visualizeData[second]['chart_type'] == 'radar') {
      this.runRadarChartQuery(second);
    } else if (this.visualizeData[second]['chart_type'] == 'data_table') {
      this.runDataTableQuery(second);
    } else if (this.visualizeData[second]['chart_type'] == 'horizontalBar') {
      this.runHorizontalBarQuery(second);
    } else {
      this.runChartQuery(second).then(res => {

      });
    }
    // if (this.showChart[first]) {
    //   this.showChart[first] = false;
    //   setTimeout(() => {
    //     this.showChart[first] = true;
    //   }, 0);

    // }


    // if (this.showChart[second]) {
    //   this.showChart[second] = false;
    //   setTimeout(() => {
    //     this.showChart[second] = true;
    //   }, 0);

    // }
  }

  offset(el) {
    var rect = el.getBoundingClientRect(),
      scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
      scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return {
      top: rect.top + scrollTop,
      left: rect.left + scrollLeft
    }
  }
  onResizeStop(event, chartdetails, indx) {
    // this.isResize = true;
    console.log('hhhhhh', event)
    let dropdownElement = (document.querySelector('#dragElement_' + indx + ' .dropdown')['offsetHeight'] ? document.querySelector('#dragElement_' + indx + ' .dropdown')['offsetHeight'] : 23) - 23;
    console.log('dropdownElement', dropdownElement);
    chartdetails['dashboard_config']['width'] = this.getWidth(event.size.width);
    chartdetails['dashboard_config']['height'] = event.size.height;
    if (chartdetails['chart_type'] == 'data_table') {
      this.adjustTableHeight(indx);
    } else {
      this.chartJson[indx]['chartHeight'] = event.size.height - (100 + dropdownElement) + 'px'
      console.log('hhhhhh', event, this.chartJson);
      if (this.showChart[indx]) {
        this.showChart[indx] = false;
        setTimeout(() => {
          this.showChart[indx] = true;
        }, 0);
      }
    }


    console.log('visualizeData', this.visualizeData);
    // this.position = event.position;
  }
  getTitle(metrics, idx) {
    if (metrics['field'] != 'countofdimension') {
      return metrics['custom_label'] == '' ? this.getAggrigationData(metrics['aggregation'], 'label') + ' of ' + this.getfielddata(metrics['field'], 'displayName', idx) : metrics['custom_label']
    } else {
      return metrics['custom_label'] == '' ? this.getfielddata(metrics['field'], 'displayName', idx) : metrics['custom_label'];
    }
  }
  getAggrigationData(field, returntype) {
    let groupedAggregation = [
      { label: 'Average', value: 'mean' },
      { label: 'Count', value: 'count' },
      { label: 'Max', value: 'max' },
      // { label: 'Median', value: 'Median' },
      { label: 'Min', value: 'min' },
      // { label: 'Percentile Ranks', value: 'Percentile Ranks' },
      // { label: 'Percentiles', value: 'Percentiles' },
      { label: 'Standard Deviation', value: 'std' },
      { label: 'Sum', value: 'sum' },
      { label: 'Sum', value: 'Sum' },
      // { label: 'Top Hit', value: 'Top Hit' },
      // { label: 'Unique Count', value: 'Unique Count' }
      { label: 'Variance', value: 'var' },
      // { label: 'Size', value: 'size' }
      { label: 'Date Histogram', value: 'Date Histogram' },
      { label: 'Terms', value: 'Terms' }
    ];
    if (field) {
      return groupedAggregation.find(x => x.value == field)[returntype];
    } else {
      return '';
    }
  }
  exportTable(fileFormat, chartdetails, idx) {
    console.log('aaaa', this.keySpaces, idx, chartdetails)
    if (this.exportRequest['sendEmail'].length > 0) {
      if (this.chartJson[idx]['flatTableData'].length == 0) {
        this.confirmationService.confirm({
          message:
            'Your report is not generated because the data is not available for selected date range. Please select different date range to see the data.',
          header: 'Information',
          icon: 'pi pi-exclamation-triangle',
          accept: () => { },
          reject: () => { }
        });
      } else {
        this.toastService.displayToast({
          severity: 'info',
          summary: 'Export Report',
          detail:
            'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
          life: 10000
        });
        const columnDefs = this.chartJson[idx]['flatTableJson']['globalFilterFields'];
        const req = this.libServ.deepCopy(JSON.parse(chartdetails['api_request']));
        req['limit'] = '';
        req['offset'] = '';
        req['isTable'] = false;
        let timeKeyFilter = {};
        if (chartdetails['dashboard_config']['customize_daterange']['is_show']) {
          timeKeyFilter = {
            time_key1: chartdetails['dashboard_config']['customize_daterange']['time_key1'],
            time_key2: chartdetails['dashboard_config']['customize_daterange']['time_key2']
          };
        } else {
          timeKeyFilter = this.filtersApplied['timeKeyFilter'];
        }
        req['timeKeyFilter'] = timeKeyFilter;
        const sheetDetails = {};
        sheetDetails['columnDef'] = columnDefs;
        sheetDetails['data'] = [];
        sheetDetails['sheetName'] = 'Dashboard Data';
        sheetDetails['isRequest'] = true;
        sheetDetails['request'] = {
          url: this.keySpaces[idx]['export_url'],
          method: 'POST',
          param: req
        };
        sheetDetails['disclaimer'] = [
          {
            position: 'bottom',
            label: 'Note: Data present in the table may vary over a period of time.',
            color: '#000000'
          }
        ];
        sheetDetails['totalFooter'] = {
          available: false,
          custom: false
        };
        sheetDetails['tableTitle'] = {
          available: false,
          label: chartdetails['dashboard_config']['panel_title']['title']
        };
        sheetDetails['image'] = [
          {
            available: true,
            path: environment.exportConfig.exportLogo,
            position: 'top'
          }
        ];
        const sheetDetailsarray = [];
        sheetDetailsarray.push(sheetDetails);
        this.exportRequest['sheetDetails'] = <SheetDetails[]>(
          sheetDetailsarray
        );
        this.exportRequest['fileName'] =
          chartdetails['dashboard_config']['panel_title']['title'] +
          moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
        this.exportRequest['exportFormat'] = fileFormat;
        this.exportRequest['exportConfig'] = environment.exportConfig;
        this.exportRequest['exportAppName'] = this.keySpaces[idx]['name'];
        // console.log('exportreport', this.exportRequest);
        // return false;
        this.dataFetchServ
          .getExportReportData(this.exportRequest,this.keySpaces[idx]['backend_url'])
          .subscribe(response => {
            console.log(response);
          });
      }
    } else {
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Export Report',
        detail:
          'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
        life: 10000
      });
    }
    return false;
  }
  getTableJson(chartjsondata, chart, index) {
    chartjsondata['chart'] = chart;
    chartjsondata['index'] = index;
    return chartjsondata;

  }
  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }
}
