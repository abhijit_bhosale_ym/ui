import { Component, OnInit, OnDestroy } from '@angular/core';
import { SelectItemGroup, ConfirmationService } from 'primeng/api';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { FetchApiDataService } from '../../fetch-ad-hoc-dashboard-api-data.service';
import { CommonLibService } from 'src/app/_services';
import { DynamicDialogConfig, DynamicDialogRef, DialogService } from 'primeng/dynamicdialog';
@Component({
  selector: 'ym-dashboard-details',
  templateUrl: './dashboard-details.component.html',
  styleUrls: ['./dashboard-details.component.scss']
})
export class DashboardDetailsComponent implements OnInit {
  // private appConfigObs: Subscription;

  appConfig: object = {};
  registerForm: FormGroup;
  isCreateRole = false;
  isCreateTeam = false;
  groupedYears: SelectItemGroup[];
  groupedMediaGroups: SelectItemGroup[];
  groupedProperty: SelectItemGroup[];
  fileName = '';
  paymentMethodLists: any[];
  fileData: File;
  userDetails: {
    name: '';
  };
  RowData: object = {};
  constructor(
    private formBuilder: FormBuilder,
    public router: Router,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private confirmationService: ConfirmationService,
    private libServ: CommonLibService,
    private config: DynamicDialogConfig,
    private dynamicDialogRef: DynamicDialogRef
  ) {
    this.RowData = this.config.data;
    // this.paymentMethodLists = this.RowData['paymentMethodLists'];
  }

  ngOnInit() {

    this.registerForm = this.formBuilder.group({
      chartTitle: [this.RowData['title'], [Validators.required]],
      description: [this.RowData['description']]
    });



  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {

    this.RowData['title'] = this.registerForm.value.chartTitle;
    this.RowData['description'] = this.registerForm.value.description;
    if (this.RowData['isUpdate']) {
      this.dataFetchServ.updateDashboard(this.RowData).subscribe(data => {
        if (data['status']) {
          this.toastService.displayToast({
            severity: 'success',
            summary: 'Dashboard Update',
            detail: 'Dashboard updated successfully'
          });
          //localStorage.setItem('visualizeIds', [this.RowData['id']].toString());
          // this.router.navigate(['/ad-hoc-dashboard']);
          this.dynamicDialogRef.close(null);

        } else {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
        }
      });

    } else {
      this.dataFetchServ.saveDashboard(this.RowData).subscribe(data => {
        if (data['status']) {
          this.toastService.displayToast({
            severity: 'success',
            summary: 'Dashboard Save',
            detail: 'Dashboard save successfully'
          });
          if (localStorage.getItem('type')) {
            localStorage.removeItem('type');
          }
          if (localStorage.getItem('visualizeIds')) {
            localStorage.removeItem('visualizeIds');
          }

          this.dialogService.dialogComponentRef.destroy();
          this.router.navigate(['/ad-hoc-dashboard']);
        } else {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });

        }
      });

    }


  }

  goBack() {
    // this.router.navigate(['/users']);
    this.dialogService.dialogComponentRef.destroy();
  }

}
