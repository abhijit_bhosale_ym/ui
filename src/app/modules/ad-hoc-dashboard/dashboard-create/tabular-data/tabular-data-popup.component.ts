import { Component, OnInit } from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { FetchApiDataService } from '../../fetch-ad-hoc-dashboard-api-data.service';
import * as moment from 'moment';
import { TreeNode } from 'primeng/api';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-tabular-data-popup',
  templateUrl: './tabular-data-popup.component.html',
  styleUrls: ['./tabular-data-popup.component.scss']
})
export class TabularDataPopupComponent implements OnInit {
  chartJson: object;
  showChart = false;
  noDataChart = true;
  filtersApplied: object;
  appName: string;
  email: [];
  tabChangedFlag = false;
  graphData: object;
  tableDataReq: object;
  tableData: TreeNode[];
  tableColumnDef: any[];
  dimColDef: any[];
  tableJson: Object;
  exportRequest: ExportRequest = <ExportRequest>{};

  constructor(
    private libServ: CommonLibService,
    private formatNumPipe: FormatNumPipe,
    private dataFetchServ: FetchApiDataService,
    private config: DynamicDialogConfig,
    private exportService: ExportdataService,
    private toastService: ToastService
  ) {
    this.graphData = this.config.data;

  }

  ngOnInit() {

    this.dimColDef = [];
    let req = this.graphData['request'];
    if (this.graphData['view'] == 'tabular') {
      this.exportRequest = this.graphData['exportRequest'];
      this.graphData['fieldslist'].forEach(element => {
        this.dimColDef.push({
          field: element.field,
          displayName: element.displayName,
          format: element.format,
          width: '150',
          exportConfig: {
            format: element.format == '$' ? 'currency' : element.format,
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          }
        })
      });


      this.tableJson = {
        page_size: 10,
        page: 0,
        lazy: false,
        loading: false,
        export: true,
        sortMode: 'multiple',
        resizableColumns: true,
        columnResizeMode: 'fit',
        reorderableColumns: true,
        scrollHeight: '400px',
        totalRecords: 1000,
        columns: this.dimColDef.slice(0),
        selectedColumns: this.dimColDef.slice(0),
        // frozenCols: [this.dimColDef[0]],
        scrollable: true,
        selectionMode: 'multiple',
        selectedColsModal: [],
        selectionDataKey: 'name',
        metaKeySelection: true,
        showHideCols: false,
        overallSearch: false,
        columnSearch: false
      };

      this.loadTable(req);
    } else {
      this.chartJson = this.graphData['chartJson'];
      this.chartJson['chartHeight'] = '450px';
      this.showChart = true;
      this.noDataChart = false;
      if (this.graphData['chart_type'] == 'data_table') {
        setTimeout(() => {
          this.adjustHeight();
        }, 0);
      }
    }
  }

  loadTable(req) {
    this.tableJson['loading'] = true;


    this.dataFetchServ.getChartData(req, this.graphData['api_url']).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }
      const tableData = data['data'];

      const arr = [];
      tableData.forEach((row: object) => {
        const obj = {
          data: row
        };
        arr.push(obj);
      });

      this.tableData = <TreeNode[]>arr;
      this.tableJson['totalRecords'] = tableData.length;
      this.tableJson['loading'] = false;
    });
  }

  exportTable(fileFormat) {
    const req = this.graphData['request'];
    // this.exportRequest['sendEmail'] = this.email;
    // this.exportRequest['appName'] = this.appName;
    if (this.exportRequest['sendEmail'].length > 0) {
      this.toastService.displayToast({
        severity: 'info',
        summary: 'Export Report',
        detail:
          'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
        life: 10000
      });
      const columnDefs = this.libServ.deepCopy(this.dimColDef);


      req['isTable'] = false;
      this.dataFetchServ.getChartData(req, this.graphData['api_url']).subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        const tableData = data['data'];
        const sheetDetails = {};
        sheetDetails['columnDef'] = columnDefs;
        sheetDetails['data'] = tableData;
        sheetDetails['sheetName'] = 'Data';
        sheetDetails['isRequest'] = false;
        sheetDetails['request'] = {
          url: '',
          method: '',
          param: {}
        };
        sheetDetails['disclaimer'] = [
          {
            position: 'top',
            label:
              'Data Present in below table is not final and may varies over period of time',
            color: '#3A37CF'
          },
          {
            position: 'bottom',
            label: 'Thank You',
            color: '#EC6A15'
          }
        ];
        sheetDetails['totalFooter'] = {
          available: false,
          custom: false
        };
        sheetDetails['tableTitle'] = {
          available: true,
          label: this.graphData['title']
        };
        sheetDetails['image'] = [
          {
            available: true,
            path: environment.exportConfig.exportLogo,
            position: 'top'
          }
        ];
        const sheetDetailsarray = [];
        sheetDetailsarray.push(sheetDetails);
        this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
        this.exportRequest['fileName'] = ' Visualise tabular Data ' +
          moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
        this.exportRequest['exportFormat'] = fileFormat;
        this.exportRequest['exportConfig'] = environment.exportConfig;
        this.exportRequest['exportAppName'] = this.graphData['name'];
        this.dataFetchServ
          .getExportReportData(this.exportRequest,this.graphData['backend_url'])
          .subscribe(response => {
            console.log('response of table', response);
          });
      });
    } else {
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Export Report',
        detail:
          'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
        life: 10000
      });
    }
    return false;
  }

  isHiddenColumn(col: Object) { }

  adjustHeight() {
    const tableTh: any = document.querySelectorAll('.table-head');
    const tableTh1 = document.querySelector('.tabular-view .ui-treetable-caption');
    const scrollHeight1: any = document.querySelectorAll('body .tabular-view .ui-treetable-scrollable-body');
    let thArray = [40];
    for (const th of tableTh) {
      thArray.push(th.offsetHeight);
    }
    const maxheight = thArray.reduce((a, b) => Math.max(a, b));
    let extraHeight = 143;
    if (this.chartJson['flatTableJson']['columnSearch']) {
      extraHeight = 184;
    }
    this.chartJson['flatTableJson']['scrollHeight'] = (526 - extraHeight - maxheight) + 'px !important';
    setTimeout(() => {
      for (const scrollHeight of scrollHeight1) {
        (scrollHeight as HTMLElement).style.cssText = 'max-height:' + (526 - extraHeight - maxheight) + 'px !important';
      }
      for (const th of tableTh) {
        (th as HTMLElement).style.cssText = 'height:' + maxheight + 'px !important';
      }
    }, 0);
  }
  onColumnResize() {
    this.adjustHeight();
  }

  chartSelected(e) { }
}
