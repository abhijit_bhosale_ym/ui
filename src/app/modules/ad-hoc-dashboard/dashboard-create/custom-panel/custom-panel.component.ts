import { Component, OnInit, OnDestroy } from '@angular/core';
import { SelectItemGroup, ConfirmationService } from 'primeng/api';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { FetchApiDataService } from '../../fetch-ad-hoc-dashboard-api-data.service';
import { CommonLibService } from 'src/app/_services';
import { DynamicDialogConfig, DynamicDialogRef, DialogService } from 'primeng/dynamicdialog';
@Component({
  selector: 'ym-custom-panel',
  templateUrl: './custom-panel.component.html',
  styleUrls: ['./custom-panel.component.scss']
})
export class CustomPanelComponent implements OnInit {
  registerForm: FormGroup;
  RowData: object = {};
  isPanelTitle = false;
  constructor(
    private formBuilder: FormBuilder,
    public router: Router,
    private dialogService: DialogService,
    private config: DynamicDialogConfig,
    private dynamicDialogRef: DynamicDialogRef
  ) {
    this.RowData = this.config.data;
  }

  ngOnInit() {
    console.log('rowdata', this.RowData)
    this.isPanelTitle = this.RowData['is_show'];
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {

    this.RowData['is_show'] = this.isPanelTitle;
    this.dynamicDialogRef.close(this.RowData);

  }

  goBack() {
    // this.router.navigate(['/users']);
    this.dialogService.dialogComponentRef.destroy();
  }

}
