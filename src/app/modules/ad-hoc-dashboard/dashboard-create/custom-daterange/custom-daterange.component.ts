import { Component, OnInit, OnDestroy } from '@angular/core';
import { SelectItemGroup, ConfirmationService } from 'primeng/api';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { FetchApiDataService } from '../../fetch-ad-hoc-dashboard-api-data.service';
import { CommonLibService } from 'src/app/_services';
import { DynamicDialogConfig, DynamicDialogRef, DialogService } from 'primeng/dynamicdialog';
import * as moment from 'moment';
@Component({
  selector: 'ym-custom-daterange',
  templateUrl: './custom-daterange.component.html',
  styleUrls: ['./custom-daterange.component.scss']
})
export class CustomDateRangeComponent implements OnInit {
  // private appConfigObs: Subscription;

  appConfig: object = {};
  registerForm: FormGroup;
  today = new Date();
  minDate = new Date();
  dateRangeOptions: any[];
  selectedRange: object = {};
  RowData: object = {};
  selectedDate: Date[];
  date1: Date;
  customize_daterange: object = {};
  isDisabled = false;
  isUpdate = false;
  constructor(
    private formBuilder: FormBuilder,
    public router: Router,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private confirmationService: ConfirmationService,
    private libServ: CommonLibService,
    private config: DynamicDialogConfig,
    private dynamicDialogRef: DynamicDialogRef
  ) {
    this.customize_daterange = this.config.data;
    // this.paymentMethodLists = this.RowData['paymentMethodLists'];
  }

  ngOnInit() {

    this.dateRangeOptions = [

      { key: 'today', label: 'Today' },
      { key: 'yesterday', label: 'Yesterday' },
      { key: 'last_7_days', label: 'Last 7 days' },
      { key: 'last_week', label: 'Last Week' },
      { key: 'last_month', label: 'Last Month' },
      { key: 'last_30_days', label: 'Last 30 days' },
      { key: 'this_month', label: 'This Month' },
      { key: 'custom', label: 'Custom' },
    ];

    this.selectedRange = this.dateRangeOptions[
      this.dateRangeOptions.findIndex(
        item => item['key'] == this.customize_daterange['date_range']
      )
    ];
    this.isUpdate = this.customize_daterange['is_show'];
    this.changeDate();
    if (this.selectedRange['key'] == 'custom') {
      this.selectedDate = [moment(this.customize_daterange['time_key1'], 'YYYYMMDD').toDate(), moment(this.customize_daterange['time_key2'], 'YYYYMMDD').toDate()]
      this.isDisabled = false;
    }
    // this.rangeDates2 = [];
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {


    if (this.selectedRange['key'] == 'custom') {

      this.customize_daterange['date_range'] = 'custom';
      this.customize_daterange['time_key1'] = moment(this.selectedDate[0]).format('YYYYMMDD');
      this.customize_daterange['time_key2'] = moment(this.selectedDate[1]).format('YYYYMMDD');
      this.customize_daterange['label'] = `${moment(this.selectedDate[0]).format('MM-DD-YYYY')} to ${moment(this.selectedDate[1]).format('MM-DD-YYYY')}`;
      this.customize_daterange['is_show'] = true;
    }
    console.log('select', this.customize_daterange)
    this.dynamicDialogRef.close(this.customize_daterange);
  }
  onDateSelected() {
    document.body.click();
    if (this.selectedDate) {
      this.isDisabled = false;
    }
  }
  changeDate() {
    this.isDisabled = false;
    console.log('select', this.selectedRange)
    switch (this.selectedRange['key']) {
      case 'today':
        {
          this.customize_daterange['date_range'] = 'today';
          this.customize_daterange['time_key1'] = moment().format('YYYYMMDD');
          this.customize_daterange['time_key2'] = moment().format('YYYYMMDD');
          this.customize_daterange['label'] = 'Today';
          this.customize_daterange['is_show'] = true;
        }
        break;

      case 'yesterday':
        {
          this.customize_daterange['date_range'] = 'yesterday';
          this.customize_daterange['time_key1'] = moment().subtract(1, 'days').format('YYYYMMDD');
          this.customize_daterange['time_key2'] = moment().subtract(1, 'days').format('YYYYMMDD');
          this.customize_daterange['label'] = 'Yesterday';
          this.customize_daterange['is_show'] = true;
        }
        break;
      case 'last_7_days':
        {
          this.customize_daterange['date_range'] = 'last_7_days';
          this.customize_daterange['time_key1'] = moment().subtract(7, 'days').format('YYYYMMDD');
          this.customize_daterange['time_key2'] = moment().subtract(1, 'days').format('YYYYMMDD');
          this.customize_daterange['label'] = 'Last 7 days';
          this.customize_daterange['is_show'] = true;

        }
        break;
      case 'last_30_days':
        {
          this.customize_daterange['date_range'] = 'last_30_days';
          this.customize_daterange['time_key1'] = moment().subtract(30, 'days').format('YYYYMMDD');
          this.customize_daterange['time_key2'] = moment().subtract(1, 'days').format('YYYYMMDD');
          this.customize_daterange['label'] = 'Last 30 days';
          this.customize_daterange['is_show'] = true;
        }
        break;
      case 'this_month':
        {
          this.customize_daterange['date_range'] = 'this_month';
          this.customize_daterange['time_key1'] = moment().startOf('month').format('YYYYMMDD');
          this.customize_daterange['time_key2'] = moment().format('YYYYMMDD');
          this.customize_daterange['label'] = 'This Month';
          this.customize_daterange['is_show'] = true;
        }
        break;
      case 'last_month':
        {
          this.customize_daterange['date_range'] = 'last_month';
          this.customize_daterange['time_key1'] = moment().subtract(1, 'months').startOf('month').format('YYYYMMDD');
          this.customize_daterange['time_key2'] = moment().subtract(1, 'months').endOf('month').format('YYYYMMDD');
          this.customize_daterange['label'] = 'Last Month';
          this.customize_daterange['is_show'] = true;
        }
        break;

      case 'last_week':
        {

          this.customize_daterange['date_range'] = 'last_week';
          this.customize_daterange['time_key1'] = moment().subtract(1, 'weeks').startOf('isoWeek').format('YYYYMMDD');
          this.customize_daterange['time_key2'] = moment().subtract(1, 'weeks').endOf('isoWeek').format('YYYYMMDD');
          this.customize_daterange['label'] = 'Last Week';
          this.customize_daterange['is_show'] = true;
        }
        break;
      case 'custom':
        {

          this.isDisabled = true;
        }
        break;
    }
  }
  removeRange() {
    this.customize_daterange['is_show'] = false;
    this.dynamicDialogRef.close(this.customize_daterange);
  }

  goBack() {
    // this.router.navigate(['/users']);
    this.dialogService.dialogComponentRef.destroy();
  }

}
