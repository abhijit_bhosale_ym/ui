import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListDashboardGraphComponent } from './list-dashboard-graph.component';

describe('ListDashboardGraphComponent', () => {
  let component: ListDashboardGraphComponent;
  let fixture: ComponentFixture<ListDashboardGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListDashboardGraphComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListDashboardGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
