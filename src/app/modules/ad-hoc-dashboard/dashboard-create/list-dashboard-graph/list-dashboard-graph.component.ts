import { Component, OnInit, OnDestroy } from '@angular/core';
import { SelectItemGroup, ConfirmationService, SelectItem } from 'primeng/api';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
// import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { FetchApiDataService } from '../../fetch-ad-hoc-dashboard-api-data.service';
// import { CommonLibService } from 'src/app/_services';
import { DynamicDialogConfig, DynamicDialogRef, DialogService } from 'primeng/dynamicdialog';
@Component({
  selector: 'ym-list-dashboard-graph',
  templateUrl: './list-dashboard-graph.component.html',
  styleUrls: ['./list-dashboard-graph.component.scss']
})
export class ListDashboardGraphComponent implements OnInit {
  // private appConfigObs: Subscription;

  appConfig: object = {};
  iskeyspace = false;
  RowData: any[];
  isSingle = false;
  chartType = [{
    chart_type: 'bar',
    display_name: 'Vertical Bar',
    icon_class: 'fas fa-poll',
    info: 'A bar chart or bar graph is a chart that presents Grouped data with rectangular bars with lengths proportional to the values that they represent.'
  }, {
    chart_type: 'line',
    display_name: 'Line',
    icon_class: 'fas fa-chart-line',
    info: 'A line chart or line graph is a type of chart which displays information as a series of data points called \'markers\' connected by straight line segments.'
  },
  {
    chart_type: 'area',
    display_name: 'Area',
    icon_class: 'fas fa-chart-area',
    info: 'Emphasize the quantity beneath a line chart'
  },
  {
    chart_type: 'pie',
    display_name: 'Pie',
    icon_class: 'fas fa-chart-pie',
    info: 'A doughnut chart is a variant of the pie chart, with a blank center allowing for additional information about the data as a whole to be included.'
  },
  {
    chart_type: 'cards_panel',
    display_name: 'Cards Panel',
    icon_class: 'fas fa-id-card',
    info: 'Display a calculation as a single number'
  },
  {
    chart_type: 'polarArea',
    display_name: 'Polar Area',
    icon_class: 'fas fa-radiation-alt',
    info: 'Polar area charts are similar to pie charts, but each segment has the same angle - the radius of the segment differs depending on the value.'
  },
  {
    chart_type: 'radar',
    display_name: 'Radar',
    icon_class: 'fas fa-podcast',
    info: 'A radar chart is a graphical method of displaying multivariate data in the form of a two-dimensional chart of three or more quantitative variables represented on axes starting from the same point.'
  },{
    chart_type: 'data_table',
    display_name: 'Data Table',
    icon_class: 'fas fa-table',
    info: 'Display value in table'
  }, {
    chart_type: 'horizontalBar',
    display_name: 'Horizontal Bar',
    icon_class: 'fas fa-poll-h',
    info: 'A bar chart or bar graph is a chart that presents Grouped data with rectangular bars with lengths proportional to the values that they represent.'
  }
  ];

  visualiseList: SelectItem[];
  selectedVisualiseList: any[];
  selectedVisualiseSingle: object;
  constructor(
    private formBuilder: FormBuilder,
    public router: Router,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    // private toastService: ToastService,
    private confirmationService: ConfirmationService,
    // private libServ: CommonLibService,
    private config: DynamicDialogConfig,
    private dynamicDialogRef: DynamicDialogRef,
    private currentActivatedRoute: ActivatedRoute,
  ) {
    this.RowData = this.config.data;
  }

  ngOnInit() {
    console.log('this.RowData', this.RowData)
    this.visualiseList = this.RowData;

  }
  back() {
    this.iskeyspace = false;
  }
  getVisaulizeData() {
    let datalist = this.RowData;
    this.visualiseList = [];
    datalist.forEach(element => {
      this.visualiseList.push({ label: element['dashboard_config'], value: element })
    });

  }
  getIconClass(obj) {
    return this.chartType.find(x => x.chart_type == obj.chart_type).icon_class;
  }
  onAdd() {
    this.dynamicDialogRef.close(this.selectedVisualiseList.map(x => x.id));
  }
  goBack() {
    // this.router.navigate(['/users']);
    this.dialogService.dialogComponentRef.destroy();
  }
  // ngOnDestroy() {
  //   if (this.appConfigObs && !this.appConfigObs.closed) {
  //     this.appConfigObs.unsubscribe();
  //   }
  // }
}
