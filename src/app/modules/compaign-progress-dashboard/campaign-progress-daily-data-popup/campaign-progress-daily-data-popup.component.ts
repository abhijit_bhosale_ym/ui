import { Component, OnInit } from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { FetchApiDataService } from '../fetch-api-data.service';
import * as moment from 'moment';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { DynamicDialogConfig } from 'primeng';
import { FormatNumPipe } from '../../../_pipes/number-format.pipe';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-campaign-progress-daily-data-popup',
  templateUrl: './campaign-progress-daily-data-popup.component.html',
  styleUrls: ['./campaign-progress-daily-data-popup.component.scss']
})
export class CampaignProgressDailyDataPopupComponent implements OnInit {
  request: object;
  appName: string;
  email: [];
  tabChangedFlag = false;
  displayAggTable: boolean;
  tableDataReq: object;
  tableData: TreeNode[];
  tableColumnDef: any[];
  dimColDef: any[];
  tableJson: Object;
  timeout: any;
  exportRequest: ExportRequest = <ExportRequest>{};
  showCharts = false;
  defaultChartsJson: any;
  metricChartJson: any;
  tableData1 = [];
  selectedDimension: any;
  selectedMetric: any;
  dimensionsDropdown: any[];
  metricDropdown: any[];
  showDailyData = "";
  isSourceRevenue = false;
  chartsMultiselectButton: object = {
    data: [
      {
        label: 'Revenue',
        value: 'dp_revenue'
      },
      {
        label: 'eCPM',
        value: 'eCPM'
      },
      {
        label: 'Impressions',
        value: 'dp_impressions'
      }
    ],
    model: ['dp_revenue']
  };
  chartsMultiselectButtonDimension: object = {
    data: [
      {
        label: 'Campaign',
        value: 'campaign_name'
      },
      {
        label: 'Channel',
        value: 'channel_name'
      }
    ],
    model: ['campaign_name']
  };

  constructor(
    private libServ: CommonLibService,
    private dataFetchServ: FetchApiDataService,
    private config: DynamicDialogConfig,
    private exportService: ExportdataService,
    private toastService: ToastService,
    private confirmationService: ConfirmationService,
    private formatNumPipe: FormatNumPipe
  ) {
    this.request = this.config.data['request'];
    this.appName = this.config.data['appName'];
    this.email = this.config.data['email'];
    this.showDailyData = this.config.data['field']
  }

  ngOnInit() {
    this.tableColumnDef = [
      {
        field: 'time_key',
        displayName: 'Date',
        format: 'date',
        width: '120',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'campaign_name',
        displayName: 'Campaign Name',
        format: '',
        width: '190',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: '-'
      },
      {
        field: 'deal_id',
        displayName: 'Deal ID',
        format: '',
        width: '100',
        exportConfig: {
          format: '',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: '-'
      },
      {
        field: 'dp_impressions',
        displayName: 'Impressions',
        format: 'number',
        width: '110',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'dp_revenue',
        displayName: 'Revenue',
        format: '$',
        width: '110',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'eCPM',
        displayName: 'eCPM',
        format: '$',
        width: '95',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'cvr',
        displayName: 'CVR',
        format: 'percentage',
        width: '85',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'ctr',
        displayName: 'CTR',
        format: 'percentage',
        width: '85',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'sell_through_rate',
        displayName: 'Sell-Through Rate',
        format: 'percentage',
        width: '115',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'vast_time_out',
        displayName: 'Vast Time-Out',
        format: 'percentage',
        width: '95',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'vast_opt_out',
        displayName: 'Vast Opt-Out',
        format: 'percentage',
        width: '95',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'tech_error',
        displayName: 'Tech Errors',
        format: 'percentage',
        width: '100',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'vpaid_opt_out',
        displayName: 'VPAID Opt-Out',
        format: 'percentage',
        width: '100',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      }
    ];
    this.tableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '400px',
      totalRecords: 1000,

      columns: this.tableColumnDef,
      selectedColumns: this.tableColumnDef,
      globalFilterFields: this.libServ.deepCopy(this.tableColumnDef.slice(0)),
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: false,
      overallSearch: true,
      columnSearch: false
    };

    this.defaultChartsJson = {
      chartTypes: [
        { key: 'line', label: 'Linear Trend' },
        { key: 'bar', label: 'Stack Trend', stacked: true }
      ],
      chartData: {
        labels: [],
        datasets: []
      },
      chartOptions: {
        title: {
          display: true,
          text: ''
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              display: true,
              scaleLabel: {
                display: true
              }
            }
          ]
        },
        pan: {
          enabled: true,
          mode: 'x'
        },
        zoom: {
          enabled: true,
          mode: 'x'
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '270px'
    };
    this.metricDropdown = [
      {
        field: 'dp_revenue',
        displayName: 'Revenue'
      },
      {
        field: 'dp_impressions',
        displayName: 'Impressions'
      },
      {
        field: 'eCPM',
        displayName: 'eCPM'
      }
    ];
    this.selectedMetric = "dp_revenue";
    this.dimensionsDropdown = [
      {
        field: 'campaign_name',
        displayName: 'Campaign'
      },
      {
        field: 'channel_name',
        displayName: 'Channel'
      }
    ];
    this.selectedDimension = 'campaign_name';
    this.dimColDef = this.libServ.deepCopy(this.tableColumnDef);
    if (this.showDailyData === 'creative_name') {
      this.dimColDef.splice(1, 2,
        {
          field: 'creative_name',
          displayName: 'Creative  Name',
          format: '',
          width: '190',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          },
          footerTotal: '-'
        })
      this.tableJson['columns'] = this.dimColDef;
      this.tableJson['selectedColumns'] = this.dimColDef;
    }
    this.loadTable(this.dimColDef);
  }

  tabChanged(event) {
    this.dimColDef = this.libServ.deepCopy(this.tableColumnDef);
    this.displayAggTable = false;
    if (event.index === 0 && this.showDailyData === 'campaign_name') {
      this.tableJson['columns'] = this.dimColDef;
      this.tableJson['selectedColumns'] = this.dimColDef;
      this.displayAggTable = true;
    } else if (event.index === 1 && this.showDailyData === 'campaign_name') {
      this.dimColDef.splice(1, 2,
        {
          field: 'channel_name',
          displayName: 'Channel',
          format: '',
          width: '190',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          },
          footerTotal: '-'
        },
        {
          field: 'channel_id',
          displayName: 'Channel ID',
          format: '',
          width: '190',
          exportConfig: {
            format: '',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          },
          footerTotal: '-'
        })
      this.getChartJson();
      this.tableJson['columns'] = this.dimColDef;
      this.tableJson['selectedColumns'] = this.dimColDef;
      this.displayAggTable = true;
    } else if (event.index === 2 && this.showDailyData === 'campaign_name') {
      this.dimColDef.splice(1, 2,
        {
          field: 'creative_name',
          displayName: 'Creative  Name',
          format: '',
          width: '190',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          },
          footerTotal: '-'
        })
      this.tableJson['columns'] = this.dimColDef;
      this.tableJson['selectedColumns'] = this.dimColDef;
      this.displayAggTable = true;
    } else if (event.index === 1 && this.showDailyData === 'creative_name') {
      this.dimColDef.splice(1, 2,
        {
          field: 'channel_name',
          displayName: 'Channel',
          format: '',
          width: '190',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          },
          footerTotal: '-'
        },
        {
          field: 'channel_id',
          displayName: 'Channel ID',
          format: '',
          width: '190',
          exportConfig: {
            format: '',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          },
          footerTotal: '-'
        })
      this.getChartJson();
      this.tableJson['columns'] = this.dimColDef;
      this.tableJson['selectedColumns'] = this.dimColDef;
      this.displayAggTable = true;
    } else if (event.index === 0 && this.showDailyData === 'creative_name') {
      this.dimColDef.splice(1, 2,
        {
          field: 'creative_name',
          displayName: 'Creative  Name',
          format: '',
          width: '190',
          exportConfig: {
            format: 'string',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          },
          footerTotal: '-'
        })
      this.tableJson['columns'] = this.dimColDef;
      this.tableJson['selectedColumns'] = this.dimColDef;
      this.displayAggTable = true;
    }
  }

  loadTable(tableDef) {
    this.tableJson['loading'] = true;
    const tableDataReq = this.request;
    this.dataFetchServ.getCPRData(tableDataReq).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }
      this.tableData1 = data['data'];

      const arr = [];
      this.tableData1.forEach((row: object, index: number) => {
        const obj = {
          data: row
        };
        arr.push(obj);
      });

      this.tableJson['columns'] = tableDef;
      this.tableJson['selectedColumns'] = tableDef;
      this.tableData = <TreeNode[]>arr;
      this.tableJson['totalRecords'] = data['totalItems'];
      this.tableJson['loading'] = false;
      this.displayAggTable = true;
      this.getChartJson();
    });
  }

  exportTablePopup(fileFormat) {
    const req = this.libServ.deepCopy(this.request);
    this.exportRequest['sendEmail'] = this.email;
    this.exportRequest['appName'] = this.appName;
    if (this.tableData.length == 0) {
      this.confirmationService.confirm({
        message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
        header: 'Information',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
        },
        reject: () => {
        }
      });
    } else {
      if (this.exportRequest['sendEmail'].length > 0) {
        this.toastService.displayToast({
          severity: 'info',
          summary: 'Export Report',
          detail:
            'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
          life: 10000
        });
        const columnDefs = this.libServ.deepCopy(this.dimColDef);

        const sheetDetails = {};
        sheetDetails['columnDef'] = columnDefs;
        sheetDetails['data'] = this.tableData1;
        sheetDetails['sheetName'] = 'Daily Data Distribution';
        sheetDetails['isRequest'] = false;
        sheetDetails['request'] = {
          url: '',
          method: '',
          param: ''
        };
        sheetDetails['disclaimer'] = [
          {
            position: 'bottom',
            label: 'Note: Data present in the table may vary over a period of time.',
            color: '#000000'
          }
        ];
        sheetDetails['totalFooter'] = {
          available: false,
          custom: false
        };
        sheetDetails['tableTitle'] = {
          available: false,
          label: 'Daily Data Distribution'
        };
        sheetDetails['image'] = [
          {
            available: true,
            path: environment.exportConfig.exportLogo,
            position: 'top'
          }
        ];
        const sheetDetailsarray = [];
        sheetDetailsarray.push(sheetDetails);
        this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
        this.exportRequest['fileName'] =
          this.config.data['field'] +
          ' Daily Distribution ' +
          moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
        this.exportRequest['exportFormat'] = fileFormat;
        this.exportRequest['exportConfig'] = environment.exportConfig;
        this.dataFetchServ
          .getExportReportData(this.exportRequest)
          .subscribe(response => {
            console.log('response of table', response);
          });
      } else {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Export Report',
          detail:
            'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
          life: 10000
        });
      }
    }
    return false;
  }

  isHiddenColumn(col: Object) { }

  onGlobalSearchChanged(searchValue, tableColDef) {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    this.timeout = setTimeout(() => {
      this.tableDataReq['filters']['globalSearch'] = {
        dimensions: [],
        value: searchValue
      };
      tableColDef.forEach(element => {
        this.tableDataReq['filters']['globalSearch']['dimensions'].push(
          element.field
        );
      });
      this.loadTable(this.tableDataReq);
    }, 3000);
  }

  getChartJson() {
    this.showCharts = false;
    this.metricChartJson = this.libServ.deepCopy(this.defaultChartsJson);
    if (this.selectedMetric === 'eCPM') {
      this.metricChartJson['chartTypes'] = [{ key: 'line', label: 'Linear Trend' }];
    }
    let labelStr = '';
    let text = '';
    switch (this.selectedMetric) {
      case 'dp_impressions':
        labelStr = 'Impressions';
        text = "Impressions"
        break;
      case 'dp_revenue':
        labelStr = 'Revenue' + ' ($)';
        text = "Revenue"
        break;
      case 'eCPM':
        labelStr = 'eCPM' + ' ($)';
        text = 'eCPM';

        break;
    }
    this.metricChartJson['chartOptions']['title'] = {
      display: true,
      text: text + ` by ${
        this.dimensionsDropdown.find(x => x.field == this.selectedDimension)
          .displayName
        }`
    };

    this.metricChartJson['chartOptions']['scales']['yAxes'][0]['scaleLabel'][
      'labelString'
    ] = labelStr;

    this.metricChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
      // Include a dollar sign in the ticks
      callback: (label, index, values) => {
        // return this.formatNumPipe.transform(value, '$', [0]);
        let value = label;
        switch (this.selectedMetric) {
          case 'dp_impressions':
            // value = value.toLocaleString('US');
            value = this.formatNumPipe.transform(value, 'number', [])
            break;
          case 'dp_revenue':
            value = this.formatNumPipe.transform(value, '$', [2]);
            break;
          case 'eCPM':
            value = this.formatNumPipe.transform(value, '$', []);
            break;
        }
        return value;
      }
    };

    this.metricChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          let formattedValue = null;
          switch (this.selectedMetric) {
            case 'dp_impressions':
              formattedValue = this.formatNumPipe.transform(currentValue, 'number', [])
              break;
            case 'dp_revenue':
              formattedValue = this.formatNumPipe.transform(currentValue, '$', [2]);
              break;
            case 'eCPM':
              formattedValue = this.formatNumPipe.transform(currentValue, '$', [])
              break;
          }
          return `${data.datasets[tooltipItem.datasetIndex].label} : ${formattedValue}`;
        }
      }
    }

    const chartData = this.tableData1;;
    if (typeof chartData === 'undefined' || !chartData.length) {
      this.showCharts = false;
    } else {
      const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));
      const sources = Array.from(
        new Set(chartData.map(s => s[this.selectedDimension]))
      );
      const colors = this.libServ.dynamicColors(sources.length);
      this.metricChartJson['chartData']['labels'] = datesArr.map(d =>
        moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
      );

      sources.forEach((src, i) => {
        const revArr = [];
        datesArr.forEach(time_key => {
          this.isSourceRevenue = false;
          chartData.forEach(r => {
            if (r[this.selectedDimension] === src && r['time_key'] === time_key) {
              revArr.push(r[this.selectedMetric]);
              this.isSourceRevenue = true;
            }
          });
          if (this.isSourceRevenue == false) {
            revArr.push(0);
          }
        });

        this.metricChartJson['chartData']['datasets'].push({
          label: src,
          data: revArr,
          borderColor: colors[i],
          fill: false,
          backgroundColor: colors[i]
        });
      });
      setTimeout(() => {
        this.showCharts = true;
      }, 0);
    }
  }
}
