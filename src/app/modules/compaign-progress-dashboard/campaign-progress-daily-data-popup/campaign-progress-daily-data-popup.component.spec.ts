import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignProgressDailyDataPopupComponent } from './campaign-progress-daily-data-popup.component';

describe('Rev360DailyDataPopupComponent', () => {
  let component: CampaignProgressDailyDataPopupComponent;
  let fixture: ComponentFixture<CampaignProgressDailyDataPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CampaignProgressDailyDataPopupComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignProgressDailyDataPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
