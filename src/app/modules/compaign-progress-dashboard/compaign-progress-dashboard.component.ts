import { Component, OnInit, OnDestroy } from '@angular/core';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { AppConfigService } from '../../_services/app-config/app-config.service';
import { CommonLibService } from '../../_services';
import { Title } from '@angular/platform-browser';
import { FetchApiDataService } from './fetch-api-data.service';
import { ExportdataService } from '../../_services/export/exportdata.service';
import { ExportPptService } from '../../_services/export/export-ppt.service';
import { HtmltoimageService } from '../../_services/screencapture/htmltoimage.service';
import { ToastService } from '../../_services/toast-notification/toast.service';
import { Subscription } from 'rxjs';
import { SendEmailComponent } from '../send-email/send-email.component';
import { ExportRequest } from '../../_interfaces/exportRequest';
import { FormatNumPipe } from '../../_pipes/number-format.pipe';
import * as moment from 'moment';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { CampaignProgressDailyDataPopupComponent } from './campaign-progress-daily-data-popup/campaign-progress-daily-data-popup.component';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-compaign-progress-dashboard',
  templateUrl: './compaign-progress-dashboard.component.html',
  styleUrls: ['./compaign-progress-dashboard.component.scss']
})
export class CompaignProgressDashboardComponent implements OnInit, OnDestroy {

  lastUpdatedOn: Date;
  dataUpdatedThrough: Date;
  nextUpdated: Date;

  exportRequest: ExportRequest = <ExportRequest>{};
  appConfigObs: Subscription;
  appConfig: object;
  tableRequestParam = {};
  filtersApplied: object = {};

  // filtersApplied: { timeKeyFilter: { time_key1: any; time_key2: any; }; filters: { dimensions: any[]; metrics: any[]; }; groupby: any[]; };

  showMainPieChart: boolean;
  mainPieChartJson: object;
  noDataMainPieChart: boolean;

  campaignsEndIn3DayTableData: TreeNode[];
  campaignsEndIn3DayTableJson: object;

  showChartsData: boolean = false;
  chartsMultiselectButton: object = {
    data: [
      {
        label: 'Revenue',
        value: 'dp_revenue'
      },
      {
        label: 'eCPM',
        value: 'eCPM'
      },
      {
        label: 'Sell Through Rate',
        value: 'sell_through_rate'
      },
      {
        label: 'Impressions',
        value: 'dp_impressions'
      }
    ],
    model: ['dp_revenue']
  };
  chartsMultiselectButtonDimension: object = {
    data: [
      {
        label: 'Campaign',
        value: 'campaign_name'
      },
      {
        label: 'Channel',
        value: 'channel_name'
      }
    ],
    model: ['campaign_name']
  };
  showCharts: boolean = false;
  revenueChartJson: any;
  ecpmChartJson: any;
  strChartJson: any;
  impressionsChartJson: any;
  defaultChartsJson: any;

  flag: boolean = true;

  noTableData: boolean = true;
  displayAggTable: boolean;
  aggTableData: TreeNode[];
  aggTableJson: object;
  aggColDef: any[];
  dimensionsDropdown: any[];
  selectedDimension: any;
  metricsDropdown: any[];
  metricChartJson: any;
  selectedMetric: any;
  tableReq: object;
  isSourceRevenue = false;

  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private htmltoimage: HtmltoimageService,
    private pptExport: ExportPptService,
    private dialogService: DialogService,
    private dataFetchServ: FetchApiDataService,
    private toastService: ToastService,
    private formatNumPipe: FormatNumPipe,
    private confirmationService: ConfirmationService
  ) { }

  ngOnInit(): void {
    this.dimensionsDropdown = [
      {
        field: 'campaign_name',
        displayName: 'Campaign'
      },
      {
        field: 'channel_name',
        displayName: 'Channel'
      }
    ];
    this.selectedDimension = 'campaign_name';
    this.selectedMetric = "dp_revenue";

    this.defaultChartsJson = {
      chartTypes: [
        { key: 'line', label: 'Linear Trend' },
        { key: 'bar', label: 'Stack Trend', stacked: true }
      ],
      chartData: {
        labels: [],
        datasets: []
      },
      chartOptions: {
        title: {
          display: true,
          text: ''
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              display: true,
              scaleLabel: {
                display: true
              }
            }
          ]
        },
        pan: {
          enabled: true,
          mode: 'x'
        },
        zoom: {
          enabled: true,
          mode: 'x'
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '250px'
    };


    this.aggColDef = [
      {
        field: 'campaign_name',
        displayName: 'Campaign Name',
        format: '',
        width: '280',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'creative_name',
        displayName: 'Creative Name',
        format: '',
        width: '230',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'deal_id',
        displayName: 'Campaign Id',
        format: '',
        width: '125',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'campaign_start_date',
        displayName: 'Start Date',
        format: 'date',
        width: '120',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'campaign_end_date',
        displayName: 'End Date',
        format: 'date',
        width: '120',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'status',
        displayName: 'Status',
        format: '',
        width: '110',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'source',
        displayName: 'Source',
        format: '',
        width: '110',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'campaign_manager',
        displayName: 'Campaign Manager',
        format: '',
        width: '110',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'OSI',
        displayName: 'OSI',
        format: 'percentage',
        width: '180',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'io_target_impressions',
        displayName: 'IO Target Impressions',
        format: 'number',
        width: '180',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'budget',
        displayName: 'Budget',
        format: '$',
        width: '180',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'dp_impressions',
        displayName: 'Impressions',
        format: 'number',
        width: '110',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'eCPM',
        displayName: 'eCPM',
        format: '$',
        width: '95',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'dp_revenue',
        displayName: 'Revenue',
        format: '$',
        width: '110',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'cvr',
        displayName: 'CVR',
        format: 'percentage',
        width: '95',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'ctr',
        displayName: 'CTR',
        format: 'percentage',
        width: '85',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'sell_through_rate',
        displayName: 'Sell-Through Rate',
        format: 'percentage',
        width: '115',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'vast_time_out',
        displayName: 'Vast Time-Out',
        format: 'percentage',
        width: '150',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'vast_opt_out',
        displayName: 'Vast Opt-Out',
        format: 'percentage',
        width: '95',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'tech_error',
        displayName: 'Tech Errors',
        format: 'percentage',
        width: '100',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'vpaid_opt_out',
        displayName: 'VPAID Opt-Out',
        format: 'percentage',
        width: '100',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      }
    ];
    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      reload: false,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,

      columns: this.libServ.deepCopy(this.aggColDef.slice(2)),
      selectedColumns: this.libServ.deepCopy(this.aggColDef.slice(2)),
      globalFilterFields: this.libServ.deepCopy(this.aggColDef.slice(0)),
      frozenCols: this.libServ.deepCopy([...this.aggColDef.slice(0, 2)]),
      frozenWidth:
        this.aggColDef
          .slice(0, 2)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        console.log('appConfig', this.appConfig);

        this._titleService.setTitle(this.appConfig['displayName']);
        let startDate;
        let endDate;
        const date_config = this.appConfig['filter']['filterConfig']['filters'][
          'datePeriod'
        ][0];
        if ((date_config['defaultDate'][0]['value'] === 0 && date_config['defaultDate'][0]['period'] === 'month') && (moment().format('DD') === '01' || moment().format('DD') === '02')) {
          startDate = moment().subtract(1, 'months').startOf('month');
          endDate = moment().subtract(1, 'months').endOf('month');
        } else {
          if (
            date_config[
            'defaultDate'
            ][0]['startOf']
          ) {
            startDate = moment()
              .subtract(
                date_config['defaultDate'][0]['value'],
                date_config['defaultDate'][0]['period']
              )
              .startOf(
                date_config['defaultDate'][0]['period']
              );
          } else {
            startDate = moment().subtract(
              date_config['defaultDate'][0]['value'],
              date_config['defaultDate'][0]['period']
            );
          }
          endDate = moment().subtract(
            date_config[
            'defaultDate'
            ][1]['value'],
            date_config[
            'defaultDate'
            ][1]['period']
          );
        }

        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: startDate.format('YYYYMMDD'),
            time_key2: endDate.format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: ['campaign_name', 'creative_name']
        };
        const dateArr = [];
        // let startDateCp = moment(this.filtersApplied.timeKeyFilter.time_key1, 'YYYYMMDD');
        // const endDateCp = moment(this.filtersApplied.timeKeyFilter.time_key2, 'YYYYMMDD');
        // while (startDateCp.isSameOrBefore(endDateCp)) {
        //   dateArr.push(startDateCp.format('DD/MM/YYYY'));
        //   startDateCp.add(1, 'day');
        // }
        // this.filtersApplied.filters.dimensions.push({
        //   key: 'start_date',
        //   values: dateArr
        // });

        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        this.initialLoading();
      }
    });

    this.tableRequestParam = {
      dimensions: [],
      metrics: [],
      derived_metrics: [],
      timeKeyFilter: {},
      filters: { dimensions: [], metrics: [] },
      groupByTimeKey: { key: [], interval: 'daily' },
      gidGroupBy: '',
      orderBy: [
        { key: '', opcode: '' }
      ],
      limit: '',
      offset: ''
    };
  }

  initialLoading() {

    this.dataFetchServ
    .getLastUpdatedData(this.appConfig['id'])
    .subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      }
      this.lastUpdatedOn = moment(data[0]['updated_at']).toDate();
      this.nextUpdated = moment(data[0]['next_run_at']).toDate();
      this.dataUpdatedThrough = moment(
        data[0]['source_updated_through'],
        'YYYYMMDD'
      ).toDate();
    });
    this.loadCampaignStatusChart();
    this.getChartJson();

    this.tableReq = this.libServ.deepCopy(this.tableRequestParam);
    console.log("req");

    this.tableReq['dimensions'] = ['0 As $$treeLevel'];
    this.tableReq['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);
    this.tableReq['gidGroupBy'] = ['campaign_manager', 'campaign_name', 'source'],
      this.tableReq['orderBy'] = [{ key: 'campaign_name', opcode: 'asc' }],
      this.tableReq['groupByTimeKey'] = { key: [], interval: 'daily' },
      this.tableReq['limit'] = '',
      this.tableReq['offset'] = ''
    this.loadTableMainData(this.tableReq);
  }

  changeSearchFlag(flag) {
    setTimeout(() => {
      this.loadTableMainData(this.tableReq);
    }, 0);
  }

  loadCampaignStatusChart() {
    const request = {
      dimensions: ['0 As $$treeLevel'],
      metrics: [],
      derived_metrics: [],
      timeKeyFilter: {},
      filters: this.filtersApplied['filters'],
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: [
        'campaign_name', 'channel_name'
      ],
      orderBy: [{ key: 'channel_name', opcode: 'asc' }],
      limit: '',
      offset: ''
    };
    this.mainPieChartJson = {
      chartTypes: [{ key: 'doughnut', label: 'Pie Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Campaigns Running'
        },
        legend: {
          display: false
        },
        elements: {
          arc: {
            borderWidth: 0
          }
        },
        plugins: {
          datalabels: {
            formatter: (value, ctx) => {
              let sum = 0;
              const dataArr = ctx.chart.data.datasets[0].data;
              dataArr.map(data => {
                sum += data;
              });
              return (value * 100) / sum;
              const percentage = (value * 100) / sum;
              if (percentage > 10) {
                return `${percentage.toFixed(2)} %`;
              } else {
                return '';
              }
            },
            color: 'black'
          }
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '350px'
    };
    this.mainPieChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          let sum = 0;
          const dataArr = data.datasets[0].data;
          dataArr.map(data => {
            sum += data;
          });
          const percentage = (currentValue * 100) / sum;
          return `${
            data.labels[tooltipItem.index]
            } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
        }
      }
    };
    this.showMainPieChart = false;
    this.dataFetchServ.getCPRData(request).subscribe(data => {
      const chartData = data['data'];
      this.noDataMainPieChart = false;
      if (!chartData.length) {
        this.noDataMainPieChart = true;
        return;
      }
      let active = 0;
      let inactive = 0;
      chartData.forEach(element => {
        if (element.status === 1) {
          active++;
        } else {
          inactive++;
        }
      });
      const data1 = [active, inactive];
      const sourceArr = ['Active', 'Inactive'];
      const colors = ['green', 'red'];
      this.mainPieChartJson['chartData']['labels'] = sourceArr;
      this.mainPieChartJson['chartData']['datasets'][0]['data'] = data1;
      this.mainPieChartJson['chartData']['datasets'][0]['backgroundColor'] = colors;
      setTimeout(() => {
      this.showMainPieChart = true
      }, 0);
    });
  }

  loadTableMainData(tableReq) {
    this.dataFetchServ.getCPRData(tableReq).subscribe(data => {
      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      } else {
        this.noTableData = false;
      }
      const tableData = data['data'] as [];
      const arr = [];
      tableData.forEach((row: object) => {
        arr.push({
          data: row,
          children: [{ data: {} }]
        });
      });
      this.aggTableData = <TreeNode[]>arr;
      this.aggTableJson['totalRecords'] = arr.length;
      setTimeout(() => {
        this.aggTableJson['loading'] = false;
        this.displayAggTable = true;
      });
    });
  }

  loadCharts() {
    let selectedDimension = this.selectedDimension;
    const chartReq = {
      dimensions: ['campaign_name', 'time_key', 'start_date', 'channel'],
      metrics: [
        'revenue',
        'impression',
        'inventory'
      ],
      derived_metrics: [],
      timeKeyFilter: [],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key'],
        interval: 'daily'
      },
      gidGroupBy: [this.selectedDimension],
      orderBy: [{ key: this.selectedDimension, opcode: 'desc' }],
      limit: 10,
      offset: 0
    };
    // Reset Data
    this.metricChartJson = this.libServ.deepCopy(this.defaultChartsJson);
    this.metricChartJson['chartOptions']['title'] = {
      display: true,
      text: `Revenue by ${
        this.dimensionsDropdown.find(x => x.field == selectedDimension)
          .displayName
        } Trend`
    };
    this.revenueChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
      // Include a dollar sign in the ticks
      callback: (value, index, values) => {
        return this.formatNumPipe.transform(value, '$', [0]);
      }
    };
    this.revenueChartJson['chartOptions']['scales']['yAxes'][0]['scaleLabel'][
      'labelString'
    ] = 'Revenue ($)';
    this.revenueChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return `${
            data.datasets[tooltipItem.datasetIndex].label
            } : ${this.formatNumPipe.transform(currentValue, '$', [2])}`;
        }
      }
    };
    this.ecpmChartJson = this.libServ.deepCopy(this.defaultChartsJson);
    this.ecpmChartJson['chartTypes'] = [{ key: 'line', label: 'Linear Trend' }];
    this.ecpmChartJson['chartOptions']['title'] = {
      display: true,
      text: `eCPM by ${
        this.dimensionsDropdown.find(x => x.field == this.selectedDimension)
          .displayName
        } Trend`
    };
    this.ecpmChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
      // Include a dollar sign in the ticks
      callback: (value, index, values) => {
        return this.formatNumPipe.transform(value, '$', []);
      }
    };
    this.ecpmChartJson['chartOptions']['scales']['yAxes'][0]['scaleLabel'][
      'labelString'
    ] = 'eCPM ($)';
    this.ecpmChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return `${
            data.datasets[tooltipItem.datasetIndex].label
            } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
        }
      }
    };
    this.strChartJson = this.libServ.deepCopy(this.defaultChartsJson);
    this.strChartJson['chartOptions']['title']['text'] = 'Sell Through Rate Trend';
    this.strChartJson['chartOptions']['title'] = {
      display: true,
      text: `Sell Through Rate by ${
        this.dimensionsDropdown.find(x => x.field == this.selectedDimension)
          .displayName
        } Trend`
    };
    this.strChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
      callback: (value, index, values) => {
        return this.formatNumPipe.transform(value, 'percentage', [2]);
      }
    };
    this.strChartJson['chartOptions']['scales']['yAxes'][0]['scaleLabel'][
      'labelString'
    ] = 'Sell Through Rate (%)';
    this.strChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return `${
            data.datasets[tooltipItem.datasetIndex].label
            } : ${this.formatNumPipe.transform(currentValue, 'percentage', [2])}`;
        }
      }
    };
    this.impressionsChartJson = this.libServ.deepCopy(this.defaultChartsJson);
    this.impressionsChartJson['chartOptions']['title']['text'] =
      'Paid Impressions Trend';
    this.impressionsChartJson['chartOptions']['title'] = {
      display: true,
      text: `Paid Impressions by ${
        this.dimensionsDropdown.find(x => x.field == this.selectedDimension)
          .displayName
        } Trend`
    };
    this.impressionsChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
      // Include a dollar sign in the ticks
      callback: (value, index, values) => {
        return this.formatNumPipe.transform(value, 'number', []);
      }
    };
    this.impressionsChartJson['chartOptions']['scales']['yAxes'][0][
      'scaleLabel'
    ]['labelString'] = 'Impressions';
    this.impressionsChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return `${
            data.datasets[tooltipItem.datasetIndex].label
            } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
        }
      }
    };
    this.showCharts = false;
    this.showChartsData = false;
    this.dataFetchServ.getCPRData(chartReq).subscribe(data => {
      const chartData = data['data'];

      if (typeof chartData === 'undefined' || !chartData.length) {
        this.showCharts = true;
      } else {
        const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));

        const sources = Array.from(
          new Set(chartData.map(s => s[this.selectedDimension]))
        );
        const colors = this.libServ.dynamicColors(sources.length);
        this.revenueChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );
        this.ecpmChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );
        this.strChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );
        this.impressionsChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );

        sources.forEach((src, i) => {
          const revArr = [];
          const ecpmArr = [];
          const sellThroughtRateArr = [];
          const impArr = [];
          datesArr.forEach(time_key => {
            chartData.forEach(r => {
              if (r[this.selectedDimension] === src && r['time_key'] === time_key) {
                revArr.push(r['revenue']);
                ecpmArr.push(r['revenue'] * 1000 / r['impression']);
                sellThroughtRateArr.push(r['impression'] / r['inventory'] * 100);
                impArr.push(r['impression']);
              }
            });
          });

          this.revenueChartJson['chartData']['datasets'].push({
            label: src,
            data: revArr,
            borderColor: colors[i],
            fill: false,
            backgroundColor: colors[i]
          });
          this.ecpmChartJson['chartData']['datasets'].push({
            label: src,
            data: ecpmArr,
            borderColor: colors[i],
            fill: false,
            backgroundColor: colors[i]
          });
          this.strChartJson['chartData']['datasets'].push({
            label: src,
            data: sellThroughtRateArr,
            borderColor: colors[i],
            fill: false,
            backgroundColor: colors[i]
          });
          this.impressionsChartJson['chartData']['datasets'].push({
            label: src,
            data: impArr,
            borderColor: colors[i],
            fill: false,
            backgroundColor: colors[i]
          });
        });
        this.showCharts = false;
        this.showChartsData = true;
      }
    });
  }

  onFiltersApplied(filterData: object) {
    console.log("filterData", filterData);

    const dim = {};
    this.filtersApplied['filters']['dimensions'] = [];
    for (const k in filterData['filter']['dimensions']) {
      if (filterData['filter']['dimensions'][k].length !== 0) {
        this.filtersApplied['filters']['dimensions'].push({
          key: k,
          values: filterData['filter']['dimensions'][k]
        });
      }
    }
    // const dateArr = [];
    // let startDateCp = moment(filterData['date'][0], 'YYYYMMDD');
    // const endDateCp = moment(filterData['date'][1], 'YYYYMMDD');
    // while (startDateCp.isSameOrBefore(endDateCp)) {
    //   dateArr.push(startDateCp.format('DD/MM/YYYY'));
    //   startDateCp.add(1, 'day');
    // }
    // this.filtersApplied.filters.dimensions.push({
    //   key: 'start_date',
    //   values: dateArr
    // });
    // const filtersArr = this.filtersApplied['filters']['dimensions'].map(x => x.key);
    // const groupBy = Array.from(new Set(filtersArr));
    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];
    console.log(this.filtersApplied);
    this.initialLoading();
  }

  onLazyLoadAggTable(e: Event) { }

  onTableDrill1(e: Event) {
    if (!e['node']['childLoaded']) {
      this.aggTableJson['loading'] = true;
      const tblReq = {
        dimensions: ['channel', 'campaign_id', 'campaign_name', 'creative_name', 'start_date', 'end_date', 'status', 'campaign_manager'],
        metrics: ['revenue', 'impression', 'inventory', 'ecpm', 'click', 'conversion', 'ctr', 'cvr', 'str', 'vast_time_out', 'vast_opt_out', 'tech_err', 'vpaid_opt_out'],
        derived_metrics: [],
        timeKeyFilter: this.filtersApplied['timeKeyFilter'],
        filters: this.libServ.deepCopy(this.filtersApplied['filters']),
        groupByTimeKey: {
          key: [''],
          interval: 'daily'
        },
        gidGroupBy: ['channel', 'campaign_name', 'creative_name'],
        orderBy: [{ key: 'creative_name', opcode: 'asc' }],
        limit: '',
        offset: ''
      };
      ['channel', 'campaign_name'].forEach(g => {
        if (tblReq['filters']['dimensions'].findIndex(e => e.key === g) === -1) {
          tblReq['filters']['dimensions'].push({ key: g, values: [e['node']['data'][g]] });
        } else {
          if (tblReq['filters']['dimensions'].find(e => e.key === g)['values'].findIndex(v => v === e['node']['data'][g]) === -1) {
            tblReq['filters']['dimensions'].push(e['node']['data'][g]);
          }
        }
      });

      this.dataFetchServ.getCPRData(tblReq).subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        const childData = data['data'];
        const arr = [];

        childData.forEach((row: object, index: number) => {
          row['campaign_id'] = '' + row['campaign_id'] + '' + (index + 2);
          row['org_campaign_name'] = row['campaign_name'];
          row['org_creative_name'] = row['creative_name'];
          row['org_channel'] = row['channel'];
          row['campaign_name'] = row['creative_name'];
          row['ecpm'] = row['revenue'] * 1000 / row['impression'];
          row['ctr'] = row['click'] / row['impression'] * 100;
          row['cvr'] = row['conversion'] / row['click'] * 100;
          row['str'] = row['impression'] / row['inventory'] * 100;
          row['isCreativeData'] = true;
          let obj = {};
          obj = {
            data: row
          }
          obj['data']['isExpanded'] = '1';
          arr.push(obj);
        });

        this.aggTableJson['loading'] = false;
        e['node']['children'] = <TreeNode[]>arr;
        this.aggTableData = [...this.aggTableData];
        e['node']['childLoaded'] = true;
      });
    }
  }

  onTableDrill(e: Event) {
    if (!e['node']['childLoaded']) {
      setTimeout(() => {
        this.aggTableJson['loading'] = true;
      });
      const tableReq = this.libServ.deepCopy(this.tableRequestParam);
      tableReq.filters = this.libServ.deepCopy(this.filtersApplied['filters']);
      if (tableReq['filters']['dimensions'].find(e => e.key === 'campaign_name')) {
        tableReq['filters']['dimensions'] = tableReq['filters']['dimensions'].slice(0, 1)
      }
      if (
        tableReq['filters']['dimensions'].findIndex(
          itm => itm.key === 'campaign_name'
        ) === -1
      ) {
        tableReq['filters']['dimensions'].push({
          key: 'campaign_name',
          values: [e['node']['data']['campaign_name']]
        });
      } else {
        tableReq['filters']['dimensions'].push({
          key: 'campaign_name',
          values: [e['node']['data']['campaign_name']]
        });
      }

      tableReq['dimensions'] = [];
      tableReq['gidGroupBy'] = ['campaign_manager', 'campaign_name', 'creative_name', 'source'];
      tableReq['orderBy'] = [{ key: 'campaign_name', opcode: 'asc' }];
      tableReq['limit'] = '';
      tableReq['offset'] = '';
      this.dataFetchServ
        .getCPRData(tableReq)
        .subscribe(data => {
          if (data['status'] === 0) {
            // this.noTableData = true;
            setTimeout(() => {
              this.aggTableJson['loading'] = false;
            });
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Server Error',
              detail: 'Please refresh the page'
            });
            return;
          } else {
            // this.noTableData = false;
          }
          const tableData = data['data'];
          const arr = [];
          tableData.forEach((row: object) => {
            arr.push({
              data: row
            });
          });
          e['node']['children'] = arr;
          this.aggTableData = [...this.aggTableData];
          e['node']['childLoaded'] = true;
          setTimeout(() => {
            this.aggTableJson['loading'] = false;
          });
        });
    }
  }

  openPopup(row, field) {
    console.log('row', row);
    const tableReq = this.libServ.deepCopy(this.tableRequestParam);
    tableReq.filters = this.libServ.deepCopy(this.filtersApplied['filters']);
    const f = this.libServ.deepCopy(this.filtersApplied['filters']);
    tableReq['dimensions'] = [];
    tableReq['groupByTimeKey'] = { key: [], interval: 'daily' },
      tableReq['gidGroupBy'] = ['campaign_name', 'channel_name', 'creative_name'],
      tableReq['orderBy'] = [{ key: 'time_key', opcode: 'desc' }],
      tableReq['limit'] = '',
      tableReq['offset'] = ''
    console.log("tableReq['filters']['dimensions']====", this.filtersApplied['filters'], f);

    for (const grp of this.filtersApplied['groupby']) {
      console.log("gr===", grp, this.filtersApplied['groupby'], field);

      if (grp === field) {
        tableReq['filters']['dimensions'].push({
          key: grp,
          values: [row[grp]]
        });
        break;
      } else {
        // tableReq['gidGroupBy'] =  ['channel_name','creative_name'],
        tableReq['filters']['dimensions'].push({
          key: grp,
          values: [row[grp]]
        });
      }
    }
    const data = {
      row: row,
      request: tableReq,
      exportRequest: this.exportRequest,
      field: field,
      appName: this.exportRequest['appName'],
      email: this.exportRequest['sendEmail']
    };
    const ref = this.dialogService.open(CampaignProgressDailyDataPopupComponent, {
      header: "Daily BreakDown",
      contentStyle: { width: '90vw', height: '900px', overflow: 'auto' },
      data: data
    });
  }

  isHiddenColumn(col: Object) {
    return (
      this.aggTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }

  progressRowClass = (row) => {
    const status = row['status'];
    // moment(value, 'YYYYMMDD').format('MM-DD-YYYY')
    const endDate = moment(row['end_date'], 'DD/MM/YYYY');
    // const today = moment().tz('America/New_York').startOf('day');
    const today = moment().startOf('day');
    let rowClass = '';
    if (typeof row['creative_id'] !== 'undefined') {
      // const cStartDate = moment.tz(row.entity.campaign_start, 'YYYYMMDD', 'America/New_York');
      // const cEndDate = moment.tz(row.entity.campaign_end, 'YYYYMMDD', 'America/New_York');
      // const startDate = moment.tz(row.entity.flight_start, 'YYYY-MM-DD', 'America/New_York');
      const cStartDate = moment(row['start_date'], 'DD/MM/YYYY');
      const cEndDate = moment(row['end_date'], 'DD/MM/YYYY');
      const startDate = moment(row['start_date'], 'DD/MM/YYYY');
      if (endDate.isAfter(cEndDate) || cStartDate.isAfter(startDate)) {
        rowClass = 'ui-grid-row-blue';
      }
    }
    if (endDate.isSameOrAfter(today) && status === 'inactive') {
      rowClass = 'ui-grid-row-paused ';
    }
    return rowClass;
  };

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['displayName']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    } else if (type === 'ppt') {
      const chartElements = document.getElementsByTagName('ym-charts');
      const slides = [];
      [].forEach.call(chartElements, (el, index) => {
        const c = el.getElementsByTagName('canvas')[0];

        slides.push({
          title: `${this.appConfig['displayName']}`,
          base64data: this.libServ.canvasToImage('white', c),
          width: 8,
          height: 3
        });

        if (index === chartElements.length - 1) {
          const pptConfig = {
            filename: 'Charts',
            logo: {
              visible: true,
              path: '/assets/images/ym.png',
              width: 0.53125,
              height: 0.458333
            },
            slides: slides
          };
          this.pptExport.exportPPT(pptConfig);
        }
      });
    }
    return false;
  }

  sendEmail() {
    const imageConfig = {
      imgName: `${this.appConfig['name']}.png`,
      type: 'image/png'
    };

    this.htmltoimage
      .canvasImage(document.getElementById('capture'), imageConfig)
      .then(canvas => {
        this.dialogService.open(SendEmailComponent, {
          header: 'Email Screenshot',
          contentStyle: {
            'max-height': '80vh',
            width: '30vw',
            overflow: 'auto'
          },
          data: canvas.toDataURL('image/png')
        });
      });
  }

  getChartJson() {
    this.showCharts = false;
    this.showChartsData = false;
    this.metricChartJson = this.libServ.deepCopy(this.defaultChartsJson);
    if (this.selectedMetric === 'eCPM') {
      this.metricChartJson['chartTypes'] = [{ key: 'line', label: 'Linear Trend' }];
    }
    let labelStr = '';
    let text = '';
    switch (this.selectedMetric) {
      case 'dp_impressions':
        labelStr = 'Impressions';
        text = "Impressions"
        break;
      case 'dp_revenue':
        labelStr = 'Revenue' + ' ($)';
        text = "Revenue"
        break;
      case 'eCPM':
        labelStr = 'eCPM' + ' ($)';
        text = 'eCPM';
        break;
      case 'sell_through_rate':
        labelStr = 'Sell Through Rate' + ' (%)';
        text = 'Sell Through Rate';
        break;
    }
    this.metricChartJson['chartOptions']['title'] = {
      display: true,
      text: text + ` by ${
        this.dimensionsDropdown.find(x => x.field == this.selectedDimension)
          .displayName
        }`
    };

    this.metricChartJson['chartOptions']['scales']['yAxes'][0]['scaleLabel'][
      'labelString'
    ] = labelStr;

    this.metricChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
      // Include a dollar sign in the ticks
      callback: (label, index, values) => {
        // return this.formatNumPipe.transform(value, '$', [0]);
        let value = label;
        switch (this.selectedMetric) {
          case 'dp_impressions':
            // value = value.toLocaleString('US');
            value = this.formatNumPipe.transform(value, 'number', [])
            break;
          case 'dp_revenue':
            value = this.formatNumPipe.transform(value, '$', [2]);
            break;
          case 'eCPM':
            value = this.formatNumPipe.transform(value, '$', []);
            break;
          case 'sell_through_rate':
            value = this.formatNumPipe.transform(value, 'percentage', [2]);
            break;
        }
        return value;
      }
    };

    this.metricChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          let formattedValue = null;
          switch (this.selectedMetric) {
            case 'dp_impressions':
              formattedValue = this.formatNumPipe.transform(currentValue, 'number', [])
              break;
            case 'dp_revenue':
              formattedValue = this.formatNumPipe.transform(currentValue, '$', [2]);
              break;
            case 'eCPM':
              formattedValue = this.formatNumPipe.transform(currentValue, '$', [])
              break;
            case 'sell_through_rate':
              formattedValue = this.formatNumPipe.transform(currentValue, 'percentage', [2])
              break;
          }
          return `${data.datasets[tooltipItem.datasetIndex].label} : ${formattedValue}`;
        }
      }
    }

    const tblReq = {
      dimensions: ['0 As $$treeLevel'],
      metrics: [],
      derived_metrics: [],
      timeKeyFilter: {},
      filters: this.filtersApplied['filters'],
      groupByTimeKey: { key: [], interval: 'daily' },
      gidGroupBy: ['campaign_name', 'channel_name'],
      orderBy: [{ key: 'campaign_name', opcode: 'asc' }],
      limit: '',
      offset: ''
      // campaign_manager,campaign_name,source
    }
    // console.log("table req",tblReq);

    this.dataFetchServ.getCPRData(tblReq).subscribe(data => {
      const chartData = data['data'];
      console.log("chard data", chartData);

      if (typeof chartData === 'undefined' || !chartData.length) {
        this.showCharts = true;
      } else {
        const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));

        const sources = Array.from(
          // new Set(chartData.map(s => s[this.selectedDimension]))
          new Set(chartData.map(s => s[this.selectedDimension]))

        );
        const colors = this.libServ.dynamicColors(sources.length);
        console.log("dates array", datesArr);

        this.metricChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );

        sources.forEach((src, i) => {
          const revArr = [];
          datesArr.forEach(time_key => {
            this.isSourceRevenue = false;
            chartData.forEach(r => {
              if (r[this.selectedDimension] === src && r['time_key'] === time_key) {
                revArr.push(r[this.selectedMetric]);
                this.isSourceRevenue = true;
              }
            });
            if (this.isSourceRevenue == false) {
              revArr.push(0);
            }
          });
          this.metricChartJson['chartData']['datasets'].push({
            label: src,
            data: revArr,
            borderColor: colors[i],
            fill: false,
            backgroundColor: colors[i]
          });
        });

        setTimeout(() => {
          this.showCharts = false;
          this.showChartsData = true;
        }, 0);
      }
    });
  }

  exportTable(fileFormat) {
    const data = [];

    if (this.aggTableData.length == 0) {
      this.confirmationService.confirm({
        message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
        header: 'Information',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
        },
        reject: () => {
        }
      });
    } else {


      if (this.exportRequest['sendEmail'].length > 0) {
        this.toastService.displayToast({
          severity: 'info',
          summary: 'Export Report',
          detail:
            'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
          life: 10000
        });
        const finalColDef = this.libServ.deepCopy(this.aggColDef);
        // finalColDef.splice(0, 0, ...this.dimColDef);
        // finalColDef.splice(
        //   finalColDef.findIndex(itm => itm.field === 'favorite'),
        //   2
        // );
        const tableReq = this.libServ.deepCopy(this.tableRequestParam);
        tableReq['dimensions'] = [];
        tableReq['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);
        tableReq['gidGroupBy'] = ['campaign_manager', 'campaign_name', 'source'],
          tableReq['orderBy'] = [{ key: 'campaign_name', opcode: 'asc' }],
          tableReq['groupByTimeKey'] = { key: [], interval: 'daily' },
          tableReq['limit'] = '',
          tableReq['offset'] = ''

        const creativeReq = this.libServ.deepCopy(this.tableRequestParam);
        creativeReq['dimensions'] = [];
        creativeReq['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);
        creativeReq['gidGroupBy'] = ['campaign_manager', 'campaign_name', 'creative_name'],
          creativeReq['orderBy'] = [{ key: 'creative_name', opcode: 'asc' }],
          creativeReq['groupByTimeKey'] = { key: [], interval: 'daily' },
          creativeReq['limit'] = '',
          creativeReq['offset'] = ''
        this.dataFetchServ.getCPRData(tableReq).subscribe(data => {
          const tableData = data['data'] as [];
          this.dataFetchServ.getCPRData(creativeReq).subscribe(data1 => {
            const creativeData = data1['data'] as [];
            const sheetDetailsarray = [];
            const sheetDetails = {};
            sheetDetails['columnDef'] = finalColDef;
            sheetDetails['data'] = tableData;
            sheetDetails['sheetName'] = 'Campaign Distribution';
            sheetDetails['isRequest'] = false;
            sheetDetails['request'] = {
              url: '',
              method: '',
              param: ''
            };
            sheetDetails['disclaimer'] = [
              {
                position: 'top',
                label:
                  'Data persent in below table is not final and may varies over period of time',
                color: '#3A37CF'
              },
              {
                position: 'bottom',
                label: 'Thank You',
                color: '#EC6A15'
              }
            ];
            sheetDetails['totalFooter'] = {
              available: false,
              custom: false
            };
            sheetDetails['tableTitle'] = {
              available: true,
              label: 'Campaign Distribution'
            };
            sheetDetails['image'] = [
              {
                available: true,
                path: environment.exportConfig.exportLogo,
                position: 'top'
              }
            ];


            sheetDetailsarray.push(sheetDetails);

            const sheetDetailsCreative = {};
            sheetDetailsCreative['columnDef'] = finalColDef;
            sheetDetailsCreative['data'] = creativeData;
            sheetDetailsCreative['sheetName'] = 'Creative Distribution';
            sheetDetailsCreative['isRequest'] = false;
            sheetDetailsCreative['request'] = {
              url: '',
              method: '',
              param: ''
            };
            sheetDetailsCreative['disclaimer'] = [
              {
                position: 'top',
                label:
                  'Data persent in below table is not final and may varies over period of time',
                color: '#3A37CF'
              },
              {
                position: 'bottom',
                label: 'Thank You',
                color: '#EC6A15'
              }
            ];
            sheetDetailsCreative['totalFooter'] = {
              available: false,
              custom: false
            };
            sheetDetailsCreative['tableTitle'] = {
              available: true,
              label: 'Campaign Distribution'
            };
            sheetDetailsCreative['image'] = [
              {
                available: true,
                path: environment.exportConfig.exportLogo,
                position: 'top'
              }
            ];


            sheetDetailsarray.push(sheetDetailsCreative);
            this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
            this.exportRequest['fileName'] =
              'WayPonit CPR Breckdown ' + moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
            this.exportRequest['exportFormat'] = fileFormat;
            this.exportRequest['exportConfig'] = environment.exportConfig;
            this.dataFetchServ
              .getExportReportData(this.exportRequest)
              .subscribe(response => {
                console.log(response);
              });
          })
        })
      } else {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Export Report',
          detail:
            'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email.Thanks!',
          life: 10000
        });
      }
    }
  }
  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }
}
