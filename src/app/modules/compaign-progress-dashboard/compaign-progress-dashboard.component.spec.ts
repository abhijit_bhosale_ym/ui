import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompaignProgressDashboardComponent } from './compaign-progress-dashboard.component';

describe('CompaignDashboardComponent', () => {
  let component: CompaignProgressDashboardComponent;
  let fixture: ComponentFixture<CompaignProgressDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompaignProgressDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompaignProgressDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
