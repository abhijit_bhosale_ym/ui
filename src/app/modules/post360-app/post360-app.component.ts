import {
  Component,
  OnInit,
  QueryList,
  ElementRef,
  ViewChildren,
  OnDestroy
} from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { FetchApiDataService } from './fetch-api-data.service';
import { TreeNode } from 'primeng/api';
import { ToastService } from '../../_services/toast-notification/toast.service';
import { DialogService } from 'primeng/dynamicdialog';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import * as moment from 'moment';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { Title } from '@angular/platform-browser';
import { DataShareService } from 'src/app/_services/data-share/data-share.service';
import { Subscription } from 'rxjs';
import { float } from 'html2canvas/dist/types/css/property-descriptors/float';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { SendEmailComponent } from '../send-email/send-email.component';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-post360-app',
  templateUrl: './post360-app.component.html',
  styleUrls: ['./post360-app.component.scss']
})
export class Post360AppComponent implements OnInit ,OnDestroy{
  private appConfigObs: Subscription;
  lastUpdatedOn: Date;

  appConfig: object = {};
  cardsJson = [];
  showCards = true;
  noDataPieChart = false;
  showMainLineChart = false;
  noDataMainLineChart = false;
  showMainPieChart = false;
  mainLineChartJson: object;
  mainPieChartJson: object;
  showCharts = false;
  filtersApplied: object = {};
  noTableData = false;
  finalData: any[];
  footerData: any[];
  exportRequest: ExportRequest = <ExportRequest>{};

  aggTableData: TreeNode[];
  aggTableColumnDef: any[];
  aggTableJson: object;
  isExportReport = false;
  appFeaturesConfigs = {};
  mainCardsConfig = {};

  constructor(
    private appConfigService: AppConfigService,
    private _titleService: Title,
    private libServ: CommonLibService,
    private dataFetchServ: FetchApiDataService,
    private toastService: ToastService,
    private dialogService: DialogService,
    private htmltoimage: HtmltoimageService,
    private pptExport: ExportPptService,
    private exportService: ExportdataService,
    private formatNumPipe: FormatNumPipe
  ) { }

  ngOnInit(): void {
    this.aggTableColumnDef = [
      {
        field: 'time_key',
        displayName: 'Month-Year',
        format: 'date',
        formatConfig: ['MMM-YY'],
        width: '145',
        exportConfig: {
          format: 'date<<MMM-YY<<-',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: '-'
      },
      {
        field: 'account_name',
        displayName: 'Account',
        format: '',
        formatConfig: [],
        width: '145',
        exportConfig: {
          format: '',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: '-'
      },
      {
        field: 'dp_paid_impressions',
        displayName: 'Impressions',
        format: 'number',
        width: '135',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: '0'
      },
      {
        field: 'dp_net_revenue',
        displayName: 'Revenue',
        width: '125',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: '0'

      },
      {
        field: 'dp_clicks',
        displayName: 'Clicks',
        format: 'number',
        width: '135',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: '0'

      },
      {
        field: 'ctr',
        displayName: 'ctr',
        format: 'percentage',
        width: '135',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: '0'
      },
    ];
    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '300px',
      totalRecords: 1000,
      columns: this.aggTableColumnDef.slice(2),
      selectedColumns: this.aggTableColumnDef.slice(2),
      frozenCols: [...this.aggTableColumnDef.slice(0, 2)],
      frozenWidth:
        this.aggTableColumnDef
          .slice(0, 2)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: false,
      columnSearch: false
    };

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        this.isExportReport = this.appConfig['permissions'].some(
          o => o.name === 'post-360-export-reports'
        );
        this._titleService.setTitle(this.appConfig['displayName']);
        let startDate;
        let endDate;
        const date_config = this.appConfig['filter']['filterConfig']['filters'][
          'datePeriod'
        ][0];

        if ((date_config['defaultDate'][0]['value'] === 0 && date_config['defaultDate'][0]['period'] === 'month') && (moment().format('DD') === '01' || moment().format('DD') === '02')) {
          startDate = moment().subtract(1, 'months').startOf('month');
          endDate = moment().subtract(1, 'months').endOf('month');
        } else {
          if (
            date_config[
            'defaultDate'
            ][0]['startOf']
          ) {
            startDate = moment()
              .subtract(
                date_config['defaultDate'][0]['value'],
                date_config['defaultDate'][0]['period']
              )
              .startOf(
                date_config['defaultDate'][0]['period']
              );
          } else {
            startDate = moment().subtract(
              date_config['defaultDate'][0]['value'],
              date_config['defaultDate'][0]['period']
            );
          }
          endDate = moment().subtract(
            date_config[
            'defaultDate'
            ][1]['value'],
            date_config[
            'defaultDate'
            ][1]['period']
          );
        }
        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: startDate.format('YYYYMMDD'),
            time_key2: endDate.format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: [{ key: 'source' }]
        };
        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        this.initialLoading();
      }
    });
  }

  initialLoading() {
    console.log('this.appConfig last up', this.appConfig['id']);
    // this.dataFetchServ
    //   .getLastUpdatedData(this.appConfig['id'])
    //   .subscribe(data => {
    //     if (data['status'] === 0) {
    //       this.toastService.displayToast({
    //         severity: 'error',
    //         summary: 'Server Error',
    //         detail: 'Please refresh the page'
    //       });
    //       console.log(data['status_msg']);
    //       return;
    //     }
    //     this.lastUpdatedOn = moment(data[0]['updated_at']).toDate();
    //     this.nextUpdated = moment(data[0]['next_run_at']).toDate();
    //     this.dataUpdatedThrough = moment(
    //       data[0]['source_updated_through'],
    //       'YYYYMMDD'
    //     ).toDate();
    //   });

    this.libServ.getAppFeaturesConfigs(this.appConfig['id']).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: data['message']
        });
        console.log(data['message']);
        return;
      }
      this.appFeaturesConfigs = data['data'];
      this.appFeaturesConfigs['cardsConfig']['cards'].forEach(element => {
        if (element['name'] === 'main') {
          this.mainCardsConfig = element['config'];
          this.cardsJson = this.mainCardsConfig['list'];
          if (this.mainCardsConfig['display']) {
            this.loadCards();
          }
        }
      });
    });

    const mainLineChartReq = {
      dimensions: ['time_key'],
      metrics: [
        'dp_net_revenue',
        'dp_paid_impressions'
      ],
      derived_metrics: [],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: '',
      isTable: false
    };
    this.loadMainLineChart(mainLineChartReq);

    const mainPieChartReq = {
      dimensions: ['source'],
      metrics: ['dp_net_revenue'],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [],
      limit: '',
      offset: ''
    };
    this.loadMainPieChart(mainPieChartReq);

    this.loadTableData();
  }

  loadTableData() {
    const cardReq = {
      dimensions: [],
      metrics: [
        'dp_net_revenue',
        'dp_paid_impressions',
        'dp_clicks'
      ],
      derived_metrics: ['ctr'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys(),
      orderBy: [],
      limit: '',
      offset: '',
      isTable: false
    };
    const tableReq = {
      dimensions: ['time_key', 'account_name', 'source'], // [this.filtersApplied['groupby'][0]['key']],
      metrics: [
        'dp_clicks', 'dp_net_revenue', 'dp_paid_impressions'
      ],
      derived_metrics: ['ctr'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source', 'account_name'], // grpBys,
      orderBy: [{ key: 'time_key', opcode: 'desc' }],
      limit: '',
      offset: ''
    };
    this.aggTableJson['loading'] = true;
    this.dataFetchServ.getRevMgmtData(tableReq).subscribe(data => {
      this.dataFetchServ.getRevMgmtData(cardReq).subscribe(res => {

        if (data['status'] === 0) {
          this.noTableData = true;
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          return;
        } else {
          this.noTableData = false;
        }
        this.finalData = data['data'];

        const tableData = data['data'];
        const totalFooterData = res['data'];
        console.log("totalFooterData", totalFooterData);

        const arr = [];
        tableData.forEach((row: object) => {
          arr.push({
            data: row,
            children: [{ data: {} }]
          });
        });

        if (totalFooterData[0] !== undefined) {
          for (const key in totalFooterData[0]) {
            if (this.aggTableColumnDef.some(o => o.field == key)) {

              this.aggTableColumnDef.find(x => x.field == key).footerTotal =
                totalFooterData[0][key];

            }
          }
        } else {
          this.aggTableJson['footerColumns'].forEach(c => {
            if (this.aggTableColumnDef.some(o => o.field == c.field)) {
              c['footerTotal'] = 0;
            }
          });
        }
        this.aggTableData = <TreeNode[]>arr;
        this.aggTableJson['totalRecords'] = arr.length;
        setTimeout(() => {
          this.aggTableJson['loading'] = false;
        });
      });
    });
  }

  loadCards() {
    this.showCards = false;
    const apiUrl = this.mainCardsConfig['api']['url'];
    const cardsReq = this.libServ.deepCopy(
      this.mainCardsConfig['api']['request']
    );
    cardsReq['timeKeyFilter'] = this.libServ.deepCopy(
      this.filtersApplied['timeKeyFilter']
    );
    cardsReq['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);

    this.libServ.getDataFromDynamicAPI(apiUrl, cardsReq).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error - Contact to administator',
          detail: data
        });
        console.log(data['status_msg']);
        return;
      }
      if (!this.libServ.isEmptyObj(data['data'])) {
        this.cardsJson.map(o => {
          o['value'] = data['data'][0][o['field']];
        });
      } else {
        this.cardsJson.map(o => {
          o['value'] = 0;
        });
      }
      this.showCards = this.mainCardsConfig['display'];
    });
  }

  loadMainPieChart(params) {
    params['orderBy'] = [{ key: 'dp_net_revenue', opcode: 'desc' }];

    this.mainPieChartJson = {
      chartTypes: [{ key: 'pie', label: 'Pie Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Revenue Distribution Across Source'
        },
        legend: {
          display: false
        },
        elements: {
          arc: {
            borderWidth: 0
          }
        },
        plugins: {
          datalabels: {
            formatter: (value, ctx) => {
              let sum = 0;
              const dataArr = ctx.chart.data.datasets[0].data;
              dataArr.map(data => {
                sum += data;
              });
              const percentage = (value * 100) / sum;
              if (percentage > 10) {
                return `${percentage.toFixed(2)} %`;
                // ${this.formatNumPipe.transform(
                //   value,
                //   '$',
                //   []
                // )} - ${percentage.toFixed(2)} %`;
              } else {
                return '';
              }
            },
            color: 'black'
          }
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };
    this.mainPieChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          let sum = 0;
          const dataArr = data.datasets[0].data;
          dataArr.map(data => {
            sum += data;
          });
          const percentage = (currentValue * 100) / sum;
          return `${
            data.labels[tooltipItem.index]
            } : ${this.formatNumPipe.transform(
              currentValue,
              '$',
              []
            )} - ${percentage.toFixed(2)} %`;
        }
      }
    };
    this.showMainPieChart = false;
    this.dataFetchServ.getRevMgmtData(params).subscribe(data => {
      const chartData = data['data'];
      if (typeof chartData === 'undefined' || !chartData.length) {
        this.noDataPieChart = true;
      } else {
        const sources = Array.from(
          new Set(chartData.map(s => s['source']))
        );
        const colors = this.libServ.dynamicColors(sources.length);
        this.mainPieChartJson['chartData']['labels'] = sources;
        this.mainPieChartJson['chartData']['datasets'][0]['data'] = Array.from(
          new Set(chartData.map(s => s['dp_net_revenue']))
        );
        this.mainPieChartJson['chartData']['datasets'][0][
          'backgroundColor'
        ] = colors;
        this.showMainPieChart = true;
        this.noDataPieChart = false;
      }
    });
  }

  loadMainLineChart(params) {
    const colors = this.libServ.dynamicColors(2);
    this.mainLineChartJson = {
      chartTypes: [{ key: 'bar', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'Revenue',
            type: 'bar',
            yAxisID: 'y-axis-0',

            borderColor: colors[0],
            fill: false,
            backgroundColor: colors[0],
            data: []
          },
          {
            label: 'Impressions',
            type: 'bar',
            yAxisID: 'y-axis-1',

            borderColor: colors[1],
            fill: false,
            backgroundColor: colors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Impressions vs Revenue'
        },
        legend: {
          display: true,
          onClick: e => e.stopPropagation()
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: 'Revenue ($)'
              },
              position: 'right',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', []);
                }
              }
              // scaleFontColor: "rgba(151,137,200,0.8)"
            },
            {
              id: 'y-axis-1',
              scaleLabel: {
                display: true,
                labelString: 'Impressions'
              },
              position: 'left',
              name: '2',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, 'number', [0]);
                }
              }
              // scaleFontColor: "rgba(151,187,205,0.8)"
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[
                tooltipItem.index
                ];
              if (tooltipItem.datasetIndex !== 0) {
                return `${
                  data.datasets[tooltipItem.datasetIndex].label
                  } : ${this.formatNumPipe.transform(
                    currentValue,
                    'number',
                    []
                  )}`;
              }
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    this.showMainLineChart = false;
    this.dataFetchServ.getRevMgmtData(params).subscribe(data => {
      const chartData = data['data'];
      this.noDataMainLineChart = false;
      if (!chartData.length) {
        this.noDataMainLineChart = true;
        return;
      }
      const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));

      this.mainLineChartJson['chartData']['labels'] = datesArr.map(d =>
        moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
      );

      const revArr = [];
      const impArr = [];
      datesArr.forEach(time_key => {
        chartData.forEach(r => {
          if (r['time_key'] === time_key) {
            revArr.push(r['dp_net_revenue']);
            impArr.push(r['dp_paid_impressions']);
          }
        });
      });
      this.mainLineChartJson['chartData']['datasets'][0]['data'] = revArr;
      this.mainLineChartJson['chartData']['datasets'][1]['data'] = impArr;
      this.showMainLineChart = true;
    });
  }

  onFiltersApplied(filterData: object) {
    this.filtersApplied['filters']['dimensions'] = [];
    // tslint:disable-next-line: forin
    for (const k in filterData['filter']['dimensions']) {
      if (filterData['filter']['dimensions'][k].length !== 0) {
        this.filtersApplied['filters']['dimensions'].push({
          key: k,
          values: filterData['filter']['dimensions'][k]
        });
      }
    }

    // this.filtersApplied['groupby'] = filterData['groupby'];
    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];

    if (this.mainCardsConfig['display']) {
      this.mainCardsConfig['api']['request']['gidGroupBy'] = this.getGrpBys();
      this.loadCards();
    }

    const mainLineChartReq = {
      dimensions: ['time_key'],
      metrics: [
        'dp_net_revenue',
        'dp_paid_impressions'
      ],
      derived_metrics: [],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys(),
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: '',
      isTable: false
    };
    this.loadMainLineChart(mainLineChartReq);

    const mainPieChartReq = {
      dimensions: ['source'],
      metrics: ['dp_net_revenue'],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys(),
      orderBy: [],
      limit: '',
      offset: ''
    };
    this.loadMainPieChart(mainPieChartReq);

    this.loadTableData();
  }

  getGrpBys() {
    let grpBys = this.filtersApplied['groupby'].map(e => e.key);

    // let grpBys = [this.filtersApplied['groupby'][0]['key']];
    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          grpBys.concat(
            this.filtersApplied['filters']['dimensions']
              .filter(f => f.values.length)
              .map(m => m.key)
          )
        )
      );
    }
    return grpBys;
  }

  exportTable(table, fileFormat) {
    // console.log('col', this.flatTableColumnDef);
    if (this.exportRequest['sendEmail'].length > 0) {
      this.toastService.displayToast({
        severity: 'info',
        summary: 'Export Report',
        detail:
          'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
        life: 10000
      });
      if (table === 'flat') {
        const columnDefs = this.libServ.deepCopy(
          this.aggTableColumnDef
        );
        const sheetDetailsarray = [];
        const sheetDetails = {};
        sheetDetails['columnDef'] = columnDefs;
        sheetDetails['data'] = this.finalData;
        sheetDetails['sheetName'] = 'Overall KPI Report Distribution';
        sheetDetails['isRequest'] = false;
        sheetDetails['request'] = {
          url: '',
          method: '',
          param: {}
        };
        sheetDetails['disclaimer'] = [
          {
            position: 'top',
            label:
              'Data persent in below table is not final and may varies over period of time',
            color: '#3A37CF'
          },
          {
            position: 'bottom',
            label: 'Thank You',
            color: '#EC6A15'
          }
        ];
        sheetDetails['totalFooter'] = {
          available: false,
          custom: true
        };
        sheetDetails['tableTitle'] = {
          available: true,
          label: 'Overall KPI Report Distribution'
        };
        sheetDetails['image'] = [
          {
            available: true,
            path: environment.exportConfig.exportLogo,
            position: 'top'
          }
        ];
        sheetDetailsarray.push(sheetDetails);
        this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
        this.exportRequest['fileName'] =
          'Overall KPI Report Distribution ' +
          moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
        this.exportRequest['exportFormat'] = fileFormat;
        this.exportRequest['exportConfig'] = environment.exportConfig;
        this.dataFetchServ
          .getExportReportData(this.exportRequest)
          .subscribe(response => {
            console.log(response);



          });
      }
    } else {
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Export Report',
        detail:
          'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
        life: 10000
      });
    }
    return false;
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['name']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    }
    return false;
  }

  sendEmail() {
    const imageConfig = {
      imgName: `${this.appConfig['name']}.png`,
      type: 'image/png'
    };

    this.htmltoimage
      .canvasImage(document.getElementById('capture'), imageConfig)
      .then(canvas => {
        this.dialogService.open(SendEmailComponent, {
          header: 'Email Screenshot',
          contentStyle: {
            'max-height': '80vh',
            width: '30vw',
            overflow: 'auto'
          },
          data: canvas.toDataURL('image/png')
        });
      });
  }

  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }

}
