import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Post360AppComponent } from './post360-app.component';

describe('Post360AppComponent', () => {
  let component: Post360AppComponent;
  let fixture: ComponentFixture<Post360AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Post360AppComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Post360AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
