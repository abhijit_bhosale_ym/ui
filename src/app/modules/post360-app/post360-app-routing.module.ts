import { Routes } from '@angular/router';
import { Post360AppComponent } from './post360-app.component';

export const Post360AppRoutes: Routes = [
  {
    path: '',
    component: Post360AppComponent
  }
];
