import { Routes } from '@angular/router';

import { CampaignMappingComponent } from './campaign-mapping.component';
export const CampaignMappingAppRoutes: Routes = [
  {
    path: '',
    component: CampaignMappingComponent
    // data: {
    //   breadcrumb: 'Yield Management'
    // }
  }
];
