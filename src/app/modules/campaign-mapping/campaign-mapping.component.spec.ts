import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignMappingComponent } from './campaign-mapping.component';

describe('CampaignMappingComponent', () => {
  let component: CampaignMappingComponent;
  let fixture: ComponentFixture<CampaignMappingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignMappingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignMappingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
