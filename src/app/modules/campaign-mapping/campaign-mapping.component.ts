import { Component, OnInit, OnDestroy } from '@angular/core';
import { TreeNode } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { CommonLibService } from 'src/app/_services';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';
import { FetchApiDataService } from './fetch-api-data.service';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { Subscription } from 'rxjs';
import { SendEmailComponent } from '../send-email/send-email.component';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';


@Component({
  selector: 'ym-campaign-mapping',
  templateUrl: './campaign-mapping.component.html',
  styleUrls: ['./campaign-mapping.component.scss']
})
export class CampaignMappingComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;

  appConfig: object = {};
  lastUpdatedOn: Date;
  nextUpdated: Date;
  dataUpdatedThrough: Date;
  aggTableData: TreeNode[];
  noTableData = false;
  dimColDef: any[];
  //  metricColDef: any[];
  aggTableColumnDef: any[];
  aggTableJson: Object;
  displayAggTable = true;

  filtersApplied: object = {};
  exportRequest: ExportRequest = <ExportRequest>{};
  schedularStartDate: any

  campaignOption = [
    { label: 'Select Campaign', value: null },
    { label: 'ABC-Imaginary Mary', value: 'ABC-Imaginary Mary' },
    { label: 'ABC-Fun_and_games', value: 'ABC-Fun_and_games' },
    { label: 'Alphonso Test1', value: 'Alphonso Test' }
  ];

  lineItemOption = [
    { label: 'Select Line Item', value: null },
    { label: 'LID-10000-Amazon-Amazon_Fire_TV-P4', value: 'LID-10000-Amazon-Amazon_Fire_TV-P4' },
    { label: 'LID-1111-Amazon-Amazon_Fire_TV-P4', value: 'LID-1111-Amazon-Amazon_Fire_TV-P4' },
    { label: 'LID-10006-WB-Riverdale_Mid-Season_S3-P2', value: 'LID-10006-WB-Riverdale_Mid-Season_S3-P2' }
  ];

  billingSourceOption = [
    { label: 'Select Data Source', value: null },
    { label: 'AdForm', value: 'AdForm' },
    { label: 'Atlas', value: 'Atlas' },
    { label: 'Beeswax', value: 'Beeswax' }
  ]
  isExportReport = false;
  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private htmltoimage: HtmltoimageService,
    private exportService: ExportdataService,
    private pptExport: ExportPptService
  ) { }

  ngOnInit(): void {

    this.schedularStartDate = new Date();

    this.dimColDef = [
      {
        field: 'line_placement_names',
        displayName: 'Line Placement Name',
        format: '',
        width: '250',
        value: '',
        isExpanded: 'false',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'server_campaign_name',
        displayName: 'Server Campaign Name',
        format: '',
        width: '250',
        isExpanded: 'false',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'trafficking_source_name',
        displayName: 'Data Source',
        format: '',
        width: '140',
        isExpanded: 'false',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'campaign',
        displayName: 'Campaign *',
        format: '',
        width: '170',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'line_item',
        displayName: 'Line Item *',
        width: '155',
        exportConfig: {
          format: '',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'billing_source_name',
        displayName: 'Mappping Source *',
        width: '155',
        exportConfig: {
          format: '',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'start_date',
        displayName: 'Start Date *',
        format: 'date',
        width: '120',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'end_date',
        displayName: 'End Date *',
        format: 'date',
        width: '120',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      }
    ];

    // this.metricColDef = [

    // ];
    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      reload: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,

      columns: this.libServ.deepCopy(this.dimColDef),
      selectedColumns: this.libServ.deepCopy(this.dimColDef),
      frozenCols: this.libServ.deepCopy([...this.dimColDef.slice(0, 0)]),
      frozenWidth:
        this.dimColDef
          .slice(0, 0)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: false,
      columnSearch: true
    };

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        console.log('appConfig', this.appConfig);
        this.isExportReport = this.appConfig['permissions'].some(
          o => o.name === 'campaign-mapping-export-reports'
        );
        this._titleService.setTitle(this.appConfig['displayName']);
        let startDate;
        let endDate;
        // const date_config = this.appConfig['filter']['filterConfig']['filters'][
        //   'datePeriod'
        // ][0];
        // if ((date_config['defaultDate'][0]['value'] === 0 && date_config['defaultDate'][0]['period'] === 'month') && (moment().format('DD') === '01' || moment().format('DD') === '02')) {
        //   startDate = moment().subtract(1, 'months').startOf('month');
        //   endDate = moment().subtract(1, 'months').endOf('month');
        // } else {
        //   if (
        //     date_config[
        //     'defaultDate'
        //     ][0]['startOf']
        //   ) {
        //     startDate = moment()
        //       .subtract(
        //         date_config['defaultDate'][0]['value'],
        //         date_config['defaultDate'][0]['period']
        //       )
        //       .startOf(
        //         date_config['defaultDate'][0]['period']
        //       );
        //   } else {
        //     startDate = moment().subtract(
        //       date_config['defaultDate'][0]['value'],
        //       date_config['defaultDate'][0]['period']
        //     );
        //   }
        //   endDate = moment().subtract(
        //     date_config[
        //     'defaultDate'
        //     ][1]['value'],
        //     date_config[
        //     'defaultDate'
        //     ][1]['period']
        //   );
        // }

        // this.filtersApplied = {
        //   timeKeyFilter: {
        //     // time_key1: startDate.format('YYYYMMDD'),
        //     // time_key2: endDate.format('YYYYMMDD')
        //   },
        //   filters: { dimensions: [], metrics: [] },
        //   groupby: [{ key: 'station_group' }] // this.appConfig["filter"]["filterConfig"]["groupBy"].filter(
        //   // v => v.selected
        //   // )
        // };
        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        this.initialLoading();
      }
    });
  }

  initialLoading() {
    this.loadTableData();
  }

  loadTableData() {
    this.dataFetchServ.getTableData1().subscribe((data) => {
      console.log("data---11", data);

      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      } else {
        this.noTableData = false;
      }
      // const tableData = data['data'];
      const tableData = data as [];
      const arr = [];
      tableData.forEach((row: object) => {
        arr.push({
          data: row,
          children: [{ data: {} }]
        });
      });
      this.aggTableData = <TreeNode[]>arr;
      this.aggTableJson['totalRecords'] = arr.length;
      setTimeout(() => {
        this.aggTableJson['loading'] = false;
      });
    });
  }

  getOptionData() {
    this.dataFetchServ.getTableData1().subscribe((data) => {

    });
  }

  onDateSelected() {
    console.log("date===", this.schedularStartDate);

  }

  afterEditCell(e, rowdata, id, val, field) {
    console.log("field", field);

    // if (e.keyCode === 13) {

    if (typeof rowdata[field] !== 'undefined') {
      this.dataFetchServ.editData(id, rowdata).subscribe(data => {
        console.log("data in cell edit", data);
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          return;
        } else {
          this.toastService.displayToast({
            severity: 'success',
            summary: 'Data Updated',
            detail: 'Data updated successfully!'
          });
        }

        // this.initialLoading();
      });
    }
    // }
  }

  onSearchChanged(searchvalue, column) {
    this.dataFetchServ.getTableData2(searchvalue, column).subscribe((data) => {
      console.log("data---in if", data);

      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      } else {
        this.noTableData = false;
      }
      // const tableData = data['data'];
      const tableData = data as [];
      const arr = [];
      tableData.forEach((row: object) => {
        arr.push({
          data: row,
          children: [{ data: {} }]
        });
      });
      this.aggTableData = <TreeNode[]>arr;
      this.aggTableJson['totalRecords'] = arr.length;
      setTimeout(() => {
        this.aggTableJson['loading'] = false;
      });
    });
  }

  onFiltersApplied(filterData: object) {
    this.filtersApplied['filters']['dimensions'] = [];

    // tslint:disable-next-line: forin
    for (const k in filterData['filter']['dimensions']) {
      if (filterData['filter']['dimensions'][k].length !== 0) {
        this.filtersApplied['filters']['dimensions'].push({
          key: k,
          values: filterData['filter']['dimensions'][k]
        });
      }
    }

    // this.filtersApplied['groupby'] = filterData['groupby'];
    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];

  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['name']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    } else if (type === 'ppt') {
      const chartElements = document.getElementsByTagName('ym-charts');
      const slides = [];
      [].forEach.call(chartElements, (el, index) => {
        const c = el.getElementsByTagName('canvas')[0];

        slides.push({
          title: `${this.appConfig['name']}`,
          base64data: this.libServ.canvasToImage('white', c),
          width: 8,
          height: 3
        });

        if (index === chartElements.length - 1) {
          const pptConfig = {
            filename: 'Charts',
            logo: {
              visible: true,
              path: '/assets/images/ym.png',
              width: 0.53125,
              height: 0.458333
            },
            slides: slides
          };
          this.pptExport.exportPPT(pptConfig);
        }
      });
    }
    return false;
  }

  sendEmail() {
    const imageConfig = {
      imgName: `${this.appConfig['name']}.png`,
      type: 'image/png'
    };

    this.htmltoimage
      .canvasImage(document.getElementById('capture'), imageConfig)
      .then(canvas => {
        this.dialogService.open(SendEmailComponent, {
          header: 'Email Screenshot',
          contentStyle: {
            'max-height': '80vh',
            width: '30vw',
            overflow: 'auto'
          },
          data: canvas.toDataURL('image/png')
        });
      });
  }
  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }
}
