import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,HttpParams} from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { TreeNode } from 'primeng/api';
import {Observable} from 'rxjs'
// import { RequestOptions } from '@angular/http';
import { HttpRequest} from '@angular/common/http';



const headerOption = {
  headers : new HttpHeaders({'Content-Type':'application/json                                                                                                 '})
}



@Injectable({
  providedIn: 'root'
})
export class FetchApiDataService {
  private BASE_URL: string = environment.baseUrl;
  // private options = new HttpRequest({headers: new Headers({'Content-Type': 'application/json'})});
   private options =  {headers: new  HttpHeaders({ 'Content-Type': 'application/json'})};
   val ="";

  url =  "http://localhost:3000/data";
  url1=  "http://localhost:3000/optionData";
  constructor(private http: HttpClient) { }

  getTableData1():Observable<Object>
  {
    return this.http.get<Object>(this.url);
  }

  getTableData2(value,col):Observable<Object>
  {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');  
    let params = new HttpParams();
    params = params.append(col, value);
    console.log("httpOptions",params);
    return this.http.get<Object>(this.url,{params: params});
  }

  getTableData3(value,col):Observable<Object>
  {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');  
    let params = new HttpParams();
    params = params.append(col, value);
    console.log("httpOptions",params);
    return this.http.get<Object>(this.url1,{params: params});
  }

  editCellData(id1,rowdata):Observable<Object>
  {
    return this.http.put<Object>(this.url + '/' +id1,rowdata,headerOption);
  }

  editData(id,rowdata):Observable<object>
  {
    return this.http.put<Object>(this.url + '/' +id,rowdata,headerOption);
  }

  getChildData1():Observable<Object> {
    return this.http.get<Object>(this.url1);
  }

  getDailyData():Observable<Object>
  {
    return this.http.get<Object>("http://localhost:3000/campaigns");
  }

  getDailyLineItems():Observable<Object>
  {
    return this.http.get<Object>("http://localhost:3000/dailyLineItems");
  }

  getTeamMembers():Observable<Object>
  {
    return this.http.get<Object>("http://localhost:3000/teamMembers");
  }



 
  getExportReportData(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/common/exportData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
    // return <TreeNode[]> json.data;
  }
  getChildData(params: object) {
    const url = `${this.BASE_URL}/dummyData/childTable`;
    return this.http.get(url, params);
    // return <TreeNode[]> json.data;
  }

  getLastUpdatedData(appId) {
    const url = `${this.BASE_URL}/frankly/v1/common/getLastUpdatedData/${appId}`;
    return this.http.get(url);
  }
}
