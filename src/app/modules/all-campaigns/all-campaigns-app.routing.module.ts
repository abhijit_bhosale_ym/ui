import { Routes } from '@angular/router';

import { AllCampaignsComponent } from './all-campaigns.component';
export const AllCampaignAppRoutes: Routes = [
  {
    path: '',
    component: AllCampaignsComponent
  }
];
