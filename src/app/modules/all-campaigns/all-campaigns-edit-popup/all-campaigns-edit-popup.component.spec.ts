import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignEditPopupComponent } from './campaign-edit-popup.component';

describe('CampaignEditPopupComponent', () => {
  let component: CampaignEditPopupComponent;
  let fixture: ComponentFixture<CampaignEditPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CampaignEditPopupComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignEditPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
