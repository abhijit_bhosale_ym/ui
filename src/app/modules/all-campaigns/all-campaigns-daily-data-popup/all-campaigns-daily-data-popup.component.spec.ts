import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignDailyDataPopupComponent } from './campaign-daily-data-popup.component';

describe('Rev360DailyDataPopupComponent', () => {
  let component: CampaignDailyDataPopupComponent;
  let fixture: ComponentFixture<CampaignDailyDataPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CampaignDailyDataPopupComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignDailyDataPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
