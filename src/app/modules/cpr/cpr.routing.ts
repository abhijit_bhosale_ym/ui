import { Routes } from '@angular/router';

import { CprComponent } from './cpr.component';
export const CPRRoutes: Routes = [
  {
    path: '',
    component: CprComponent
  }
];
