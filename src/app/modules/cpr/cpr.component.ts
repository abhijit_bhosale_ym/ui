import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, from, config } from 'rxjs';
import { Title } from '@angular/platform-browser';
import { TreeNode } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import * as moment from 'moment';

import { CommonLibService, AppConfigService } from 'src/app/_services';
import { FetchApiDataService } from './fetch-api-data.service';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { CommentComponent } from './comment/comment.component';
import { DailyDataComponent } from './daily-data/daily-data.component';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { ChatComponent } from '../common/chat/chat/chat.component';
import { CommentJson } from 'src/app/_interfaces/commentJson';
import { CommentService } from '../common/chat/comment.service';
import { stringify } from 'querystring';
import { timeout } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-cpr',
  templateUrl: './cpr.component.html',
  styleUrls: ['./cpr.component.scss']
})
export class CprComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;

  appConfig: object = {};
  lastUpdatedOn: Date;
  aggTableData: TreeNode[];
  noTableData = false;
  dimColDef: any[];
  metricColDef: any[];
  aggTableColumnDef: any[];
  aggTableJson: Object;
  displayAggTable = true;
  toggleView = true;
  activeView = true;
  tableRequestParam = {};
  favoriteCollection = {};
  filtersApplied: object = {};
  bellIconLoaded = false;
  isExportReport = false;
  allStatus: any;
  // messages = [];
  // unreadCount = 0;
  commentData = [];
  chatcount = 0;
  nextUpdated: Date;
  dataUpdatedThrough: Date;
  exportRequest: ExportRequest = <ExportRequest>{};
  commentJson: CommentJson = <CommentJson>{};
  timeout:any;
  tableReq: object;
  searchvalue="";
  searchTableDef: object;


  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private toastService: ToastService,
    private dialogService: DialogService,
    private formatNumPipe: FormatNumPipe,
    private dataFetchServ: FetchApiDataService,
    private exportService: ExportdataService,
    private pptExport: ExportPptService,
    private htmltoimage: HtmltoimageService,

    private commentservice: CommentService,
  ) { }

  ngOnInit() {
    this.dimColDef = [
      {
        field: 'drill_key',
        displayName: 'Order/LI ID',
        format: '',
        width: '250',
        defaultValue: [
          {
            condition: [null],
            value: 'N/A'
          }
        ],
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'drill_name',
        displayName: 'Order/LI Name',
        format: '',
        width: '250',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      }
    ];

    this.metricColDef = [
      {
        field: 'advertiser',
        displayName: 'Advertiser',
        format: '',
        width: '250',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'favorite',
        displayName: 'Fav.',
        format: '',
        width: '80',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'comments',
        displayName: 'Comments',
        format: '',
        width: '100',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'OSI',
        displayName: 'OSI',
        width: '155',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'percentage',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: '3rd_party_OSI',
        displayName: '3rd Party OSI',
        width: '155',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'percentage',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'revenue_at_risk',
        displayName: 'Revenue At Risk',
        width: '155',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: '3rd_party_revenue_at_risk',
        displayName: '3rd Party Revenue At Risk',
        width: '155',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'start_date_key',
        displayName: 'Start Date',
        width: '155',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'date',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'end_date_key',
        displayName: 'End Date',
        width: '155',
        defaultValue: [
          {
            condition: ['Unlimited'],
            value: 'Unlimited'
          }
        ],
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'date',
        formatConfig: ['mixed', 'Unlimited'],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'contracted_quantity',
        displayName: 'Contracted Quantity',
        width: '155',
        defaultValue: [
          {
            condition: ['Unlimited'],
            value: 'Unlimited'
          },
          {
            condition: ['N/A'],
            value: 'N/A'
          }
        ],
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'goal_quantity',
        displayName: 'Goal Quantity',
        width: '155',
        defaultValue: [
          {
            condition: ['Unlimited'],
            value: 'Unlimited'
          },
          {
            condition: ['N/A'],
            value: 'N/A'
          }
        ],
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'impressions',
        displayName: 'Impressions',
        width: '155',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: '3rd_party_impressions',
        displayName: '3rd Party Impressions',
        width: '155',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'remaining_impressions',
        displayName: 'Remaining Impressions',
        width: '155',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'days_remaining',
        displayName: 'Days Remaining',
        width: '155',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'clicks',
        displayName: 'Clicks',
        width: '155',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: '3rd_party_clicks',
        displayName: '3rd Party Clicks',
        width: '155',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'ctr',
        displayName: 'CTR',
        width: '155',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'percentage',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: '3rd_party_ctr',
        displayName: '3rd Party CTR',
        width: '155',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'percentage',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'cpm',
        displayName: 'CPM',
        width: '155',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'revenue',
        displayName: 'Revenue',
        width: '155',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: '3rd_party_revenue',
        displayName: '3rd Party Revenue',
        width: '155',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'days_passed',
        displayName: 'Days Passed',
        width: '155',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'source',
        displayName: 'Source',
        format: '',
        width: '250',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'lineitemtype',
        displayName: 'Line Item Type',
        format: '',
        width: '250',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'line_item_priority',
        displayName: 'Line Priority',
        format: '',
        width: '250',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'trafficker',
        displayName: 'Trafficker',
        format: '',
        width: '250',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'cost_type',
        displayName: 'Cost Type',
        format: '',
        width: '250',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'in_view_impressions',
        displayName: 'In View Impressions',
        width: '155',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'in_view_rate',
        displayName: 'In View Rate',
        width: '155',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'campaign_length',
        displayName: 'Campaign Length',
        width: '155',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'expected_actual_revenue',
        displayName: 'Expected Actual Revenue',
        width: '155',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'revenue_to_be_delivered',
        displayName: 'Revenue To Be Delivered',
        width: '155',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      }
    ];

    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: false,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        this.isExportReport = this.appConfig['permissions'].some(
          o => o.name === 'cpr-export-report'
        );
        this._titleService.setTitle(this.appConfig['displayName']);
        this.filtersApplied = {
          filters: { dimensions: [], metrics: [] },
          groupby: []
        };
        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');

        this.commentJson['app_id'] = this.appConfig['id'];
        this.commentJson['user_id'] = this.appConfig['user']['id'];
        this.commentJson['receiver'] = this.appConfig['route'];
        this.commentJson['user_name'] = this.appConfig['user']['userName'];
        // this.bellIconLoaded = true;
        this.initialLoading();
      }
    });

    const finalColDef = this.libServ.deepCopy(this.metricColDef);

    finalColDef.splice(0, 0, ...this.dimColDef);
    this.aggTableColumnDef = finalColDef.slice();

    this.aggTableJson['columns'] = this.aggTableColumnDef.slice(
      this.dimColDef.length
    );
    this.aggTableJson['selectedColumns'] = this.aggTableColumnDef.slice(
      this.dimColDef.length
    );
    this.aggTableJson['frozenCols'] = [
      ...this.aggTableColumnDef.slice(0, this.dimColDef.length)
    ];
    this.aggTableJson['frozenWidth'] =
      this.aggTableColumnDef
        .slice(0, this.dimColDef.length)
        .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px';

    this.tableRequestParam = {
      dimensions: [],
      metrics: [],
      derived_metrics: [],
      timeKeyFilter: {},
      filters: { dimensions: [], metrics: [] },
      groupByTimeKey: { key: [], interval: 'daily' },
      gidGroupBy: '',
      orderBy: [
        { key: !this.toggleView ? 'lineitem' : 'order_name', opcode: 'asc' }
      ],
      limit: '0',
      offset: '-1'
    };
  }

  osiProgressClass(row, col) {
    let tagClass = 'badge ';
    const value = row[col];
    if (value === 'N/A' || value === null || typeof value === 'undefined') {
      tagClass = 'badge badge-secondary';
    } else {
      const cellValue = parseFloat(value);
      if (cellValue === 0.0) {
        tagClass = 'badge badge-secondary';
      } else if (cellValue < 75) {
        tagClass = 'badge badge-danger';
      } else if (cellValue >= 75 && cellValue < 80) {
        tagClass = 'badge badge-warning';
      } else if (cellValue >= 80 && cellValue < 95) {
        tagClass = 'badge badge-primary';
      } else if (cellValue >= 95 && cellValue < 105) {
        tagClass = 'badge badge-success';
      } else if (cellValue >= 105) {
        tagClass = 'badge badge-secondary';
      }
    }
    return tagClass;
  }

  initialLoading() {
    this.UpdateBellIcon();
    this.dataFetchServ
      .getLastUpdatedData(this.appConfig['id'])
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          return;
        }
        this.lastUpdatedOn = moment(data[0]['updated_at']).toDate();
        this.nextUpdated = moment(data[0]['next_run_at']).toDate();
        this.dataUpdatedThrough = moment(
          data[0]['source_updated_through'],
          'YYYYMMDD'
        ).toDate();
      });
    this.dataFetchServ.getFavorite().subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      }
      this.favoriteCollection = data['data'];
      this.tableReq = this.libServ.deepCopy(this.tableRequestParam);
      this.tableReq['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);
      this.tableReq['dimensions'].push('0 As $$treeLevel');
      this.tableReq['gidGroupBy'] = [
        'advertiser',
        'order_name',
        'source',
        'trafficker'
      ];
      this.tableReq['limit'] = 10;
      this.tableReq['offset'] = 0;
      this.tableReq['orderBy'] = [{ key: 'order_name', opcode: 'asc' }];
      this.loadTableData(this.tableReq);
    });
    // this.dataFetchServ.getCommentsCountData().subscribe(res => {
    //   this.unreadCount = res['data']['count'];
    //   this.fetchComments();
    // });
  }

  isFavorite(row, col) {
    if (typeof row['$$treeLevel'] === 'undefined') {
      if (
        this.favoriteCollection['lineItems'].findIndex(
          itm => itm === row['lineitem']
        ) !== -1
      ) {
        return 'fav fas fa-star';
      }
      return 'fav far fa-star';
    } else {
      if (
        this.favoriteCollection['orders'].findIndex(
          itm => itm === row['order_name']
        ) !== -1
      ) {
        return 'fav fas fa-star';
      }
      return 'fav far fa-star';
    }
  }

  UpdateBellIcon() {
    const req1 = {
      user_id: this.appConfig['user']['id'],
      app_id: this.appConfig['id']
    };

    this.commentservice
      .getAppBellCommentData(req1)
      .subscribe(commentData => {
        if (commentData['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          return;
        }
        this.bellIconLoaded = false;
        this.commentData = commentData['data'];
        this.chatcount = this.commentData.filter(x => x.is_read === 'N').length;
        setTimeout(() => {
          this.bellIconLoaded = true;
        }, 0);


      });
  }

  clickOnComment(comment: object) {
    console.log('comment......', comment);
    const req1 = {
      user_id: this.appConfig['user']['id'],
      app_id: this.appConfig['id']
    };

    this.commentservice
      .getAppCommentData(req1)
      .subscribe(commentData => {
        if (commentData['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          return;
        }
        // this.commentData = commentData['data'];
        if (typeof comment['config'] == 'string') {
          this.commentJson['config'] = JSON.parse(comment['config']);
        } else {
          this.commentJson['config'] = comment['config'];
        }

        const data = {};
        const CommentSortData = [];
        commentData['data'].forEach(element => {
          const commentconfig = JSON.parse(element.config);
          if (
            this.commentJson['config']['order_name'] == commentconfig.order_name
            && this.commentJson['config']['lineitem'] == commentconfig.lineitem
          ) {
            CommentSortData.push(element);
          }
        });
        data['commentJson'] = this.commentJson;
        data['commentData'] = CommentSortData;
        const chatSetting = {
          showChatMsg: false,
          title: `${this.commentJson['config']['comment_on'] == 'order_name'
            ? this.commentJson['config']['order_name']
            : this.commentJson['config']['lineitem']
            }`,
          showCommentTextArea: true,
          chatURL: '/cpr/send/message',
          data: data
        };
        console.log('chatSetting', chatSetting);

        const ref = this.dialogService.open(ChatComponent, {
          header: `Comment on ${
            this.commentJson['config']['comment_on'] == 'order_name'
              ? this.commentJson['config']['order_name']
              : this.commentJson['config']['lineitem']
            }`,
          contentStyle: { width: '30vw', height: '370px', overflow: 'auto' },
          data: chatSetting,
          width: '30vw'
        });

        ref.onClose.subscribe(() => {
          // let UnreadCommentIdList = CommentSortData.filter(o => o.is_read === "N"
          // && o.user_id != this.commentJson['user_id']).map(x => x.comment_id);

          // console.log('popup close', CommentSortData, UnreadCommentIdList);
          // if (UnreadCommentIdList.length > 0) {
          this.UpdateBellIcon();
          // }
        });
      });

  }

  addComment(row) {
    const req1 = {
      user_id: this.appConfig['user']['id'],
      app_id: this.appConfig['id']
    };

    this.commentservice
      .getAppCommentData(req1)
      .subscribe(commentData => {
        if (commentData['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          return;
        }
        const data = {};
        const CommentSortData = [];
        const config = {
          comment_on: typeof row['$$treeLevel'] !== 'undefined' ? 'order_name' : 'lineitem',
          order_name: row['order_name'],
          lineitem: row['lineitem']
        };
        commentData['data'].forEach(element => {
          const commentconfig = JSON.parse(element.config);
          if (config.order_name == commentconfig.order_name && config.lineitem == commentconfig.lineitem) {
            CommentSortData.push(element);
          }
        });

        this.commentJson['config'] = config;

        data['commentJson'] = this.commentJson;
        data['commentData'] = CommentSortData;
        const chatSetting = {
          showChatMsg: false,
          title: `${
            typeof row['$$treeLevel'] !== 'undefined'
              ? row['order_name']
              : row['lineitem']
            }`,
          showCommentTextArea: true,
          chatURL: '/cpr/send/message',
          data: data
        };

        const ref = this.dialogService.open(ChatComponent, {
          header: `Comment on ${
            typeof row['$$treeLevel'] !== 'undefined'
              ? row['order_name']
              : row['lineitem']
            }`,
          contentStyle: { width: '30vw', height: '370px', overflow: 'auto' },
          data: chatSetting,
          width: '30vw'
        });

        ref.onClose.subscribe((data: string) => {
          // let UnreadCommentIdList = CommentSortData.filter(o => o.is_read === "N" && o.user_id != this.commentJson['user_id']).map(x => x.comment_id);
          // console.log('popup close', CommentSortData,UnreadCommentIdList);
          // if (UnreadCommentIdList.length > 0) {
          this.UpdateBellIcon();
          // }
        });
      });

  }

  // fetchComments() {
  //   this.messages = [];
  //   this.dataFetchServ.getCommentsData().subscribe(res => {
  //     if (res['status'] === 0) {
  //       this.toastService.displayToast({
  //         severity: 'error',
  //         summary: 'Server Error',
  //         detail: 'Please refresh the page'
  //       });
  //       return;
  //     }
  //     this.messages = res['data'];
  //   });
  // }

  toggleFavorite(row) {
    if (typeof row['$$treeLevel'] === 'undefined') {
      const index = this.favoriteCollection['lineItems'].findIndex(
        itm => itm === row['lineitem']
      );
      if (index !== -1) {
        const req = {
          lineitem: row['lineitem'],
          opcode: 'delete'
        };
        this.dataFetchServ.setFavouriteLine(req).subscribe(data => {
          if (data['status'] === 0) {
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Server Error',
              detail: 'Please refresh the page'
            });
            return;
          }
          this.favoriteCollection['lineItems'].splice(index, 1);
        });
      } else {
        const req = {
          lineitem: row['lineitem'],
          opcode: 'insert'
        };
        this.dataFetchServ.setFavouriteLine(req).subscribe(data => {
          if (data['status'] === 0) {
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Server Error',
              detail: 'Please refresh the page'
            });
            return;
          }
          this.favoriteCollection['lineItems'].push(row['lineitem']);
        });
      }
    } else {
      const index = this.favoriteCollection['orders'].findIndex(
        itm => itm === row['order_name']
      );
      if (index !== -1) {
        const req = {
          orderName: row['order_name'],
          opcode: 'delete'
        };
        this.dataFetchServ.setFavourite(req).subscribe(data => {
          if (data['status'] === 0) {
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Server Error',
              detail: 'Please refresh the page'
            });
            return;
          }
          this.favoriteCollection['orders'].splice(index, 1);
        });
      } else {
        const req = {
          orderName: row['order_name'],
          opcode: 'insert'
        };
        this.dataFetchServ.setFavourite(req).subscribe(data => {
          if (data['status'] === 0) {
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Server Error',
              detail: 'Please refresh the page'
            });
            return;
          }
          this.favoriteCollection['orders'].push(row['order_name']);
        });
      }
    }
  }

  reLoadTableData() {
    this.searchvalue = "";
    this.tableReq = this.libServ.deepCopy(this.tableRequestParam);
    this.tableReq['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);
    if (!this.toggleView) {
      this.tableReq['gidGroupBy'] = [
        'advertiser',
        'costtype',
        'line_item_priority',
        'lineitem',
        'lineitemtype',
        'order_name',
        'source',
        'trafficker'
      ];
      this.tableReq['limit'] = 10;
      this.tableReq['offset'] = 0;
      this.tableReq['orderBy'] = [{ key: 'lineitem', opcode: 'asc' }];
    } else {
      this.tableReq['dimensions'].push('0 As $$treeLevel');
      this.tableReq['gidGroupBy'] = [
        'advertiser',
        'order_name',
        'source',
        'trafficker'
      ];
      this.tableReq['limit'] = 10;
      this.tableReq['offset'] = 0;
      this.tableReq['orderBy'] = [{ key: 'order_name', opcode: 'asc' }];
    }
    this.aggTableJson['lazy'] = false;
    this.displayAggTable = false;
    setTimeout(() => {
      this.displayAggTable = true;
    }, 0);
    this.loadTableData(this.tableReq);
  }

  openPopup(row, col) {
    // console.log('row', row);
    const data = {
      row: row,
      exportRequest: this.exportRequest,
      isExportReport: this.isExportReport
    };
    const ref = this.dialogService.open(DailyDataComponent, {
      header: !this.toggleView
        ? `Line Item Name : ${row['lineitem']}`
        : typeof row['$$treeLevel'] === 'undefined'
          ? `Line Item Name : ${row['lineitem']}`
          : `Order Name : ${row['order_name']}`,
      contentStyle: { width: '80vw', height: '570px', overflow: 'auto' },
      data: data
    });
  }

  loadTableData(tableReq) {
    setTimeout(() => {
      this.aggTableJson['loading'] = true;
    });
    // this.reLoadTableData();
    this.dataFetchServ
      .getTableData(tableReq, this.activeView)
      .subscribe(data => {
        if (data['status'] === 0) {
          this.noTableData = true;
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          return;
        } else {
          this.noTableData = false;
        }
        const tableData = data['data'];
        console.log("tableData", tableData);

        const arr = [];
        tableData.forEach((row: object) => {
          arr.push({
            data: row,
            children: [{ data: {} }]
          });
        });
        this.aggTableData = <TreeNode[]>arr;
        this.aggTableJson['totalRecords'] = data['totalItems'];
        setTimeout(() => {
          this.aggTableJson['loading'] = false;
        });
        this.aggTableJson['lazy'] = true;
      });
  }

  onTableDrill(e: Event) {
    if (!e['node']['childLoaded']) {
      setTimeout(() => {
        this.aggTableJson['loading'] = true;
      });
      const tableReq = this.libServ.deepCopy(this.tableRequestParam);
      tableReq.filters = this.libServ.deepCopy(this.filtersApplied['filters']);
      if (
        tableReq['filters']['dimensions'].findIndex(
          itm => itm.key === 'order_name'
        ) === -1
      ) {
        tableReq['filters']['dimensions'].push({
          key: 'order_name',
          values: [e['node']['data']['order_name']]
        });
      } else {
        tableReq['filters']['dimensions'].splice(
          tableReq['filters']['dimensions'].findIndex(
            itm => itm.key === 'order_name'
          ),
          1
        );
        tableReq['filters']['dimensions'].push({
          key: 'order_name',
          values: [e['node']['data']['order_name']]
        });
      }
      tableReq.gidGroupBy = [
        'advertiser',
        'costtype',
        'line_item_priority',
        'lineitem',
        'lineitemtype',
        'order_name',
        'source',
        'trafficker'
      ];
      tableReq['limit'] = '-1';
      tableReq['offset'] = '-1';
      tableReq['orderBy'] = [{ key: 'lineitem', opcode: 'asc' }];
      this.dataFetchServ
        .getTableData(tableReq, this.allStatus)
        .subscribe(data => {
          if (data['status'] === 0) {
            // this.noTableData = true;
            setTimeout(() => {
              this.aggTableJson['loading'] = false;
            });
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Server Error',
              detail: 'Please refresh the page'
            });
            return;
          } else {
            // this.noTableData = false;
          }
          const tableData = data['data'];
          const arr = [];
          tableData.forEach((row: object) => {
            arr.push({
              data: row
            });
          });
          e['node']['children'] = arr;
          this.aggTableData = [...this.aggTableData];
          e['node']['childLoaded'] = true;
          setTimeout(() => {
            this.aggTableJson['loading'] = false;
          });
        });
    }
  }

  onLazyLoadAggTable(e: Event) {
    if (this.aggTableJson['lazy']) {
      this.tableReq = this.libServ.deepCopy(this.tableRequestParam);
      this.tableReq['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);
      if(this.searchvalue !== "")
      {
        this.tableReq['filters']['globalSearch'] = {
          dimensions: this.searchTableDef,
          value: this.searchvalue
        };
      }
      if (!this.toggleView) {
        this.tableReq['gidGroupBy'] = [
          'advertiser',
          'costtype',
          'line_item_priority',
          'lineitem',
          'lineitemtype',
          'order_name',
          'source',
          'trafficker'
        ];
      } else {
        this.tableReq['dimensions'].push('0 As $$treeLevel');
        this.tableReq['gidGroupBy'] = [
          'advertiser',
          'order_name',
          'source',
          'trafficker'
        ];
      }
      const orderby = [];
      if (
        typeof e['multiSortMeta'] !== 'undefined' &&
        e['multiSortMeta'] !== null
      ) {
        e['multiSortMeta'].forEach(sort => {
          orderby.push({
            key: sort['field'],
            opcode: sort['order'] === -1 ? 'desc' : 'asc'
          });
        });
      }
      this.tableReq['limit'] = e['rows'];
      this.tableReq['offset'] = e['first'];
      this.tableReq['orderBy'] = orderby;      
      this.loadTableData(this.tableReq);
    }
  }

  isHiddenColumn(col: Object) {
    return false;
  }

  exportTable(fileFormat) {
    const data = [];
    if (this.exportRequest['sendEmail'].length > 0) {
      this.toastService.displayToast({
        severity: 'info',
        summary: 'Export Report',
        detail:
          'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
        life: 10000
      });
      const finalColDef = this.libServ.deepCopy(this.metricColDef);
      finalColDef.splice(0, 0, ...this.dimColDef);
      finalColDef.splice(
        finalColDef.findIndex(itm => itm.field === 'favorite'),
        2
      );
      const tableoederReq = this.libServ.deepCopy(this.tableRequestParam);
      tableoederReq.filters = { dimensions: [], metrics: [] };
      tableoederReq.gidGroupBy = [
        'advertiser',
        'order_name',
        'source',
        'trafficker'
      ];
      tableoederReq['limit'] = '-1';
      tableoederReq['offset'] = '-1';
      tableoederReq['orderBy'] = [{ key: 'order_name', opcode: 'asc' }];
      tableoederReq['isTable'] = false;
      const tableLineReq = this.libServ.deepCopy(tableoederReq);
      tableLineReq.gidGroupBy = [
        'advertiser',
        'costtype',
        'line_item_priority',
        'lineitem',
        'lineitemtype',
        'order_name',
        'source',
        'trafficker'
      ];
      const sheetDetailsarray = [];
      const sheetDetails = {};
      sheetDetails['columnDef'] = finalColDef;
      sheetDetails['data'] = [];
      sheetDetails['sheetName'] = 'Order Distribution';
      sheetDetails['isRequest'] = true;
      sheetDetails['request'] = {
        url: 'active',
        method: 'POST',
        param: tableoederReq
      };
      sheetDetails['disclaimer'] = [
        {
          position: 'top',
          label:
            'Data persent in below table is not final and may varies over period of time',
          color: '#3A37CF'
        },
        {
          position: 'bottom',
          label: 'Thank You',
          color: '#EC6A15'
        }
      ];
      sheetDetails['totalFooter'] = {
        available: false,
        custom: false
      };
      sheetDetails['tableTitle'] = {
        available: true,
        label: 'Order Distribution'
      };
      sheetDetails['image'] = [
        {
          available: true,
          path: environment.exportConfig.exportLogo,
          position: 'top'
        }
      ];

      const sheetlineDetails = {};
      sheetlineDetails['columnDef'] = finalColDef;
      sheetlineDetails['data'] = [];
      sheetlineDetails['sheetName'] = 'Line Distribution';
      sheetlineDetails['isRequest'] = true;
      sheetlineDetails['request'] = {
        url: 'active',
        method: 'POST',
        param: tableLineReq
      };
      sheetlineDetails['disclaimer'] = [
        {
          position: 'bottom',
          label: 'Note: Data present in the table may vary over a period of time.',
          color: '#000000'
        }
      ];
      sheetlineDetails['totalFooter'] = {
        available: false,
        custom: false
      };
      sheetlineDetails['tableTitle'] = {
        available: false,
        label: 'Line Distribution'
      };
      sheetlineDetails['image'] = [];

      sheetDetailsarray.push(sheetDetails, sheetlineDetails);
      this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
      this.exportRequest['fileName'] =
        'CPR Breackdown ' + moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
      this.exportRequest['exportFormat'] = fileFormat;
      this.exportRequest['exportConfig'] = environment.exportConfig;
      // console.log('exportreport', this.exportRequest);
      // return false;
      this.dataFetchServ
        .getExportReportData(this.exportRequest)
        .subscribe(response => {
          console.log(response);
        });
    } else {
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Export Report',
        detail:
          'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email.Thanks!',
        life: 10000
      });
    }
  }

  onFiltersApplied(filterData: object) {
    this.searchvalue = "";
    this.filtersApplied['filters']['dimensions'] = [];
    for (const k in filterData['filter']['dimensions']) {
      if (filterData['filter']['dimensions'][k].length !== 0) {
        this.filtersApplied['filters']['dimensions'].push({
          key: k,
          values: filterData['filter']['dimensions'][k]
        });
      }
    }
    this.tableReq = this.libServ.deepCopy(this.tableRequestParam);
    this.tableReq['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);
    if (!this.toggleView) {
      this.tableReq['gidGroupBy'] = [
        'advertiser',
        'costtype',
        'line_item_priority',
        'lineitem',
        'lineitemtype',
        'order_name',
        'source',
        'trafficker'
      ];
      this.tableReq['limit'] = 10;
      this.tableReq['offset'] = 0;
      this.tableReq['orderBy'] = [{ key: 'lineitem', opcode: 'asc' }];
    } else {
      this.tableReq['dimensions'].push('0 As $$treeLevel');
      this.tableReq['gidGroupBy']= [
        'advertiser',
        'order_name',
        'source',
        'trafficker'
      ];
      this.tableReq['limit'] = 10;
      this.tableReq['offset'] = 0;
      this.tableReq['orderBy'] = [{ key: 'order_name', opcode: 'asc' }];
    }
    this.aggTableJson['lazy'] = false;
    this.loadTableData(this.tableReq);
  }

  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }

  onGlobalSearchChanged(searchValue, tableColDef) {
    // this.searchvalue = searchValue;
    tableColDef = tableColDef.filter(x=>x.field !=="drill_key" && x.field !== "drill_name" && x.field !=="favorite" &&  
    x.field !=="comments" && x.field !=="3rd_party_revenue_at_risk" && x.field !=="cost_type" && 
    x.field !== "expected_actual_revenue" && x.field !=="revenue_to_be_delivered" && x.field !=="lineitemtype" && 
    x.field !=="line_item_priority")
    if (this.timeout) {
       clearTimeout(this.timeout);
    }
    this.timeout = setTimeout(() => {
      this.tableReq['filters']['globalSearch'] = {
        dimensions: [],
        value: searchValue
      };
      tableColDef.forEach(element => {
        this.tableReq['filters']['globalSearch']['dimensions'].push(
          element.field
        );
      });
      if(this.toggleView)
      {
        this.tableReq['filters']['globalSearch']['dimensions'].push(
         "order_key","order_name"
        ); 
      } else {
        this.tableReq['filters']['globalSearch']['dimensions'].push(
          "lineitem_key","lineitem","costtype","lineitemtype","line_item_priority"
         ); 
      }
      this.searchTableDef = this.tableReq['filters']['globalSearch']['dimensions'];
      this.loadTableData(this.tableReq);
    }, 3000);
  }
}
