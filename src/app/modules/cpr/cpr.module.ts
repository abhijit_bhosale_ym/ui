import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FilterContainerModule } from '../common/filter-container/filter-container.module';
import { CPRRoutes } from './cpr.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { SharedModule } from 'src/app/_pipes/shared.module';
import { ChatModule } from '../common/chat/chat.module';
import { TreeTableModule } from 'primeng/treetable';
import { MultiSelectModule } from 'primeng/multiselect';
import { InputSwitchModule } from 'primeng/inputswitch';
import { CprComponent } from './cpr.component';

@NgModule({
  declarations: [CprComponent],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    FilterContainerModule,
    TreeTableModule,
    MultiSelectModule,
    InputSwitchModule,
    ChatModule,
    RouterModule.forChild(CPRRoutes)
  ],
  providers: [FormatNumPipe]
})
export class CprModule {}
