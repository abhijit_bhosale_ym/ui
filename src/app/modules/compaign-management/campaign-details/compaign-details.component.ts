import { Component, OnInit } from '@angular/core';
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder
} from '@angular/forms';
import {
  Router,
  ActivatedRoute,
  NavigationExtras,
  NavigationEnd,
  RouterEvent
} from '@angular/router';
import { TreeNode } from 'primeng/api';
import { DialogService} from 'primeng/dynamicdialog';

import {TagCodePopupComponent} from '../tag-code-popup/tag-code-popup.component'

@Component({
  selector: 'ym-compaign-details',
  templateUrl: './compaign-details.component.html',
  styleUrls: ['./compaign-details.component.scss']
})
export class CompaignDetailsComponent implements OnInit {
  addCompaignForm: FormGroup;
  addPlacementForm: FormGroup;
  updatePlacementForm: any = [];
  showCompaign: boolean = false;
  filtersApplied: object = {};
  DesktopDimColDef: any[];
  DesktopaggTableJson: Object;
  DesktopaggTableData: TreeNode[];

  DesktopTableJson: Object;
  DesktopTableData: TreeNode[];

  mobileDimColDef: any[];
  mobileaggTableJson: Object;
  mobileaggTableData: TreeNode[];

  mobileTableJson: Object;
  mobileTableData: TreeNode[];

  tabletDimColDef: any[];
  tabletaggTableJson: Object;
  tabletaggTableData: TreeNode[];

  tabletTableJson: Object;
  tabletTableData: TreeNode[];
  // noTableData = true;
  showPlacement = false;
  showAddPlacement = false;
  totalPalcements = [{id : 1,placement_name :"abc"},{id : 2,placement_name :"pqr"} ,{id : 3,placement_name :"xyz"}];
  compagianState = "";
  comapignEditable = false;
  placementEdiatble = false;
  isDisabled = false;
  tableData = [];
  addCreativeDetails = false;
  tableData1 = [];
  tabIndex=0;
  value :Date;


  comapiagnStatus = [
    { label: 'Select Campaign Status', value: null },
    { label: 'Submitted for Aprroval', value: 'Submitted_for_aprroval' },
    { label: 'Pending', value: 'Pending' },
    { label: 'Revise', value: 'Revise' },
    { label: 'Created in BW', value: 'Created_in_BW' },
    { label: 'Rejected', value: 'Rejected' }
  ];

  campaignRateTypeOption = [
    { label: 'Select Rate Type', value: null },
    { label: 'CPCV', value: 'CPVC' },
    { label: 'CPM', value: 'CPM' }];

  alpPixelOption = [
    { label: 'Select a Value', value: null },
    { label: 'N/A', value: "N/A" },
    { label: 'Yes', value: 'Yes' },
    { label: 'No', value: 'No' }];

  viewableCompleteOption = [
    { label: 'Select a Value', value: null },
    { label: 'N/A', value: "N/A" },
    { label: 'Yes', value: 'Yes' },
    { label: 'No', value: 'No' }];

  alphonsoAttributionStudyOption = [
    { label: 'Select a Value', value: null },
    { label: 'N/A', value: "N/A" },
    { label: 'Yes', value: 'Yes' },
    { label: 'No', value: 'No' }];

  brandLiftStudyOption = [
    { label: 'Select a Value', value: null },
    { label: 'N/A', value: "N/A" },
    { label: 'Yes', value: 'Yes' },
    { label: 'No', value: 'No' }];

  reportingOption = [
    { label: 'Select a Value', value: null },
    { label: 'Post-only', value: 'Post-only' },
    { label: 'Mid and Post', value: "Mid and Post" }];

  billingAttributionOption = [
    { label: 'Select a Value', value: null },
    { label: 'N/A', value: "N/A" },
    { label: 'Yes', value: 'Yes' },
    { label: 'No', value: 'No' }];

  accessToAlphonsoOption = [
    { label: 'Select a Value', value: null },
    { label: 'N/A', value: "N/A" },
    { label: 'Yes', value: 'Yes' },
    { label: 'No', value: 'No' }];

  lineItemTypeoOption = [
    { label: 'Line Item Type', value: null },
    { label: 'Banner', value: "Banner" },
    { label: 'Vedio', value: 'Vedio' },
    { label: 'Native', value: 'Native' }];

    AdditionalPixel = [
      { label:'Select Yes / No',value:null},
      {label:'Yes', value:'Yes'},
      {label:'No',value:'No'}
    ]

  constructor(
    private formBuilder: FormBuilder,
    private currentActivatedRoute: ActivatedRoute,
    private router: Router,
    private dialogService: DialogService,

  ) { }

  ngOnInit() {
    console.log("history.state---", history.state.data);
    this.compagianState = history.state.data;
    this.DesktopDimColDef = [
      {
        field: 'lid',
        displayName: 'LID',
        format: '',
        width: '100',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
      },
      {
        field: 'tag_name',
        displayName: 'Tag Name',
        format: 'string',
        width: '200',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
      },
      {
        field: 'tag_type',
        displayName: 'Tag Type',
        format: 'string',
        width: '120',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
      },
      {
        field: 'tag_code',
        displayName: 'Tag Code',
        format: '',
        width: '90',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
      },


      {
        field: 'additional_pixel',
        displayName: 'Additional Pixel',
        width: '160',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'id',
        displayName: 'Additional Pixel Code',
        format: '',
        width: '150',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'id',
        displayName: 'Impressions',
        format: '',
        width: '120',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'comment',
        displayName: 'Video Duration',
        format: '',
        width: '120',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },

      },
      {
        field: 'attachement',
        displayName: 'Comments',
        format: '',
        width: '100',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },

      },
      {
        field: 'delete',
        displayName: 'Delete',
        width: '70',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
      },

    ];
    this.DesktopaggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,

      // columns: this.DesktopDimColDef,
      // selectedColumns: this.aggTableColumnDef,
      // frozenCols: [],
      // frozenWidth: '0px',
      footerColumns: this.DesktopDimColDef,
      columns: this.DesktopDimColDef,
      selectedColumns: this.DesktopDimColDef,
      // frozenCols: [...this.DesktopDimColDef.slice(0, 4)],
      // frozenWidth:
      //   this.DesktopDimColDef
      //     .slice(0, 4)
      //     .reduce((tot, cur) => tot + parseInt(cur.width), 0) + 'px',

      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };

    this.DesktopTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,
      footerColumns: this.DesktopDimColDef,
      columns: this.DesktopDimColDef,
      selectedColumns: this.DesktopDimColDef,
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };

    this.mobileDimColDef = [
      {
        field: 'lid',
        displayName: 'LID',
        format: '',
        width: '100',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
      },
      {
        field: 'tag_name',
        displayName: 'Tag Name',
        format: 'string',
        width: '200',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
      },
      {
        field: 'tag_type',
        displayName: 'Tag Type',
        format: 'string',
        width: '120',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
      },
      {
        field: 'tag_code',
        displayName: 'Tag Code',
        format: '',
        width: '90',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
      },


      {
        field: 'additional_pixel',
        displayName: 'Additional Pixel',
        width: '160',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'id',
        displayName: 'Additional Pixel Code',
        format: '',
        width: '150',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'id',
        displayName: 'Impressions',
        format: '',
        width: '120',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'comment',
        displayName: 'Video Duration',
        format: '',
        width: '120',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },

      },
      {
        field: 'attachement',
        displayName: 'Comments',
        format: '',
        width: '100',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },

      },
      {
        field: 'delete',
        displayName: 'Delete',
        width: '70',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
      },

    ];
    this.mobileaggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,

      // columns: this.DesktopDimColDef,
      // selectedColumns: this.aggTableColumnDef,
      // frozenCols: [],
      // frozenWidth: '0px',
      footerColumns: this.mobileDimColDef,
      columns: this.mobileDimColDef,
      selectedColumns: this.mobileDimColDef,
      // frozenCols: [...this.DesktopDimColDef.slice(0, 4)],
      // frozenWidth:
      //   this.DesktopDimColDef
      //     .slice(0, 4)
      //     .reduce((tot, cur) => tot + parseInt(cur.width), 0) + 'px',

      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };

    this.mobileTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,
      footerColumns: this.mobileDimColDef,
      columns: this.mobileDimColDef,
      selectedColumns: this.mobileDimColDef,
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };

    this.tabletDimColDef = [
      {
        field: 'lid',
        displayName: 'LID',
        format: '',
        width: '100',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
      },
      {
        field: 'tag_name',
        displayName: 'Tag Name',
        format: 'string',
        width: '200',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
      },
      {
        field: 'tag_type',
        displayName: 'Tag Type',
        format: 'string',
        width: '120',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
      },
      {
        field: 'tag_code',
        displayName: 'Tag Code',
        format: '',
        width: '90',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
      },


      {
        field: 'additional_pixel',
        displayName: 'Additional Pixel',
        width: '160',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'id',
        displayName: 'Additional Pixel Code',
        format: '',
        width: '120',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'id',
        displayName: 'Impressions',
        format: 'number',
        width: '120',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'comment',
        displayName: 'Video Duration',
        format: '',
        width: '120',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },

      },
      {
        field: 'attachement',
        displayName: 'Comments',
        format: '',
        width: '100',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },

      },
      {
        field: 'delete',
        displayName: 'Delete',
        width: '70',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
      },

    ];
    this.tabletaggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,

      // columns: this.DesktopDimColDef,
      // selectedColumns: this.aggTableColumnDef,
      // frozenCols: [],
      // frozenWidth: '0px',
      footerColumns: this.tabletDimColDef,
      columns: this.tabletDimColDef,
      selectedColumns: this.tabletDimColDef,
      // frozenCols: [...this.DesktopDimColDef.slice(0, 4)],
      // frozenWidth:
      //   this.DesktopDimColDef
      //     .slice(0, 4)
      //     .reduce((tot, cur) => tot + parseInt(cur.width), 0) + 'px',

      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };

    this.tabletTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,
      footerColumns: this.tabletDimColDef,
      columns: this.tabletDimColDef,
      selectedColumns: this.tabletDimColDef,
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };


    this.addCompaignForm = this.formBuilder.group({
      cid: [''],
      compaign_name: ['', Validators.required],
      compaign_status: [''],
      campaign_Rate_Type: [''],
      alp_Pixel_Option: [''],
      viewable_Complete_Option: [''],
      alphonso_AttributionStudyOption: [''],
      brand_LiftStudyOption: [''],
      reporting_Option: [''],
      billing_AttributionOption: [''],
      access_ToAlphonsoOption: [''],
      start_date: [''],
      end_date: [''],
      advertiser: ['', Validators.required],
      advertiser_domain: [''],
      billingAdServer:[''],
      agency: ['', Validators.required],
      third_party_verification: [''],
      whitelist: [''],
      blacklist: [''],
      tremor_video_ad_sale_no: [''],
      budget: ['', [Validators.required, Validators.min(1), Validators.max(9999999999)]],
      impressions: ['', [Validators.required, Validators.min(0), Validators.max(9999999999)]],
      rate: ['', Validators.max(100)],
      frequency_cap: ['']
    });


    this.addPlacementForm = this.formBuilder.group({
      placement_id: [''],
      placement_name: ['', Validators.required],
      lineitemid: [''],
      placement_start_date: ['', Validators.required],
      placement_end_date: [''],
      placement_impressions: ['', [Validators.required, Validators.min(0), Validators.max(9999999999)]],
      placement_budget: ['', [Validators.required, Validators.min(1), Validators.max(9999999999)]],
      // placement_rate_type:[''],
      campaign_Rate_Type: [''],
      placement_rate: ['', Validators.max(100)],
      placementlineitemtype: ['', Validators.required],
      targeting: [''],
      placementsecondarykpi: [''],
      creativeDetails: ['']

    });

    this.totalPalcements.forEach(x => {
    //   console.log(x);
      this.updatePlacementForm[x['id']]= this.formBuilder.group({
        'placement_id': [''],
        'placement_name': ['', Validators.required],
        'lineitemid': [''],
        'placement_start_date': ['', Validators.required],
        'placement_end_date': [''],
        'placement_impressions': ['', [Validators.required, Validators.min(0), Validators.max(9999999999)]],
        'placement_budget': ['', [Validators.required, Validators.min(1), Validators.max(9999999999)]],
        // placement_rate_type:[''],
        'campaign_Rate_Type': [''],
        'placement_rate': ['', Validators.max(100)],
        'placementlineitemtype': ['', Validators.required],
        'targeting': [''],
        'placementsecondarykpi': [''],
        'creativeDetails': ['']
  
      });
      
    })
    // this.updatePlacementForm = this.formBuilder.group({
    //   placement_id: [''],
    //   placement_name: ['', Validators.required],
    //   lineitemid: [''],
    //   placement_start_date: ['', Validators.required],
    //   placement_end_date: [''],
    //   placement_impressions: ['', [Validators.required, Validators.min(0), Validators.max(9999999999)]],
    //   placement_budget: ['', [Validators.required, Validators.min(1), Validators.max(9999999999)]],
    //   // placement_rate_type:[''],
    //   campaign_Rate_Type: [''],
    //   placement_rate: ['', Validators.max(100)],
    //   placementlineitemtype: ['', Validators.required],
    //   targeting: [''],
    //   placementsecondarykpi: [''],
    //   creativeDetails: ['']

    // });

    if (this.compagianState === 'Setup') {
      this.addCompaignForm.disable();
      // this.updatePlacementForm.disable();
      this.isDisabled = true;
    }

    this.loadDesktopAggTableData('tableReq');
    // this.loadMobileTableData('tableReq');
    // this.loadTabletTableData('tableReq');

    this.loadDesktopTableData('tableReq');

  }

  deleteCard() {
    this.showAddPlacement = true;
    this.showPlacement = false
  }
  get c() {
    return this.addCompaignForm.controls;
  }

  get p() {
    return this.addPlacementForm.controls;
  }

  // get u() {
  //   return this.updatePlacementForm[0].controls;
  // }

  loadDesktopAggTableData(tableReq) {
    setTimeout(() => {
      this.DesktopaggTableJson['loading'] = true;
    });
    // const tableData = data['data'];

    this.tableData = [{ lid: 1, tag_name: "abc", tag_type: "abc", start_date: 2019, tag_code: "All", additional_pixel: 200000 }];
    let data = [];
    data = this.tableData.concat(this.tableData1);
    console.log("data---", data);
    // let newArray = 
    const arr = [];
    data.forEach((row: object) => {
      arr.push({
        data: row,
        children: [{ data: {} }]
      });
    });
    this.DesktopaggTableData = <TreeNode[]>arr;
    this.DesktopaggTableJson['totalRecords'] = data.length;
    setTimeout(() => {
      this.DesktopaggTableJson['loading'] = false;
    });
    this.DesktopaggTableJson['lazy'] = true;
  }

  loadMobileAggTableData(tableReq) {
    console.log("data---of mobile");
    setTimeout(() => {
      this.mobileaggTableJson['loading'] = true;
    });
    // const tableData = data['data'];

    this.tableData = [{ lid: 1, tag_name: "abc", tag_type: "abc", start_date: 2019, tag_code: "All", additional_pixel: 200000 }];
    let data = [];


    data = this.tableData.concat(this.tableData1);

    console.log("data---of mobile", data);
    // let newArray = 
    const arr = [];
    data.forEach((row: object) => {
      arr.push({
        data: row,
        children: [{ data: {} }]
      });
    });
    this.mobileaggTableData = <TreeNode[]>arr;
    this.mobileaggTableJson['totalRecords'] = data.length;
    setTimeout(() => {
      this.mobileaggTableJson['loading'] = false;
    });
    this.mobileaggTableJson['lazy'] = true;
  }

  loadTabletAggTableData(tableReq) {
    setTimeout(() => {
      this.tabletaggTableJson['loading'] = true;
    });
    // const tableData = data['data'];

    this.tableData = [{ lid: 1, tag_name: "abc", tag_type: "abc", start_date: 2019, tag_code: "All", additional_pixel: 200000 }];
    let data = [];


    data = this.tableData.concat(this.tableData1);

    console.log("data---", data);
    // let newArray = 
    const arr = [];
    data.forEach((row: object) => {
      arr.push({
        data: row,
        children: [{ data: {} }]
      });
    });
    this.tabletaggTableData = <TreeNode[]>arr;
    this.tabletaggTableJson['totalRecords'] = data.length;
    setTimeout(() => {
      this.tabletaggTableJson['loading'] = false;
    });
    this.tabletaggTableJson['lazy'] = true;
  }

  loadDesktopTableData(tableReq) {
    setTimeout(() => {
      this.DesktopTableJson['loading'] = true;
    });
    // const tableData = data['data'];

    this.tableData = [{ lid: 1, tag_name: "abc", tag_type: "abc", start_date: 2019, tag_code: "All", additional_pixel: 200000 }];
    let data = [];
    data = this.tableData.concat(this.tableData1);
    console.log("data---", data);
    // let newArray = 
    const arr = [];
    data.forEach((row: object) => {
      arr.push({
        data: row,
        children: [{ data: {} }]
      });
    });
    this.DesktopTableData = <TreeNode[]>arr;
    this.DesktopTableJson['totalRecords'] = data.length;
    setTimeout(() => {
      this.DesktopTableJson['loading'] = false;
    });
    this.DesktopTableJson['lazy'] = true;
  }

  loadMobileTableData(tableReq) {
    console.log("data---of mobile");
    setTimeout(() => {
      this.mobileTableJson['loading'] = true;
    });
    // const tableData = data['data'];

    this.tableData = [{ lid: 1, tag_name: "abc", tag_type: "abc", start_date: 2019, tag_code: "All", additional_pixel: 200000 }];
    let data = [];


    data = this.tableData.concat(this.tableData1);

    console.log("data---of mobile", data);
    // let newArray = 
    const arr = [];
    data.forEach((row: object) => {
      arr.push({
        data: row,
        children: [{ data: {} }]
      });
    });
    this.mobileTableData = <TreeNode[]>arr;
    this.mobileTableJson['totalRecords'] = data.length;
    setTimeout(() => {
      this.mobileTableJson['loading'] = false;
    });
    this.mobileTableJson['lazy'] = true;
  }

  loadTabletTableData(tableReq) {
    setTimeout(() => {
      this.tabletTableJson['loading'] = true;
    });
    // const tableData = data['data'];

    this.tableData = [{ lid: 1, tag_name: "abc", tag_type: "abc", start_date: 2019, tag_code: "All", additional_pixel: 200000 }];
    let data = [];


    data = this.tableData.concat(this.tableData1);

    console.log("data---", data);
    // let newArray = 
    const arr = [];
    data.forEach((row: object) => {
      arr.push({
        data: row,
        children: [{ data: {} }]
      });
    });
    this.tabletTableData = <TreeNode[]>arr;
    this.tabletTableJson['totalRecords'] = data.length;
    setTimeout(() => {
      this.tabletTableJson['loading'] = false;
    });
    this.tabletTableJson['lazy'] = true;
  }

  addCreative() {
    this.addCreativeDetails = true;
    this.tableData1.push({});
    console.log('e...index', this.tabIndex);
    console.log("this.tableData1", this.tableData1);
    if(this.tabIndex === 0)
    {
      console.log("in desktop");
      
      this.loadDesktopTableData('tableReq');

    } else if(this.tabIndex === 1)
    {
      console.log("in mobile");
      this.loadMobileTableData('tableReq');
    } else {
      console.log("in tablet");
      this.loadTabletTableData('tableReq')
    }
  }

  addPlacementCreative()
  {
    this.tableData1.push({});
    console.log('e...index', this.tabIndex);
    console.log("this.tableData1", this.tableData1);
    if(this.tabIndex === 0)
    {
      console.log("in desktop");
      
      this.loadDesktopAggTableData('tableReq');

    } else if(this.tabIndex === 1)
    {
      console.log("in mobile");
      this.loadMobileAggTableData('tableReq');
    } else {
      console.log("in tablet");
      this.loadTabletAggTableData('tableReq')
    }
  }

  tabChanged(e)
  {
    this.tableData1=[];
    this.tabIndex = e;
    console.log('e...index', e);
  }

  returnToComapign() {
    this.router.navigate(['/campaign-management'], {
      relativeTo: this.currentActivatedRoute
    });
  }

  addTagCode()
  {
    const ref = this.dialogService.open(TagCodePopupComponent, {
      header:'Tag Code ',
      contentStyle: { width: '80vw', overflow: 'auto' },
      data: ''
    });
    ref.onClose.subscribe((data: string) => { });
  }

  onLazyLoadAggTable() {

  }

  editCampaign()
  {

  }
}
