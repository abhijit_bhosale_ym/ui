import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompaignDetailsComponent } from './compaign-details.component';

describe('CompaignDetailsComponent', () => {
  let component: CompaignDetailsComponent;
  let fixture: ComponentFixture<CompaignDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompaignDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompaignDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
