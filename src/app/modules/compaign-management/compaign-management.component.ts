import {
  Component,
  OnInit,
  QueryList,
  ElementRef,
  ViewChildren,
  OnDestroy
} from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { FetchApiDataService } from './fetch-api-data.service';
import { TreeNode, ConfirmationService, MenuItem } from 'primeng/api';
import { ToastService } from '../../_services/toast-notification/toast.service';
import { DialogService } from 'primeng/dynamicdialog';

// import { DummyCompComponent } from './dummy-comp/dummy-comp.component';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import * as moment from 'moment';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { Title } from '@angular/platform-browser';
import { DataShareService } from 'src/app/_services/data-share/data-share.service';
import { Subscription } from 'rxjs';
import { float } from 'html2canvas/dist/types/css/property-descriptors/float';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { SendEmailComponent } from '../send-email/send-email.component';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { ApprovalEmailComponent } from './approval-email/approval-email.component'
import {
  Router,
  ActivatedRoute,
  NavigationExtras,
  NavigationEnd,
  RouterEvent
} from '@angular/router';


@Component({
  selector: 'ym-compaign-management',
  templateUrl: './compaign-management.component.html',
  styleUrls: ['./compaign-management.component.scss']
})
export class CompaignManagementComponent implements OnInit, OnDestroy {

  private appConfigObs: Subscription;
  appConfig: object = {};
  currentUser:any;
  filtersApplied: object = {};
  exportRequest: ExportRequest = <ExportRequest>{};
  tableDimColDef: any[];
  tableDimColDefLineItem: any[];
  aggTableJson: Object;
  aggTableJsonLineItem: Object;
  nextUpdated: Date;
  dataUpdatedThrough: Date;
  aggTableData: TreeNode[];
  aggTableDataLineItem: TreeNode[];
  noTableData = false;
  noTableDataLineItem = false;
  display: boolean = false;
  creativeTableJson: Object;
  flatTableColumnDef: any[];
  creativeTableData: TreeNode[];
  campaignTableFlag: boolean = true;
  lineItemTableFlag: boolean = false;
  creativeTableFlag: boolean = false;
  breadcrumbFlag: boolean = false;
  addLineItemCreativeFormFlag: boolean = false;
  selectedLineItem: any;
  selectedOrderName: any;
  formIdentity: any;
  orderId: any;
  lineItemId: any;
  createCampaign = false;
  updateCampaign = false;
  deleteCampaign = false;
  approveCampaign = false;
  pushToGamCampaign = false;
  reportingCampaign = false;
  connectionList=[];




  constructor(
    private appConfigService: AppConfigService,
    private _titleService: Title,
    private libServ: CommonLibService,
    private dataFetchServ: FetchApiDataService,
    private toastService: ToastService,
    private dialogService: DialogService,
    private htmltoimage: HtmltoimageService,
    private pptExport: ExportPptService,
    private exportService: ExportdataService,
    private formatNumPipe: FormatNumPipe,
    private currentActivatedRoute: ActivatedRoute,
    private router: Router,
    private confirmationService: ConfirmationService,
  ) { }
  items: MenuItem[];
  selectedConnectorLabel:any
  connectorFlag:boolean=false;
  ngOnInit() {

    this.items = [
      { label: 'Order' },
      { label: 'Line Items' },
    ];
    this.campaignTableFlag = false;



    this.flatTableColumnDef = [
      {
        field: 'name',
        displayName: 'Creative Name',
        format: '',
        width: '170',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'setup',
        displayName: 'Setup Creative',
        format: '',
        width: '170',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },


      {
        field: 'status',
        displayName: 'Status',
        format: '',
        width: '110',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },


    ];


    this.tableDimColDef = [

      {
        field: 'name',
        displayName: 'Campaign Name',
        format: 'string',
        width: '200',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
      },
      {
        field: 'status',
        displayName: 'Campaign Status',
        format: 'string',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
      },

      {
        field: 'ad_server_status',
        displayName: 'Ad Server Campaign Status',
        width: '170',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
      },


      {
        field: 'comment',
        displayName: 'Comments',
        format: '',
        width: '90',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },

      },
      {
        field: 'attachement',
        displayName: 'Attachments',
        format: '',
        width: '100',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },

      },

    ];
    this.tableDimColDefLineItem = [
      {
        field: 'name',
        displayName: 'Line Item Name',
        format: 'string',
        width: '200',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
      },


      {
        field: 'status',
        displayName: 'Line Item Status',
        format: 'string',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
      },

      {
        field: 'ad_server_status',
        displayName: 'Ad Server Line Item Status',
        width: '170',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      // {
      //   field: 'details',
      //   displayName: 'Details',
      //   format: '',
      //   width: '90',
      //   exportConfig: {
      //     format: 'String',
      //     styleinfo: {
      //       thead: 'default',
      //       tdata: 'white'
      //     }
      //   },
      //   formatConfig: [],
      //   options: {
      //     editable: false,
      //     colSearch: false,
      //     colSort: true,
      //     resizable: true,
      //     movable: true
      //   },
      // },
      {
        field: 'startDate',
        displayName: 'Start Date',
        format: '',
        width: '170',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'endDate',
        displayName: 'End Date',
        format: '',
        width: '90',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },

      },
      {
        field: 'attachement',
        displayName: 'Attachments',
        format: '',
        width: '100',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },

      },

      // {
      //   field: 'delete',
      //   displayName: 'Delete',
      //   width: '70',
      //   exportConfig: {
      //     format: 'String',
      //     styleinfo: {
      //       thead: 'default',
      //       tdata: 'white'
      //     }
      //   },
      //   format: '',
      //   formatConfig: [],
      //   options: {
      //     editable: false,
      //     colSearch: false,
      //     colSort: true,
      //     resizable: true,
      //     movable: true
      //   },
      // },
    ]

    this.getConnectionsList();

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        console.log("appConfig   :", appConfig)
        this.appConfig = appConfig;
        this.currentUser = this.appConfig['user']['id'];
        this._titleService.setTitle(this.appConfig['displayName']);
        let startDate;
        let endDate;
        this.createCampaign = this.appConfig['permissions'].some(
          o => o.name === 'create-campaign'
        );
        this.updateCampaign = this.appConfig['permissions'].some(
          o => o.name === 'update-campaign'
        );
        this.deleteCampaign = this.appConfig['permissions'].some(
          o => o.name === 'delete-campaign'
        );
        this.approveCampaign = this.appConfig['permissions'].some(
          o => o.name === 'approve-campaign'
        );

        this.pushToGamCampaign = this.appConfig['permissions'].some(
          o => o.name === 'push-to-gam-campaign'
        );

        this.reportingCampaign = this.appConfig['permissions'].some(
          o => o.name === 'reporting-campaign'
        );




        if (!((this.tableDimColDef.some(x => x.field === 'details')))) {
          this.updateCampaign ? this.tableDimColDef.splice(this.tableDimColDef.length, 0,
            {
              field: 'details',
              displayName: 'Details',
              format: '',
              width: '90',
              exportConfig: {
                format: 'String',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: true
              },
            }
          ) : '';
        }

        if (!((this.tableDimColDef.some(x => x.field === 'setup_campaign')))) {
          this.pushToGamCampaign ? this.tableDimColDef.splice(this.tableDimColDef.length, 0,
            {
              field: 'setup_campaign',
              displayName: 'Setup Campaign',
              format: '',
              width: '170',
              exportConfig: {
                format: 'number',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: true
              },
            }
          ) : '';
        }

        if (!((this.tableDimColDef.some(x => x.field === 'approver')))) {
          this.approveCampaign ? this.tableDimColDef.splice(this.tableDimColDef.length, 0,
            {
              field: 'approver',
              displayName: 'Approve',
              format: '',
              width: '100',
              exportConfig: {
                format: 'number',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: true
              },

            }
          ) : '';
        }

        if (!((this.tableDimColDef.some(x => x.field === 'delete')))) {
          this.deleteCampaign ? this.tableDimColDef.splice(this.tableDimColDef.length, 0, {
            field: 'delete',
            displayName: 'Delete',
            width: '70',
            exportConfig: {
              format: 'String',
              styleinfo: {
                thead: 'default',
                tdata: 'white'
              }
            },
            format: '',
            formatConfig: [],
            options: {
              editable: false,
              colSearch: false,
              colSort: true,
              resizable: true,
              movable: true
            },
          }) : '';
        }

        if (!((this.tableDimColDefLineItem.some(x => x.field === 'details')))) {
          this.updateCampaign ? this.tableDimColDefLineItem.splice(this.tableDimColDefLineItem.length, 0,
            {
              field: 'details',
              displayName: 'Details',
              format: '',
              width: '90',
              exportConfig: {
                format: 'String',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: true
              },
            }
          ) : '';
        }

        if (!((this.tableDimColDefLineItem.some(x => x.field === 'delete')))) {
          this.deleteCampaign ? this.tableDimColDefLineItem.splice(this.tableDimColDefLineItem.length, 0,
            {
              field: 'delete',
              displayName: 'Delete',
              width: '70',
              exportConfig: {
                format: 'String',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              format: '',
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: true
              },
            }
          ) : '';
        }

        if (!((this.flatTableColumnDef.some(x => x.field === 'edit')))) {
          this.updateCampaign ? this.flatTableColumnDef.splice(this.flatTableColumnDef.length, 0,
            {
              field: 'edit',
              displayName: 'Edit',
              format: '',
              width: '60',
              value: '',
              condition: '',
              columnType: 'dimensions',
              isClearSearch: true,
              exportConfig: {
                format: 'string',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: false,
                resizable: true,
                movable: false
              }
            }
          ) : '';
        }

        if (!((this.flatTableColumnDef.some(x => x.field === 'delete')))) {
          this.deleteCampaign ? this.flatTableColumnDef.splice(this.flatTableColumnDef.length, 0,
            {
              field: 'delete',
              displayName: 'Delete',
              format: '',
              width: '60',
              value: '',
              condition: '',
              columnType: 'dimensions',
              isClearSearch: true,
              exportConfig: {
                format: 'string',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: false,
                resizable: true,
                movable: false
              }
            }
          ) : '';
        }




        this.filtersApplied = {
          timeKeyFilter: {
            // time_key1: startDate.format('YYYYMMDD'),
            // time_key2: endDate.format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          // groupby: this.appConfig['filter']['filterConfig']['groupBy'].filter(
          //   v => v.selected
          // )
        };
        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');

        this.aggTableJson = {
          page_size: 10,
          lazy: false,
          loading: false,
          export: true,
          sortMode: 'multiple',
          resizableColumns: true,
          columnResizeMode: 'fit',
          reorderableColumns: true,
          scrollHeight: '500px',
          totalRecords: 1000,

          // columns: this.tableDimColDef,
          // selectedColumns: this.aggTableColumnDef,
          // frozenCols: [],
          // frozenWidth: '0px',
          //  footerColumns: this.tableDimColDef,
          // columns: this.tableDimColDef,
          //selectedColumns: this.tableDimColDef,

          columns: this.tableDimColDef,


          selectedColumns: this.tableDimColDef,

          scrollable: true,
          selectionMode: 'multiple',
          selectedColsModal: [],
          selectionDataKey: 'name',
          metaKeySelection: true,
          showHideCols: true,
          overallSearch: true,
          columnSearch: false
        };

        this.aggTableJsonLineItem = {
          page_size: 10,
          lazy: false,
          loading: false,
          export: true,
          sortMode: 'multiple',
          resizableColumns: true,
          columnResizeMode: 'fit',
          reorderableColumns: true,
          scrollHeight: '500px',
          totalRecords: 1000,


          columns: this.tableDimColDefLineItem,


          selectedColumns: this.tableDimColDefLineItem,

          scrollable: true,
          selectionMode: 'multiple',
          selectedColsModal: [],
          selectionDataKey: 'name',
          metaKeySelection: true,
          showHideCols: true,
          overallSearch: true,
          columnSearch: false
        };

        this.creativeTableJson = {
          page_size: 50,
          page: 0,
          lazy: false,
          loading: false,
          export: true,
          sortMode: 'multiple',
          resizableColumns: true,
          columnResizeMode: 'fit',
          reorderableColumns: true,
          scrollHeight: '300px',
          totalRecords: 1000,
          columns: this.flatTableColumnDef.slice(0),
          selectedColumns: this.flatTableColumnDef.slice(0),
          frozenCols: [this.flatTableColumnDef[0]],
          // frozenWidth: '250px',
          frozenWidth:
            this.flatTableColumnDef
              .slice(0, 0)
              .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',

          scrollable: true,
          selectionMode: 'multiple',
          selectedColsModal: [],
          selectionDataKey: 'name',
          metaKeySelection: true,
          showHideCols: true,
          overallSearch: true,
          columnSearch: true
        };

        // this.initialLoading();
        let resourcesList = this.appConfig['user']['team']['resources']
        resourcesList = JSON.parse(resourcesList)

          resourcesList.forEach(element => {
            if (element.key =='connector'){
              if(element.value.length>0){
                this.selectedConnectorLabel = element.value[0]
              }
            }
          });

            this.breadcrumbFlag=false;
            this.campaignTableFlag=true;
            console.log("this.selectedConnectorLabel   :",this.selectedConnectorLabel)
            this.initialLoading();
            let ConnectorFlag=false;
            let appgroups = this.appConfig['user']['role']['appGroups']
            appgroups.forEach(element => {
              if(element.name=='Campaign Management'){
                element.apps.forEach(nestedElement => {
                  if(nestedElement.name=='Campaign Connector'){
                    ConnectorFlag=true;
                  }
                });
              }
            });

            if(ConnectorFlag && this.selectedConnectorLabel==undefined){
              this.connectorFlag=true;
            }
      }
    });

  }

  initialLoading() {

    const tableReq = {
      dimensions: ['station_group'], // [this.filtersApplied['groupby'][0]['key']],
      metrics: [
        'dp_impressions',
        'gross_revenue',
        'total_expenses',
        'station_revenue_share_distribution',
        'total_recoupable_expenses'
      ],
      derived_metrics: ['client_net_share', 'gross_ecpm', 'client_net_ecpm'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: ['station_group'], // grpBys,
      orderBy: [{ key: 'accounting_key', opcode: 'desc' }],
      limit: '',
      offset: ''
    };

    this.loadTableData(tableReq);

  }

  loadTableDataCreative(rowData, identity) {

    this.creativeTableData = [];
    if (identity == 'table') {
      console.log("id     :>", rowData);
      this.items.push({ label: 'Creative' },)
      this.lineItemId = rowData['id'];
      this.selectedLineItem = rowData['name'];
    }

    this.dataFetchServ
      .getCreativeByLineItems(this.lineItemId)
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['data']);
          return;
        }
        if (data['status'] == 1) {
          this.creativeTableFlag = true;
          this.lineItemTableFlag = false;
          if (data['data'].length > 0) {
            const tableData = data['data'];
            const arr = [];
            console.log("line_item_json   :", JSON.stringify(tableData['line_item_json']))
            tableData.forEach((row: object) => {
              // row['line_item'] = 'All'
              arr.push({
                data: row,
                children: [{ data: {} }]
              });
            });

            console.log("arr====", arr);

            this.creativeTableData = <TreeNode[]>arr;
            this.creativeTableData['totalRecords'] = arr.length;
            this.creativeTableData['loading'] = false;
          }
        }
      })
  }



  loadTableDataLineItem(rowData, identity) {
    this.aggTableDataLineItem = [];
    if (identity == 'table') {
      this.items = [{ label: 'Order' }, { label: 'Line Items' }];
      console.log("id     :>", rowData);
      this.orderId = rowData['id'];
      this.selectedOrderName = rowData['name'];
    }

    this.dataFetchServ
      .getLineItemsByCampaign(this.orderId)
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['data']);
          return;
        }
        if (data['status'] == 1) {
          this.breadcrumbFlag=true;
          this.lineItemTableFlag = true;
          this.campaignTableFlag = false;
          if (data['data'].length > 0) {
            const tableData = data['data'];
            const arr = [];
            console.log("line_item_json   :", JSON.stringify(tableData['line_item_json']))
            tableData.forEach((row: object) => {
              // row['line_item'] = 'All'
              arr.push({
                data: row,
                children: [{ data: {} }]
              });
            });

            console.log("arr====", arr);

            this.aggTableDataLineItem = <TreeNode[]>arr;
            this.aggTableDataLineItem['totalRecords'] = arr.length;
            this.aggTableDataLineItem['loading'] = false;
          }
        }
      })
  }

  loadTableData(params) {
    // this.reloadAggTable();
    this.aggTableJson['loading'] = true;

    params['dimensions'].push('accounting_key');
    const grpBysFilter = ['station_group', 'derived_station_rpt'];

    this.dataFetchServ
      .getAllCampaigns()
      .subscribe(data => {
        console.log("data   :",data)
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['data']);
          return;
        }
        if (data['status'] == 1) {
          const tableData = data['data'];
          const arr = [];
          tableData.forEach((row: object) => {
            arr.push({
              data: row,
              children: [{ data: {} }]
            });
          });

          this.aggTableData = <TreeNode[]>arr;
          this.aggTableJson['totalRecords'] = arr.length;
          this.aggTableJson['loading'] = false;
        }
      });
  }


  onTableDrill(e: Event) {
    console.log("e     :>", e['node']['data']['id']);

    // if (!e['node']['childLoaded']) {


    this.dataFetchServ
      .getLineItemsByCampaign(e['node']['data']['id'])
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['data']);
          return;
        }
        if (data['status'] == 1) {

          if (data['data'].length > 0) {
            const childData = data['data'];
            const arr = [];
            console.log("childData   :", childData)
            childData.forEach((row: object) => {
              row['line_item'] = this.libServ.deepCopy(row['name'])
              row['name'] = e['node']['data']['name'];
              let obj = {};

              obj = {
                data: row,
              };
              arr.push(obj);
            });
            this.aggTableJson['loading'] = false;
            e['node']['children'] = <TreeNode[]>arr;
            this.aggTableData = [...this.aggTableData];
            e['node']['childLoaded'] = true;

          }
          else {
            const arr = [];
            let obj = {};
            obj = {
              data: {},
            };
            arr.push(obj);

            this.aggTableJson['loading'] = false;
            e['node']['children'] = <TreeNode[]>arr;
            this.aggTableData = [...this.aggTableData];
            e['node']['childLoaded'] = true;
          }
        }
      })
  }

  sendEmail() {
    const imageConfig = {
      imgName: `${this.appConfig['name']}.png`,
      type: 'image/png'
    };

    this.htmltoimage
      .canvasImage(document.getElementById('capture'), imageConfig)
      .then(canvas => {
        this.dialogService.open(SendEmailComponent, {
          header: 'Email Screenshot',
          contentStyle: {
            'max-height': '80vh',
            width: '100vw',
            overflow: 'auto'
          },
          data: canvas.toDataURL('image/png')
        });
      });
  }

  openAddCompaign() {
    this.connectionList.forEach(element=>{
      if(element.label == this.selectedConnectorLabel){
        this.selctedConnector = element.value;
        console.log("this.selctedConnector   :",this.selctedConnector)
        this.createConnection(this.selctedConnector)
      }
    })

    if(this.selctedConnector!=undefined){
      localStorage.setItem('selctedConnector', this.selctedConnector);
    }else{
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Server Error',
        detail: 'Please select the Connector first'
      });
      return;
    }
    this.router.navigate(['/campaign-delivery-dashboard/add-campaign'], {
      relativeTo: this.currentActivatedRoute
    });
  }

  deleteAlertGroup() {

  }

  editOperationFlag=false;
  operationIdentity:any;
  formData:any;
  openCompaignDetails(rowData) {
    // this.router.navigate(['/campaign-delivery-dashboard/campaign-details'], { state: { data: 'Details' } }), {
    //   relativeTo: this.currentActivatedRoute
    // };
    this.connectionList.forEach(element=>{
      if(element.label == this.selectedConnectorLabel){
        this.selctedConnector = element.value;
        console.log("this.selctedConnector   :",this.selctedConnector)
        this.createConnection(this.selctedConnector)
      }
    })

    if(this.selctedConnector!=undefined){
      localStorage.setItem('selctedConnector', this.selctedConnector);
    }else{
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Server Error',
        detail: 'Please select the Connector first'
      });
      return;
    }
      this.editOperationFlag = true;
      this.connectorFlag=false;
      this.campaignTableFlag=false;
      this.operationIdentity='editOrder'
      this.formData = rowData;
  }



  editOperationBack(){
    this.editOperationFlag = false;
    this.connectorFlag=true;
    this.campaignTableFlag=true;
  }

  openCompaignSetup() {
    this.router.navigate(['/campaign-delivery-dashboard/campaign-details'], { state: { data: 'Setup' } }), {
      relativeTo: this.currentActivatedRoute
    };
  }

  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['displayName']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    } else if (type === 'ppt') {
      const chartElements = document.getElementsByTagName('ym-charts');
      const slides = [];
      [].forEach.call(chartElements, (el, index) => {
        const c = el.getElementsByTagName('canvas')[0];

        slides.push({
          title: `${this.appConfig['name']}`,
          base64data: this.libServ.canvasToImage('white', c),
          width: 8,
          height: 3
        });

        if (index === chartElements.length - 1) {
          const pptConfig = {
            filename: 'Charts',
            logo: {
              visible: true,
              path: '/assets/images/ym.png',
              width: 0.53125,
              height: 0.458333
            },
            slides: slides
          };
          this.pptExport.exportPPT(pptConfig);
        }
      });
    }
    return false;
  }

  openApprover(rowData) {
    console.log("rowData", rowData);

    const ref = this.dialogService.open(ApprovalEmailComponent, {
      header: 'Send approval email',
      contentStyle: { width: '60vw', overflow: 'auto' },
      data: rowData
    });
    ref.onClose.subscribe((data: string) => { });
  }

  onLazyLoadAggTable(event) {

  }
  pushToGAM(CampignId) {
    console.log("CampignId   :", CampignId)

this.connectionList.forEach(element=>{
  if(element.label == this.selectedConnectorLabel){
    this.selctedConnector = element.value;
    console.log("this.selctedConnector   :",this.selctedConnector)
    this.createConnection(this.selctedConnector)
  }
})

if(this.selctedConnector!=undefined){
  localStorage.setItem('selctedConnector', this.selctedConnector);
}else{
  this.toastService.displayToast({
    severity: 'error',
    summary: 'Server Error',
    detail: 'Please select the Connector first'
  });
  return;
}

    this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?',
      key: 'campaign',
      accept: () => {

        let request = {
          campaignId: CampignId.id,
          selctedConnector:this.selctedConnector
        }
        console.log("Accepted")
        this.dataFetchServ.pushToGAM(request).subscribe(response => {
          console.log("response  :", response)
          console.log("response['msg']   :", response['msg'])
          if (response['msg'][0] == 'Order Creation Failed') {
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Server Error',
              detail: response['msg'][0]
            });
            return;
          }
          if (response['msg'][1] == 'Line Item Creation Failed') {
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Server Error',
              detail: response['msg'][1]
            });
            return;
          }
          if (response['msg'][2] == 'Line Item and Creative association Creation Failed') {
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Server Error',
              detail: response['msg'][2]
            });
            return;
          }
          if (response['msg'][3] == 'Order Creation Failed') {
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Server Error',
              detail: response['msg'][3]
            });
            return;
          }
          if (response['msg'][0] == "Order Created Successfully") {
            this.toastService.displayToast({
              severity: 'success',
              summary: 'Campaign Management',
              detail: response['msg'][0]
            });
          }
          if (response['msg'][1] == "Line Item Created Successfully") {
            this.toastService.displayToast({
              severity: 'success',
              summary: 'Campaign Management',
              detail: response['msg'][1]
            });
          }
          if (response['msg'][2] == "Creative Created Successfully") {
            this.toastService.displayToast({
              severity: 'success',
              summary: 'Campaign Management',
              detail: response['msg'][2]
            });
          }
          if (response['msg'][3] == "Line Item and Creative association Created Successfully") {
            this.toastService.displayToast({
              severity: 'success',
              summary: 'Campaign Management',
              detail: response['msg'][3]
            });
          }
        });
      },
      reject: () => {
        console.log("Rejected")
      }
    });
    console.log("Rejected    :")
  }
  lineItemPopUp
  openPopup(rowData1, col, rowData) {
    console.log("rowData1  :", rowData1, "col   :", col, "rowData   :", rowData)
    this.lineItemPopUp = true;
  }
  openForm() {
    this.display = true;
  }
  openAddLineItemCreativeForm(identity) {

    this.connectionList.forEach(element=>{
      if(element.label == this.selectedConnectorLabel){
        this.selctedConnector = element.value;
        console.log("this.selctedConnector   :",this.selctedConnector)
        this.createConnection(this.selctedConnector)
      }
    })

    if(this.selctedConnector!=undefined){
      localStorage.setItem('selctedConnector', this.selctedConnector);
    }else{
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Server Error',
        detail: 'Please select the Connector first'
      });
      return;
    }

    if (identity == 'LineItem') {
      this.addLineItemCreativeFormFlag = true;
      this.lineItemTableFlag = false;
      this.formIdentity = 'lineItem'
    }
    if (identity == 'Creative') {
      this.addLineItemCreativeFormFlag = true;
      this.creativeTableFlag = false;
      this.formIdentity = 'creative'
    }
  }

  openAddLineItemCreativeTable() {
    if (this.formIdentity == 'lineItem') {
      this.addLineItemCreativeFormFlag = false;
      this.lineItemTableFlag = true
    }
    if (this.formIdentity == 'creative') {
      this.addLineItemCreativeFormFlag = false;
      this.creativeTableFlag = true
    }
  }
  triggerBreadcrumb(event) {
    console.log("Event   :", event['item']['label'])

    if (event['item']['label'] == 'Order') {
      this.items = [{ label: 'Order' }]
      this.campaignTableFlag = true;
      this.breadcrumbFlag=false;
      this.lineItemTableFlag = false;
      this.creativeTableFlag = false;
      this.addLineItemCreativeFormFlag = false;
    }


    if (event['item']['label'] == 'Line Items') {
      this.items = [{ label: 'Order' }, { label: 'Line Items' }]
      this.lineItemTableFlag = true;
      this.creativeTableFlag = false;
      this.addLineItemCreativeFormFlag = false;
      this.loadTableDataLineItem(null, 'back');
    }


    if (event['item']['label'] == 'Creative') {
      this.creativeTableFlag = true;
      this.addLineItemCreativeFormFlag = false;
      this.loadTableDataCreative(null, 'back')
    }
  }

  childResponse(event) {
    console.log("childResponse    :", event)
    this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?',
      key: 'campaign',
      accept: () => {
        if (event == 'creative') {
          this.creativeTableFlag = true;
          this.loadTableDataCreative(null, 'back')
        }
        if (event == 'lineItem') {
          this.lineItemTableFlag = true;
          this.loadTableDataLineItem(null, 'back');
        }
        if(event == 'order'){
          this.connectorFlag=true;
          this.campaignTableFlag=true;
          // this.loadTableDataLineItem(null, 'back');
        }
        this.addLineItemCreativeFormFlag = false;
        this.editOperationFlag = false;
      },
      reject: () => {
      }
    });
  }

  backOperation(identity) {
    if (identity == 'LineItem') {
      this.campaignTableFlag = true;
      this.breadcrumbFlag=false;
      this.lineItemTableFlag = false;
    }
    if (identity == 'Creative') {
      this.lineItemTableFlag = true;
      this.creativeTableFlag = false;
      this.items = [{ label: 'Order' }, { label: 'Line Items' }]
    }
  }

  getConnectionsList() {
    this.dataFetchServ
    .getConnectionsList()
    .subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['data']);
        return;
      }
      if (data['status'] == 1) {
       let connectionArray = data['data'];
       connectionArray.forEach(element => {
         let conJson = JSON.parse(element.connection_json)
         this.connectionList.push({label:element.connection_name,value:conJson.applicationName+'_'+conJson.networkCode})
       });
          console.log("this.connectionList====", this.connectionList);
      }
    })
  }
  selctedConnector
  createConnection(selctedConnector){
     console.log("selctedConnector    :",selctedConnector)
     if(selctedConnector!=undefined){
      //  this.initialLoading();
      // this.appConfigService.changeConnectionSource(selctedConnector);
      this.campaignTableFlag=true;
      this.breadcrumbFlag=false;
      // this.connectorFlag=true;
      this.selctedConnector = selctedConnector;
      localStorage.setItem('selctedConnector', selctedConnector);
     }
  }

  deleteOrder(rowData){
    console.log("rowData    :",rowData)
    let request = {
       updated_by: this.currentUser,
       id:rowData.id
    }

    this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?',
      key: 'campaign',
      accept: () => {
        this.dataFetchServ
        .deleteOrder(JSON.stringify(request))
        .subscribe(data => {
          if (data['status'] === 0) {
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Server Error',
              detail: 'Please refresh the page'
            });
            console.log(data['data']);
            return;
          }
          if (data['status'] === -1) {
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Error',
              detail: 'Order name already push to GAM.'
            });
            console.log(data['data']);
            return;
          }
          if (data['status'] == 1) {
            this.toastService.displayToast({
              severity: 'success',
              summary: 'Campaign Management',
              detail: 'Order deleted successfully'
            });
          }
          this.initialLoading();
        });

      },
      reject: () => {
      }

    });

  }

  deleteLineItem(rowData){
    console.log("rowData    :",rowData)
     let request = {
        updated_by: this.currentUser,
        id:rowData.id
     }
     this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?',
      key: 'campaign',
      accept: () => {
        this.dataFetchServ
        .deleteLineItem(JSON.stringify(request))
        .subscribe(data => {
          if (data['status'] === 0) {
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Server Error',
              detail: 'Please refresh the page'
            });
            console.log(data['data']);
            return;
          }
          if (data['status'] === -1) {
           this.toastService.displayToast({
             severity: 'error',
             summary: 'Error',
             detail: 'Line Item already pushed to GAM.'
           });
           console.log(data['data']);
           return;
         }
          if (data['status'] == 1) {
            this.toastService.displayToast({
              severity: 'success',
              summary: 'Campaign Management',
              detail: 'Line Item deleted successfully'
            });
          }
         //  this.initialLoading();
          this.loadTableDataLineItem(null, 'back');
        });

      },
      reject: () => {
      }

    });


  }
  deleteCreative(rowData:any){
    console.log("rowData    :",rowData)
     let request = {
        updated_by: this.currentUser,
        id:rowData.id
     }

     this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?',
      key: 'campaign',
      accept: () => {
        this.dataFetchServ.deleteCreative(JSON.stringify(request))
        .subscribe(data => {
          if (data['status'] === 0) {
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Server Error',
              detail: 'Please refresh the page'
            });
            console.log(data['data']);
            return;
          }
          if (data['status'] === -1) {
           this.toastService.displayToast({
             severity: 'error',
             summary: 'Error',
             detail: 'Creative already pushed to GAM.'
           });
           console.log(data['data']);
           return;
         }
          if (data['status'] == 1) {
            this.toastService.displayToast({
              severity: 'success',
              summary: 'Campaign Management',
              detail: 'Creative deleted successfully'
            });
          }
         //  this.initialLoading();
          this.loadTableDataCreative(null, 'back')
        });

      },
      reject: () => {
      }
    });


  }

  openLineItemDetails(rowData) {
    this.connectionList.forEach(element=>{
      if(element.label == this.selectedConnectorLabel){
        this.selctedConnector = element.value;
        console.log("this.selctedConnector   :",this.selctedConnector)
        this.createConnection(this.selctedConnector)
      }
    })

    if(this.selctedConnector!=undefined){
      localStorage.setItem('selctedConnector', this.selctedConnector);
    }else{
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Server Error',
        detail: 'Please select the Connector first'
      });
      return;
    }
    this.editOperationFlag = true;
    this.connectorFlag=false;
    this.breadcrumbFlag=false;
    this.lineItemTableFlag = false;
    this.operationIdentity='editLineItem';
    this.formData = rowData;
  }

  openCreativeDetails(rowData) {
    this.connectionList.forEach(element=>{
      if(element.label == this.selectedConnectorLabel){
        this.selctedConnector = element.value;
        console.log("this.selctedConnector   :",this.selctedConnector)
        this.createConnection(this.selctedConnector)
      }
    })

    if(this.selctedConnector!=undefined){
      localStorage.setItem('selctedConnector', this.selctedConnector);
    }else{
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Server Error',
        detail: 'Please select the Connector first'
      });
      return;
    }
    this.editOperationFlag = true;
    this.connectorFlag=false;
    this.breadcrumbFlag=false;
    this.creativeTableFlag = false;
    this.operationIdentity='editCreative'
    this.formData = rowData;
  }


}
