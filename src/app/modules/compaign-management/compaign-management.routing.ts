import { Routes } from '@angular/router';
import { CompaignManagementComponent } from './compaign-management.component';
import {AddCompaignComponent} from './add-campaign/add-compaign.component'
import {CompaignDetailsComponent} from './campaign-details/compaign-details.component';
import {ConnectorDetailsComponent} from './connector-details/connector-details.component';
import {ConnectorListComponent} from './connector-list/connector-list.component';
export const CompaignManagementRoutes: Routes = [
  {
    path: '',
    component: CompaignManagementComponent
  },
  {
    path: 'add-campaign',
    component: AddCompaignComponent
  },
  {
    path: 'campaign-details',
    component : CompaignDetailsComponent
  },
  // {
  //   path: '',
  //   component: ConnectorDetailsComponent
  // },
  {
    path: 'campaign-connector',
    component: ConnectorListComponent
  }
];
