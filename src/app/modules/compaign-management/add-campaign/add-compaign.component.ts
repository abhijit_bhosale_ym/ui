import { Component, OnInit, Input, Output, EventEmitter, ViewChild,ElementRef } from '@angular/core';
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
  FormArray
} from '@angular/forms';
import { ConfirmationService } from 'primeng/api';
import { TreeNode } from 'primeng/api';
import { CommonLibService } from 'src/app/_services';
import { DialogService } from 'primeng/dynamicdialog';
import { TagCodePopupComponent } from '../tag-code-popup/tag-code-popup.component'
import {
  Router,
  ActivatedRoute,
  NavigationExtras,
  NavigationEnd,
  RouterEvent
} from '@angular/router';

import { SelectItemGroup, MenuItem } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { FetchApiDataService } from '../fetch-api-data.service';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { Accordion } from 'primeng';
import { Subscription } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'ym-add-compaign',
  templateUrl: './add-compaign.component.html',
  styleUrls: ['./add-compaign.component.scss']
})
export class AddCompaignComponent implements OnInit {
  // comapiagnStatus = ['Submitted for Aprroval','Pending','Revise','Created in BW','Rejected'];
  private appConfigObs: Subscription;
  appConfig: object = {};
  currentUser: any;
  addCompaignForm: FormGroup;
  addCreativeFormDynamic: FormGroup;
  addCreativeForm: FormGroup;
  addGamLineItemForm: FormGroup;
  targetFlag = false;
  value: Date;
  value1: Date;
  keyValuePair = [];
  sampleValueList = [];
  showAddPlacement = false;
  addPlacementForm: FormGroup;
  customTargeting: FormGroup;
  demoList: any[];
  dimColDef: any[];
  DesktopTableJson: Object;
  DesktopTableData: TreeNode[];

  mobileTableJson: Object;
  mobileTableData: TreeNode[];

  tabletTableJson: Object;
  tabletTableData: TreeNode[];
  aggTableData: TreeNode[];
  aggTableJson: Object;
  tableData = [];
  tableData1 = [];
  tabIndex = 0;
  addCreativeDetails = false;
  adServerName = 'GAM'
  showLineItem = false;
  showCreatives = false;
  lineItemNote;
  gamDateTimeValue = false;
  selectedGamAdvertisers: any;
  availableMobileApps = [];
  gamCampaignId;
  gamLineItemId;
  DisplayVASTFlag: boolean = false;
  isGamTrafficker: any;
  isGamSalesperson: any;
  isprimarySalesPersonPresent = false;
  presetAdd = false;
  selectedCustomTargeting:any;
  li_total_value :any;
  discount_value : any;
  final_total_value : any;
  frequencyCapFlag = false;
  lineStartMinDate = new Date();
  lineEndMinDate : Date;
  lineStartMaxDate = new Date("20301231");
  @Input() formIdentity: string;
  @Input() orderId: string;
  @Input() lineItemId: string;
  @Input() operationIdentity: string;
  @Input() formData:any;
  @Output() triggeredEvent = new EventEmitter<any>();
  activeIndex: number = null;
  lastIndex = -1;
  customTargetingValid = false;
  @ViewChild('accordion') accordion: Accordion;
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  image:any;
  data:any;
  CreativeAssetData:any;
    creative_server_id:any;
    retrievedCreativeId:any;


  closeAllAccordionTabs() {
    // if(!isNullOrUndefined(this.accordion.tabs)){
    for (let tab of this.accordion.tabs) {
      if (tab.selected) tab.selected = false;
      // }
    }
  }
  availableAdType = [
    { label: 'Display', value: 'DISPLAY' },
    { label: 'Video', value: 'VIDEO' },
  ]
  availablePresetList: any = [];

  availableLineType = [
    { label: 'Sponsorship(4)', value: 'sponsorship', priority: '4', note: 'Highest-ranking line item type for fixed-position and time-based campaigns' },
    { label: 'Standard(6,8,10)', value: 'standard', priority: '6', note: 'For impression-based campaigns', availablePriority: [{ label: 'High', value: 'high' }, { label: 'Normal', value: 'normal' }, { label: 'Low', value: 'low' }] },
    { label: 'Network(12)', value: 'network', priority: '12', note: 'For unsold inventory' },
    { label: 'Bulk(12)', value: 'bulk', priority: '12', note: 'For unsold inventory' },
    { label: 'Price priority(12)', value: 'price_priority', priority: '12', note: 'For unsold inventory' },
    { label: 'House(16) ', value: 'house', priority: '16', note: 'Lowest-ranking line item type. Typically used for ads that promote products and services chosen by you' },
  ]

  availableRateValues = [
    { label: 'CPD', value: 'CPD' },
    { label: 'CPC', value: 'CPC' },
    { label: 'CPM', value: 'CPM' }
  ]

  availableRateValuesForHouse = [
    { label: 'CPC', value: 'CPC' },
    { label: 'CPM', value: 'CPM' }
  ]

  availableAndOr = [
    { label: 'AND', value: 'AND' },
    { label: 'OR', value: 'OR' },
  ]


  availablisanyof = [
    { label: 'is any of', value: 'is any of' },
    { label: 'is none of', value: 'is none of' },
  ]

  selectedQtyValues = {}
  selectedQtyValuesBulk = {}
  selectedQtyValuesPricePriority = {}
  customTargetingArr = []
  selectedDiscountValue = "PERCENTAGE";

  availableQuantityValues = [
    { label: 'Impressions', value: 'IMPRESSIONS', rateValues: 'CPM' },
    { label: 'Clicks', value: 'CLICKS', rateValues: 'CPC' },
    { label: 'Viewable Impressions', value: 'VIEWABLE_IMPRESSIONS', rateValues: 'Viewable CPM' },
  ]

  availableQuantityValuesBulk = [
    { label: 'Impressions', value: 'IMPRESSIONS', rateValues: 'CPM' },
    { label: 'Clicks', value: 'CLICKS', rateValues: 'CPC' }]

  availableDiscountValues = [
    { label: 'Absolute Value', value: 'ABSOLUTE_VALUE' },
    { label: 'Percentage', value: 'PERCENTAGE' }
  ]

  availableImpressionLimit = [
    { label: 'None', value: 'NONE' },
    { label: 'Lifetime', value: 'LIFETIME' },
  ]

  availablePricePriorityLimit = [
    { label: 'None', value: 'NONE' },
    { label: 'Daily', value: 'DAILY' },
    { label: 'Lifetime', value: 'LIFETIME' },
  ]

  selectedPricePriorityLimitValue = {}
  selectedLineItem = {};

  comapiagnStatus = [
    { label: 'Select Campaign Status', value: null },
    { label: 'Submitted for Aprroval', value: 'Submitted_for_aprroval' },
    { label: 'Pending', value: 'Pending' },
    { label: 'Revise', value: 'Revise' },
    { label: 'Created in BW', value: 'Created_in_BW' },
    { label: 'Rejected', value: 'Rejected' }
  ];

  campaignRateTypeOption = [
    { label: 'Select Rate Type', value: null },
    { label: 'CPCV', value: 'CPVC' },
    { label: 'CPM', value: 'CPM' }];

  alpPixelOption = [
    { label: 'Select a Value', value: null },
    { label: 'N/A', value: "N/A" },
    { label: 'Yes', value: 'Yes' },
    { label: 'No', value: 'No' }];

  viewableCompleteOption = [
    { label: 'Select a Value', value: null },
    { label: 'N/A', value: "N/A" },
    { label: 'Yes', value: 'Yes' },
    { label: 'No', value: 'No' }];

  alphonsoAttributionStudyOption = [
    { label: 'Select a Value', value: null },
    { label: 'N/A', value: "N/A" },
    { label: 'Yes', value: 'Yes' },
    { label: 'No', value: 'No' }];

  brandLiftStudyOption = [
    { label: 'Select a Value', value: null },
    { label: 'N/A', value: "N/A" },
    { label: 'Yes', value: 'Yes' },
    { label: 'No', value: 'No' }];

  reportingOption = [
    { label: 'Select a Value', value: null },
    { label: 'Post-only', value: 'Post-only' },
    { label: 'Mid and Post', value: "Mid and Post" }];

  billingAttributionOption = [
    { label: 'Select a Value', value: null },
    { label: 'N/A', value: "N/A" },
    { label: 'Yes', value: 'Yes' },
    { label: 'No', value: 'No' }];

  accessToAlphonsoOption = [
    { label: 'Select a Value', value: null },
    { label: 'N/A', value: "N/A" },
    { label: 'Yes', value: 'Yes' },
    { label: 'No', value: 'No' }];

  availableCutomTargingCon = [
    { label: 'is any of', value: 'is_any_of' },
    { label: 'is none of', value: 'is_none_of' },
  ]

  gamWeekDays: any = [
    { label: 'Mon', value: 'mon', selectedGamDays: "" },
    { label: 'Tue', value: 'tue', selectedGamDays: "" },
    { label: 'Wed', value: 'wed', selectedGamDays: "" },
    { label: 'Thur', value: 'thur', selectedGamDays: "" },
    { label: 'Fri', value: 'fri', selectedGamDays: "" },
    { label: 'Sat', value: 'sat', selectedGamDays: "" },
    { label: 'Sun', value: 'sun', selectedGamDays: "" },
  ]

  severities: any = [{ 'label': 'Highest', 'value': 'Highest' }, { 'label': 'Medium', 'value': 'Medium' }, { 'label': 'Lowest', 'value': 'Lowest' }]
  selectedSeverities: any
  value2: string;

  paymentOptions;

  availableVideoPositioning: any = [
    { 'id': 'PREROLL', name: 'Pre-roll', 'checked': false, 'hideShow': false },
    { 'id': 'MIDROLL', name: 'Mid-roll', 'checked': false, 'hideShow': false },
    { 'id': 'POSTROLL', name: 'Post-roll', 'checked': false, 'hideShow': false },]



  availableInventoryType: any = [
    { 'id': 'display', name: 'Display', 'checked': false, 'hideShow': false },
    { 'id': 'mobile-app', name: 'Mobile-App', 'checked': false, 'hideShow': false },
    { 'id': 'in-stream-video', name: 'In-stream video', 'checked': false, 'hideShow': false },]

  availableInventoryFormat: any = [
    { 'id': 'rewarded', name: 'Rewarded', 'checked': false, 'hideShow': false },]


  availableDeliverOptions: any = [
    { id: 'EVENLY', name: 'Evenly' },
    { id: 'FRONTLOADED', name: 'Frontloaded' },
    { id: 'AS_FAST_AS_POSSIBLE', name: 'As fast as possible' }]

  availableDisplayCompany: any = [
    { id: 'OPTIONAL', name: 'Optional' },
    { id: 'AT_LEAST_ONE', name: 'At least once' },
    { id: 'ALL', name: 'All' }
  ]

  availableRotateCreativeSets: any = [
    { id: 'EVEN', name: 'Evenly' },
    { id: 'OPTIMIZED', name: 'Optimized' },
    // { id: 'weighted', name: 'Weighted' }, Not Available in API
    { id: 'SEQUENTIAL', name: 'Sequential' },
  ]

  availableDisplayCreative: any = [
    { id: 'ALL_ROADBLOCK', name: 'All'},
    { id: 'ONLY_ONE', name: 'Onle one' },
    { id: 'ONE_OR_MORE', name: 'One or more' },
    { id: 'AS_MANY_AS_POSSIBLE', name: 'As many as possible' },
  ]


  availableChildDirectedAds: any = [
    { id: 'DISALLOWED', name: "Don't serve on child directed-requests" },
    { id: 'ALLOWED', name: 'Allow to serve on child directed-request' }
  ]

  availableFrequencyCapOptions = [
    { id: 'minute', name: 'minutes' },
    { id: 'hour', name: 'hours' },
    { id: 'day', name: 'days' },
    { id: 'week', name: 'weeks' },
    { id: 'month', name: 'months' },
    { id: 'lifetime', name: 'lifetime' },
    { id: 'pod', name: 'pods' },
    { id: 'stream', name: 'stream' },
  ]

  transitionList = [
    { label: 'none', value: 'none' },
    { label: 'dissolve', value: 'dissolve' },
    { label: 'movein', value: 'movein' },
    { label: 'push', value: 'push' },
    { label: 'scale', value: 'scale' },
    { label: 'pivot', value: 'pivot' },
  ]

  transitionDirectionList = [
    { label: 'l', value: 'l' },
    { label: 'r', value: 'r' },
    { label: 'u', value: 'u' },
    { label: 'd', value: 'd' },
    { label: 'none', value: 'none' },
  ]

  transitionAnchorList = [
    { label: 'c', value: 'c' },
    { label: 'tl', value: 'tl' },
    { label: 'tr', value: 'tr' },
    { label: 'bl', value: 'bl' },
    { label: 'br', value: 'br' },
    { label: 'none', value: 'none' },
  ]

  urlTargetList = [
    { label: 'in-app-browser', value: 'in-app-browser' },
    { label: 'external-browser', value: 'external-browser' },
  ]

  transionEasingList = [
    { label: 'ease', value: 'ease' },
    { label: 'linear', value: 'linear' },
    { label: 'ease-in', value: 'ease-in' },
    { label: 'ease-out', value: 'ease-out' },
    { label: 'ease-in-out', value: 'ease-in-out' },
    { label: 'none', value: 'none' },
  ]


  availableTimeZone: any = [
    { id: "publisher_time_zone", name: "Publisher's time zone" },
    { id: "user_time_zone", name: "User's time zone" }]

  hourMinuteList: any = [
    { id: "00:00", name: "12:00 AM" }, { id: "00:15", name: "12:15 AM" }, { id: "00:30", name: "12:30 AM" }, { id: "00:45", name: "12:45 AM" },
    { id: "01:00", name: "01:00 AM" }, { id: "01:15", name: "01:15 AM" }, { id: "01:30", name: "01:30 AM" }, { id: "01:45", name: "01:45 AM" },
    { id: "02:00", name: "02:00 AM" }, { id: "02:15", name: "02:15 AM" }, { id: "02:30", name: "02:30 AM" }, { id: "02:45", name: "02:45 AM" },
    { id: "03:00", name: "03:00 AM" }, { id: "03:15", name: "03:15 AM" }, { id: "03:30", name: "03:30 AM" }, { id: "03:45", name: "03:45 AM" },
    { id: "04:00", name: "04:00 AM" }, { id: "04:15", name: "04:15 AM" }, { id: "04:30", name: "04:30 AM" }, { id: "04:45", name: "04:45 AM" },
    { id: "05:00", name: "05:00 AM" }, { id: "05:15", name: "05:15 AM" }, { id: "05:30", name: "05:30 AM" }, { id: "05:45", name: "05:45 AM" },
    { id: "06:00", name: "06:00 AM" }, { id: "06:15", name: "06:15 AM" }, { id: "06:30", name: "06:30 AM" }, { id: "06:45", name: "06:45 AM" },
    { id: "07:00", name: "07:00 AM" }, { id: "07:15", name: "07:15 AM" }, { id: "07:30", name: "07:30 AM" }, { id: "07:45", name: "07:45 AM" },
    { id: "08:00", name: "08:00 AM" }, { id: "08:15", name: "08:15 AM" }, { id: "08:30", name: "08:30 AM" }, { id: "08:45", name: "08:45 AM" },
    { id: "09:00", name: "09:00 AM" }, { id: "09:15", name: "09:15 AM" }, { id: "09:30", name: "09:30 AM" }, { id: "09:45", name: "09:45 AM" },
    { id: "10:00", name: "10:00 AM" }, { id: "10:15", name: "10:15 AM" }, { id: "10:30", name: "10:30 AM" }, { id: "10:45", name: "10:45 AM" },
    { id: "11:00", name: "11:00 AM" }, { id: "11:15", name: "11:15 AM" }, { id: "11:30", name: "11:30 AM" }, { id: "11:45", name: "11:45 AM" },


    { id: "12:00", name: "12:00 PM" }, { id: "12:15", name: "12:15 PM" }, { id: "12:30", name: "12:30 PM" }, { id: "12:45", name: "12:45 PM" },
    { id: "13:00", name: "01:00 PM" }, { id: "13:15", name: "01:15 PM" }, { id: "13:30", name: "01:30 PM" }, { id: "13:45", name: "01:45 PM" },
    { id: "14:00", name: "02:00 PM" }, { id: "14:15", name: "02:15 PM" }, { id: "14:30", name: "02:30 PM" }, { id: "14:45", name: "02:45 PM" },
    { id: "15:00", name: "03:00 PM" }, { id: "15:15", name: "03:15 PM" }, { id: "15:30", name: "03:30 PM" }, { id: "15:45", name: "03:45 PM" },
    { id: "16:00", name: "04:00 PM" }, { id: "16:15", name: "04:15 PM" }, { id: "16:30", name: "04:30 PM" }, { id: "16:45", name: "04:45 PM" },
    { id: "17:00", name: "05:00 PM" }, { id: "17:15", name: "05:15 PM" }, { id: "17:30", name: "05:30 PM" }, { id: "17:45", name: "05:45 PM" },
    { id: "18:00", name: "06:00 PM" }, { id: "18:15", name: "06:15 PM" }, { id: "18:30", name: "06:30 PM" }, { id: "18:45", name: "06:45 PM" },
    { id: "19:00", name: "07:00 PM" }, { id: "19:15", name: "07:15 PM" }, { id: "19:30", name: "07:30 PM" }, { id: "19:45", name: "07:45 PM" },
    { id: "20:00", name: "08:00 PM" }, { id: "20:15", name: "08:15 PM" }, { id: "20:30", name: "08:30 PM" }, { id: "20:45", name: "08:45 PM" },
    { id: "21:00", name: "09:00 PM" }, { id: "21:15", name: "09:15 PM" }, { id: "21:30", name: "09:30 PM" }, { id: "21:45", name: "09:45 PM" },
    { id: "22:00", name: "10:00 PM" }, { id: "22:15", name: "10:15 PM" }, { id: "22:30", name: "10:30 PM" }, { id: "22:45", name: "10:45 PM" },
    { id: "23:00", name: "11:00 PM" }, { id: "23:15", name: "11:15 PM" }, { id: "23:30", name: "11:30 PM" }, { id: "23:45", name: "11:45 PM" },
  ]

  destinationList = [
    { label: 'Click-Through URL', value: 'click_through_url' },
    { label: 'Not Clickable', value: 'not-clickable' }
  ]

  nativeFormatList = [
    { label: 'Native app install ad', value: 'Native app install ad' },
    { label: 'Native content ad', value: 'Native content ad' },
    { label: 'Native video app install ad', value: 'Native video app install ad' },
    { label: 'Native video content ad', value: 'Native video content ad' },
  ]
  customSettingList = [
    { label: 'Use EU user consent settings', value: 'Use EU user consent settings' },
    { label: 'Set custom settings', value: 'Set custom settings' },
  ]
  enableDisableList = [
    { label: 'Disabled', value: 'Disabled' },
    { label: 'Enabled', value: 'Enabled' },
  ]

  redirectTypeList = [
    { label: 'Video', value: 'Video' },
    { label: 'Overlay', value: 'Overlay' },
    { label: 'Both video and overlay', value: 'Both video and overlay' },
  ]

  typeList = [
    { label: 'Video', value: 'Video' },
    { label: 'Overlay', value: 'Overlay' },
    { label: 'Redirect', value: 'Redirect' },
  ];
  sslCompatibleList = [
    { label: 'Auto-scan', value: 'Auto-scan' },
    { label: 'Manual', value: 'Manual' }
  ]


  gamWeekDaysArr = [];
  expectedCreativeArr = []
  gamSelectedWeekDays: any;
  expectedCreativeCount = 1;
  gamFrequencyCapArr = [];
  userDomainIncluded = [];
  userDomainExcluded = [];
  availableAdvertisers = [];
  availableGamLables = [];
  gamOrderData: any;
  availableGamAdContacts: any;
  availableGamAgencies: any;
  availableGamSecTrafficker: any;
  availableGamSalesperson: any;
  availableGamSecSalesperson: any;
  availableGamTargetingOS = [];
  availableTargetingOS = [];
  availableGamBrowswerLang = [];
  availableGamBrowswer = [];
  availableGamDeviceCategory = [];
  availableGamGeograhy = [];
  availableGamDeviceCapability = [];
  availableGamDeviceManufaturere = [];
  availableGamDeviceBandwidth = [];
  availableGammobileCarrier = [];
  availableGamInventoryType = []
  availableGamAdUnits = [];
  availableGamPlacement = [];
  availableGamAgencyContact = [];

  gamOrderName;
  gamOrderTrafficker;
  selectedLabels = [];
  selectedGamContacts = [];
  selectedGamAgencies: any
  selectedGamSecTrafficker = []
  selectedGamSalesperson: any;
  selectedGamSecSalesPerson = [];
  gamOrderPoNumber;
  gamOrderNote;
  gamOrderCustomFields;
  selectedGamOrderTrafficker: any;
  currency;
  currencySymbol;
  density1Checked = true;

  constructor(
    private appConfigService: AppConfigService,
    private formBuilder: FormBuilder,
    private confirmationService: ConfirmationService,
    private currentActivatedRoute: ActivatedRoute,
    private router: Router,
    private libServ: CommonLibService,
    private dialogService: DialogService,
    private dataFetchServ: FetchApiDataService,
    private toastService: ToastService,
    private sanitizer:DomSanitizer
  ) {

    this.paymentOptions = [
      'Option 1', 'Option 2', 'Option 3'
    ];


    this.value2 = this.paymentOptions[0];

  }

  standardCreativeTemplateList1 = [
    { label: 'Forecasting Pixel', value: 'Forecasting Pixel' },
    { label: 'HTML5 animated ad', value: 'HTML5 animated ad' },
    { label: 'Image banner with optional third party tracking', value: 'Image banner with optional third party tracking' },
    { label: 'Image with Google +1', value: 'Image with Google +1' },
    { label: 'Mobile application download', value: 'Mobile application download' },
    { label: 'Mobile fix-position banner', value: 'Mobile fix-position banner' },
    { label: 'Mobile image carousel(app and web)', value: 'Mobile image carousel(app and web)' },
    { label: 'Mobile image with Google +1', value: 'Mobile image with Google +1' },
    { label: 'Mobile interstitial with auto close (app)', value: 'Mobile interstitial with auto close (app)' },
    { label: 'Pop-up or pop-under with image content', value: 'Pop-up or pop-under with image content' },
    { label: 'Pop-up or pop-under with third party content', value: 'Pop-up or pop-under with third party content' },
    { label: 'Text ad', value: 'Text ad' },

  ]
  standardCreativeTemplateList = [
    { label: 'Forecasting Pixel', value: 'Forecasting Pixel', 'checked': false },
    { label: 'HTML5 animated ad', value: 'HTML5 animated ad', 'checked': false },
    { label: 'Image banner with optional third party tracking', value: 'Image banner with optional third party tracking', 'checked': false },
    { label: 'Image with Google +1', value: 'Image with Google +1', 'checked': false },
    { label: 'Mobile application download', value: 'Mobile application download', 'checked': false },
    { label: 'Mobile fix-position banner', value: 'Mobile fix-position banner', 'checked': false },
    { label: 'Mobile image carousel(app and web)', value: 'Mobile image carousel(app and web)', 'checked': false },
    { label: 'Mobile image with Google +1', value: 'Mobile image with Google +1', 'checked': false },
    { label: 'Mobile interstitial with auto close (app)', value: 'Mobile interstitial with auto close (app)', 'checked': false },
    { label: 'Pop-up or pop-under with image content', value: 'Pop-up or pop-under with image content', 'checked': false },
    { label: 'Pop-up or pop-under with third party content', value: 'Pop-up or pop-under with third party content', 'checked': false },
    { label: 'Text ad', value: 'Text ad', 'checked': false },

  ]
  standardCreativeTemplateListDynamic = [
    { label: 'Forecasting Pixel', value: 'Forecasting Pixel', 'checked': false, 'dynamic': [], 'hideShow': false },
    { label: 'Mobile image carousel(app and web)', value: 'Mobile image carousel(app and web)', 'hideShow': false, 'checked': false, 'dynamic': [] },
    { label: 'Mobile image with Google +1', value: 'Mobile image with Google +1', 'checked': false, 'hideShow': false, 'dynamic': this.standardCreativeTemplateList },
    { label: 'Mobile interstitial with auto close (app)', value: 'Mobile interstitial with auto close (app)', 'hideShow': false, 'checked': false, 'dynamic': this.standardCreativeTemplateList },
    { label: 'Pop-up or pop-under with image content', value: 'Pop-up or pop-under with image content', 'checked': false, 'hideShow': false, 'dynamic': this.standardCreativeTemplateList },
    { label: 'Pop-up or pop-under with third party content', value: 'Pop-up or pop-under with third party content', 'hideShow': false, 'checked': false, 'dynamic': [] },
    { label: 'Text ad', value: 'Text ad', 'checked': false, 'hideShow': false, 'dynamic': [] },
  ]

  selectedData = [];
  selectedDataDynamic = [];
  rowSelect(event, rowData) {
    if (event.checked) {

      this.selectedData.push({ 'label': rowData.label, 'value': rowData.value, 'checked': false })
    }
    if (!event.checked) {
      let index = 0
      this.selectedData.forEach(element => {
        if (element.label == rowData.label) {
          this.selectedData.splice(index, 1);
        }
        index++;
      })
    }
  }

  removeElement(rowData) {
    let index = 0
    this.selectedData.forEach(element => {
      if (element.label == rowData.label) {
        this.selectedData.splice(index, 1);
      }
      index++;
    })
    this.standardCreativeTemplateList.forEach(element => {
      if (element.label == rowData.label) {
        element.checked = false;
      }
      index++;
    })
  }


  toggleSelection(event, data) {
    this.selectedData.forEach(element => {
      if (element.label == data.label) {
        element.label = data.label
        element.value = data.value
        element.checked = event.checked
      }
    })

  }

  rowSelectDynamic(event, rowData) {
    if (event.checked) {

      this.selectedDataDynamic.push({ 'label': rowData.label, 'value': rowData.value, 'checked': false })
    }
    if (!event.checked) {
      let index = 0
      this.selectedDataDynamic.forEach(element => {
        if (element.label == rowData.label) {
          this.selectedDataDynamic.splice(index, 1);
        }
        index++;
      })
    }
  }
  toggleSelectionDynamic(event, data) {
    this.selectedDataDynamic.forEach(element => {
      if (element.label == data.label) {
        element.label = data.label
        element.value = data.value
        element.checked = event.checked
      }
    })

  }

  removeElementDynamic(rowData) {
    let index = 0
    this.selectedDataDynamic.forEach(element => {
      if (element.label == rowData.label) {
        this.selectedDataDynamic.splice(index, 1);
      }
      index++;
    })
    this.standardCreativeTemplateListDynamic.forEach(element => {
      if (element.label == rowData.label) {
        element.checked = false;
      }
      index++;
    })
  }


  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }
  selectedConnector
  ngOnInit() {
    this.lineEndMinDate = this.lineStartMinDate;
    if(this.operationIdentity=='editLineItem'){
      this.formIdentity ='lineItem';
    }
    if( this.operationIdentity=='editCreative'){
      this.formIdentity ='creative';
    }
    this.showLineItem = this.formIdentity == 'lineItem' ? true : false;

    this.gamCampaignId = this.orderId;
    this.gamLineItemId = this.lineItemId;

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      console.log("appConfig   :", appConfig)
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        this.currentUser = this.appConfig['user']['id'];
      }
    });

  //  this.appConfigService.selectedConnector$.subscribe(selectedConnector => {
  //   console.log("selectedConnector   :",selectedConnector);
  //   // this.selectedConnector = selectedConnector;
  //   });

    let data = localStorage.getItem("selctedConnector");
    this.selectedConnector = data;

    this.displayOrVideo("")
    this.initialLoading();
    this.initializationOfList();
    this.initializeDynamicCreatives();
    // this.initializationAddCreativeFormControls();
    this.initializationOfCustomTargetingForm();
    this.getAvailablePresetList();
    this.addCompaignForm = this.formBuilder.group({
      cid: [''],
      compaign_name: ['', Validators.required],
      compaign_status: [''],
      campaign_Rate_Type: [''],
      alp_Pixel_Option: [''],
      viewable_Complete_Option: [''],
      alphonso_AttributionStudyOption: [''],
      brand_LiftStudyOption: [''],
      reporting_Option: [''],
      billing_AttributionOption: [''],
      billingAdServer: [''],
      access_ToAlphonsoOption: [''],
      start_date: [''],
      end_date: [''],
      advertiser: ['', Validators.required],
      advertiser_domain: [''],
      agency: ['', Validators.required],
      third_party_verification: [''],
      whitelist: [''],
      blacklist: [''],
      tremor_video_ad_sale_no: [''],
      budget: ['', [Validators.required, Validators.min(1), Validators.max(9999999999)]],
      impressions: ['', [Validators.required, Validators.min(0), Validators.max(9999999999)]],
      rate: ['', Validators.max(100)],
      frequency_cap: ['']
    });

    this.addGamLineItemForm = this.formBuilder.group({
      lineadType: [''],
      lineItemName: ['', Validators.required],
      lineItemType: [this.availableLineType[0], Validators.required], 
      presetTargeting: '',
      presetTargetingName: '',
      lineItemTypePriority: [''],
      lineItemTypePriorityValue: ['4'],
      expectedCreatives: [''],
      lineItemLables: [''],
      sameAdvertiserException: [''],
      comments: [''],
      customFields: [''],
      openMeasurementViewPartner: [''],
      startDate: [''],
      endDate: [''],
      goal: [''],
      rate: [''],
      rateValue: [''],
      rateValueHouse: [''],
      discount: ['0'],
      discountValues: [''],
      cpmValue: [''],
      quantity: [''],
      quantityValues: [''],
      qtyBulkValue: [''],
      rate1: [''],
      standardQtyRateValue: [''],
      bulkQtyRateValue: [''],
      impressionLimit: [''],
      impressions: [''],
      pricePriorityLimitValue: [''],
      dailyQtyValuePricePriority: [''],
      dailyQtyValuePricePriority1: [''],
      pricePriorityRate: [''],
      pricePriorityLimitNoneRateValue: [''],
      pricePriorityLimitDailyRateValue: ['CPC'],
      standardDiscount: ['0'],
      standardDiscountValue: [''],
      videoCreativeProfiles: [''],
      maxDuration: [''],
      deliverImpressions: [''],
      displayCompany: [''],
      rotateCreativeSets: [''],
      displayCreative: [''],
      childDirected: [''],
      dayAndTime: [''],
      frequencyCapFlag: [false],
      timeZone: [''],
      targeting_json: [],
      liTotalValue:[],
      timeZomeArray: this.formBuilder.array([
        this.formBuilder.group({
          timeZoneStartTime: [{ id: "00:00", name: "12:00 AM" }],
          timeZoneEndTime: [{ id: "00:00", name: "12:00 AM" }],
          timeZoneWeeksDays: [''],
          timeZoneWeeksDaysExclude: [''],
          daysArr: this.formBuilder.array([
            new FormControl(true),
            new FormControl(false),
            new FormControl(false),
            new FormControl(false),
            new FormControl(false),
            new FormControl(false),
            new FormControl(false),

          ])
        })
      ]),

      frequencyCapArray: this.formBuilder.array([
        this.formBuilder.group({
          impressionPer: ['1'],
          impressionPer1: ['1'],
          selectedFreqCap: [{ id: 'day', name: 'days' }],

        })
      ])


    })



    this.addPlacementForm = this.formBuilder.group({
      placement_id: [''],
      placement_name: ['', Validators.required],
      lineitemid: [''],
      placement_start_date: ['', Validators.required],
      placement_end_date: [''],
      placement_impressions: ['', [Validators.required, Validators.min(0), Validators.max(9999999999)]],
      placement_budget: ['', [Validators.required, Validators.min(1), Validators.max(9999999999)]],
      // placement_rate_type:[''],
      campaign_Rate_Type: [''],
      placement_rate: ['', Validators.max(100)],
      placementlineitemtype: ['', Validators.required],
      targeting: [''],
      placementsecondarykpi: [''],
      creativeDetails: ['']

    });

    this.selectedLineItem = this.availableLineType[0];
    this.expectedCreativeArr.push("abc");
    this.gamFrequencyCapArr.push({
      impressionPer: '',
      impressionPer1: '',
      availableFrequencyCap: this.availableFrequencyCapOptions,
      selectedFreqCap: ''
    });
    this.customTargetingArr.push({
      dims: [],
      condition: this.availableCutomTargingCon,
      values: [],
      selectedDim: '',
      selectedCondition: [],
      selectedValues: []
    }
    )
    this.selectedPricePriorityLimitValue = this.availablePricePriorityLimit[0]
    this.selectedQtyValues = this.availableQuantityValues[0];
    this.selectedSeverities = this.severities[0].label
    this.gamWeekDaysArr.push(
      {
        startTime: new Date(),
        endTime: new Date(),
        availableWeekDays: this.gamWeekDays,
        isNotRun: false,
        selectedGamDays: []
      }
    )


    this.dimColDef = [
      {
        field: 'lid',
        displayName: 'LID',
        format: '',
        width: '100',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
      },
      {
        field: 'tag_name',
        displayName: 'Tag Name',
        format: 'string',
        width: '200',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
      },
      {
        field: 'tag_type',
        displayName: 'Tag Type',
        format: 'string',
        width: '120',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
      },
      {
        field: 'tag_code',
        displayName: 'Tag Code',
        format: '',
        width: '90',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
      },


      {
        field: 'additional_pixel',
        displayName: 'Additional Pixel',
        width: '160',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'id',
        displayName: 'Additional Pixel Code',
        format: '',
        width: '150',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'id',
        displayName: 'Impressions',
        format: '',
        width: '120',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'comment',
        displayName: 'Video Duration',
        format: '',
        width: '120',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },

      },
      {
        field: 'attachement',
        displayName: 'Comments',
        format: '',
        width: '100',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },

      },
      {
        field: 'delete',
        displayName: 'Delete',
        width: '70',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
      },

    ];

    this.DesktopTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,
      footerColumns: this.libServ.deepCopy(this.dimColDef),
      columns: this.libServ.deepCopy(this.dimColDef),
      selectedColumns: this.libServ.deepCopy(this.dimColDef),
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };

    this.mobileTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,
      footerColumns: this.libServ.deepCopy(this.dimColDef),
      columns: this.libServ.deepCopy(this.dimColDef),
      selectedColumns: this.libServ.deepCopy(this.dimColDef),
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };

    this.tabletTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,
      footerColumns: this.libServ.deepCopy(this.dimColDef),
      columns: this.libServ.deepCopy(this.dimColDef),
      selectedColumns: this.libServ.deepCopy(this.dimColDef),
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };






    this.loadDesktopTableData('tablereq');
    this.loadMobileTableData('tablereq');
    this.loadTabletTableData('tablereq');
    this.getAuth();
    this.getTargeting();
  
  }


  getAuth() {
    let request={
      selectedConnector:this.selectedConnector
    }
    this.dataFetchServ
      .getAuth(request)
      .subscribe(data => {

        // console.log('data', data);
        this.gamOrderData = data;
        this.availableAdvertisers = data['advertiser'];
        this.availableGamLables = data['labels']
        this.availableGamAgencies = data['agency'].filter(item => item['type']['value'] === 'AGENCY')
        this.availableGamSecTrafficker = data['users']
        this.availableGamSalesperson = data['users']
        this.availableGamSecSalesperson = data['users']
        this.currency = data['currency']

        if (this.currency == 'INR')
          this.currencySymbol = "&#8377;";

          if(this.operationIdentity=='editOrder'){
            this.setOrderDetails(this.formData);
          }
          if(this.operationIdentity=='editLineItem'){
            this.setLineItemDetails(this.formData);
          }
          if(this.operationIdentity=='editCreative'){
            this.setCreativeDetails(this.formData);
          }
      });
  }

  getTargeting() {

  }

  onOpenGamTargetingOS(event) {

    // console.log("event: ", event);
    let request = {};

    switch (event.index) {

      case 1:
        request = {
          from: 'Ad_Unit',
          select: 'Id,Name',
          orderBy: 'Name ASC',
          selectedConnector:this.selectedConnector
        }

        this.dataFetchServ
          .getTargeting(request)
          .subscribe(data => {
            // console.log('data', data);

            let tempData = [];
            data['data']['rows'].forEach(item => {

              this.availableGamAdUnits.push({
                'name': item.values[1].value,
                'id': item.values[0].value,
                'checked': false
              })
            });

            if(this.editLineItemFlag){
              this.selectedGamAdUnits = this.retrievedTaragetingData.selectedGamAdUnits
            }

            let selectedId = this.selectedGamAdUnits['selectedData'].map(item => item['id']);
            if (selectedId.length != 0) {
              selectedId.forEach(element => {
                this.availableGamAdUnits.forEach(nested => {
                  if (nested['id'] == element) {
                    nested.checked = true;
                  }
                });
              })
            }
          });

           request={
            selectedConnector:this.selectedConnector
          }
        this.dataFetchServ
          .getTargetingPlacement(request)
          .subscribe(data => {
            data['data'].forEach(item => {
              // console.log('data  item :', item);
              this.availableGamPlacement.push({
                'name': item['name'],
                'id': item['id'],
                'checked': false
              })
            });

            if(this.editLineItemFlag){
              this.selectGamPlacement = this.retrievedTaragetingData.selectGamPlacement
            }

            let selectedId = this.selectGamPlacement['selectedData'].map(item => item['id']);
            if (selectedId.length != 0) {
              selectedId.forEach(element => {
                this.availableGamPlacement.forEach(nested => {
                  if (nested['id'] == element) {
                    nested.checked = true;
                  }
                });
              })
            }


          });
        break;

      case 2:
          request={
            selectedConnector:this.selectedConnector
          }

        this.dataFetchServ
          .getCustomTargeting(request)
          .subscribe(data => {
            // console.log("data   :", data)
            let json = JSON.parse(data['keyValue']);
            let sampleValueList = []
            json['res'].forEach(element => {
              this.keyValuePair.push({ 'value': element.id, 'label': element.value })

              let data1 = [];
              data['customTargets'].forEach(listElement => {
                if (listElement.customTargetingKeyId == element.id) {
                  data1.push({ 'value': listElement.id, 'label': listElement.name, 'customTargetingKeyId': listElement.customTargetingKeyId })
                }
              });
              sampleValueList.push({ 'id': element.id, 'sampleList': data1 });

              if (this.selectedCustomTargeting.length > 0) {

                this.customTargeting.controls.customTargetingArray.patchValue(this.selectedCustomTargeting);
                // console.log("this.customTargeting   :", this.customTargeting.value)
              }
            });
            // console.log("SampleList     :", sampleValueList)
            this.sampleValueList = sampleValueList;
            this.targetFlag = true;
          });

        break;
      case 3:
        let type = "'Country'"
        request = {
          from: 'Geo_Target',
          select: 'Id,Name,ParentIds',
          orderBy: 'Name ASC',
          where: 'type=' + type,
          selectedConnector:this.selectedConnector
        }
        
       
        // ParentIds,CountryCode,Type,Targetable'
        this.dataFetchServ
          .getTargetingWithWhereClause(request)
          .subscribe(data => {
            // console.log('data', data);

            let dynamicData = [];
            dynamicData.push({
              'checked': false,
              'hideShow': false
            })
            data['data']['rows'].forEach(item => {

              this.availableGamGeograhy.push({
                'name': item.values[1].value,
                'id': item.values[0].value,
                'parentIds': item.values[2].value,
                'checked': false,
                'hideShow': false,
                'dynamic': dynamicData
              })
            });
           
            if(this.editLineItemFlag){
              this.selectGamPlacement = this.retrievedTaragetingData.selectGamGeograhy
            }
            let selectedId = this.selectGamGeograhy['selectedData'].map(item => item['id']);
            if (selectedId.length != 0) {
              selectedId.forEach(element => {
                this.availableGamGeograhy.forEach(nested => {
                  if (nested['id'] == element) {
                    nested.checked = true;
                  }
                });
              })
            }
          });
        break;

      case 5:
        request = {
          from: 'Device_Category',
          select: 'Id,DeviceCategoryName',
          orderBy: 'DeviceCategoryName ASC',
          selectedConnector:this.selectedConnector
        }
        this.dataFetchServ
          .getTargeting(request)
          .subscribe(data => {
            // console.log('data', data);

            let tempData = [];
            data['data']['rows'].forEach(item => {

              this.availableGamDeviceCategory.push({
                'name': item.values[1].value,
                'id': item.values[0].value,
                'checked': false
              })
            });
            if(this.editLineItemFlag){
              this.selectGamPlacement = this.retrievedTaragetingData.selectAvailableGamDeviceCategory
            }

            let selectedId = this.selectAvailableGamDeviceCategory['selectedData'].map(item => item['id']);
            if (selectedId.length != 0) {
              selectedId.forEach(element => {
                this.availableGamDeviceCategory.forEach(nested => {
                  if (nested['id'] == element) {
                    nested.checked = true;
                  }
                });
              })
            }


          });
        break;

      case 6:
        request = {
          from: 'Browser',
          select: 'Id,BrowserName,MajorVersion,MinorVersion',
          orderBy: 'BrowserName ASC',
          selectedConnector:this.selectedConnector
        }

        this.dataFetchServ
          .getTargeting(request)
          .subscribe(data => {
            // console.log('data', data);

            let tempData = [];
            data['data']['rows'].forEach(item => {

              this.availableGamTargetingOS.push({
                'name': item.values[1].value + ' ( ' + item.values[2].value + ' )',
                'id': item.values[0].value,
                'checked': false
              })
            });

            if(this.editLineItemFlag){
              this.selectGamPlacement = this.retrievedTaragetingData.selectavailableGamTargetingOS
            }
            let selectedId = this.selectavailableGamTargetingOS['selectedData'].map(item => item['id']);
            if (selectedId.length != 0) {
              selectedId.forEach(element => {
                this.availableGamTargetingOS.forEach(nested => {
                  if (nested['id'] == element) {
                    nested.checked = true;
                  }
                });
              })
            }


          });
        break;


      case 7:
        request = {
          from: 'Operating_System',
          select: 'Id,OperatingSystemName',
          orderBy: 'OperatingSystemName ASC',
          selectedConnector:this.selectedConnector
        }

        this.dataFetchServ
          .getTargeting(request)
          .subscribe(data => {
            // console.log('data    OperatingSystemName:', data);

            let dynamicData = [];
            dynamicData.push({
              'checked': false,
              'hideShow': false
            })
            data['data']['rows'].forEach(item => {

              this.availableTargetingOS.push({
                'name': item.values[1].value,
                'id': item.values[0].value,
                'checked': false,
                'hideShow': false,
                'dynamic': dynamicData
              })
            });
            if(this.editLineItemFlag){
              this.selectGamPlacement = this.retrievedTaragetingData.selectGamTargetingOS
            }
            let selectedId = this.selectGamTargetingOS['selectedData'].map(item => item['id']);
            if (selectedId.length != 0) {
              selectedId.forEach(element => {
                this.availableTargetingOS.forEach(nested => {
                  if (nested['id'] == element) {
                    nested.checked = true;
                  }
                });
              })
            }

          });
        break;

      case 8:
        request = {
          from: 'Browser_Language',
          select: 'Id,BrowserLanguageName',
          orderBy: 'BrowserLanguageName ASC',
          selectedConnector:this.selectedConnector
        }

        this.dataFetchServ
          .getTargeting(request)
          .subscribe(data => {
            // console.log('data', data);

            let tempData = [];
            data['data']['rows'].forEach(item => {

              this.availableGamBrowswerLang.push({
                'name': item.values[1].value,
                'id': item.values[0].value,
                'checked': false
              })
            });
            if(this.editLineItemFlag){
              this.selectGamPlacement = this.retrievedTaragetingData.selectGamBrowswerLang
            }
            let selectedId = this.selectGamBrowswerLang['selectedData'].map(item => item['id']);
            if (selectedId.length != 0) {
              selectedId.forEach(element => {
                this.availableGamBrowswerLang.forEach(nested => {
                  if (nested['id'] == element) {
                    nested.checked = true;
                  }
                });
              })
            }

          });
        break;

      case 9:
        request = {
          from: 'Device_Capability',
          select: 'Id,DeviceCapabilityName	',
          orderBy: 'DeviceCapabilityName	 ASC',
          selectedConnector:this.selectedConnector
        }

        this.dataFetchServ
          .getTargeting(request)
          .subscribe(data => {
            // console.log('data', data);

            let tempData = [];
            data['data']['rows'].forEach(item => {

              this.availableGamDeviceCapability.push({
                'name': item.values[1].value,
                'id': item.values[0].value,
                'checked': false
              })
            });
            if(this.editLineItemFlag){
              this.selectGamPlacement = this.retrievedTaragetingData.selectGamDeviceCapability
            }
            let selectedId = this.selectGamDeviceCapability['selectedData'].map(item => item['id']);
            if (selectedId.length != 0) {
              selectedId.forEach(element => {
                this.availableGamDeviceCapability.forEach(nested => {
                  if (nested['id'] == element) {
                    nested.checked = true;
                  }
                });
              })
            }

          });
        break;

      case 10:
        request = {
          from: 'Device_Manufacturer',
          select: 'Id,MobileDeviceManufacturerName	',
          orderBy: 'MobileDeviceManufacturerName ASC',
          selectedConnector:this.selectedConnector
        }

        this.dataFetchServ
          .getTargeting(request)
          .subscribe(data => {
            // console.log('data', data);

            let dynamicData = [];
            dynamicData.push({
              'checked': false,
              'hideShow': false
            })
            data['data']['rows'].forEach(item => {

              this.availableGamDeviceManufaturere.push({
                'name': item.values[1].value,
                'id': item.values[0].value,
                'checked': false,
                'hideShow': false,
                'dynamic': dynamicData
              })
            });
            if(this.editLineItemFlag){
              this.selectGamPlacement = this.retrievedTaragetingData.selectGAMDeviceManufacturer
            }
            let selectedId = this.selectGAMDeviceManufacturer['selectedData'].map(item => item['id']);
            if (selectedId.length != 0) {
              selectedId.forEach(element => {
                this.availableGamDeviceManufaturere.forEach(nested => {
                  if (nested['id'] == element) {
                    nested.checked = true;
                  }
                });
              })
            }

          });
        break;

      case 11:
        request = {
          from: 'Bandwidth_Group',
          select: 'Id,BandwidthName',
          orderBy: 'BandwidthName ASC',
          selectedConnector:this.selectedConnector
        }

        this.dataFetchServ
          .getTargeting(request)
          .subscribe(data => {
            // console.log('data', data);

            let tempData = [];
            data['data']['rows'].forEach(item => {

              this.availableGamDeviceBandwidth.push({
                'name': item.values[1].value,
                'id': item.values[0].value,
                'checked': false
              })
            });
            if(this.editLineItemFlag){
              this.selectGamPlacement = this.retrievedTaragetingData.selectGamDeviceBandwidth
            }

            let selectedId = this.selectGamDeviceBandwidth['selectedData'].map(item => item['id']);
            if (selectedId.length != 0) {
              selectedId.forEach(element => {
                this.availableGamDeviceBandwidth.forEach(nested => {
                  if (nested['id'] == element) {
                    nested.checked = true;
                  }
                });
              })
            }

          });
        break;

      case 12:
        request = {
          from: 'Mobile_Carrier',
          select: 'Id,MobileCarrierName,CountryCode',
          orderBy: 'MobileCarrierName ASC',
          selectedConnector:this.selectedConnector
        }

        this.dataFetchServ
          .getTargeting(request)
          .subscribe(data => {
            // console.log('data', data);

            let dynamicData = [];
            dynamicData.push({
              'checked': false,
              'hideShow': false
            })
            data['data']['rows'].forEach(item => {

              this.availableGammobileCarrier.push({
                'name': item.values[1].value + ' ' + item.values[2].value,
                'id': item.values[0].value,
                'checked': false,
                'hideShow': false,
                'dynamic': dynamicData
              })
            });
            if(this.editLineItemFlag){
              this.selectGamPlacement = this.retrievedTaragetingData.selectGamMobileCarrier
            }
            let selectedId = this.selectGamMobileCarrier['selectedData'].map(item => item['id']);
            if (selectedId.length != 0) {
              selectedId.forEach(element => {
                this.availableGammobileCarrier.forEach(nested => {
                  if (nested['id'] == element) {
                    nested.checked = true;
                  }
                });
              })
            }
          });
        break;

      case 14:
        request = {
          from: 'Mobile_Carrier',
          select: 'Id,MobileCarrierName',
          orderBy: 'MobileCarrierName ASC',
          selectedConnector:this.selectedConnector
        }

        this.dataFetchServ
          .getTargeting(request)
          .subscribe(data => {
            // console.log('data', data);

            let tempData = [];
            data['data']['rows'].forEach(item => {

              this.availableGammobileCarrier.push({
                'name': item.values[1].value,
                'id': item.values[0].value,
              })
            });
            if(this.editLineItemFlag){
              this.selectGamPlacement = this.retrievedTaragetingData.selectGamMobileCarrier
            }
            let selectedId = this.selectGamMobileCarrier['selectedData'].map(item => item['id']);
            if (selectedId.length != 0) {
              selectedId.forEach(element => {
                this.availableGammobileCarrier.forEach(nested => {
                  if (nested['id'] == element) {
                    nested.checked = true;
                  }
                });
              })
            }

          });
        break;


      default:
        break;
    }
  }




  headlineExpandCollapse: boolean = true
  bodyExpandCollapse: boolean = true
  call_to_actionExpandCollapse: boolean = true
  star_ratingExpandCollapse: boolean = true
  priceExpandCollapse: boolean = true
  advertiserExpandCollapse: boolean = true
  storeExpandCollapse: boolean = true

  onChangeLineItem() {
    // console.log(this.selectedLineItem);
    this.selectedQtyValues = {};
    this.addGamLineItemForm.controls['standardDiscount'].setValue(0);
    this.addGamLineItemForm.controls['discount'].setValue(0);
    this.final_total_value = 0;
  }

  onChangeLineItemPriority(lineItemTypePriority) {

    let value = (lineItemTypePriority['value']['value'] == 'high') ? 6 : (lineItemTypePriority['value']['value'] == 'normal') ? 8 : 10
    this.lineItem.lineItemTypePriorityValue.patchValue(value);
  }

  onChangeAdvertisers() {

    this.availableGamAdContacts = this.gamOrderData['contact'].filter(item => item['companyId'] === this.selectedGamAdvertisers['id'])

  }
  addGamExpectedCreative() {
    this.expectedCreativeArr.push("abc");
  }

  deleteGamExpectedCreative(index) {
    this.expectedCreativeArr.splice(index, 1);
  }

  addGamFrequencyCap() {

    let tempGamWeekDayArr = this.addGamLineItemForm.controls.frequencyCapArray as FormArray;
    let newFrequencyCapArr: FormGroup = this.formBuilder.group({
      impressionPer: ['1'],
      impressionPer1: ['1'],
      selectedFreqCap: [{ id: 'day', name: 'days' }],
    })
    tempGamWeekDayArr.insert(tempGamWeekDayArr.length, newFrequencyCapArr);

  }
  removeGamFrequencyCap(index) {
    //   this.gamFrequencyCapArr.splice(index,1);
    let temGamFrequencyCapArray = this.addGamLineItemForm.controls.frequencyCapArray as FormArray;
    temGamFrequencyCapArray.removeAt(index);
  }
  onCustomTargetingAdded() {
    this.customTargetingArr.push('abc');
  }

  addGamWeekDays() {

    let tempGamWeekDayArr = this.addGamLineItemForm.controls.timeZomeArray as FormArray;
    let newTimeZoneArr: FormGroup = this.formBuilder.group({
      timeZoneStartTime: { id: "00:00", name: "12:00 AM" },
      timeZoneEndTime: { id: "00:00", name: "12:00 AM" },
      timeZoneWeeksDays: [''],
      timeZoneWeeksDaysExclude: [''],
      daysArr: this.formBuilder.array([
        new FormControl(false),
        new FormControl(false),
        new FormControl(false),
        new FormControl(false),
        new FormControl(false),
        new FormControl(false),
        new FormControl(false),


      ])
    })

    tempGamWeekDayArr.insert(tempGamWeekDayArr.length, newTimeZoneArr);

    // console.log(this.addGamLineItemForm);

  }
  removeGamWeekDays(index) {
    //   this.gamWeekDaysArr.splice(index,1);
    let tempGamWeekDayArr = this.addGamLineItemForm.controls.timeZomeArray as FormArray;
    tempGamWeekDayArr.removeAt(index);
  }

  onChangeSev() {
    // console.log("selected ser", this.selectedSeverities);
  }

  saveGamOrders(orderName, gamAdvertiser, gamTrafficker) {
    this.isGamTrafficker
    this.isGamSalesperson
    this.isprimarySalesPersonPresent
    if (orderName.control.status != 'VALID' || gamAdvertiser.control.status != 'VALID' ||
      gamTrafficker.control.status != 'VALID' || this.isGamTrafficker != undefined || this.isGamSalesperson != undefined) {
      return false;
    }

    let agency = (this.selectedGamAgencies != undefined) ? this.selectedGamAgencies.id : "";
    let salesperson = (this.selectedGamSalesperson != undefined) ? this.selectedGamSalesperson.id : "";
    let orderDetails = {
      orderName: this.gamOrderName,
      advertiserId: this.selectedGamAdvertisers.id,
      traffickerId: this.selectedGamOrderTrafficker.id,
      labels: this.selectedLabels.map(item => item['id']),
      contact: this.selectedGamContacts.map(item => item['id']),
      agency: agency,
      secTrafficker: this.selectedGamSecTrafficker.map(item => item['id']),
      salesperson: salesperson,
      secSalesperson: this.selectedGamSecSalesPerson.map(item => item['id']),
      poNumber: this.gamOrderPoNumber,
      note: this.gamOrderNote,
      customFiels: this.gamOrderCustomFields
    }

    const line_item_json = {}
    // console.log("selectedGamAdvertisers   :", this.selectedGamAdvertisers)
    let request = {
      id: "",
      campaign_json: JSON.stringify(orderDetails),
      ad_server_id: 1,
      name: this.gamOrderName,
      server_id: "",
      line_item_json: JSON.stringify(line_item_json),
      approval_status: "Pending for approval",
      status: 'Active',
      created_by: this.currentUser,
      approved_by: 1,
      advertiser_id: this.selectedGamAdvertisers.id,
      advertiser_name: this.selectedGamAdvertisers.name
    }

    // console.log("request   :", request)
    this.dataFetchServ
      .saveGamOrders(request)
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['data']);
          return;
        }
        if (data['status'] === -1) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Error',
            detail: 'Order name already exist.'
          });
          console.log(data['data']);
          return;
        }
        if (data['status'] == 1) {
          this.toastService.displayToast({
            severity: 'success',
            summary: 'Campaign Management',
            detail: 'Order created successfully'
          });

          this.gamCampaignId = data['id']
          this.showLineItem = true;
        }
        //  console.log("status",data);


      });

  }

  saveLineItems() {
    // console.log("savelineItem   :",JSON.stringify(this.addGamLineItemForm.value));

    if (this.addGamLineItemForm.status == 'VALID') {
      console.log("Validd")
    } else {
      console.log("InnnnValidd")
      // return;
    }

    if (this.selectGamPlacement.selectedData.length == 0 || this.selectedGamAdUnits.selectedData.length == 0) {
      // return;
    }

    this.addGamLineItemForm.controls.targeting_json.patchValue(this.targetingJsonSave())
    let request = {
      campaign_id: this.gamCampaignId,
      line_item_json: JSON.stringify(this.addGamLineItemForm.value),
      name: this.addGamLineItemForm.controls['lineItemName'].value,
      status: 'Inactive',
      created_by: this.currentUser,
    }

    this.dataFetchServ
      .saveGamLineItems(request)
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          // console.log(data['data']);
          return;
        }
        if (data['status'] == 1) {
          this.toastService.displayToast({
            severity: 'success',
            summary: 'Campaign Management',
            detail: 'Line Item created successfully'
          });

          this.gamLineItemId = data['id']
          this.showCreatives = true;
          this.scrollToCreative();

          this.lineItem.expectedCreatives.value.forEach(element => {
            this.selectedAdUnitSize.push({ 'label': element['width'] + 'X' + element['height'], 'value': element })
          })
          //  console.log("this.selectedAdUnitSize    :",this.selectedAdUnitSize)
        }
      });
  }

  get c() {
    return this.addCompaignForm.controls;
  }

  get p() {
    return this.addPlacementForm.controls;
  }
  onAddChips(e) { }

  addCreative() {
    this.addCreativeDetails = true;
    this.tableData1.push({});
    if (this.tabIndex === 0) {
      this.loadDesktopTableData('tableReq');
    } else if (this.tabIndex === 1) {
      this.loadMobileTableData('tableReq');
    } else {
      this.loadTabletTableData('tableReq')
    }
  }

  addTagCode() {
    const ref = this.dialogService.open(TagCodePopupComponent, {
      header: 'Tag Code ',
      contentStyle: { width: '80vw', overflow: 'auto' },
      data: ''
    });
    ref.onClose.subscribe((data: string) => { });
  }

  loadDesktopTableData(tableReq) {
    setTimeout(() => {
      this.DesktopTableJson['loading'] = true;
    });
    // const tableData = data['data'];

    this.tableData = [{ lid: 1, tag_name: "abc", tag_type: "abc", start_date: 2019, tag_code: "All", additional_pixel: 200000 }];
    let data = [];
    data = this.tableData.concat(this.tableData1);
    // console.log("data---", data);
    // let newArray = 
    const arr = [];
    data.forEach((row: object) => {
      arr.push({
        data: row,
        children: [{ data: {} }]
      });
    });
    this.DesktopTableData = <TreeNode[]>arr;
    this.DesktopTableJson['totalRecords'] = data.length;
    setTimeout(() => {
      this.DesktopTableJson['loading'] = false;
    });
    this.DesktopTableJson['lazy'] = true;
  }

  onLazyLoadAggTable() {

  }

  loadMobileTableData(tableReq) {
    setTimeout(() => {
      this.mobileTableJson['loading'] = true;
    });

    this.tableData = [{ lid: 1, tag_name: "abc", tag_type: "abc", start_date: 2019, tag_code: "All", additional_pixel: 200000 }];
    let data = [];


    data = this.tableData.concat(this.tableData1);

    const arr = [];
    data.forEach((row: object) => {
      arr.push({
        data: row,
        children: [{ data: {} }]
      });
    });
    this.mobileTableData = <TreeNode[]>arr;
    this.mobileTableJson['totalRecords'] = data.length;
    setTimeout(() => {
      this.mobileTableJson['loading'] = false;
    });
    this.mobileTableJson['lazy'] = true;
  }

  loadTabletTableData(tableReq) {
    setTimeout(() => {
      this.tabletTableJson['loading'] = true;
    });
    // const tableData = data['data'];

    this.tableData = [{ lid: 1, tag_name: "abc", tag_type: "abc", start_date: 2019, tag_code: "All", additional_pixel: 200000 }];
    let data = [];


    data = this.tableData.concat(this.tableData1);

    // console.log("data---", data);
    // let newArray = 
    const arr = [];
    data.forEach((row: object) => {
      arr.push({
        data: row,
        children: [{ data: {} }]
      });
    });
    this.tabletTableData = <TreeNode[]>arr;
    this.tabletTableJson['totalRecords'] = data.length;
    setTimeout(() => {
      this.tabletTableJson['loading'] = false;
    });
    this.tabletTableJson['lazy'] = true;
  }

  goBack(identity) {
    if (this.formIdentity == 'lineItem' || this.formIdentity == 'creative') {
      this.triggeredEvent.emit(this.formIdentity);
      return;
    }
    if (identity == 'editOrder') {
      this.triggeredEvent.emit('order');
      return;
    }
    this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?',
      accept: () => {
        this.router.navigate(['/campaign-delivery-dashboard'], {
          relativeTo: this.currentActivatedRoute
        });
      },
      reject: () => {
      }
    });
  }

  tabChanged(e) {
    this.tableData1 = [];
    this.tabIndex = e;
    console.log('e...index', e);
  }

  overlay: boolean
  initializationAddCreativeFormControls() {
    return this.formBuilder.group({
      name: new FormControl(''),
      advertiserId: ['4673378073'],
      videoContent: [],
      target_ad_unit_size: new FormControl(''),
      ad_manager_hosted: new FormControl('ad_manager_hosted'),
      upload_as_mezzanine: new FormControl(''),
      externally_hosted_asset: new FormControl(''),
      duration: new FormControl(''),
      skippable_video: new FormControl(''),
      scaling: new FormControl(''),
      destination: new FormControl(''),
      url: new FormControl(''),
      internal_redirect: new FormControl(''),
      ssl_compatible: new FormControl(''),
      alt_text: new FormControl(''),
      redirect_type: new FormControl(''),
      vast_tag_url: new FormControl(''),
      type: new FormControl(''),
      density1: new FormControl(''),
      density2: new FormControl(''),
      density3: new FormControl(''),
      asset_size: new FormControl(''),
      orientation: new FormControl(''),
      ssl_compatibleSwitch: new FormControl(''),
      label: new FormControl(''),
      custom_parameter: new FormControl(''),
      creative_view: new FormControl(''),
      third_party_url: new FormControl(''),
      third_party_trackingArray: this.formBuilder.array([this.initializationOfthird_party_tracking()]),
      userDefinedVariableArray: this.formBuilder.array([this.initializationUserDefinedVariable()]),
      userDefinedVariableArray2: this.formBuilder.array([this.initializationUserDefinedVariable2()]),
      custom_field: new FormControl(''),
      unlink_from_creative_template: new FormControl(''),
      custom_setting: new FormControl(''),
      formControlNameSwtch: new FormControl(''),
      costType_standard_amp: new FormControl('Standard'),
      standard_macro: new FormControl(''),
      amp_macro: new FormControl(''),
      serve_into_safeframe: new FormControl('true'),
      template_name: new FormControl(''),
      template_description: new FormControl(''),
      creative_template_snippet: new FormControl(''),
      headline: new FormControl(''),
      body: new FormControl(''),
      call_to_action: new FormControl(''),
      star_rating: new FormControl(''),
      price: new FormControl(''),
      store: new FormControl(''),
      deep_link_click_action_url: new FormControl(''),
      third_party_impression_tracker: new FormControl(''),
      third_party_click_tracker: new FormControl(''),
      advertiser: new FormControl(''),
      template_type: new FormControl(''),
      height: new FormControl(''),
      width: new FormControl(''),
      iconName: new FormControl(''),
      application_download_uri: new FormControl(''),
      description_line: new FormControl(''),
      description_line2: new FormControl(''),
      buttonText: new FormControl(''),
      targetWindow: new FormControl(''),
      imageBorder: new FormControl(''),
      safecount_survey_url: new FormControl(''),
      third_party_impression_tracker2: new FormControl(''),
      third_party_impression_tracker1: new FormControl(''),
      url2: new FormControl(''),
      autoDisappear: new FormControl(''),
      annotation: new FormControl(''),
      buttonPosition: new FormControl(''),
      button_Size: new FormControl(''),
      displayTime: new FormControl(''),
      allowScaling: new FormControl(''),
      allowStretching: new FormControl(''),
      popType: new FormControl(''),
      displayMenuBar: new FormControl(''),
      displayStatusBar: new FormControl(''),
      displayAddressBar: new FormControl(''),
      displayToolbar: new FormControl(''),
      displayScrollBar: new FormControl(''),
      allow_window_to_resize: new FormControl(''),
      displayDirectoryButtons: new FormControl(''),
      displayFullScreenMode: new FormControl(''),
      hoverText: new FormControl(''),
      linkColor: new FormControl(''),
      linktitle: new FormControl(''),
      descriptionTextAd: new FormControl(''),
      textColor: new FormControl(''),
      backgroundColor: new FormControl(''),
      creativeWidth: new FormControl(''),
      creativeHeight: new FormControl(''),
      image: '',
      selectedFileName: new FormControl(''),
      invalidFileType: false,
      assetRequired: false,
      selectedNativeHeadlineFileName: '',
      InvalidNativeHeadlineFileType: false,
      selectedNativeIconFileName: '',
      InvalidNativeBodyFileType: false,
      submittedSuccessfully: false,
      selectedFileName1: '',
      invalidFileType1: false,
      selectedFileName2: '',
      invalidFileType2: false,
      selectedFileName3: '',
      invalidFileType3: false,
      selectedFileName4: '',
      invalidFileType4: false,
      selectedFileName5: '',
      invalidFileType5: false,
      // name: new FormControl(''), 
    })
  }

  textColorExpandCollapse: boolean = true
  descriptionTextAdExpandCollapse: boolean = true
  linktitleExpandCollapse: boolean = true
  linkColorExpandCollapse: boolean = true
  hoverTextExpandCollapse: boolean = true
  description_lineExpandCollapse: boolean = true
  buttonTextExpandCollapse: boolean = true
  targetWindowExpandCollapse: boolean = true
  description_line2ExpandCollapse: boolean = true
  application_download_uriExpandCollapse: boolean = true
  initializationOfthird_party_tracking() {
    return this.formBuilder.group({
      creative_view: new FormControl(''),
      third_party_url: new FormControl(''),
    })
  }

  initializationUserDefinedVariable() {
    return this.formBuilder.group({
      image: new FormControl(''),
      transitionName: new FormControl(''),
      transitionDirection: new FormControl(''),
      transitionAnchor: new FormControl(''),
      transitionDuration: new FormControl(''),
      transitionEasing: new FormControl(''),
      transitionDelay: new FormControl(''),
    })
  }
  linkMapCoordinatesExpandCollapse = []
  initializationUserDefinedVariable2() {
    return this.formBuilder.group({
      linkMapCoordinates: new FormControl(''),
      url: new FormControl(''),
      urlTarget: new FormControl(''),
      hideShow: new FormControl('true'),
    })
  }

  getthird_party_trackingArray() {
    return this.addCreativeForm.get('third_party_trackingArray') as FormArray;
  }

  addDynamic() {
    this.getthird_party_trackingArray().push(this.initializationOfthird_party_tracking())
  }

  removeDynamic(Index) {
    if (this.getthird_party_trackingArray().length > 1) {
      this.getthird_party_trackingArray().removeAt(Index);
    }
  }

  getuserDefinedVariableArray() {
    return this.addCreativeForm.get('userDefinedVariableArray') as FormArray;
  }

  addUserDefinedDynamic() {
    this.getuserDefinedVariableArray().push(this.initializationUserDefinedVariable())
  }

  removeUserDefinedDynamic(Index) {
    if (this.getuserDefinedVariableArray().length > 1) {
      this.getuserDefinedVariableArray().removeAt(Index);
    }
  }



  getuserDefinedVariableArray2() {
    return this.addCreativeForm.get('userDefinedVariableArray2') as FormArray;
  }

  addUserDefinedDynamic2() {
    this.getuserDefinedVariableArray2().push(this.initializationUserDefinedVariable2())
  }

  removeUserDefinedDynamic2(Index) {
    if (this.getuserDefinedVariableArray2().length > 1) {
      this.getuserDefinedVariableArray2().removeAt(Index);
    }
  }


  get lineItem() {
    return this.addGamLineItemForm.controls;
  }



  get templateType() {
    return this.addCreativeForm.controls.template_type.value
  }

  tableDimColDef: any[];
  noTableData = false;
  initialLoading() {
    this.tableDimColDef = [
      {
        field: 'asset',
        displayName: 'Asset',
        format: '',
        width: '100',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: false
        },
      },
      {
        field: 'active',
        displayName: 'Active',
        format: 'string',
        width: '200',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: true,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: false
        },
      },
      {
        field: 'scalling',
        displayName: 'Scalling',
        format: 'string',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: true,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: false
        },
      },
      {
        field: 'duration',
        displayName: 'Duration',
        format: '',
        width: '120',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: true,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'bitrate',
        displayName: 'Bitrate',
        format: '',
        width: '120',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: true,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'dimensions',
        displayName: 'Dimensions',
        width: '170',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: true,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'type',
        displayName: 'Type',
        format: '',
        width: '90',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: true,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'delivery',
        displayName: 'Delivery',
        format: '',
        width: '170',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: true,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'codecs',
        displayName: 'Codecs',
        format: '',
        width: '90',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: true,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: true
        },

      },
    ];
    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,


      footerColumns: this.tableDimColDef,
      columns: this.tableDimColDef,
      selectedColumns: this.tableDimColDef,


      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: false,
      columnSearch: false
    };

  }
  initializationForDisplay() {
    this.typeList = [
      { label: 'Image', value: 'image' },
      { label: 'HTML5', value: 'html5' },
      { label: 'Campaign Manager', value: 'campaign_manager' },
      { label: 'Third Party', value: 'third_party' },
      { label: 'Custom', value: 'custom' },
      { label: 'Native format', value: 'native_format' },
      { label: 'Custom creative template', value: 'custom_creative_template' },
      { label: 'Standard creative template', value: 'standard_creative_template' },
    ];

    this.destinationList = [
      { label: 'Click-Through URL', value: 'click_through_url' },
      { label: 'Click-to-app URL', value: 'click_to_app_url' },
      { label: 'Phone Number', value: 'phone_number' }
    ]
  }

  initializationForVideo() {
    this.typeList = [
      { label: 'Video', value: 'Video' },
      { label: 'Overlay', value: 'Overlay' },
      { label: 'Redirect', value: 'Redirect' },
    ];
    this.destinationList = [
      { label: 'Click-Through URL', value: 'click_through_url' },
      { label: 'Not Clickable', value: 'not-clickable' }
    ]
  }
  targetWindowList = [
    { label: '__blank', value: '__blank' },
    { label: '__top', value: '__top' }
  ]
  buttonSizeList = [
    { label: 'Standard', value: 'standard' },
    { label: 'Small', value: 'small' },
    { label: 'Medium', value: 'medium' },
    { label: 'Tall', value: 'tall' },
  ]

  buttonPositionList = [
    { label: 'Top-left', value: 'top-left' },
    { label: 'Top-right', value: 'top-right' },
    { label: 'Bottom-left', value: 'bottom-left' },
    { label: 'Bottom-right', value: 'bottom-right' },

  ]

  annotationList = [
    { label: 'None', value: 'none' },
    { label: 'Bubble', value: 'bubble' },
    { label: 'Inline', value: 'inline' },
  ]

  yesNoList = [
    { label: 'Yes', value: 'yes' },
    { label: 'No', value: 'no' },
  ]

  popTypeList = [
    { label: 'Pop-up', value: 'Pop-up' },
    { label: 'Pop-under', value: 'Pop-under' },
  ]


  displayOrVideo(identity) {

    this.initializationForDisplay();
  }
  unitSizeList = [];
  selectedAdUnitSize = [];
  labelList = [];
  initializationOfList() {
    let request={
       selectedConnector:this.selectedConnector
    }
    this.dataFetchServ.getData(request).subscribe(response => {
      let data = response;

      data['adUnitSizes'].forEach(element => {
        if (element.environmentType.value == 'BROWSER') {
          this.unitSizeList.push({ 'label': element['fullDisplayString'], 'value': element['size'] })
        }
      });
      this.selectedAdUnitSize = this.unitSizeList;
      data['labels'].forEach(element => {
        let typeLabel; let index = 0;
        element['types'].forEach(type => {
          if (index == 0) { typeLabel = type['value'] }
          else { typeLabel = typeLabel + ',' + type['value'] }
          index++;
        })
        // console.log("typeLabel  :", typeLabel)
        this.labelList.push({ 'label': element['name'] + ' (' + typeLabel + ')', 'value': element['id'] })
      });
    })
  }


  trackingEventList = [
    { label: 'Creative view', value: 'Creative view' },
    { label: 'Start', value: 'Start' },
    { label: 'First quartile', value: 'First quartile' },
    { label: 'Midpoint', value: 'Midpoint' },
    { label: 'Third quartile', value: 'Third quartile' },
    { label: 'Complete', value: 'Complete' },
    { label: 'Click tracking', value: 'Click tracking' },
    { label: 'Custom click', value: 'Custom click' },
    { label: 'Measurable impression', value: 'Measurable impression' },
    { label: 'Viewable impression', value: 'Viewable impression' },
    { label: 'Video abandon', value: 'Video abandon' },
    { label: 'Fully viewable and audible for 50% duration', value: 'Fully viewable and audible for 50% duration' },
  ]
  scalingList = [
    { label: 'Original size', value: 'Original size' },
    { label: 'Maintain aspect ratio', value: 'Maintain aspect ratio' },
    { label: 'Stretch to fit', value: 'Stretch to fit' },
  ]

  orientationList = [
    { label: 'any', value: 'FREE_ORIENTATION' },
    { label: 'Portrait', value: 'PORTRAIT_ONLY' },
    { label: 'Landscape', value: 'LANDSCAPE_ONLY' },
  ]

  filetoUpload = [];
  selectedFile: any;
  selectedFileName: any;
  InvalidFileType = false;
  uploadFile(event, typeValue, selectedFileName, InvalidFileType) {
    selectedFileName.patchValue("Selected File :" + event.target.files[0].name);
    
    let fileType = event.target.files[0].type;
    if (fileType != 'image/jpeg') {
      InvalidFileType.patchValue(true);
    } else { InvalidFileType.patchValue(false); }
    if (typeValue == 'html5') {
      let value = (fileType != 'application/zip') ? true : false
      InvalidFileType.patchValue(value);
    }
    this.selectedFile = event.target.files[0]
  }


  NativeHeadlineFile: any
  selectedNativeHeadlineFileName: any;
  InvalidNativeHeadlineFileType = false;
  uploadNativeHeadlineFile(event, selectedNativeHeadlineFileName, InvalidNativeHeadlineFileType) {
    this.NativeHeadlineFile = event.target.files[0];
    selectedNativeHeadlineFileName.patchValue("Selected File :" + event.target.files[0].name);
    let fileType = event.target.files[0].type;
    let value = (fileType != 'image/jpeg') ? true : false
    InvalidNativeHeadlineFileType.patchValue(value);
  }
  NativeAppIconFile: any;
  selectedNativeIconFileName: any
  InvalidNativeBodyFileType = false;
  uploadNativeAppIconFile(event, selectedNativeIconFileName, InvalidNativeBodyFileType) {
    this.NativeAppIconFile = event.target.files[0];
    selectedNativeIconFileName.patchValue("Selected File :" + event.target.files[0].name);
    let fileType = event.target.files[0].type;
    let value = (fileType != 'image/jpeg') ? true : false
    InvalidNativeBodyFileType.patchValue(value);
  }

  NativeVideoFile: any;
  uploadNativeVideoFile(event, selectedFileName) {
    this.NativeVideoFile = event.target.files[0]
    selectedFileName.patchValue("Selected File :" + event.target.files[0].name)
  }

  imageFile1: any;
  imageFile2: any;
  imageFile3: any;
  imageFile4: any;
  imageFile5: any;
  selectedFileName1: any
  InvalidFileType1 = false;
  selectedFileName2: any
  InvalidFileType2 = false;
  selectedFileName3: any
  InvalidFileType3 = false;
  selectedFileName4: any
  InvalidFileType4 = false;
  selectedFileName5: any
  InvalidFileType5 = false;
  uploadImageFile(event, identity, selectedFileName, InvalidFileType) {
    let fileType = event.target.files[0].type;
    if (identity == 'image1') {
      this.imageFile1 = event.target.files[0];
    }
    selectedFileName.patchValue("Selected File :" + event.target.files[0].name);
    let value = (fileType != 'image/jpeg') ? true : false;
    selectedFileName.patchValue(value);
    if (identity == 'image2') {
      this.imageFile2 = event.target.files[0];

    }
    if (identity == 'image3') {
      this.imageFile3 = event.target.files[0];

    }
    if (identity == 'image4') {
      this.imageFile4 = event.target.files[0];

    }
    if (identity == 'image5') {
      this.imageFile5 = event.target.files[0];

    }
  }

  portraitImage: any
  portraitImageHD: any
  landscapeImage: any
  landscapeImageHD: any
  uploadImageFileHD(event, identity, selectedFileName, InvalidFileType) {
    let fileType = event.target.files[0].type;
    if (identity == 'portraitImage') {
      this.portraitImage = event.target.files[0];
    }
    selectedFileName.patchValue("Selected File :" + event.target.files[0].name);
    let value = (fileType != 'image/jpeg') ? true : false
    InvalidFileType.patchValue(value);
    if (identity == 'portraitImageHD') {
      this.portraitImageHD = event.target.files[0];

    }
    if (identity == 'landscapeImage') {
      this.landscapeImage = event.target.files[0];

    }
    if (identity == 'landscapeImageHD') {
      this.landscapeImageHD = event.target.files[0];

    }
  }
  imageFile6: any;
  imageFile7: any;
  imageFile8: any;
  imageFile9: any;
  imageFile10: any;
  uploadAnimatedFile(event, index) {
    // console.log("index   :"+index)
    if (index == 0) {
      this.imageFile1 = event.target.files[0]
    }
    if (index == 1) {
      this.imageFile2 = event.target.files[0]
    }
    if (index == 2) {
      this.imageFile3 = event.target.files[0]
    }
    if (index == 3) {
      this.imageFile4 = event.target.files[0]
    }
    if (index == 4) {
      this.imageFile5 = event.target.files[0]
    }
    if (index == 5) {
      this.imageFile6 = event.target.files[0]
    }
    if (index == 6) {
      this.imageFile7 = event.target.files[0]
    }
    if (index == 7) {
      this.imageFile8 = event.target.files[0]
    }
    if (index == 8) {
      this.imageFile9 = event.target.files[0]
    }
    if (index == 9) {
      this.imageFile10 = event.target.files[0]
    }
  }



  private markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach(control => {
      control.markAsTouched();

      if (control.controls) {
        this.markFormGroupTouched(control);
      }
    });
  }

  assetRequired = false;
  creative: any
  typeValue: any;
  createCreative(addCreativeForm) {

    let templateType = addCreativeForm.controls.template_type.value;
    this.creative = addCreativeForm.controls;
    this.typeValue = addCreativeForm.controls.type.value;

    let request;
    const frmData = new FormData();

    frmData.append("creativeFile", this.selectedFile);
    frmData.append("nativeHeadlineFile", this.NativeHeadlineFile);
    frmData.append("nativeAppIconFile", this.NativeAppIconFile);
    frmData.append("nativeVideoFile", this.NativeVideoFile);

    if (templateType == 'Mobile image carousel(app and web)') {
      frmData.append("imageFile1", this.imageFile1);
      frmData.append("imageFile2", this.imageFile2);
      frmData.append("imageFile3", this.imageFile3);
      frmData.append("imageFile4", this.imageFile4);
      frmData.append("imageFile5", this.imageFile5);
      let flag1 = (this.imageFile1 == undefined) ? true : false;
      let flag2 = (this.imageFile2 == undefined) ? true : false;
      let flag3 = (this.imageFile3 == undefined) ? true : false;
      let flag4 = (this.imageFile4 == undefined) ? true : false;
      let flag5 = (this.imageFile5 == undefined) ? true : false;
      let value = (flag1 == true || flag2 == true || flag3 == true || flag4 == true || flag5 == true) ? true : false;
      addCreativeForm.controls.assetRequired.patchValue(value);
    }
    if (templateType == 'Mobile interstitial with auto close (app)') {
      frmData.append("portraitImage", this.portraitImage);
      frmData.append("portraitImageHD", this.portraitImageHD);
      frmData.append("landscapeImage", this.landscapeImage);
      frmData.append("landscapeImageHD", this.landscapeImageHD);
      let flag1 = (this.portraitImage == undefined) ? true : false;
      let flag2 = (this.landscapeImage == undefined) ? true : false;
      let value = (flag1 == true || flag2 == true) ? true : false;
      addCreativeForm.controls.assetRequired.patchValue(value);
    }
    if (templateType == 'HTML5 animated ad') {
      frmData.append("imageFile1", this.imageFile1);
      frmData.append("imageFile2", this.imageFile2);
      frmData.append("imageFile3", this.imageFile3);
      frmData.append("imageFile4", this.imageFile4);
      frmData.append("imageFile5", this.imageFile5);
      frmData.append("imageFile6", this.imageFile6);
      frmData.append("imageFile7", this.imageFile7);
      frmData.append("imageFile8", this.imageFile8);
      frmData.append("imageFile9", this.imageFile9);
      frmData.append("imageFile10", this.imageFile10);
    }

    // console.log("this.selectedFile   :", this.selectedFile)
    addCreativeForm.controls.videoContent.patchValue(frmData);

    if (addCreativeForm.status == 'INVALID') {
      this.markFormGroupTouched(addCreativeForm);
      if (this.typeValue != 'campaign_manager') {
        let value = (this.selectedFile == undefined) ? true : false;
        addCreativeForm.controls.assetRequired.patchValue(value);
      }
      if (this.typeValue == 'native_format') {
        let flag1 = (this.NativeHeadlineFile == undefined) ? true : false;
        let flag2 = (this.NativeAppIconFile == undefined) ? true : false;
        let value = (flag1 == true || flag2 == true) ? true : false;
        addCreativeForm.controls.assetRequired.patchValue(value);
      }
      return false;
    }

    if (this.typeValue == 'Video') {
      request = {
        creativetype: this.typeValue,
        creativeName: this.creative.name.value,
        target_ad_unit_size: this.creative.target_ad_unit_size.value,
        ad_manager_hosted: this.creative.ad_manager_hosted.value,
        externally_hosted_asset: this.creative.externally_hosted_asset.value,
        upload_as_mezzanine: this.creative.upload_as_mezzanine.value,
        duration: this.creative.duration.value,
        skippable_video: this.creative.skippable_video.value,
        destination: this.creative.destination.value,
        destination_url: this.creative.url.value,
        ssl_compatible: this.creative.ssl_compatible.value,
        ssl_compatibleSwitch: this.creative.ssl_compatibleSwitch.value,
        label: this.creative.label.value,
        custom_parameter: this.creative.custom_parameter.value,
        custom_field: this.creative.custom_field.value,
        third_party_trackingArray: JSON.stringify(this.creative.third_party_trackingArray.value),
        custom_setting: this.creative.custom_setting.value,
        formControlNameSwtch: this.creative.formControlNameSwtch.value,
      }
    }
    else if (this.typeValue == 'Redirect') {
      request = {
        creativetype: this.typeValue,
        creativeName: this.creative.name.value,
        target_ad_unit_size: this.creative.target_ad_unit_size.value,
        vast_tag_url: this.creative.vast_tag_url.value,
        redirect_type: this.creative.redirect_type.value,
        duration: this.creative.duration.value,
        skippable_video: this.creative.skippable_video.value,
        internal_redirect: this.creative.ssl_compatible.value,
        ssl_compatible: this.creative.ssl_compatible.value,
        ssl_compatibleSwitch: this.creative.ssl_compatibleSwitch.value,
        label: this.creative.label.value,
        custom_field: this.creative.custom_field.value,
        third_party_trackingArray: JSON.stringify(this.creative.third_party_trackingArray.value),
        custom_setting: this.creative.custom_setting.value,
        formControlNameSwtch: this.creative.formControlNameSwtch.value,
      }
    }
    else if (this.typeValue == 'Overlay') {
      request = {
        creativetype: this.typeValue,
        creativeName: this.creative.name.value,
        target_ad_unit_size: this.creative.target_ad_unit_size.value,
        ad_manager_hosted: this.creative.ad_manager_hosted.value,
        externally_hosted_asset: this.creative.externally_hosted_asset.value,
        scaling: this.creative.url.value,
        destination_url: this.creative.url.value,
        duration: this.creative.duration.value,
        orientation: this.creative.orientation.value,
        label: this.creative.label.value,
        custom_parameter: this.creative.custom_parameter.value,
        custom_field: this.creative.custom_field.value,
        third_party_trackingArray: JSON.stringify(this.creative.third_party_trackingArray.value),
        custom_setting: this.creative.custom_setting.value,
        formControlNameSwtch: this.creative.formControlNameSwtch.value,
      }

    }
    else if (this.typeValue == 'image') {
      request = {
        templateType: templateType,
        creativetype: this.typeValue,
        creativeName: this.creative.name.value,
        target_ad_unit_size: this.creative.target_ad_unit_size.value,
        ad_manager_hosted: this.creative.ad_manager_hosted.value,
        externally_hosted_asset: this.creative.externally_hosted_asset.value,
        density1: this.creative.density1.value,
        density2: this.creative.density2.value,
        density3: this.creative.density3.value,
        destination: this.creative.destination.value,
        destination_url: this.creative.url.value,
        alt_text: this.creative.alt_text.value,
        orientation: this.creative.orientation.value,
        label: this.creative.label.value,
        custom_field: this.creative.custom_field.value,
        third_party_trackingArray: this.creative.third_party_trackingArray.value,
        custom_setting: this.creative.custom_setting.value,
        formControlNameSwtch: this.creative.formControlNameSwtch.value,
      }
    }

    else if (this.typeValue == 'html5') {
      request = {
        templateType: templateType,
        creativetype: this.typeValue,
        creativeName: this.creative.name.value,
        target_ad_unit_size: this.creative.target_ad_unit_size.value,
        ssl_compatible: this.creative.ssl_compatible.value,
        ssl_compatibleSwitch: this.creative.ssl_compatibleSwitch.value,
        orientation: this.creative.orientation.value,
        label: this.creative.label.value,
        custom_field: this.creative.custom_field.value,
        third_party_trackingArray: this.creative.third_party_trackingArray.value,
        custom_setting: this.creative.custom_setting.value,
        formControlNameSwtch: this.creative.formControlNameSwtch.value,
      }
    }

    else if (this.typeValue == 'campaign_manager') {
      request = {
        templateType: templateType,
        creativetype: this.typeValue,
        creativeName: this.creative.name.value,
        target_ad_unit_size: this.creative.target_ad_unit_size.value,
        destination_url: this.creative.url.value,
        ssl_compatible: this.creative.ssl_compatible.value,
        ssl_compatibleSwitch: this.creative.ssl_compatibleSwitch.value,
        orientation: this.creative.orientation.value,
        label: this.creative.label.value,
        custom_field: this.creative.custom_field.value,
        third_party_trackingArray: this.creative.third_party_trackingArray.value,
        custom_setting: this.creative.custom_setting.value,
        formControlNameSwtch: this.creative.formControlNameSwtch.value,
      }
    }

    else if (this.typeValue == 'third_party') {
      request = {
        templateType: templateType,
        creativetype: this.typeValue,
        creativeName: this.creative.name.value,
        target_ad_unit_size: this.creative.target_ad_unit_size.value,
        costType_standard_amp: this.creative.costType_standard_amp.value,
        standard_macro: this.creative.standard_macro.value,
        amp_macro: this.creative.amp_macro.value,
        serve_into_safeframe: this.creative.serve_into_safeframe.value,
        ssl_compatible: this.creative.ssl_compatible.value,
        ssl_compatibleSwitch: this.creative.ssl_compatibleSwitch.value,
        orientation: this.creative.orientation.value,
        label: this.creative.label.value,
        custom_field: this.creative.custom_field.value,
        third_party_trackingArray: this.creative.third_party_trackingArray.value,
        custom_setting: this.creative.custom_setting.value,
        formControlNameSwtch: this.creative.formControlNameSwtch.value,
      }
    }

    else if (this.typeValue == 'custom') {
      request = {
        templateType: templateType,
        creativetype: this.typeValue,
        creativeName: this.creative.name.value,
        target_ad_unit_size: this.creative.target_ad_unit_size.value,
        costType_standard_amp: this.creative.costType_standard_amp.value,
        standard_macro: this.creative.standard_macro.value,
        amp_macro: this.creative.amp_macro.value,
        destination_url: this.creative.url.value,
        serve_into_safeframe: this.creative.serve_into_safeframe.value,
        ssl_compatible: this.creative.ssl_compatible.value,
        ssl_compatibleSwitch: this.creative.ssl_compatibleSwitch.value,
        orientation: this.creative.orientation.value,
        label: this.creative.label.value,
        custom_field: this.creative.custom_field.value,
        third_party_trackingArray: this.creative.third_party_trackingArray.value,
        custom_setting: this.creative.custom_setting.value,
        formControlNameSwtch: this.creative.formControlNameSwtch.value,
      }
    }

    else if (this.typeValue == 'native_format' && templateType == 'Native app install ad') {
      request = {
        templateId: '10004400',
        templateType: templateType,
        creativetype: this.typeValue,
        creativeName: this.creative.name.value,
        target_ad_unit_size: this.creative.target_ad_unit_size.value,
        destination_url: this.creative.url.value,
        ssl_compatible: this.creative.ssl_compatible.value,
        ssl_compatibleSwitch: this.creative.ssl_compatibleSwitch.value,
        label: this.creative.label.value,
        custom_field: this.creative.custom_field.value,

        custom_setting: this.creative.custom_setting.value,
        formControlNameSwtch: this.creative.formControlNameSwtch.value,

        headline: this.creative.headline.value,
        body: this.creative.body.value,
        call_to_action: this.creative.call_to_action.value,
        star_rating: this.creative.star_rating.value,
        store: this.creative.store.value,
        price: this.creative.price.value,
        deep_link_click_action_url: this.creative.deep_link_click_action_url.value,
        third_party_impression_tracker: this.creative.third_party_impression_tracker.value,
        third_party_click_tracker: this.creative.third_party_click_tracker.value,
        template_name: this.creative.template_name.value,
        template_description: this.creative.template_description.value,

      }
    }
    else if (this.typeValue == 'native_format' && templateType == 'Native content ad') {
      request = {
        templateId: '10004520',
        templateType: templateType,
        creativetype: this.typeValue,
        creativeName: this.creative.name.value,
        target_ad_unit_size: this.creative.target_ad_unit_size.value,
        destination_url: this.creative.url.value,
        ssl_compatible: this.creative.ssl_compatible.value,
        ssl_compatibleSwitch: this.creative.ssl_compatibleSwitch.value,
        label: this.creative.label.value,
        custom_field: this.creative.custom_field.value,

        custom_setting: this.creative.custom_setting.value,
        formControlNameSwtch: this.creative.formControlNameSwtch.value,

        headline: this.creative.headline.value,
        body: this.creative.body.value,
        call_to_action: this.creative.call_to_action.value,
        advertiser: this.creative.advertiser.value,
        deep_link_click_action_url: this.creative.deep_link_click_action_url.value,
        third_party_impression_tracker: this.creative.third_party_impression_tracker.value,
        third_party_click_tracker: this.creative.third_party_click_tracker.value,
        template_name: this.creative.template_name.value,
        template_description: this.creative.template_description.value,

      }
    }
    else if (this.typeValue == 'native_format' && templateType == 'Native video app install ad') {
      request = {
        templateId: '10006920',
        templateType: templateType,
        creativetype: this.typeValue,
        creativeName: this.creative.name.value,
        target_ad_unit_size: this.creative.target_ad_unit_size.value,
        destination_url: this.creative.url.value,
        ssl_compatible: this.creative.ssl_compatible.value,
        ssl_compatibleSwitch: this.creative.ssl_compatibleSwitch.value,
        label: this.creative.label.value,
        custom_field: this.creative.custom_field.value,

        custom_setting: this.creative.custom_setting.value,
        formControlNameSwtch: this.creative.formControlNameSwtch.value,

        headline: this.creative.headline.value,
        body: this.creative.body.value,
        star_rating: this.creative.star_rating.value,
        store: this.creative.store.value,
        price: this.creative.price.value,
        call_to_action: this.creative.call_to_action.value,
        advertiser: this.creative.advertiser.value,
        deep_link_click_action_url: this.creative.deep_link_click_action_url.value,
        third_party_impression_tracker: this.creative.third_party_impression_tracker.value,
        third_party_click_tracker: this.creative.third_party_click_tracker.value,
        template_name: this.creative.template_name.value,
        template_description: this.creative.template_description.value,

        ad_manager_hosted: this.creative.ad_manager_hosted.value,
        externally_hosted_asset: this.creative.externally_hosted_asset.value,
        duration: this.creative.duration.value,
        third_party_trackingArray: this.creative.third_party_trackingArray.value,
        vast_tag_url: this.creative.vast_tag_url.value,

      }
    }
    else if (this.typeValue == 'native_format' && templateType == 'Native video content ad') {
      request = {
        templateId: '10007040',
        templateType: templateType,
        creativetype: this.typeValue,
        creativeName: this.creative.name.value,
        target_ad_unit_size: this.creative.target_ad_unit_size.value,
        destination_url: this.creative.url.value,
        ssl_compatible: this.creative.ssl_compatible.value,
        ssl_compatibleSwitch: this.creative.ssl_compatibleSwitch.value,
        label: this.creative.label.value,
        custom_field: this.creative.custom_field.value,

        custom_setting: this.creative.custom_setting.value,
        formControlNameSwtch: this.creative.formControlNameSwtch.value,

        headline: this.creative.headline.value,
        body: this.creative.body.value,
        call_to_action: this.creative.call_to_action.value,
        advertiser: this.creative.advertiser.value,
        deep_link_click_action_url: this.creative.deep_link_click_action_url.value,
        third_party_impression_tracker: this.creative.third_party_impression_tracker.value,
        third_party_click_tracker: this.creative.third_party_click_tracker.value,
        template_name: this.creative.template_name.value,
        template_description: this.creative.template_description.value,

        ad_manager_hosted: this.creative.ad_manager_hosted.value,
        externally_hosted_asset: this.creative.externally_hosted_asset.value,
        duration: this.creative.duration.value,
        third_party_trackingArray: this.creative.third_party_trackingArray.value,
        vast_tag_url: this.creative.vast_tag_url.value,

      }
    }
    else if (this.typeValue == 'standard_creative_template' && templateType == 'Forecasting Pixel') {
      request = {
        templateId: '11773039',
        templateType: templateType,
        creativetype: this.typeValue,
        creativeName: this.creative.name.value,
        target_ad_unit_size: this.creative.target_ad_unit_size.value,
        ssl_compatible: this.creative.ssl_compatible.value,
        ssl_compatibleSwitch: this.creative.ssl_compatibleSwitch.value,
        orientation: this.creative.orientation.value,
        label: this.creative.label.value,
        custom_field: this.creative.custom_field.value,
        unlink_from_creative_template: this.creative.unlink_from_creative_template.value,

        custom_setting: this.creative.custom_setting.value,
        formControlNameSwtch: this.creative.formControlNameSwtch.value,

        template_name: this.creative.template_name.value,
        template_description: this.creative.template_description.value,
        creative_template_snippet: this.creative.template_description.value,
      }
    }

    else if (this.typeValue == 'standard_creative_template' && templateType == 'HTML5 animated ad') {
      request = {
        templateId: '10001520',
        templateType: templateType,
        creativetype: this.typeValue,
        creativeName: this.creative.name.value,
        target_ad_unit_size: this.creative.target_ad_unit_size.value,
        ssl_compatible: this.creative.ssl_compatible.value,
        ssl_compatibleSwitch: this.creative.ssl_compatibleSwitch.value,
        orientation: this.creative.orientation.value,
        label: this.creative.label.value,
        custom_field: this.creative.custom_field.value,
        unlink_from_creative_template: this.creative.unlink_from_creative_template.value,

        custom_setting: this.creative.custom_setting.value,
        formControlNameSwtch: this.creative.formControlNameSwtch.value,

        template_name: this.creative.template_name.value,
        template_description: this.creative.template_description.value,
        creative_template_snippet: this.creative.creative_template_snippet.value,

        // width: this.creative.width.value,
        // height: this.creative.height.value,
        backgroundColor: this.creative.backgroundColor.value,
        creativeWidth: this.creative.creativeWidth.value,
        creativeHeight: this.creative.creativeHeight.value,
        userDefinedVariableArray: this.creative.userDefinedVariableArray.value,
        userDefinedVariableArray2: this.creative.userDefinedVariableArray2.value,
      }
    }
    else if (this.typeValue == 'standard_creative_template' &&
      templateType == 'Image banner with optional third party tracking') {
      request = {
        templateId: '10000680',
        templateType: templateType,
        creativetype: this.typeValue,
        creativeName: this.creative.name.value,
        target_ad_unit_size: this.creative.target_ad_unit_size.value,
        ssl_compatible: this.creative.ssl_compatible.value,
        ssl_compatibleSwitch: this.creative.ssl_compatibleSwitch.value,
        orientation: this.creative.orientation.value,
        label: this.creative.label.value,
        custom_field: this.creative.custom_field.value,
        unlink_from_creative_template: this.creative.unlink_from_creative_template.value,

        custom_setting: this.creative.custom_setting.value,
        formControlNameSwtch: this.creative.formControlNameSwtch.value,

        template_name: this.creative.template_name.value,
        template_description: this.creative.template_description.value,
        creative_template_snippet: this.creative.creative_template_snippet.value,

        imageWidth: this.creative.width.value,
        imageHeight: this.creative.height.value,
        url: this.creative.url.value,
        targetWindow: this.creative.targetWindow.value,
        third_party_impression_tracker1: this.creative.third_party_impression_tracker1.value,
        third_party_impression_tracker2: this.creative.third_party_impression_tracker2.value,
        safecount_survey_url: this.creative.safecount_survey_url.value,
        imageBorder: this.creative.imageBorder.value,

      }
    }
    else if (this.typeValue == 'standard_creative_template' &&
      templateType == 'Image with Google +1') {
      request = {

        templateId: '10001760',
        templateType: templateType,
        creativetype: this.typeValue,
        creativeName: this.creative.name.value,
        target_ad_unit_size: this.creative.target_ad_unit_size.value,
        ssl_compatible: this.creative.ssl_compatible.value,
        ssl_compatibleSwitch: this.creative.ssl_compatibleSwitch.value,
        orientation: this.creative.orientation.value,
        label: this.creative.label.value,
        custom_field: this.creative.custom_field.value,
        unlink_from_creative_template: this.creative.unlink_from_creative_template.value,

        custom_setting: this.creative.custom_setting.value,
        formControlNameSwtch: this.creative.formControlNameSwtch.value,

        template_name: this.creative.template_name.value,
        template_description: this.creative.template_description.value,
        creative_template_snippet: this.creative.creative_template_snippet.value,

        width: this.creative.width.value,
        height: this.creative.height.value,
        url: this.creative.url.value,
        targetWindow: this.creative.targetWindow.value,
        url2: this.creative.url2.value,
        button_Size: this.creative.button_Size.value,
        buttonPosition: this.creative.buttonPosition.value,
        annotation: this.creative.annotation.value,
        autoDisappear: this.creative.autoDisappear.value,

      }
    }

    else if (this.typeValue == 'standard_creative_template' &&
      templateType == 'Mobile application download') {
      request = {

        templateId: '10001280',
        templateType: templateType,
        creativetype: this.typeValue,
        creativeName: this.creative.name.value,
        target_ad_unit_size: this.creative.target_ad_unit_size.value,
        ssl_compatible: this.creative.ssl_compatible.value,
        ssl_compatibleSwitch: this.creative.ssl_compatibleSwitch.value,
        orientation: this.creative.label.value,
        label: this.creative.label.value,
        custom_field: this.creative.custom_field.value,
        unlink_from_creative_template: this.creative.unlink_from_creative_template.value,

        custom_setting: this.creative.custom_setting.value,
        formControlNameSwtch: this.creative.formControlNameSwtch.value,

        template_name: this.creative.template_name.value,
        template_description: this.creative.template_description.value,
        creative_template_snippet: this.creative.creative_template_snippet.value,

        iconName: this.creative.iconName.value,
        application_download_uri: this.creative.application_download_uri.value,
        headline: this.creative.headline.value,
        description_line: this.creative.description_line.value,
        description_line2: this.creative.description_line2.value,
        buttonText: this.creative.buttonText.value,

      }
    }
    else if (this.typeValue == 'standard_creative_template' &&
      templateType == 'Mobile fix-position banner') {
      request = {

        templateId: '10004280',
        templateType: templateType,
        creativetype: this.typeValue,
        creativeName: this.creative.name.value,
        target_ad_unit_size: this.creative.target_ad_unit_size.value,
        ssl_compatible: this.creative.ssl_compatible.value,
        ssl_compatibleSwitch: this.creative.ssl_compatibleSwitch.value,
        orientation: this.creative.orientation.value,
        label: this.creative.label.value,
        custom_field: this.creative.custom_field.value,
        unlink_from_creative_template: this.creative.unlink_from_creative_template.value,

        custom_setting: this.creative.custom_setting.value,
        formControlNameSwtch: this.creative.formControlNameSwtch.value,

        template_name: this.creative.template_name.value,
        template_description: this.creative.template_description.value,
        creative_template_snippet: this.creative.creative_template_snippet.value,

        url: this.creative.url.value,
        height: this.creative.height.value,
        width: this.creative.width.value,

      }
    }

    else if (this.typeValue == 'standard_creative_template' &&
      templateType == 'Mobile image carousel(app and web)') {
      request = {
        templateId: '10003800',
        templateType: templateType,
        creativetype: this.typeValue,
        creativeName: this.creative.name.value,
        target_ad_unit_size: this.creative.target_ad_unit_size.value,
        ssl_compatible: this.creative.ssl_compatible.value,
        ssl_compatibleSwitch: this.creative.ssl_compatibleSwitch.value,
        orientation: this.creative.orientation.value,
        label: this.creative.label.value,
        custom_field: this.creative.custom_field.value,
        unlink_from_creative_template: this.creative.unlink_from_creative_template.value,

        custom_setting: this.creative.custom_setting.value,
        formControlNameSwtch: this.creative.formControlNameSwtch.value,

        template_name: this.creative.template_name.value,
        template_description: this.creative.template_description.value,
        creative_template_snippet: this.creative.creative_template_snippet.value,

        url: this.creative.url.value,
        height: this.creative.height.value,
        width: this.creative.width.value,

      }
    }


    else if (this.typeValue == 'standard_creative_template' &&
      templateType == 'Mobile image with Google +1') {
      request = {
        templateId: '10003200',
        templateType: templateType,
        creativetype: this.typeValue,
        creativeName: this.creative.name.value,
        target_ad_unit_size: this.creative.target_ad_unit_size.value,
        ssl_compatible: this.creative.ssl_compatible.value,
        ssl_compatibleSwitch: this.creative.ssl_compatibleSwitch.value,
        orientation: this.creative.orientation.value,
        label: this.creative.label.value,
        custom_field: this.creative.custom_field.value,
        unlink_from_creative_template: this.creative.unlink_from_creative_template.value,

        custom_setting: this.creative.custom_setting.value,
        formControlNameSwtch: this.creative.formControlNameSwtch.value,

        template_name: this.creative.template_name.value,
        template_description: this.creative.template_description.value,
        creative_template_snippet: this.creative.creative_template_snippet.value,

        url: this.creative.url.value,
        height: this.creative.height.value,
        width: this.creative.width.value,
        targetWindow: this.creative.targetWindow.value,
        url2: this.creative.url2.value,
        button_Size: this.creative.button_Size.value,
        buttonPosition: this.creative.buttonPosition.value,
        annotation: this.creative.annotation.value,
        autoDisappear: this.creative.autoDisappear.value,

      }
    }

    else if (this.typeValue == 'standard_creative_template' &&
      templateType == 'Mobile interstitial with auto close (app)') {
      request = {
        templateId: '10003920',
        templateType: templateType,
        creativetype: this.typeValue,
        creativeName: this.creative.name.value,
        target_ad_unit_size: this.creative.target_ad_unit_size.value,
        ssl_compatible: this.creative.ssl_compatible.value,
        ssl_compatibleSwitch: this.creative.ssl_compatibleSwitch.value,
        orientation: this.creative.orientation.value,
        label: this.creative.label.value,
        custom_field: this.creative.custom_field.value,
        unlink_from_creative_template: this.creative.unlink_from_creative_template.value,

        custom_setting: this.creative.custom_setting.value,
        formControlNameSwtch: this.creative.formControlNameSwtch.value,

        template_name: this.creative.template_name.value,
        template_description: this.creative.template_description.value,
        creative_template_snippet: this.creative.creative_template_snippet.value,

        url: this.creative.url.value,
        displayTime: this.creative.displayTime.value,
        allowScaling: this.creative.allowScaling.value,
        allowStretching: this.creative.allowStretching.value,
        third_party_impression_tracker: this.creative.third_party_impression_tracker.value,

      }
    }

    else if (this.typeValue == 'standard_creative_template' &&
      templateType == 'Pop-up or pop-under with image content') {
      request = {
        templateId: '10000920',
        templateType: templateType,
        creativetype: this.typeValue,
        creativeName: this.creative.name.value,
        target_ad_unit_size: this.creative.target_ad_unit_size.value,
        ssl_compatible: this.creative.ssl_compatible.value,
        ssl_compatibleSwitch: this.creative.ssl_compatibleSwitch.value,
        orientation: this.creative.orientation.value,
        label: this.creative.label.value,
        custom_field: this.creative.custom_field.value,
        unlink_from_creative_template: this.creative.unlink_from_creative_template.value,

        custom_setting: this.creative.custom_setting.value,
        formControlNameSwtch: this.creative.formControlNameSwtch.value,

        template_name: this.creative.template_name.value,
        template_description: this.creative.template_description.value,
        creative_template_snippet: this.creative.creative_template_snippet.value,

        url: this.creative.url.value,
        height: this.creative.height.value,
        width: this.creative.width.value,
        popType: this.creative.popType.value,
        displayToolbar: this.creative.displayToolbar.value,
        displayAddressBar: this.creative.displayAddressBar.value,
        displayStatusBar: this.creative.displayStatusBar.value,
        displayMenuBar: this.creative.displayMenuBar.value,
        displayScrollBar: this.creative.displayScrollBar.value,
        allow_window_to_resize: this.creative.allow_window_to_resize.value,
        displayDirectoryButtons: this.creative.displayDirectoryButtons.value,
        displayFullScreenMode: this.creative.displayFullScreenMode.value,
        targetWindow: this.creative.targetWindow.value,

      }
    }


    else if (this.typeValue == 'standard_creative_template' &&
      templateType == 'Pop-up or pop-under with third party content') {
      request = {
        templateId: '10000320',
        templateType: templateType,
        creativetype: this.typeValue,
        creativeName: this.creative.name.value,
        target_ad_unit_size: this.creative.target_ad_unit_size.value,
        ssl_compatible: this.creative.ssl_compatible.value,
        ssl_compatibleSwitch: this.creative.ssl_compatibleSwitch.value,
        orientation: this.creative.orientation.value,
        label: this.creative.label.value,
        custom_field: this.creative.custom_field.value,
        unlink_from_creative_template: this.creative.unlink_from_creative_template.value,

        custom_setting: this.creative.custom_setting.value,
        formControlNameSwtch: this.creative.formControlNameSwtch.value,

        template_name: this.creative.template_name.value,
        template_description: this.creative.template_description.value,
        creative_template_snippet: this.creative.creative_template_snippet.value,

        url: this.creative.url.value,
        height: this.creative.height.value,
        width: this.creative.width.value,
        popType: this.creative.popType.value,
        displayToolbar: this.creative.displayToolbar.value,
        displayAddressBar: this.creative.displayAddressBar.value,
        displayStatusBar: this.creative.displayStatusBar.value,
        displayMenuBar: this.creative.displayMenuBar.value,
        displayScrollBar: this.creative.displayScrollBar.value,
        allow_window_to_resize: this.creative.allow_window_to_resize.value,
        displayDirectoryButtons: this.creative.displayDirectoryButtons.value,
        displayFullScreenMode: this.creative.displayFullScreenMode.value,
        targetWindow: this.creative.targetWindow.value,

      }
    }


    else if (this.typeValue == 'standard_creative_template' &&
      templateType == 'Text ad') {
      request = {
        templateId: '10000440',
        templateType: templateType,
        creativetype: this.typeValue,
        creativeName: this.creative.name.value,
        target_ad_unit_size: this.creative.target_ad_unit_size.value,
        ssl_compatible: this.creative.ssl_compatible.value,
        ssl_compatibleSwitch: this.creative.ssl_compatibleSwitch.value,
        orientation: this.creative.orientation.value,
        label: this.creative.label.value,
        custom_field: this.creative.custom_field.value,
        unlink_from_creative_template: this.creative.unlink_from_creative_template.value,

        custom_setting: this.creative.custom_setting.value,
        formControlNameSwtch: this.creative.formControlNameSwtch.value,

        template_name: this.creative.template_name.value,
        template_description: this.creative.template_description.value,
        creative_template_snippet: this.creative.creative_template_snippet.value,

        url: this.creative.url.value,
        targetWindow: this.creative.targetWindow.value,
        hoverText: this.creative.hoverText.value,
        linkColor: this.creative.linkColor.value,
        linktitle: this.creative.linktitle.value,
        descriptionTextAd: this.creative.descriptionTextAd.value,
        textColor: this.creative.textColor.value,

      }
    }

    request['line_item_id'] = this.gamLineItemId;

    // console.log("request   :", request)

  
if(this.editCreativeFlag){
 let creativeRequest = {
    creative_json: JSON.stringify(request),
    line_ItemId: this.gamLineItemId,
    name: this.creative.name.value,
    updated_by: this.currentUser,
    creativeAssetData:this.CreativeAssetData,
    id:this.retrievedCreativeId
  }
  frmData.append("payload", JSON.stringify(creativeRequest));

    this.dataFetchServ.updateCreative(frmData).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        // console.log(data['data']);
        return;
      }
      if (data['status'] == 1) {
        addCreativeForm.controls.submittedSuccessfully.patchValue(true)
        addCreativeForm.disable();
        this.toastService.displayToast({
          severity: 'success',
          summary: 'Campaign Management',
          detail: 'Creative Updated successfully'
        });
      }
      return;
    });
    return;
  }

  let creativeRequest = {
    creative_json: JSON.stringify(request),
    line_ItemId: this.gamLineItemId,
    name: this.creative.name.value,
    status: 'Inactive',
    created_by: this.currentUser,
    updated_by: 1
  }
  frmData.append("payload", JSON.stringify(creativeRequest));
    this.dataFetchServ.saveCreative(frmData).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        // console.log(data['data']);
        return;
      }
      if (data['status'] == 1) {
        addCreativeForm.controls.submittedSuccessfully.patchValue(true)
        addCreativeForm.disable();
        this.toastService.displayToast({
          severity: 'success',
          summary: 'Campaign Management',
          detail: 'Creative created successfully'
        });
      }
    });

  }


  selectedAvailableVideoPositioning = { 'checked': true, 'selectedData': [] };
  rowSelectVideoPosition(event, rowData) {
    if (event.checked) {
      this.selectedAvailableVideoPositioning['selectedData'].push({ 'name': rowData.name, 'id': rowData.id, 'checked': false })
    }
    if (!event.checked) {
      let index = 0
      this.selectedAvailableVideoPositioning['selectedData'].forEach(element => {
        if (element.id == rowData.id) {
          this.selectedAvailableVideoPositioning['selectedData'].splice(index, 1);
        }
        index++;
      })
    }
  }


  removeSelectedAvailableVideo(rowData) {
    let index = 0
    this.selectedAvailableVideoPositioning['selectedData'].forEach(element => {
      if (element.id == rowData.id) {
        this.selectedAvailableVideoPositioning['selectedData'].splice(index, 1);
      }
      index++;
    })
    this.availableVideoPositioning.forEach(element => {
      if (element.id == rowData.id) {
        element.checked = false;
      }
      index++;
    })
  }

  selectAvailableGamDeviceCategory = {
    'checked': true,
    'selectedData': []
  };
  rowSelectgamDeviceCategory(event, rowData) {
    if (event.checked) {
      this.selectAvailableGamDeviceCategory['selectedData'].push({ 'name': rowData.name, 'id': rowData.id, 'checked': false })
    }
    if (!event.checked) {
      let index = 0
      this.selectAvailableGamDeviceCategory['selectedData'].forEach(element => {
        if (element.id == rowData.id) {
          this.selectAvailableGamDeviceCategory['selectedData'].splice(index, 1);
        }
        index++;
      })
    }
    this.availableGamDeviceCategory.forEach(element => {
      if (element.id == rowData.id) {
        element.checked = event.checked;
      }
    })
  }


  removeSelectedGamDeviceCategory(rowData) {
    let index = 0
    this.selectAvailableGamDeviceCategory['selectedData'].forEach(element => {
      if (element.id == rowData.id) {
        this.selectAvailableGamDeviceCategory['selectedData'].splice(index, 1);
      }
      index++;
    })
    this.availableGamDeviceCategory.forEach(element => {
      if (element.id == rowData.id) {
        element.checked = false;
      }
      index++;
    })
    // console.log("this.availableGamDeviceCategory     :",this.availableGamDeviceCategory)
  }



  selectedGamAdUnits = { 'checked': true, 'selectedData': [] };
  rowSelectGamAdUnits(event, rowData) {
    if (event.checked) {
      this.selectedGamAdUnits['selectedData'].push({ 'name': rowData.name, 'id': rowData.id, 'checked': false })
    }
    if (!event.checked) {
      let index = 0
      this.selectedGamAdUnits['selectedData'].forEach(element => {
        if (element.id == rowData.id) {
          this.selectedGamAdUnits['selectedData'].splice(index, 1);
        }
        index++;
      })
    }
    this.availableGamAdUnits.forEach(element => {
      if (element.id == rowData.id) {
        element.checked = event.checked;
      }
    })
  }


  removeGamAdUnits(rowData) {
    let index = 0
    this.selectedGamAdUnits['selectedData'].forEach(element => {
      if (element.id == rowData.id) {
        this.selectedGamAdUnits['selectedData'].splice(index, 1);
      }
      index++;
    })
    this.availableGamAdUnits.forEach(element => {
      if (element.id == rowData.id) {
        element.checked = false;
      }
      index++;
    })
  }

  selectGamPlacement = { 'checked': true, 'selectedData': [] };
  rowSelectGamPlacement(event, rowData) {
    if (event.checked) {
      this.selectGamPlacement['selectedData'].push({ 'name': rowData.name, 'id': rowData.id, 'checked': false })
    }
    if (!event.checked) {
      let index = 0
      this.selectGamPlacement['selectedData'].forEach(element => {
        if (element.id == rowData.id) {
          this.selectGamPlacement['selectedData'].splice(index, 1);
        }
        index++;
      })
    }
    this.availableGamPlacement.forEach(element => {
      if (element.id == rowData.id) {
        element.checked = event.checked;
      }
    })
  }


  removeGamPlacement(rowData) {
    let index = 0
    this.selectGamPlacement['selectedData'].forEach(element => {
      if (element.id == rowData.id) {
        this.selectGamPlacement['selectedData'].splice(index, 1);
      }
      index++;
    })
    this.availableGamPlacement.forEach(element => {
      if (element.id == rowData.id) {
        element.checked = false;
      }
      index++;
    })
  }

  selectGamDeviceBandwidth = { 'checked': true, 'selectedData': [] };
  rowSelectGamDeviceBandwidth(event, rowData) {
    if (event.checked) {
      this.selectGamDeviceBandwidth['selectedData'].push({ 'name': rowData.name, 'id': rowData.id, 'checked': false })
    }
    if (!event.checked) {
      let index = 0
      this.selectGamDeviceBandwidth['selectedData'].forEach(element => {
        if (element.id == rowData.id) {
          this.selectGamDeviceBandwidth['selectedData'].splice(index, 1);
        }
        index++;
      })

    }
    this.availableGamDeviceBandwidth.forEach(element => {
      if (element.id == rowData.id) {
        element.checked = event.checked;
      }
    })
  }


  removeselectedGamDeviceBandwidth(rowData) {
    let index = 0
    this.selectGamDeviceBandwidth['selectedData'].forEach(element => {
      if (element.id == rowData.id) {
        this.selectGamDeviceBandwidth['selectedData'].splice(index, 1);
      }
      index++;
    })
    this.availableGamDeviceBandwidth.forEach(element => {
      if (element.id == rowData.id) {
        element.checked = false;
      }
      index++;
    })
  }

  toggleSelectionBandwidth(event, data) {
    this.selectGamDeviceBandwidth['selectedData'].forEach(element => {
      if (element.name == data.name) {
        element.name = data.name;
        element.id = data.id;
        element.checked = event.checked;
      }
    })
  }

  toggleSelectionAdUnit(event, data) {
    this.selectedGamAdUnits['selectedData'].forEach(element => {
      if (element.name == data.name) {
        element.name = data.name;
        element.id = data.id;
        element.checked = event.checked;
      }
    })
  }

  toggleSelectionDeviceCategory(event, data) {
    this.selectAvailableGamDeviceCategory['selectedData'].forEach(element => {
      if (element.name == data.name) {
        element.name = data.name;
        element.id = data.id;
        element.checked = event.checked;
      }
    })
  }



  toggleSelectionPlacement(event, data) {
    this.selectGamPlacement['selectedData'].forEach(element => {
      if (element.name == data.name) {
        element.name = data.name;
        element.id = data.id;
        element.checked = event.checked;
      }
    })
  }

  selectGamDeviceCapability = { 'checked': true, 'selectedData': [] };
  rowSelectGamDeviceCapability(event, rowData) {
    if (event.checked) {
      this.selectGamDeviceCapability['selectedData'].push({ 'name': rowData.name, 'id': rowData.id, 'checked': false })
    }
    if (!event.checked) {
      let index = 0
      this.selectGamDeviceCapability['selectedData'].forEach(element => {
        if (element.id == rowData.id) {
          this.selectGamDeviceCapability['selectedData'].splice(index, 1);
        }
        index++;
      })

    }
    this.availableGamDeviceCapability.forEach(element => {
      if (element.id == rowData.id) {
        element.checked = event.checked;
      }
    })
  }


  removeselectedGamDeviceCapability(rowData) {
    let index = 0
    this.selectGamDeviceCapability['selectedData'].forEach(element => {
      if (element.id == rowData.id) {
        this.selectGamDeviceCapability['selectedData'].splice(index, 1);
      }
      index++;
    })
    this.availableGamDeviceCapability.forEach(element => {
      if (element.id == rowData.id) {
        element.checked = false;
      }
      index++;
    })
  }

  toggleSelectionDeviceCapability(event, data) {
    this.selectGamDeviceCapability['selectedData'].forEach(element => {
      if (element.name == data.name) {
        element.name = data.name;
        element.id = data.id;
        element.checked = event.checked;
      }
    })
  }


  selectGamBrowswerLang = { 'checked': true, 'selectedData': [] };
  rowSelectGamBrowswerLang(event, rowData) {
    if (event.checked) {
      this.selectGamBrowswerLang['selectedData'].push({ 'name': rowData.name, 'id': rowData.id, 'checked': false })
    }
    if (!event.checked) {
      let index = 0
      this.selectGamBrowswerLang['selectedData'].forEach(element => {
        if (element.id == rowData.id) {
          this.selectGamBrowswerLang['selectedData'].splice(index, 1);
        }
        index++;
      })

    }
    this.availableGamBrowswerLang.forEach(element => {
      if (element.id == rowData.id) {
        element.checked = event.checked;
      }
    })
  }


  removeselectedGamBrowswerLang(rowData) {
    let index = 0
    this.selectGamBrowswerLang['selectedData'].forEach(element => {
      if (element.id == rowData.id) {
        this.selectGamBrowswerLang['selectedData'].splice(index, 1);
      }
      index++;
    })
    this.availableGamBrowswerLang.forEach(element => {
      if (element.id == rowData.id) {
        element.checked = false;
      }
      index++;
    })
  }

  toggleSelectionBrowserLanguage(event, data) {
    this.selectGamBrowswerLang['selectedData'].forEach(element => {
      if (element.name == data.name) {
        element.name = data.name;
        element.id = data.id;
        element.checked = event.checked;
      }
    })
  }


  selectavailableGamTargetingOS = { 'checked': true, 'selectedData': [] };
  rowSelectGamTargetingOS(event, rowData) {
    if (event.checked) {
      this.selectavailableGamTargetingOS['selectedData'].push({ 'name': rowData.name, 'id': rowData.id, 'checked': false })
    }
    if (!event.checked) {
      let index = 0
      this.selectavailableGamTargetingOS['selectedData'].forEach(element => {
        if (element.id == rowData.id) {
          this.selectavailableGamTargetingOS['selectedData'].splice(index, 1);
        }
        index++;
      })
    }
    this.availableGamTargetingOS.forEach(element => {
      if (element.id == rowData.id) {
        element.checked = event.checked;
      }
    })
  }


  removeSelectedGamTargetingOS(rowData) {
    let index = 0
    this.selectavailableGamTargetingOS['selectedData'].forEach(element => {
      if (element.id == rowData.id) {
        this.selectavailableGamTargetingOS['selectedData'].splice(index, 1);
      }
      index++;
    })
    this.availableGamTargetingOS.forEach(element => {
      if (element.id == rowData.id) {
        element.checked = false;
      }
      index++;
    })
  }

  toggleSelectionBrowser(event, data) {
    this.selectavailableGamTargetingOS['selectedData'].forEach(element => {
      if (element.id == data.id) {
        element.name = data.name;
        element.id = data.id;
        element.checked = event.checked;
      }
    })
  }

  selectGamGeograhy = { 'checked': true, 'selectedData': [] };
  rowSelectGamGeograhy(event, rowData) {
    if (event.checked) {
      this.selectGamGeograhy['selectedData'].push({ 'name': rowData.name, 'id': rowData.id, 'checked': false })
    }
    if (!event.checked) {
      let index = 0
      this.selectGamGeograhy['selectedData'].forEach(element => {
        if (element.id == rowData.id) {
          this.selectGamGeograhy['selectedData'].splice(index, 1);
        }
        index++;
      })
    }
    this.availableGamGeograhy.forEach(element => {
      if (element.id == rowData.id) {
        element.checked = event.checked;
      }
    })
  }


  removeselectedGamGeograhy(rowData) {
    let index = 0
    this.selectGamGeograhy['selectedData'].forEach(element => {
      if (element.id == rowData.id) {
        this.selectGamGeograhy['selectedData'].splice(index, 1);
      }
      index++;
    })
    this.availableGamGeograhy.forEach(element => {
      if (element.id == rowData.id) {
        element.checked = false;
      }
      element.dynamic.forEach(nested => {
        if (nested.id == rowData.id) {
          nested.checked = false;
        }
      });
      index++;
    })

  }

  toggleSelectionGeography(event, data) {
    this.selectGamGeograhy['selectedData'].forEach(element => {
      if (element.id == data.id) {
        element.name = data.name;
        element.id = data.id;
        element.checked = event.checked;
      }
    })
  }

  rowSelectGeoNested(event, rowData) {
    if (event.checked) {
      this.selectGamGeograhy['selectedData'].push({ 'name': rowData.name, 'id': rowData.id, 'checked': false })
    }
    if (!event.checked) {
      let index = 0
      this.selectGamGeograhy['selectedData'].forEach(element => {
        if (element.id == rowData.id) {
          this.selectGamGeograhy['selectedData'].splice(index, 1);
        }
        index++;
      })
    }
    this.availableGamGeograhy.forEach(element => {
      // element.dynamic.forEach(nestedElement => {
      //       if(nestedElement.id == rowData.id){
      //         nestedElement.checked = event.checked;
      //       }
      // });

    })
  }



  getNestedData(rowData, identity) {
    let id = rowData.id
    let type = "'" + id + "'"
    // console.log("typeID   :id", id, "value   :", type, "rowData  :", rowData)
    if (identity == 'Geo') {
      let request = {
        from: 'Geo_Target',
        select: 'Id,Name',
        orderBy: 'Name ASC',
        where: 'parentids=' + type + ' AND ' + 'type=' + "'State'",
        selectedConnector:this.selectedConnector
      }
      // ParentIds,CountryCode,Type,Targetable'
      let dynamicData = [];
      this.dataFetchServ
        .getTargetingWithWhereClause(request)
        .subscribe(data => {
          data['data']['rows'].forEach(item => {
            dynamicData.push({
              'id': item.values[0].value,
              'name': item.values[1].value,
              'checked': false,
              'hideShow': false
            })
          });
          let selectedId = this.selectGamGeograhy['selectedData'].map(item => item['id']);
          if (selectedId.length != 0) {
            selectedId.forEach(element => {
              dynamicData.forEach(nested => {
                if (nested['id'] == element) {
                  nested.checked = true;
                }
              });
            })
          }


        });

      this.availableGamGeograhy.forEach(element => {
        if (element.id == id) {
          element.dynamic = dynamicData
        }
      })
      // console.log("dynamicData   :", dynamicData)
    }
    if (identity == 'OS') {


      let request = {
        from: 'Operating_System_Version',
        select: 'Id,MajorVersion,MinorVersion,MicroVersion,OperatingSystemId',
        orderBy: 'MajorVersion ASC',
        where: 'OperatingSystemId=' + type,
        selectedConnector:this.selectedConnector
      }
      // ParentIds,CountryCode,Type,Targetable'
      let dynamicData = [];
      this.dataFetchServ
        .getTargetingWithWhereClause(request)
        .subscribe(data => {
          // console.log('data    Operating_System_Version  :', data);
          data['data']['rows'].forEach(item => {
            dynamicData.push({
              'id': item.values[0].value,
              'name': rowData.name + ' ' + item.values[1].value + '.' + item.values[2].value + '.' + 'x',
              'checked': false,
              'hideShow': false
            })
          });
          let selectedId = this.selectGamTargetingOS['selectedData'].map(item => item['id']);
          if (selectedId.length != 0) {
            selectedId.forEach(element => {
              dynamicData.forEach(nested => {
                if (nested['id'] == element) {
                  nested.checked = true;
                }
              });
            })
          }

        });

      this.availableTargetingOS.forEach(element => {
        if (element.id == id) {
          element.dynamic = dynamicData
        }
      })
      // console.log("dynamicData   :", dynamicData)
    }
    if (identity == 'DM') {


      let request = {
        from: 'Mobile_Device',
        select: 'Id,MobileDeviceName',
        orderBy: 'MobileDeviceName ASC',
        where: 'MobileDeviceManufacturerId=' + type,
        selectedConnector:this.selectedConnector
      }
      // ParentIds,CountryCode,Type,Targetable'
      let dynamicData = [];
      this.dataFetchServ
        .getTargetingWithWhereClause(request)
        .subscribe(data => {
          // console.log('data    Mobile_Device  :', data);
          data['data']['rows'].forEach(item => {
            dynamicData.push({
              'id': item.values[0].value,
              'name': rowData.name + ' ' + item.values[1].value,
              'checked': false,
              'hideShow': false
            })
          });
          let selectedId = this.selectGAMDeviceManufacturer['selectedData'].map(item => item['id']);
          if (selectedId.length != 0) {
            selectedId.forEach(element => {
              dynamicData.forEach(nested => {
                if (nested['id'] == element) {
                  nested.checked = true;
                }
              });
            })
          }

        });

      this.availableGamDeviceManufaturere.forEach(element => {
        if (element.id == id) {
          element.dynamic = dynamicData
        }
      })
      // console.log("dynamicData   :", dynamicData)
    }
    if (identity == 'MobileCarrier') {


      let request = {
        from: 'Mobile_Carrier',
        select: 'Id,MobileCarrierName,CountryCode',
        orderBy: 'Name ASC',
        where: 'Id=' + type,
        selectedConnector:this.selectedConnector
      }
      // ParentIds,CountryCode,Type,Targetable'
      let dynamicData = [];
      this.dataFetchServ
        .getTargetingWithWhereClause(request)
        .subscribe(data => {
          // console.log('data      :', data);
          data['data']['rows'].forEach(item => {
            dynamicData.push({
              'id': item.values[0].value,
              'name': item.values[1].value + ' ' + item.values[2].value,
              'checked': false,
              'hideShow': false
            })
          });
          let selectedId = this.selectGamMobileCarrier['selectedData'].map(item => item['id']);
          if (selectedId.length != 0) {
            selectedId.forEach(element => {
              dynamicData.forEach(nested => {
                if (nested['id'] == element) {
                  nested.checked = true;
                }
              });
            })
          }
        });

      this.availableGammobileCarrier.forEach(element => {
        if (element.id == id) {
          element.dynamic = dynamicData
        }
      })
    }
  }


  selectGAMDeviceManufacturer = { 'checked': true, 'selectedData': [] };
  rowSelectgamDeviceManufature(event, rowData) {
    if (event.checked) {
      this.selectGAMDeviceManufacturer['selectedData'].push({ 'name': rowData.name, 'id': rowData.id, 'checked': false })
    }
    if (!event.checked) {
      let index = 0
      this.selectGAMDeviceManufacturer['selectedData'].forEach(element => {
        if (element.id == rowData.id) {
          this.selectGAMDeviceManufacturer['selectedData'].splice(index, 1);
        }
        index++;
      })
    }
    this.availableGamDeviceManufaturere.forEach(element => {
      if (element.id == rowData.id) {
        element.checked = event.checked;
      }
    })
  }


  removeGamDeviceManufaturere(rowData) {
    let index = 0
    this.selectGAMDeviceManufacturer['selectedData'].forEach(element => {
      if (element.id == rowData.id) {
        this.selectGAMDeviceManufacturer['selectedData'].splice(index, 1);
      }
      index++;
    })
    this.availableGamDeviceManufaturere.forEach(element => {
      if (element.id == rowData.id) {
        element.checked = false;
      }
      element.dynamic.forEach(nested => {
        if (nested.id == rowData.id) {
          nested.checked = false;
        }
      });
      index++;
    })
  }

  toggleSelectionDM(event, data) {
    this.selectGAMDeviceManufacturer['selectedData'].forEach(element => {
      if (element.id == data.id) {
        element.name = data.name;
        element.id = data.id;
        element.checked = event.checked;
      }
    })
  }

  rowSelectDynamicDM(event, rowData, parentData) {
    if (event.checked) {
      this.selectGAMDeviceManufacturer['selectedData'].push({ 'name': rowData.name, 'id': rowData.id, 'checked': false })
    }
    if (!event.checked) {
      let index = 0
      this.selectGAMDeviceManufacturer['selectedData'].forEach(element => {
        if (element.id == rowData.id) {
          this.selectGAMDeviceManufacturer['selectedData'].splice(index, 1);
        }
        index++;
      })
    }
    this.availableGamDeviceManufaturere.forEach(element => {
      if (element.id == parentData.id) {
        element.dynamic.forEach(nestedElement => {
          if (nestedElement.id == rowData.id) {
            nestedElement.checked = event.checked;
          }
        });
      }
    })
  }



  selectGamTargetingOS = { 'checked': true, 'selectedData': [] };
  rowSelectTargetingOS(event, rowData) {
    // console.log("event   :", event)
    if (event.checked) {
      this.selectGamTargetingOS['selectedData'].push({ 'name': rowData.name, 'id': rowData.id, 'checked': false })
    }
    if (!event.checked) {
      let index = 0
      this.selectGamTargetingOS['selectedData'].forEach(element => {
        if (element.id == rowData.id) {
          this.selectGamTargetingOS['selectedData'].splice(index, 1);
        }
        index++;
      })
    }
    this.availableTargetingOS.forEach(element => {
      if (element.id == rowData.id) {
        element.checked = event.checked;
      }
    })
  }


  removeGamTargetingOS(rowData) {
    let index = 0
    this.selectGamTargetingOS['selectedData'].forEach(element => {
      if (element.id == rowData.id) {
        this.selectGamTargetingOS['selectedData'].splice(index, 1);
      }
      index++;
    })
    this.availableTargetingOS.forEach(element => {
      if (element.id == rowData.id) {
        element.checked = false;
      }
      element.dynamic.forEach(nested => {

        if (nested.id == rowData.id) {
          nested.checked = false;
        }
      });
      index++;
    })
  }

  toggleSelectionOS(event, data) {
    this.selectGamTargetingOS['selectedData'].forEach(element => {
      if (element.id == data.id) {
        element.name = data.name;
        element.id = data.id;
        element.checked = event.checked;
      }
    })
  }

  rowSelectDynamicOS(event, rowData, parentData) {
    if (event.checked) {
      this.selectGamTargetingOS['selectedData'].push({ 'name': rowData.name, 'id': rowData.id, 'checked': false })
    }
    if (!event.checked) {
      let index = 0
      this.selectGamTargetingOS['selectedData'].forEach(element => {
        if (element.id == rowData.id) {
          this.selectGamTargetingOS['selectedData'].splice(index, 1);
        }
        index++;
      })
    }
    this.availableTargetingOS.forEach(element => {
      if (element.id == parentData.id) {
        element.dynamic.forEach(nestedElement => {
          if (nestedElement.id == rowData.id) {
            nestedElement.checked = event.checked;
          }
        });
      }
    })
  }

  toggleSelectionDynamicOS(event, data) {
    this.selectGamTargetingOS['selectedData'].forEach(element => {
      if (element.id == data.id) {
        element.name = data.name;
        element.id = data.id;
        element.checked = event.checked;
      }
    })
  }


  selectGamMobileCarrier = { 'checked': true, 'selectedData': [] };
  rowSelectMobileCarrier(event, rowData) {
    if (event.checked) {
      this.selectGamMobileCarrier['selectedData'].push({ 'name': rowData.name, 'id': rowData.id, 'checked': false })
    }
    if (!event.checked) {
      let index = 0
      this.selectGamMobileCarrier['selectedData'].forEach(element => {
        if (element.id == rowData.id) {
          this.selectGamMobileCarrier['selectedData'].splice(index, 1);
        }
        index++;
      })
    }
    this.availableGammobileCarrier.forEach(element => {
      if (element.id == rowData.id) {
        element.checked = event.checked;
      }
    })
  }


  removeGamMobileCarrier(rowData) {
    let index = 0
    this.selectGamMobileCarrier['selectedData'].forEach(element => {
      if (element.id == rowData.id) {
        this.selectGamMobileCarrier['selectedData'].splice(index, 1);
      }
      index++;
    })
    this.availableGammobileCarrier.forEach(element => {
      if (element.id == rowData.id) {
        element.checked = false;
      }
      element.dynamic.forEach(nested => {

        if (nested.id == rowData.id) {
          nested.checked = false;
        }
      });
      index++;
    })
  }

  toggleSelectionMobileCarrier(event, data) {
    this.selectGamMobileCarrier['selectedData'].forEach(element => {
      if (element.id == data.id) {
        element.name = data.name;
        element.id = data.id;
        element.checked = event.checked;
      }
    })
  }

  rowSelectDynamicMobileCarrier(event, rowData, parentData) {
    if (event.checked) {
      this.selectGamMobileCarrier['selectedData'].push({ 'name': rowData.name, 'id': rowData.id, 'checked': false })
    }
    if (!event.checked) {
      let index = 0
      this.selectGamMobileCarrier['selectedData'].forEach(element => {
        if (element.id == rowData.id) {
          this.selectGamMobileCarrier['selectedData'].splice(index, 1);
        }
        index++;
      })
    }
    this.availableGammobileCarrier.forEach(element => {
      if (element.id == parentData.id) {
        element.dynamic.forEach(nestedElement => {
          if (nestedElement.id == rowData.id) {
            nestedElement.checked = event.checked;
          }
        });
      }
    })
  }

  initializationOfCustomTargetingForm() {
    this.customTargeting = this.formBuilder.group({
      customTargetingArray: this.formBuilder.array([this.initializationOfCustomTargeting()]),
    });
  }

  initializationOfCustomTargeting() {
    return this.formBuilder.group({
      andOr: new FormControl(''),
      sampleValueList: [],
      sampleValue: '',
      keyValuePairControl: '',
      isAnyOf: '',

    })
  }

  getCustomTargetingArrayArray() {
    return this.customTargeting.get('customTargetingArray') as FormArray;
  }

  addCustomTargeting(formArray) {
    this.markFormArrayTouched(formArray);
    if (formArray.status != 'INVALID') {
      this.getCustomTargetingArrayArray().push(this.initializationOfCustomTargeting())
    }
  }

  removeCustomTargeting(Index) {
    if (this.getCustomTargetingArrayArray().length > 1) {
      this.getCustomTargetingArrayArray().removeAt(Index);
    }
  }

  private markFormArrayTouched(formArray: FormArray) {
    (<any>Object).values(formArray.controls).forEach(control => {
      control.markAsTouched();
      if (control.controls) {
        this.markFormArrayTouched(control);
      }
    });
  }




  selectedGamInventoryType = { 'checked': true, 'selectedData': [] };
  rowSelectGamInventoryType(event, rowData) {
    if (event.checked) {
      this.selectedGamInventoryType['selectedData'].push({ 'name': rowData.name, 'id': rowData.id, 'checked': false })
    }
    if (!event.checked) {
      let index = 0
      this.selectedGamInventoryType['selectedData'].forEach(element => {
        if (element.id == rowData.id) {
          this.selectedGamInventoryType['selectedData'].splice(index, 1);
        }
        index++;
      })
    }
    this.availableInventoryType.forEach(element => {
      if (element.id == rowData.id) {
        element.checked = event.checked;
      }
    })
  }


  removeGamInventoryType(rowData) {
    let index = 0
    this.selectedGamInventoryType['selectedData'].forEach(element => {
      if (element.id == rowData.id) {
        this.selectedGamInventoryType['selectedData'].splice(index, 1);
      }
      index++;
    })
    this.availableInventoryType.forEach(element => {
      if (element.id == rowData.id) {
        element.checked = false;
      }
      index++;
    })
  }

  toggleSelectionGamInventoryType(event, data) {
    this.selectedGamInventoryType['selectedData'].forEach(element => {
      if (element.name == data.name) {
        element.name = data.name;
        element.id = data.id;
        element.checked = event.checked;
      }
    })
  }


  selectedGamInventoryFormat = { 'checked': true, 'selectedData': [] };
  rowSelectGamInventoryFormat(event, rowData) {
    if (event.checked) {
      this.selectedGamInventoryFormat['selectedData'].push({ 'name': rowData.name, 'id': rowData.id, 'checked': false })
    }
    if (!event.checked) {
      let index = 0
      this.selectedGamInventoryFormat['selectedData'].forEach(element => {
        if (element.id == rowData.id) {
          this.selectedGamInventoryFormat['selectedData'].splice(index, 1);
        }
        index++;
      })
    }
    this.availableInventoryFormat.forEach(element => {
      if (element.id == rowData.id) {
        element.checked = event.checked;
      }
    })
  }


  removeGamInventoryFormat(rowData) {
    let index = 0
    this.selectedGamInventoryFormat['selectedData'].forEach(element => {
      if (element.id == rowData.id) {
        this.selectedGamInventoryFormat['selectedData'].splice(index, 1);
      }
      index++;
    })
    this.availableInventoryFormat.forEach(element => {
      if (element.id == rowData.id) {
        element.checked = false;
      }
      index++;
    })
  }

  toggleSelectionGamInventoryFormat(event, data) {
    this.selectedGamInventoryFormat['selectedData'].forEach(element => {
      if (element.name == data.name) {
        element.name = data.name;
        element.id = data.id;
        element.checked = event.checked;
      }
    })
  }
  selectedUserDomain = [];
  addUserDoamin(userDomainIncluded, userDomainExcluded) {
    // console.log("userDomainIncluded   :", userDomainIncluded, "userDomainExcluded    :", userDomainExcluded)
    this.selectedUserDomain = [];
    userDomainIncluded.forEach(element => {
      this.selectedUserDomain.push({ 'name': element, 'id': element, 'checked': true })
    });

    userDomainExcluded.forEach(element => {
      this.selectedUserDomain.push({ 'name': element, 'id': element, 'checked': false })
    });
  }

  removeUserDomain(rowData) {

    if (rowData.checked == true) {
      let index = 0; let index2 = 0
      this.selectedUserDomain.forEach(element => {
        if (element.id == rowData.id) {
          this.selectedUserDomain.splice(index, 1);
        }
        index++;
      })
      this.userDomainIncluded.forEach(element => {
        if (element == rowData.id) {
          this.userDomainIncluded.splice(index2, 1);
        }
        index2++;
      });
    }
    if (rowData.checked == false) {
      let index = 0; let index2 = 0
      this.selectedUserDomain.forEach(element => {
        if (element.id == rowData.id) {
          this.selectedUserDomain.splice(index, 1);
        }
        index++;
      })
      this.userDomainExcluded.forEach(element => {
        if (element == rowData.id) {
          this.userDomainExcluded.splice(index2, 1);
        }
        index2++;
      });
    }
  }
  sampleList = [];
  changeOptionList(dynamicArray) {
    // console.log("dynamicArray   :", dynamicArray)
    this.sampleValueList.forEach(element => {

      if (element.id == dynamicArray.controls.keyValuePairControl.value) {
        // this.sampleList=element.sampleList;
        dynamicArray.controls.sampleValueList.patchValue(element.sampleList)
      }

    })

  }

  removeSpace(control: any) {
    if (control.value) {
      control.patchValue(control.value.trim())
    }
  }

  targetingJsonSave() {
    let request = {
      selectedAvailableVideoPositioning: this.selectedAvailableVideoPositioning,
      selectedGamAdUnits: this.selectedGamAdUnits,
      selectGamPlacement: this.selectGamPlacement,
      customTargeting: JSON.stringify(this.customTargeting.value),
      selectGamGeograhy: this.selectGamGeograhy,
      selectAvailableGamDeviceCategory: this.selectAvailableGamDeviceCategory,
      selectavailableGamTargetingOS: this.selectavailableGamTargetingOS,
      selectGamTargetingOS: this.selectGamTargetingOS,
      selectGamBrowswerLang: this.selectGamBrowswerLang,
      selectGamDeviceCapability: this.selectGamDeviceCapability,
      selectGAMDeviceManufacturer: this.selectGAMDeviceManufacturer,
      selectGamDeviceBandwidth: this.selectGamDeviceBandwidth,
      selectGamMobileCarrier: this.selectGamMobileCarrier,
      selectedUserDomain: this.selectedUserDomain,
      selectedGamInventoryType: this.selectedGamInventoryType,
      selectedGamInventoryFormat: this.selectedGamInventoryFormat
    }
    return request;
  }
  deviceCategorySearch = '';
  deviceCategoryIncludeExclude;

  fileName; fileData
  fileChangeEvent(event: any): void {
    if (event.target.files.length > 0) {
      this.fileName = event.target.files[0].name;
      this.fileData = event.target.files[0];
      let instanceOfFileReader = new FileReader();
    }
  }
  onNavigate(url) {
    if (!url.match(/^https?:\/\//i)) {
      url = 'http://' + url;
  }
  return window.open(url, '_blank');
  }

  onChangeAdType(adType) {
    if (adType == 'VIDEO') {
      this.DisplayVASTFlag = true;
    } else {
      this.DisplayVASTFlag = false;
    }
  }

  validateField(fieldControl) {
    if (fieldControl.value > 100) {
      fieldControl.setErrors({ 'limitExceed': true })
    } else {
      fieldControl.clearValidators();
      fieldControl.updateValueAndValidity();
    }
  }

  validateAddFreaquencyCap(formArray) {
    this.markFormArrayTouched(formArray);
    if (formArray.status != 'INVALID') {
      this.addGamFrequencyCap();
    }
  }
  validateDynamicField(fieldControl1, fieldControl2) {
    let freqCap = fieldControl2.value['id']
    if (fieldControl1.value != "") {
      if (fieldControl1.value > 120 && freqCap == 'minute') {
        fieldControl1.setErrors({ 'limitExccedMinute': true })
      } else if (fieldControl1.value > 48 && freqCap == 'hour') {
        fieldControl1.setErrors({ 'limitExccedHour': true })
      } else if (fieldControl1.value > 14 && freqCap == 'day') {
        fieldControl1.setErrors({ 'limitExccedDay': true })
      } else if (fieldControl1.value > 26 && freqCap == 'week') {
        fieldControl1.setErrors({ 'limitExccedWeek': true })
      } else if (fieldControl1.value > 6 && freqCap == 'month') {
        fieldControl1.setErrors({ 'limitExccedMonth': true })
      } else {
        fieldControl1.clearValidators();
        fieldControl1.updateValueAndValidity();
      }
    }
  }
  validateWeekDays(event, indexTimeArray, IndexDayArray, dateTimeformGroup, timeZoneStartTimeControl, timeZoneEndTimeControl) {

    let OuterIndex = 0; let index = 0;
    let startTimeD = (timeZoneStartTimeControl.value['id']).split(":")
    let startTime = (Number)(startTimeD[0] + startTimeD[1]);
    let endTimeD = (timeZoneEndTimeControl.value['id']).split(":")
    let endTime = (Number)(endTimeD[0] + endTimeD[1]);
    dateTimeformGroup.controls.forEach(TimeControls => {
      let InnerIndex = 0;
      if (OuterIndex != indexTimeArray) {
        let startTimeC = (TimeControls.controls.timeZoneStartTime.value['id']).split(":")
        let startTimeCurrent = (Number)(startTimeC[0] + startTimeC[1]);
        let endTimeC = (TimeControls.controls.timeZoneEndTime.value['id']).split(":")
        let endTimeurrent = (Number)(endTimeC[0] + endTimeC[1]);

        let timeRepeatFlag = false;
        if (((startTime > startTimeCurrent || startTime == startTimeCurrent) &&
          (startTime < endTimeurrent)) &&
          (endTime > startTimeCurrent)
        ) {
          // && (endTime < endTimeurrent || endTime == endTimeurrent)
          timeRepeatFlag = true;
        }
        let timeZoneWeeksDaysExclude = TimeControls.controls.timeZoneWeeksDaysExclude.value
        // console.log("timeRepeatFlag   :", timeRepeatFlag)

        TimeControls.controls.daysArr.controls.forEach(DayControls => {
          if (IndexDayArray == InnerIndex) {
            if (DayControls.value && event.checked) {
              index++; let dayControl; let controlField;
              dayControl = dateTimeformGroup.at(indexTimeArray).get('daysArr');
              if (IndexDayArray == 0) { controlField = dayControl.get('0') }
              if (IndexDayArray == 1) { controlField = dayControl.get('1') }
              if (IndexDayArray == 2) { controlField = dayControl.get('2') }
              if (IndexDayArray == 3) { controlField = dayControl.get('3') }
              if (IndexDayArray == 4) { controlField = dayControl.get('4') }
              if (IndexDayArray == 5) { controlField = dayControl.get('5') }
              if (IndexDayArray == 6) { controlField = dayControl.get('6') }

              if (timeRepeatFlag || timeZoneWeeksDaysExclude) {
                controlField.setErrors({ "timeOverLap": true })
              }
              else {
                controlField.clearValidators();
                controlField.updateValueAndValidity();
              }
            }
          }
          InnerIndex++;
        })
      }
      OuterIndex++
    })
  }

  excludeWeekDaysValidation(event, timeZoneStartTime, timeZoneEndTime) {
    if (event.checked) {
      timeZoneStartTime.patchValue({ id: "00:00", name: "12:00 AM" });
      timeZoneEndTime.patchValue({ id: "00:00", name: "12:00 AM" });
    }
  }

  validateTime(starttimeControl, endTimeControl) {
    let startTime = starttimeControl.value['id'].split(':');
    let endTime = endTimeControl.value['id'].split(':');;
    if (startTime[0] > endTime[0]) {
      endTimeControl.setErrors({ 'overLapTime': true })
    } else if (startTime[0] == endTime[0] && (startTime[1] == endTime[1] || startTime[1] > endTime[1])) {
      endTimeControl.setErrors({ 'overLapTime': true })
    }
    else {
      endTimeControl.clearValidators();
      endTimeControl.updateValueAndValidity();
    }

  }


  onChangeSecondaryTrafficker() {
    this.isGamTrafficker = this.selectedGamSecTrafficker.find(x => x.id == this.selectedGamOrderTrafficker.id);
  }
  onChangeSalesperson() {
    this.isprimarySalesPersonPresent = false;
    if (this.selectedGamSalesperson == undefined && this.selectedGamSecSalesPerson.length > 0)
      this.isprimarySalesPersonPresent = true;
    this.isGamSalesperson = this.selectedGamSecSalesPerson.find(x => x.id == this.selectedGamSalesperson.id);
  }

  initializeDynamicCreatives() {
    this.addCreativeFormDynamic = this.formBuilder.group({
      creativeArray: this.formBuilder.array([this.initializationAddCreativeFormControls()])
    });
  }
  addCreativeDynamic() {
    let DynamicArray = this.addCreativeFormDynamic.get('creativeArray') as FormArray;
    DynamicArray.push(this.initializationAddCreativeFormControls());
  }


  getAvailablePresetList() {
    this.dataFetchServ.getPresetTargetingList().subscribe(response => {
      this.availablePresetList = response['data']
    })
  }

  onChangePresetTargeting(presetTargetingControl) {
    let value = presetTargetingControl.value
    this.dataFetchServ.getSelectedPresetTargeting(value['id']).subscribe(response => {
      if (response['status'] == 1) {
        let targeting_json: any = JSON.parse(response['data'][0]['targeting_json'])
        let i = 0;
        this.selectedAvailableVideoPositioning = targeting_json['selectedAvailableVideoPositioning'];
        let selectedId = targeting_json['selectedAvailableVideoPositioning']['selectedData'].map(item => item['id']);
        this.availableVideoPositioning.forEach(element => {
          if (element['id'] == selectedId[i]) {
            element.checked = true;
            i++;
          }
        })


        let event = { index: 1 };
        this.availableGamAdUnits = [];
        this.availableGamPlacement = [];
        this.selectedGamAdUnits = targeting_json['selectedGamAdUnits'];
        this.selectGamPlacement = targeting_json['selectGamPlacement'];

        this.availableGamGeograhy = [];
        this.selectGamGeograhy = targeting_json['selectGamGeograhy'];

        this.customTargetingValid = false;
        for (let i = this.getCustomTargetingArrayArray().length; i >= 0; i--) {
          this.getCustomTargetingArrayArray().removeAt(i);
        }
        this.getCustomTargetingArrayArray().push(this.initializationOfCustomTargeting())
        this.selectedCustomTargeting = targeting_json['customTargeting']['customTargetingArray']
        // console.log("targeting_json['customTargeting']['customTargetingArray']  :",targeting_json['customTargeting']['customTargetingArray'])

        if (this.selectedCustomTargeting.length > 0) {
          this.customTargetingValid = true;

          if (this.selectedCustomTargeting.length == 1) {
            if (this.selectedCustomTargeting[0]['isAnyOf'] == '' || this.selectedCustomTargeting[0]['keyValuePairControl'] == '' ||
              this.selectedCustomTargeting[0]['sampleValue'] == '' || this.selectedCustomTargeting[0]['sampleValueList'] == '') {
              this.customTargetingValid = false;
            }
          }
          for (let i = 0; i < targeting_json['customTargeting']['customTargetingArray'].length - 1; i++) {
            this.getCustomTargetingArrayArray().push(this.initializationOfCustomTargeting())
          }

          setTimeout(() => {
          }, 23000);
        }




        this.availableGamDeviceCategory = [];
        this.selectAvailableGamDeviceCategory = targeting_json['selectAvailableGamDeviceCategory'];

        this.availableGamTargetingOS = [];
        this.selectavailableGamTargetingOS = targeting_json['selectavailableGamTargetingOS'];

        this.availableTargetingOS = [];
        this.selectGamTargetingOS = targeting_json['selectGamTargetingOS'];

        this.availableGamTargetingOS = [];
        this.selectGamTargetingOS = targeting_json['selectGamTargetingOS'];

        this.availableTargetingOS = [];
        this.selectGamBrowswerLang = targeting_json['selectGamBrowswerLang'];

        this.availableGamDeviceCapability = [];
        this.selectGamDeviceCapability = targeting_json['selectGamDeviceCapability'];

        this.availableGamDeviceManufaturere = [];
        this.selectGAMDeviceManufacturer = targeting_json['selectGAMDeviceManufacturer'];

        this.availableGamDeviceBandwidth = [];
        this.selectGamDeviceBandwidth = targeting_json['selectGamDeviceBandwidth'];

        this.activeIndex = this.lastIndex--;
        this.closeAllAccordionTabs()
        this.availableGammobileCarrier = [];
        this.selectGamMobileCarrier = targeting_json['selectGamMobileCarrier'];

        this.selectedUserDomain = targeting_json['selectedUserDomain'];

        this.selectedGamInventoryType = targeting_json['selectedGamInventoryType'];

        this.availableInventoryFormat = [];
        this.selectedGamInventoryFormat = targeting_json['selectedGamInventoryFormat'];
        selectedId = targeting_json['selectedGamInventoryFormat']['selectedData'].map(item => item['id']);
        selectedId.forEach(element => {
          this.availableInventoryFormat.forEach(nested => {
            if (nested['id'] == element) {
              nested.checked = true;
            }
          });
        })
      }
    });
  }

  setSelctedId(availableList, selectedList) {
    let selectedId = selectedList.map(item => item['id']);
    let i = 0;
    availableList.forEach(element => {
      if (element['id'] == selectedId[i]) {
        element.checked = true;
        i++;
      }
    })
  }

  savePresetTargeting(presetTargetingName) {
    let targeting_json = {
      selectedAvailableVideoPositioning: this.selectedAvailableVideoPositioning,
      selectedGamAdUnits: this.selectedGamAdUnits,
      selectGamPlacement: this.selectGamPlacement,
      customTargeting: this.customTargeting.value,
      selectGamGeograhy: this.selectGamGeograhy,
      selectAvailableGamDeviceCategory: this.selectAvailableGamDeviceCategory,
      selectavailableGamTargetingOS: this.selectavailableGamTargetingOS,
      selectGamTargetingOS: this.selectGamTargetingOS,
      selectGamBrowswerLang: this.selectGamBrowswerLang,
      selectGamDeviceCapability: this.selectGamDeviceCapability,
      selectGAMDeviceManufacturer: this.selectGAMDeviceManufacturer,
      selectGamDeviceBandwidth: this.selectGamDeviceBandwidth,
      selectGamMobileCarrier: this.selectGamMobileCarrier,
      selectedUserDomain: this.selectedUserDomain,
      selectedGamInventoryType: this.selectedGamInventoryType,
      selectedGamInventoryFormat: this.selectedGamInventoryFormat
    }
    let request = {
      name: presetTargetingName.value,
      targeting_json: JSON.stringify(targeting_json)
    };
    this.dataFetchServ.savePresetTargeting(request).subscribe(response => {
      // console.log("response   :", response)
      if (response['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(response['data']);
        return;
      }
      if (response['status'] == 1) {
        this.toastService.displayToast({
          severity: 'success',
          summary: 'Campaign Management',
          detail: 'Preset Targeting created successfully'
        });
        this.getAvailablePresetList();
      }
    })
  }

  calculateGAMTotalValue(lineItem,rateType,discountType)
  {    
    this.discount_value = 0;
    this.li_total_value = 0;
    this.final_total_value = 0;
    if((lineItem === 'standard' || lineItem === 'bulk') && (rateType === "CPM" || rateType === "Viewable CPM")) {
      this.li_total_value = (this.addGamLineItemForm.controls['quantity'].value / 1000) * this.addGamLineItemForm.controls['rate1'].value;
      if(this.addGamLineItemForm.controls['standardDiscount'].value !== 0)
      {
        if(discountType === 'PERCENTAGE')
        {
          this.discount_value = this.addGamLineItemForm.controls['standardDiscount'].value / 100 * this.li_total_value;
        }
        else
        {
          this.discount_value = this.addGamLineItemForm.controls['standardDiscount'].value;
        }
      }
      this.final_total_value = this.li_total_value - this.discount_value;
     } else if((lineItem === 'standard' || lineItem === 'bulk') && rateType === "CPC") {
      this.li_total_value = this.addGamLineItemForm.controls['quantity'].value * this.addGamLineItemForm.controls['rate1'].value;

      if(this.addGamLineItemForm.controls['standardDiscount'].value !== 0)
      {
        if(discountType === 'PERCENTAGE')
        {          
          this.discount_value = this.addGamLineItemForm.controls['standardDiscount'].value / 100 * this.li_total_value;      
        }
        else
        {
          this.discount_value = this.addGamLineItemForm.controls['standardDiscount'].value;
        }
      }
      this.final_total_value = this.li_total_value - this.discount_value;
     } else if((lineItem === 'sponsorship' || lineItem === 'network') && rateType === "CPD") {
       let startDate= moment(this.addGamLineItemForm.controls['startDate'].value, 'YYYYMMDD');
       let endDate = moment(this.addGamLineItemForm.controls['endDate'].value, 'YYYYMMDD')
   
      this.li_total_value = this.addGamLineItemForm.controls['rate'].value * (endDate.diff(startDate, 'days') + 1);
      if(this.addGamLineItemForm.controls['discount'].value !== 0)
      {
        if(discountType === 'PERCENTAGE')
        {          
          this.discount_value = this.addGamLineItemForm.controls['discount'].value / 100 * this.li_total_value;
        }
        else
        {
          this.discount_value = this.addGamLineItemForm.controls['discount'].value;
        }
      }
      this.final_total_value = this.li_total_value - this.discount_value;

     } else if(lineItem === 'price_priority' && rateType === "CPD") {
      let startDate= moment(this.addGamLineItemForm.controls['startDate'].value, 'YYYYMMDD');
      let endDate = moment(this.addGamLineItemForm.controls['endDate'].value, 'YYYYMMDD')
  
     this.li_total_value = this.addGamLineItemForm.controls['rate'].value * (endDate.diff(startDate, 'days') + 1);
     if(this.addGamLineItemForm.controls['standardDiscount'].value !== 0)
     {
       if(discountType === 'PERCENTAGE')
       {          
         this.discount_value = this.addGamLineItemForm.controls['standardDiscount'].value / 100 * this.li_total_value;
       }
       else
       {
         this.discount_value = this.addGamLineItemForm.controls['standardDiscount'].value;
       }
     }
     this.final_total_value = this.li_total_value - this.discount_value;
  
    } else if(lineItem === "price_priority" && rateType === "CPM") {
      if(this.selectedPricePriorityLimitValue === 'LIFETIME')
      {
        this.li_total_value = (this.addGamLineItemForm.controls['dailyQtyValuePricePriority1'].value / 1000) * this.addGamLineItemForm.controls['pricePriorityRate'].value;
      } else if(this.selectedPricePriorityLimitValue === 'DAILY')
      {
        let startDate= moment(this.addGamLineItemForm.controls['startDate'].value, 'YYYYMMDD');
        let endDate = moment(this.addGamLineItemForm.controls['endDate'].value, 'YYYYMMDD')
        this.li_total_value = ((this.addGamLineItemForm.controls['dailyQtyValuePricePriority1'].value / 1000) * this.addGamLineItemForm.controls['pricePriorityRate'].value) * (endDate.diff(startDate, 'days') + 1);
      }

      if(this.addGamLineItemForm.controls['standardDiscount'].value !== 0)
      {
        if(discountType === 'PERCENTAGE')
        {          
          this.discount_value = this.addGamLineItemForm.controls['standardDiscount'].value / 100 * this.li_total_value;
        }
        else
        {
          this.discount_value = this.addGamLineItemForm.controls['standardDiscount'].value;
        }
      }
      this.final_total_value = this.li_total_value - this.discount_value;

     } else if(lineItem === "price_priority" && rateType === "CPC") {
      if(this.selectedPricePriorityLimitValue === 'LIFETIME')
      {
        this.li_total_value = this.addGamLineItemForm.controls['dailyQtyValuePricePriority1'].value * this.addGamLineItemForm.controls['pricePriorityRate'].value;
      } else if(this.selectedPricePriorityLimitValue === 'DAILY')
      {
        let startDate= moment(this.addGamLineItemForm.controls['startDate'].value, 'YYYYMMDD');
        let endDate = moment(this.addGamLineItemForm.controls['endDate'].value, 'YYYYMMDD')
        this.li_total_value = (this.addGamLineItemForm.controls['dailyQtyValuePricePriority1'].value * this.addGamLineItemForm.controls['pricePriorityRate'].value) * (endDate.diff(startDate, 'days') + 1);
      }

      if(this.addGamLineItemForm.controls['standardDiscount'].value !== 0)
      {
        if(discountType === 'PERCENTAGE')
        {          
          this.discount_value = this.addGamLineItemForm.controls['standardDiscount'].value / 100 * this.li_total_value;
        }
        else
        {
          this.discount_value = this.addGamLineItemForm.controls['standardDiscount'].value;
        }
      }
      this.final_total_value = this.li_total_value - this.discount_value;
      
     }
  }

  onChangeLineItemDate()
  {
    this.lineEndMinDate = this.addGamLineItemForm.controls['startDate'].value;
    this.lineStartMaxDate = this.addGamLineItemForm.controls['endDate'].value;
  }

  scrollToCreative()
  {
      setTimeout(() => {
        this.myScrollContainer.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start'});
      },0);
  }

  dbOrderId
  setOrderDetails(formData) {
    // console.log("formDataa    :",formData)
    this.formIdentity='order';
    let campaign_json = JSON.parse(formData.campaign_json);

    let agency = (this.selectedGamAgencies != undefined) ? this.selectedGamAgencies.id : "";
    let salesperson = (this.selectedGamSalesperson != undefined) ? this.selectedGamSalesperson.id : "";
    let labels=[],contact=[],secTrafficker=[],secSalesperson=[];
    campaign_json.labels.forEach(element => {labels.push({id:element})});
    campaign_json.contact.forEach(element => {contact.push({id:element})});
    campaign_json.secTrafficker.forEach(element => {secTrafficker.push({id:element})});
    campaign_json.secSalesperson.forEach(element => {secSalesperson.push({id:element})});
    // console.log("labels   :",labels)
    // console.log("campaign_json    :",campaign_json)
    this.dbOrderId = formData.id;
    this.gamOrderName = campaign_json.orderName;
    this.selectedGamAdvertisers = {id: campaign_json.advertiserId}//campaign_json.advertiserId
    this.availableGamAdContacts = this.gamOrderData['contact'].filter(item => item['companyId'] === this.selectedGamAdvertisers['id'])
   
    this.selectedGamOrderTrafficker = {id: campaign_json.traffickerId}
    this.selectedLabels = labels;
    this.selectedGamContacts = contact;
    this.selectedGamAgencies  = {id: campaign_json.agency};

    this.selectedGamSecTrafficker  = secTrafficker;
    this.selectedGamSalesperson =  {id: campaign_json.salesperson};
    this.selectedGamSecSalesPerson = secSalesperson;

    this.gamOrderCustomFields  = campaign_json.hasOwnProperty('customFiels')?campaign_json.customFiels:null;
    this.gamOrderNote = campaign_json.hasOwnProperty('note')?campaign_json.note:null;
    this.gamOrderPoNumber = campaign_json.hasOwnProperty('poNumber')?campaign_json.poNumber:null;
  }


  updateGamOrders(orderName, gamAdvertiser, gamTrafficker) {
    this.isGamTrafficker
    this.isGamSalesperson
    this.isprimarySalesPersonPresent
    if (orderName.control.status != 'VALID' || gamAdvertiser.control.status != 'VALID' ||
      gamTrafficker.control.status != 'VALID' || this.isGamTrafficker != undefined || this.isGamSalesperson != undefined) {
      return false;
    }

    let agency = (this.selectedGamAgencies != undefined) ? this.selectedGamAgencies.id : "";
    let salesperson = (this.selectedGamSalesperson != undefined) ? this.selectedGamSalesperson.id : "";
    let orderDetails = {
      orderName: this.gamOrderName,
      advertiserId: this.selectedGamAdvertisers.id,
      traffickerId: this.selectedGamOrderTrafficker.id,
      labels: this.selectedLabels.map(item => item['id']),
      contact: this.selectedGamContacts.map(item => item['id']),
      agency: agency,
      secTrafficker: this.selectedGamSecTrafficker.map(item => item['id']),
      salesperson: salesperson,
      secSalesperson: this.selectedGamSecSalesPerson.map(item => item['id']),
      poNumber: this.gamOrderPoNumber,
      note: this.gamOrderNote,
      customFiels: this.gamOrderCustomFields
    }

    const line_item_json = {}
    let request = {
      id: this.dbOrderId,
      campaign_json: JSON.stringify(orderDetails),
      ad_server_id: 1,
      name: this.gamOrderName,
      server_id: "",
      line_item_json: JSON.stringify(line_item_json),
      approval_status: "Pending for approval",
      status: 'Active',
      created_by: this.currentUser,
      approved_by: 1,
      advertiser_id: this.selectedGamAdvertisers.id,
      advertiser_name: this.selectedGamAdvertisers.name
    }

    // console.log("request   :", request)
    this.dataFetchServ
      .updateGamOrders(request)
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['data']);
          return;
        }
        if (data['status'] === -1) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Error',
            detail: 'Order name already push to GAM.'
          });
          console.log(data['data']);
          return;
        }
        if (data['status'] == 1) {
          this.toastService.displayToast({
            severity: 'success',
            summary: 'Campaign Management',
            detail: 'Order updated successfully'
          });

          // this.gamCampaignId = data['id']
          // this.showLineItem = true;
        }
        //  console.log("status",data);


      });

  }

  retrievedTaragetingData:any;
  editLineItemFlag=false;
  retrievedLineItemId:any;
     
  setLineItemDetails(formData){
    // console.log("formData   :",formData);
    this.formIdentity='lineItem';
    // console.log("json    :",JSON.parse(formData.line_item_json))
    let lineItemData = JSON.parse(formData.line_item_json)
    this.retrievedTaragetingData = lineItemData.targeting_json;
    // console.log("targetingjson    :",lineItemData.targeting_json)
    let timeZomeArray = lineItemData.timeZomeArray;
    let i=1;
    timeZomeArray.forEach(element => {
      if(i!=1){
        this.addGamWeekDays();
      }i++;
    });
    let frequencyCapArray = lineItemData.frequencyCapArray;
    i=0;
    frequencyCapArray.forEach(element => {
      if(i!=1){
        this.addGamFrequencyCap();
      }i++;
    });
    this.editLineItemFlag = true;
    this.retrievedLineItemId = formData.id;
    let startDate = formData.line_item_json.startDate
    let endDate = formData.line_item_json.endDate
    this.selectedAvailableVideoPositioning= this.retrievedTaragetingData.selectedAvailableVideoPositioning;
    this.selectedGamAdUnits = this.retrievedTaragetingData.selectedGamAdUnits;
    this.selectGamPlacement = this.retrievedTaragetingData.selectGamPlacement;
    this.selectGamGeograhy = this.retrievedTaragetingData.selectGamGeograhy;
    this.selectAvailableGamDeviceCategory = this.retrievedTaragetingData.selectAvailableGamDeviceCategory;
    this.selectavailableGamTargetingOS = this.retrievedTaragetingData.selectavailableGamTargetingOS;
    this.selectGamTargetingOS = this.retrievedTaragetingData.selectGamTargetingOS;
    this.selectGamBrowswerLang = this.retrievedTaragetingData.selectGamBrowswerLang;
    this.selectGamDeviceCapability = this.retrievedTaragetingData.selectGamDeviceCapability;
    this.selectGAMDeviceManufacturer = this.retrievedTaragetingData.selectGAMDeviceManufacturer;
    this.selectGamDeviceBandwidth = this.retrievedTaragetingData.selectGamDeviceBandwidth;
    
    this.selectGamMobileCarrier = this.retrievedTaragetingData.selectGamMobileCarrier;
    this.selectedUserDomain = this.retrievedTaragetingData.selectedUserDomain;
    this.selectedGamInventoryType = this.retrievedTaragetingData.selectedGamInventoryType;
    this.selectedGamInventoryFormat = this.retrievedTaragetingData.selectedGamInventoryFormat;
    
    let customTargeting =   JSON.parse(this.retrievedTaragetingData.customTargeting);
    // console.log("customTargeting['customTargetingArray']   :",customTargeting['customTargetingArray'])
    customTargeting['customTargetingArray'].forEach(element => {
      if(i!=1){
        this.getCustomTargetingArrayArray().push(this.initializationOfCustomTargeting())
      }i++;
    });
    let lineItemJson = JSON.parse(formData.line_item_json);
    let sDateArr = lineItemJson.startDate.split('T')
    let eDateArr = lineItemJson.endDate.split('T')
    lineItemJson.startDate = new Date(sDateArr[0]);
    lineItemJson.endDate = new Date(eDateArr[0]);
    // console.log("lineItemJson.lineItemType['value']    :",lineItemJson.lineItemType['value'])
    this.addGamLineItemForm.controls.lineItemType.patchValue(lineItemJson.lineItemType)


    if(lineItemJson.lineItemType['value']=='standard'){
      this.selectedLineItem = lineItemJson.lineItemType;
      // this.addGamLineItemForm.controls.lineItemTypePriority.patchValue(lineItemJson.lineItemTypePriority)
      // this.addGamLineItemForm.controls.lineItemTypePriorityValue.patchValue(lineItemJson.lineItemTypePriorityValue);
      // this.addGamLineItemForm.controls.standardQtyRateValue.patchValue(lineItemJson.pricePriorityLimitDailyRateValue);
      this.standardQtyRateValue = lineItemJson.pricePriorityLimitDailyRateValue;
      this.selectedLineItemPriority = lineItemJson.lineItemTypePriority;
      this.selectedLineItem['priority'] = lineItemJson.lineItemTypePriorityValue;
    }

    if(lineItemJson.lineItemType['value']=='standard' || lineItemJson.lineItemType['value']=='bulk' 
    || lineItemJson.lineItemType['value']=='price_priority' ){
      this.selectedDiscountValue = lineItemJson.standardDiscountValue;
    }
    
    this.lineItemTypePriorityValue= lineItemJson.lineItemTypePriorityValue;
    this.addGamLineItemForm.patchValue(lineItemJson);
    this.final_total_value = lineItemJson.liTotalValue;
    this.selectedQtyValues = lineItemJson.quantityValues;
    // console.log("this.selectedQtyValues    :",this.selectedQtyValues)
    // console.log("this.addGamLineItemForm  :",this.addGamLineItemForm.value)
    if(lineItemJson.lineItemType['value']=='bulk'){
         this.selectedQtyValues = lineItemJson.qtyBulkValue;
        this.standardQtyRateValue = lineItemJson.qtyBulkValue['rateValues'];
      }
  if(lineItemJson.lineItemType['value']=='network'){
       this.selectedDiscountValue = lineItemJson.discountValues
      }
      if(lineItemJson.lineItemType['value']=='price_priority'){
      this.selectedPricePriorityLimitValue = lineItemJson.pricePriorityLimitValue;
      this.selectedQtyValues = lineItemJson.dailyQtyValuePricePriority
      }
      if(lineItemJson.lineItemType['value']=='house'){
        this.selectedRateValues = lineItemJson.rateValueHouse;
      }
      if(lineItemJson.lineItemType['value']=='sponsorship'){
        this.lineItemTypePriorityValue = lineItemJson.lineItemType['priority'];
        this.selectedDiscountValue = lineItemJson.discountValues;
      }
      
  }
  selectedLineItemPriority;
  standardQtyRateValue;
  lineItemTypePriorityValue = '4';
  selectedRateValues;

  updateLineItems() {
    //  console.log("Form Details   :",JSON.stringify(this.addGamLineItemForm.value));
    if (this.addGamLineItemForm.status == 'VALID') {
      console.log("Validd")
    } else {
      console.log("InnnnValidd")
      // return;
    }

    if (this.selectGamPlacement.selectedData.length == 0 || this.selectedGamAdUnits.selectedData.length == 0) {
      // return;
    }
    this.addGamLineItemForm.controls.targeting_json.patchValue(this.targetingJsonSave())
    let request = {
      id:this.retrievedLineItemId,
      line_item_json: JSON.stringify(this.addGamLineItemForm.value),
      name: this.addGamLineItemForm.controls['lineItemName'].value,
    }
    this.dataFetchServ
      .updateGamLineItems(request)
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          // console.log(data['data']);
          return;
        }
        if (data['status'] === -1) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Error',
            detail: 'Line Item already pushed to GAM.'
          });
          console.log(data['data']);
          return;
        }
        if (data['status'] == 1) {
          this.toastService.displayToast({
            severity: 'success',
            summary: 'Campaign Management',
            detail: 'Line Item updated successfully'
          });
        }
      });
  }

  editCreativeFlag=false;
  setCreativeDetails(formData){
    // console.log("formData   :",formData);
    // console.log("this.addCreativeFormDynamic   :",this.addCreativeFormDynamic)
    this.formIdentity='creative';
    this.editCreativeFlag=true;
    // console.log("json    :",JSON.parse(formData.creative_json))
    let DynamicArray = this.addCreativeFormDynamic.get('creativeArray') as FormArray;
    let addCreativeForm = DynamicArray.at(0) as FormGroup;
    let CreativeJson = JSON.parse(formData.creative_json);
    addCreativeForm.patchValue(CreativeJson)
    // console.log("CreativeJson  :",CreativeJson);
    addCreativeForm.controls.type.setValue(CreativeJson['creativetype']);
    addCreativeForm.controls.name.setValue(CreativeJson['creativeName']);
    this.creative_server_id = formData.creative_server_id; 
    this.retrievedCreativeId = formData.id;
    CreativeJson.hasOwnProperty('templateType')?addCreativeForm.controls.type.setValue(CreativeJson['creativetype']):"";
    CreativeJson.hasOwnProperty('destination_url')?addCreativeForm.controls.url.setValue(CreativeJson['destination_url']):"";
    if(CreativeJson.hasOwnProperty('templateType')&&CreativeJson['templateId']=='10000680'){
      addCreativeForm.controls.width.setValue(CreativeJson['imageWidth']);
      addCreativeForm.controls.height.setValue(CreativeJson['imageHeight']);
    }

    this.dataFetchServ.getCreativeAsset(formData.id).subscribe(data => {
      // console.log("data   :",data)
      this.CreativeAssetData = data;
    })
    // third_party_trackingArray: this.formBuilder.array([this.initializationOfthird_party_tracking()]),
    // userDefinedVariableArray: this.formBuilder.array([this.initializationUserDefinedVariable()]),
    // userDefinedVariableArray2: this.formBuilder.array([this.initializationUserDefinedVariable2()]),
   //-----UI for HTML5 animated changed need work --changed unknowingly---
  } 
 
  updateCreative(addCreativeForm){
    // console.log("addCreativeForm   :",addCreativeForm);
    if (this.creative_server_id!=0) {
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Error',
        detail: 'Creative already pushed to GAM.'
      });
      return;
    }
    this.createCreative(addCreativeForm);
  }

  imagePreview(identity:any){
    this.data = null;
      this.CreativeAssetData['data'].forEach(element => {
       if( element.creative_asset_identity==identity){
         this.data = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/png;base64, '+element.creative_asset);
       }
      });
    this.image=true;
  }

}