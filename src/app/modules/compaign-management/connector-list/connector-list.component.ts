import { Component, OnInit } from '@angular/core';
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
  FormArray
} from '@angular/forms';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { FetchApiDataService } from '../fetch-api-data.service';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { Subscription } from 'rxjs';
import { CommonLibService } from 'src/app/_services';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
@Component({
  selector: 'ym-connector-list',
  templateUrl: './connector-list.component.html',
  styleUrls: ['./connector-list.component.scss']
})
export class ConnectorListComponent implements OnInit {

  dynamicFormControlArray = [];
  private appConfigObs: Subscription;
  appConfig: object = {};
  currentUser:any;
  connectionTableData: TreeNode[];
  connectionTableJson: Object;
  flatTableColumnDef: any[];
  connectionFlag:boolean=true;
  connectionId:any;
  formIdentity:any;
  formData:any;

  constructor(private _formBuilder: FormBuilder, private dataFetchServ: FetchApiDataService,
    private toastService: ToastService,private appConfigService: AppConfigService,
    private libServ: CommonLibService,private confirmationService: ConfirmationService,private router: Router,
    private currentActivatedRoute: ActivatedRoute,private _titleService: Title,) { }

  ngOnInit() { 
    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      console.log("appConfig   :", appConfig)
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        this.currentUser = this.appConfig['user']['id'];
        this._titleService.setTitle(this.appConfig['displayName']);
      }
    });

    this.flatTableColumnDef = [
      {
        field: 'connection_name',
        displayName: 'Connection Name',
        format: '',
        width: '170',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'ad_server_name',
        displayName: 'Ad Server Name',
        format: '',
        width: '170',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'status',
        displayName: 'Status',
        format: '',
        width: '110',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'edit',
        displayName: 'Details',
        width: '70',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'update',
        displayName: 'Update',
        width: '70',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'delete',
        displayName: 'Delete',
        width: '70',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'status',
        displayName: 'Created At',
        format: '',
        width: '110',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'status',
        displayName: 'Updated At',
        format: '',
        width: '110',
        value: '',
        condition: '',
        columnType: 'dimensions',
        isClearSearch: true,
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
    ];

    this.connectionTableJson = {
      page_size: 50,
      page: 0,
      lazy: false,
      loading: false,
      export: false,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '300px',
      totalRecords: 1000,
      columns: this.flatTableColumnDef.slice(0),
      selectedColumns: this.flatTableColumnDef.slice(0),
      frozenCols: [this.flatTableColumnDef[0]],
      // frozenWidth: '250px',
      frozenWidth:
        this.flatTableColumnDef
          .slice(0, 0)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',

      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: true
    }; 
    this.getConnectionsList();
  }

  getConnectionsList() {
    this.dataFetchServ
    .getConnectionsList()
    .subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['data']);
        return;
      }
      if (data['status'] == 1) {
        
        if (data['data'].length > 0) {
          const tableData = data['data'];
          const arr = [];
          tableData.forEach((row: object) => {
            arr.push({
              data: row,
              children: [{ data: {} }]
            });
          });

          console.log("arr====", arr);

          this.connectionTableData = <TreeNode[]>arr;
          this.connectionTableData['totalRecords'] = arr.length;
          this.connectionTableData['loading'] = false;
        }
      }
    })
  }
 
  editUpdateConnection(rowData,formIdentity){
    this.connectionId = rowData.id;
    this.formIdentity = formIdentity;
    this.formData = rowData;
    this.connectionFlag=!this.connectionFlag;
  }


  backOperation(){
    this.connectionFlag=!this.connectionFlag;
    this.getConnectionsList();
  }

  

  deleteConnection(rowData){
    console.log("rowData    :",rowData)
   let connection_json = JSON.parse(rowData.connection_json)
   let applicationName = connection_json.applicationName;
   let networkCode = connection_json.networkCode;
    let request = {
       applicationName: applicationName,
       networkCode: networkCode,
       updated_by: this.currentUser,
       id:rowData.id
    }
    this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?',
      key:'connectionList',
      accept: () => {

        this.dataFetchServ
        .deleteConnectionDeatils(JSON.stringify(request))
        .subscribe(data => {
          if (data['status'] === 0) {
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Server Error',
              detail: 'Please refresh the page'
            });
            console.log(data['data']);
            return;
          }
          if (data['status'] == 1) {
            this.toastService.displayToast({
              severity: 'success',
              summary: 'Campaign Management',
              detail: 'Connection deleted successfully'
            });
            this.getConnectionsList();
          }
        });
      },
      reject: () => {
       return;
      }
    });

     
  }

  childResponse(event){
    if(event=='ConnectionDetail')
    this.confirmationService.confirm({
      message: 'Are you sure that you want to perform this action?',
      key:'connectionList',
      accept: () => {
        this.connectionFlag=true;
      },
      reject: () => {
      }
    });
  }

}
