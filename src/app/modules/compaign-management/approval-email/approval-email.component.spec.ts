import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApprovalEmailComponent } from './approval-email.component';

describe('ApprovalEmailComponent', () => {
  let component: ApprovalEmailComponent;
  let fixture: ComponentFixture<ApprovalEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApprovalEmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApprovalEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
