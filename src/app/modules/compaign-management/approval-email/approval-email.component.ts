import { Component, OnInit } from '@angular/core';
import { FetchApiDataService } from '../fetch-api-data.service';
import { ToastService } from '../../../_services/toast-notification/toast.service';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { Validators,FormControl,FormGroup,FormBuilder,FormArray} from '@angular/forms';


@Component({
  selector: 'ym-approval-email',
  templateUrl: './approval-email.component.html',
  styleUrls: ['./approval-email.component.scss']
})
export class ApprovalEmailComponent implements OnInit {
  public contactEmail: string[];
  public data: any;
  public isValid = true;
  
  addEmailForm: FormGroup;
  emailFormArray: any = [];
  
  constructor(
    private dataFetchServ: FetchApiDataService,
    private toastService: ToastService,
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private formBuilder: FormBuilder,
  ) {
    this.data = this.config.data;
  }

  ngOnInit(): void {
    this.addEmailForm = this.formBuilder.group({
      emailFormArray: this.formBuilder.array([this.createItem()])
    })
  }

  createItem() {
    return this.formBuilder.group({
      approverName: [''],
      contactEmail: ['']
    })
  }

  addItem() {
    this.emailFormArray = this.addEmailForm.get('emailFormArray') as FormArray;
    this.emailFormArray.push(this.createItem());
  }

  onSubmit() {
    console.log(this.addEmailForm.value);
  }

  onRemoved(index){
    if(this.emailFormArray.length > 1)
    {
      this.emailFormArray = this.addEmailForm.get('emailFormArray') as FormArray;
      this.emailFormArray.removeAt(index);
    } else {
      
    }
  }

  chipsValidation(i) {
    this.emailFormArray = this.addEmailForm.get('emailFormArray') as FormArray;
    // var item = (<FormArray>this.addEmailForm.get('contactEmail')).at(0);
    // let 
    console.log("validation",this.emailFormArray.value[i]['contactEmail']);
    this.isValid = true;
    const regexp = new RegExp(
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
    this.emailFormArray.value[i]['contactEmail'].forEach(val => {
      if (!regexp.test(val)) {
        this.isValid = false;
      }
    });
  }
  sendEmail() {
    if (this.isValid) {
      const userInfo = {
        email: this.addEmailForm.value['emailFormArray'],
        campaign:this.data['campaign'],
        start_Date:this.data['start_date'],
        end_Date:this.data['end_date']
      };
      this.dataFetchServ.sendScreenshot(userInfo).subscribe(
        res => {
          const data = res as {};
          if (data['status']) {
            this.toastService.displayToast({
              severity: 'success',
              summary: 'Send Email',
              detail: 'Email sent successfully'
            });
            this.ref.close(null);
          }
        },
        error => {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Something went wrong',
            detail: 'Please refresh page if problem persist contact us'
          });
          this.ref.close(null);
        }
      );
     }
  }

  cancel() {
    this.ref.close(null);
  }



}
