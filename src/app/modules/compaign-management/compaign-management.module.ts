import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FilterContainerModule } from '../common/filter-container/filter-container.module';
import { CompaignManagementRoutes } from './compaign-management.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { SharedModule } from 'src/app/_pipes/shared.module';
import { ChatModule } from '../common/chat/chat.module';
import { TreeTableModule } from 'primeng/treetable';
import { MultiSelectModule } from 'primeng/multiselect';
import { InputSwitchModule } from 'primeng/inputswitch';
import { CompaignManagementComponent } from './compaign-management.component';
import { AddCompaignComponent } from './add-campaign/add-compaign.component';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { CheckboxModule } from 'primeng/checkbox';
import { DropdownModule } from 'primeng/dropdown';
import { CalendarModule } from 'primeng/calendar';
import { CompaignDetailsComponent } from './campaign-details/compaign-details.component';
import {PanelModule} from 'primeng/panel';
import { AccordionModule } from 'primeng/accordion';
import { TabViewModule } from 'primeng/tabview';
import { TableModule } from 'primeng/table';
import {CardModule} from 'primeng/card';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService} from 'primeng/api';
import { ApprovalEmailComponent } from './approval-email/approval-email.component';
import {ChipsModule} from 'primeng/chips';
import {RadioButtonModule} from 'primeng/radiobutton';
import {FileUploadModule} from 'primeng/fileupload';
import {EditorModule} from 'primeng/editor';
import {ToggleButtonModule} from 'primeng/togglebutton';
import {BreadcrumbModule} from 'primeng/breadcrumb';
import {ConnectorDetailsComponent} from './connector-details/connector-details.component';
import {ConnectorListComponent} from './connector-list/connector-list.component'



@NgModule({
  declarations: [CompaignManagementComponent, AddCompaignComponent, CompaignDetailsComponent, ApprovalEmailComponent
    ,ConnectorDetailsComponent,ConnectorListComponent],
  imports: [
    CommonModule,
    FormsModule,FileUploadModule,EditorModule,ToggleButtonModule,
    SharedModule,BreadcrumbModule,
    FilterContainerModule,
    TreeTableModule,
    MultiSelectModule,
    InputSwitchModule,
    ChatModule,
    ButtonModule,
    ReactiveFormsModule,
    DropdownModule,
    CalendarModule,
    PanelModule,
    CheckboxModule,
    AccordionModule,
    TabViewModule,
    TableModule,
    CardModule,
    ConfirmDialogModule,
    ChipsModule,RadioButtonModule,DialogModule,
    RouterModule.forChild(CompaignManagementRoutes)
  ],
  providers: [FormatNumPipe,ConfirmationService]
})
export class ComapaignManagementModule {}
