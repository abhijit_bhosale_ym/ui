import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
  FormArray
} from '@angular/forms';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { FetchApiDataService } from '../fetch-api-data.service';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { Subscription } from 'rxjs';
import { CommonLibService } from 'src/app/_services';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'ym-connector-details',
  templateUrl: './connector-details.component.html',
  styleUrls: ['./connector-details.component.scss']
})
export class ConnectorDetailsComponent implements OnInit {

  @Input() formIdentity: string;
  @Input() connectionId: string;
  @Input() formData:any
  @Output() triggeredEvent = new EventEmitter<any>();
  
  dynamicFormControlArray = [];
  private appConfigObs: Subscription;
  appConfig: object = {};
  currentUser:any;
  availableAdServers = [
    { label: 'Google Ad Manager', value: 'GAM' },
    { label: 'Beesvax', value: 'BEESVAX' },
  ];
  adServer: any;
  formFlag = false;
  demoForm: FormGroup;

  constructor(private _formBuilder: FormBuilder, private dataFetchServ: FetchApiDataService,
    private toastService: ToastService,private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private confirmationService: ConfirmationService,private router: Router,
    private currentActivatedRoute: ActivatedRoute) { }

  ngOnInit() { 
    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      console.log("appConfig   :", appConfig)
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        this.currentUser = this.appConfig['user']['id'];
      }
    });
    this.demoForm = null;
    this.editUpdateConnection();
  }


  getConnectionForm() {
    this.demoForm = null;
    this.dataFetchServ.getConnectionsDynamicForm(this.adServer['value']).subscribe(response => {
      if (response['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      }
      if (response['status'] == 1) {
        this.dynamicFormControlArray = JSON.parse(response['data'][0].connection_form_json);
        if (this.dynamicFormControlArray.length > 0) {
          const group: any = {};
          this.dynamicFormControlArray.forEach(question => {
            group[question.ngModel] = new FormControl()
          });
          this.demoForm = new FormGroup(group);
          this.formFlag = true;
          if(this.formIdentity=='update'  || this.formIdentity=='edit'){
            this.demoForm.patchValue(JSON.parse(this.formData.connection_json))
            if(this.formIdentity=='edit'){
              this.demoForm.disable();
            }
          }


        } else {
          this.formFlag = false;
          this.demoForm = new FormGroup({});
        }
      }
    })
  }

  submitConnection(demoForm){
    let request = {
      ad_server_name: this.adServer['value'],
      connection_name: demoForm.controls.connection_name.value,
      connection_json: JSON.stringify(demoForm.value),
      created_by: this.currentUser,
    }
    this.dataFetchServ
      .saveConnectionDeatils(request)
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['data']);
          return;
        }
        if (data['status'] == 1) {
          this.toastService.displayToast({
            severity: 'success',
            summary: 'Campaign Management',
            detail: 'Connection created successfully'
          });
        }
        if (data['status'] === 2) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: data['data']
          });
          console.log(data['data']);
          return;
        }
      });
  }
  editUpdateConnection(){
    console.log("Details   formData     :",this.formData)

        if(this.formIdentity=='update'  || this.formIdentity=='edit'){
          if(this.formData.ad_server_name=='GAM'){
            this.adServer =  { label: 'Google Ad Manager', value: 'GAM' };
          }
          if(this.formData.ad_server_name=='BEESVAX'){
            this.adServer = { label: 'Beesvax', value: 'BEESVAX' };
          }
          this.getConnectionForm();
        }
  }


  updateConnection(demoForm){
    let request = {
      ad_server_name: this.adServer['value'],
      connection_name: demoForm.controls.connection_name.value,
      connection_json: JSON.stringify(demoForm.value),
      updated_by: this.currentUser,
      id:this.formData.id
    }
    this.dataFetchServ
      .updateConnectionDeatils(request)
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Plese provide correct credentials'
          });
          console.log(data['data']);
          return;
        }
        if (data['status'] == 1) {
          this.toastService.displayToast({
            severity: 'success',
            summary: 'Campaign Management',
            detail: 'Connection updated successfully'
          });
        }
      });
  }

backToConnectionList(){
  this.triggeredEvent.emit('ConnectionDetail');
}

}
