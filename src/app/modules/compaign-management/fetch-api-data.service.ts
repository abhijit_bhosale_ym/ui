import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FetchApiDataService {
  private BASE_URL: string = environment.baseUrl;

  constructor(private http: HttpClient) {}

getData(details) {
  let url = `${this.BASE_URL}/unified/v1/campaign-delivery-dashboard/getData/getAuthentication`;
  return this.http.post(url, details);
}

getData2() {
  let url = `${this.BASE_URL}/unified/v1/campaign-delivery-dashboard/getOrdersDataMapping`;
  return this.http.post(url, {});
}

  getTableData(params: object, activeView: boolean) {
    let url = `${this.BASE_URL}/frankly/v1/campaign-progress-report/getData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    if (typeof activeView !== 'undefined') {
      url = `${url}${activeView ? '?status=active' : '?status=inactive'}`;
    }
    return this.http.post(url, params);
  }
  getExportReportData(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/common/exportData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
    // return <TreeNode[]> json.data;
  }

  getCommentsCounts() {
    const url = `${this.BASE_URL}/frankly/v1/campaign-progress-report/getCommentsCountData`;
    return this.http.get(url);
  }

  getFavorite() {
    const url = `${this.BASE_URL}/frankly/v1/campaign-progress-report/getFavoriteData`;
    return this.http.get(url);
  }

  setFavourite(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/campaign-progress-report/set-favorite`;
    return this.http.post(url, params);
  }

  setFavouriteLine(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/campaign-progress-report/set-favorite-line`;
    return this.http.post(url, params);
  }

  getCommentsData() {
    const url = `${this.BASE_URL}/frankly/v1/campaign-progress-report/getCommentsData`;
    return this.http.get(url);
  }

  getCommentsCountData() {
    const url = `${this.BASE_URL}/frankly/v1/campaign-progress-report/getCommentsCountData`;
    return this.http.get(url);
  }

  getLastUpdatedData(appId) {
    const url = `${this.BASE_URL}/frankly/v1/common/getLastUpdatedData/${appId}`;
    return this.http.get(url);
  }

  public sendScreenshot(details) {
    const url = `${this.BASE_URL}/frankly/v1/campaign-delivery-dashboard/send-email`;
    return this.http.post(url, details);
  }

   getAuth(details) {
    const url = `${this.BASE_URL}/unified/v1/campaign-delivery-dashboard/getOrdersDataMapping`;
    return this.http.post(url, details);
  }

  public getTargeting(details) {
    const url = `${this.BASE_URL}/unified/v1/campaign-delivery-dashboard/getTargeting`;
    return this.http.post(url, details);
  }

  public getTargetingPlacement(details) {
    const url = `${this.BASE_URL}/unified/v1/campaign-delivery-dashboard/getTargetingPlacement`;
    return this.http.post(url, details);
  }

  public getTargetingCustom(details) {
    const url = `${this.BASE_URL}/unified/v1/campaign-delivery-dashboard/getTargetingCustom`;
    return this.http.post(url, details);
  }

  public saveGamOrders(details) {
    const url = `${this.BASE_URL}/unified/v1/campaign-delivery-dashboard/createOrder`;
    return this.http.post(url, details);
  }

  public saveGamLineItems(details) {
    const url = `${this.BASE_URL}/unified/v1/campaign-delivery-dashboard/createLineItem`;
    return this.http.post(url, details);
  }

  createCreative(req) {
    const url = `${this.BASE_URL}/unified/v1/campaign-delivery-dashboard/createCreative`;
    return this.http.post(url, req);
  }

  saveCreative(details) {
    const url = `${this.BASE_URL}/unified/v1/campaign-delivery-dashboard/saveCreative`;
    return this.http.post(url, details);
  }

  public getTargetingWithWhereClause(details) {
    const url = `${this.BASE_URL}/unified/v1/campaign-delivery-dashboard/getTargetingWhereClause`;
    return this.http.post(url, details);
  }
  
  public getAllCampaigns() {
    const url = `${this.BASE_URL}/unified/v1/campaign-delivery-dashboard/getAllCampaigns`;
    return this.http.post(url,{});
  }

  public getLineItemsByCampaign(id) {
    const url = `${this.BASE_URL}/unified/v1/campaign-delivery-dashboard/getLineItemsByCampaignId/${id}`;
    return this.http.post(url,{});
  }
  public getCustomTargeting(details) {
    const url = `${this.BASE_URL}/unified/v1/campaign-delivery-dashboard/getCustomTargeting`;
    return this.http.post(url, details);
  }

  public pushToGAM(payload) {
     const url = `${this.BASE_URL}/unified/v1/campaign-delivery-dashboard/pushToGAM`;
    return this.http.post(url, payload);
   }

   public getCreativeByLineItems(id) {
     const params = new HttpParams().append("id",id);
     const url = `${this.BASE_URL}/unified/v1/campaign-delivery-dashboard/getCreativeByLineItems`;
    return this.http.get(url,{params});
  }
  public getPresetTargetingList() {
    const url = `${this.BASE_URL}/unified/v1/campaign-delivery-dashboard/getPresetTargetingList`;
   return this.http.get(url);
 }
 public getSelectedPresetTargeting(id) {
  const params = new HttpParams().append("id",id);
  const url = `${this.BASE_URL}/unified/v1/campaign-delivery-dashboard/getSelectedPresetTargeting`;
  return this.http.get(url,{params});
}

public savePresetTargeting(payload) {
  const url = `${this.BASE_URL}/unified/v1/campaign-delivery-dashboard/savePresetTargeting`;
  return this.http.post(url,payload);
}

public getConnectionsDynamicForm(ad_server_name:string){
const params = new HttpParams().append("ad_server_name",ad_server_name);
  const url = `${this.BASE_URL}/unified/v1/campaign-connector/getConnectionsDynamicForm`;
  return this.http.get(url,{params});
}

public saveConnectionDeatils(details) {
  const url = `${this.BASE_URL}/unified/v1/campaign-connector/saveConnectionDeatils`;
  return this.http.post(url, details);
}


public getConnectionsList() {
  // const params = new HttpParams().append("id",id);
  const url = `${this.BASE_URL}/unified/v1/campaign-delivery-dashboard/getConnectionsList`;
 return this.http.get(url);
}


public updateConnectionDeatils(details) {
  const url = `${this.BASE_URL}/unified/v1/campaign-connector/updateConnectionDeatils`;
  return this.http.post(url, details);
}

public deleteConnectionDeatils(payload) {
  const params = new HttpParams().append("payload",payload);
  const url = `${this.BASE_URL}/unified/v1/campaign-connector/deleteConnectionDeatils`;
  return this.http.delete(url, {params});
}

public updateGamOrders(details) {
  const url = `${this.BASE_URL}/unified/v1/campaign-delivery-dashboard/updateOrder`;
  return this.http.post(url, details);
}


public deleteOrder(payload) {
  const params = new HttpParams().append("payload",payload);
  const url = `${this.BASE_URL}/unified/v1/campaign-delivery-dashboard/deleteOrder`;
  return this.http.delete(url, {params});
}



public updateGamLineItems(details) {
  const url = `${this.BASE_URL}/unified/v1/campaign-delivery-dashboard/updateLineItem`;
  return this.http.post(url, details);
}

public deleteLineItem(payload) {
  const params = new HttpParams().append("payload",payload);
  const url = `${this.BASE_URL}/unified/v1/campaign-delivery-dashboard/deleteLineItem`;
  return this.http.delete(url, {params});
}

public updateCreative(details) {
  const url = `${this.BASE_URL}/unified/v1/campaign-delivery-dashboard/updateCreative`;
  return this.http.post(url, details);
}

public deleteCreative(payload) {
  const params = new HttpParams().append("payload",payload);
  const url = `${this.BASE_URL}/unified/v1/campaign-delivery-dashboard/deleteCreative`;
  return this.http.delete(url, {params});
}

public getCreativeAsset(id) {
  const params = new HttpParams().append("id",id);
  const url = `${this.BASE_URL}/unified/v1/campaign-delivery-dashboard/getCreativeAsset`;
  return this.http.get(url,{params});
}

}