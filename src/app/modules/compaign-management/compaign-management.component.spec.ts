import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompaignManagementComponent } from './compaign-management.component';

describe('CompaignManagementComponent', () => {
  let component: CompaignManagementComponent;
  let fixture: ComponentFixture<CompaignManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompaignManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompaignManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
