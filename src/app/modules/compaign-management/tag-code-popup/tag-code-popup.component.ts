import { Component, OnInit } from '@angular/core';
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder
} from '@angular/forms';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'ym-tag-code-popup',
  templateUrl: './tag-code-popup.component.html',
  styleUrls: ['./tag-code-popup.component.scss']
})
export class TagCodePopupComponent implements OnInit {
  tagCodeForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    public ref: DynamicDialogRef

  ) { }

  ngOnInit() {

    const reg = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';
    this.tagCodeForm = this.formBuilder.group({
      tagCode:['',[Validators.required, Validators.pattern(reg)]],
     });

  }

  get c() {
    return this.tagCodeForm.controls;
  }

  goBack()
  {
    this.ref.close();
  }

}
