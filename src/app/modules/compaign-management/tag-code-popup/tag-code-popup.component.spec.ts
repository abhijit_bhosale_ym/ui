import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TagCodePopupComponent } from './tag-code-popup.component';

describe('TagCodePopupComponent', () => {
  let component: TagCodePopupComponent;
  let fixture: ComponentFixture<TagCodePopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TagCodePopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagCodePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
