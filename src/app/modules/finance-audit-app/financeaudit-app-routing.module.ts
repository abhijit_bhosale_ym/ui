import { Routes } from '@angular/router';
import { FinanceAuditAppComponent } from './financeaudit-app.component';

export const FinanceAuditAppRoutes: Routes = [
  {
    path: '',
    component: FinanceAuditAppComponent
  }
];
