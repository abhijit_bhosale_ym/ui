import { Component, OnInit, OnDestroy } from '@angular/core';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { AppConfigService } from '../../_services/app-config/app-config.service';
import { CommonLibService } from '../../_services';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';
import { FetchApiDataService } from './fetch-api-data.service';
import { ExportdataService } from '../../_services/export/exportdata.service';

import { ExportPptService } from '../../_services/export/export-ppt.service';
import { HtmltoimageService } from '../../_services/screencapture/htmltoimage.service';
import { ToastService } from '../../_services/toast-notification/toast.service';
import { Subscription } from 'rxjs';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { DialogService } from 'primeng';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-financeaudit-app',
  templateUrl: './financeaudit-app.component.html',
  styleUrls: ['./financeaudit-app.component.scss']
})
export class FinanceAuditAppComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;

  appConfig: object = {};
  lastUpdatedOn: Date;

  flatTableReq: object;
  flatTableData: TreeNode[];
  flatTableColumnDef: any[];
  flatTableJson: Object;
  flatTableReqSource: object;
  flatTableDataSource: TreeNode[];
  flatTableSourceColumnDef: any[];
  flatTableJsonSource: Object;
  tableReqSource: Object;
  breakdownFooters: object;
  noTableDataSource = false;
  noTableData = false;
  pagination = false;
  isSourceBreakDown = false;
  isUpdateStatus = false;
  selectedAccounting_key = 0;
  statusList = ['Approve', 'Reject', 'In Review'];
  statusLists = this.statusList.map(name => {
    return { label: name, value: name };
  });
  isExportReport = false;
  totalMonth: any[];
  filtersApplied: object = {};
  timeout: any;
  exportRequest: ExportRequest = <ExportRequest>{};
  isExport = false;
  searchVal = "";
  searchColDef = [];

  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private htmltoimage: HtmltoimageService,
    private exportService: ExportdataService,
    private pptExport: ExportPptService,
    private confirmationService: ConfirmationService
  ) { }

  ngOnInit() {
    this.flatTableColumnDef = [
      {
        field: 'accounting_key',
        displayName: 'Accounting Period',
        format: 'date',
        width: '150',
        value: '',
        isExpanded: 'false',
        exportConfig: {
          format: 'date<<MMMM YYYY<< ',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: ['MMMM YYYY'],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'dfp_adserver_impressions',
        displayName: 'DFP Impressions',
        format: 'number',
        width: '130',
        isExpanded: 'false',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'dp_impressions',
        displayName: '3P Paid Impressions',
        format: 'number',
        value: '',
        condition: '',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        width: '120',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'fill_rate_finance',
        displayName: 'Fill Rate(%)',
        format: 'percentage',
        width: '115',
        formatConfig: [2],
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'gross_revenue',
        displayName: 'Gross Revenue',
        width: '135',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'pubmatic_fee',
        displayName: 'PubMatic Fee',
        format: '$',
        width: '130',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'rubicon_fee',
        displayName: 'Rubicon Fee',
        format: '$',
        width: '120',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },

      {
        field: 'associated_press_fee_distribution',
        displayName: 'Associated Press Fee',
        format: '$',
        width: '120',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'staq_fee_distribution',
        displayName: 'STAQ Fee',
        format: '$',
        width: '110',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },

      {
        field: 'yuktamedia_fee_distribution',
        displayName: 'YuktaMedia Fee',
        format: '$',
        width: '155',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'yukta_portal_fee',
        displayName: 'YuktaMedia Portal Fee',
        format: '$',
        width: '110',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },

      {
        field: 'mediatrust_fee_distribution',
        displayName: 'Media Trust Fee',
        format: '$',
        width: '115',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'ga360_fee_distribution',
        displayName: 'GA360 Fee',
        format: '$',
        width: '115',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'lmc_fee_distribution',
        displayName: 'LMC Fee',
        format: '$',
        width: '115',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'dfp_recharge_fee_distribution',
        displayName: 'DFP Recharges',
        format: '$',
        width: '140',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'total_expenses',
        displayName: 'Total Expenses',
        format: '$',
        width: '145',
        exportConfig: {
          format: 'currency',
          styleinfo: {

            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'total_recoupable_expenses',
        displayName: 'Total Recoupable Expenses',
        format: '$',
        width: '150',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'station_revenue_share_distribution',
        displayName: 'Client Net Share($)',
        format: '$',
        width: '100',
        value: '',
        condition: '',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },

      {
        field: 'client_net_share',
        displayName: 'Client Net Share(%)',
        format: 'percentage',
        width: '100',
        value: '',
        condition: '',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'healthination_net_revenue_share',
        displayName: 'Healthination Net Share($)',
        format: '$',
        width: '125',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'healthination_net_revenue_share_per',
        displayName: 'Healthination Net Share(%)',
        format: '',
        width: '130',
        value: '',
        condition: '',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },

      {
        field: 'frankly_net_revenue_share',
        displayName: 'YuktaOne Net Revenue Share($)',
        format: '$',
        width: '160',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },

      {
        field: 'frankly_net_revenue_share_percentage',
        displayName: 'YuktaOne Net Share(%)',
        format: 'percentage',
        width: '125',
        value: '',
        condition: '',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },

      {
        field: 'check_balance',
        displayName: 'Check Balance',
        width: '140',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      }
    ];

    this.flatTableJson = {
      page_size: 10,
      lazy: false,
      loading: true,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '300px',
      totalRecords: 1000,
      columns: this.flatTableColumnDef.slice(1),
      selectedColumns: this.flatTableColumnDef.slice(1),
      frozenCols: [...this.flatTableColumnDef.slice(0, 1)],
      frozenWidth:
        this.flatTableColumnDef
          .slice(0, 1)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };

    this.flatTableSourceColumnDef = [
      {
        field: 'source',
        displayName: 'Source',
        format: '',
        width: '250',
        isExpanded: 'false',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'actual_gross_revenue',
        displayName: 'Actual Gross Revenue (A)',
        format: '$',
        width: '130',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: 0
      },

      {
        field: 'dp_revenue_x',
        displayName: 'Source Revenue by Ad Unit(B)',
        format: '$',
        width: '132',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: 0
      },

      {
        field: 'gross_revenue',
        displayName: 'Gross Revenue by Ad Unit (C)',
        format: '$',
        width: '150',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: 0
      },

      {
        field: 'adjusted_invoice',
        displayName: '3P Source Gross Revenue (D)',
        format: '$',
        width: '140',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: 0
      },

      {
        field: 'daily_variance_amt_3p',
        displayName: 'Daily Variance Amount 3P Source (D-A)',
        format: '$',
        width: '160',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: 0
      },

      {
        field: 'daily_variance_amt_source',
        displayName: 'Daily Variance Amount Source (A-B)',
        format: '$',
        width: '165',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: 0
      },

      {
        field: 'daily_variance_amt_source_per',
        displayName: 'Daily Variance % Source (A-B)/B',
        format: 'percentage',
        width: '140',
        value: '',
        condition: '',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: 0
      },

      {
        field: 'monthend_variance_amt_source',
        displayName: 'Monthend Variance Amount Source (D-C)',
        format: '$',
        width: '170',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: 0
      },

      {
        field: 'monthend_variance_amt_source_per',
        displayName: 'Monthend Variance % Source (D-C)/C',
        format: 'percentage',
        width: '160',
        value: '',
        condition: '',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: 0
      },

      {
        field: 'status',
        displayName: 'Approval Status',
        format: '',
        width: '160',
        isExpanded: 'false',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: '-'
      },

      {
        field: 'name',
        displayName: 'Approved By',
        format: '',
        width: '160',
        isExpanded: 'false',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: '-'
      },
      {
        field: 'approval_date',
        displayName: 'Approval Date',
        format: 'date',
        width: '160',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: ['MM-DD-YYYY'],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: '-'
      }
    ];

    this.flatTableJsonSource = {
      page_size: 10,
      lazy: false,
      loading: true,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '300px',
      totalRecords: 1000,
      columns: this.flatTableSourceColumnDef.slice(1),
      selectedColumns: this.flatTableSourceColumnDef.slice(1),
      frozenCols: [...this.flatTableSourceColumnDef.slice(0, 1)],
      frozenWidth:
        this.flatTableSourceColumnDef
          .slice(0, 1)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      globalFilterFields: this.flatTableSourceColumnDef,
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };

    this.breakdownFooters = {
      dp_revenue_x: 0,
      adjusted_invoice: 0,
      gross_revenue: 0,
      actual_gross_revenue: 0,
      daily_variance_amt_3p: 0,
      daily_variance_amt_source: 0,
      daily_variance_amt_source_per: 0,
      monthend_variance_amt_source: 0,
      monthend_variance_amt_source_per: 0
    };

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        console.log('appConfig', this.appConfig);
        this.isExportReport = this.appConfig['permissions'].some(
          o => o.name === 'finance-audit-export-report'
        );
        this.isUpdateStatus = this.appConfig['permissions'].some(
          o => o.name == 'finance-audit-approval-status'
        );
        this._titleService.setTitle(this.appConfig['displayName']);
        let startDate;
        if (
          this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
          'defaultDate'
          ][0]['startOf']
        ) {
          startDate = moment()
            .subtract(
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['value'],
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['period']
            )
            .startOf(
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['period']
            );
        } else {
          startDate = moment().subtract(
            this.appConfig['filter']['filterConfig']['filters'][
            'datePeriod'
            ][0]['defaultDate'][0]['value'],
            this.appConfig['filter']['filterConfig']['filters'][
            'datePeriod'
            ][0]['defaultDate'][0]['period']
          );
        }
        const endDate = moment().subtract(
          this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
          'defaultDate'
          ][1]['value'],
          this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
          'defaultDate'
          ][1]['period']
        );

        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: startDate.format('YYYYMMDD'),
            time_key2: endDate.endOf('month').format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: ['station_group'] // this.appConfig["filter"]["filterConfig"]["groupBy"].filter(
          // v => v.selected
          // )
        };
        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        this.initialLoading();
      }
    });
  }

  initialLoading() {
    this.dataFetchServ
      .getLastUpdatedData(this.appConfig['id'])
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        this.lastUpdatedOn = moment(data).toDate();
      });

    this.flatTableReq = {
      dimensions: ['accounting_key'],
      metrics: [
        'gross_revenue',
        'dfp_adserver_impressions',
        'dp_impressions',
        'pubmatic_fee',
        'rubicon_fee',
        'associated_press_fee_distribution',
        'staq_fee_distribution',
        'yuktamedia_fee_distribution',
        'yukta_portal_fee',
        'mediatrust_fee_distribution',
        'ga360_fee_distribution',
        'lmc_fee_distribution',
        'dfp_recharge_fee_distribution',
        'total_expenses',
        'total_recoupable_expenses',
        'healthination_net_revenue_share',
        'frankly_net_revenue_share',
        'check_balance',
        'station_revenue_share_distribution'
      ],
      derived_metrics: [
        'fill_rate_finance',
        'healthination_net_revenue_share_per',
        'frankly_net_revenue_share_percentage',
        'client_net_share'
      ],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [{ key: 'accounting_key', opcode: 'desc' }],
      limit: '',
      offset: ''
    };

    this.loadTableData(this.flatTableReq);
    this.flatTableSourceColumnDef.forEach(element => {
      this.searchColDef.push({ 'field': element['field'], 'format': element['format'], 'formatConfig': element['formatConfig'] });
    });
  }

  resetPagination(tt1) {
    if (typeof tt1 !== 'undefined') {
      tt1.reset();
    }
  }

  loadTableData(params) {
    this.isExport = false;
    this.flatTableJson['loading'] = true;

    this.dataFetchServ.getPayoutData(params).subscribe(data => {
      this.totalMonth = Array.from(
        new Set(data['data'].map(s => s['accounting_key']))
      );
      const arr = [];
      const resData = data as {};
      for (const r of resData['data']) {
        const obj = {};
        obj['data'] = r;
        arr.push(obj);
      }
      if (arr.length > 0) {
        this.isExport = true;
      }
      this.flatTableData = <TreeNode[]>arr;
      this.pagination = true;
      this.flatTableJson['totalRecords'] = resData['totalItems'];
      this.flatTableJson['loading'] = false;
    });
  }

  onGlobalSearchChanged(searchValue, tableColDef) {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    this.timeout = setTimeout(() => {
      this.flatTableReq['filters']['globalSearch'] = {
        dimensions: [],
        value: searchValue
      };
      tableColDef.forEach(element => {
        this.flatTableReq['filters']['globalSearch']['dimensions'].push(
          element.field
        );
      });
      this.loadTableData(this.flatTableReq);
    }, 3000);
  }

  loadSourceWiseTableData(params1) {
    this.flatTableJsonSource['loading'] = true;

    this.dataFetchServ.getPayoutDataSource(params1).subscribe(data => {
      const footerTotal = {};
      const arr = [];
      const resData = data as [];
      this.breakdownFooters = {
        dp_revenue_x: 0,
        adjusted_invoice: 0,
        gross_revenue: 0,
        actual_gross_revenue: 0,
        daily_variance_amt_3p: 0,
        daily_variance_amt_source: 0,
        daily_variance_amt_source_per: 0,
        monthend_variance_amt_source: 0,
        monthend_variance_amt_source_per: 0
      };
      for (const r of resData) {
        const obj = {};
        obj['data'] = r;
        arr.push(obj);

        this.breakdownFooters['dp_revenue_x'] += r['dp_revenue_x']
          ? r['dp_revenue_x']
          : 0;
        this.breakdownFooters['gross_revenue'] += r['gross_revenue']
          ? r['gross_revenue']
          : 0;
        this.breakdownFooters['actual_gross_revenue'] += r[
          'actual_gross_revenue'
        ]
          ? r['actual_gross_revenue']
          : 0;
        this.breakdownFooters['adjusted_invoice'] += r['adjusted_invoice']
          ? r['adjusted_invoice']
          : 0;
        this.breakdownFooters['daily_variance_amt_3p'] += r[
          'daily_variance_amt_3p'
        ]
          ? r['daily_variance_amt_3p']
          : 0;
        this.breakdownFooters['daily_variance_amt_source'] += r[
          'daily_variance_amt_source'
        ]
          ? r['daily_variance_amt_source']
          : 0;
        this.breakdownFooters['daily_variance_amt_source_per'] += r[
          'daily_variance_amt_source_per'
        ]
          ? r['daily_variance_amt_source_per']
          : 0;
        this.breakdownFooters['monthend_variance_amt_source'] += r[
          'monthend_variance_amt_source'
        ]
          ? r['monthend_variance_amt_source']
          : 0;
        this.breakdownFooters['monthend_variance_amt_source_per'] += r[
          'monthend_variance_amt_source_per'
        ]
          ? r['monthend_variance_amt_source_per']
          : 0;
      }
      this.breakdownFooters['daily_variance_amt_source_per'] =
        this.breakdownFooters['daily_variance_amt_source_per'] != 0
          ? this.breakdownFooters['daily_variance_amt_source_per'] /
          resData.length
          : 0;
      this.breakdownFooters['monthend_variance_amt_source_per'] =
        this.breakdownFooters['monthend_variance_amt_source_per'] != 0
          ? this.breakdownFooters['monthend_variance_amt_source_per'] /
          resData.length
          : 0;

      this.flatTableDataSource = <TreeNode[]>arr;
      this.pagination = true;
      this.flatTableJsonSource['totalRecords'] = resData.length;
      this.flatTableJsonSource['loading'] = false;


      this.flatTableSourceColumnDef.forEach(c => {
        c['footerTotal'] = this.breakdownFooters[c['field']];
      });
      // this.flatTableJsonSource['lazy'] = true;
    });
  }

  onChangeStatus(row) {
    console.log(row);
    const req = {
      status: row['status'],
      source: row['source'],
      approver: this.appConfig['user']['id'].toString(),
      accounting_key: this.selectedAccounting_key,
    };

    this.dataFetchServ.updateStatus(req).subscribe(data => {
      if (data === 'OK') {
        this.toastService.displayToast({
          severity: 'success',
          summary: 'Status Update',
          detail: row['source'] + ' status updated successfully'
        });

        this.loadSourceWiseTableData(this.tableReqSource);
      } else {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
      }
    });
  }

  onLazyLoadAggTable(e: Event) {
    console.log('Lazy Agg', e);
  }
  onLazyLoadFlatTable(e: Event, tbleName) {
    // console.log('tbleName',tbleName ,e)
    if (tbleName == 'flatTable') {
      if (typeof this.flatTableReq !== 'undefined') {
        let orderby = [];
        if (
          typeof e['multiSortMeta'] !== 'undefined' &&
          e['multiSortMeta'] !== null
        ) {
          e['multiSortMeta'].forEach(sort => {
            orderby.push({
              key: sort['field'],
              opcode: sort['order'] === -1 ? 'desc' : 'asc'
            });
          });
        } else {
          orderby = [{ key: 'accounting_key', opcode: 'desc' }];
        }
        this.flatTableReq['limit'] = e['rows'];
        this.flatTableReq['offset'] = e['first'];
        this.flatTableReq['orderBy'] = orderby;
        this.loadTableData(this.flatTableReq);
      }
    }
    if (tbleName == 'flatTableSource') {
      if (typeof this.tableReqSource !== 'undefined') {
        let orderby = [];
        if (
          typeof e['multiSortMeta'] !== 'undefined' &&
          e['multiSortMeta'] !== null
        ) {
          e['multiSortMeta'].forEach(sort => {
            orderby.push({
              key: sort['field'],
              opcode: sort['order'] === -1 ? 'desc' : 'asc'
            });
          });
        } else {
          orderby = [{ key: 'source', opcode: 'asc' }];
        }

        this.tableReqSource['limit'] = e['rows'];
        this.tableReqSource['offset'] = e['first'];
        this.tableReqSource['orderBy'] = orderby;
        this.loadSourceWiseTableData(this.tableReqSource);
      }
    }
  }

  showSourceWiseBreakDown(row, field, rowData) {
    this.isSourceBreakDown = true;
    this.selectedAccounting_key = row;
    const timeKey1 = row;
    const timeKey2 = moment(timeKey1, 'YYYYMMDD')
      .endOf('month')
      .format('YYYYMMDD');
    this.tableReqSource = {
      dimensions: ['source'],
      metrics: ['gross_revenue', 'dp_revenue_x'],
      derived_metrics: [],
      timeKeyFilter: {
        time_key1: timeKey1,
        time_key2: timeKey2
      },
      filters: {
        dimensions: [],
        metrics: []
      },
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [],
      limit: '',
      offset: ''
    };
    this.loadSourceWiseTableData(this.tableReqSource);
  }

  onClickBack() {
    this.isSourceBreakDown = false;
  }

  exportTable(table, fileFormat) {
    if (this.exportRequest['sendEmail'].length > 0) {

      if (table == 'flatTable') {
        if (this.flatTableData.length == 0) {
          this.confirmationService.confirm({
            message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
            header: 'Information',
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
            },
            reject: () => {
            }
          });
        } else {
          this.toastService.displayToast({
            severity: 'info',
            summary: 'Export Report',
            detail:
              'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
            life: 10000
          });
          const colDef = [
            {
              field: 'accounting_key',
              displayName: 'Accounting Period',
              format: 'date',
              width: '170',
              value: '',
              isExpanded: 'false',
              exportConfig: {
                format: 'date<<MMMM YYYY<< ',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: false
              }
            },
            {
              field: 'station_group',
              displayName: 'Station Group rpt',
              format: '',
              width: '250',
              isExpanded: 'false',
              exportConfig: {
                format: 'string',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: false
              }
            },
            {
              field: 'derived_station_rpt',
              displayName: 'Station rpt',
              format: '',
              width: '250',
              isExpanded: 'false',
              exportConfig: {
                format: 'string',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: false
              }
            },
            {
              field: 'source',
              displayName: 'Source Provider rpt',
              format: '',
              width: '250',
              isExpanded: 'false',
              exportConfig: {
                format: 'string',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: false
              }
            },

            {
              field: 'derived_placement_name',
              displayName: 'Placement rpt',
              format: '',
              width: '250',
              isExpanded: 'false',
              exportConfig: {
                format: 'string',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: false
              }
            },

            {
              field: 'derived_inventry_name',
              displayName: 'Inventory rpt',
              format: '',
              width: '250',
              isExpanded: 'false',
              exportConfig: {
                format: 'string',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: false
              }
            },

            {
              field: 'creative_size_rpt_name',
              displayName: 'Creative Size rpt',
              format: '',
              width: '250',
              isExpanded: 'false',
              exportConfig: {
                format: 'string',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: false
              }
            },

            {
              field: 'derived_sales_type_name',
              displayName: 'Sales Type rpt',
              format: '',
              width: '250',
              isExpanded: 'false',
              exportConfig: {
                format: 'string',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: false
              }
            },
            {
              field: 'derived_device_type1_name',
              displayName: 'Device Type1 rpt',
              format: '',
              width: '250',
              isExpanded: 'false',
              exportConfig: {
                format: 'string',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: false
              }
            },
            {
              field: 'derived_ad_type_name',
              displayName: 'Ad Type rpt',
              format: '',
              width: '250',
              isExpanded: 'false',
              exportConfig: {
                format: 'string',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: false
              }
            }
          ];
          colDef.push(
            ...this.libServ.deepCopy(
              this.flatTableColumnDef.filter(x => (x.field != 'accounting_key' && x.field != "fill_rate_finance"))
            )
          );
          colDef.forEach(x => {
            if (x['field'] === 'dfp_recharge_fee_distribution') {
              x['displayName'] = 'DFP Recharge Fees';
            }
            if (x['field'] === 'dp_impressions') {
              x['displayName'] = 'Billable Impressions';
            }
            if (x['field'] === 'total_expenses') {
              x['field'] = 'total_expense'
            }
          });


          this.totalMonth.forEach(startDate => {
            let start = startDate.toString();
            let endDate = moment(startDate, 'YYYYMMDD').endOf('month').format('YYYYMMDD');
            let month = moment(startDate, 'YYYYMMDD').format('MMM')

            const tableReq = {
              dimensions: [],
              metrics: [],
              derived_metrics: [],
              // timeKeyFilter: this.libServ.deepCopy(
              //   this.filtersApplied['timeKeyFilter']
              // ),
              timeKeyFilter: { time_key1: start, time_key2: endDate },
              filters: this.libServ.deepCopy(this.filtersApplied['filters']),
              groupByTimeKey: {
                key: ['accounting_key', 'time_key'],
                interval: 'daily'
              },
              gidGroupBy: [],
              orderBy: [{ key: 'accounting_key', opcode: 'desc' }],
              limit: '',
              offset: ''
            };

            const sheetDetails = {};
            sheetDetails['columnDef'] = colDef;
            sheetDetails['data'] = [];
            sheetDetails['sheetName'] = 'Detail Report';
            sheetDetails['isRequest'] = true;
            sheetDetails['request'] = {
              url: '/api/flask/finance-audit/cassandradata',
              method: 'POST',
              param: tableReq
            };
            sheetDetails['disclaimer'] = [
              {
                position: 'bottom',
                label: 'Note: Data present in the table may vary over a period of time.',
                color: '#000000'
              }
            ];
            sheetDetails['totalFooter'] = {
              available: false,
              custom: false
            };
            sheetDetails['tableTitle'] = {
              available: false,
              label: 'Detail Report'
            };
            sheetDetails['image'] = [
              {
                available: true,
                path: environment.exportConfig.exportLogo,
                position: 'top'
              }
            ];
            const sheetDetailsarray = [];
            sheetDetailsarray.push(sheetDetails);
            this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
            this.exportRequest['fileName'] =
              month + ' Detail Report ' + moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
            this.exportRequest['exportFormat'] = fileFormat;
            this.exportRequest['exportConfig'] = environment.exportConfig;
            this.dataFetchServ
              .getExportReportData(this.exportRequest)
              .subscribe(response => {
                console.log(response);
              });
          });
        }
        return false;
      } else if (table == 'sourceTable') {
        if (this.flatTableDataSource.length == 0) {
          this.confirmationService.confirm({
            message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
            header: 'Information',
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
            },
            reject: () => {
            }
          });
        } else {
          this.toastService.displayToast({
            severity: 'info',
            summary: 'Export Report',
            detail:
              'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
            life: 10000
          });
          const colDef = this.libServ.deepCopy(this.flatTableSourceColumnDef);
          const tbleReq = this.libServ.deepCopy(this.tableReqSource);

          tbleReq['isTable'] = true;
          this.dataFetchServ.getPayoutDataSource(tbleReq).subscribe(success => {
            if (success['status'] === 0) {
              this.toastService.displayToast({
                severity: 'error',
                summary: 'Server Error',
                detail: 'Please refresh the page'
              });
              console.log(success['status_msg']);
              return;
            }

            const dailyData = this.libServ.deepCopy(success);
            this.breakdownFooters = {
              dp_revenue_x: 0,
              adjusted_invoice: 0,
              gross_revenue: 0,
              actual_gross_revenue: 0,
              daily_variance_amt_3p: 0,
              daily_variance_amt_source: 0,
              daily_variance_amt_source_per: 0,
              monthend_variance_amt_source: 0,
              monthend_variance_amt_source_per: 0
            };
            for (const r of dailyData) {
              if (r['approval_date'] != '') {
                r['approval_date'] = moment(r['approval_date'], 'YYYYMMDD').format('MM-DD-YYYY');
              }
              this.breakdownFooters['dp_revenue_x'] += r['dp_revenue_x']
                ? r['dp_revenue_x']
                : 0;
              this.breakdownFooters['gross_revenue'] += r['gross_revenue']
                ? r['gross_revenue']
                : 0;
              this.breakdownFooters['actual_gross_revenue'] += r[
                'actual_gross_revenue'
              ]
                ? r['actual_gross_revenue']
                : 0;
              this.breakdownFooters['adjusted_invoice'] += r['adjusted_invoice']
                ? r['adjusted_invoice']
                : 0;
              this.breakdownFooters['daily_variance_amt_3p'] += r[
                'daily_variance_amt_3p'
              ]
                ? r['daily_variance_amt_3p']
                : 0;
              this.breakdownFooters['daily_variance_amt_source'] += r[
                'daily_variance_amt_source'
              ]
                ? r['daily_variance_amt_source']
                : 0;
              this.breakdownFooters['daily_variance_amt_source_per'] += r[
                'daily_variance_amt_source_per'
              ]
                ? r['daily_variance_amt_source_per']
                : 0;
              this.breakdownFooters['monthend_variance_amt_source'] += r[
                'monthend_variance_amt_source'
              ]
                ? r['monthend_variance_amt_source']
                : 0;
              this.breakdownFooters['monthend_variance_amt_source_per'] += r[
                'monthend_variance_amt_source_per'
              ]
                ? r['monthend_variance_amt_source_per']
                : 0;
            }
            this.breakdownFooters['daily_variance_amt_source_per'] =
              this.breakdownFooters['daily_variance_amt_source_per'] != 0
                ? this.breakdownFooters['daily_variance_amt_source_per'] /
                dailyData.length
                : 0;
            this.breakdownFooters['monthend_variance_amt_source_per'] =
              this.breakdownFooters['monthend_variance_amt_source_per'] != 0
                ? this.breakdownFooters['monthend_variance_amt_source_per'] /
                dailyData.length
                : 0;
            dailyData.push(this.breakdownFooters);
            const sheetDetails = {};
            sheetDetails['columnDef'] = colDef;
            sheetDetails['data'] = dailyData;
            sheetDetails['sheetName'] = 'Data';
            sheetDetails['isRequest'] = false;
            sheetDetails['request'] = {
              url: '',
              method: '',
              param: {
                timeKeyFilter: this.libServ.deepCopy(
                  this.filtersApplied['timeKeyFilter']
                )
              }
            };
            sheetDetails['disclaimer'] = [
              {
                position: 'bottom',
                label: 'Note: Data present in the table may vary over a period of time.',
                color: '#000000'
              }
            ];
            sheetDetails['totalFooter'] = {
              available: true,
              custom: false
            };
            sheetDetails['tableTitle'] = {
              available: false,
              label: 'Overall Finance Report Source Wise Distribution'
            };
            sheetDetails['image'] = [
              {
                available: true,
                path: environment.exportConfig.exportLogo,
                position: 'top'
              }
            ];
            const sheetDetailsarray = [];
            sheetDetailsarray.push(sheetDetails);
            this.exportRequest['sheetDetails'] = <SheetDetails[]>(
              sheetDetailsarray
            );
            this.exportRequest['fileName'] =
              'Overall Finance Report Source Wise Distribution ' +
              moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
            this.exportRequest['exportFormat'] = fileFormat;
            this.exportRequest['exportConfig'] = environment.exportConfig;
            // console.log('exportreport', this.exportRequest);
            // return false;
            this.dataFetchServ
              .getExportReportData(this.exportRequest)
              .subscribe(response => {
                console.log(response);
              });
          });
        }
        return false;
      }
    } else {
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Export Report',
        detail:
          'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
        life: 10000
      });
    }
  }


  isHiddenColumn(col: Object) {
    return (
      this.flatTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.flatTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }

  /* --------------------- On Filter Apply Button Clicked --------------------- */

  onFiltersApplied(filterData: object) {
    this.filtersApplied['filters']['dimensions'] = [];

    // tslint:disable-next-line: forin

    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = moment(filterData['date'][1], 'YYYYMMDD').endOf('month').format('YYYYMMDD');

    this.flatTableReq = {
      dimensions: ['accounting_key'],
      metrics: [
        'gross_revenue',
        'dfp_adserver_impressions',
        'dp_impressions',
        'pubmatic_fee',
        'rubicon_fee',
        'associated_press_fee_distribution',
        'staq_fee_distribution',
        'yuktamedia_fee_distribution',
        'yukta_portal_fee',
        'mediatrust_fee_distribution',
        'ga360_fee_distribution',
        'lmc_fee_distribution',
        'dfp_recharge_fee_distribution',
        'total_expenses',
        'total_recoupable_expenses',
        'healthination_net_revenue_share',
        'frankly_net_revenue_share',
        'check_balance',
        'station_revenue_share_distribution'
      ],
      derived_metrics: [
        'fill_rate_finance',
        'healthination_net_revenue_share_per',
        'frankly_net_revenue_share_percentage',
        'client_net_share'
      ],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [{ key: 'accounting_key', opcode: 'desc' }],
      limit: '',
      offset: ''
    };

    this.isSourceBreakDown = false;

    this.loadTableData(this.flatTableReq);
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['displayName']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    } else if (type === 'ppt') {
      const chartElements = document.getElementsByTagName('ym-charts');
      const slides = [];
      [].forEach.call(chartElements, (el, index) => {
        const c = el.getElementsByTagName('canvas')[0];

        slides.push({
          title: `${this.appConfig['displayName']}`,
          base64data: this.libServ.canvasToImage('white', c),
          width: 8,
          height: 3
        });

        if (index === chartElements.length - 1) {
          const pptConfig = {
            filename: 'Charts',
            logo: {
              visible: true,
              path: '/assets/images/ym.png',
              width: 0.53125,
              height: 0.458333
            },
            slides: slides
          };
          this.pptExport.exportPPT(pptConfig);
        }
      });
    }
    return false;
  }

  exportNotification() {
    this.confirmationService.confirm({
      message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
      header: 'Information',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {

      },
      reject: () => {
        // this.msgs = [{severity:'info', summary:'Rejected', detail:'You have rejected'}];
      }
    });
  }

  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }
}
