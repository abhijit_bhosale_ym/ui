import { Routes } from '@angular/router';
import { CampaignEnableComponent } from './campaign-enable.component';

export const CampaignEnableRoutes: Routes = [
  {
    path: '',
    component: CampaignEnableComponent
  }
];
