import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';

const headerOption = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json                                                                                                 ' })
}

@Injectable({
  providedIn: 'root'
})
export class FetchApiDataService {
  private BASE_URL: string = "http://localhost:3000/data";


  constructor(private http: HttpClient) { }

  getTableData() {
    return this.http.get(this.BASE_URL);
  }

  deleteTableRow(id) {
    return this.http.delete(this.BASE_URL + '/' + id);
  }

  updateTableRow(post) {

    return this.http.patch(this.BASE_URL + '/' + post.id, JSON.stringify({ isRead: true }))

  }

  editData(id, rowdata): Observable<object> {
    return this.http.put<Object>(this.BASE_URL + '/' + id, rowdata, headerOption);
  }
}
