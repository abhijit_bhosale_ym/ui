import { Component, OnInit, OnDestroy } from '@angular/core';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { CommonLibService } from 'src/app/_services';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';
import { FetchApiDataService } from './fetch-api-data.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { Subscription } from 'rxjs';
import { SendEmailComponent } from '../send-email/send-email.component';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { environment } from 'src/environments/environment';
import { DialogService } from 'primeng';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'ym-campaign-enable',
  templateUrl: './campaign-enable.component.html',
  styleUrls: ['./campaign-enable.component.scss']
})
export class CampaignEnableComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;
  timeZone = null;//environment.timeZone;
  appConfig: object = {};
  lastUpdatedOn: Date;
  nextUpdated: Date;
  dataUpdatedThrough: Date;

  cardsJson = [];
  showCards = true;
  isVideo = false;
  aggTableData: TreeNode[];
  noTableData = true; //make it false after dev complete
  VideoMetricColDef: any[];
  DisplayMetricColDef: any[];
  aggTableColumnDef: any[];
  aggTableJson: Object;
  displayAggTable = true; //make it false after dev complete
  toggleView = true;
  filtersApplied: object = {};
  exportRequest: ExportRequest = <ExportRequest>{};
  showhide = true;
  tableReq: object;
  oppsMissedTotal: object = {};
  oppsCPMTotal: object = {};
  rpmTotal: object = {};
  columnDefsOG: any[];
  hide_show = new FormControl();

  constructor(
    private dataFetchServ: FetchApiDataService,
    private libServ: CommonLibService,
    private toastService: ToastService,
    private appConfigService: AppConfigService,
    private _titleService: Title,
  ) { }

  hide_showOptions: any[];
  thirdPartyPayScheduleOptions: any[];
  thirdPartyDivOptions: any[];
  thirdPartyMappingOptions: any[];
  thirdPartyServeOptions: any[];
  cost_TypeOptions: any[];
  yes_NoOptions: any[];

  hide_showValue: any
  startDate: any;
  endDate: any;
  ngOnInit() {
    this.startDate = new Date();
    this.endDate = new Date();
    this.hide_showOptions = [
      { label: 'Show', value: 'show' },
      { label: 'Hide', value: 'hide' },
    ];

    this.thirdPartyPayScheduleOptions = [
      { label: 'API', value: 'API' },
      { label: 'Automated', value: 'Automated' },
      { label: 'Scheduled', value: 'Scheduled' },

    ];
    this.thirdPartyDivOptions = [
      { label: 'DV', value: 'DV' },
      { label: 'Null', value: 'null' },
    ];
    this.thirdPartyMappingOptions = [
      { label: 'LineItem', value: 'LineItem' },
      { label: 'Campaign', value: 'Campaign' },
      { label: 'Null', value: 'null' },

    ];
    this.cost_TypeOptions = [
      { label: 'CPM', value: 'CPM' },
      { label: 'CPVC', value: 'CPVC' },
    ];
    this.yes_NoOptions = [
      { label: 'Yes', value: 'Yes' },
      { label: 'No', value: 'No' },
    ];


    this.columnDefsOG = [

      {
        field: 'hide_show',
        displayName: 'Show / Hide',
        format: 'string',
        width: '80',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: false,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'campaign_id',
        displayName: 'CID',
        format: 'string',
        width: '125',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: false,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'server_campaign_name',
        displayName: 'Campaign Name',
        format: 'string',
        width: '165',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: false,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'flight_start',
        displayName: 'Start Date',
        format: 'date',
        width: '115',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: false,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'flight_end',
        displayName: 'End Date',
        format: 'date',
        width: '115',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: false,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      }
      ,
      {
        field: 'placement_monthly_splits',
        displayName: 'Monthly Split',
        format: 'string',
        width: '65',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: false,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      }

      ,
      {
        field: 'billing_ad_server',
        displayName: 'Billing Source',
        format: 'string',
        width: '135',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: false,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'io',
        displayName: 'IO',
        format: 'string',
        width: '135',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: false,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'cost_type',
        displayName: 'Cost Type',
        format: 'string',
        width: '135',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: false,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'third_party_site_served',
        displayName: '3rd Party Site Served',
        format: 'string',
        width: '65',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: false,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'impressions',
        displayName: 'Impressions',
        format: 'number',
        width: '135',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: false,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'budget',
        displayName: 'Budget',
        format: '',
        width: '135',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: true,
          colSearch: true,
          colSort: false,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'third_party_mapping_at',
        displayName: 'Third Party Mapping at',
        format: 'string',
        width: '115',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: false,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: '3rd_party_DV',
        displayName: '3rd Party DV',
        format: 'string',
        width: '110',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: false,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'dropbox_url',
        displayName: 'Dropbox Dealsheet Link',
        format: 'string',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: false,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'report_scheduled_by',
        displayName: 'Third Party Report Scheduled',
        format: 'string',
        width: '135',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: false,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: '3p_file_search_string',
        displayName: '3P File Search String',
        format: 'string',
        width: '135',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: true,
          colSearch: true,
          colSort: false,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'delete',
        displayName: 'Delete',
        format: 'string',
        width: '65',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      }
    ];

    this.aggTableJson = {
      page_size: 50,
      lazy: false,
      loading: false,
      export: true,
      reload: false,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '400px',
      totalRecords: 1000,

      // columns: this.columnDefsOG,
      // selectedColumns: this.columnDefsOG,
      columns: this.libServ.deepCopy(this.columnDefsOG),
      selectedColumns: this.libServ.deepCopy(this.columnDefsOG),
      frozenCols: this.libServ.deepCopy([...this.columnDefsOG.slice(0, 0)]),
      frozenWidth:
        this.columnDefsOG
          .slice(0, 0)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',



      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: false,
      columnSearch: true
    };
    this.getTableData();



    /* -------------------------------- Table End ------------------------------- */

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        this._titleService.setTitle(this.appConfig['displayName']);
        let startDate;
        let endDate;
        const date_config = this.appConfig['filter']['filterConfig']['filters'][
          'datePeriod'
        ][0];

        if ((date_config['defaultDate'][0]['value'] === 0 && date_config['defaultDate'][0]['period'] === 'month') && (moment().format('DD') === '01' || moment().format('DD') === '02')) {
          startDate = moment().subtract(1, 'months').startOf('month');
          endDate = moment().subtract(1, 'months').endOf('month');
        } else {
          if (
            date_config[
            'defaultDate'
            ][0]['startOf']
          ) {
            startDate = moment()
              .subtract(
                date_config['defaultDate'][0]['value'],
                date_config['defaultDate'][0]['period']
              )
              .startOf(
                date_config['defaultDate'][0]['period']
              );
          } else {
            startDate = moment().subtract(
              date_config['defaultDate'][0]['value'],
              date_config['defaultDate'][0]['period']
            );
          }
          endDate = moment().subtract(
            date_config[
            'defaultDate'
            ][1]['value'],
            date_config[
            'defaultDate'
            ][1]['period']
          );
        }
        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: startDate.format('YYYYMMDD'),
            time_key2: endDate.format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: this.appConfig['filter']['filterConfig']['groupBy'].filter(
            v => v.selected
          )
        };
        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        //  this.initialLoading();
      }
    });



  }
  TableData1: any;
  getTableData() {
    this.dataFetchServ.getTableData().subscribe(data => {
      if (data['status'] === 0) {
        this.noTableData = true;
        // this.toastService.displayToast({
        //   severity: 'error',
        //   summary: 'Server Error',
        //   detail: 'Please refresh the page'
        // });
        return;
      } else {
        this.noTableData = false;
      }
      // const tableData = data['data'];
      const tableData = data as [];
      const arr = [];
      tableData.forEach((row: object) => {
        arr.push({
          data: row,
          children: [{ data: {} }]
        });
      });
      this.aggTableData = <TreeNode[]>arr;

      this.aggTableJson['scrollable'] = true;
      this.aggTableJson['totalRecords'] = arr.length;
      setTimeout(() => {
        this.aggTableJson['loading'] = false;
      });
    });
  }

  onLazyLoadAggTable(e: Event) {
    //console.log('Lazy Agg', e);
  }


  isHiddenColumn(col: Object) {
    return (
      this.aggTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }


  deleteRow(Data) {


    this.dataFetchServ.deleteTableRow(Data).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      } else {

        this.toastService.displayToast({

          severity: 'success',

          summary: 'Data Deleted',

          detail: 'Data deleted successfully!'
        });

      }
      this.getTableData();
    });
  }

  updateTableRow(post) {
    this.dataFetchServ.updateTableRow(post).subscribe(data => {
    });
  }



  afterEditCell(e, rowdata, id, val, field) {


    // if (e.keyCode === 13) {

    if (typeof rowdata[field] !== 'undefined') {
      this.dataFetchServ.editData(id, rowdata).subscribe(data => {

        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          return;
        } else {

          this.toastService.displayToast({

            severity: 'success',

            summary: 'Data Updated',

            detail: 'Data updated successfully!'

          });

        }

        // this.initialLoading();
      });
    }
    // }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode == 46) { return true; }
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  numberWithComma(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode == 44) { return true; }
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }
}