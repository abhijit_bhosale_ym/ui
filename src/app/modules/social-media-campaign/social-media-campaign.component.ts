import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, from } from 'rxjs';
import { Title } from '@angular/platform-browser';
import { TreeNode } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import * as moment from 'moment';
import { CommonLibService, AppConfigService } from 'src/app/_services';
import { FetchApiDataService } from './fetch-api-data.service';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SendEmailComponent } from '../send-email/send-email.component';

@Component({
  selector: 'ym-social-media-campaign',
  templateUrl: './social-media-campaign.component.html',
  styleUrls: ['./social-media-campaign.component.scss']
})
export class SocialMediaCampaignComponent implements OnInit {
  private appConfigObs: Subscription;

  appConfig: object = {};
  filtersApplied: object = {};
  aggTableColumnDef: any[];
  aggTableJson: Object;
  displayAggTable = true;
  noTableData = true;
  aggTableData: TreeNode[];
  showMainLineChartSource = false;
  noDataMainLineChart = false;
  mainLineChartJson: object;

  noDataEngagementChart = false;
  mainEngagementChartJson: object;
  showEngagementChartSource = false;




  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private htmltoimage: HtmltoimageService,
    private pptExport: ExportPptService,
    private formatNumPipe: FormatNumPipe,
  ) { }

  ngOnInit(): void {

    this.aggTableColumnDef = [
      {
        field: 'start_date_id',
        displayName: 'Start Date',
        format: 'date',
        width: '150',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'end_date_id',
        displayName: 'End Date',
        format: 'date',
        width: '170',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'campaign',
        displayName: 'Campaign',
        format: '',
        width: '120',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'placement',
        displayName: 'placement',
        format: '',
        width: '270',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'followers',
        displayName: 'Followers',
        format: 'number',
        width: '120',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'social_reach',
        displayName: 'Social Reach',
        format: 'number',
        width: '270',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'social_engagements',
        displayName: 'Social Engagements',
        width: '155',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'social_engagement_rate',
        displayName: 'Social Engagement Rate',
        width: '155',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      }
    ];

    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      reload: false,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,

      columns: this.libServ.deepCopy(this.aggTableColumnDef),
      selectedColumns: this.libServ.deepCopy(this.aggTableColumnDef),
      // frozenCols: this.libServ.deepCopy([...this.dimColDef.slice(0, 3)]),
      // frozenWidth:
      //   this.dimColDef
      //     .slice(0, 3)
      //     .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: false,
      columnSearch: false
    };

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;

        this._titleService.setTitle(this.appConfig['displayName']);
        let startDate;
        let endDate;
        const date_config = this.appConfig['filter']['filterConfig']['filters'][
          'datePeriod'
        ][0];
        if ((date_config['defaultDate'][0]['value'] === 0 && date_config['defaultDate'][0]['period'] === 'month') && (moment().format('DD') === '01' || moment().format('DD') === '02')) {
          startDate = moment().subtract(1, 'months').startOf('month');
          endDate = moment().subtract(1, 'months').endOf('month');
        } else {
          if (
            date_config[
            'defaultDate'
            ][0]['startOf']
          ) {
            startDate = moment()
              .subtract(
                date_config['defaultDate'][0]['value'],
                date_config['defaultDate'][0]['period']
              )
              .startOf(
                date_config['defaultDate'][0]['period']
              );
          } else {
            startDate = moment().subtract(
              date_config['defaultDate'][0]['value'],
              date_config['defaultDate'][0]['period']
            );
          }
          endDate = moment().subtract(
            date_config[
            'defaultDate'
            ][1]['value'],
            date_config[
            'defaultDate'
            ][1]['period']
          );
        }

        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: startDate.format('YYYYMMDD'),
            time_key2: endDate.format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: {}
        };
        // this.exportRequest['appName'] = this.appConfig['name'].toString();
        // this.exportRequest['sendEmail'] = this.appConfig['user'][
        //   'contactEmail'
        // ].split(',');
        this.initialLoading();
      }
    });


  }

  initialLoading() {

    const tableReq = {
      dimensions: ['start_date_id', 'end_date_id', 'campaign', 'placement', 'followers', 'social_reach',
        'social_engagements', 'social_engagement_rate'], // [this.filtersApplied['groupby'][0]['key']],
      metrics: [],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: ['campaign_id'], // grpBys,
      orderBy: [{ key: 'start_date_id', opcode: 'desc' }],
      limit: '',
      offset: ''
    };
    this.loadTableData(tableReq);
    this.loadMainLineChart();
    this.loadEngagmentChart();
  }

  loadTableData(tableReq) {
    console.log("request of table=====", tableReq);
    this.aggTableJson['loading'] = true;
    this.dataFetchServ.getTableData(tableReq).subscribe(data => {
      console.log('data---', data);

      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      } else {
        this.noTableData = false;
      }
      const tableData = data['data'];
      const arr = [];
      tableData.forEach((row: object) => {
        arr.push({
          data: row,
          children: [{ data: {} }]
        });
      });
      this.aggTableData = <TreeNode[]>arr;
      this.aggTableJson['totalRecords'] = arr.length;
      setTimeout(() => {
        this.aggTableJson['loading'] = false;
      });
    });

  }

  loadMainLineChart() {
    const mainLineChartReqSource = {
      dimensions: ['start_date_id', 'end_date_id', 'social_reach',
        'social_engagements', 'social_engagement_rate', 'data_source', 'campaign'], // [this.filtersApplied['groupby'][0]['key']],
      metrics: [],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: ['campaign_id', 'data_source'], // grpBys,
      orderBy: [{ key: 'social_reach', opcode: 'asc' }],
      limit: '',
      offset: ''
    };
    const colors = this.libServ.dynamicColors(2);
    this.mainLineChartJson = {
      chartTypes: [{ key: 'bar', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'Social Reach',
            type: 'bar',
            yAxisID: 'y-axis-1',

            borderColor: colors[1],
            fill: false,
            backgroundColor: colors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'TOTAL POTENTIAL REACH'
        },
        legend: {
          display: true,
          onClick: e => e.stopPropagation()
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Sources'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-1',
              scaleLabel: {
                display: true,
                labelString: 'Social Reach'
              },
              position: 'left',
              name: '2',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, 'number', [0]);
                }
              }
              // scaleFontColor: "rgba(151,187,205,0.8)"
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    this.showMainLineChartSource = false;
    this.dataFetchServ.getTableData(mainLineChartReqSource).subscribe(data => {
      const chartData = data['data'];
      this.noDataMainLineChart = false;
      if (!chartData.length) {
        this.noDataMainLineChart = true;
        return;
      }
      const sourceArr = Array.from(new Set(chartData.map(r => r['data_source'])));

      // this.mainLineChartJson['chartData']['labels'] = chartData.map(r => r['source']);

      const revArr = [];
      const processedArray = [];
      // sourceArr.forEach(source => {
      chartData.forEach(r => {

        if (r['data_source'] == 'Facebook') {
          processedArray.push(
            (r['social_reach']),
          );
          revArr.push(r['data_source'])
        }
        if (r['data_source'] == 'Social Flow') {
          processedArray.push(
            (r['social_reach']),
          );
          revArr.push(r['data_source'])
        }
        if (r['data_source'] == 'Twitter') {
          processedArray.push(
            (r['social_reach']),
          );
          revArr.push(r['data_source'])
        }
        // if (r['source'] === source) {
        //   ecpmArr.push(r['gross_ecpm']);
        // }
      });
      // });
      this.mainLineChartJson['chartData']['labels'] = revArr;
      this.mainLineChartJson['chartData']['datasets'][0]['data'] = processedArray;
      this.showMainLineChartSource = true;
    });
  }

  loadEngagmentChart() {
    const mainLineChartReqSource = {
      dimensions: ['start_date_id', 'end_date_id', 'social_reach',
        'social_engagements', 'data_source', 'campaign'], // [this.filtersApplied['groupby'][0]['key']],
      metrics: [],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: ['campaign_id', 'data_source'], // grpBys,
      orderBy: [{ key: 'social_reach', opcode: 'asc' }],
      limit: '',
      offset: ''
    };
    const colors = this.libServ.dynamicColors(2);
    this.mainEngagementChartJson = {
      chartTypes: [{ key: 'bar', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'Social Reachsocial Engagements',
            type: 'bar',
            yAxisID: 'y-axis-1',

            borderColor: colors[1],
            fill: false,
            backgroundColor: colors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'TOTAL ENGAGEMENTS'
        },
        legend: {
          display: true,
          onClick: e => e.stopPropagation()
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Sources'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-1',
              scaleLabel: {
                display: true,
                labelString: 'Social Reachsocial Engagements'
              },
              position: 'left',
              name: '2',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, 'number', [0]);
                }
              }
              // scaleFontColor: "rgba(151,187,205,0.8)"
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    this.showEngagementChartSource = false;
    this.dataFetchServ.getTableData(mainLineChartReqSource).subscribe(data => {
      const chartData = data['data'];
      this.noDataEngagementChart = false;
      if (!chartData.length) {
        this.noDataEngagementChart = true;
        return;
      }
      const sourceArr = Array.from(new Set(chartData.map(r => r['data_source'])));

      // this.mainLineChartJson['chartData']['labels'] = chartData.map(r => r['source']);

      const revArr = [];
      const processedArray = [];
      // sourceArr.forEach(source => {
      chartData.forEach(r => {

        if (r['data_source'] == 'Facebook') {
          processedArray.push(
            (r['social_engagements']),
          );
          revArr.push(r['data_source'])
        }
        if (r['data_source'] == 'Social Flow') {
          processedArray.push(
            (r['social_engagements']),
          );
          revArr.push(r['data_source'])
        }
        if (r['data_source'] == 'Twitter') {
          processedArray.push(
            (r['social_engagements']),
          );
          revArr.push(r['data_source'])
        }
        // if (r['source'] === source) {
        //   ecpmArr.push(r['gross_ecpm']);
        // }
      });
      // });
      this.mainEngagementChartJson['chartData']['labels'] = revArr;
      this.mainEngagementChartJson['chartData']['datasets'][0]['data'] = processedArray;
      this.showEngagementChartSource = true;
    });
  }

  onFiltersApplied(filterData: object) {
    this.filtersApplied['filters']['dimensions'] = [];

    // tslint:disable-next-line: forin
    for (const k in filterData['filter']['dimensions']) {
      if (filterData['filter']['dimensions'][k].length !== 0) {
        this.filtersApplied['filters']['dimensions'].push({
          key: k,
          values: filterData['filter']['dimensions'][k]
        });
      }
    }

    // this.filtersApplied['groupby'] = filterData['groupby'];
    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];

    const tableReq = {
      dimensions: ['start_date_id', 'end_date_id', 'campaign', 'placement', 'followers', 'social_reach',
        'social_engagements', 'social_engagement_rate'], // [this.filtersApplied['groupby'][0]['key']],
      metrics: [],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: ['campaign_id'], // grpBys,
      orderBy: [{ key: 'start_date_id', opcode: 'desc' }],
      limit: '',
      offset: ''
    };
    this.loadTableData(tableReq);
    this.loadMainLineChart();
    this.loadEngagmentChart();
  }

  sendEmail() {
    const imageConfig = {
      imgName: `${this.appConfig['name']}.png`,
      type: 'image/png'
    };

    this.htmltoimage
      .canvasImage(document.getElementById('capture'), imageConfig)
      .then(canvas => {
        this.dialogService.open(SendEmailComponent, {
          header: 'Email Screenshot',
          contentStyle: { 'max-height': '80vh', overflow: 'auto' },
          data: canvas.toDataURL('image/png')
        });
      });
  }
  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['name']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    } else if (type === 'ppt') {
      const chartElements = document.getElementsByTagName('ym-charts');
      const slides = [];
      [].forEach.call(chartElements, (el, index) => {
        const c = el.getElementsByTagName('canvas')[0];

        slides.push({
          title: `${this.appConfig['name']}`,
          base64data: this.libServ.canvasToImage('white', c),
          width: 8,
          height: 3
        });

        if (index === chartElements.length - 1) {
          const pptConfig = {
            filename: 'Charts',
            logo: {
              visible: true,
              path: '/assets/images/ym.png',
              width: 0.53125,
              height: 0.458333
            },
            slides: slides
          };
          this.pptExport.exportPPT(pptConfig);
        }
      });
    }
    return false;
  }

}
