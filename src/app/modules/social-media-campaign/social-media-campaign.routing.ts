import { Routes } from '@angular/router';

import { SocialMediaCampaignComponent } from './social-media-campaign.component';
export const SocialMediaCampaignRoutes: Routes = [
  {
    path: '',
    component: SocialMediaCampaignComponent
  }
];
