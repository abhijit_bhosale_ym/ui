import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FilterContainerModule } from '../common/filter-container/filter-container.module';
import { SocialMediaCampaignRoutes } from './social-media-campaign.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormatNumPipe } from '../../_pipes/number-format.pipe';
import { SharedModule } from '../../_pipes/shared.module';
import { ChatModule } from '../common/chat/chat.module';
import { TreeTableModule } from 'primeng/treetable';
import { MultiSelectModule } from 'primeng/multiselect';
import { InputSwitchModule } from 'primeng/inputswitch';
import { SocialMediaCampaignComponent } from './social-media-campaign.component';
import { ChartsModule } from '../common/charts/charts.module';
import { CardsModule } from '../common/cards/cards.module';


@NgModule({
  declarations: [SocialMediaCampaignComponent],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    FilterContainerModule,
    TreeTableModule,
    MultiSelectModule,
    InputSwitchModule,
    ChartsModule,
    CardsModule,
    RouterModule.forChild(SocialMediaCampaignRoutes)
  ],
  providers: [FormatNumPipe]
})
export class SocialMediaCampaignModule {}
