import { Component, OnInit, OnDestroy } from '@angular/core';
import { SelectItemGroup } from 'primeng/api';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { FetchApiDataService } from '../../fetch-api-data.service';
import * as moment from 'moment';
// import { Subscription } from 'rxjs';
import { CommonLibService } from 'src/app/_services';
// import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { DynamicDialogConfig, DynamicDialogRef, DialogService } from 'primeng/dynamicdialog';
@Component({
  selector: 'ym-main-form',
  templateUrl: './main-form.component.html',
  styleUrls: ['./main-form.component.scss']
})
export class MainFormComponent implements OnInit {
  // private appConfigObs: Subscription;

  appConfig: object = {};
  registerForm: FormGroup;
  isAllProperty = false;
  isCreateTeam = false;
  groupedYears: SelectItemGroup[];
  groupedMediaGroups: SelectItemGroup[];
  groupedProperty: SelectItemGroup[];
  fileName = '';
  fileData: any[] = [];
  signName = '';
  signimage;
  signData: File;
  userDetails: {
    name: '';
  };
  RowData: object = {};
  FormData: object = {};
  constructor(
    private formBuilder: FormBuilder,
    public router: Router,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    // private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private config: DynamicDialogConfig,
    private dynamicDialogRef: DynamicDialogRef
  ) {
    this.RowData = this.config.data['rowData'];
    this.FormData = this.config.data['row'];

  }

  ngOnInit() {

    this.registerForm = this.formBuilder.group({
      sitename: [this.FormData['site_name'], [Validators.required]],
      bankholder: [this.FormData['bank_holder'], [Validators.required]],
      bankname: [this.FormData['bank_address'], [Validators.required]],
      bankaddress: [this.FormData['bank_name'], [Validators.required]],
      routing: [this.FormData['routing'], [Validators.required]],
      account: [this.FormData['account'], [Validators.required]],
      authorizedperson: [this.FormData['authorized_person'], [Validators.required]],
      date: [this.FormData['date'] ? moment(this.FormData['date'], 'YYYYMMDD').format("YYYY-MM-DD") : moment().format("YYYY-MM-DD"), [Validators.required]]

    });

    if (this.FormData['signature_path']) {

      this.signimage = this.FormData['signature_path'];
    }

    console.log('this.FormData', this.FormData);

    if (this.FormData['id']) {
      this.getBankingAttachment();
    }

  }


  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  getBankingAttachment() {
    this.dataFetchServ.getBankingAttachment(this.FormData['id']).subscribe(res => {
      res['data'].forEach(element => {
        this.fileData.push({
          isSave: true,
          id: element.id,
          fileName: element.file_name,
          data: element.file_path
        });
      });

    });
  }
  signChangeEvent(event: any): void {
    console.log('evenet', event)
    if (event.target.files.length > 0) {
      this.signName = event.target.files[0].name;
      this.signData = event.target.files[0];
    }
    var myReader: FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.signimage = myReader.result;
    }
    myReader.readAsDataURL(this.signData);
  }
  fileChangeEvent(event: any): void {
    console.log('evenet', event)
    for (let i = 0; i < event.target.files.length; i++) {

      this.fileData.push({
        isSave: false,
        fileName: event.target.files[i].name,
        data: event.target.files[i]
      });
    }
    console.log('evenet', event, this.fileData)
  }
  deleteattach(i) {
    this.fileData.splice(i, 1);
  }
  onSubmit() {
    console.log('this.registerForm ', this.registerForm);
    console.log('fileData', this.fileData)
    const fd = new FormData();
    if (this.FormData['id']) {
      fd.append('status', 'update');
    } else {
      fd.append('status', 'insert');
    }
    if (this.signName != '') {
      fd.append('signData', this.signData, this.signName);
      fd.append('signstatus', 'signupdate');
    } else {
      fd.append('signstatus', 'nosignupdate');
      fd.append('signature_path', this.FormData['signature_path']);
    }

    fd.append('user_id', this.RowData['user_id']);
    fd.append('site_name', this.registerForm.value.sitename);
    fd.append('bank_holder', this.registerForm.value.bankholder);
    fd.append('bank_name', this.registerForm.value.bankname);
    fd.append('bank_address', this.registerForm.value.bankaddress);
    fd.append('routing', this.registerForm.value.routing);
    fd.append('account', this.registerForm.value.account);
    fd.append('authorized_person', this.registerForm.value.authorizedperson);
    fd.append('date', moment(this.registerForm.value.date).format("YYYYMMDD"));
    fd.append('attachment', this.fileData.length.toString());
    fd.append('id', this.FormData['id']);
    fd.append('attachmentupload', this.fileData.filter(x => x.isSave == false).length.toString());
    fd.append('attachmentData', JSON.stringify(this.fileData.filter(x => x.isSave == true)));
    this.fileData.filter(x => x.isSave == false).forEach((file, i) => {
      fd.append('attachmentFile[' + i + ']', file['data'], file['fileName']);
    });
    if (this.isAllProperty) {
      fd.append('derived_station_rpt_id', this.FormData['derived_station_rpt_ids']);

    } else {
      fd.append('derived_station_rpt_id', this.FormData['derived_station_rpt_main_id']);
      // fd.append('year', moment().format('YYYY'));
    }

    this.dataFetchServ.UpdateBankingForm(fd).subscribe(data => {
      if (data['status']) {
        this.toastService.displayToast({
          severity: 'success',
          summary: 'Upload Files',
          detail: 'Banking form uploaded successfully'
        });
        this.dialogService.dialogComponentRef.destroy();
      } else {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Something went wrong',
          detail: 'Error updated banking form'
        });
      }
    });
  }

  goBack() {
    // this.router.navigate(['/users']);
    this.dialogService.dialogComponentRef.destroy();
  }
  // ngOnDestroy() {
  //   if (this.appConfigObs && !this.appConfigObs.closed) {
  //     this.appConfigObs.unsubscribe();
  //   }
  // }
}
