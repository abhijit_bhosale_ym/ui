import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { TreeNode } from 'primeng/api';

@Injectable({
  providedIn: 'root'
})
export class FetchApiDataService {
  private BASE_URL: string = environment.baseUrl;

  constructor(private http: HttpClient) { }

  getpaymentData(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/payment-contact-info/getData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
  }

  deleteTaxForm(id) {
    const url = `${this.BASE_URL}/frankly/v1/payment-contact-info/delete-tax-form/${id}`;
    return this.http.post(url, id);
  }

  deleteBankingForm(id) {
    const url = `${this.BASE_URL}/frankly/v1/payment-contact-info/delete-banking-form/${id}`;
    return this.http.post(url, id);
  }

  deleteContactForm(id) {
    const url = `${this.BASE_URL}/frankly/v1/payment-contact-info/delete-contact-form/${id}`;
    return this.http.post(url, id);
  }

  downloadTaxForm(req) {
    const url = `${this.BASE_URL}/frankly/v1/payment-contact-info/download-tax-form`;
    return this.http.post(url, req);
  }

  UploadTaxForm(req) {
    const url = `${this.BASE_URL}/frankly/v1/payment-contact-info/upload-tax-form`;
    return this.http.post(url, req);
  }

  UpdateBankingForm(req) {
    const url = `${this.BASE_URL}/frankly/v1/payment-contact-info/update-banking-form`;
    return this.http.post(url, req);
  }
  getBankingAttachment(id) {
    const url = `${this.BASE_URL}/frankly/v1/payment-contact-info/getBankingAttachment/${id}`;
    return this.http.get(url);
  }
  UpdateContactForm(req) {
    const url = `${this.BASE_URL}/frankly/v1/payment-contact-info/update-contact-form`;
    return this.http.post(url, req);
  }
  updatePaymentMethod(req) {
    const url = `${this.BASE_URL}/frankly/v1/payment-contact-info/update-payment-method`;
    return this.http.post(url, req);
  }

  loadDimdata(id) {
    const url = `${this.BASE_URL}/frankly/v1/payment-contact-info/getPropertyByStationGroup/${id}`;
    return this.http.get(url);
  }

  getPaymentMethod() {
    const url = `${this.BASE_URL}/frankly/v1/payment-contact-info/getPaymentMethod`;
    return this.http.get(url);
  }

  getTaxFormData(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/payment-contact-info/tax-form/getData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
  }

  getBankingFormData(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/payment-contact-info/banking-form/getData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
  }

  getContactFormData(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/payment-contact-info/contact-form/getData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
  }

  getExportReportData(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/common/exportData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
    // return <TreeNode[]> json.data;
  }

  getLastUpdatedData(appId) {
    const url = `${this.BASE_URL}/frankly/v1/common/getLastUpdatedData/${appId}`;
    return this.http.get(url);
  }
}
