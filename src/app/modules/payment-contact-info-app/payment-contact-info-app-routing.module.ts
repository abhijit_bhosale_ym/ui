import { Routes } from '@angular/router';
import { PaymentContactInfoAppComponent } from './payment-contact-info-app.component';

export const PaymentContactInfoAppRoutes: Routes = [
  {
    path: '',
    component: PaymentContactInfoAppComponent
  }
];
