import { Component, OnInit, OnDestroy } from '@angular/core';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { CommonLibService } from 'src/app/_services';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';
import { FetchApiDataService } from '../payment-contact-info-app/fetch-api-data.service';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { Subscription } from 'rxjs';
import { SendEmailComponent } from '../send-email/send-email.component';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { TaxFormComponent } from './tax-form/tax-form.component';

import { ChatComponent } from '../common/chat/chat/chat.component';
import { CommentJson } from 'src/app/_interfaces/commentJson';
import { CommentService } from '../common/chat/comment.service';
import { BankingFormComponent } from './banking-form/banking-form.component';
import { ContactFormComponent } from './contact-form/contact-form.component';
import { PaymentMethodFormComponent } from './payment-method/payment-method-form.component';
import { DialogService } from 'primeng';

@Component({
  selector: 'ym-payment-contact-info-app',
  templateUrl: './payment-contact-info-app.component.html',
  styleUrls: ['./payment-contact-info-app.component.scss']
})
export class PaymentContactInfoAppComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;

  appConfig: object = {};
  lastUpdatedOn: Date;
  nextUpdated: Date;
  dataUpdatedThrough: Date;
  showCards = true;
  aggTableData: TreeNode[];
  noTableData = false;
  dimColDef: any[];
  //  metricColDef: any[];
  aggTableColumnDef: any[];
  aggTableJson: Object;
  displayAggTable = true;
  isUpdateStatus = false;
  filtersApplied: object = {};
  exportRequest: ExportRequest = <ExportRequest>{};
  bellIconLoaded = false;
  commentData = [];
  paymentMethodLists: any[];
  chatcount = 0;
  commentJson: CommentJson = <CommentJson>{};

  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private htmltoimage: HtmltoimageService,
    private exportService: ExportdataService,
    private pptExport: ExportPptService,
    private confirmationService: ConfirmationService,
    private commentservice: CommentService,
  ) { }

  ngOnInit() {


    this.dimColDef = [
      {
        field: 'station_group',
        displayName: 'Media Group',
        format: '',
        width: '300',
        isExpanded: 'false',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'derived_station_rpt',
        displayName: 'Property',
        format: '',
        width: '300',
        isExpanded: 'false',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'tax_forms',
        displayName: 'Tax Forms',
        format: '',
        width: '130',
        exportConfig: {
          format: '',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'banking_info',
        displayName: 'Banking Info',
        width: '130',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'contact_info',
        displayName: 'Contact_info',
        width: '130',
        exportConfig: {
          format: '',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'method_type',
        displayName: 'Payment Method',
        format: '',
        width: '180',
        exportConfig: {
          format: '',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      }
    ];

    // this.metricColDef = [

    // ];
    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: false,
      reload: false,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,

      columns: this.libServ.deepCopy(this.dimColDef.slice(2)),
      selectedColumns: this.libServ.deepCopy(this.dimColDef.slice(2)),
      frozenCols: this.libServ.deepCopy([...this.dimColDef.slice(0, 2)]),
      frozenWidth:
        this.dimColDef
          .slice(0, 2)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: false,
      overallSearch: false,
      columnSearch: false
    };

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        console.log('appConfig', this.appConfig);
        this.isUpdateStatus = this.appConfig['permissions'].some(
          o => o.name == 'update-payment-method'
        );

        // this.isPopupChartView = this.appConfig['permissions'].some(
        //   o => o.name == 'frank-payout-view-charts-popup'
        // );
        this._titleService.setTitle(this.appConfig['displayName']);

        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: '',
            time_key2: ''
          },
          filters: { dimensions: [], metrics: [] },
          groupby: [{ key: 'station_group' }] // this.appConfig["filter"]["filterConfig"]["groupBy"].filter(
          // v => v.selected
          // )
        };
        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');

        this.commentJson['app_id'] = this.appConfig['id'];
        this.commentJson['user_id'] = this.appConfig['user']['id'];
        this.commentJson['receiver'] = this.appConfig['route'];
        this.commentJson['user_name'] = this.appConfig['user']['userName'];
        this.initialLoading();
        this.UpdateBellIcon();
      }
    });
  }
  UpdateBellIcon() {
    const req1 = {
      user_id: this.appConfig['user']['id'],
      app_id: this.appConfig['id']
    };

    this.commentservice
      .getAppBellCommentData(req1)
      .subscribe(commentData => {
        if (commentData['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          return;
        }
        this.bellIconLoaded = false;
        this.commentData = commentData['data'];
        this.chatcount = this.commentData.filter(x => x.is_read === 'N').length;
        setTimeout(() => {
          this.bellIconLoaded = true;
          console.log('bell', this.commentData, this.chatcount, this.bellIconLoaded);
        }, 0);


      });
  }

  clickOnComment(comment: object) {
    console.log('comment......', comment);
    const req1 = {
      user_id: this.appConfig['user']['id'],
      app_id: this.appConfig['id']
    };

    this.commentservice
      .getAppCommentData(req1)
      .subscribe(commentData => {
        if (commentData['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          return;
        }
        // this.commentData = commentData['data'];
        if (typeof comment['config'] == 'string') {
          this.commentJson['config'] = JSON.parse(comment['config']);
        } else {
          this.commentJson['config'] = comment['config'];
        }

        const data = {};
        const CommentSortData = [];
        commentData['data'].forEach(element => {
          const commentconfig = JSON.parse(element.config);
          if (this.commentJson['config']['formType'] == commentconfig.formType && this.commentJson['config']['id'] == commentconfig.id) {
            CommentSortData.push(element);
          }
        });
        data['commentJson'] = this.commentJson;
        data['commentData'] = CommentSortData;
        const chatSetting = {
          showChatMsg: false,
          title: `${this.commentJson['config']['comment_on']}`,
          showCommentTextArea: true,
          chatURL: '/payment-contact-info/send/message',
          data: data
        };
        console.log('chatSetting', chatSetting);

        const ref = this.dialogService.open(ChatComponent, {
          header: `Comment on ${this.commentJson['config']['comment_on']}`,
          contentStyle: { width: '30vw', height: '370px', overflow: 'auto' },
          data: chatSetting,
          width: '30vw'
        });

        ref.onClose.subscribe((data: string) => {
          // let UnreadCommentIdList = CommentSortData.filter(o => o.is_read == "N" && o.user_id != this.commentJson['user_id']).map(x => x.comment_id);

          // console.log('popup close', CommentSortData, UnreadCommentIdList);
          // if (UnreadCommentIdList.length > 0) {
          this.UpdateBellIcon();
          // }
        });
      });

  }

  initialLoading() {
    this.dataFetchServ
      .getLastUpdatedData(this.appConfig['id'])
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        this.lastUpdatedOn = moment(data[0]['updated_at']).toDate();
        this.nextUpdated = moment(data[0]['next_run_at']).toDate();
        this.dataUpdatedThrough = moment(
          data[0]['source_updated_through'],
          'YYYYMMDD'
        ).toDate();
      });

    const tableReq = {
      dimensions: ['station_group'], // [this.filtersApplied['groupby'][0]['key']],
      metrics: [],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: ['station_group'], // grpBys,
      orderBy: [{ key: 'station_group', opcode: 'asc' }],
      limit: '',
      offset: ''
    };
    this.getPaymentMethod();
    this.loadTableData(tableReq);
  }

  getPaymentMethod() {
    // this.reloadAggTable();
    this.aggTableJson['loading'] = true;


    this.dataFetchServ.getPaymentMethod().subscribe(data => {
      this.paymentMethodLists = data['data'];
    });
  }
  onChangeStatus(row) {
    console.log(row);
    this.confirmationService.confirm({
      message: 'Are you sure you want to change update status?',
      accept: () => {
        const req = {
          method_type: row['method_type']['key'],
          derived_station_rpt_id: [row['derived_station_rpt_id']],
          user_id: this.appConfig['user']['id']
        };

        this.dataFetchServ.updatePaymentMethod(req).subscribe(data => {
          if (data['status']) {
            this.toastService.displayToast({
              severity: 'success',
              summary: 'Payment Method Update',
              detail: 'Payment method updated successfully'
            });
            row['payment_method_id'] = row['method_type']['key'];
          } else {
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Server Error',
              detail: 'Please refresh the page'
            });
          }
        });
      },
      reject: () => {
        row['method_type'] = this.paymentMethodLists.find(x => x.key == row['payment_method_id']);
      }
    });
  }
  resetPagination(tt1) {
    if (typeof tt1 !== 'undefined') {
      tt1.reset();
    }
  }

  loadTableData(params) {
    // this.reloadAggTable();
    this.aggTableJson['loading'] = true;


    this.dataFetchServ.getpaymentData(params).subscribe(data => {
      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      } else {
        this.noTableData = false;
      }
      const grpBysFilter = ['station_group', 'derived_station_rpt'];

      const tableData = data['data'];
      const arr = [];
      tableData.forEach((row: object) => {
        for (let i = 0; i < grpBysFilter.length; i++) {
          if (i !== grpBysFilter.length - 1) {
            row['derived_station_rpt'] = 'All';
          }
        }
        let obj = {};
        if (grpBysFilter.length === 1) {
          obj = {
            data: row
          };
        } else {
          obj = {
            data: row,
            children: [{ data: {} }]
          };
        }

        obj['data']['isExpanded'] = '0';
        arr.push(obj);
      });

      this.aggTableData = <TreeNode[]>arr;
      this.aggTableJson['totalRecords'] = data['totalItems'];
      this.aggTableJson['loading'] = false;
    });
  }

  reloadAggTable() {
    this.displayAggTable = false;
    setTimeout(() => {
      this.displayAggTable = true;
    }, 0);
  }


  onTableDrill(e: Event) {
    console.log('Drilled', e);
    if (!e['node']['childLoaded']) {
      this.aggTableJson['loading'] = true;
      const tableReq = {
        dimensions: ['station_group', 'derived_station_rpt'],
        metrics: [],
        derived_metrics: [],
        timeKeyFilter: {},
        filters: this.libServ.deepCopy(this.filtersApplied['filters']),
        groupByTimeKey: {
          key: [],
          interval: 'daily'
        },
        gidGroupBy: ['derived_station_rpt', 'station_group'], // this.getGrpBys(),
        orderBy: [],
        limit: '',
        offset: ''
      };

      // const timeKey1 = e['node']['data']['accounting_key'];
      // const timeKey2 = moment(timeKey1, 'YYYYMMDD')
      //   .endOf('month')
      //   .format('YYYYMMDD');

      // tableReq['timeKeyFilter'] = { time_key1: timeKey1, time_key2: timeKey2 };

      const grpBys = ['derived_station_rpt', 'station_group']; // this.filtersApplied['groupby'].map(e => e.key);
      for (const g of grpBys) {
        if (e['node']['data'][g] !== 'All') {
          // tableReq['dimensions'].push(g);
          if (!tableReq['gidGroupBy'].includes(g)) {
            tableReq['gidGroupBy'].push(g);
          }
          if (
            tableReq['filters']['dimensions'].findIndex(e => e.key === g) === -1
          ) {
            tableReq['filters']['dimensions'].push({
              key: g,
              values: [e['node']['data'][g]]
            });
          } else {
            tableReq['filters']['dimensions'].splice(
              tableReq['filters']['dimensions'].findIndex(e => e.key === g),
              1
            );
            tableReq['filters']['dimensions'].push({
              key: g,
              values: [e['node']['data'][g]]
            });
            if (
              tableReq['filters']['dimensions']
                .find(e => e.key === g)
              ['values'].findIndex(v => v === e['node']['data'][g]) === -1
            ) {
              tableReq['filters']['dimensions'].push(e['node']['data'][g]);
            }
          }
        }
        // else {
        //   tableReq['dimensions'].push(g);
        //   if (!tableReq['gidGroupBy'].includes(g)) {
        //     tableReq['gidGroupBy'].push(g);
        //   }
        //   break;
        // }
      }

      // tableReq['gidGroupBy'] = this.getGrpBys();
      // For Dev purpose
      // tableReq['gidGroupBy'] = [
      //   'ad_size',
      //   'ad_type',
      //   'ad_units',
      //   'bidding_type',
      //   'device_category',
      //   'geography',
      //   'sales_channel',
      //   'sales_type',
      //   'site',
      //   'source'
      // ];

      this.dataFetchServ.getpaymentData(tableReq).subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        const childData = data['data'];
        const arr = [];

        childData.forEach((row: object) => {
          row['method_type'] = { key: row['payment_method_id'], label: row['method_type'] }
          this.aggTableJson['frozenCols'].forEach(r => {
            if (typeof row[r['field']] === 'undefined') {
              row[r['field']] = 'All';
            }
          });
          let obj = {};
          if (row[grpBys[grpBys.length - 1]] === 'All') {
            obj = {
              data: row,
              children: [{ data: {} }]
            };
          } else {
            obj = {
              data: row
            };
          }
          obj['data']['isExpanded'] = '1';
          arr.push(obj);
        });

        this.aggTableJson['loading'] = false;
        e['node']['children'] = <TreeNode[]>arr;
        this.aggTableData = [...this.aggTableData];
        e['node']['childLoaded'] = true;
      });
    }
  }

  onLazyLoadAggTable(e: Event) {
    console.log('Lazy Agg', e);
  }

  openPopup(row, field, rowData) {
    const grpBys = ['station_group'];
    const filtersApplied = this.libServ.deepCopy(this.filtersApplied);

    const isStationGrpAvail = filtersApplied['filters']['dimensions'].findIndex(
      item => item.key == 'station_group'
    );

    filtersApplied['timeKeyFilter']['time_key1'] = rowData['accounting_key'];
    filtersApplied['timeKeyFilter']['time_key2'] = moment(
      rowData['accounting_key'],
      'YYYYMMDD'
    )
      .endOf('month')
      .format('YYYYMMDD');

    filtersApplied['filters']['dimensions'].splice(
      filtersApplied['filters']['dimensions'].findIndex(
        item => item.key == 'station_group'
      ),
      1
    );

    filtersApplied['filters']['dimensions'].push({
      key: 'station_group',
      values: [rowData['station_group']]
    });

    if (rowData['derived_station_rpt'] != 'All') {
      filtersApplied['filters']['dimensions'].splice(
        filtersApplied['filters']['dimensions'].findIndex(
          item => item.key == 'derived_station_rpt'
        ),
        1
      );

      filtersApplied['filters']['dimensions'].push({
        key: 'derived_station_rpt',
        values: [rowData['derived_station_rpt']]
      });
    }

    filtersApplied['groupby'] = grpBys;
    const data = {
      filters: filtersApplied,
      field: row,
      // isPopupChartView: this.isPopupChartView,
      // isPopupGroupbyView: this.isPopupGroupbyView,
      exportRequest: this.exportRequest
    };
    // const ref = this.dialogService.open(PaymentContactInfoDailyDataPopupComponent, {
    //   header: row + ' Daily Data ',
    //   contentStyle: { width: '80vw', overflow: 'auto' },
    //   data: data
    // });
    // ref.onClose.subscribe((data: string) => { });
  }


  isHiddenColumn(col: Object) {
    return (
      this.aggTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }

  /* --------------------- On Filter Apply Button Clicked --------------------- */

  onFiltersApplied(filterData: object) {
    this.filtersApplied['filters']['dimensions'] = [];

    // tslint:disable-next-line: forin
    for (const k in filterData['filter']['dimensions']) {
      if (filterData['filter']['dimensions'][k].length !== 0) {
        this.filtersApplied['filters']['dimensions'].push({
          key: k,
          values: filterData['filter']['dimensions'][k]
        });
      }
    }

    // this.filtersApplied['groupby'] = filterData['groupby'];
    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];

    const tableReq = {
      dimensions: ['station_group'],
      metrics: [],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys(), // this.filtersApplied['filters']['dimensions'].map(e => e.key), // grpBys,
      orderBy: [{ key: 'station_group', opcode: 'asc' }],
      limit: '',
      offset: ''
    };

    this.loadTableData(tableReq);
  }

  getGrpBys() {
    let grpBys = this.filtersApplied['groupby'].map(e => e.key);
    // let grpBys = [this.filtersApplied['groupby'][0]['key']];
    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          grpBys.concat(
            this.filtersApplied['filters']['dimensions']
              .filter(f => f.values.length)
              .map(m => m.key)
          )
        )
      );
    }
    return grpBys;
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['name']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    } else if (type === 'ppt') {
      const chartElements = document.getElementsByTagName('ym-charts');
      const slides = [];
      [].forEach.call(chartElements, (el, index) => {
        const c = el.getElementsByTagName('canvas')[0];

        slides.push({
          title: `${this.appConfig['name']}`,
          base64data: this.libServ.canvasToImage('white', c),
          width: 8,
          height: 3
        });

        if (index === chartElements.length - 1) {
          const pptConfig = {
            filename: 'Charts',
            logo: {
              visible: true,
              path: '/assets/images/ym.png',
              width: 0.53125,
              height: 0.458333
            },
            slides: slides
          };
          this.pptExport.exportPPT(pptConfig);
        }
      });
    }
    return false;
  }

  openTaxForms(row) {
    console.log('row data tax', row);

    const data = {
      config: this.appConfig,
      rowData: row,
      commentJson: this.commentJson
    };
    console.log(' row.isExpand', row.isExpanded == '0', row.isExpanded)
    const ref = this.dialogService.open(TaxFormComponent, {
      header: 'Tax Forms : ' + (row.isExpanded == '0' ? row.station_group : row.derived_station_rpt),
      contentStyle: { width: '80vw', overflow: 'auto' },
      data: data
    });
    ref.onClose.subscribe((data: string) => {
      console.log("tax closeeeeee")
      this.UpdateBellIcon();
    });
  }

  openBankingInfo(row) {
    console.log('row data bank', row);

    const data = {
      config: this.appConfig,
      rowData: row,
      commentJson: this.commentJson
    };
    console.log(' row.isExpand', row.isExpanded == '0', row.isExpanded)
    const ref = this.dialogService.open(BankingFormComponent, {
      header: 'Tax Forms : ' + (row.isExpanded == '0' ? row.station_group : row.derived_station_rpt),
      contentStyle: { width: '80vw', overflow: 'auto' },
      data: data
    });
    ref.onClose.subscribe((data: string) => {
      this.UpdateBellIcon();
    });
  }

  openContactInfo(row) {
    console.log('row data contact', row);
    const data = {
      config: this.appConfig,
      rowData: row,
      commentJson: this.commentJson
    };
    console.log(' row.isExpand', row.isExpanded == '0', row.isExpanded)
    const ref = this.dialogService.open(ContactFormComponent, {
      header: 'Contact Forms : ' + (row.isExpanded == '0' ? row.station_group : row.derived_station_rpt),
      contentStyle: { width: '80vw', overflow: 'auto' },
      data: data
    });
    ref.onClose.subscribe((data: string) => {
      this.UpdateBellIcon();
    });
  }

  openPaymentInfo(row) {
    console.log('row data payment', row);
    row['user_id'] = this.appConfig['user']['id'];
    row['paymentMethodLists'] = this.paymentMethodLists;
    const ref = this.dialogService.open(PaymentMethodFormComponent, {
      header: 'Contact Forms : ' + row.station_group,
      contentStyle: { width: '60vw', overflow: 'auto' },
      data: row
    });
    ref.onClose.subscribe((data: string) => {
    });
  }



  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }

  sendEmail() {
    const imageConfig = {
      imgName: `${this.appConfig['name']}.png`,
      type: 'image/png'
    };

    this.htmltoimage
      .canvasImage(document.getElementById('capture'), imageConfig)
      .then(canvas => {
        this.dialogService.open(SendEmailComponent, {
          header: 'Email Screenshot',
          contentStyle: {
            'max-height': '80vh',
            width: '30vw',
            overflow: 'auto'
          },
          data: canvas.toDataURL('image/png')
        });
      });
  }
}
