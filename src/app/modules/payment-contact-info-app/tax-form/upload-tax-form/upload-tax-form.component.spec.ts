import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadTaxFormComponent } from './upload-tax-form.component';

describe('UploadTaxFormComponent', () => {
  let component: UploadTaxFormComponent;
  let fixture: ComponentFixture<UploadTaxFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UploadTaxFormComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadTaxFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
