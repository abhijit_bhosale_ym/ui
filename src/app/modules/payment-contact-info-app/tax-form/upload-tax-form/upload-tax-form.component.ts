import { Component, OnInit, OnDestroy } from '@angular/core';
import { SelectItemGroup } from 'primeng/api';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { FetchApiDataService } from '../../fetch-api-data.service';
import * as moment from 'moment';
// import { Subscription } from 'rxjs';
import { CommonLibService } from 'src/app/_services';
// import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { DynamicDialogConfig, DynamicDialogRef, DialogService } from 'primeng/dynamicdialog';
@Component({
  selector: 'ym-upload-tax-form',
  templateUrl: './upload-tax-form.component.html',
  styleUrls: ['./upload-tax-form.component.scss']
})
export class UploadTaxFormComponent implements OnInit {
  // private appConfigObs: Subscription;

  appConfig: object = {};
  registerForm: FormGroup;
  isCreateRole = false;
  isCreateTeam = false;
  groupedYears: SelectItemGroup[];
  groupedMediaGroups: SelectItemGroup[];
  groupedProperty: SelectItemGroup[];
  fileName = '';
  fileData: File;
  userDetails: {
    name: '';
  };
  UploadData: object = {};
  constructor(
    private formBuilder: FormBuilder,
    public router: Router,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    // private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private config: DynamicDialogConfig,
    private dynamicDialogRef: DynamicDialogRef
  ) {
    this.UploadData = this.config.data;

  }

  ngOnInit() {
    if (this.UploadData['form_type'] == '1099 Form') {
      if (this.UploadData['isExpanded'] == 0) {
        this.registerForm = this.formBuilder.group({
          year: ['', [Validators.required]],
          derived_station_rpt: ['', [Validators.required]],
          Comment: ['']
        });
      } else {
        this.registerForm = this.formBuilder.group({
          year: ['', [Validators.required]],
          // derived_station_rpt: ['', [Validators.required]],
          Comment: ['']
        });
      }
      const currentYear = new Date().getFullYear();
      const yeardata = [];
      for (let i = 2015; i < currentYear; i++) {
        yeardata.push({ 'key': i, 'value': i });
      }
      this.groupedYears = yeardata;
      if (this.UploadData['isExpanded'] == 0) {
        this.loadProperty();
      }
    } else {
      if (this.UploadData['isExpanded'] == 0) {
        this.registerForm = this.formBuilder.group({
          Comment: [''],
          derived_station_rpt: ['', [Validators.required]]
        });
      } else {
        this.registerForm = this.formBuilder.group({
          Comment: [''],
          // derived_station_rpt: ['', [Validators.required]]
        });
      }

      if (this.UploadData['isExpanded'] == 0) {
        this.loadProperty();
      }
    }
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  loadProperty() {
    this.dataFetchServ.loadDimdata(this.UploadData['station_group_id']).subscribe(res => {
      this.groupedProperty = res['data'];
    });
  }
  fileChangeEvent(event: any): void {
    console.log('evenet', event)
    if (event.target.files.length > 0) {
      this.fileName = event.target.files[0].name;
      this.fileData = event.target.files[0];
    }
  }
  onSubmit() {
    console.log('this.registerForm ', this.registerForm);
    const fd = new FormData();
    fd.append('fileData', this.fileData, this.fileName);
    fd.append('user_id', this.UploadData['user_id']);
    fd.append('form_type', this.UploadData['form_type']);
    fd.append('description', this.registerForm.value.Comment);
    fd.append('file_type', this.fileData.type);

    if (this.UploadData['isExpanded'] == 0) {
      fd.append('derived_station_rpt_id', this.registerForm.value.derived_station_rpt.map(o => o.key));

    } else {
      fd.append('derived_station_rpt_id', this.UploadData['derived_station_rpt_id']);
      // fd.append('year', moment().format('YYYY'));
    }
    if (this.UploadData['form_type'] == '1099 Form') {
      fd.append('year', this.registerForm.value.year.key);
      let newFileName = this.registerForm.value.year.key + '-'
        + this.UploadData['form_type'] + '-' + moment().format('YYYY-MM-DD-HH-mm-ss') + '.'
        + this.fileName.split('.').pop();
      fd.append('file_name', newFileName);
    } else {
      fd.append('year', moment().format('YYYY'));
      let newFileName = moment().format('YYYY') + '-'
        + this.UploadData['form_type'] + '-' + moment().format('YYYY-MM-DD-HH-mm-ss') + '.'
        + this.fileName.split('.').pop();
      fd.append('file_name', newFileName);
    }

    // console.log('fileData', fd, newFileName);
    this.dataFetchServ.UploadTaxForm(fd).subscribe(data => {
      if (data['status']) {
        this.toastService.displayToast({
          severity: 'success',
          summary: 'Upload Files',
          detail: 'Tax Form uploaded successfully'
        });
        this.dialogService.dialogComponentRef.destroy();
      } else {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Something went wrong',
          detail: 'Error Changing Role/Team'
        });
      }
    });
  }

  goBack() {
    // this.router.navigate(['/users']);
    this.dynamicDialogRef.close(null);
  }
  // ngOnDestroy() {
  //   if (this.appConfigObs && !this.appConfigObs.closed) {
  //     this.appConfigObs.unsubscribe();
  //   }
  // }
}
