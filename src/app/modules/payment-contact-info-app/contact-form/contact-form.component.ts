import {
  Component,
  OnInit,
  OnDestroy,
} from '@angular/core';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { CommonLibService, DataShareService } from 'src/app/_services';
import * as moment from 'moment';
import { FetchApiDataService } from '../fetch-api-data.service';
// import { UnfilledDataPopupComponent } from "./unfilled-popup/unfilled-daily-data-popup.component";
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { Subscription } from 'rxjs';
import { DynamicDialogConfig, DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { CommentService } from '../../common/chat/comment.service';
import { CommentJson } from 'src/app/_interfaces/commentJson';
import { ChatComponent } from '../../common/chat/chat/chat.component';
import { ContactMainFormComponent } from './contact-main-form/contact-main-form.component';

@Component({
  selector: 'ym-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss'],
  providers:[DialogService]
})
export class ContactFormComponent implements OnInit {
  private appConfigObs: Subscription;

  appConfig: object = {};
  rowData: object = {};
  aggTableData: TreeNode[];
  noTableData = false;
  aggTableColumnDef: any[];
  aggTableJson: Object;
  displayAggTable = false;
  appliedFilters: object = {};
  filtersApplied: object = {};
  derived_station_rpt_ids: any[];
  /* ---------------------------------- Table --------------------------------- */

  flatTableReq: object;

  flatTableData: TreeNode[];
  flatTableColumnDef: any[];
  flatTableJson: Object;
  deleteForm = false;
  uploadContactForm = false;
  downloadForm = false;
  frameUploader: any;
  commentJson: CommentJson = <CommentJson>{};

  constructor(
    private libServ: CommonLibService,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private config: DynamicDialogConfig,
    private confirmationService: ConfirmationService,
    private commentservice: CommentService,
    private dynamicDialogRef: DynamicDialogRef
  ) {
    this.appConfig = this.config.data['config'];
    this.rowData = this.config.data['rowData'];
    this.commentJson = this.config.data['commentJson'];
  }

  ngOnInit() {

    this.flatTableColumnDef = [

      // {
      //   field: 'station_group',
      //   displayName: 'Media Group',
      //   format: '',
      //   width: '200',
      //   isExpanded: 'false',
      //   exportConfig: {
      //     format: 'string',
      //     styleinfo: {
      //       thead: 'default',
      //       tdata: 'white'
      //     }
      //   },
      //   formatConfig: [],
      //   options: {
      //     editable: false,
      //     colSearch: false,
      //     colSort: true,
      //     resizable: true,
      //     movable: false
      //   }
      // },
      {
        field: 'derived_station_rpt',
        displayName: 'Property',
        format: '',
        width: '200',
        isExpanded: 'false',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'contact_person_name',
        displayName: 'Contact Name',
        format: '',
        width: '200',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'business_name',
        displayName: 'Full Name',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'address',
        displayName: 'address',
        format: '',
        width: '250',
        exportConfig: {
          format: '',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'city',
        displayName: 'City',
        format: '',
        width: '200',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'state',
        displayName: 'State',
        format: '',
        width: '130',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'zip_code',
        displayName: 'Zip Code',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'email',
        displayName: 'Email',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'phone_no',
        displayName: 'Phone No.',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'domain_site_name',
        displayName: 'Domain-Site Name',
        format: '',
        width: '200',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'date',
        displayName: 'Date',
        format: '',
        width: '100',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'Update',
        displayName: 'Edit',
        format: '',
        width: '100',
        exportConfig: {
          format: '',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      // {
      //   field: 'Delete',
      //   displayName: 'Delete',
      //   format: '',
      //   width: '100',
      //   exportConfig: {
      //     format: '',
      //     styleinfo: {
      //       thead: 'default',
      //       tdata: 'white'
      //     }
      //   },
      //   formatConfig: [],
      //   options: {
      //     editable: false,
      //     colSearch: false,
      //     colSort: true,
      //     resizable: true,
      //     movable: true
      //   },
      //   footerTotal: '-'
      // },


      {
        field: 'Comments',
        displayName: 'Comments',
        format: '',
        width: '125',
        exportConfig: {
          format: '',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
    ];

    this.flatTableJson = {
      page_size: 10,
      page: 1,
      lazy: true,
      loading: false,
      // export: true,
      // sortMode: "multiple",
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '300px',
      totalRecords: 1000,
      columns: this.libServ.deepCopy(this.flatTableColumnDef.slice(1)),
      selectedColumns: this.libServ.deepCopy(this.flatTableColumnDef.slice(1)),
      frozenCols: this.libServ.deepCopy([...this.flatTableColumnDef.slice(0, 1)]),
      frozenWidth:
        this.flatTableColumnDef
          .slice(0, 1)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: false,
      columnSearch: false
    };
    this.uploadContactForm = this.appConfig['permissions'].some(
      o => o.name === 'upload-contact-form'
    );
    this.deleteForm = this.appConfig['permissions'].some(
      o => o.name === 'delete-contact-form'
    );

    if (!((this.flatTableColumnDef.some(x => x.field === 'Delete')))) {
      this.deleteForm ? this.flatTableColumnDef.splice(5, 0, {
        field: 'Delete',
        displayName: 'Delete',
        format: '',
        width: '100',
        exportConfig: {
          format: '',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      }) : '';
    }
    this.initialLoading();
  }

  initialLoading() {
    if (this.rowData['isExpanded'] == '0') {
      this.flatTableReq = {
        station_group_id: this.rowData['station_group_id'],
        isExpanded: false
      };
    } else {
      this.flatTableReq = {
        station_group_id: this.rowData['station_group_id'],
        derived_station_rpt_id: this.rowData['derived_station_rpt_id'],
        isExpanded: true
      };
    }
    this.flatTableJson['loading'] = true;
    this.dataFetchServ.getContactFormData(this.flatTableReq).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      }

      const data2 = data['data'];
      this.derived_station_rpt_ids = data2.map(x => x.derived_station_rpt_main_id);
      console.log('derived_station_rpt_main_id', this.derived_station_rpt_ids)
      const arr = [];
      for (const r of data2) {
        const ak = r['date'];
        delete r['date'];
        r['date'] = moment(ak, 'YYYYMMDD').format('MM-DD-YYYY');
        const obj = {};
        obj['data'] = r;
        arr.push(obj);
      }

      this.flatTableData = <TreeNode[]>arr;
      this.flatTableJson['totalRecords'] = 25;
      this.flatTableJson['loading'] = false;
    });
  }

  commentForm(row) {
    const req1 = {
      user_id: this.appConfig['user']['id'],
      app_id: this.appConfig['id']
    };

    this.commentservice
      .getAppCommentData(req1)
      .subscribe(commentData => {
        if (commentData['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          return;
        }
        const data = {};
        const CommentSortData = [];
        const config = {
          formType: 'contact_form',
          id: row['id'],
          comment_on: row['contact_person_name']
        };
        commentData['data'].forEach(element => {
          const commentconfig = JSON.parse(element.config);
          if (config.formType == commentconfig.formType && config.id == commentconfig.id) {
            CommentSortData.push(element);
          }
        });

        this.commentJson['config'] = config;

        data['commentJson'] = this.commentJson;
        data['commentData'] = CommentSortData;
        const chatSetting = {
          showChatMsg: false,
          title: `${row['contact_person_name']}`,
          showCommentTextArea: true,
          chatURL: '/payment-contact-info/send/message',
          data: data
        };

        const ref = this.dialogService.open(ChatComponent, {
          header: `Comment on ${row['contact_person_name']}`,
          contentStyle: { width: '30vw', height: '370px', overflow: 'auto' },
          data: chatSetting,
          width: '30vw'
        });

        ref.onClose.subscribe((data: string) => {
          // let UnreadCommentIdList = CommentSortData.filter(o => o.is_read == "N" && o.user_id != this.commentJson['user_id']).map(x => x.comment_id);
          // console.log('popup close', CommentSortData,UnreadCommentIdList);
          // if (UnreadCommentIdList.length > 0) {
          // this.UpdateBellIcon();
          // }
        });
      });

  }

  getContactInfo(type, row) {
    this.rowData['type'] = type;
    this.rowData['user_id'] = this.appConfig['user']['id'];
    const data = {
      rowData: this.rowData,
      row: row
    }
    const ref = this.dialogService.open(ContactMainFormComponent, {
      header: "Contact Form",
      contentStyle: {
        'min-height': '65vh',
        'max-height': '65vh',
        width: '50vw',
        overflow: 'auto'
      },
      data: data
    });
    ref.onClose.subscribe((data: string) => {
      this.initialLoading();
    });
  }


  CloseDialog() {
    this.dynamicDialogRef.destroy();
  }

  deleteForms(row) {
    console.log('hii', row);
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete this file?',
      accept: () => {
        this.dataFetchServ.deleteContactForm(row['id']).subscribe(data => {
          this.initialLoading();
        })
      },
      reject: () => {

      }
    });

  }
}
