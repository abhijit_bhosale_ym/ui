import { Component, OnInit, OnDestroy } from '@angular/core';
import { SelectItemGroup } from 'primeng/api';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { FetchApiDataService } from '../../fetch-api-data.service';
import * as moment from 'moment';
// import { Subscription } from 'rxjs';
import { CommonLibService } from 'src/app/_services';
// import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { DynamicDialogConfig, DynamicDialogRef, DialogService } from 'primeng/dynamicdialog';
@Component({
  selector: 'ym-contact-main-form',
  templateUrl: './contact-main-form.component.html',
  styleUrls: ['./contact-main-form.component.scss']
})
export class ContactMainFormComponent implements OnInit {
  // private appConfigObs: Subscription;

  appConfig: object = {};
  registerForm: FormGroup;
  isAllProperty = false;
  isCreateTeam = false;
  groupedYears: SelectItemGroup[];
  groupedMediaGroups: SelectItemGroup[];
  groupedProperty: SelectItemGroup[];
  fileName = '';
  fileData: any[] = [];
  signName = '';
  signimage;
  signData: File;
  userDetails: {
    name: '';
  };
  RowData: object = {};
  FormData: object = {};
  constructor(
    private formBuilder: FormBuilder,
    public router: Router,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    // private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private config: DynamicDialogConfig,
    private dynamicDialogRef: DynamicDialogRef
  ) {
    this.RowData = this.config.data['rowData'];
    this.FormData = this.config.data['row'];

  }

  ngOnInit() {


    console.log('rowdata', this.RowData)
    if (this.RowData['type'] == 'new') {
      this.registerForm = this.formBuilder.group({
        derived_station_rpt: [FormData['derived_station_rpt'], [Validators.required]],
        contactPersonName: [this.FormData['contact_person_name'], [Validators.required]],
        businessName: [this.FormData['business_name'], [Validators.required]],
        address: [this.FormData['address'], [Validators.required]],
        city: [this.FormData['city'], [Validators.required]],
        state: [this.FormData['state'], [Validators.required]],
        zipCode: [this.FormData['zip_code'], [Validators.required]],
        email: [this.FormData['email'], [Validators.required, Validators.email]],
        phoneNo: [this.FormData['phone_no'], [Validators.required]],
        domainSiteName: [this.FormData['domain_site_name'], [Validators.required]],
        date: [this.FormData['date'] ? moment(this.FormData['date'], 'MM-DD-YYYY').format("YYYYMMDD") : moment().format("YYYYMMDD"), [Validators.required]]

      });
      this.loadProperty();

    } else {
      this.registerForm = this.formBuilder.group({
        contactPersonName: [this.FormData['contact_person_name'], [Validators.required]],
        businessName: [this.FormData['business_name'], [Validators.required]],
        address: [this.FormData['address'], [Validators.required]],
        city: [this.FormData['city'], [Validators.required]],
        state: [this.FormData['state'], [Validators.required]],
        zipCode: [this.FormData['zip_code'], [Validators.required]],
        email: [this.FormData['email'], [Validators.required, Validators.email]],
        phoneNo: [this.FormData['phone_no'], [Validators.required]],
        domainSiteName: [this.FormData['domain_site_name'], [Validators.required]],
        date: [this.FormData['date'] ? moment(this.FormData['date'], 'MM-DD-YYYY').format("YYYYMMDD") : moment().format("YYYYMMDD"), [Validators.required]]

      });
    }

  }
  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  loadProperty() {
    this.dataFetchServ.loadDimdata(this.RowData['station_group_id']).subscribe(res => {
      this.groupedProperty = res['data'];
    });
  }

  onSubmit() {
    console.log('this.registerForm ', this.registerForm);
    let req = {
      contact_person_name: this.registerForm.value.contactPersonName,
      business_name: this.registerForm.value.businessName,
      address: this.registerForm.value.address,
      city: this.registerForm.value.city,
      state: this.registerForm.value.state,
      zip_code: this.registerForm.value.zipCode,
      date: this.registerForm.value.date,
      email: this.registerForm.value.email,
      phone_no: this.registerForm.value.phoneNo,
      domain_site_name: this.registerForm.value.domainSiteName,
      user_id: this.RowData['user_id'],
      type: this.RowData['type']
    }
    if (this.RowData['type'] == 'new') {
      req['derived_station_rpt'] = this.registerForm.value.derived_station_rpt.map(o => o.key);

    } else {
      req['derived_station_rpt'] = [this.FormData['derived_station_rpt_id']];
      req['id'] = this.FormData['id'];
    }
    this.dataFetchServ.UpdateContactForm(req).subscribe(data => {
      if (data['status']) {
        this.toastService.displayToast({
          severity: 'success',
          summary: 'Upload Files',
          detail: 'Contact form uploaded successfully'
        });
        this.dialogService.dialogComponentRef.destroy();
      } else {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Something went wrong',
          detail: 'Error updating contact form'
        });
      }
    });
  }

  goBack() {
    // this.router.navigate(['/users']);
    this.dynamicDialogRef.close(null);
  }
  // ngOnDestroy() {
  //   if (this.appConfigObs && !this.appConfigObs.closed) {
  //     this.appConfigObs.unsubscribe();
  //   }
  // }
}
