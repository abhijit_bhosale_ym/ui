import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactMainFormComponent } from './contact-main-form.component';

describe('ContactMainFormComponent', () => {
  let component: ContactMainFormComponent;
  let fixture: ComponentFixture<ContactMainFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ContactMainFormComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactMainFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
