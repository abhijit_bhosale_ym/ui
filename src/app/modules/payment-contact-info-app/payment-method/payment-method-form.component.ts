import { Component, OnInit, OnDestroy } from '@angular/core';
import { SelectItemGroup, ConfirmationService } from 'primeng/api';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { FetchApiDataService } from '../fetch-api-data.service';
import { CommonLibService } from 'src/app/_services';
import { DynamicDialogConfig, DynamicDialogRef, DialogService } from 'primeng/dynamicdialog';
@Component({
  selector: 'ym-payment-method-form',
  templateUrl: './payment-method-form.component.html',
  styleUrls: ['./payment-method-form.component.scss']
})
export class PaymentMethodFormComponent implements OnInit {
  // private appConfigObs: Subscription;

  appConfig: object = {};
  registerForm: FormGroup;
  isCreateRole = false;
  isCreateTeam = false;
  groupedYears: SelectItemGroup[];
  groupedMediaGroups: SelectItemGroup[];
  groupedProperty: SelectItemGroup[];
  fileName = '';
  paymentMethodLists: any[];
  fileData: File;
  userDetails: {
    name: '';
  };
  RowData: object = {};
  constructor(
    private formBuilder: FormBuilder,
    public router: Router,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private confirmationService: ConfirmationService,
    private libServ: CommonLibService,
    private config: DynamicDialogConfig,
    private dynamicDialogRef: DynamicDialogRef
  ) {
    this.RowData = this.config.data;
    this.paymentMethodLists = this.RowData['paymentMethodLists'];
  }

  ngOnInit() {

    this.registerForm = this.formBuilder.group({
      paymentMethod: ['', [Validators.required]],
      derived_station_rpt: ['', [Validators.required]]
    });

    this.loadProperty();
    // this.getPaymentMethod();

  }

  // convenience getter for easy access to form fields
  get f() {
    return this.registerForm.controls;
  }

  loadProperty() {
    this.dataFetchServ.loadDimdata(this.RowData['station_group_id']).subscribe(res => {
      this.groupedProperty = res['data'];
    });
  }

  // getPaymentMethod() {
  //   this.dataFetchServ.getPaymentMethod().subscribe(data => {
  //     this.paymentMethodLists = data['data'];
  //   });
  // }

  onSubmit() {
    this.confirmationService.confirm({
      message: 'Are you sure you want to change update status?',
      accept: () => {
        const req = {
          method_type: this.registerForm.value.paymentMethod.key,
          derived_station_rpt_id: this.registerForm.value.derived_station_rpt.map(o => o.key),
          user_id: this.RowData['user_id']
        };

        this.dataFetchServ.updatePaymentMethod(req).subscribe(data => {
          if (data['status']) {
            this.toastService.displayToast({
              severity: 'success',
              summary: 'Payment Method Update',
              detail: 'Payment method updated successfully'
            });
            this.dialogService.dialogComponentRef.destroy();
          } else {
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Server Error',
              detail: 'Please refresh the page'
            });
          }
        });
      },
      reject: () => {

      }
    });
  }

  goBack() {
    // this.router.navigate(['/users']);
    this.dialogService.dialogComponentRef.destroy();
  }
  // ngOnDestroy() {
  //   if (this.appConfigObs && !this.appConfigObs.closed) {
  //     this.appConfigObs.unsubscribe();
  //   }
  // }
}
