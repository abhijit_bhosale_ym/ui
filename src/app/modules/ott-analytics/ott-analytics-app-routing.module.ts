import { Routes } from '@angular/router';
import { OTTAnalyticsAppComponent } from './ott-analytics-app.component';

export const OTTAnalyticsAppRoutes: Routes = [
  {
    path: '',
    component: OTTAnalyticsAppComponent
  }
];
