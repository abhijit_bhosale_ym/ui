import { Component, OnInit } from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { FetchApiDataService } from '../fetch-api-data.service';
import * as moment from 'moment';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { DynamicDialogConfig } from 'primeng';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-ott-analytics-daily-data-popup',
  templateUrl: './ott-analytics-daily-data-popup.component.html',
  styleUrls: ['./ott-analytics-daily-data-popup.component.scss']
})
export class OTTAnalyticsDailyDataPopupComponent implements OnInit {
  filtersApplied: object;
  appName: string;
  email: [];
  tabChangedFlag = false;

  tableDataReq: object;
  tableData: TreeNode[];
  tableColumnDef: any[];
  dimColDef: any[];
  tableJson: Object;
  timeout: any;
  dailyData = [];
  exportRequest: ExportRequest = <ExportRequest>{};

  constructor(
    private libServ: CommonLibService,
    private dataFetchServ: FetchApiDataService,
    private config: DynamicDialogConfig,
    private exportService: ExportdataService,
    private toastService: ToastService,
    private confirmationService: ConfirmationService
  ) {
    this.filtersApplied = this.config.data['filters'];
    this.appName = this.config.data['appName'];
    this.email = this.config.data['email'];
  }

  ngOnInit() {
    this.dimColDef = [
      {
        field: 'time_key',
        displayName: 'Date',
        format: 'date',
        width: '150',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'channels',
        displayName: 'Channel',
        format: '',
        width: '230',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: '-'
      },
      {
        field: 'demand_partner',
        displayName: 'Demand Parner',
        format: '',
        width: '190',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: '-'
      },
      {
        field: 'distribution_platform',
        displayName: 'Distribution Platform',
        format: '',
        width: '190',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: '-'
      },
      {
        field: 'video_service',
        displayName: 'Video Service',
        format: '',
        width: '190',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: '-'
      },
      {
        field: 'series',
        displayName: 'Series',
        format: '',
        width: '190',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: '-'
      },
      {
        field: 'episode',
        displayName: 'Episode',
        format: '',
        width: '190',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: '-'
      },
      {
        field: 'measure_names',
        displayName: 'Measure Names',
        format: '',
        width: '190',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: '-'
      }
    ];
    this.tableColumnDef = [
      {
        field: 'hov',
        displayName: 'Total HOV',
        format: 'number',
        width: '170',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '0'
      },
      {
        field: 'sessions',
        displayName: 'Total Sessions',
        format: 'number',
        width: '170',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'estm_hov',
        displayName: 'Estimated HOV',
        width: '155',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '0'
      },
      {
        field: 'publisher_inventory',
        displayName: 'Publisher Inventory',
        format: 'number',
        width: '250',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '0'
      },
      {
        field: 'publisher_impressions',
        displayName: 'Publisher Impressions',
        format: 'number',
        width: '155',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '0'
      },
      {
        field: 'publisher_render_rate',
        displayName: 'Publisher Render Rate(%)',
        format: 'percentage',
        width: '200',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '0'
      },
      {
        field: 'unique_user',
        displayName: 'Unique Users',
        format: 'number',
        width: '155',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '0'
      }
    ];
    this.tableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '400px',
      totalRecords: 1000,

      columns: this.tableColumnDef,
      selectedColumns: this.tableColumnDef,
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: false,
      overallSearch: true,
      columnSearch: false
    };

    this.tableDataReq = {
      dimensions: ['time_key'].concat(this.filtersApplied['filters']['dimensions'].map(e => e.key)),
      metrics: [
        'hov',
        'sessions',
        'publisher_impressions',
        'publisher_inventory',
        'unique_user'
      ],
      derived_metrics: ['estm_hov', 'publisher_render_rate'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key'],
        interval: 'daily'
      },
      gidGroupBy: this.filtersApplied['filters']['dimensions'].map(e => e.key),
      orderBy: [{ key: 'time_key', opcode: 'desc' }],
      limit: '',
      offset: ''
    };
    this.loadTable(this.tableDataReq);
  }

  loadTable(tableDataReq) {
    let grpBysFilter = tableDataReq.dimensions;
    let tableDimColDef = [];
    grpBysFilter.forEach(ele => {
      tableDimColDef.push(this.dimColDef.find(x => x.field == ele));
    });

    tableDimColDef = tableDimColDef.concat(this.tableColumnDef);
    this.tableJson['columns'] = tableDimColDef;
    this.tableJson['selectedColumns'] = tableDimColDef;

    this.dataFetchServ.getRevMgmtData(tableDataReq).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      }
      this.dailyData = data['data'];
      const arr = [];
      this.dailyData.forEach((row: object) => {
        const obj = {
          data: row
        };
        arr.push(obj);
      });
      this.tableData = <TreeNode[]>arr;
      this.tableJson['totalRecords'] = data['totalItems'];
      this.tableJson['loading'] = false;
    });
  }

  exportTablePopup(fileFormat) {
    this.exportRequest['sendEmail'] = this.email;
    this.exportRequest['appName'] = this.appName;
    if (this.tableData.length == 0) {
      this.confirmationService.confirm({
        message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
        header: 'Information',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
        },
        reject: () => {
        }
      });
    } else {
      if (this.exportRequest['sendEmail'].length > 0) {
        this.toastService.displayToast({
          severity: 'info',
          summary: 'Export Report',
          detail:
            'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
          life: 10000
        });

        let grpBysFilter = this.tableDataReq['dimensions'];
        let tableDimColDef = [];
        grpBysFilter.forEach(ele => {
          tableDimColDef.push(this.dimColDef.find(x => x.field == ele));
        });

        tableDimColDef = tableDimColDef.concat(this.tableColumnDef);

        const sheetDetails = {};
        sheetDetails['columnDef'] = tableDimColDef;
        sheetDetails['data'] = this.dailyData;
        sheetDetails['sheetName'] = 'Daily Data Distribution';
        sheetDetails['isRequest'] = false;
        sheetDetails['request'] = {
          url: '',
          method: '',
          param: ''
        };
        sheetDetails['disclaimer'] = [
          {
            position: 'bottom',
            label: 'Note: Data present in the table may vary over a period of time.',
            color: '#000000'
          }
        ];
        sheetDetails['totalFooter'] = {
          available: false,
          custom: false
        };
        sheetDetails['tableTitle'] = {
          available: false,
          label: 'Daily Data Distribution'
        };
        sheetDetails['image'] = [
          {
            available: true,
            path: environment.exportConfig.exportLogo,
            position: 'top'
          }
        ];
        const sheetDetailsarray = [];
        sheetDetailsarray.push(sheetDetails);
        this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
        this.exportRequest['fileName'] =
          this.config.data['field'] +
          ' Daily Distribution ' +
          moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
        this.exportRequest['exportFormat'] = fileFormat;
        this.exportRequest['exportConfig'] = environment.exportConfig;
        this.dataFetchServ
          .getExportReportData(this.exportRequest)
          .subscribe(response => {
          });
      } else {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Export Report',
          detail:
            'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
          life: 10000
        });
      }
    }
    return false;
  }

  isHiddenColumn(col: Object) { }

  onGlobalSearchChanged(searchValue, tableColDef) {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    this.timeout = setTimeout(() => {
      this.tableDataReq['filters']['globalSearch'] = {
        dimensions: [],
        value: searchValue
      };
      tableColDef.forEach(element => {
        this.tableDataReq['filters']['globalSearch']['dimensions'].push(
          element.field
        );
      });
      this.loadTable(this.tableDataReq);
    }, 3000);
  }
}
