import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OTTAnalyticsDailyDataPopupComponent } from './ott-analytics-daily-data-popup.component';

describe('OTTAnalyticsDailyDataPopupComponent', () => {
  let component: OTTAnalyticsDailyDataPopupComponent;
  let fixture: ComponentFixture<OTTAnalyticsDailyDataPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OTTAnalyticsDailyDataPopupComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OTTAnalyticsDailyDataPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
