import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectButtonModule } from 'primeng/selectbutton';
import { ButtonModule } from 'primeng/button';
import { TabViewModule } from 'primeng/tabview';
import { TreeTableModule } from 'primeng/treetable';
import { InputSwitchModule } from 'primeng/inputswitch';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MultiSelectModule } from 'primeng/multiselect';
import { PanelModule } from 'primeng/panel';
import { SharedModule } from 'src/app/_pipes/shared.module';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { FilterContainerModule } from '../common/filter-container/filter-container.module';
import { CardsModule } from '../common/cards/cards.module';
import { ChartsModule } from '../common/charts/charts.module';
import { RouterModule } from '@angular/router';
import { OTTAnalyticsAppRoutes } from './ott-analytics-app-routing.module';
import { OTTAnalyticsAppComponent } from './ott-analytics-app.component';
import { RadioButtonModule } from 'primeng/radiobutton';
import { ConfirmDialogModule, ConfirmationService } from 'primeng';
// import { DaterangePickerComponent } from '../common/filter-container/daterange-picker/daterange-picker.component';

@NgModule({
  declarations: [OTTAnalyticsAppComponent],
  imports: [
    CommonModule,
    SelectButtonModule,
    ButtonModule,
    TabViewModule,
    TreeTableModule,
    InputSwitchModule,
    FormsModule,
    ReactiveFormsModule,
    MultiSelectModule,
    SharedModule,
    FilterContainerModule,
    CardsModule,
    ChartsModule,
    PanelModule,
    RadioButtonModule, ConfirmDialogModule,
    RouterModule.forChild(OTTAnalyticsAppRoutes)
  ],
  providers: [FormatNumPipe, ConfirmationService]
})
export class OTTAnalyticsAppModule { }
