import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OTTAnalyticsExportPopupComponent } from './ott-analytics-export-popup.component';

describe('Rev360ExportPopupComponent', () => {
  let component: OTTAnalyticsExportPopupComponent;
  let fixture: ComponentFixture<OTTAnalyticsExportPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OTTAnalyticsExportPopupComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OTTAnalyticsExportPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
