import { Component, OnInit, OnDestroy } from '@angular/core';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { CommonLibService } from 'src/app/_services';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';
import { FetchApiDataService } from './fetch-api-data.service';
import { OTTAnalyticsDailyDataPopupComponent } from './ott-analytics-daily-data-popup/ott-analytics-daily-data-popup.component';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { Subscription } from 'rxjs';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { SendEmailComponent } from '../send-email/send-email.component';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { OTTAnalyticsExportPopupComponent } from './ott-analytics-export-popup/ott-analytics-export-popup.component';
import { environment } from 'src/environments/environment';
import { DialogService } from 'primeng';

@Component({
  selector: 'ym-ott-analytics-app',
  templateUrl: './ott-analytics-app.component.html',
  styleUrls: ['./ott-analytics-app.component.scss']
})
export class OTTAnalyticsAppComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;

  timeZone = environment.timeZone;
  public reportType = 'isSummary';
  appConfig: object = {};
  appConfig1: object = {};
  lastUpdatedOn: Date;
  collapsedCustom = false;
  cardsJson = [];
  showCards = true;
  chartColors: string[];
  selectedDates: Date[];
  defaultChartJson: object;

  noDataImpressionsLineChart = false;
  showImpressionsLineChart = false;
  ImpressionsLineChartJson: object;

  noDataRevenueLineChart = false;
  showRevenueLineChart = false;
  RevenueLineChartJson: object;

  noDataEcpmLineChart = false;
  showEcpmLineChart = false;
  EcpmLineChartJson: object;



  MonthlyBarChartJson: object;
  MonthlyStackChartJson: object;
  nextUpdated: Date;
  dataUpdatedThrough: Date;
  timeout: any;
  tableReq: object;

  sourcesJson = {
    titantv: {
      Columns: ['titantv_page_views', 'titantv_revenue', 'titantv_rpm'],
      DisplayName: 'TitanTv'
    },
    antennaweb: {
      Columns: [
        'antennaweb_page_views',
        'antennaweb_revenue',
        'antennaweb_rpm'
      ],
      DisplayName: 'Antennaweb'
    }
  };

  aggTableData: TreeNode[];
  noTableData = false;
  tableDimColDef: any[];
  tableMatColDef: any[];
  aggTableColumnDef: any[];
  aggTableJson: Object;
  displayAggTable = false;
  toggleView = true;
  filtersApplied: object = {};
  exportReportData: object = {};
  exportRequest: ExportRequest = <ExportRequest>{};

  appFeaturesConfigs = {};
  mainCardsConfig = {};

  constructor(
    private appConfigService: AppConfigService,
    public libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private htmltoimage: HtmltoimageService,
    private exportService: ExportdataService,
    private pptExport: ExportPptService,
    private formatNumPipe: FormatNumPipe,
    private confirmationService: ConfirmationService
  ) { }

  ngOnInit() {
    this.tableDimColDef = [
      {
        field: 'channels',
        displayName: 'Channel',
        format: '',
        width: '180',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: '-'
      },
      {
        field: 'demand_partner',
        displayName: 'Demand Parner',
        format: '',
        width: '180',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: '-'
      },
      {
        field: 'distribution_platform',
        displayName: 'Distribution Platform',
        format: '',
        width: '180',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: '-'
      },
      {
        field: 'video_service',
        displayName: 'Video Service',
        format: '',
        width: '180',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: '-'
      },
      {
        field: 'series',
        displayName: 'Series',
        format: '',
        width: '180',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: '-'
      },
      {
        field: 'episode',
        displayName: 'Episode',
        format: '',
        width: '180',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: '-'
      },
      {
        field: 'measure_names',
        displayName: 'Measure Names',
        format: '',
        width: '180',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: '-'
      }
    ];
    this.tableMatColDef = [
      {
        field: 'hov',
        displayName: 'Total HOV',
        format: 'number',
        width: '120',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '0'
      },
      {
        field: 'sessions',
        displayName: 'Total Sessions',
        format: 'number',
        width: '130',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'estm_hov',
        displayName: 'Estimated HOV',
        width: '135',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '0'
      },
      {
        field: 'publisher_inventory',
        displayName: 'Publisher Inventory',
        format: 'number',
        width: '160',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '0'
      },
      {
        field: 'publisher_impressions',
        displayName: 'Publisher Impressions',
        format: 'number',
        width: '175',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '0'
      },
      {
        field: 'publisher_render_rate',
        displayName: 'Publisher Render Rate(%)',
        format: 'percentage',
        width: '200',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '0'
      },
      {
        field: 'unique_user',
        displayName: 'Unique Users',
        format: 'number',
        width: '155',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '0'
      }
    ];
    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,
      columns: this.tableDimColDef,
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: false,
      overallSearch: true,
      columnSearch: false
    };
    const colors = this.libServ.dynamicColors(2);
    this.defaultChartJson = {
      chartTypes: [{ key: 'bar', label: '', stacked: true }],
      chartData: {
        labels: [],
        datasets: []
      },
      chartOptions: {
        title: {
          display: true,
          text: ''
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: 'HOV'
              },
              position: 'left',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, 'number', [2]);
                }
              }
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              if (currentValue) {
                return `${
                  data.datasets[tooltipItem.datasetIndex].label
                  } : ${this.formatNumPipe.transform(currentValue, 'number', [2])}`;
              } else {
                return null;
              }
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        this._titleService.setTitle(this.appConfig['displayName']);
        let startDate;
        let endDate;
        const date_config = this.appConfig['filter']['filterConfig']['filters'][
          'datePeriod'
        ][0];
        if ((date_config['defaultDate'][0]['value'] === 0 && date_config['defaultDate'][0]['period'] === 'month') && (moment().format('DD') === '01' || moment().format('DD') === '02')) {
          startDate = moment().subtract(1, 'months').startOf('month');
          endDate = moment().subtract(1, 'months').endOf('month');
        } else {
          if (
            date_config[
            'defaultDate'
            ][0]['startOf']
          ) {
            startDate = moment()
              .subtract(
                date_config['defaultDate'][0]['value'],
                date_config['defaultDate'][0]['period']
              )
              .startOf(
                date_config['defaultDate'][0]['period']
              );
          } else {
            startDate = moment().subtract(
              date_config['defaultDate'][0]['value'],
              date_config['defaultDate'][0]['period']
            );
          }
          endDate = moment().subtract(
            date_config[
            'defaultDate'
            ][1]['value'],
            date_config[
            'defaultDate'
            ][1]['period']
          );
        }

        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: startDate.format('YYYYMMDD'),
            time_key2: endDate.format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: this.appConfig['filter']['filterConfig']['groupBy'].filter(
            v => v.selected
          )
        };
        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        this.appConfig1 = this.appConfig;
        this.initialLoading();
      }
    });
  }

  initialLoading() {
    this.chartColors = this.libServ.dynamicColors(5);
    this.dataFetchServ
      .getLastUpdatedData(this.appConfig['id'])
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          return;
        }
        this.lastUpdatedOn = moment(data[0]['updated_at']).toDate();
        this.nextUpdated = moment(data[0]['next_run_at']).toDate();
        this.dataUpdatedThrough = moment(
          data[0]['source_updated_through'],
          'YYYYMMDD'
        ).toDate();
      });

    this.libServ.getAppFeaturesConfigs(this.appConfig['id']).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: data['message']
        });
        return;
      }
      this.appFeaturesConfigs = data['data'];
      this.appFeaturesConfigs['cardsConfig']['cards'].forEach(element => {
        if (element['name'] === 'main') {
          this.mainCardsConfig = element['config'];
          this.cardsJson = this.mainCardsConfig['list'];
          if (this.mainCardsConfig['display']) {
            this.loadCards();
          }
        }
      });
    });
    const gidGroupBy = this.getGrpBys();
    this.tableReq = {
      dimensions: [],
      metrics: [
        'hov',
        'sessions',
        'publisher_impressions',
        'publisher_inventory',
        'unique_user'
      ],
      derived_metrics: ['estm_hov', 'publisher_render_rate'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: gidGroupBy,
      orderBy: [
        { key: gidGroupBy[0], opcode: 'asc' }
      ],
      limit: '',
      offset: ''
    };

    this.loadTableData(this.tableReq);

    const loadGraph = {
      dimensions: [],
      metrics: [
        'hov'
      ],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: { key: ['time_key'], interval: 'daily' },
      gidGroupBy: [],
      orderBy: [{ key: 'hov', opcode: 'desc' }],
      limit: '',
      offset: '',
      isTable: false
    };
    loadGraph.dimensions = ['time_key', 'episode'];
    loadGraph.gidGroupBy = ['episode'];
    this.loadEODLineChart(loadGraph);
    loadGraph.dimensions = ['time_key', 'measure_names'];
    loadGraph.gidGroupBy = ['measure_names'];
    this.loadMODLineChart(loadGraph);
    loadGraph.dimensions = ['time_key', 'video_service'];
    loadGraph.gidGroupBy = ['video_service'];
    this.loadVODLineChart(loadGraph);
  }

  resetPagination(tt1) {
    if (typeof tt1 !== 'undefined') {
      tt1.reset();
    }
  }

  loadTableData(param) {
    let grpBysFilter = this.filtersApplied['groupby'].map(e => e.key);
    let tableDimColDef = [];

    grpBysFilter.forEach(ele => {
      tableDimColDef.push(this.tableDimColDef.find(x => x.field == ele));
    });
    param['dimensions'] = [this.libServ.deepCopy(this.filtersApplied['groupby'][0]['key'])];
    param['orderBy'] = [{ key: this.libServ.deepCopy(this.filtersApplied['groupby'][0]['key']), opcode: 'asc' }];
    param['groupByTimeKey'] = { key: [], interval: 'daily' };

    tableDimColDef = tableDimColDef.concat(this.tableMatColDef);
    this.aggTableJson['columns'] = tableDimColDef.slice(
      grpBysFilter.length
    );
    this.aggTableJson['selectedColumns'] = tableDimColDef.slice(
      grpBysFilter.length
    );
    this.aggTableJson['frozenCols'] = [
      ...tableDimColDef.slice(0, grpBysFilter.length)
    ];
    this.aggTableJson['frozenWidth'] = tableDimColDef
      .slice(0, grpBysFilter.length)
      .reduce((tot, cur) => tot + parseInt(cur.width), 0) + 'px',
      this.aggTableJson['footerColumns'] = tableDimColDef;
    this.reloadAggTable();

    this.aggTableJson['loading'] = true;

    this.dataFetchServ.getRevMgmtData(param).subscribe(data => {
      param['dimensions'] = [];
      param['orderBy'] = [];
      if (param['filters']['globalSearch']) {
        param['filters']['globalSearch']['dimensions'].shift();
      }
      this.dataFetchServ.getRevMgmtData(param).subscribe(dataTotal => {
        if (data['status'] === 0) {
          this.noTableData = true;
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          return;
        } else {
          this.noTableData = false;
        }

        const tableData = data['data'];
        const totalFooterData = dataTotal['data'];

        const arr = [];
        tableData.forEach((row: object) => {
          for (let i = 1; i < grpBysFilter.length; i++) {
            if (i !== grpBysFilter.length) {
              row[grpBysFilter[i]] = 'All';
            }
          }
          let obj = {};
          if (grpBysFilter.length === 1) {
            obj = {
              data: row
            };
          } else {
            obj = {
              data: row,
              children: [{ data: {} }]
            };
          }
          arr.push(obj);
        });
        if (totalFooterData[0] !== undefined) {
          for (const key in totalFooterData[0]) {
            if (this.tableMatColDef.some(o => o.field == key)) {
              this.tableMatColDef.find(x => x.field == key).footerTotal =
                totalFooterData[0][key];
            }
          }
        } else {
          this.aggTableJson['footerColumns'].forEach(c => {
            if (this.tableMatColDef.some(o => o.field == c.field)) {
              c['footerTotal'] = 0;
            }
          });
        }
        this.aggTableData = <TreeNode[]>arr;
        this.aggTableJson['totalRecords'] = tableData.length;
        this.aggTableJson['loading'] = false;
        // this.addReportTotal(tableData);
      });
    });
  }

  exportTable(table, fileFormat) {
    if (this.aggTableData.length == 0) {
      this.confirmationService.confirm({
        message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
        header: 'Information',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
        },
        reject: () => {
        }
      });
    } else {
      if (this.exportRequest['sendEmail'].length > 0) {
        this.toastService.displayToast({
          severity: 'info',
          summary: 'Export Report',
          detail:
            'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
          life: 10000
        });
        let grpBys = this.getGrpBys();
        const tableReq = this.libServ.deepCopy(this.tableReq);
        let temp = 0;
        const sheetDetailsarray = [];
        while (temp < grpBys.length) {
          const grpbyExport = [];
          for (let i = 0; i <= temp; i++) {
            grpbyExport.push(grpBys[i]);
            tableReq['orderBy'] = [{ key: grpBys[i], opcode: 'asc' }];
          }

          tableReq['gidGroupBy'] = this.getGrpBysKey(grpbyExport);
          tableReq['dimensions'] = grpbyExport;
          tableReq['isTable'] = false;

          const colDefOptions = [];
          let i = 0;
          let sheetName = '';

          while (i < grpbyExport.length) {
            let displayName = '';
            if (grpbyExport[i] === 'channels') {
              displayName = 'Channels ';
              if (i === 0) {
                sheetName = 'Channels';
              } else {
                sheetName = '+Channels';
              }
            } else if (grpbyExport[i] === 'demand_partner') {
              displayName = 'Demand Partner ';
              if (i === 0) {
                sheetName = 'Demand Partner';
              } else {
                sheetName = '+Demand Partner';
              }
            } else if (grpbyExport[i] === 'distribution_platform') {
              displayName = 'Distribution Platform ';
              if (i === 0) {
                sheetName = 'Distribution Platform';
              } else {
                sheetName = '+Distribution Platform';
              }
            } else if (grpbyExport[i] === 'video_service') {
              displayName = 'video Service ';
              if (i === 0) {
                sheetName = 'Video Service"';
              } else {
                sheetName = '+Video Service';
              }
            } else if (grpbyExport[i] === 'series') {
              displayName = 'Series ';
              if (i === 0) {
                sheetName = 'Series';
              } else {
                sheetName = '+Series';
              }
            } else if (grpbyExport[i] === 'episode') {
              displayName = 'Episode ';
              if (i === 0) {
                sheetName = 'Episode';
              } else {
                sheetName = '+Episode';
              }
            }  else if (grpbyExport[i] === 'measure_names') {
              displayName = 'Measure Names ';
              if (i === 0) {
                sheetName = 'Measure Names';
              } else {
                sheetName = '+Measure Names';
              }
            }

            colDefOptions.push({
              field: grpbyExport[i],
              displayName: displayName,
              visible: true,
              width: 150,
              format: 'string',
              exportConfig: {
                format: 'string',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              }
            });
            i++;
          }

          const sheetDetails = {};
          let colDef = colDefOptions.concat(this.tableMatColDef);
          sheetDetails['columnDef'] = colDef.slice(0, colDef.length);
          sheetDetails['data'] = [];
          sheetDetails['sheetName'] = sheetName;
          sheetDetails['isRequest'] = true;
          sheetDetails['request'] = {
            url: '/api/flask/ott/cassandradata',
            method: 'POST',
            param: this.libServ.deepCopy(tableReq)
          };
          sheetDetails['disclaimer'] = [
            {
              position: 'bottom',
              label: 'Note: Data present in the table may vary over a period of time.',
              color: '#000000'
            }
          ];
          sheetDetails['totalFooter'] = {
            available: true,
            custom: true
          };
          sheetDetails['tableTitle'] = {
            available: false,
            label: 'Programmatic Daily Data'
          };
          sheetDetails['image'] = [
            {
              available: true,
              path: environment.exportConfig.exportLogo,
              position: 'top'
            }
          ];

          sheetDetailsarray.push(sheetDetails);
          temp++;
        }
        this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
        this.exportRequest['fileName'] =
          'Aggregated Distribution ' +
          moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
        this.exportRequest['exportFormat'] = fileFormat;
        this.exportRequest['exportConfig'] = environment.exportConfig;
        this.dataFetchServ
          .getExportReportData(this.exportRequest)
          .subscribe(response => {
          });
      } else {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Export Report',
          detail:
            'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
          life: 10000
        });
      }
    }

  }

  reloadAggTable() {
    this.displayAggTable = false;
    setTimeout(() => {
      this.displayAggTable = true;
    }, 0);
  }

  loadCards() {
    this.showCards = false;
    const apiUrl = this.mainCardsConfig['api']['url'];
    const cardsReq = this.libServ.deepCopy(
      this.mainCardsConfig['api']['request']
    );
    cardsReq['timeKeyFilter'] = this.libServ.deepCopy(
      this.filtersApplied['timeKeyFilter']
    );
    cardsReq['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);
    this.libServ.getDataFromDynamicAPI(apiUrl, cardsReq).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error - Contact to administator',
          detail: data
        });
        return;
      }
      if (!this.libServ.isEmptyObj(data['data'])) {
        this.getLastSevenDaysAverage(cardsReq, data['data'][0]);
        this.cardsJson.map(o => {
          o['value'] = data['data'][0][o['field']];
        });
      } else {
        this.cardsJson.map(o => {
          o['value'] = 0;
        });
      }
      this.showCards = this.mainCardsConfig['display'];
    });
  }

  getLastSevenDaysAverage(params, currentData) {
    // const totalDataDateDiff = parseInt(params['timeKeyFilter']['time_key2'], 10) - parseInt(params['timeKeyFilter']['time_key1'], 10);
    // const time_key1 = moment(params['timeKeyFilter']['time_key1'], 'YYYYMMDD')
    // .subtract('days', 1)
    // .format('YYYYMMDD');
    // const time_key2 = moment(time_key1, 'YYYYMMDD')
    // .subtract('days', 7)
    // .format('YYYYMMDD');
    // params['timeKeyFilter'] = { 'time_key1': time_key2, 'time_key2': time_key1 };

    // this.dataFetchServ.getRevMgmtData(params).subscribe(data => {
    //   if (data['data'].length) {
    //     const last7Days = data['data'][0];
    //     if (!this.libServ.isEmptyObj(last7Days)) {
    //       this.cardsJson.map(o => {
    //         const last7DaysAvg = parseFloat(last7Days[o['field']]) / 7 || 0;
    //         const totalAvg = parseFloat(currentData[o['field']]) / totalDataDateDiff || 0;
    //         o['metaConfig']['avgValue'] = ((totalAvg - last7DaysAvg) / totalAvg * 100).toFixed(2);
    //         o['metaConfig']['icon'] = totalAvg >= last7DaysAvg ? 'fas fa-arrow-up green' : 'fas fa-arrow-down red';
    //         o['metaConfig']['dateRange'] = `${moment(params['timeKeyFilter']['time_key1'], 'YYYYMMDD').format('MMM-DD-YYYY')} - ${moment(params['timeKeyFilter']['time_key2'], 'YYYYMMDD').format('MMM-DD-YYYY')}`;
    //       });
    //     }
    //   } else {
    //     this.cardsJson.map(o => {
    //       o['metaConfig']['avgValue'] = 'N/A';
    //       o['metaConfig']['icon'] = 'fas fa-minus';
    //       o['metaConfig']['dateRange'] = `${moment(params['timeKeyFilter']['time_key1'], 'YYYYMMDD').format('MMM-DD-YYYY')} - ${moment(params['timeKeyFilter']['time_key2'], 'YYYYMMDD').format('MMM-DD-YYYY')}`;
    //     });
    //   }
    // });


    this.cardsJson.map(o => {
    })

    this.cardsJson[0]['metaConfig']['avgValue'] = '10'
    this.cardsJson[0]['metaConfig']['icon'] = 'fas fa-arrow-up green';
    this.cardsJson[0]['metaConfig']['dateRange'] = `${moment(params['timeKeyFilter']['time_key1'], 'YYYYMMDD').format('MMM-DD-YYYY')} - ${moment(params['timeKeyFilter']['time_key2'], 'YYYYMMDD').format('MMM-DD-YYYY')}`;

    this.cardsJson[1]['metaConfig']['avgValue'] = '60'
    this.cardsJson[1]['metaConfig']['icon'] = 'fas fa-arrow-up green';
    this.cardsJson[1]['metaConfig']['dateRange'] = `${moment(params['timeKeyFilter']['time_key1'], 'YYYYMMDD').format('MMM-DD-YYYY')} - ${moment(params['timeKeyFilter']['time_key2'], 'YYYYMMDD').format('MMM-DD-YYYY')}`;

    this.cardsJson[2]['metaConfig']['avgValue'] = '40'
    this.cardsJson[2]['metaConfig']['icon'] = 'fas fa-arrow-up green';
    this.cardsJson[2]['metaConfig']['dateRange'] = `${moment(params['timeKeyFilter']['time_key1'], 'YYYYMMDD').format('MMM-DD-YYYY')} - ${moment(params['timeKeyFilter']['time_key2'], 'YYYYMMDD').format('MMM-DD-YYYY')}`;

    this.cardsJson[3]['metaConfig']['avgValue'] = '80'
    this.cardsJson[3]['metaConfig']['icon'] = 'fas fa-arrow-up green';
    this.cardsJson[3]['metaConfig']['dateRange'] = `${moment(params['timeKeyFilter']['time_key1'], 'YYYYMMDD').format('MMM-DD-YYYY')} - ${moment(params['timeKeyFilter']['time_key2'], 'YYYYMMDD').format('MMM-DD-YYYY')}`;
  }

  onLazyLoadAggTable(e: Event) {
  }

  onTableDrill(e: Event) {
    if (!e['node']['childLoaded']) {
      this.aggTableJson['loading'] = true;
      const tableReq = {
        dimensions: [],
        metrics: [
          'hov',
          'sessions',
          'publisher_impressions',
          'publisher_inventory',
          'publisher_render_rate',
          'unique_user'
        ],
        derived_metrics: [],
        timeKeyFilter: this.libServ.deepCopy(
          this.filtersApplied['timeKeyFilter']
        ),
        filters: this.libServ.deepCopy(this.filtersApplied['filters']),
        groupByTimeKey: { key: [], interval: 'daily' },
        gidGroupBy: this.getGrpBys(),
        orderBy: [],
        limit: '',
        offset: ''
      };
      const grpBys = this.filtersApplied['groupby'].map(e => e.key);
      for (const g of grpBys) {
        if (e['node']['data'][g] !== 'All') {
          tableReq['dimensions'].push(g);
          if (!tableReq['gidGroupBy'].includes(g)) {
            tableReq['gidGroupBy'].push(g);
          }
          if (
            tableReq['filters']['dimensions'].findIndex(e => e.key === g) === -1
          ) {
            tableReq['filters']['dimensions'].push({
              key: g,
              values: [e['node']['data'][g]]
            });
          } else {
            tableReq['filters']['dimensions'].splice(
              tableReq['filters']['dimensions'].findIndex(e => e.key === g),
              1
            );
            tableReq['filters']['dimensions'].push({
              key: g,
              values: [e['node']['data'][g]]
            });
          }
        } else {
          tableReq['dimensions'].push(g);
          if (!tableReq['gidGroupBy'].includes(g)) {
            tableReq['gidGroupBy'].push(g);
          }
          break;
        }
      }

      this.dataFetchServ.getRevMgmtData(tableReq).subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          return;
        }
        const childData = data['data'];
        const arr = [];

        childData.forEach((row: object) => {
          this.aggTableJson['frozenCols'].forEach(r => {
            if (typeof row[r['field']] === 'undefined') {
              row[r['field']] = 'All';
            }
          });
          let obj = {};
          row['estm_hov'] = parseFloat(row['hov']) + (parseFloat(row['hov']) * 0.2);
          row['publisher_render_rate'] = parseInt(row['publisher_impressions'], 10) / parseInt(row['publisher_inventory'], 10) * 100;
          if (row[grpBys[grpBys.length - 1]] === 'All') {
            obj = {
              data: row,
              children: [{ data: {} }]
            };
          } else {
            obj = {
              data: row
            };
          }
          arr.push(obj);
        });
        this.aggTableJson['loading'] = false;
        e['node']['children'] = <TreeNode[]>arr;
        this.aggTableData = [...this.aggTableData];
        e['node']['childLoaded'] = true;
      });
    }
  }

  openPopup(row, field) {
    const grpBys = [];
    const filtersApplied = this.libServ.deepCopy(this.filtersApplied);
    const fieldIdx = filtersApplied['groupby'].findIndex(
      e => e['key'] === field
    );
    filtersApplied['groupby'].forEach((grp, i) => {
      if (i <= fieldIdx) {
        if (
          filtersApplied['filters']['dimensions'].findIndex(
            e => e.key === grp['key']
          ) !== -1
        ) {
          filtersApplied['filters']['dimensions'].splice(
            filtersApplied['filters']['dimensions'].findIndex(
              e => e.key === grp['key']
            ),
            1
          );
        }
        filtersApplied['filters']['dimensions'].push({
          key: grp['key'],
          values: [row[grp['key']]]
        });
      }
    });

    filtersApplied['groupby'] = grpBys;
    const data = {
      filters: filtersApplied,
      field: row[field],
      appName: this.exportRequest['appName'],
      email: this.exportRequest['sendEmail']
    };
    const ref = this.dialogService.open(
      OTTAnalyticsDailyDataPopupComponent,
      {
        header: row[field] + ' Daily Distribution',
        contentStyle: { width: '80vw', overflow: 'auto' },
        data: data
      }
    );
    ref.onClose.subscribe((data: string) => { });
  }

  isHiddenColumn(col: Object) {
    return (
      this.aggTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }

  /* --------------------- On Filter Apply Button Clicked --------------------- */

  onFiltersApplied(filterData: object) {
    this.filtersApplied['filters']['dimensions'] = [];

    // tslint:disable-next-line: forin
    for (const k in filterData['filter']['dimensions']) {
      if (filterData['filter']['dimensions'][k].length !== 0) {
        this.filtersApplied['filters']['dimensions'].push({
          key: k,
          values: filterData['filter']['dimensions'][k]
        });
      }
    }

    this.filtersApplied['groupby'] = filterData['groupby'];
    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];

    const grpBys = this.getGrpBys();
    this.tableReq = {
      dimensions: grpBys,
      metrics: [
        'hov',
        'sessions',
        'publisher_impressions',
        'publisher_inventory',
        'publisher_render_rate',
        'unique_user'
      ],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: grpBys,
      orderBy: [
        { key: grpBys[0], opcode: 'asc' }
      ],
      limit: '',
      offset: ''
    };

    this.loadTableData(this.tableReq);

    if (this.mainCardsConfig['display']) {
      this.mainCardsConfig['api']['request']['gidGroupBy'] = grpBys;
      this.loadCards();
    }

    const loadGraph = {
      dimensions: [],
      metrics: [
        'hov'
      ],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: { key: ['time_key'], interval: 'daily' },
      gidGroupBy: [],
      orderBy: [{ key: 'hov', opcode: 'desc' }],
      limit: '',
      offset: '',
      isTable: false
    };
    loadGraph.dimensions = ['time_key', 'episode'];
    loadGraph.gidGroupBy = ['episode'];
    this.loadEODLineChart(loadGraph);
    loadGraph.dimensions = ['time_key', 'measure_names'];
    loadGraph.gidGroupBy = ['measure_names'];
    this.loadMODLineChart(loadGraph);
    loadGraph.dimensions = ['time_key', 'video_service'];
    loadGraph.gidGroupBy = ['video_service'];
    this.loadVODLineChart(loadGraph);
  }

  onGlobalSearchChanged(searchValue, tableColDef) {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    this.timeout = setTimeout(() => {
      this.tableReq['filters']['globalSearch'] = {
        dimensions: [],
        value: searchValue
      };

      this.tableReq['filters']['globalSearch']['dimensions'].push(this.filtersApplied['groupby'][0]['key'], 'hov',
        'sessions',
        'publisher_impressions',
        'publisher_inventory',
        'publisher_render_rate',
        'unique_user'
      );
      this.loadTableData(this.tableReq);
    }, 3000);
  }

  getGrpBys() {
    let grpBys = this.filtersApplied['groupby'].map(e => e.key);
    // let grpBys = [];
    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          grpBys.concat(
            this.filtersApplied['filters']['dimensions']
              .filter(f => f.values.length)
              .map(m => m.key)
          )
        )
      );
    }
    return grpBys;
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['name']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    } else if (type === 'ppt') {
      const chartElements = document.getElementsByTagName('ym-charts');
      const slides = [];
      [].forEach.call(chartElements, (el, index) => {
        const c = el.getElementsByTagName('canvas')[0];

        slides.push({
          title: `${this.appConfig['name']}`,
          base64data: this.libServ.canvasToImage('white', c),
          width: 8,
          height: 3
        });

        if (index === chartElements.length - 1) {
          const pptConfig = {
            filename: 'Charts',
            logo: {
              visible: true,
              path: '/assets/images/ym.png',
              width: 0.53125,
              height: 0.458333
            },
            slides: slides
          };
          this.pptExport.exportPPT(pptConfig);
        }
      });
    }
    return false;
  }

  loadVODLineChart(params) {
    this.ImpressionsLineChartJson = this.libServ.deepCopy(this.defaultChartJson);
    this.showImpressionsLineChart = false;
    this.dataFetchServ.getRevMgmtData(params).subscribe(data => {
      const chartData = data['data'] as [];
      if (typeof chartData === 'undefined' || !chartData.length) {
        this.noDataImpressionsLineChart = true;
      } else {
        const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));

        this.ImpressionsLineChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('YYYY-MM-DD')
        );
        const videoServiceArr = Array.from(
          new Set(chartData.map(r => r['video_service']))
        );
        const colors = this.libServ.dynamicColors(videoServiceArr.length);

        videoServiceArr.forEach((src, i) => {
          const pageViewsArr = [];
          datesArr.forEach((time_key, i2) => {
            let value = null;
            chartData.forEach((r, i1) => {
              if (r['video_service'] === src && r['time_key'] === time_key) {
                value = r['hov']
              }
            });
            pageViewsArr.push(value);
          });
          this.ImpressionsLineChartJson['chartData']['datasets'].push({
            label: src,
            type: 'bar',
            data: pageViewsArr,
            borderColor: colors[i],
            fill: false,
            backgroundColor: colors[i]
          });
        });

        this.ImpressionsLineChartJson['chartOptions']['title']['text'] = 'Most consumed Video Service';
        this.ImpressionsLineChartJson['chartOptions']['scales']['xAxes'][0]['scaleLabel']['labelString'] = 'Video Service';
        this.ImpressionsLineChartJson['chartOptions']['scales']['yAxes'][0]['scaleLabel']['labelString'] = 'HOV';
        this.showImpressionsLineChart = true;
        this.noDataImpressionsLineChart = false;
      }
    });
  }
  loadEODLineChart(params) {
    this.RevenueLineChartJson = this.libServ.deepCopy(this.defaultChartJson);
    this.showRevenueLineChart = false;
    this.dataFetchServ.getRevMgmtData(params).subscribe(data => {
      const chartData = data['data'] as [];
      if (typeof chartData === 'undefined' || !chartData.length) {
        this.noDataRevenueLineChart = true;
      } else {
        const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));

        this.RevenueLineChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('YYYY-MM-DD')
        );
        const eodServiceArr = Array.from(
          new Set(chartData.map(r => r['episode']))
        );
        const colors = this.libServ.dynamicColors(eodServiceArr.length);

        eodServiceArr.forEach((src, i) => {
          const pageViewsArr = [];
          datesArr.forEach((time_key, i2) => {
            let value = null;
            chartData.forEach((r, i1) => {
              if (r['episode'] === src && r['time_key'] === time_key) {
                value = r['hov']
              }
            });
            pageViewsArr.push(value);
          });
          this.RevenueLineChartJson['chartData']['datasets'].push({
            label: src,
            type: 'bar',
            data: pageViewsArr,
            borderColor: colors[i],
            fill: false,
            backgroundColor: colors[i]
          });
        });
        this.RevenueLineChartJson['chartOptions']['title']['text'] = 'Most Viewed Episode';
        this.RevenueLineChartJson['chartOptions']['scales']['xAxes'][0]['scaleLabel']['labelString'] = 'Episode';
        this.RevenueLineChartJson['chartOptions']['scales']['yAxes'][0]['scaleLabel']['labelString'] = 'HOV';
        this.showRevenueLineChart = true;
        this.noDataRevenueLineChart = false;
      }
    });
  }
  loadMODLineChart(params) {
    this.EcpmLineChartJson = this.libServ.deepCopy(this.defaultChartJson);
    this.showEcpmLineChart = false;
    this.dataFetchServ.getRevMgmtData(params).subscribe(data => {
      const chartData = data['data'] as [];
      if (typeof chartData === 'undefined' || !chartData.length) {
        this.noDataEcpmLineChart = true;
      } else {
        const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));

        this.EcpmLineChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('YYYY-MM-DD')
        );
        const mNameArr = Array.from(
          new Set(chartData.map(r => r['measure_names']))
        );
        const colors = this.libServ.dynamicColors(mNameArr.length);

        mNameArr.forEach((src, i) => {
          const pageViewsArr = [];
          datesArr.forEach((time_key, i2) => {
            let value = null;
            chartData.forEach((r, i1) => {
              if (r['measure_names'] === src && r['time_key'] === time_key) {
                value = parseFloat(r['hov']) + (parseFloat(r['hov']) * 0.2)
              }
            });
            pageViewsArr.push(value);
          });
          this.EcpmLineChartJson['chartData']['datasets'].push({
            label: src,
            type: 'bar',
            data: pageViewsArr,
            borderColor: colors[i],
            fill: false,
            backgroundColor: colors[i]
          });
        });
        this.EcpmLineChartJson['chartOptions']['title']['text'] = 'Estimated HOV by Audience';
        this.EcpmLineChartJson['chartOptions']['scales']['xAxes'][0]['scaleLabel']['labelString'] = 'Measure Name';
        this.EcpmLineChartJson['chartOptions']['scales']['yAxes'][0]['scaleLabel']['labelString'] = 'Estimated HOV';
        this.showEcpmLineChart = true;
        this.noDataEcpmLineChart = false;
      }
    });
  }

  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }

  sendEmail() {
    const imageConfig = {
      imgName: `${this.appConfig['name']}.png`,
      type: 'image/png'
    };

    this.htmltoimage
      .canvasImage(document.getElementById('capture'), imageConfig)
      .then(canvas => {
        this.dialogService.open(SendEmailComponent, {
          header: 'Email Screenshot',
          contentStyle: {
            'max-height': '80vh',
            width: '30vw',
            overflow: 'auto'
          },
          data: canvas.toDataURL('image/png')
        });
      });
  }

  changeReport() {
    this.tableReq = {
      dimensions: [],
      metrics: [
        'gross_revenue',
        'dp_impressions',
        'dfp_adserver_impressions',
        'station_revenue_share_distribution',
        'total_recoupable_expenses'
      ],
      derived_metrics: ['gross_ecpm', 'client_net_ecpm', 'client_net_share'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: this.reportType == 'isDetailed' ? ['time_key', 'accounting_key'] : ['accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys(),
      orderBy: [
        { key: 'accounting_key', opcode: 'asc' }

        // { key: 'derived_station_rpt', opcode: 'asc' }
      ],
      limit: '',
      offset: ''
    };
    this.loadTableData(this.tableReq);
  }

  openExportPopup(row, field) {

    const grpBys = [];
    const filtersApplied = this.libServ.deepCopy(this.filtersApplied);
    const fieldIdx = filtersApplied['groupby'].findIndex(
      e => e['key'] === field
    );
    filtersApplied['groupby'].forEach((grp, i) => {
      if (i <= fieldIdx) {
        if (
          filtersApplied['filters']['dimensions'].findIndex(
            e => e.key === grp['key']
          ) !== -1
        ) {
          filtersApplied['filters']['dimensions'].splice(
            filtersApplied['filters']['dimensions'].findIndex(
              e => e.key === grp['key']
            ),
            1
          );
        }
        filtersApplied['filters']['dimensions'].push({
          key: grp['key'],
          values:
            grp['key'] == 'accounting_key'
              ? [moment(row[grp['key']]).format('YYYYMMDD')]
              : [row[grp['key']]]
        });
      }
    });

    filtersApplied['groupby'] = grpBys;
    const data = {
      filters: filtersApplied,
      field: row[field],
      appName: this.exportRequest['appName'],
      email: this.exportRequest['sendEmail']
    };
    const ref = this.dialogService.open(OTTAnalyticsExportPopupComponent, {
      header: row[field] + ' Daily Distribution',
      contentStyle: { width: '80vw', overflow: 'auto' },
      data: data
    });
    ref.onClose.subscribe((data: string) => { });

    return false;
  }

  getGrpBysKey(key) {
    let grpBys = key;
    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          grpBys.concat(
            this.filtersApplied['filters']['dimensions']
              .filter(f => f.values.length)
              .map(m => m.key)
          )
        )
      );
    }
    return grpBys;
  }
}
