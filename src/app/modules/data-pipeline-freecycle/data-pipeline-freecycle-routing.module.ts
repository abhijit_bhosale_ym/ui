import { Routes } from '@angular/router';
import { DataPipelineFreecycleComponent } from './data-pipeline-freecycle.component';

export const DataPipelineAppRoutesAppRoutes: Routes = [
  {
    path: '',
    component: DataPipelineFreecycleComponent
  }
];
