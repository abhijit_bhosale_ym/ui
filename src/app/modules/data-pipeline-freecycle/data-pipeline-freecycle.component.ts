import { Component, OnInit, OnDestroy } from '@angular/core';
import { TreeNode } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { CommonLibService } from 'src/app/_services';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';
import { FetchUnfilledApiDataService } from './fetch-data-pipeline-api-data.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { Subscription } from 'rxjs';
import { SendEmailComponent } from '../send-email/send-email.component';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-data-pipeline-freecycle',
  templateUrl: './data-pipeline-freecycle.component.html',
  styleUrls: ['./data-pipeline-freecycle.component.scss']
})
export class DataPipelineFreecycleComponent implements OnInit, OnDestroy {
  appConfig: object = {};
  lastUpdatedOn: Date;

  aggTableData: TreeNode[];
  dimColDef: any[];
  metricColDef: any[];
  aggTableColumnDef: any[];
  aggTableJson: Object;
  displayAggTable = false;
  appConfigObs: Subscription;
  cars: any[];
  rowGroupMetadata: any;

  flatTableData: TreeNode[];
  flatTableColumnDef: any[];
  flatTableJson: Object;
  imgSrcJson: Object;
  exportRequest: ExportRequest = <ExportRequest>{};
  isExportReport = false;

  searchvalue = "";
  searchColDef = ['updated_at', 'source', 'name', 'source_link', 'source_updated_through'];
  newData: any[];

  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: FetchUnfilledApiDataService,
    private toastService: ToastService,
    private htmltoimage: HtmltoimageService,
    private pptExport: ExportPptService,
    private exportService: ExportdataService,
    private dialogService: DialogService
  ) { }

  ngOnInit() {
    this.imgSrcJson = {
      Prebid: { src: 'prebid.png', link: 'NA' },
      'Amazon(HB)': {
        src: 'amazon.png',
        link: 'https://ams.amazon.com/webpublisher/analytics/pub_ss_earnings'
      },
      Brightcom: {
        src: 'brightcom.png',
        link: 'https://compass.brightcom.com/onetag/'
      },
      'Google Ad Exchange': {
        src: 'ad manager 2.jpg',
        link: 'https://admanager.google.com/'
      },
      'Index Exchange(HB)': {
        src: 'index_exchange_hb.png',
        link: 'https://system.indexexchange.com/publisher'
      },
      Nativo: { src: 'nativo.jpg', link: 'https://admin.nativo.com/dashboard' },
      'OpenX(HB)': {
        src: 'openx_hb.jpg',
        link: 'http://observermedia-ui.openx.net/#dashboard?'
      },
      Outbrain: { src: 'outbrain.png', link: 'https://my.outbrain.com/login' },
      Rubicon: {
        src: 'rubicon_hb.png',
        link: 'https://platform.rubiconproject.com/#/'
      },
      'Sonobi(HB)': {
        src: 'sonobi_hb.png',
        link: 'https://jetstream.sonobi.com/#/path=home'
      },
      Teads: {
        src: 'teads.png',
        link:
          'https://login.teads.tv/login?redirectUrl=https:~2F~2Fmanager.teads.tv~2Fcampaign~2Fedit~2F84309~2Finsertion~2F279946&clientId=manager'
      },
      Analytics: { src: 'NA', link: 'NA' },
      DFP: { src: 'NA', link: 'NA' },
      '33Across': { src: 'NA', link: 'NA' },
      'Antenna Deals': { src: 'NA', link: 'NA' },
      AppNexus: { src: 'NA', link: 'NA' },
      Connatix: { src: 'NA', link: 'NA' },
      Conversant: { src: 'NA', link: 'NA' },
      'Google Ad Exchange - PD & PG': { src: 'NA', link: 'NA' },
      'Google Adsense for Search': { src: 'NA', link: 'NA' },
      'Google EB - EB.SpotX': { src: 'NA', link: 'NA' },
      'Google EB - Frankly.IndexExchange': { src: 'NA', link: 'NA' },
      'Google EB - Frankly.Rubicon': { src: 'NA', link: 'NA' },
      'Google EB - Frankly.RythmOne': { src: 'NA', link: 'NA' },
      'Google EB - Frankly.Smaato-EB': { src: 'NA', link: 'NA' },
      'Google EB - Frankly.Sovrn': { src: 'NA', link: 'NA' },
      'Google EB - Frankly.YieldMo-EB': { src: 'NA', link: 'NA' },
      'Google EB - [nw] Openx': { src: 'NA', link: 'NA' },
      'Google EB - [nw] PubMatic': { src: 'NA', link: 'NA' },
      'Google EB - [nw] TripleLift': { src: 'NA', link: 'NA' },
      RevContent: { src: 'NA', link: 'NA' },
      SlingTV: { src: 'NA', link: 'NA' },
      'Smart Asset': { src: 'NA', link: 'NA' },
      YieldMo: { src: 'NA', link: 'NA' },
      Unfilled: { src: 'NA', link: 'NA' },
      'Google Analytics': { src: 'NA', link: 'NA' },
      'DFP Pricing Rules': { src: 'NA', link: 'NA' },
      'Google EB - EB.Verizon.Video': { src: 'NA', link: 'NA' },
      SpotXchange: { src: 'NA', link: 'NA' },
      Consumables: { src: 'NA', link: 'NA' },
      AAX: { src: 'NA', link: 'NA' },
      OpenX: { src: 'NA', link: 'NA' },
      'Just Premium': { src: 'NA', link: 'NA' },
    };

    this.flatTableColumnDef = [
      {
        field: 'name',
        displayName: 'App Name',
        format: '',
        width: '175',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'status',
        displayName: 'Status',
        format: '',
        width: '80',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'source',
        displayName: 'Source',
        format: '',
        width: '175',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'updated_at',
        displayName: 'Last Updated At (EDT)',
        format: '',
        width: '175',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'source_updated_through',
        displayName: 'Updated Through',
        format: '',
        width: '175',
        exportConfig: {
          format: 'date<<MM-DD-YYYY<<-',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: true
        }
      }
    ];
    this.flatTableJson = {
      page_size: 10,
      page: 1,
      lazy: true,
      loading: true,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '200px',
      totalRecords: 1000,
      columns: this.flatTableColumnDef,
      selectedColumns: this.flatTableColumnDef,
      frozenCols: [this.flatTableColumnDef[0]],
      // frozenWidth: '250px',String startDate = (String) params.get("startDate");
      // 		String endDate = (String) params.get("endDate");
      // 		ArrayList<String> status= (ArrayList<String>) params.get("status");
      // 		String user_id=params.get("user_id").toString();
      //
      // 		        Map<String, Object> billingMap = new HashMap<String, Object>();
      // 		      	billingMap.put("startDate", startDate);
      // 				billingMap.put("endDate", endDate);
      // 				billingMap.put("status",status);
      // 				billingMap.put("user_id",user_id);
      frozenWidth:
        this.flatTableColumnDef
          .slice(0, 1)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        this.isExportReport = this.appConfig['permissions'].some(
          o => o.name == 'data-pipeline-export-report'
        );
        this._titleService.setTitle(this.appConfig['displayName']);
        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        this.initialLoading();
      }
    });
  }

  initialLoading() {
    this.dataFetchServ.getData().subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }
      this.rowGroupMetadata = {};
      const data2 = data as [];
      const colorArr = [
        '#b3e5fc',
        '#ffccbc',
        '#b9f6ca',
        '#cfd8dc',
        '#e1bee7',
        '#b2dfdb'
      ];
      const arr = [];

      for (const r of data2) {
        const obj = { data: { rowspan: {} } };

        obj['data'] = r;

        // obj['data']['imgsrc'] =
        //   '../../assets/source_logos/' + this.imgSrcJson[r['source']]['src'];
        // obj['data']['link'] = this.imgSrcJson[r['source']]['link'];

        obj['data']['imgsrc'] =
          '../../assets/source_logos/';
        // obj['data']['link'] = this.imgSrcJson[r['source']]['link'];
        obj['data']['link'] = r['source_link'];

        if (r['status'] === 'done') {
          obj['data']['status'] = 'fav fa fa-check-square text-success';
        } else if (r['status'] === 'stopped') {
          obj['data']['status'] = 'fav fa fa-ban text-danger';
        } else if (r['status'] === 'inprogress') {
          obj['data']['status'] = 'fav fa fa-exclamation-triangle text-warning';
        }

        obj['data']['source_updated_through'] = moment(
          r['source_updated_through'],
          'YYYYMMDD'
        ).format('MM-DD-YYYY');

        arr.push(obj);
      }

      let index = 0;
      let counter = 0;
      if (arr) {
        for (let i = 0; i < arr.length; i++) {
          const rowData = arr[i].data;
          const appname = rowData.name;
          if (i === 0) {
            this.rowGroupMetadata[appname] = {
              index: 0,
              size: 1,
              isRow: 'false'
            };
            arr[i].data['rowspan'] = 1;
            arr[i].data['color'] = colorArr[counter];
            index++;
          } else {
            const previousRowData = arr[i - 1]['data'];
            const previousRowGroup = previousRowData.name;
            if (appname === previousRowGroup) {
              this.rowGroupMetadata[appname].size++;
              arr[i].data['color'] = colorArr[counter];
            } else {
              const obj = {};
              this.rowGroupMetadata[appname] = {
                index: i,
                size: 1,
                isRow: 'false'
              };
              counter++;
              arr[i].data['color'] = colorArr[counter];
            }
          }
        }
      }
      this.newData = arr;
      this.flatTableData = arr;
      this.flatTableJson['loading'] = false;
    });
  }

  onGlobalSearchChanged(searchvalue) {
    const colorArr = [
      '#b3e5fc',
      '#ffccbc',
      '#b9f6ca',
      '#cfd8dc',
      '#e1bee7',
      '#b2dfdb'
    ];
    let index = 0;
    let counter = 0;
    let arr = this.searchVal(this.newData, this.searchColDef, searchvalue);
    if (arr) {
      for (let i = 0; i < arr.length; i++) {
        const rowData = arr[i].data;
        const appname = rowData.name;
        if (i === 0) {
          this.rowGroupMetadata[appname] = {
            index: 0,
            size: 1,
            isRow: 'false'
          };
          arr[i].data['rowspan'] = 1;
          arr[i].data['color'] = colorArr[counter];
          index++;
        } else {
          const previousRowData = arr[i - 1]['data'];
          const previousRowGroup = previousRowData.name;
          if (appname === previousRowGroup) {
            this.rowGroupMetadata[appname].size++;
            arr[i].data['color'] = colorArr[counter];
          } else {
            const obj = {};
            this.rowGroupMetadata[appname] = {
              index: i,
              size: 1,
              isRow: 'false'
            };
            counter++;
            arr[i].data['color'] = colorArr[counter];
          }
        }
      }
    }
    this.flatTableData = arr;
  }
  searchVal(items: any[], field: string[], value: string) {
    let v = [];
    if (value !== '') {
      items.forEach(x => {
        let isAvailable = true;
        field.forEach(element => {
          if (x['data'][element] !== null && isAvailable) {
            if (element === 'updated_at') {
              if (moment(x['data'][element], 'YYYY-MM-DD HH:mm:ss').format('MM-DD-YYYY HH:mm:ss ').includes(value)) {
                v.push(x);
                isAvailable = false;
              }
            }
            else {
              if (typeof x['data'][element] === "number") {
                if (x['data'][element].toFixed(2).includes(value)) {
                  v.push(x);
                  isAvailable = false;
                }
              } else {
                if (x['data'][element].toLowerCase().includes(value.toLowerCase())) {
                  isAvailable = false;
                  v.push(x);
                }
              }
            }
          }
        });
      })
      return v;
    } else {
      return items;
    }
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['name']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    }
    return false;
  }

  exportTable(table, fileFormat) {
    // console.log('col',this.flatTableColumnDef)
    if (this.exportRequest['sendEmail'].length > 0) {
      this.toastService.displayToast({
        severity: 'info',
        summary: 'Export Report',
        detail:
          'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
        life: 10000
      });
      if (table === 'flat') {
        const columnDefs = this.libServ.deepCopy(
          this.flatTableColumnDef
        );
        columnDefs.splice(1, 1);

        this.dataFetchServ.getData().subscribe(response => {
          let data = [];
          data = response as [];

          data.forEach(ele => {
            ele['updated_at'] = moment(ele['updated_at'], 'YYYY-MM-DD HH:mm:ss').format('MM-DD-YYYY HH:mm:ss ');
          })

          const sheetDetails = {};
          sheetDetails['columnDef'] = columnDefs;
          sheetDetails['data'] = data;
          sheetDetails['sheetName'] = 'Data Pipeline';
          sheetDetails['isRequest'] = false;
          sheetDetails['request'] = {
            url: '',
            method: '',
            param: {}
          };
          sheetDetails['disclaimer'] = [
            {
              position: 'bottom',
              label: 'Note: Data present in the table may vary over a period of time.',
              color: '#000000'

            }
          ];
          sheetDetails['totalFooter'] = {
            available: false,
            custom: false
          };
          sheetDetails['tableTitle'] = {
            available: false,
            label: 'Data Pipeline Status'
          };
          sheetDetails['image'] = [
            {
              available: true,
              path: environment.exportConfig.exportLogo,
              position: 'top'
            }
          ];
          const sheetDetailsarray = [];
          sheetDetailsarray.push(sheetDetails);
          this.exportRequest['sheetDetails'] = <SheetDetails[]>(
            sheetDetailsarray
          );
          this.exportRequest['fileName'] =
            'Data Pipeline Status ' +
            moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
          this.exportRequest['exportFormat'] = fileFormat;
          this.exportRequest['exportConfig'] = environment.exportConfig;
          // console.log('exportreport', this.exportRequest);
          // return false;
          this.dataFetchServ
            .getExportReportData(this.exportRequest)
            .subscribe(response => {
              console.log(response);
            });
        });

      }
    } else {
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Export Report',
        detail:
          'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
        life: 10000
      });
    }
    return false;
  }

  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }


  sendEmail() {
    const imageConfig = {
      imgName: `${this.appConfig['name']}.png`,
      type: 'image/png'
    };

    this.htmltoimage
      .canvasImage(document.getElementById('capture'), imageConfig)
      .then(canvas => {
        this.dialogService.open(SendEmailComponent, {
          header: 'Email Screenshot',
          contentStyle: {
            'max-height': '80vh',
            width: '30vw',
            overflow: 'auto'
          },
          data: canvas.toDataURL('image/png')
        });
      });
  }
}
