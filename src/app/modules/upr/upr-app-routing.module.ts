import { Routes } from '@angular/router';
import { UPRAppComponent } from './upr-app.component';

export const UPRAppRoutes: Routes = [
  {
    path: '',
    component: UPRAppComponent
  }
];
