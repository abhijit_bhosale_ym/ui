import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UPRExportPopupComponent } from './upr-export-popup.component';

describe('UPRExportPopupComponent', () => {
  let component: UPRExportPopupComponent;
  let fixture: ComponentFixture<UPRExportPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UPRExportPopupComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UPRExportPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
