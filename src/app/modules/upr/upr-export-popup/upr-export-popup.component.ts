import { Component, OnInit } from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { FetchApiDataService } from '../fetch-api-data.service';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import * as moment from 'moment';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-upr-export-popup',
  templateUrl: './upr-export-popup.component.html',
  styleUrls: ['./upr-export-popup.component.scss']
})
export class UPRExportPopupComponent implements OnInit {
  configData: object;
  exportRequest: ExportRequest = <ExportRequest>{};
  constructor(
    private libServ: CommonLibService,
    private dataFetchServ: FetchApiDataService,
    private exportService: ExportdataService,
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private toastService: ToastService
  ) {
    this.configData = this.config.data;
  }

  ngOnInit() {
    this.exportRequest = this.configData['exportRequest'];
  }

  async downloadData(daily) {
    this.toastService.displayToast({
      severity: 'info',
      summary: 'Export Report',
      detail:
        'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
      life: 10000
    });
    const filtersApplied = this.configData['filters'];
    const colDef = this.configData['colDef'];
    let grpBys = filtersApplied['groupby'].map(e => e.key);
    if (
      this.libServ.isEmptyObj(filtersApplied['filters']['dimensions']) ||
      filtersApplied['filters']['dimensions'].length === 0
    ) {
      filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          filtersApplied['filters']['dimensions']
            .filter(f => f.values.length)
            .map(m => m.key)
            .concat(grpBys)
        )
      );
    }

    if (daily) {
      grpBys.push('time_key');
      const tableReq = {
        dimensions: ['time_key', 'unified_pricing_rule'],
        metrics: [
          'adx_ad_request',
          'adx_impressions',
          'total_estimated_revenue',
          'total_impressions',
          'adx_estimated_revenue',
          'adx_impressions',
          'adx_matched_request',
          'yield_group_estimated_revenue',
          'yield_group_impressions'
        ],
        derived_metrics: [
          'yield_group_ecpm',
          'adx_ecpm',
          'total_ecpm',
          'adx_fill_rate'
        ],
        timeKeyFilter: this.libServ.deepCopy(filtersApplied['timeKeyFilter']),
        filters: this.libServ.deepCopy(filtersApplied['filters']),
        groupByTimeKey: {
          key: ['time_key', 'accounting_key'],
          interval: 'daily'
        },
        gidGroupBy: ['unified_pricing_rule'],
        orderBy: [{ key: 'time_key', opcode: 'asc' }],
        limit: '',
        offset: ''
      };

      tableReq['isTable'] = false;
        const obj = {
          field: 'time_key',
          displayName: 'Date',
          format: 'date',
          width: '150',
          exportConfig: {
            format: 'date',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: false
          }
        };

        if (!colDef.some(o => o.field == 'time_key')) {
          colDef.splice(0, 0, obj);
        }

        const sheetDetails = {};
      sheetDetails['columnDef'] = colDef;
      sheetDetails['data'] = [];
      sheetDetails['sheetName'] = 'Daily Distribution';
      sheetDetails['isRequest'] = true;
      sheetDetails['request'] = {
        url: '/api/flask/upr/cassandradata',
        method: 'POST',
        param: tableReq
      };
      sheetDetails['disclaimer'] = [
        {
          position: 'top',
          label:
            'Data persent in below table is not final and may varies over period of time',
          color: '#3A37CF'
        },
        {
          position: 'bottom',
          label: 'Thank You',
          color: '#EC6A15'
        }
      ];
      sheetDetails['totalFooter'] = {
        available: true,
        custom: true
      };
      sheetDetails['tableTitle'] = {
        available: true,
        label: 'Daily Distribution'
      };
      sheetDetails['image'] = [
        {
          available: true,
          path: environment.exportConfig.exportLogo,
          position: 'top'
        }
      ];
      const sheetDetailsarray = [];
      sheetDetailsarray.push(sheetDetails);
      this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
      this.exportRequest['fileName'] =
        'UPR Daily Data ' + moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
      this.exportRequest['exportFormat'] = this.configData['fileFormat'];
      this.exportRequest['exportConfig'] = environment.exportConfig;
      // console.log('exportreport', this.exportRequest);
      // return false;
      this.dataFetchServ
        .getExportReportData(this.exportRequest)
        .subscribe(response => {
          console.log(response);
        });
    } else {
      const tableReq = {
        dimensions: ['unified_pricing_rule'],
        metrics: [
          'adx_ad_request',
          'adx_impressions',
          'total_estimated_revenue',
          'total_impressions',
          'adx_estimated_revenue',
          'adx_impressions',
          'adx_matched_request',
          'yield_group_estimated_revenue',
          'yield_group_impressions'
        ],
        derived_metrics: [
          'yield_group_ecpm',
          'adx_ecpm',
          'total_ecpm',
          'adx_fill_rate'
        ],
        timeKeyFilter: this.libServ.deepCopy(filtersApplied['timeKeyFilter']),
        filters: this.libServ.deepCopy(filtersApplied['filters']),
        groupByTimeKey: { key: [], interval: 'daily' },
        gidGroupBy: ['unified_pricing_rule'],
        orderBy: [{ key: 'unified_pricing_rule', opcode: 'asc' }],
        limit: '',
        offset: ''
      };

      const temp = 0;
      const data = [];

      tableReq['isTable'] = false;
      const sheetDetails = {};
      sheetDetails['columnDef'] = colDef;
      sheetDetails['data'] = [];
      sheetDetails['sheetName'] = 'Data Distribution';
      sheetDetails['isRequest'] = true;
      sheetDetails['request'] = {
        url: '/api/flask/upr/cassandradata',
        method: 'POST',
        param: tableReq
      };
      sheetDetails['disclaimer'] = [
        {
          position: 'top',
          label:
            'Data persent in below table is not final and may varies over period of time',
          color: '#3A37CF'
        },
        {
          position: 'bottom',
          label: 'Thank You',
          color: '#EC6A15'
        }
      ];
      sheetDetails['totalFooter'] = {
        available: true,
        custom: true
      };
      sheetDetails['tableTitle'] = {
        available: true,
        label: 'UPR Aggregated Data'
      };
      sheetDetails['image'] = [
        {
          available: true,
          path: environment.exportConfig.exportLogo,
          position: 'top'
        }
      ];
      const sheetDetailsarray = [];
      sheetDetailsarray.push(sheetDetails);
      this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
      this.exportRequest['fileName'] =
        'UPR Aggregated Data ' + moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
      this.exportRequest['exportFormat'] = this.configData['fileFormat'];
      this.exportRequest['exportConfig'] = environment.exportConfig;
      // console.log('exportreport', this.exportRequest);
      // return false;
      this.dataFetchServ
      .getExportReportData(this.exportRequest)
      .subscribe(response => {
        console.log(response);
      });
    }
  }

  cancel() {
    this.ref.close(null);
  }
}
