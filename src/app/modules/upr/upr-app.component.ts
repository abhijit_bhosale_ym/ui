import { Component, OnInit, OnDestroy } from '@angular/core';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { CommonLibService } from 'src/app/_services';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';
import { FetchApiDataService } from './fetch-api-data.service';
import { UPRDailyDataPopupComponent } from './upr-daily-data-popup/upr-daily-data-popup.component';
import { UPRExportPopupComponent } from './upr-export-popup/upr-export-popup.component';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { Subscription } from 'rxjs';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { SendEmailComponent } from '../send-email/send-email.component';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';

@Component({
  selector: 'ym-upr-app',
  templateUrl: './upr-app.component.html',
  styleUrls: ['./upr-app.component.scss']
})
export class UPRAppComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;

  appConfig: object = {};
  lastUpdatedOn: Date;

  cardsJson = [];
  showCards = true;
  showMainLineChart = false;
  noDataMainLineChart = false;
  showMainPieChart = false;
  showCharts = false;
  defaultChartsJson: object;
  mainLineChartJson: object;
  mainPieChartJson: object;
  noDataPieChart = false;
  aggTableData: TreeNode[];
  noTableData = false;
  tableDimColDef: any[];
  metricColDef: any[];
  aggTableColumnDef: any[];
  aggTableJson: Object;
  displayAggTable = false;
  toggleView = true;
  filtersApplied: object = {};
  exportReportData: object = {};
  exportRequest: ExportRequest = <ExportRequest>{};
  dataStr: any;
  nextUpdated: Date;
  dataUpdatedThrough: Date;
  isExportReport = false;

  appFeaturesConfigs = {};
  mainCardsConfig = {};

  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private htmltoimage: HtmltoimageService,
    private exportService: ExportdataService,
    private pptExport: ExportPptService,
    private formatNumPipe: FormatNumPipe,
    private confirmationService: ConfirmationService
  ) { }

  ngOnInit() {
    this.tableDimColDef = [
      {
        field: 'unified_pricing_rule',
        displayName: 'Unified Pricing Rule',
        format: '',
        width: '200',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: '-'
      },
      {
        field: 'total_impressions',
        displayName: 'Total Impressions',
        format: 'number',
        width: '115',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: 0
      },
      {
        field: 'total_estimated_revenue',
        displayName: 'Total Revenue',
        width: '110',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: 0
      },
      {
        field: 'total_ecpm',
        displayName: 'Total eCPM',
        format: '$',
        width: '110',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: 0
      },
      {
        field: 'yield_group_impressions',
        displayName: 'Yield Group Impressions',
        format: 'number',
        width: '130',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: 0
      },
      {
        field: 'yield_group_estimated_revenue',
        displayName: 'Yield Group Estimated Revenue',
        width: '160',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: 0
      },
      {
        field: 'yield_group_ecpm',
        displayName: 'Yield Group eCPM',
        format: '$',
        width: '120',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: 0
      },
      {
        field: 'adx_ad_request',
        displayName: 'AdX Ad Requests',
        format: 'number',
        width: '105',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: 0
      },
      {
        field: 'adx_matched_request',
        displayName: 'AdX Matched Requests',
        format: 'number',
        width: '125',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: 0
      },
      {
        field: 'adx_impressions',
        displayName: 'Adx Impressions',
        format: 'number',
        width: '120',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: 0
      },
      {
        field: 'adx_estimated_revenue',
        displayName: 'Adx Estimated Revenue',
        width: '115',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: 0
      },
      {
        field: 'adx_ecpm',
        displayName: 'Adx eCPM',
        format: '$',
        width: '105',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: 0
      },
      {
        field: 'adx_fill_rate',
        displayName: 'Adx Fill Rate',
        format: 'percentage',
        width: '105',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      }
    ];
    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,

      // columns: this.tableDimColDef,
      // selectedColumns: this.aggTableColumnDef,
      // frozenCols: [],
      // frozenWidth: '0px',
      footerColumns: this.tableDimColDef.slice(1),
      columns: this.tableDimColDef.slice(4),
      selectedColumns: this.tableDimColDef.slice(4),
      frozenCols: [...this.tableDimColDef.slice(0, 4)],
      frozenWidth:
        this.tableDimColDef
          .slice(0, 4)
          .reduce((tot, cur) => tot + parseInt(cur.width), 0) + 'px',

      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: false,
      columnSearch: false
    };

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        console.log('appConfig', this.appConfig);
        this.isExportReport = this.appConfig['permissions'].some(
          o => o.name === 'upr-export-report'
        );
        this._titleService.setTitle(this.appConfig['displayName']);
        let startDate;
        if (
          this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
          'defaultDate'
          ][0]['startOf']
        ) {
          startDate = moment()
            .subtract(
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['value'],
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['period']
            )
            .startOf(
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['period']
            );
        } else {
          startDate = moment().subtract(
            this.appConfig['filter']['filterConfig']['filters'][
            'datePeriod'
            ][0]['defaultDate'][0]['value'],
            this.appConfig['filter']['filterConfig']['filters'][
            'datePeriod'
            ][0]['defaultDate'][0]['period']
          );
        }
        const endDate = moment().subtract(
          this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
          'defaultDate'
          ][1]['value'],
          this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
          'defaultDate'
          ][1]['period']
        );

        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: startDate.format('YYYYMMDD'),
            time_key2: endDate.format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: [{ key: 'unified_pricing_rule' }]
        };
        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        this.initialLoading();
      }
    });
  }

  initialLoading() {
    this.dataFetchServ
      .getLastUpdatedData(this.appConfig['id'])
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        this.lastUpdatedOn = moment(data[0]['updated_at']).toDate();
        this.nextUpdated = moment(data[0]['next_run_at']).toDate();
        this.dataUpdatedThrough = moment(
          data[0]['source_updated_through'],
          'YYYYMMDD'
        ).toDate();
      });

    this.libServ.getAppFeaturesConfigs(this.appConfig['id']).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: data['message']
        });
        console.log(data['message']);
        return;
      }
      this.appFeaturesConfigs = data['data'];
      this.appFeaturesConfigs['cardsConfig']['cards'].forEach(element => {
        if (element['name'] === 'main') {
          this.mainCardsConfig = element['config'];
          this.cardsJson = this.mainCardsConfig['list'];
          if (this.mainCardsConfig['display']) {
            this.loadCards();
          }
        }
      });
    });

    this.loadTableData();

    const mainLineChartReq = {
      dimensions: ['time_key'],
      metrics: ['total_estimated_revenue', 'total_impressions'],
      derived_metrics: ['total_ecpm'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };
    this.loadMainLineChart(mainLineChartReq);

    const mainPieChartReq = {
      dimensions: ['unified_pricing_rule'],
      metrics: ['total_estimated_revenue'],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: ['unified_pricing_rule'],
      orderBy: [],
      limit: '',
      offset: ''
    };
    this.loadMainPieChart(mainPieChartReq);
  }

  resetPagination(tt1) {
    if (typeof tt1 !== 'undefined') {
      tt1.reset();
    }
  }

  loadTableData() {
    this.reloadAggTable();

    this.aggTableJson['loading'] = true;

    const tableReq = {
      dimensions: ['unified_pricing_rule'],
      metrics: [
        'adx_ad_request',
        'adx_impressions',
        'total_estimated_revenue',
        'total_impressions',
        'adx_estimated_revenue',
        'adx_impressions',
        'adx_matched_request',
        'yield_group_estimated_revenue',
        'yield_group_impressions'
      ],
      derived_metrics: [
        'yield_group_ecpm',
        'adx_ecpm',
        'total_ecpm',
        'adx_fill_rate'
      ],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: { key: [], interval: 'daily' },
      gidGroupBy: ['unified_pricing_rule'],
      orderBy: [{ key: 'unified_pricing_rule', opcode: 'asc' }],
      limit: '',
      offset: ''
    };

    const tableTotalReq = {
      dimensions: [],
      metrics: [
        'adx_ad_request',
        'adx_impressions',
        'total_estimated_revenue',
        'total_impressions',
        'adx_estimated_revenue',
        'adx_impressions',
        'adx_matched_request',
        'yield_group_estimated_revenue',
        'yield_group_impressions'
      ],
      derived_metrics: [
        'yield_group_ecpm',
        'adx_ecpm',
        'total_ecpm',
        'adx_fill_rate'
      ],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: { key: [], interval: 'daily' },
      gidGroupBy: ['source', 'unified_pricing_rule'],
      orderBy: [],
      limit: '',
      offset: ''
    };
    this.dataFetchServ.getUPRData(tableReq).subscribe(data => {
      this.dataFetchServ.getUPRData(tableTotalReq).subscribe(data1 => {
        if (data['status'] === 0) {
          this.noTableData = true;
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        } else {
          this.noTableData = false;
        }

        const tableData = data['data'];
        const tableTotalData = data1['data'];

        const arr = [];
        tableData.forEach((row: object) => {
          let obj = {};
          obj = {
            data: row
          };
          arr.push(obj);
        });

        if (tableTotalData[0] !== undefined) {
          for (const key in tableTotalData[0]) {
            if (this.tableDimColDef.some(o => o.field == key)) {
              if (key != 'unified_pricing_rule') {
                this.tableDimColDef.find(x => x.field == key).footerTotal =
                  tableTotalData[0][key];
              }
            }
          }
        } else {
          this.aggTableJson['footerColumns'].forEach(c => {
            c['footerTotal'] = 0;
          });
        }

        this.aggTableData = <TreeNode[]>arr;
        this.aggTableJson['totalRecords'] = data['totalItems'];
        this.aggTableJson['loading'] = false;
      });
    });
  }

  reloadAggTable() {
    this.displayAggTable = false;
    setTimeout(() => {
      this.displayAggTable = true;
    }, 0);
  }

  loadCards() {
    this.showCards = false;
    const apiUrl = this.mainCardsConfig['api']['url'];
    const cardsReq = this.libServ.deepCopy(
      this.mainCardsConfig['api']['request']
    );
    cardsReq['timeKeyFilter'] = this.libServ.deepCopy(
      this.filtersApplied['timeKeyFilter']
    );
    cardsReq['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);

    this.libServ.getDataFromDynamicAPI(apiUrl, cardsReq).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error - Contact to administator',
          detail: data
        });
        console.log(data['status_msg']);
        return;
      }
      if (!this.libServ.isEmptyObj(data['data'])) {
        this.cardsJson.map(o => {
          o['value'] = data['data'][0][o['field']];
        });
      } else {
        this.cardsJson.map(o => {
          o['value'] = 0;
        });
      }
      this.showCards = this.mainCardsConfig['display'];
    });
  }

  onLazyLoadAggTable(e: Event) {
    console.log('Lazy Agg', e);
  }

  openPopup(row, field) {
    const grpBys = [];
    const filtersApplied = this.libServ.deepCopy(this.filtersApplied);

    const fieldIdx = filtersApplied['groupby'].findIndex(
      e => e['key'] === field
    );
    filtersApplied['groupby'].forEach((grp, i) => {
      if (i <= fieldIdx) {
        if (
          filtersApplied['filters']['dimensions'].findIndex(
            e => e.key === grp['key']
          ) !== -1
        ) {
          filtersApplied['filters']['dimensions'].splice(
            filtersApplied['filters']['dimensions'].findIndex(
              e => e.key === grp['key']
            ),
            1
          );
        }
        filtersApplied['filters']['dimensions'].push({
          key: grp['key'],
          values:
            grp['key'] == 'accounting_key'
              ? [moment(row[grp['key']]).format('YYYYMMDD')]
              : [row[grp['key']]]
        });
      }
    });

    filtersApplied['groupby'] = grpBys;
    const data = {
      filters: filtersApplied,
      heading: row[field],
      exportRequest: this.exportRequest,
      isExportReport: this.isExportReport
    };
    const ref = this.dialogService.open(UPRDailyDataPopupComponent, {
      header: row[field] + ' Distribution',
      contentStyle: { width: '80vw', overflow: 'auto' },
      data: data
    });
    ref.onClose.subscribe((data: string) => { });
  }

  exportTable(table, fileFormat) {
    if (this.exportRequest['sendEmail'].length > 0) {
      if (this.aggTableData.length == 0) {
        this.confirmationService.confirm({
          message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
          header: 'Information',
          icon: 'pi pi-exclamation-triangle',
          accept: () => {
          },
          reject: () => {
          }
        });
      } else {
      const data = {
        filters: this.filtersApplied,
        colDef: this.tableDimColDef,
        fileFormat: fileFormat,
        exportRequest: this.exportRequest
      };

      const ref = this.dialogService.open(UPRExportPopupComponent, {
        header: 'Download Report',
        contentStyle: { 'max-height': '80vh', overflow: 'auto' },
        data: data
      });
      ref.onClose.subscribe((data: string) => { });
    }
    } else {
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Export Report',
        detail:
          'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
        life: 10000
      });
    }
    return false;
  }

  isHiddenColumn(col: Object) {
    return (
      this.aggTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }

  /* --------------------- On Filter Apply Button Clicked --------------------- */

  onFiltersApplied(filterData: object) {
    this.filtersApplied['filters']['dimensions'] = [];

    // tslint:disable-next-line: forin
    for (const k in filterData['filter']['dimensions']) {
      if (filterData['filter']['dimensions'][k].length !== 0) {
        this.filtersApplied['filters']['dimensions'].push({
          key: k,
          values: filterData['filter']['dimensions'][k]
        });
      }
    }

    // this.filtersApplied['groupby'] = filterData['groupby'];
    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];

    this.loadTableData();

    const grpBys = this.getGrpBys();

    if (this.mainCardsConfig['display']) {
      this.mainCardsConfig['api']['request']['gidGroupBy'] = grpBys;
      this.loadCards();
    }

    const mainLineChartReq = {
      dimensions: ['time_key'],
      metrics: ['total_estimated_revenue', 'total_impressions'],
      derived_metrics: ['total_ecpm'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: grpBys,
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };

    this.loadMainLineChart(mainLineChartReq);
    const mainPieChartReq = {
      dimensions: ['unified_pricing_rule'],
      metrics: ['total_estimated_revenue'],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: ['unified_pricing_rule'],
      orderBy: [],
      limit: '',
      offset: ''
    };
    this.loadMainPieChart(mainPieChartReq);
  }

  getGrpBys() {
    // let grpBys = this.filtersApplied['groupby'].map(e => e.key);
    let grpBys = [];
    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          grpBys.concat(
            this.filtersApplied['filters']['dimensions']
              .filter(f => f.values.length)
              .map(m => m.key)
          )
        )
      );
    }
    grpBys.push('source');
    return grpBys;
  }

  loadMainPieChart(params) {
    params['orderBy'] = [{ key: 'total_estimated_revenue', opcode: 'desc' }];

    this.mainPieChartJson = {
      chartTypes: [{ key: 'pie', label: 'Pie Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Revenue Distribution Across Unified Pricing Rules'
        },
        legend: {
          display: false
        },
        elements: {
          arc: {
            borderWidth: 0
          }
        },
        plugins: {
          datalabels: {
            formatter: (value, ctx) => {
              let sum = 0;
              const dataArr = ctx.chart.data.datasets[0].data;
              dataArr.map(data => {
                sum += data;
              });
              const percentage = (value * 100) / sum;
              if (percentage > 10) {
                return `${percentage.toFixed(2)} %`;
              } else {
                return '';
              }
            },
            color: 'black'
          }
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };
    this.mainPieChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];

          let sum = 0;
          const dataArr = data.datasets[0].data;
          dataArr.map(data => {
            sum += data;
          });
          const percentage = (currentValue * 100) / sum;
          const arrData = [`${data.labels[tooltipItem.index]}:`];
          this.dataStr =
            '    ' +
            `${this.formatNumPipe.transform(
              currentValue,
              '$',
              []
            )} - ${percentage.toFixed(2)} %`;
          return arrData;
        },
        afterBody: (tooltipItem, data) => {
          if (this.dataStr != '') {
            return [this.dataStr];
          }
        }
      }
    };
    this.showMainPieChart = false;
    this.dataFetchServ.getUPRData(params).subscribe(data => {
      const chartData = data['data'];
      this.noDataPieChart = false;
      if (!chartData.length) {
        this.noDataPieChart = true;
        return;
      }
      const unified_pricing_rule = Array.from(
        new Set(chartData.map(s => s['unified_pricing_rule']))
      );
      const colors = this.libServ.dynamicColors(unified_pricing_rule.length);
      this.mainPieChartJson['chartData']['labels'] = unified_pricing_rule;
      this.mainPieChartJson['chartData']['datasets'][0]['data'] = Array.from(
        new Set(chartData.map(s => s['total_estimated_revenue']))
      );
      this.mainPieChartJson['chartData']['datasets'][0][
        'backgroundColor'
      ] = colors;
      this.showMainPieChart = true;
    });
  }

  loadMainLineChart(params) {
    const colors = this.libServ.dynamicColors(2);
    this.mainLineChartJson = {
      chartTypes: [{ key: 'bar', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'Total eCPM',
            type: 'line',
            yAxisID: 'y-axis-0',

            borderColor: colors[0],
            fill: false,
            backgroundColor: colors[0],
            data: []
          },
          {
            label: 'Total Revenue',
            type: 'bar',
            yAxisID: 'y-axis-1',

            borderColor: colors[1],
            fill: false,
            backgroundColor: colors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Total Revenue vs Total eCPM'
        },
        legend: {
          display: true,
          // onClick: e => e.stopPropagation()
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: 'Total eCPM ($)'
              },
              position: 'right',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', []);
                }
              }
              // scaleFontColor: "rgba(151,137,200,0.8)"
            },
            {
              id: 'y-axis-1',
              scaleLabel: {
                display: true,
                labelString: 'Total Revenue ($)'
              },
              position: 'left',
              name: '2',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '$', []);
                }
              }
              // scaleFontColor: "rgba(151,187,205,0.8)"
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, '$', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    this.showMainLineChart = false;
    this.dataFetchServ.getUPRData(params).subscribe(data => {
      const chartData = data['data'];
      this.noDataMainLineChart = false;
      if (!chartData.length) {
        this.noDataMainLineChart = true;
        return;
      }
      const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));

      this.mainLineChartJson['chartData']['labels'] = datesArr.map(d =>
        moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
      );

      const revArr = [];
      const ecpmArr = [];
      datesArr.forEach(time_key => {
        chartData.forEach(r => {
          if (r['time_key'] === time_key) {
            revArr.push(r['total_estimated_revenue']);
            ecpmArr.push(r['total_ecpm']);
          }
        });
      });
      this.mainLineChartJson['chartData']['datasets'][0]['data'] = ecpmArr;
      this.mainLineChartJson['chartData']['datasets'][1]['data'] = revArr;
      this.showMainLineChart = true;
    });
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['name']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    } else if (type === 'ppt') {
      const chartElements = document.getElementsByTagName('ym-charts');
      const slides = [];
      [].forEach.call(chartElements, (el, index) => {
        const c = el.getElementsByTagName('canvas')[0];

        slides.push({
          title: `${this.appConfig['name']}`,
          base64data: this.libServ.canvasToImage('white', c),
          width: 8,
          height: 3
        });

        if (index === chartElements.length - 1) {
          const pptConfig = {
            filename: 'Charts',
            logo: {
              visible: true,
              path: '/assets/images/ym.png',
              width: 0.53125,
              height: 0.458333
            },
            slides: slides
          };
          this.pptExport.exportPPT(pptConfig);
        }
      });
    }
    return false;
  }

  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }

  sendEmail() {
    const imageConfig = {
      imgName: `${this.appConfig['name']}.png`,
      type: 'image/png'
    };

    this.htmltoimage
      .canvasImage(document.getElementById('capture'), imageConfig)
      .then(canvas => {
        this.dialogService.open(SendEmailComponent, {
          header: 'Email Screenshot',
          contentStyle: { 'max-height': '80vh', width: '30vw', overflow: 'auto' },
          data: canvas.toDataURL('image/png')
        });
      });
  }
}
