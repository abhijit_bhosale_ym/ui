import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UPRDailyDataPopupComponent } from './upr-daily-data-popup.component';

describe('UPRDailyDataPopupComponent', () => {
  let component: UPRDailyDataPopupComponent;
  let fixture: ComponentFixture<UPRDailyDataPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UPRDailyDataPopupComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UPRDailyDataPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
