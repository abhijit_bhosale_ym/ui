import { Component, OnInit, OnDestroy } from '@angular/core';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { CommonLibService, DataShareService } from 'src/app/_services';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';
import { FetchUnfilledApiDataService } from './fetch-vast-api-data.service';
import { VastDataPopupComponent } from './vast-popup/vast-daily-data-popup.component';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { groupBy, filter } from 'rxjs/operators';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { Subscription } from 'rxjs';
import { SendEmailComponent } from '../send-email/send-email.component';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
// import { element } from '@angular/core/src/render3';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-vast-app',
  templateUrl: './vast-app.component.html',
  styleUrls: ['./vast-app.component.scss']
})
export class VastAppComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;
  timeZone = environment.timeZone;

  appConfig: object = {};
  lastUpdatedOn: Date;

  aggTableData: TreeNode[];
  dimColDef: any[];
  metricColDef: any[];
  noTableData = false;
  aggTableColumnDef: any[];
  aggTableJson: Object;
  displayAggTable = false;
  noDataMainLineChart = false;
  showMainLineChart = false;
  showMainPieChart = false;
  mainLineChartJson: object;
  mainPieChartJson: object;
  appliedFilters: object = {};
  noDataPieChart = false;
  filtersApplied: object = {};
  exportRequest: ExportRequest = <ExportRequest>{};
  nextUpdated: Date;
  dataUpdatedThrough: Date;
  tableReq: object;
  timeout: any;
  vastErrors =
    'error_100_count,error_101_count,error_102_count,error_200_count,error_201_count,error_202_count,error_203_count,error_300_count,error_301_count,error_302_count,error_303_count,error_400_count,error_401_count,error_402_count,error_403_count,error_405_count,error_500_count,error_501_count,error_502_count,error_503_count,error_600_count,error_601_count,error_602_count,error_603_count,error_604_count,error_900_count,error_901_count';
  isExportReport = false;
  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: FetchUnfilledApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private formatNumPipe: FormatNumPipe,
    private exportService: ExportdataService,
    private pptExport: ExportPptService,
    private htmltoimage: HtmltoimageService,
    private confirmationService: ConfirmationService,
  ) { }

  ngOnInit() {
    this.metricColDef = [];

    this.dimColDef = [
      {
        field: 'advertiser',
        displayName: 'Advertiser',
        format: '',
        width: '250',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'creative',
        displayName: 'Creative',
        format: '',
        width: '250',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      }
    ];

    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '300px',
      totalRecords: 1000,

      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        console.log('appconfig .....', this.appConfig);
        this.isExportReport = this.appConfig['permissions'].some(
          o => o.name === 'vast-export-reports'
        );
        this._titleService.setTitle(this.appConfig['displayName']);
        let startDate;
        if (
          this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
          'defaultDate'
          ][0]['startOf']
        ) {
          startDate = moment()
            .subtract(
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['value'],
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['period']
            )
            .startOf(
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['period']
            );
        } else {
          startDate = moment().subtract(
            this.appConfig['filter']['filterConfig']['filters'][
            'datePeriod'
            ][0]['defaultDate'][0]['value'],
            this.appConfig['filter']['filterConfig']['filters'][
            'datePeriod'
            ][0]['defaultDate'][0]['period']
          );
        }
        const endDate = moment().subtract(
          this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
          'defaultDate'
          ][1]['value'],
          this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
          'defaultDate'
          ][1]['period']
        );

        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: startDate.format('YYYYMMDD'),
            time_key2: endDate.format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: this.appConfig['filter']['filterConfig']['groupBy']
        };
        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        this.initialLoading();
      }
    });
  }

  initialLoading() {
    this.dataFetchServ
      .getLastUpdatedData(this.appConfig['id'])
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        this.lastUpdatedOn = moment(data[0]['updated_at']).toDate();
        this.nextUpdated = moment(data[0]['next_run_at']).toDate();
        this.dataUpdatedThrough = moment(data[0]['source_updated_through'], 'YYYYMMDD').toDate();
      });

    console.log('in initial loading');
    const mainLineChartReq = {
      dimensions: ['accounting_key'],
      metrics: ['error_100_count',
        'error_101_count',
        'error_102_count',
        'error_200_count',
        'error_201_count',
        'error_202_count',
        'error_203_count',
        'error_300_count',
        'error_301_count',
        'error_302_count',
        'error_303_count',
        'error_400_count',
        'error_401_count',
        'error_402_count',
        'error_403_count',
        'error_405_count',
        'error_500_count',
        'error_501_count',
        'error_502_count',
        'error_503_count',
        'error_600_count',
        'error_601_count',
        'error_602_count',
        'error_603_count',
        'error_604_count',
        'error_900_count',
        'error_901_count'],
      derived_metrics: ['total_vast_rate'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.filtersApplied['filters'],
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: ['advertiser', 'creative'],
      orderBy: [{ key: 'accounting_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };

    this.loadMainLineChart(mainLineChartReq);

    const mainPieChartReq = {
      dimensions: [],
      metrics: [
        'error_100_count',
        'error_101_count',
        'error_102_count',
        'error_200_count',
        'error_201_count',
        'error_202_count',
        'error_203_count',
        'error_300_count',
        'error_301_count',
        'error_302_count',
        'error_303_count',
        'error_400_count',
        'error_401_count',
        'error_402_count',
        'error_403_count',
        'error_405_count',
        'error_500_count',
        'error_501_count',
        'error_502_count',
        'error_503_count',
        'error_600_count',
        'error_601_count',
        'error_602_count',
        'error_603_count',
        'error_604_count',
        'error_900_count',
        'error_901_count'
      ],
      derived_metrics: [],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.filtersApplied['filters'],
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: ['advertiser'],
      orderBy: [],
      limit: '',
      offset: ''
    };

    this.loadMainPieChart(mainPieChartReq);
    this.tableReq = {
      dimensions: [this.filtersApplied['groupby'][0]['key'], 'accounting_key'],
      metrics: ['error_100_count',
        'error_101_count',
        'error_102_count',
        'error_200_count',
        'error_201_count',
        'error_202_count',
        'error_203_count',
        'error_300_count',
        'error_301_count',
        'error_302_count',
        'error_303_count',
        'error_400_count',
        'error_401_count',
        'error_402_count',
        'error_403_count',
        'error_405_count',
        'error_500_count',
        'error_501_count',
        'error_502_count',
        'error_503_count',
        'error_600_count',
        'error_601_count',
        'error_602_count',
        'error_603_count',
        'error_604_count',
        'error_900_count',
        'error_901_count'],
      derived_metrics: ['total_vast_rate'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys(), // grpBys,
      orderBy: [{ key: 'accounting_key', opcode: 'desc' }],
      limit: '',
      offset: ''
    };
    this.loadTableData(this.tableReq);
  }

  loadTableData(params) {
    const finalColDef = [];
    this.metricColDef = [];
    const grpBys = this.filtersApplied['groupby'].map(e => e.key);
    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    }

    grpBys.forEach((row: object) => {
      const displayName = row
        .toString()
        .replace(/(^|_)(\w)/g, function ($0, $1, $2) {
          return ($1 && ' ') + $2.toUpperCase();
        });

      this.metricColDef.push({
        field: row,
        displayName: displayName,
        format: '',
        width: '250',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: '-'
      });
    });
    this.aggTableJson['loading'] = true;

    this.dataFetchServ.getVastData(params).subscribe(data => {
      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      } else {
        this.noTableData = false;
      }

      const data1 = data as [];
      const arr1 = [];
      let resultArray = [];

      resultArray = Array.from(
        new Set(data1['data'].map(s => s['accounting_key']))
      );

      resultArray.forEach((col: Object) => {
        const d1 = moment(JSON.stringify(col), 'YYYYMMDD').format('MMM-YYYY');

        this.metricColDef.push({
          field: col.toString(),
          displayName: d1,
          format: 'number',
          width: '120',
          exportConfig: {
            format: 'number',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          },
          footerTotal: '0'
        });
      });

      this.aggTableColumnDef = this.metricColDef;
      this.aggTableJson['columns'] = this.aggTableColumnDef.slice(
        grpBys.length
      );

      this.getFooters(grpBys);

      this.aggTableJson['selectedColumns'] = this.aggTableColumnDef.slice(
        grpBys.length
      );
      this.aggTableJson['frozenCols'] = [
        ...this.aggTableColumnDef.slice(0, grpBys.length)
      ];
      this.aggTableJson['frozenWidth'] =
        this.aggTableColumnDef
          .slice(0, grpBys.length)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px';

      this.reloadAggTable();

      const dims = Array.from(new Set(data1['data'].map(s => s[grpBys[0]])));

      const sortedCols = this.aggTableColumnDef.slice(grpBys.length);

      const arr = [];

      dims.forEach(col1 => {
        let obj = {};

        if (grpBys.length == 1) {
          obj = {
            data: {}
          };
        } else {
          obj = {
            data: {},
            children: [{ data: {} }]
          };
        }

        const str = data1['data'].filter(x => x[grpBys[0]] == col1);

        grpBys.forEach((grp, index) => {
          if (index === 0) {
            obj['data'][grp] = str[0][grp];
          } else {
            obj['data'][grp] = 'All';
          }
        });

        str.forEach((s1, i) => {
          sortedCols.forEach((sortedCols1, index1) => {
            if (parseInt(sortedCols1.field) === s1['accounting_key']) {
              obj['data'][parseInt(sortedCols1.field)] = s1['total_vast_rate'];
            }
          });
        });

        arr.push(obj);
      });

      this.aggTableData = <TreeNode[]>arr;
      this.aggTableJson['totalRecords'] = arr.length;;
      this.aggTableJson['loading'] = false;
    });
  }

  getFooters(grpBys) {
    const tableReq1 = {
      dimensions: ['accounting_key'],
      metrics: ['error_100_count',
        'error_101_count',
        'error_102_count',
        'error_200_count',
        'error_201_count',
        'error_202_count',
        'error_203_count',
        'error_300_count',
        'error_301_count',
        'error_302_count',
        'error_303_count',
        'error_400_count',
        'error_401_count',
        'error_402_count',
        'error_403_count',
        'error_405_count',
        'error_500_count',
        'error_501_count',
        'error_502_count',
        'error_503_count',
        'error_600_count',
        'error_601_count',
        'error_602_count',
        'error_603_count',
        'error_604_count',
        'error_900_count',
        'error_901_count'],
      derived_metrics: ['total_vast_rate'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },
      // gidGroupBy: [
      //   "ad_units",
      //   "app_names",
      //   "creative_size",
      //   "demand_channel",
      //   "device_category",
      //   "inventory_types",
      //   "source"
      // ],
      gidGroupBy: this.getGrpBys(), // grpBys,
      orderBy: [{ key: 'accounting_key', opcode: 'desc' }],
      limit: '',
      offset: ''
    };

    this.dataFetchServ.getVastData(tableReq1).subscribe(data11 => {
      if (data11['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data11['status_msg']);
        return;
      }

      const data1 = data11['data'] as [];
      data1.forEach(d => {
        this.aggTableJson['selectedColumns'].forEach(c => {
          if (parseInt(c['field']) === d['accounting_key']) {
            c['footerTotal'] = d['total_vast_rate'];
          }
        });
      });
    });
  }

  reloadAggTable() {
    this.displayAggTable = false;
    setTimeout(() => {
      this.displayAggTable = true;
    }, 0);
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['name']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    } else if (type === 'ppt') {
      const chartElements = document.getElementsByTagName('ym-charts');
      const slides = [];
      [].forEach.call(chartElements, (el, index) => {
        const c = el.getElementsByTagName('canvas')[0];

        slides.push({
          title: `${this.appConfig['name']}`,
          base64data: this.libServ.canvasToImage('white', c),
          width: 8,
          height: 3
        });

        if (index === chartElements.length - 1) {
          const pptConfig = {
            filename: 'Charts',
            logo: {
              visible: true,
              path: '/assets/images/ym.png',
              width: 0.53125,
              height: 0.458333
            },
            slides: slides
          };
          this.pptExport.exportPPT(pptConfig);
        }
      });
    }
    return false;
  }

  loadMainLineChart(params) {
    const colors = this.libServ.dynamicColors(2);
    this.mainLineChartJson = {
      chartTypes: [{ key: 'line', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'VAST Errors',
            type: 'line',
            borderColor: colors[1],
            fill: false,
            backgroundColor: colors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'VAST Errors'
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Months'
              }
            }
          ],
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: 'VAST Errors'
              },
              position: 'left',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, 'number', []);
                }
              }
            }
          ]
        },
        tooltips: {
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    this.showMainLineChart = false;
    this.dataFetchServ.getVastData(params).subscribe(data => {
      const chartData = data['data'] as [];
      if (typeof chartData === 'undefined' || !chartData.length) {
        this.noDataMainLineChart = true;
      } else {
        const datesArr = Array.from(
          new Set(chartData.map(r => r['accounting_key']))
        );

        this.mainLineChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MMM-YYYY')
        );

        const revArr = [];
        datesArr.forEach(time_key => {
          chartData.forEach(r => {
            if (r['accounting_key'] === time_key) {
              revArr.push(r['total_vast_rate']);
            }
          });
        });
        this.mainLineChartJson['chartData']['datasets'][0]['data'] = revArr;
        this.showMainLineChart = true;
        this.noDataMainLineChart = false;
      }
    });
  }

  loadMainPieChart(params) {
    this.mainPieChartJson = {
      chartTypes: [{ key: 'pie', label: 'Pie Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'VAST Errors'
        },
        legend: {
          display: false
        },
        elements: {
          arc: {
            borderWidth: 0
          }
        },
        plugins: {
          datalabels: {
            formatter: (value, ctx) => {
              let sum = 0;
              const dataArr = ctx.chart.data.datasets[0].data;
              dataArr.map(data => {
                sum += data;
              });
              const percentage = (value * 100) / sum;
              if (percentage > 10) {
                return `${this.formatNumPipe.transform(value, 'number', [])} `;
              } else {
                return '';
              }
            },
            color: 'black'
          }
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };
    this.mainPieChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];

          return `${
            data.labels[tooltipItem.index]
            } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
        }
      }
    };

    this.showMainPieChart = false;
    this.dataFetchServ.getVastData(params).subscribe(data => {
      const chartData = data['data'];
      if (typeof chartData === 'undefined' || !chartData.length) {
        this.noDataPieChart = true;
      } else {
        const processedArray = [];

        chartData.forEach(element => {
          processedArray.push(element);
        });

        delete chartData[0]['cards_value'];

        let sortableArray = [];
        for (const item in processedArray[0]) {
          sortableArray.push([item, processedArray[0][item]]);
        }

        sortableArray = sortableArray
          .sort(function (a, b) {
            return b[1] - a[1];
          })
          .slice(0, 5);

        const sources = Array.from(new Set(sortableArray.map(s => s[0])));
        const colors = this.libServ.dynamicColors(sources.length);
        this.mainPieChartJson['chartData']['labels'] = sources;
        this.mainPieChartJson['chartData']['datasets'][0]['data'] = Array.from(
          new Set(sortableArray.map(s => s[1]))
        );
        this.mainPieChartJson['chartData']['datasets'][0][
          'backgroundColor'
        ] = colors;
        this.showMainPieChart = true;
        this.noDataPieChart = false;
      }
    });
  }

  chartSelected(data: Event) {
    console.log('Chart Selected', data);
  }

  onTableDrill(e: Event) {
    console.log('Drilled', e);
    if (!e['node']['childLoaded']) {
      this.aggTableJson['loading'] = true;

      if (Object.keys(this.appliedFilters).length === 0) {
        this.appliedFilters['filters'] = { dimensions: [] };
      }

      const tableReq = {
        dimensions: ['accounting_key'], // [this.filtersApplied['groupby'][0]['key'],'accounting_key'],
        metrics: ['error_100_count',
          'error_101_count',
          'error_102_count',
          'error_200_count',
          'error_201_count',
          'error_202_count',
          'error_203_count',
          'error_300_count',
          'error_301_count',
          'error_302_count',
          'error_303_count',
          'error_400_count',
          'error_401_count',
          'error_402_count',
          'error_403_count',
          'error_405_count',
          'error_500_count',
          'error_501_count',
          'error_502_count',
          'error_503_count',
          'error_600_count',
          'error_601_count',
          'error_602_count',
          'error_603_count',
          'error_604_count',
          'error_900_count',
          'error_901_count'],
        derived_metrics: ['total_vast_rate'],
        timeKeyFilter: this.filtersApplied['timeKeyFilter'],
        filters: this.libServ.deepCopy(this.filtersApplied['filters']), // this.appliedFilters["filters"],
        groupByTimeKey: {
          key: ['accounting_key', 'time_key'],
          interval: 'daily'
        },
        // gidGroupBy: [
        //   "app_names",
        //   "creative_size",
        //   "demand_channel",
        //   "device_category",
        //   "source",
        //   "ad_units",
        //   "inventory_types"
        // ],
        gidGroupBy: this.getGrpBys(), // this.filtersApplied['groupby'].map(e => e.key),
        orderBy: [{ key: 'accounting_key', opcode: 'asc' }],
        limit: '',
        offset: ''
      };

      const grpBys = this.filtersApplied['groupby'].map(e => e.key);

      for (const g of grpBys) {
        if (e['node']['data'][g] !== 'All') {
          tableReq['dimensions'].push(g);
          if (!tableReq['gidGroupBy'].includes(g)) {
            tableReq['gidGroupBy'].push(g);
          }
          if (
            tableReq['filters']['dimensions'].findIndex(e => e.key === g) === -1
          ) {
            tableReq['filters']['dimensions'].push({
              key: g,
              values: [e['node']['data'][g]]
            });
          } else {
            if (
              tableReq['filters']['dimensions']
                .find(e => e.key === g)
              ['values'].findIndex(v => v === e['node']['data'][g]) === -1
            ) {
              tableReq['filters']['dimensions'].push(e['node']['data'][g]);
            }
          }
        } else {
          tableReq['dimensions'].push(g);
          if (!tableReq['gidGroupBy'].includes(g)) {
            tableReq['gidGroupBy'].push(g);
          }
          break;
        }
      }

      this.dataFetchServ.getVastData(tableReq).subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        const childData = data['data'] as [];
        const arr = [];

        const sortedCols = this.aggTableColumnDef.slice(grpBys.length);

        const dims = Array.from(new Set(childData.map(s => s[grpBys[1]])));

        dims.forEach(col1 => {
          const obj = {
            data: {},
            children: [{ data: {} }]
          };

          const str = childData.filter(x => x[grpBys[1]] == col1);
          str.forEach((s1, i) => {
            grpBys.forEach(g => {
              if (str[i][g] === undefined) {
                obj['data'][g] = 'All';
              } else {
                obj['data'][g] = str[i][g];
              }
            });

            sortedCols.forEach((sortedCols1, index1) => {
              if (parseInt(sortedCols1.field) === s1['accounting_key']) {
                obj['data'][parseInt(sortedCols1.field)] = s1['total_vast_rate'];
              }
            });
          });

          arr.push(obj);
        });

        arr.forEach((row: object) => {
          if (row['data'][grpBys[grpBys.length - 1]] !== 'All') {
            delete row['children'];
          }
        });

        this.aggTableJson['loading'] = false;
        e['node']['children'] = <TreeNode[]>arr;
        this.aggTableData = [...this.aggTableData];
        e['node']['childLoaded'] = true;
      });
    }
  }

  getGrpBys() {
    let grpBys = this.filtersApplied['groupby'].map(e => e.key);
    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          grpBys.concat(
            this.filtersApplied['filters']['dimensions']
              .filter(f => f.values.length)
              .map(m => m.key)
          )
        )
      );
    }
    return grpBys;
  }

  onLazyLoadAggTable(e: Event) {
    console.log('Lazy Agg', e);
  }

  resetPagination(tt1) {
    if (typeof tt1 !== 'undefined') {
      tt1.reset();
    }
  }

  openPopup(row, field) {
    const grpBys = [];
    const filtersApplied = this.libServ.deepCopy(this.filtersApplied);
    let str = '';

    for (const grp of this.filtersApplied['groupby']) {
      if (grp['key'] === field) {
        grpBys.push(grp);
        filtersApplied['filters']['dimensions'].push({
          key: grp['key'],
          values: [row[grp['key']]]
        });
        const keyFormat = grp['key'].replace(/(^|_)(\w)/g, function (
          $0,
          $1,
          $2
        ) {
          return ($1 && ' ') + $2.toUpperCase();
        });
        if (str === '') {
          str = str + ' ' + keyFormat + ' : ' + row[grp['key']];
        } else {
          str = str + ' > ' + keyFormat + ' : ' + row[grp['key']];
        }

        break;
      } else {
        grpBys.push(grp);
        filtersApplied['filters']['dimensions'].push({
          key: grp['key'],
          values: [row[grp['key']]]
        });
        const keyFormat = grp['key'].replace(/(^|_)(\w)/g, function (
          $0,
          $1,
          $2
        ) {
          return ($1 && ' ') + $2.toUpperCase();
        });
        if (str === '') {
          str = str + ' ' + keyFormat + ' : ' + row[grp['key']];
        } else {
          str = str + ' > ' + keyFormat + ' : ' + row[grp['key']];
        }
      }
    }

    filtersApplied['groupby'] = grpBys;
    const data = {
      filters: filtersApplied
    };

    const ref = this.dialogService.open(VastDataPopupComponent, {
      header: str + ' VAST Errors ',
      contentStyle: { 'max-height': '80vh', width: '50vw', overflow: 'auto' },
      width:'50vw',
      data: data
    });
    ref.onClose.subscribe((data1: string) => { });
  }

  async exportTable(table, fileFormat) {

    if (this.aggTableData.length == 0) {
      this.confirmationService.confirm({
        message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
        header: 'Information',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {

        },
        reject: () => {
          // this.msgs = [{severity:'info', summary:'Rejected', detail:'You have rejected'}];
        }
      });
    } else {
      if (this.exportRequest['sendEmail'].length > 0) {
        this.toastService.displayToast({
          severity: 'info',
          summary: 'Export Report',
          detail:
            'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
          life: 10000
        });
        let grpBys = this.filtersApplied['groupby'].map(e => e.key);
        if (
          this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
          this.filtersApplied['filters']['dimensions'].length === 0
        ) {
          this.filtersApplied['filters']['dimensions'] = [];
        } else {
          grpBys = Array.from(
            new Set(
              this.filtersApplied['filters']['dimensions']
                .filter(f => f.values.length)
                .map(m => m.key)
                .concat(grpBys)
            )
          );
        }

        const colDef = this.libServ.deepCopy(this.aggTableColumnDef);
        // grpBys.push('time_key');
        const tableReq = {
          dimensions: [this.filtersApplied['groupby'][0]['key'], 'accounting_key'],
          metrics: ['error_100_count',
            'error_101_count',
            'error_102_count',
            'error_200_count',
            'error_201_count',
            'error_202_count',
            'error_203_count',
            'error_300_count',
            'error_301_count',
            'error_302_count',
            'error_303_count',
            'error_400_count',
            'error_401_count',
            'error_402_count',
            'error_403_count',
            'error_405_count',
            'error_500_count',
            'error_501_count',
            'error_502_count',
            'error_503_count',
            'error_600_count',
            'error_601_count',
            'error_602_count',
            'error_603_count',
            'error_604_count',
            'error_900_count',
            'error_901_count'],
          derived_metrics: ['total_vast_rate'],
          timeKeyFilter: this.filtersApplied['timeKeyFilter'],
          filters: this.filtersApplied['filters'],
          groupByTimeKey: {
            key: ['accounting_key', 'time_key'],
            interval: 'daily'
          },

          gidGroupBy: this.getGrpBys(),
          orderBy: [{ key: 'accounting_key', opcode: 'asc' }], // [{ key: this.getGrpBys()[0], opcode: "asc" }],
          limit: '',
          offset: ''
          //  };
        };

        let temp = 0;
        const data1 = [];
        const sheetDetailsarray = [];
        while (temp < grpBys.length) {
          const grpbyExport = [];
          for (let i = 0; i <= temp; i++) {
            grpbyExport.push(grpBys[i]);
            tableReq['orderBy'] = [{ key: 'accounting_key', opcode: 'desc' }];
            tableReq['dimensions'] = grpbyExport;
          }
          // tableReq['isTable'] = false;

          tableReq['dimensions'].push('accounting_key');
          await this.dataFetchServ
            .getVastData(tableReq)
            .toPromise()
            .then(success => {
              if (success['status'] === 0) {
                this.toastService.displayToast({
                  severity: 'error',
                  summary: 'Server Error',
                  detail: 'Please refresh the page'
                });
                console.log(success['status_msg']);
                return;
              }

              let grpByColumnDef = [];
              const colDefOptions = this.aggTableColumnDef;
              let i = 0;
              let sheetName = '';

              while (i < grpbyExport.length) {
                let displayName = '';
                if (grpbyExport[i] === 'advertiser') {
                  displayName = 'Advertiser';
                  if (i === 0) {
                    sheetName = 'Advertiser';
                  } else {
                    sheetName = '+Advertiser';
                  }
                } else if (grpbyExport[i] === 'creative') {
                  displayName = 'Creative';
                  if (i === 0) {
                    sheetName = 'Creative';
                  } else {
                    sheetName = '+Creative';
                  }
                }

                grpByColumnDef.push({
                  field: grpbyExport[i],
                  displayName: displayName,
                  visible: true,
                  width: 150,
                  format: 'string',
                  exportConfig: {
                    format: 'string',
                    styleinfo: {
                      thead: 'default',
                      tdata: 'white'
                    }
                  }
                });

                i++;
              }

              const childData = success['data'] as [];
              const arr = [];

              const sortedCols = this.aggTableColumnDef.slice(grpBys.length);
              grpByColumnDef = grpByColumnDef.concat(sortedCols);
              const dims = Array.from(new Set(childData.map(s => s[grpBys[temp]])));

              dims.forEach(col1 => {
                const str = childData.filter(x => x[grpBys[temp]] == col1);

                let obj = {};

                let c = 0;
                str.forEach((s1, i) => {
                  grpBys.forEach(g => {
                    if (str[i][g] === undefined) {
                    } else {
                      obj[g] = str[i][g];
                    }
                  });

                  sortedCols.forEach((sortedCols1, index1) => {
                    if (parseInt(sortedCols1.field) === s1['accounting_key']) {
                      obj[parseInt(sortedCols1.field)] = s1['total_vast_rate'];
                      c++;
                    }
                  });

                  if (sortedCols.length === c || str.length === c) {
                    arr.push(obj);
                    c = 0;
                    obj = {};
                  }
                });
              });

              grpByColumnDef.splice(
                grpByColumnDef.findIndex(
                  item => item['field'] === 'accounting_key'
                ),
                1
              );
              const totalObj = {};
              grpByColumnDef.forEach(ele => {
                if ('footerTotal' in ele) {
                  totalObj[ele.field] = ele.footerTotal;
                }
              });
              totalObj[grpByColumnDef[0].field] = 'Total';
              arr.push(totalObj);
              const sheetDetails = {};
              sheetDetails['columnDef'] = this.libServ.deepCopy(grpByColumnDef);
              sheetDetails['data'] = arr;
              sheetDetails['sheetName'] = sheetName;
              sheetDetails['isRequest'] = false;
              sheetDetails['request'] = {
                url: '',
                method: '',
                param: {
                  timeKeyFilter: this.libServ.deepCopy(
                    this.filtersApplied['timeKeyFilter']
                  )
                }
              };
              sheetDetails['disclaimer'] = [
                {
                  position: 'bottom',
                  label: 'Note: Data present in the table may vary over a period of time.',
                  color: '#000000'
                }
              ];
              sheetDetails['totalFooter'] = {
                available: true,
                custom: true
              };
              sheetDetails['tableTitle'] = {
                available: false,
                label: 'Aggregated Data By ' + sheetName.replace('+', '')
              };
              sheetDetails['image'] = [
                {
                  available: true,
                  path: environment.exportConfig.exportLogo,
                  position: 'top'
                }
              ];

              sheetDetailsarray.push(sheetDetails);
            });

          temp++;
        }

        this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
        this.exportRequest['fileName'] =
          'VAST Errors Data ' +
          moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
        this.exportRequest['exportFormat'] = fileFormat;
        this.exportRequest['exportConfig'] = environment.exportConfig;
        this.dataFetchServ
          .getExportReportData(this.exportRequest)
          .subscribe(response => {
            console.log(response);
          });
      } else {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Export Report',
          detail:
            'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email.Thanks!',
          life: 10000
        });
      }
    }
  }

  isHiddenColumn(col: Object) {
    return (
      this.aggTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }

  /* --------------------- On Filter Apply Button Clicked --------------------- */

  onFiltersApplied(filterData: object) {
    console.log('filterData', filterData);
    this.filtersApplied['filters']['dimensions'] = [];

    // tslint:disable-next-line: forin
    for (const k in filterData['filter']['dimensions']) {
      if (filterData['filter']['dimensions'][k].length !== 0) {
        this.filtersApplied['filters']['dimensions'].push({
          key: k,
          values: filterData['filter']['dimensions'][k]
        });
      }
    }
    this.appliedFilters = this.libServ.deepCopy(this.filtersApplied);

    this.filtersApplied['groupby'] = filterData['groupby'];
    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];

    const mainLineChartReq = {
      dimensions: ['accounting_key'],
      metrics: ['error_100_count',
        'error_101_count',
        'error_102_count',
        'error_200_count',
        'error_201_count',
        'error_202_count',
        'error_203_count',
        'error_300_count',
        'error_301_count',
        'error_302_count',
        'error_303_count',
        'error_400_count',
        'error_401_count',
        'error_402_count',
        'error_403_count',
        'error_405_count',
        'error_500_count',
        'error_501_count',
        'error_502_count',
        'error_503_count',
        'error_600_count',
        'error_601_count',
        'error_602_count',
        'error_603_count',
        'error_604_count',
        'error_900_count',
        'error_901_count'],
      derived_metrics: ['total_vast_rate'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.filtersApplied['filters'],
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys(), // this.filtersApplied["groupby"].map(e => e.key),

      orderBy: [{ key: 'accounting_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };

    const mainPieChartReq = {
      dimensions: [],
      metrics: [
        'error_100_count',
        'error_101_count',
        'error_102_count',
        'error_200_count',
        'error_201_count',
        'error_202_count',
        'error_203_count',
        'error_300_count',
        'error_301_count',
        'error_302_count',
        'error_303_count',
        'error_400_count',
        'error_401_count',
        'error_402_count',
        'error_403_count',
        'error_405_count',
        'error_500_count',
        'error_501_count',
        'error_502_count',
        'error_503_count',
        'error_600_count',
        'error_601_count',
        'error_602_count',
        'error_603_count',
        'error_604_count',
        'error_900_count',
        'error_901_count'
      ],
      derived_metrics: [],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.filtersApplied['filters'],
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys(),
      orderBy: [],
      limit: '',
      offset: ''
    };

    this.loadMainPieChart(mainPieChartReq);

    this.aggTableColumnDef = [];
    this.aggTableJson['columns'] = [];
    this.metricColDef = [];
    this.loadMainLineChart(mainLineChartReq);
    this.tableReq = {
      dimensions: [this.filtersApplied['groupby'][0]['key'], 'accounting_key'],
      metrics: ['error_100_count',
        'error_101_count',
        'error_102_count',
        'error_200_count',
        'error_201_count',
        'error_202_count',
        'error_203_count',
        'error_300_count',
        'error_301_count',
        'error_302_count',
        'error_303_count',
        'error_400_count',
        'error_401_count',
        'error_402_count',
        'error_403_count',
        'error_405_count',
        'error_500_count',
        'error_501_count',
        'error_502_count',
        'error_503_count',
        'error_600_count',
        'error_601_count',
        'error_602_count',
        'error_603_count',
        'error_604_count',
        'error_900_count',
        'error_901_count'],
      derived_metrics: ['total_vast_rate'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys(), // grpBys,
      orderBy: [{ key: 'accounting_key', opcode: 'desc' }],
      limit: '',
      offset: ''
    };
    this.loadTableData(this.tableReq);
  }

  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }

  sendEmail() {
    const imageConfig = {
      imgName: `${this.appConfig['name']}.png`,
      type: 'image/png'
    };

    this.htmltoimage
      .canvasImage(document.getElementById('capture'), imageConfig)
      .then(canvas => {
        this.dialogService.open(SendEmailComponent, {
          header: 'Email Screenshot',
          contentStyle: { 'max-height': '80vh', width: '30vw', overflow: 'auto' },
          data: canvas.toDataURL('image/png')
        });
      });
  }
  onGlobalSearchChanged(searchValue) {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    this.timeout = setTimeout(() => {
      this.tableReq['filters']['globalSearch'] = {
        dimensions: [],
        value: searchValue
      };
      this.tableReq['filters']['globalSearch']['dimensions'].push(
        this.filtersApplied['groupby'][0]['key'], "total_vast_rate"
      );
      this.loadTableData(this.tableReq);
    }, 3000);
  }
}
