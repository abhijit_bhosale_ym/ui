import { Routes } from '@angular/router';
import { VastAppComponent } from './vast-app.component';

export const VastAppRoutes: Routes = [
  {
    path: '',
    component: VastAppComponent
  }
];
