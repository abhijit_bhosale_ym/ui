import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FetchUnfilledApiDataService {
  private BASE_URL: string = environment.baseUrl;

  constructor(private http: HttpClient) {}

  getVastData(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/vast-errors/getData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
  }
  getExportReportData(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/common/exportData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
  }

  getLastUpdatedData(appId) {
    const url = `${this.BASE_URL}/frankly/v1/common/getLastUpdatedData/${appId}`;
    return this.http.get(url);
  }
}
