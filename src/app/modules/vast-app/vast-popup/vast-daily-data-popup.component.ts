import { Component, OnInit } from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { FetchUnfilledApiDataService } from '../fetch-vast-api-data.service';
import * as moment from 'moment';
import { TreeNode } from 'primeng/api';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';

@Component({
  selector: 'ym-vast-daily-data-popup',
  templateUrl: './vast-daily-data-popup.component.html',
  styleUrls: ['./vast-daily-data-popup.component.scss']
})
export class VastDataPopupComponent implements OnInit {
  lineChartJson: object;
  showLineChart = false;
  filtersApplied: object;
  showMainPieChart = false;
  mainPieChartJson: object;
  tabChangedFlag = false;

  tableDataReq: object;
  tableData: TreeNode[];
  tableColumnDef: any[];
  dimColDef: any[];
  tableJson: Object;
  noDataPieChart = false;

  constructor(
    private libServ: CommonLibService,
    private formatNumPipe: FormatNumPipe,
    private dataFetchServ: FetchUnfilledApiDataService,
    private config: DynamicDialogConfig,
    private exportService: ExportdataService,
    private toastService: ToastService
  ) {
    this.filtersApplied = this.config.data['filters'];
  }

  ngOnInit() {
    this.loadLineChart();
  }

  loadLineChart() {
    let grpBys = this.filtersApplied['groupby'].map(e => e.key);
    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          this.filtersApplied['filters']['dimensions']
            .filter(f => f.values.length)
            .map(m => m.key)
            .concat(grpBys)
        )
      );
    }

    const params = {
      dimensions: [],
      metrics: [
        'error_100_count',
        'error_101_count',
        'error_102_count',
        'error_200_count',
        'error_201_count',
        'error_202_count',
        'error_203_count',
        'error_300_count',
        'error_301_count',
        'error_302_count',
        'error_303_count',
        'error_400_count',
        'error_401_count',
        'error_402_count',
        'error_403_count',
        'error_405_count',
        'error_500_count',
        'error_501_count',
        'error_502_count',
        'error_503_count',
        'error_600_count',
        'error_601_count',
        'error_602_count',
        'error_603_count',
        'error_604_count',
        'error_900_count',
        'error_901_count'
      ],
      derived_metrics: [],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.filtersApplied['filters'],
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },

      gidGroupBy: grpBys,
      orderBy: [],
      limit: '',
      offset: ''
    };

    this.mainPieChartJson = {
      chartTypes: [{ key: 'pie', label: 'Pie Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Top 5 VAST Errors'
        },
        legend: {
          display: false
        },
        elements: {
          arc: {
            borderWidth: 0
          }
        },
        plugins: {
          datalabels: {
            formatter: (value, ctx) => {
              let sum = 0;
              const dataArr = ctx.chart.data.datasets[0].data;
              dataArr.map(data => {
                sum += data;
              });
              const percentage = (value * 100) / sum;
              if (percentage > 10) {
                return `${this.formatNumPipe.transform(value, 'number', [])} `;
              } else {
                return '';
              }
            },
            color: 'black'
          }
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };
    this.mainPieChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];

          return `${
            data.labels[tooltipItem.index]
            } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
        }
      }
    };

    this.showMainPieChart = false;
    this.dataFetchServ.getVastData(params).subscribe(data => {
      const chartData = data['data'];
      this.noDataPieChart = true;
      const processedArray = [];

      chartData.forEach(element => {
        processedArray.push(element);
      });

      delete chartData[0]['cards_value'];

      let sortableArray = [];
      for (const item in processedArray[0]) {
        sortableArray.push([item, processedArray[0][item]]);
        if (processedArray[0][item] > 0) {
          this.showMainPieChart = true;
          this.noDataPieChart = false;
        }
      }
      sortableArray.sort(function (a, b) {
        return b[1] - a[1];
      });

      sortableArray = sortableArray.slice(0, 5);
      const sources = Array.from(new Set(sortableArray.map(s => s[0])));
      const colors = this.libServ.dynamicColors(sources.length);
      this.mainPieChartJson['chartData']['labels'] = sources;
      this.mainPieChartJson['chartData']['datasets'][0][
        'data'
      ] = sortableArray.map(s => s[1]);
      this.mainPieChartJson['chartData']['datasets'][0][
        'backgroundColor'
      ] = colors;
    });
  }

  tabChanged(e) {
    if (!this.tabChangedFlag) {
      this.loadLineChart();
      this.tabChangedFlag = true;
    }
    // console.log('tab changed', e);
  }

  chartSelected(e) { }
}
