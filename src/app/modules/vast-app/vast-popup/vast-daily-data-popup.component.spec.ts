import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VastDataPopupComponent } from './vast-daily-data-popup.component';

describe('Rev360DailyDataPopupComponent', () => {
  let component: VastDataPopupComponent;
  let fixture: ComponentFixture<VastDataPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VastDataPopupComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VastDataPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
