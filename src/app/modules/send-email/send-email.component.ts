import { Component, OnInit } from '@angular/core';
import { FetchApiDataService } from './fetch-api-data.service';
import { ToastService } from '../../_services/toast-notification/toast.service';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'ym-send-email',
  templateUrl: './send-email.component.html',
  styleUrls: ['./send-email.component.scss']
})
export class SendEmailComponent {
  public contactemail: string[];
  public data: any;
  public isValid = true;
  constructor(
    private dataFetchServ: FetchApiDataService,
    private toastService: ToastService,
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef
  ) {
    this.data = this.config.data;
  }
  chipsValidation() {
    this.isValid = true;
    const regexp = new RegExp(
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
    this.contactemail.forEach(val => {
      if (!regexp.test(val)) {
        this.isValid = false;
      }
    });
  }
  mailScreenshot() {
    if (this.isValid) {
      const contactEmail = this.contactemail.length
        ? this.contactemail.join(',')
        : '';
      const userInfo = {
        email: contactEmail,
        data: this.data
      };
      this.dataFetchServ.sendScreenshot(userInfo).subscribe(
        res => {
          const data = res as {};
          if (data['status']) {
            this.toastService.displayToast({
              severity: 'success',
              summary: 'email sent',
              detail: 'Email sent successfully'
            });
            this.ref.close(null);
          }
        },
        error => {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Something went wrong',
            detail: 'Please refresh page if problem persist contact us'
          });
          this.ref.close(null);
        }
      );
    }
  }

  cancel() {
    this.ref.close(null);
  }
}
