import { Routes } from '@angular/router';
import { CorporateFinanceAppComponent } from './corporate-finance-app.component';

export const CorporateFinanceAppRoutes: Routes = [
  {
    path: '',
    component: CorporateFinanceAppComponent
  }
];
