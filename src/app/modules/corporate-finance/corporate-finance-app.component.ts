import { Component, OnInit, OnDestroy } from '@angular/core';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { CommonLibService } from 'src/app/_services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { FetchApiDataService } from './fetch-api-data.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { Subscription } from 'rxjs';
import { SendEmailComponent } from '../send-email/send-email.component';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { DropdownModule } from 'primeng/dropdown';
import { SelectItemGroup } from 'primeng/api';
// import { Subscription } from 'rxjs';
// import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

import {
  Router,
  ActivatedRoute,
  NavigationEnd,
  RouterEvent
} from '@angular/router';
import {
  filter,
  takeUntil,
  pairwise,
  startWith
} from 'rxjs/operators';
import { Subject } from 'rxjs';

@Component({
  selector: 'ym-corporate-finance-app',
  templateUrl: './corporate-finance-app.component.html',
  styleUrls: ['./corporate-finance-app.component.scss']
})
export class CorporateFinanceAppComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;
  private appConfigObs1: Subscription;
  private appConfigObs2: Subscription;
  appConfig: object = {};
  FormData: object = {};
  registerForm: FormGroup;
  lastUpdatedOn: Date;
  showCards = true;
  aggTableData: TreeNode[];
  noTableData = false;
  dimColDef: any[];
  metricColDef: any[];
  aggTableColumnDef: any[];
  aggTableJson: Object;
  displayAggTable = false;
  toggleView = true;
  filtersApplied: object = {};
  exportRequest: ExportRequest = <ExportRequest>{};
  showhide = true;
  tableReq: object;
  timeout: any;
  isExportReport = false;
  tableDimColDef: any[];
  tableData = [];
  globalSearchValue = "";
  value: any;
  colSearch: any = {};
  public destroyed = new Subject<any>();
  statusList = ['Yes', 'No'];
  statusLists = this.statusList.map(name => {
    return { label: name, value: name };
  });
  tabIndex: any;
  mitigationList = ['SC', 'DNSH'];
  mitigationLists = this.mitigationList.map(name => {
    return { label: name, value: name };
  });

  constructor(
    private formBuilder: FormBuilder,
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private htmltoimage: HtmltoimageService,
    private router: Router,
    private currentActivatedRoute: ActivatedRoute,
    private confirmationService: ConfirmationService,
  ) { }

  ngOnInit() {
    this.tabIndex == 0;
    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        this._titleService.setTitle(this.appConfig['displayName']);
        this.initialLoading();
      }
    });
  }

  initialLoading() {
    this.loadCorporateData();
  }

  loadCorporateData() {
    this.tableDimColDef = [
      {
        field: 'name',
        displayName: 'Corporate Name',
        format: '',
        width: '100',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        },
      },
      {
        field: 'domicle_country',
        displayName: 'Domicle Country',
        format: '',
        width: '120',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'region',
        displayName: 'Region',
        format: '',
        width: '80',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'sector',
        displayName: 'Sectors',
        width: '120',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'date_source',
        displayName: 'Data Source',
        width: '100',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "updated_date",
        displayName: 'Date Updated',
        format: '',
        width: '120',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "ticker",
        displayName: 'Ticker',
        format: '',
        width: '100',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "activity_name",
        displayName: 'Activity Name',
        format: '',
        width: '100',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "absolute_turnover_currency",
        displayName: 'Absolute Turnover ($)',
        format: '$',
        width: '140',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "proportion_turnover_percent",
        displayName: 'Proportion Of Turnover (%)',
        format: 'percentage',
        width: '140',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "covered_by_taxonomy",
        displayName: 'Covered by Taxonomy?',
        format: '',
        width: '190',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },


      {
        field: "covered_by_taxonomy_but_do_not_meet_TSC",
        displayName: 'Covered By Taxonomy But Meet TSC ?',
        format: '',
        width: '190',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "nace_code",
        displayName: 'NACE Code',
        format: 'string',
        width: '100',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "climate_change_mitigation",
        displayName: 'Climate Change Mitigation',
        format: '',
        width: '200',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "climate_change_adaptation",
        displayName: 'Climate Change Adaptation',
        format: '',
        width: '200',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "water_and_marine_resources",
        displayName: 'Water & Marine Resources',
        format: '',
        width: '200',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "circular_economy",
        displayName: 'Circular Economy',
        format: '',
        width: '200',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "pollution",
        displayName: 'Pollution',
        format: '',
        width: '200',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "biodiversity_and_ecosystems",
        displayName: 'Biodiversity & Ecosystems',
        format: '',
        width: '200',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "minimum_safeguards",
        displayName: 'Minimum Safeguards ?',
        format: '',
        width: '200',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "taxonomy_aligned_proportion_of_turnover_year_n",
        displayName: 'Taxonomy Aligned Proportion, Year N (%)',
        format: 'percentage',
        width: '200',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "taxonomy_aligned_proportion_of_turnover_year_n_1",
        displayName: 'Taxonomy Aligned Proportion, Year N-1 (%)',
        format: 'percentage',
        width: '220',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "category",
        displayName: 'Category',
        format: '',
        width: '80',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "revenue_from_all_economic_activities_per_environmental_objective",
        displayName: 'Revenue From All Economic Activities (%)',
        format: 'percentage',
        width: '220',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      }
    ];
    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,
      footerColumns: this.tableDimColDef,
      columns: this.tableDimColDef,
      selectedColumns: this.tableDimColDef,
      globalFilterFields: this.tableDimColDef.concat({
        field: "created_at_date",
        displayName: 'Created By',
        format: '',
        width: '100',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      }),
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };
    this.noTableData = false;
    this.tableData = [
      {
        'name': 'Telefonica',
        'domicle_country': 'Italy',
        'region': 'Europe',
        'sector': 'Telecomuications',
        'date_source': 'web-link',
        'updated_date': '2020-01-07',
        'ticker': 'BME: TEF',
        'activity_name': 'Invest',
        'absolute_turnover_currency': '20',
        'proportion_turnover_percent': '50.55',
        'covered_by_taxonomy': 'Yes',
        'covered_by_taxonomy_but_do_not_meet_TSC': 'Yes',
        'nace_code': '524',
        'taxonomy_aligned_proportion_of_turnover_year_n': 10.05,
        'taxonomy_aligned_proportion_of_turnover_year_n_1': 10.85,
        'revenue_from_all_economic_activities_per_environmental_objective': '20',
        'climate_change_mitigation': 'SC',
        'climate_change_adaptation': 'DNSH',
        'water_and_marine_resources': 'DNSH',
        'circular_economy': 'DNSH',
        'pollution': 'DNSH',
        'biodiversity_and_ecosystems': 'DNSH',
        'minimum_safeguards': 'Yes',
        'category': 'Enabling'
      }
    ];
    const arr = [];
    this.tableData.forEach((row: object) => {
      arr.push({
        data: row,
        children: [{ data: {} }]
      });
    });
    this.aggTableData = <TreeNode[]>arr;
    this.reloadAggTable();
    this.aggTableJson['totalRecords'] = this.tableData.length;
    this.aggTableJson['loading'] = false;
  }

  loadFinanceData() {
    let financeTableDimColDef = [
      {
        field: 'name',
        displayName: 'Corporate Name',
        format: '',
        width: '100',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        },
      },
      {
        field: 'domicle_country',
        displayName: 'Domicle Country',
        format: '',
        width: '120',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'region',
        displayName: 'Region',
        format: '',
        width: '80',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'sector',
        displayName: 'Sectors',
        width: '120',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'date_source',
        displayName: 'Data Source',
        width: '100',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "updated_date",
        displayName: 'Date Updated',
        format: '',
        width: '120',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "ticker",
        displayName: 'Ticker',
        format: '',
        width: '100',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "overall_taxonomy_alignment_of_investments",
        displayName: 'Overall Taxonomy Alignment Of Investments (%)',
        format: 'percentage',
        width: '220',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "overall_taxonomy_alignment_of_investments_measured_by_CapEx",
        displayName: 'Overall Taxonomy Alignment Of Investments By CapEx (%)',
        format: 'percentage',
        width: '240',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "climate_change_mitigation",
        displayName: 'Climate Change Mitigation (%)',
        format: 'percentage',
        width: '200',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "climate_change_adaptation",
        displayName: 'Climate Change Adaptation',
        format: 'percentage',
        width: '200',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "sustainable_use_and_protection_of_water_and_marine_resources",
        displayName: 'Sustainable Use & Protection Of Water & Marine Resources',
        format: 'percentage',
        width: '240',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "transition_to_circular_economy",
        displayName: 'The Transition To Circular Economy',
        format: 'percentage',
        width: '200',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "pollution_prevention_and_control",
        displayName: 'Pollution Prevention & Control',
        format: 'percentage',
        width: '200',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "protection_and_restoration_of_biodiversity_and_ecosystems",
        displayName: 'Protection & Restoration Of Biodiversity & Ecosystems',
        format: 'percentage',
        width: '240',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      }
    ];
    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,
      footerColumns: financeTableDimColDef,
      columns: financeTableDimColDef,
      selectedColumns: financeTableDimColDef,
      globalFilterFields: financeTableDimColDef.concat({
        field: "created_at_date",
        displayName: 'Created By',
        format: '',
        width: '100',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      }),
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };
    this.noTableData = false;
    this.tableData = [
      {
        'name': 'Telefonica',
        'domicle_country': 'Italy',
        'region': 'Europe',
        'sector': 'Telecomuications',
        'date_source': 'web-link',
        'updated_date': '2020-01-07',
        'ticker': 'BME: TEF',
        'overall_taxonomy_alignment_of_investments': '20.25',
        'overall_taxonomy_alignment_of_investments_measured_by_CapEx': '26.45',
        'climate_change_mitigation': '51.23',
        'climate_change_adaptation': '55.64',
        'sustainable_use_and_protection_of_water_and_marine_resources': '25.25',
        'transition_to_circular_economy': '64.25',
        'pollution_prevention_and_control': '65.35',
        'protection_and_restoration_of_biodiversity_and_ecosystems': '45.65'
      }
    ];
    const arr = [];
    this.tableData.forEach((row: object) => {
      arr.push({
        data: row,
        children: [{ data: {} }]
      });
    });
    this.aggTableData = <TreeNode[]>arr;
    this.reloadAggTable();
    this.aggTableJson['totalRecords'] = this.tableData.length;
    this.aggTableJson['loading'] = false;
  }

  loadProductData() {
    let productTableDimColDef = [
      {
        field: 'provider',
        displayName: 'Provider',
        format: '',
        width: '100',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        },
      },
      {
        field: 'name',
        displayName: 'Financial Product Name',
        format: '',
        width: '180',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        },
      },
      {
        field: 'domicle_country',
        displayName: 'Domicle Country',
        format: '',
        width: '120',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'region',
        displayName: 'Region',
        format: '',
        width: '80',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'type',
        displayName: 'Type',
        width: '120',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'date_source',
        displayName: 'Data Source',
        width: '100',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "updated_date",
        displayName: 'Date Updated',
        format: '',
        width: '120',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "ticker",
        displayName: 'Ticker',
        format: '',
        width: '100',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "environmental_and_social_characteristics",
        displayName: 'Environmental & Social Characteristics',
        format: '',
        width: '220',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "investment_strategy",
        displayName: 'Investment Strategy',
        format: '',
        width: '240',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "binding_elements",
        displayName: 'Binding Elements',
        format: '',
        width: '200',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "strategy_implemented",
        displayName: 'Strategy Implemented',
        format: '',
        width: '200',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "is_amount_of_potential_investments_excluded",
        displayName: 'Is Amount Of Potential Investments Excluded',
        format: '',
        width: '240',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "policy_to_assess_good_governance_practices",
        displayName: 'Policy To Assess Good Governance Practices',
        format: 'string',
        width: '200',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "minimum_asset_allocation",
        displayName: 'Minimum Asset Allocation',
        format: '',
        width: '200',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "economic_sectors",
        displayName: 'Economic Sectors',
        format: '',
        width: '240',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "does_this_financial_product_make_use_of_derivatives",
        displayName: 'Does This Financial Product Make Use Of Derivatives?',
        format: '',
        width: '220',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "investments_are_included_under_#2_remainder",
        displayName: 'Investments Are Included Under #2 Remainder',
        format: '',
        width: '240',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "sustainable_investments_of_the_financial_product_contributeng_elements",
        displayName: 'Sustainable Investments Of The Financial Product Contribute',
        format: '',
        width: '200',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "principal_adverse_impacts_on_sustainability_factors",
        displayName: 'Principal Adverse Impacts On Sustainability Factors',
        format: '',
        width: '200',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "investments_excluded_significantly_harm_sustainable_investment",
        displayName: 'Investments Excluded Significantly Harm Sustainable Investment',
        format: '',
        width: '240',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "sustainability_indicators",
        displayName: 'Sustainability Indicators',
        format: 'string',
        width: '200',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "find_more_product_specific_information",
        displayName: 'Find More Product Specific Informationn',
        format: '',
        width: '200',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "reference_benchmark",
        displayName: 'Reference Benchmark',
        format: '',
        width: '240',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      }
    ];
    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,
      footerColumns: productTableDimColDef,
      columns: productTableDimColDef,
      selectedColumns: productTableDimColDef,
      globalFilterFields: productTableDimColDef.concat({
        field: "created_at_date",
        displayName: 'Created By',
        format: '',
        width: '100',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      }),
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };
    this.noTableData = false;
    this.tableData = [
      {
        'provider': 'Credit Suisse',
        'name': 'Credit Suisse ESG Fund',
        'domicle_country': 'Unisted States',
        'region': 'US',
        'type': 'Benchmark',
        'date_source': 'web-link',
        'updated_date': '2020-01-07',
        'ticker': 'BLK',
        'environmental_and_social_characteristics': 'The characteristics promoted by this financial product consist of investing in corporates with excellent Environmental, Social and Governance ratings while excluding issuers producing nuclear power, owning fossil fuel reserves, producing tobacco or military weapons as well as any company from the gambling sector. This product is passively managed. Its objective is to track the performance of the TRXI World 5% Issuer Capped ESG Index, and to limit to max. 1% the tracking error between the net asset value of the product and the performance of the Index.',
        'investment_strategy': 'The investment strategy aims at tracking the TRXI World 5% Issuer Capped ESG Index. The TRXI World 5% Issuer Capped ESG Index applies a max 5% weight to the largest holdings of the TRXI World ESG Index. The latter captures companies with excellent Environmental, Social and Governance ratings, following a best-in-class investment strategy, while excluding issuers producing nuclear power, owning fossil fuel reserves, producing or selling tobacco or military weapons as well as any company from the gambling sector. Both the TRXI World 5% Issuer Capped ESG Index and the TRXI World ESG Index serve as a potential replacement for current TRXI World index exposure for investors interested in selecting corporates with the best ESG ratings while eliminating companies that have a negative impact on certain sustainability factors from their portfolio.',
        'binding_elements': 'This product is aligned with the TRXI World 5% Issuer Capped ESG Index at 95%. The TRXI World 5% Issuer Capped ESG Index, tracked by this product, targets securities of companies with the highest ESG ratings making up 25% of the market capitalization in each sector and region of the parent index. Companies must have an RTT ESG rating above "BBB" and the RTT ESG Controversies score greater than 4 to be eligible for the TRXI ESG Index. The Index applies a maximum 5% weight to the largest holdings. The exclusion of issuers producing nuclear power, owning fossil fuel reserves, producing tobacco or military weapons as well as any company from the gambling sector is another binding element.',
        'strategy_implemented': 'The investment seeks to provide investment results that, before fees and expenses, correspond generally to the total return performance of the TRXI World 5% Issuer Capped ESG Index. The fund generally invests substantially all, but at least 95%, of its total assets in the securities comprising the index. In addition, it may invest in equity securities that are not included in the index, cash and cash equivalents or money market instruments. The fund is non-diversified. Compliance with the characteristics is monitored on a regular basis.',
        'is_amount_of_potential_investments_excluded': 'Yes - the implementation of the selection criteria leads to the exclusion of at least 50% of potential investments.',
        'policy_to_assess_good_governance_practices': 'The investee companies are rated for governance aspects using the RTT ESG Ratings (www.RTT-ESG-Rating.com).',
        'minimum_asset_allocation': 'The fund invests in direct holdings. In order to meet the environmental or social characteristics promoted, the fundgenerally invests at least 95% of its total assets in the securities comprising the TRXI World 5% Issuer Capped ESG Index. Hence, 95% of the investments are aligned with the environmental characteristics. This includes 15% of the total investments that are qualified as sustainable, some of which are classified as environmental investments under the EU framework to facilitate sustainable investment). 5% of the total investments do not incorporate any environmental or social characteristic.',
        'economic_sectors': '45.65% Information and communication',
        'does_this_financial_product_make_use_of_derivatives': 'How is the use of derivatives aligned with the E/S characteristics?',
        'investments_are_included_under_#2_remainder': 'The fund may invest in equity securities that are not included in the index, cash and cash equivalents or money market instruments.',
        'sustainable_investments_of_the_financial_product_contributeng_elements': 'Investments in solar photovoltaic energy production are considered as significantly contributing to climate change mitigation under the EU Taxonomy. The investments in companies supplying water contribute to climate change adaptation. The activity of the company supplying water has a substantial contribution to GHG emissions savings through low specific energy consumption in the water supply system measured in kWh per cubic meter of water. Nonetheless, all of the selected companies supplying water might not be compliant with EU Taxonomy energy consumption thresholds for substantial contribution to climate change adaptation. Our applied threshold is that of 1 kWh per cubic meter of water, instead of 0.5 kWh as per the EU Taxonomy.',
        'principal_adverse_impacts_on_sustainability_factors': 'Upon investment and over the life of the product, we assess and monitor indicators that are deemed to indicate the presence of a principal adverse impact as per EU law, except for all biodiversity-related indicators, for which we are unable to collect data. More details can be found under the prospectus section on Adverse Impact. We address adverse impacts by engaging with investee companies. We use research from proxy voting companies to help us decide how to vote.',
        'investments_excluded_significantly_harm_sustainable_investment': 'All investments qualifying as sustainable (15%) are screened against all significant harm indicators relevant to solar photovoltaic energy production and water supply under the EU Taxonomy, with the exception of biodiversity-related indicators. Environmental Impact Assessment have been completed were relevant. The implementation of site-level biodiversity management plan complies with the IFC Performance Standard 6: Biodiversity Conservation and Sustainable Management of Living Natural Resources. The production of electricity from solar Photovoltaic did not substantially undermine climate change mitigation.',
        'sustainability_indicators': 'The indicators used are: - Highest ESG ratings making up 25% of the market capitalisation in each sector and region of the parent index. Companies must have an RTT ESG rating above "BBB" and the RTT ESG Controversies score greater than 4 to be eligible for the TRXI ESG Index. The Index applies a maximum 5% weight to the largest holdings. - No economic activity of production of nuclear power, no ownership of fossil fuel reserves, no involvement in production or sale of tobacco or military weapons as well absence of activities in the gambling sector. As regards the 15% of investments qualifying as sustainable, indicators used are the one described in the relevant section above.',
        'find_more_product_specific_information': 'Data sources and methodologies used at: www.investorYX/5%CappedESG -ETFfund.com Assessment of the principal adverse impacts of our entity on sustainability factors at: www.investorYX/sustainability/adverse_impact_statement',
        'reference_benchmark': 'Yes, However the reference benchmark is not aligned with all of the environmental or social characteristics promoted. Details: This product is 95% aligned with the TRXI World 5% Issuer Capped ESG Index. The Index is a capitalisation weighted index that limits company concentration by constraining the maximum weight of a company to 5%. It is a capped version of the TRXI World ESG Index which provides exposure to corporates across the world with excellent Environmental, Social and Governance (ESG) ratings and excludes companies which are producing nuclear power, owning fossil fuel reserves, producing tobacco or military weapons as well as any company from the gambling sector. The TRXI World ESG Index is designed for investors seeking a diversified sustainable benchmark comprised of companies with strong sustainability profiles while avoiding companies incompatible with values screens. The TRXI World ESG Index is constructed in the following way. First issuers producing nuclear power, owning fossil fuel reserves, producing tobacco or military weapons as well as any company from the gambling sector is excluded from the TRXI World Index. Then, a best in‐class selection process is applied to the remaining universe of securities in the parent index.',
      }
    ];
    const arr = [];
    this.tableData.forEach((row: object) => {
      arr.push({
        data: row,
        children: [{ data: {} }]
      });
    });
    this.aggTableData = <TreeNode[]>arr;
    this.reloadAggTable();
    this.aggTableJson['totalRecords'] = this.tableData.length;
    this.aggTableJson['loading'] = false;
  }

  resetPagination(tt1) {
    if (typeof tt1 !== 'undefined') {
      tt1.reset();
    }
  }

  reset(table) {
    table.reset();
    // this.loadCorporateData();
  }

  reloadAggTable() {
    this.displayAggTable = false;
    setTimeout(() => {
      this.displayAggTable = true;
    }, 0);
  }

  isHiddenColumn(col: Object) {
    return (
      this.aggTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }

  onFiltersApplied(filterData: object) {
    // this.filtersApplied['filters']['dimensions'] = [];
    // // tslint:disable-next-line: forin
    // for (const k in filterData['filter']['dimensions']) {
    //   if (filterData['filter']['dimensions'][k].length !== 0) {
    //     this.filtersApplied['filters']['dimensions'].push({
    //       key: k,
    //       values: filterData['filter']['dimensions'][k]
    //     });
    //   }
    // }
    // this.filtersApplied['groupby'] = filterData['groupby'];
    // this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    // this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];
    // this.tableReq = {
    //   dimensions: [],
    //   metrics: [],
    //   derived_metrics: [],
    //   timeKeyFilter: this.libServ.deepCopy(
    //     this.filtersApplied['timeKeyFilter']
    //   ),
    //   filters: this.libServ.deepCopy(this.filtersApplied['filters']),
    //   groupByTimeKey: { key: [], interval: 'daily' },
    //   gidGroupBy: [],
    //   orderBy: [
    //   ],
    //   limit: '',
    //   offset: ''
    // };
    // this.loadCorporateData();
    if (this.tabIndex == 0) {
      this.loadCorporateData();
    } else if (this.tabIndex == 1) {
      this.loadFinanceData();
    } else if (this.tabIndex == 2) {
      this.loadProductData();
    }
    const grpBys = this.getGrpBys();
  }

  getGrpBys() {
    //let grpBys = this.filtersApplied['groupby'].map(e => e.key);
    let grpBys = [this.filtersApplied['groupby'][0]['key']];
    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          grpBys.concat(
            this.filtersApplied['filters']['dimensions']
              .filter(f => f.values.length)
              .map(m => m.key)
          )
        )
      );
    }
    return grpBys;
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['name']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    }
    return false;
  }

  tabChanged(e) {
    this.ngOnDestroy();
    this.tabIndex = e.index;
    console.log("index", this.tabIndex);
    if (this.tabIndex == 0) {
      this.loadCorporateData();
    } else if (this.tabIndex == 1) {
      this.loadFinanceData();
    } else if (this.tabIndex == 2) {
      this.loadProductData();
    }
  }

  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
    if (this.appConfigObs1 && !this.appConfigObs1.closed) {
      this.appConfigObs1.unsubscribe();
    }
    if (this.appConfigObs2 && !this.appConfigObs2.closed) {
      this.appConfigObs2.unsubscribe();
    }
  }

  onChangeStatus(row) {

  }

  sendEmail() {
    const imageConfig = {
      imgName: `${this.appConfig['name']}.png`,
      type: 'image/png'
    };
    this.htmltoimage
      .canvasImage(document.getElementById('capture'), imageConfig)
      .then(canvas => {
        this.dialogService.open(SendEmailComponent, {
          header: 'Email Screenshot',
          contentStyle: {
            'max-height': '80vh',
            width: '30vw',
            overflow: 'auto'
          },
          data: canvas.toDataURL('image/png')
        });
      });
  }

  onGlobalSearchChanged(searchValue, tableColDef) {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    const grpBys = this.getGrpBys();
    let col = grpBys[0];
    this.timeout = setTimeout(() => {
      this.tableReq['filters']['globalSearch'] = {
        dimensions: [],
        value: searchValue
      };
      tableColDef.forEach(element => {
        this.tableReq['filters']['globalSearch']['dimensions'].push(
          element.field
        );
      });
      this.tableReq['filters']['globalSearch']['dimensions'].push(col);
      this.loadCorporateData();
    }, 3000);
  }

}
