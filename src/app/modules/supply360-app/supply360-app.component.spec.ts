import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Supply360AppComponent } from './supply360-app.component';

describe('Supply360AppComponent', () => {
  let component: Supply360AppComponent;
  let fixture: ComponentFixture<Supply360AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Supply360AppComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Supply360AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
