import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, from } from 'rxjs';
import { Title } from '@angular/platform-browser';
import { TreeNode } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import * as moment from 'moment';
import { CommonLibService, AppConfigService } from 'src/app/_services';
import { FetchApiDataService } from './fetch-api-data.service';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { DailyDataComponent360 } from './daily-data/daily-data.component';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SendEmailComponent } from '../send-email/send-email.component';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-supply360-app',
  templateUrl: './supply360-app.component.html',
  styleUrls: ['./supply360-app.component.scss']
})
export class Supply360AppComponent implements OnInit,OnDestroy {
  private appConfigObs: Subscription;

  appConfig: object = {};
  lastUpdatedOn: Date;
  nextUpdated: Date;
  dataUpdatedThrough: Date;
  cardsJson = [];
  showCards = true;
  isPopupGroupbyView = false;
  isPopupChartView = false;
  aggTableData: TreeNode[];
  noTableData = false;
  dimColDef: any[];
  aggTableColumnDef: any[];
  aggTableJson: Object;
  displayAggTable = true;
  appliedFilters: object = {};
  filtersApplied: object = {};
  unfilledLineChartJson: object = {};
  showUnfilledLineChart = false;
  noDataUnfilledLineChart = false;
  exportRequest: ExportRequest = <ExportRequest>{};
  isExportReport = false;

  appFeaturesConfigs = {};
  mainCardsConfig = {};

  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private htmltoimage: HtmltoimageService,
    private pptExport: ExportPptService,
    private formatNumPipe: FormatNumPipe,
  ) { }

  ngOnInit() {

    this.dimColDef = [
      {
        field: 'demand_channel',
        displayName: 'Demand Channel',
        format: '',
        width: '200',
        value: '',
        isExpanded: 'false',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'device_category',
        displayName: 'Device Category',
        format: '',
        width: '200',
        isExpanded: 'false',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'creative_size_rpt_name',
        displayName: 'Creative Size',
        format: '',
        width: '250',
        isExpanded: 'false',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'total_supply',
        displayName: 'Total Impressions',
        width: '150',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'total_revenue',
        displayName: 'Total Revenue',
        format: '$',
        width: '150',
        value: '',
        condition: '',
        exportConfig: {
          format: '$',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'total_clicks',
        displayName: 'Total Clicks',
        format: 'number',
        width: '110',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'ctr',
        displayName: 'CTR',
        width: '110',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'percentage',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'fill_rate',
        displayName: 'Fill Rate',
        format: 'percentage',
        width: '110',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'unfilled_impressions',
        displayName: 'Unfilled Impressions',
        format: 'number',
        width: '150',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'ad_server_impressions',
        displayName: 'Ad Manager Impressions',
        format: 'number',
        width: '135',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'ad_server_clicks',
        displayName: 'Ad Manager Clicks',
        format: 'number',
        width: '135',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'ad_server_revenue',
        displayName: 'Ad Manager Revenue',
        format: '$',
        width: '135',
        exportConfig: {
          format: '$',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },

      {
        field: 'adsense_impressions',
        displayName: 'Ad Sense Impressions',
        format: 'number',
        width: '150',
        value: '',
        condition: '',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'adsense_clicks',
        displayName: 'Ad Sense Clicks',
        format: 'number',
        width: '150',
        value: '',
        condition: '',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'adsense_revenue',
        displayName: 'Ad Sense Revenue',
        format: '$',
        width: '150',
        value: '',
        condition: '',
        exportConfig: {
          format: '$',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'ad_exchange_impressions',
        displayName: 'Ad Exchange Impressions',
        format: 'number',
        width: '150',
        value: '',
        condition: '',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'ad_exchange_clicks',
        displayName: 'Ad Exchange Clicks',
        format: 'number',
        width: '150',
        value: '',
        condition: '',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'ad_exchange_revenue',
        displayName: 'Ad Exchange Revenue',
        format: '$',
        width: '150',
        value: '',
        condition: '',
        exportConfig: {
          format: '$',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      }


    ];

    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      reload: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,

      columns: this.libServ.deepCopy(this.dimColDef.slice(3)),
      selectedColumns: this.libServ.deepCopy(this.dimColDef.slice(3)),
      frozenCols: this.libServ.deepCopy([...this.dimColDef.slice(0, 3)]),
      frozenWidth:
        this.dimColDef
          .slice(0, 3)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: false,
      columnSearch: false
    };

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        this.isExportReport = this.appConfig['permissions'].some(
          o => o.name === 'supply-360-export-reports'
        );
        this.isPopupGroupbyView = this.appConfig['permissions'].some(
          o => o.name == 'frank-payout-grpby-popup'
        );

        this.isPopupChartView = this.appConfig['permissions'].some(
          o => o.name == 'frank-payout-view-charts-popup'
        );
        this._titleService.setTitle(this.appConfig['displayName']);
        let startDate;
        let endDate;
        const date_config = this.appConfig['filter']['filterConfig']['filters'][
          'datePeriod'
        ][0];
        if ((date_config['defaultDate'][0]['value'] === 0 && date_config['defaultDate'][0]['period'] === 'month') && (moment().format('DD') === '01' || moment().format('DD') === '02')) {
          startDate = moment().subtract(1, 'months').startOf('month');
          endDate = moment().subtract(1, 'months').endOf('month');
        } else {
          if (
            date_config[
            'defaultDate'
            ][0]['startOf']
          ) {
            startDate = moment()
              .subtract(
                date_config['defaultDate'][0]['value'],
                date_config['defaultDate'][0]['period']
              )
              .startOf(
                date_config['defaultDate'][0]['period']
              );
          } else {
            startDate = moment().subtract(
              date_config['defaultDate'][0]['value'],
              date_config['defaultDate'][0]['period']
            );
          }
          endDate = moment().subtract(
            date_config[
            'defaultDate'
            ][1]['value'],
            date_config[
            'defaultDate'
            ][1]['period']
          );
        }

        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: startDate.format('YYYYMMDD'),
            time_key2: endDate.format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: [{ key: 'demand_channel' }, { key: 'device_category' }, { key: 'creative_size_rpt_name' }] // this.appConfig["filter"]["filterConfig"]["groupBy"].filter(
          // v => v.selected
          // )
        };
        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        this.initialLoading();
      }
    });
  }

  initialLoading() {
    this.dataFetchServ
      .getLastUpdatedData(this.appConfig['id'])
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        this.lastUpdatedOn = moment(data[0]['updated_at']).toDate();
        this.nextUpdated = moment(data[0]['next_run_at']).toDate();
        this.dataUpdatedThrough = moment(
          data[0]['source_updated_through'],
          'YYYYMMDD'
        ).toDate();
      });

    this.libServ.getAppFeaturesConfigs(this.appConfig['id']).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: data['message']
        });
        console.log(data['message']);
        return;
      }
      this.appFeaturesConfigs = data['data'];
      this.appFeaturesConfigs['cardsConfig']['cards'].forEach(element => {
        if (element['name'] === 'main') {
          this.mainCardsConfig = element['config'];
          this.cardsJson = this.mainCardsConfig['list'];
          if (this.mainCardsConfig['display']) {
            this.loadCards();
          }
        }
      });
    });

    const tableReq = {
      dimensions: ['demand_channel'], // [this.filtersApplied['groupby'][0]['key']],
      metrics: [
        'unfilled_impressions',
        'total_supply',
        'ad_server_impressions',
        'ad_server_clicks',
        'adsense_impressions',
        'adsense_clicks',
        'total_impressions',
        'total_clicks',
        'total_revenue',
        'ad_server_revenue',
        'adsense_revenue',
        'ad_exchange_impressions',
        'ad_exchange_clicks',
        'ad_exchange_revenue'
      ],
      derived_metrics: ['ctr', 'fill_rate'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: ['demand_channel'],
      orderBy: [{ key: 'demand_channel', opcode: 'asc' }],
      limit: '',
      offset: ''
    };
    this.loadTableData(tableReq);

    this.loadUnfilledLineChart();
  }

  resetPagination(tt1) {
    if (typeof tt1 !== 'undefined') {
      tt1.reset();
    }
  }

  loadTableData(params) {
    this.reloadAggTable();
    this.aggTableJson['loading'] = true;

    this.dataFetchServ.getTableData(params).subscribe(data => {
      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      } else {
        this.noTableData = false;
      }
      const grpBysFilter = ['demand_channel', 'device_category', 'creative_size_rpt_name'];
      grpBysFilter.reverse();

      const tableData = data['data'];
      this.dimColDef.find(x => x.fi);
      const arr = [];
      tableData.forEach((row: object) => {

        for (let i = 0; i < grpBysFilter.length; i++) {
          if (i !== grpBysFilter.length - 1) {
            row[grpBysFilter[i]] = 'All';
          }
        }
        let obj = {};
        if (grpBysFilter.length === 1) {
          obj = {
            data: row
          };
        } else {
          obj = {
            data: row,
            children: [{ data: {} }]
          };
        }

        obj['data']['isExpanded'] = '0';
        arr.push(obj);
      });

      this.aggTableData = <TreeNode[]>arr;
      this.aggTableJson['totalRecords'] = data['totalItems'];
      this.aggTableJson['loading'] = false;
    });
  }

  reloadAggTable() {
    this.displayAggTable = false;
    setTimeout(() => {
      this.displayAggTable = true;
    }, 0);
  }

  reset(table) {
    table.reset();
    const tableReq = {
      dimensions: ['demand_channel'],
      metrics: [
        'unfilled_impressions',
        'total_supply',
        'ad_server_impressions',
        'ad_server_clicks',
        'adsense_impressions',
        'adsense_clicks',
        'total_supply',
        'total_impressions',
        'total_clicks',
        'total_revenue',
        'ad_server_revenue',
        'adsense_revenue',
        'ad_exchange_impressions',
        'ad_exchange_clicks',
        'ad_exchange_revenue'

      ],
      derived_metrics: ['fill_rate', 'ctr'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: ['demand_channel'], // this.filtersApplied['filters']['dimensions'].map(e => e.key), // grpBys,
      orderBy: [{ key: 'demand_channel', opcode: 'asc' }],
      limit: '',
      offset: ''
    };
    (this.aggTableJson['columns'] = this.libServ.deepCopy(
      this.dimColDef.slice(3)
    )),
      (this.aggTableJson['selectedColumns'] = this.libServ.deepCopy(
        this.dimColDef.slice(3)
      )),
      (this.aggTableJson['frozenCols'] = this.libServ.deepCopy([
        ...this.dimColDef.slice(0, 3)
      ])),
      this.loadTableData(tableReq);
  }

  loadCards() {
    this.showCards = false;
    const apiUrl = this.mainCardsConfig['api']['url'];
    const cardsReq = this.libServ.deepCopy(
      this.mainCardsConfig['api']['request']
    );
    cardsReq['timeKeyFilter'] = this.libServ.deepCopy(
      this.filtersApplied['timeKeyFilter']
    );
    cardsReq['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);

    this.libServ.getDataFromDynamicAPI(apiUrl, cardsReq).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error - Contact to administator',
          detail: data
        });
        console.log(data['status_msg']);
        return;
      }
      if (!this.libServ.isEmptyObj(data['data'])) {
        this.cardsJson.map(o => {
          o['value'] = data['data'][0][o['field']];
        });
      } else {
        this.cardsJson.map(o => {
          o['value'] = 0;
        });
      }
      this.showCards = this.mainCardsConfig['display'];
    });
  }

  onTableDrill(e: Event) {
    if (!e['node']['childLoaded']) {
      this.aggTableJson['loading'] = true;

      if (Object.keys(this.appliedFilters).length === 0) {
        this.appliedFilters['filters'] = { dimensions: [] };
      }

      const tableReq = {
        dimensions: [],
        metrics: [
          'unfilled_impressions',
          'total_supply',
          'ad_server_impressions',
          'ad_server_clicks',
          'adsense_impressions',
          'adsense_clicks',
          'total_supply',
          'total_impressions',
          'total_clicks',
          'total_revenue',
          'ad_server_revenue',
          'adsense_revenue',
          'ad_exchange_impressions',
          'ad_exchange_clicks',
          'ad_exchange_revenue'
        ],
        derived_metrics: ['ctr', 'fill_rate'],
        timeKeyFilter: this.filtersApplied['timeKeyFilter'],
        filters: this.libServ.deepCopy(this.filtersApplied['filters']), // this.appliedFilters["filters"],
        groupByTimeKey: {
          key: ['accounting_key', 'time_key'],
          interval: 'daily'
        },
        gidGroupBy: this.getGrpBys(),
        orderBy: [{ key: 'demand_channel', opcode: 'asc' }],
        limit: '',
        offset: ''
      };
      const grpBys = ['demand_channel', 'device_category', 'creative_size_rpt_name'];
      for (const g of grpBys) {
        if (e['node']['data'][g] !== 'All') {
          tableReq['dimensions'].push(g);
          if (!tableReq['gidGroupBy'].includes(g)) {
            tableReq['gidGroupBy'].push(g);
          }
          if (
            tableReq['filters']['dimensions'].findIndex(e => e.key === g) === -1
          ) {
            tableReq['filters']['dimensions'].push({
              key: g,
              values: [e['node']['data'][g]]
            });
          } else {
            tableReq['filters']['dimensions'].splice(
              tableReq['filters']['dimensions'].findIndex(e => e.key === g),
              1
            );
            tableReq['filters']['dimensions'].push({
              key: g,
              values: [e['node']['data'][g]]
            });
          }
        } else {
          tableReq['dimensions'].push(g);
          if (!tableReq['gidGroupBy'].includes(g)) {
            tableReq['gidGroupBy'].push(g);
          }
          break;
        }
      }
      this.dataFetchServ.getTableData(tableReq).subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        const childData = data['data'];
        const arr = [];

        childData.forEach((row: object) => {
          this.aggTableJson['frozenCols'].forEach(r => {
            if (typeof row[r['field']] === 'undefined') {
              row[r['field']] = 'All';
            }
          });
          let obj = {};
          if (row[grpBys[grpBys.length - 1]] === 'All') {
            obj = {
              data: row,
              children: [{ data: {} }]
            };
          } else {
            obj = {
              data: row
            };
          }
          arr.push(obj);
        });



        this.aggTableJson['loading'] = false;
        e['node']['children'] = <TreeNode[]>arr;
        this.aggTableData = [...this.aggTableData];
        e['node']['childLoaded'] = true;

      });
    }
  }

  onLazyLoadAggTable(e: Event) {
    console.log('Lazy Agg', e);
  }

  openPopup(row, field) {
    const grpBys = [];
    const filtersApplied = this.libServ.deepCopy(this.filtersApplied);
    let str = '';
    // const grpBysFilter = ['demand_channel', 'device_category','creative_size_rpt_name'];
    const fieldIdx = filtersApplied['groupby'].findIndex(
      e => e['key'] === field
    );
    filtersApplied['groupby'].forEach((grp, i) => {
      if (i <= fieldIdx) {
        if (
          filtersApplied['filters']['dimensions'].findIndex(
            e => e.key === grp['key']
          ) !== -1
        ) {
          filtersApplied['filters']['dimensions'].splice(
            filtersApplied['filters']['dimensions'].findIndex(
              e => e.key === grp['key']
            ),
            1
          );
        }

        if (str === '') {
          str = str + row[grp['key']];
        } else {
          str = str + ' > ' + row[grp['key']];
        }
        filtersApplied['filters']['dimensions'].push({
          key: grp['key'],
          values: [row[grp['key']]]
        });
      }
    });

    filtersApplied['groupby'] = grpBys;
    const data = {
      filters: filtersApplied,
      title: str,
      heading: row[field],
      exportRequest: this.exportRequest,
      isExportReport: this.isExportReport
    };
    let headerMsg;
    this.dimColDef.forEach((row1) => {
      if (row1['field'] == field) {
        headerMsg = row1['displayName'];
      }
    });
    const ref = this.dialogService.open(DailyDataComponent360, {
      header: headerMsg + ': ' + row[field],
      contentStyle: { width: '80vw', overflow: 'auto' },
      data: data
    });
    ref.onClose.subscribe((data: string) => { });
  }

  loadUnfilledLineChart() {
    const unfilledLineChartReq = {
      dimensions: ['demand_channel', 'time_key'],
      metrics: [
        'total_supply'
      ],
      derived_metrics: [],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key'],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys(), // this.filtersApplied['filters']['dimensions'].map(e => e.key), // grpBys,
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };

    const colors = this.libServ.dynamicColors(2);
    this.unfilledLineChartJson = {
      chartTypes: [{ key: 'line', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: []
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Overall Supply Trend'
        },
        legend: {
          display: true,
          onClick: e => e.stopPropagation()
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: 'Total Supply'
              },
              position: 'left',
              name: '2',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, 'number', [0]);
                }
              }
              // scaleFontColor: "rgba(151,187,205,0.8)"
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    this.showUnfilledLineChart = false;
    this.dataFetchServ
      .getTableData(unfilledLineChartReq)
      .subscribe(data => {
        const chartData = data['data'];
        this.noDataUnfilledLineChart = false;
        if (!chartData.length) {
          this.noDataUnfilledLineChart = true;
          return;
        }
        const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));

        this.unfilledLineChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );

        const sources = Array.from(new Set(chartData.map(r => r['demand_channel'])));

        sources.forEach((src, i) => {
          const total_supply = [];
          datesArr.forEach(time_key => {
            chartData.forEach(r => {
              if (r['demand_channel'] === src && r['time_key'] === time_key) {
                total_supply.push(r['total_supply']);
              }
            });
          });
          this.unfilledLineChartJson['chartData']['datasets'].push({
            label: src,
            data: total_supply,
            borderColor: colors[i],
            fill: false,
            backgroundColor: colors[i]
          });
          this.showUnfilledLineChart = true;
        });
      });
  }

  exportTable(table, fileFormat) {
    if (this.exportRequest['sendEmail'].length > 0) {
      this.toastService.displayToast({
        severity: 'info',
        summary: 'Export Report',
        detail:
          'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
        life: 10000
      });

      const colDef = this.libServ.deepCopy(this.dimColDef);

      const tableReq = {
        dimensions: ['demand_channel', 'device_category', 'creative_size_rpt_name'],
        metrics: [
          'unfilled_impressions',
          'total_supply',
          'ad_server_impressions',
          'ad_server_clicks',
          'adsense_impressions',
          'adsense_clicks',
          'total_supply',
          'total_impressions',
          'total_clicks',
          'total_revenue',
          'ad_server_revenue',
          'adsense_revenue',
          'ad_exchange_impressions',
          'ad_exchange_clicks',
          'ad_exchange_revenue'
        ],
        derived_metrics: ['fill_rate', 'ctr'],
        timeKeyFilter: this.libServ.deepCopy(
          this.filtersApplied['timeKeyFilter']
        ),
        filters: this.libServ.deepCopy(this.filtersApplied['filters']),
        groupByTimeKey: {
          key: [],
          interval: 'daily'
        },
        gidGroupBy: ['demand_channel', 'device_category', 'creative_size_rpt_name'], // grpBys,
        orderBy: [{ key: 'demand_channel', opcode: 'asc' }],
        limit: '',
        offset: ''
      };

      tableReq['isTable'] = false;

      const sheetDetails = {};
      sheetDetails['columnDef'] = colDef;
      sheetDetails['data'] = [];
      sheetDetails['sheetName'] = 'Data Distribution';
      sheetDetails['isRequest'] = true;
      sheetDetails['request'] = {
        url: '/api/flask/supply360/cassandradata',
        method: 'POST',
        param: tableReq
      };
      sheetDetails['disclaimer'] = [
        {
          position: 'top',
          label:
            'Data persent in below table is not final and may varies over period of time',
          color: '#3A37CF'
        },
        {
          position: 'bottom',
          label: 'Thank You',
          color: '#EC6A15'
        }
      ];
      sheetDetails['totalFooter'] = {
        available: false,
        custom: false
      };
      sheetDetails['tableTitle'] = {
        available: true,
        label: 'Overall Distribution'
      };
      sheetDetails['image'] = [
        {
          available: true,
          path: environment.exportConfig.exportLogo,
          position: 'top'
        }
      ];
      const sheetDetailsarray = [];
      sheetDetailsarray.push(sheetDetails);
      this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
      this.exportRequest['fileName'] =
        'Overall Distribution ' +
        moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
      this.exportRequest['exportFormat'] = fileFormat;
      this.exportRequest['exportConfig'] = environment.exportConfig;
      this.dataFetchServ
        .getExportReportData(this.exportRequest)
        .subscribe(response => {
          console.log('response', response);
        });
    } else {
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Export Report',
        detail:
          'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
        life: 10000
      });
    }
    return false;
  }

  isHiddenColumn(col: Object) {
    return (
      this.aggTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }

  /* --------------------- On Filter Apply Button Clicked --------------------- */

  onFiltersApplied(filterData: object) {
    this.filtersApplied['filters']['dimensions'] = [];
    // tslint:disable-next-line: forin
    for (const k in filterData['filter']['dimensions']) {
      if (filterData['filter']['dimensions'][k].length !== 0) {
        this.filtersApplied['filters']['dimensions'].push({
          key: k,
          values: filterData['filter']['dimensions'][k]
        });
      }
    }
    // this.filtersApplied['groupby'] = filterData['groupby'];
    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];

    const tableReq = {
      dimensions: ['demand_channel'],
      metrics: [
        'unfilled_impressions',
        'total_supply',
        'ad_server_impressions',
        'ad_server_clicks',
        'adsense_impressions',
        'adsense_clicks',
        'total_supply',
        'total_impressions',
        'total_clicks',
        'total_revenue',
        'ad_server_revenue',
        'adsense_revenue',
        'ad_exchange_impressions',
        'ad_exchange_clicks',
        'ad_exchange_revenue'
      ],
      derived_metrics: ['fill_rate', 'ctr'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: [],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys(), // this.filtersApplied['filters']['dimensions'].map(e => e.key), // grpBys,
      orderBy: [{ key: 'demand_channel', opcode: 'asc' }],
      limit: '',
      offset: ''
    };
    this.loadTableData(tableReq);

    if (this.mainCardsConfig['display']) {
      this.mainCardsConfig['api']['request']['gidGroupBy'] = this.getGrpBys();
      this.loadCards();
    }

    this.loadUnfilledLineChart();
  }

  getGrpBys() {
    let grpBys = this.filtersApplied['groupby'].map(e => e.key);
    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          grpBys.concat(
            this.filtersApplied['filters']['dimensions']
              .filter(f => f.values.length)
              .map(m => m.key)
          )
        )
      );
    }
    return grpBys;
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['name']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    } else if (type === 'ppt') {
      const chartElements = document.getElementsByTagName('ym-charts');
      const slides = [];
      [].forEach.call(chartElements, (el, index) => {
        const c = el.getElementsByTagName('canvas')[0];

        slides.push({
          title: `${this.appConfig['name']}`,
          base64data: this.libServ.canvasToImage('white', c),
          width: 8,
          height: 3
        });

        if (index === chartElements.length - 1) {
          const pptConfig = {
            filename: 'Charts',
            logo: {
              visible: true,
              path: '/assets/images/ym.png',
              width: 0.53125,
              height: 0.458333
            },
            slides: slides
          };
          this.pptExport.exportPPT(pptConfig);
        }
      });
    }
    return false;
  }

  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }

  sendEmail() {
    const imageConfig = {
      imgName: `${this.appConfig['name']}.png`,
      type: 'image/png'
    };

    this.htmltoimage
      .canvasImage(document.getElementById('capture'), imageConfig)
      .then(canvas => {
        this.dialogService.open(SendEmailComponent, {
          header: 'Email Screenshot',
          contentStyle: { 'max-height': '80vh', overflow: 'auto' },
          data: canvas.toDataURL('image/png')
        });
      });
  }
}
