import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FilterContainerModule } from '../common/filter-container/filter-container.module';
import { Supply360Routes } from './supply360.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormatNumPipe } from '../../_pipes/number-format.pipe';
import { SharedModule } from '../../_pipes/shared.module';
import { ChatModule } from '../common/chat/chat.module';
import { TreeTableModule } from 'primeng/treetable';
import { MultiSelectModule } from 'primeng/multiselect';
import { InputSwitchModule } from 'primeng/inputswitch';
import { Supply360AppComponent } from './supply360-app.component';
import { ChartsModule } from '../common/charts/charts.module';
import { CardsModule } from '../common/cards/cards.module';


@NgModule({
  declarations: [Supply360AppComponent],
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    FilterContainerModule,
    TreeTableModule,
    MultiSelectModule,
    InputSwitchModule,
    ChartsModule,
    CardsModule,
    RouterModule.forChild(Supply360Routes)
  ],
  providers: [FormatNumPipe]
})
export class Supply360Module {}
