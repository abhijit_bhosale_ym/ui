import { Component, OnInit } from '@angular/core';
import { CommonLibService } from '../../../../app/_services';
import { FormatNumPipe } from '../../../../app/_pipes/number-format.pipe';
import { FetchApiDataService } from '../fetch-api-data.service';
import * as moment from 'moment';
import { TreeNode } from 'primeng/api';
import { DynamicDialogConfig } from 'primeng/dynamicdialog';
import { ExportdataService } from '../../../../app/_services/export/exportdata.service';
import { ToastService } from '../../../../app/_services/toast-notification/toast.service';
import { ExportRequest } from '../../../../app/_interfaces/exportRequest';
import { SheetDetails } from '../../../../app/_interfaces/sheetDetails';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-daily-data',
  templateUrl: './daily-data.component.html',
  styleUrls: ['./daily-data.component.scss']
})
export class DailyDataComponent360 implements OnInit {
  tableDataReq: object;
  tableData: TreeNode[];
  tableColumnDef: any[];
  dimColDef: any[];
  tableJson: Object;
  apiData: any[];
  impressionChartVisited = false;
  CTRChartVisited = false;
  showImpLineChart = false;
  showCTRLineChart = false;
  ctrChartJson: object;
  impressionsChartJson: object;
  defaultChartsJson: object;
  filtersApplied: object;
  exportRequest: ExportRequest = <ExportRequest>{};
  isExportReport = false;
  constructor(
    private libServ: CommonLibService,
    private formatNumPipe: FormatNumPipe,
    private dataFetchServ: FetchApiDataService,
    private config: DynamicDialogConfig,
    private toastService: ToastService
  ) {
    this.filtersApplied = this.config.data['filters'];
  }

  ngOnInit() {
    this.exportRequest = this.config.data['exportRequest'];
    this.isExportReport = this.config.data['isExportReport'];
    this.dimColDef = [
      {
        field: 'demand_channel',
        displayName: 'Demand Channel',
        format: '',
        width: '150',
        value: '',
        isExpanded: 'false',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'device_category',
        displayName: 'Device Category',
        format: '',
        width: '250',
        isExpanded: 'false',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'creative_size_rpt_name',
        displayName: 'Creative Size',
        format: '',
        width: '150',
        isExpanded: 'false',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      }
    ];
    this.tableColumnDef = [
      {
        field: 'total_supply',
        displayName: 'Total Impressions',
        width: '150',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'total_revenue',
        displayName: 'Total Revenue',
        format: '$',
        width: '150',
        value: '',
        condition: '',
        exportConfig: {
          format: '$',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'total_clicks',
        displayName: 'Total Clicks',
        format: 'number',
        width: '110',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'ctr',
        displayName: 'CTR',
        width: '110',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'percentage',
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'fill_rate',
        displayName: 'Fill Rate',
        format: 'percentage',
        width: '110',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'unfilled_impressions',
        displayName: 'Unfilled Impressions',
        format: 'number',
        width: '150',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'ad_server_impressions',
        displayName: 'Ad Manager Impressions',
        format: 'number',
        width: '135',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'ad_server_clicks',
        displayName: 'Ad Manager Clicks',
        format: 'number',
        width: '135',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'ad_server_revenue',
        displayName: 'Ad Manager Revenue',
        format: '$',
        width: '135',
        exportConfig: {
          format: '$',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },

      {
        field: 'adsense_impressions',
        displayName: 'Ad Sense Impressions',
        format: 'number',
        width: '150',
        value: '',
        condition: '',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'adsense_clicks',
        displayName: 'Ad Sense Clicks',
        format: 'number',
        width: '150',
        value: '',
        condition: '',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'adsense_revenue',
        displayName: 'Ad Sense Revenue',
        format: '$',
        width: '150',
        value: '',
        condition: '',
        exportConfig: {
          format: '$',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'ad_exchange_impressions',
        displayName: 'Ad Exchange Impressions',
        format: 'number',
        width: '150',
        value: '',
        condition: '',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'ad_exchange_clicks',
        displayName: 'Ad Exchange Clicks',
        format: 'number',
        width: '150',
        value: '',
        condition: '',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      },
      {
        field: 'ad_exchange_revenue',
        displayName: 'Ad Exchange Revenue',
        format: '$',
        width: '150',
        value: '',
        condition: '',
        exportConfig: {
          format: '$',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      }
    ];
    this.tableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '300px',
      totalRecords: 1000,

      columns: this.tableColumnDef,
      selectedColumns: this.tableColumnDef,
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: false,
      overallSearch: false,
      columnSearch: false
    };
    this.loadTable();
    this.defaultChartsJson = {
      chartTypes: [{ key: 'bar', label: 'Line Chart', stacked: true }],
      chartData: {
        labels: [],
        datasets: []
      },
      chartOptions: {
        title: {
          display: true,
          text: ''
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              display: true,
              scaleLabel: {
                display: true
              }
            }
          ]
        },
        pan: {
          enabled: true,
          mode: 'x'
        },
        zoom: {
          enabled: true,
          mode: 'x'
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: true,
      chartWidth: '',
      chartHeight: '300px'
    };
  }

  loadTable() {
    this.tableJson['loading'] = true;
    let grpBys = this.filtersApplied['groupby'].map(e => e.key);

    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          this.filtersApplied['filters']['dimensions']
            .filter(f => f.values.length)
            .map(m => m.key)
            .concat(grpBys)
        )
      );
    }

    this.dimColDef.reverse().forEach(c => {
      if (grpBys.includes(c.field)) {
        this.tableColumnDef.unshift(c);
      }
    });
    this.tableColumnDef.unshift({
      field: 'time_key',
      displayName: 'Date',
      format: 'date',
      width: '120',
      exportConfig: {
        format: 'date',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: false,
        colSort: true,
        resizable: true,
        movable: false
      }
    });


    this.tableDataReq = {
      dimensions: grpBys,
      metrics: [
        'unfilled_impressions',
        'total_supply',
        'ad_server_impressions',
        'ad_server_clicks',
        'adsense_impressions',
        'adsense_clicks',
        'total_clicks',
        'total_impressions',
        'total_revenue',
        'ad_server_revenue',
        'adsense_revenue',
        'ad_exchange_impressions',
        'ad_exchange_clicks',
        'ad_exchange_revenue'
      ],
      derived_metrics: ['ctr', 'fill_rate'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: grpBys.filter(v => v !== 'time_key'),
      orderBy: [{ key: 'time_key', opcode: 'desc' }],
      limit: '',
      offset: ''
    };
    this.tableDataReq['dimensions'].push('time_key');
    this.dataFetchServ.getTableData(this.tableDataReq).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }
      const tableData = data['data'];
      const arr = [];
      tableData.forEach((row: object) => {
        const obj = {
          data: row
        };
        arr.push(obj);
      });

      this.tableData = <TreeNode[]>arr;
      this.tableJson['totalRecords'] = data['totalItems'];
      this.tableJson['loading'] = false;
      this.apiData = data['data'];
      this.apiData.reverse();
    });

  }

  exportTablePopup(fileFormat) {
    if (this.exportRequest['sendEmail'].length > 0) {
      this.toastService.displayToast({
        severity: 'info',
        summary: 'Export Report',
        detail:
          'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
        life: 10000
      });
      const columnDefs = this.tableColumnDef;
      const sheetDetailsarray = [];
      const sheetlineDetails = {};
      sheetlineDetails['columnDef'] = columnDefs;
      sheetlineDetails['data'] = this.apiData;
      sheetlineDetails['sheetName'] = 'Daily Breakup';
      sheetlineDetails['isRequest'] = false;
      sheetlineDetails['request'] = {
        url: '',
        method: '',
        param: {}
      };
      sheetlineDetails['disclaimer'] = [
        {
          position: 'top',
          label:
            'Data persent in below table is not final and may varies over period of time',
          color: '#3A37CF'
        },
        {
          position: 'bottom',
          label: 'Thank You',
          color: '#EC6A15'
        }
      ];
      sheetlineDetails['totalFooter'] = {
        available: false,
        custom: false
      };
      sheetlineDetails['tableTitle'] = {
        available: true,
        label: `
        ${''
          } Daily Breakup`
      };
      sheetlineDetails['image'] = [
        {
          available: true,
          path: environment.exportConfig.exportLogo,
          position: 'top'
        }
      ];

      sheetDetailsarray.push(sheetlineDetails);
      this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
      this.exportRequest['fileName'] =
        `
      ${''} Daily Breakup` + moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
      this.exportRequest['exportFormat'] = fileFormat;
      this.exportRequest['exportConfig'] = environment.exportConfig;
      this.dataFetchServ
        .getExportReportData(this.exportRequest)
        .subscribe(response => {
          console.log(response);
        });
    } else {
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Export Report',
        detail:
          'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email.Thanks!',
        life: 10000
      });
    }
  }

  tabChanged(e) {
    if (e.index === 1 && !this.impressionChartVisited) {
      this.impressionChartVisited = true;
      this.loadImpressionchart();
    } else if (e.index === 2 && !this.CTRChartVisited) {
      this.CTRChartVisited = true;
      this.loadCTRChart();
    }
  }

  loadImpressionchart() {
    this.showImpLineChart = false;
    const colors = this.libServ.dynamicColors(2);

    this.impressionsChartJson = this.libServ.deepCopy(this.defaultChartsJson);
    this.impressionsChartJson['chartOptions']['title']['text'] =
      'Unfilled vs Supply Bar-Chart';
    this.impressionsChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
      callback: (value, index, values) => {
        return this.formatNumPipe.transform(value, 'number', []);
      }
    };
    this.impressionsChartJson['chartOptions']['scales']['yAxes'][0][
      'scaleLabel'
    ]['labelString'] = 'Impressions';
    this.impressionsChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[
            tooltipItem.index
            ];
          if (tooltipItem.datasetIndex !== 0) {
            return `${
              data.datasets[tooltipItem.datasetIndex].label
              } : ${this.formatNumPipe.transform(
                currentValue,
                'number',
                []
              )}`;
          }
          return `${
            data.datasets[tooltipItem.datasetIndex].label
            } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
        }
      }
    };
    const chartData = this.apiData;
    const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));
    this.impressionsChartJson['chartData']['labels'] = datesArr.map(d =>
      moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
    );
    this.impressionsChartJson['chartData']['datasets'] = [
      {
        label: 'Unfilled Impressions',
        borderColor: colors[0],
        fill: false,
        backgroundColor: colors[0],
        data: []
      },
      {
        label: 'Total Supply',
        borderColor: colors[1],
        fill: false,
        backgroundColor: colors[1],
        data: []
      }
    ];

    const impArr = [];
    const threePImpArr = [];
    datesArr.forEach(time_key => {
      chartData.forEach(r => {
        if (r['time_key'] === time_key) {
          impArr.push(parseInt(r['unfilled_impressions'], 10));
          threePImpArr.push(parseInt(r['total_supply'], 10));
        }
      });
    });
    this.impressionsChartJson['chartData']['datasets'][0]['data'] = impArr;
    this.impressionsChartJson['chartData']['datasets'][1][
      'data'
    ] = threePImpArr;
    this.showImpLineChart = true;
  }

  loadCTRChart() {
    this.showCTRLineChart = false;
    const colors = this.libServ.dynamicColors(2);
    this.ctrChartJson = {
      chartTypes: [{ key: 'line', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'CTR',
            type: 'line',
            yAxisID: 'y-axis-0',

            borderColor: colors[1],
            fill: false,
            backgroundColor: colors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'CTR Line Chart'
        },
        legend: {
          display: true,
          onClick: e => e.stopPropagation()
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: 'CTR'
              },
              position: 'left',
              name: '2',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, 'percentage', [2]);
                }
              }
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, 'percentage', [2])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };
    const chartData = this.apiData;
    const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));

    this.ctrChartJson['chartData']['labels'] = datesArr.map(d =>
      moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
    );

    const revArr = [];
    const ecpmArr = [];
    datesArr.forEach(time_key => {
      chartData.forEach(r => {
        if (r['time_key'] === time_key) {
          ecpmArr.push(r['ctr']);
        }
      });
    });

    this.ctrChartJson['chartData']['datasets'][0]['data'] = ecpmArr;
    this.showCTRLineChart = true;
  }

  isHiddenColumn(col) { }
}
