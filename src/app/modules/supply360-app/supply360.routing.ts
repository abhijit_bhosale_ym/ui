import { Routes } from '@angular/router';

import { Supply360AppComponent } from './supply360-app.component';
export const Supply360Routes: Routes = [
  {
    path: '',
    component: Supply360AppComponent
  }
];
