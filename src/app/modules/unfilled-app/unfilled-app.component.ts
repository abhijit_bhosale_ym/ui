import { Component, OnInit, OnDestroy } from '@angular/core';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { CommonLibService, DataShareService } from 'src/app/_services';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';
import { FetchUnfilledApiDataService } from './fetch-unfilled-api-data.service';
import { UnfilledDataPopupComponent } from './unfilled-popup/unfilled-daily-data-popup.component';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { groupBy, filter } from 'rxjs/operators';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { Subscription } from 'rxjs';
import { SendEmailComponent } from '../send-email/send-email.component';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'ym-unfilled-app',
  templateUrl: './unfilled-app.component.html',
  styleUrls: ['./unfilled-app.component.scss']
})
export class UnfilledAppComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;
  timeZone = environment.timeZone;

  appConfig: object = {};
  lastUpdatedOn: Date;

  aggTableData: TreeNode[];
  dimColDef: any[];
  metricColDef: any[];
  noTableData = false;
  aggTableColumnDef: any[];
  aggTableJson: Object;
  displayAggTable = false;
  noDataMainLineChart = false;
  noDataPieChart = false;
  showMainLineChart = false;
  showMainPieChart = false;
  mainLineChartJson: object;
  mainPieChartJson: object;
  appliedFilters: object = {};
  multiSortMeta = [];
  nextUpdated: Date;
  dataUpdatedThrough: Date;
  showColumns: any[];
  isExportReport = false;
  filtersApplied: object = {};
  timeout: any;
  tableReq: object;
  exportRequest: ExportRequest = <ExportRequest>{};

  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: FetchUnfilledApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private formatNumPipe: FormatNumPipe,
    private exportService: ExportdataService,
    private pptExport: ExportPptService,
    private confirmationService: ConfirmationService,
    private htmltoimage: HtmltoimageService // private exportService: ExportdataService
  ) { }

  ngOnInit() {
    this.metricColDef = [];

    this.dimColDef = [
      {
        field: 'region',
        displayName: 'Intventory Types',
        format: '',
        width: '175',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'station',
        displayName: 'Station',
        format: '',
        width: '175',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'creative_size',
        displayName: 'Creative',
        format: '',
        width: '175',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      }
    ];

    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '200px',
      totalRecords: 1000,

      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        console.log('appconfig .....', this.appConfig);
        this.isExportReport = this.appConfig['permissions'].some(
          o => o.name === 'unfilled-export-reports'
        );
        this._titleService.setTitle(this.appConfig['displayName']);
        let startDate;
        if (
          this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
          'defaultDate'
          ][0]['startOf']
        ) {
          startDate = moment()
            .subtract(
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['value'],
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['period']
            )
            .startOf(
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['period']
            );
        } else {
          startDate = moment().subtract(
            this.appConfig['filter']['filterConfig']['filters'][
            'datePeriod'
            ][0]['defaultDate'][0]['value'],
            this.appConfig['filter']['filterConfig']['filters'][
            'datePeriod'
            ][0]['defaultDate'][0]['period']
          );
        }
        const endDate = moment().subtract(
          this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
          'defaultDate'
          ][1]['value'],
          this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
          'defaultDate'
          ][1]['period']
        );

        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: startDate.format('YYYYMMDD'),
            time_key2: endDate.format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: this.appConfig['filter']['filterConfig']['groupBy']
        };
        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        this.initialLoading();
      }
    });
  }

  initialLoading() {
    this.dataFetchServ
      .getLastUpdatedData(this.appConfig['id'])
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        this.lastUpdatedOn = moment(data[0]['updated_at']).toDate();
        this.nextUpdated = moment(data[0]['next_run_at']).toDate();
        this.dataUpdatedThrough = moment(
          data[0]['source_updated_through'],
          'YYYYMMDD'
        ).toDate();
      });

    console.log('in initial loading');
    const mainLineChartReq = {
      dimensions: ['accounting_key'],
      metrics: ['unfilled_impressions'],
      derived_metrics: [],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.filtersApplied['filters'],
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source'],
      orderBy: [{ key: 'accounting_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };

    this.loadMainLineChart(mainLineChartReq);

    const mainPieChartReq = {
      dimensions: ['region'],
      metrics: ['unfilled_impressions'],
      derived_metrics: [],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.filtersApplied['filters'],
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: ['region'],
      orderBy: [{ key: 'unfilled_impressions', opcode: 'desc' }],
      limit: '',
      offset: ''
    };

    this.loadMainPieChart(mainPieChartReq);
    this.tableReq = {
      dimensions: [this.filtersApplied['groupby'][0]['key'], 'accounting_key'],
      metrics: ['unfilled_impressions'],
      derived_metrics: [],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys(this.filtersApplied['groupby'][0]['key']), // grpBys,
      orderBy: [
        { key: 'accounting_key', opcode: 'desc' },
        { key: this.filtersApplied['groupby'][0]['key'], opcode: 'asc' }
      ],
      limit: '',
      offset: ''
    };
    this.loadTableData(this.tableReq);

  }

  loadTableData(param: Object) {
    const finalColDef = [];
    this.metricColDef = [];

    const grpBys = this.filtersApplied['groupby'].map(e => e.key);
    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    }

    grpBys.forEach((row: object) => {
      this.metricColDef.push(this.dimColDef.find(x => x.field == row));
    });
    this.aggTableJson['loading'] = true;

    // const tableReq = {
    //   dimensions: [this.filtersApplied['groupby'][0]['key'], 'accounting_key'],
    //   metrics: ['unfilled_impressions'],
    //   derived_metrics: [],
    //   timeKeyFilter: this.filtersApplied['timeKeyFilter'],
    //   filters: this.filtersApplied['filters'],
    //   groupByTimeKey: {
    //     key: ['accounting_key', 'time_key'],
    //     interval: 'daily'
    //   },
    //   gidGroupBy: this.getGrpBys(this.filtersApplied['groupby'][0]['key']), // grpBys,
    //   orderBy: [
    //     { key: 'accounting_key', opcode: 'desc' },
    //     { key: this.filtersApplied['groupby'][0]['key'], opcode: 'asc' }

    //   ],
    //   limit: '',
    //   offset: ''
    // };

    this.dataFetchServ.getunfilledData(param).subscribe(data => {
      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      } else {
        this.noTableData = false;
      }

      const data1 = data as [];
      const arr1 = [];
      let resultArray = [];
      let months = moment(this.filtersApplied['timeKeyFilter']['time_key2'], 'YYYYMMDD').diff(moment(this.filtersApplied['timeKeyFilter']['time_key1'], 'YYYYMMDD'), 'months', true);
      // resultArray = Array.from(
      //   new Set(data1['data'].map(s => s['accounting_key']))
      // );
      console.log('months', months)
      for (let i = 0; i <= months; i++) {
        resultArray.push(moment(this.filtersApplied['timeKeyFilter']['time_key1']).add(i, 'months').startOf('month').format('YYYYMMDD'));
      }
      resultArray.reverse().forEach((col: Object) => {
        const d1 = moment(JSON.stringify(col), 'YYYYMMDD').format('MMM-YYYY');

        this.metricColDef.push({
          field: col.toString(),
          displayName: d1,
          format: 'number',
          width: '120',
          exportConfig: {
            format: 'number',
            styleinfo: {
              thead: 'default',
              tdata: 'white'
            }
          },
          formatConfig: [],
          options: {
            editable: false,
            colSearch: false,
            colSort: true,
            resizable: true,
            movable: false
          },
          footerTotal: '0'
        });
      });

      this.aggTableColumnDef = this.libServ.deepCopy(this.metricColDef);
      this.aggTableJson['columns'] = this.aggTableColumnDef.slice(
        grpBys.length
      );

      this.getFooters(grpBys);

      // this.aggTableJson['selectedColumns'] = this.aggTableColumnDef.slice(
      //   grpBys.length
      // );

      this.aggTableJson['selectedColumns'] = this.aggTableColumnDef.slice(
        grpBys.length
      );
      this.aggTableJson['frozenCols'] = [
        ...this.aggTableColumnDef.slice(0, grpBys.length)
      ];
      this.aggTableJson['frozenWidth'] =
        this.aggTableColumnDef
          .slice(0, grpBys.length)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px';

      this.reloadAggTable();

      const dims = Array.from(new Set(data1['data'].map(s => s[grpBys[0]])));

      const sortedCols = this.aggTableColumnDef.slice(grpBys.length);

      const arr = [];
      console.log('dddd', dims, this.aggTableColumnDef)
      dims.forEach(col1 => {
        let obj = {};

        if (grpBys.length == 1) {
          obj = {
            data: {}
          };
        } else {
          obj = {
            data: {},
            children: [{ data: {} }]
          };
        }

        const str = data1['data'].filter(x => x[grpBys[0]] == col1);

        grpBys.forEach((grp, index) => {
          if (index === 0) {
            obj['data'][grp] = str[0][grp];
          } else {
            obj['data'][grp] = 'All';
          }
        });

        str.forEach((s1, i) => {
          sortedCols.forEach((sortedCols1, index1) => {
            if (parseInt(sortedCols1.field, 10) === s1['accounting_key']) {
              obj['data'][parseInt(sortedCols1.field, 10)] =
                s1['unfilled_impressions'];
            }
          });
        });

        arr.push(obj);
      });

      this.aggTableData = <TreeNode[]>arr;
      this.aggTableJson['totalRecords'] = arr.length;
      this.aggTableJson['loading'] = false;
    });
  }

  getFooters(grpBys) {
    const tableReq1 = {
      dimensions: ['accounting_key'],
      metrics: ['unfilled_impressions'],
      derived_metrics: [],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys('source'),
      orderBy: [{ key: 'accounting_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };

    this.dataFetchServ.getunfilledData(tableReq1).subscribe(data11 => {
      if (data11['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data11['status_msg']);
        return;
      }

      const data1 = data11['data'] as [];
      data1.forEach(d => {
        this.aggTableJson['selectedColumns'].forEach(c => {
          if (parseInt(c['field']) === d['accounting_key']) {
            c['footerTotal'] = d['unfilled_impressions'];
          }
        });
      });
    });
  }

  reloadAggTable() {
    this.displayAggTable = false;
    setTimeout(() => {
      this.displayAggTable = true;
    }, 0);
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['name']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    } else if (type === 'ppt') {
      const chartElements = document.getElementsByTagName('ym-charts');
      const slides = [];
      [].forEach.call(chartElements, (el, index) => {
        const c = el.getElementsByTagName('canvas')[0];

        slides.push({
          title: `${this.appConfig['name']}`,
          base64data: this.libServ.canvasToImage('white', c),
          width: 8,
          height: 3
        });

        if (index === chartElements.length - 1) {
          const pptConfig = {
            filename: 'Charts',
            logo: {
              visible: true,
              path: '/assets/images/ym.png',
              width: 0.53125,
              height: 0.458333
            },
            slides: slides
          };
          this.pptExport.exportPPT(pptConfig);
        }
      });
    }
    return false;
  }

  loadMainLineChart(params) {
    const colors = this.libServ.dynamicColors(2);
    this.mainLineChartJson = {
      chartTypes: [{ key: 'line', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'Impressions',
            type: 'line',
            borderColor: colors[1],
            fill: false,
            backgroundColor: colors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Unfilled Impressions'
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Months'
              }
            }
          ],
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: 'Impressions'
              },
              position: 'left',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, 'number', []);
                }
              }
            }
          ]
        },
        tooltips: {
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
              return `${
                data.datasets[tooltipItem.datasetIndex].label
                } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    this.showMainLineChart = false;
    this.dataFetchServ.getunfilledData(params).subscribe(data => {
      const chartData = data['data'] as [];
      if (typeof chartData === 'undefined' || !chartData.length) {
        this.noDataMainLineChart = true;
      } else {
        const datesArr = Array.from(
          new Set(chartData.map(r => r['accounting_key']))
        );

        this.mainLineChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MMM-YYYY')
        );

        const revArr = [];
        datesArr.forEach(time_key => {
          chartData.forEach(r => {
            if (r['accounting_key'] === time_key) {
              revArr.push(r['unfilled_impressions']);
            }
          });
        });
        this.mainLineChartJson['chartData']['datasets'][0]['data'] = revArr;
        this.showMainLineChart = true;
        this.noDataMainLineChart = false;
      }
    });
  }

  loadMainPieChart(params) {
    if (!params['gidGroupBy'].includes('region')) {
      params['gidGroupBy'].push('region');
    }

    this.mainPieChartJson = {
      chartTypes: [{ key: 'pie', label: 'Pie Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Unfilled Impressions Across Inventory'
        },
        legend: {
          display: false
        },
        elements: {
          arc: {
            borderWidth: 0
          }
        },
        plugins: {
          datalabels: {
            formatter: (value, ctx) => {
              let sum = 0;
              const dataArr = ctx.chart.data.datasets[0].data;
              dataArr.map(data => {
                sum += data;
              });
              const percentage = (value * 100) / sum;
              if (percentage > 10) {
                return `${this.formatNumPipe.transform(value, 'number', [])} `;
              } else {
                return '';
              }
            },
            color: 'black'
          }
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };
    this.mainPieChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];

          return `${
            data.labels[tooltipItem.index]
            } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
        }
      }
    };

    this.showMainPieChart = false;
    this.dataFetchServ.getunfilledData(params).subscribe(data => {
      const chartData = data['data'];
      if (typeof chartData === 'undefined' || !chartData.length) {
        this.noDataPieChart = true;
      } else {
        const sources = Array.from(new Set(chartData.map(s => s['region'])));
        const colors = this.libServ.dynamicColors(sources.length);
        this.mainPieChartJson['chartData']['labels'] = sources;
        this.mainPieChartJson['chartData']['datasets'][0]['data'] = Array.from(
          new Set(chartData.map(s => s['unfilled_impressions']))
        );
        this.mainPieChartJson['chartData']['datasets'][0][
          'backgroundColor'
        ] = colors;
        this.showMainPieChart = true;
        this.noDataPieChart = false;
      }
    });
  }

  chartSelected(data: Event) {
    console.log('Chart Selected', data);
  }

  onTableDrill(e: Event) {
    console.log('Drilled', e);
    if (!e['node']['childLoaded']) {
      this.aggTableJson['loading'] = true;

      if (Object.keys(this.appliedFilters).length === 0) {
        this.appliedFilters['filters'] = { dimensions: [] };
      }

      const tableReq = {
        dimensions: ['accounting_key'], // [this.filtersApplied['groupby'][0]['key'],'accounting_key'],
        metrics: ['unfilled_impressions'],
        derived_metrics: [],
        timeKeyFilter: this.filtersApplied['timeKeyFilter'],
        filters: this.libServ.deepCopy(this.filtersApplied['filters']), // this.appliedFilters["filters"],
        groupByTimeKey: {
          key: ['accounting_key', 'time_key'],
          interval: 'daily'
        },
        gidGroupBy: this.getGrpBys(this.filtersApplied['groupby'][0]['key']),
        orderBy: [{ key: 'accounting_key', opcode: 'asc' }],
        limit: '',
        offset: ''
      };

      const grpBys = this.filtersApplied['groupby'].map(e => e.key);

      for (const g of grpBys) {
        if (e['node']['data'][g] !== 'All') {
          tableReq['dimensions'].push(g);
          if (!tableReq['gidGroupBy'].includes(g)) {
            tableReq['gidGroupBy'].push(g);
          }
          if (
            tableReq['filters']['dimensions'].findIndex(e => e.key === g) === -1
          ) {
            tableReq['filters']['dimensions'].push({
              key: g,
              values: [e['node']['data'][g]]
            });
          } else {
            if (
              tableReq['filters']['dimensions']
                .find(e => e.key === g)
              ['values'].findIndex(v => v === e['node']['data'][g]) === -1
            ) {
              tableReq['filters']['dimensions'].push(e['node']['data'][g]);
            }
          }
        } else {
          tableReq['dimensions'].push(g);
          if (!tableReq['gidGroupBy'].includes(g)) {
            tableReq['gidGroupBy'].push(g);
          }
          break;
        }
      }

      this.dataFetchServ.getunfilledData(tableReq).subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        const childData = data['data'] as [];
        const arr = [];

        const sortedCols = this.aggTableColumnDef.slice(grpBys.length);

        const dims = Array.from(new Set(childData.map(s => s[grpBys[1]])));

        dims.forEach(col1 => {
          const obj = {
            data: {},
            children: [{ data: {} }]
          };

          const str = childData.filter(x => x[grpBys[1]] == col1);
          str.forEach((s1, i) => {
            grpBys.forEach(g => {
              if (str[i][g] === undefined) {
                obj['data'][g] = 'All';
              } else {
                obj['data'][g] = str[i][g];
              }
            });

            sortedCols.forEach((sortedCols1, index1) => {
              if (parseInt(sortedCols1.field) === s1['accounting_key']) {
                obj['data'][parseInt(sortedCols1.field)] =
                  s1['unfilled_impressions'];
              }

              // obj['data'][sortedCols1.field] = s1['unfilled_impressions'];
            });
          });

          arr.push(obj);
        });

        arr.forEach((row: object) => {
          if (row['data'][grpBys[grpBys.length - 1]] !== 'All') {
            delete row['children'];
          }
        });

        this.aggTableJson['loading'] = false;
        e['node']['children'] = <TreeNode[]>arr;
        this.aggTableData = [...this.aggTableData];
        e['node']['childLoaded'] = true;
      });
    }
  }

  getGrpBys(key) {
    // let grpBys = this.filtersApplied['groupby'].map(e => e.key);
    let grpBys = [key];
    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          grpBys.concat(
            this.filtersApplied['filters']['dimensions']
              .filter(f => f.values.length)
              .map(m => m.key)
          )
        )
      );
    }
    return grpBys;
  }

  onLazyLoadAggTable(e: Event) {
    console.log('Lazy Agg', e);
  }

  resetPagination(tt1) {
    if (typeof tt1 !== 'undefined') {
      tt1.reset();
    }
  }

  openPopup(row, field) {
    const grpBys = [];
    const filtersApplied = this.libServ.deepCopy(this.filtersApplied);
    let str = '';

    for (const grp of this.filtersApplied['groupby']) {
      if (grp['key'] === field) {
        grpBys.push(grp);
        filtersApplied['filters']['dimensions'].push({
          key: grp['key'],
          values: [row[grp['key']]]
        });
        const keyFormat = grp['key'].replace(/(^|_)(\w)/g, function (
          $0,
          $1,
          $2
        ) {
          return ($1 && ' ') + $2.toUpperCase();
        });
        if (str === '') {
          str = str + ' ' + keyFormat + ' : ' + row[grp['key']];
        } else {
          str = str + ' > ' + keyFormat + ' : ' + row[grp['key']];
        }

        break;
      } else {
        grpBys.push(grp);
        filtersApplied['filters']['dimensions'].push({
          key: grp['key'],
          values: [row[grp['key']]]
        });
        const keyFormat = grp['key'].replace(/(^|_)(\w)/g, function (
          $0,
          $1,
          $2
        ) {
          return ($1 && ' ') + $2.toUpperCase();
        });
        if (str === '') {
          str = str + ' ' + keyFormat + ' : ' + row[grp['key']];
        } else {
          str = str + ' > ' + keyFormat + ' : ' + row[grp['key']];
        }
      }
    }

    filtersApplied['groupby'] = grpBys;
    const data = {
      filters: filtersApplied
    };

    const ref = this.dialogService.open(UnfilledDataPopupComponent, {
      header: str + ' Unfilled Impressions',
      contentStyle: { 'max-height': '80vh', width: '80vw', overflow: 'auto' },
      data: data
    });
    ref.onClose.subscribe((data1: string) => { });
  }

  async exportTable(table, fileFormat) {
    if (this.aggTableData.length == 0) {
      this.confirmationService.confirm({
        message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
        header: 'Information',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {

        },
        reject: () => {
        }
      });
    } else {

      if (this.exportRequest['sendEmail'].length > 0) {
        this.toastService.displayToast({
          severity: 'info',
          summary: 'Export Report',
          detail:
            'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
          life: 10000
        });
        let grpBys = this.filtersApplied['groupby'].map(e => e.key);
        if (
          this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
          this.filtersApplied['filters']['dimensions'].length === 0
        ) {
          this.filtersApplied['filters']['dimensions'] = [];
        } else {
          grpBys = Array.from(
            new Set(
              this.filtersApplied['filters']['dimensions']
                .filter(f => f.values.length)
                .map(m => m.key)
                .concat(grpBys)
            )
          );
        }

        const colDef = this.libServ.deepCopy(this.aggTableColumnDef);
        // grpBys.push('time_key');
        const tableReq = {
          dimensions: [
            this.filtersApplied['groupby'][0]['key'],
            'accounting_key'
          ],
          metrics: ['unfilled_impressions'],
          derived_metrics: [],
          timeKeyFilter: this.filtersApplied['timeKeyFilter'],
          filters: this.filtersApplied['filters'],
          groupByTimeKey: {
            key: ['accounting_key', 'time_key'],
            interval: 'daily'
          },
          gidGroupBy: grpBys,
          orderBy: [
            { key: this.filtersApplied['groupby'][0]['key'], opcode: 'asc' }
          ], // [{ key: 'accounting_key', opcode: 'asc' }],
          limit: '',
          offset: ''
        };

        let temp = 0;
        const data1 = [];
        const sheetDetailsarray = [];
        while (temp < grpBys.length) {
          const grpbyExport = [];
          for (let i = 0; i <= temp; i++) {
            grpbyExport.push(grpBys[i]);
            tableReq['orderBy'] = [{ key: grpBys[0], opcode: 'asc' }];
            tableReq['dimensions'] = grpbyExport;
          }
          // tableReq['isTable'] = false;

          tableReq['dimensions'].push('accounting_key');
          await this.dataFetchServ
            .getunfilledData(tableReq)
            .toPromise()
            .then(success => {
              if (success['status'] === 0) {
                this.toastService.displayToast({
                  severity: 'error',
                  summary: 'Server Error',
                  detail: 'Please refresh the page'
                });
                console.log(success['status_msg']);
                return;
              }

              let grpByColumnDef = [];
              const colDefOptions = this.aggTableColumnDef;
              let i = 0;
              let sheetName = '';

              while (i < grpbyExport.length) {
                let displayName = '';
                if (grpbyExport[i] === 'region') {
                  displayName = 'Inventory Types';
                  if (i === 0) {
                    sheetName = 'Inventory Types';
                  } else {
                    sheetName = '+Inventory';
                  }
                } else if (grpbyExport[i] === 'station') {
                  displayName = 'Station';
                  if (i === 0) {
                    sheetName = 'Station';
                  } else {
                    sheetName = '+Station';
                  }
                } else if (grpbyExport[i] === 'creative_size') {
                  displayName = 'Creative';
                  if (i === 0) {
                    sheetName = 'Creative';
                  } else {
                    sheetName = '+Creative';
                  }
                }

                grpByColumnDef.push({
                  field: grpbyExport[i].toString(),
                  displayName: displayName,
                  visible: true,
                  width: 150,
                  format: 'string',
                  exportConfig: {
                    format: 'string',
                    styleinfo: {
                      thead: 'default',
                      tdata: 'white'
                    }
                  }
                });

                i++;
              }

              const childData = success['data'] as [];
              const arr = [];

              const sortedCols = this.aggTableColumnDef.slice(grpBys.length);
              grpByColumnDef = grpByColumnDef.concat(sortedCols);
              const dims = Array.from(
                new Set(childData.map(s => s[grpBys[temp]]))
              );

              dims.forEach(col1 => {
                const str = childData.filter(x => x[grpBys[temp]] == col1);
                let obj = {};

                let c = 0;
                str.forEach((s1, i) => {
                  grpBys.forEach(g => {
                    if (str[i][g] === undefined) {
                    } else {
                      obj[g] = str[i][g];
                    }
                  });

                  sortedCols.forEach((sortedCols1, index1) => {
                    if (parseInt(sortedCols1.field) === s1['accounting_key']) {
                      obj[parseInt(sortedCols1.field)] =
                        s1['unfilled_impressions'];
                      c++;
                    }
                  });

                  if (sortedCols.length === c) {
                    arr.push(obj);
                    c = 0;
                    obj = {};
                  }
                });
              });

              grpByColumnDef.splice(
                grpByColumnDef.findIndex(
                  item => item['field'] === 'accounting_key'
                ),
                1
              );
              const totalObj = {};
              grpByColumnDef.forEach(ele => {
                if ('footerTotal' in ele) {
                  totalObj[ele.field] = ele.footerTotal;
                }
              });
              totalObj[grpByColumnDef[0].field] = 'Total';
              arr.push(totalObj);
              const sheetDetails = {};
              sheetDetails['columnDef'] = this.libServ.deepCopy(grpByColumnDef);
              sheetDetails['data'] = arr;
              sheetDetails['sheetName'] = sheetName;
              sheetDetails['isRequest'] = false;
              sheetDetails['request'] = {
                url: '',
                method: '',
                param: {}
              };
              sheetDetails['disclaimer'] = [
                {
                  position: 'bottom',
                  label: 'Note: Data present in the table may vary over a period of time.',
                  color: '#000000'
                }
              ];
              sheetDetails['totalFooter'] = {
                available: true,
                custom: true
              };
              sheetDetails['tableTitle'] = {
                available: false,
                label: 'Aggregated Data By ' + sheetName.replace('+', '')
              };
              sheetDetails['image'] = [
                {
                  available: true,
                  path: environment.exportConfig.exportLogo,
                  position: 'top'
                }
              ];

              sheetDetailsarray.push(sheetDetails);
              this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
              //   data1.push({
              //     data: [
              //       {
              //         data: arr,
              //         columnDefs: grpByColumnDef
              //       }
              //     ],

              //     sheetname: sheetName
            });
          // });

          temp++;
        }

        this.exportRequest['fileName'] =
          'Unfilled Inventory Data ' +
          moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
        this.exportRequest['exportFormat'] = fileFormat;
        this.exportRequest['exportConfig'] = environment.exportConfig;
        this.dataFetchServ
          .getExportReportData(this.exportRequest)
          .subscribe(response => {
            console.log(response);
          });
      } else {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Export Report',
          detail:
            'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email.Thanks!',
          life: 10000
        });
      }
    }
  }

  isHiddenColumn(col: Object) {
    return (
      this.aggTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }

  /* --------------------- On Filter Apply Button Clicked --------------------- */

  onFiltersApplied(filterData: object) {
    this.filtersApplied['filters']['dimensions'] = [];

    // tslint:disable-next-line: forin
    for (const k in filterData['filter']['dimensions']) {
      if (filterData['filter']['dimensions'][k].length !== 0) {
        this.filtersApplied['filters']['dimensions'].push({
          key: k,
          values: filterData['filter']['dimensions'][k]
        });
      }
    }
    this.appliedFilters = this.libServ.deepCopy(this.filtersApplied);

    this.filtersApplied['groupby'] = filterData['groupby'];
    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];

    const mainLineChartReq = {
      dimensions: ['accounting_key'],
      metrics: ['unfilled_impressions'],
      derived_metrics: [],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.filtersApplied['filters'],
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },

      gidGroupBy: this.getGrpBys('source'),

      orderBy: [{ key: 'accounting_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };

    const mainPieChartReq = {
      dimensions: ['region'],
      metrics: ['unfilled_impressions'],
      derived_metrics: [],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.filtersApplied['filters'],
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys('region'),
      orderBy: [{ key: 'unfilled_impressions', opcode: 'desc' }],
      limit: '',
      offset: ''
    };

    this.loadMainPieChart(mainPieChartReq);

    this.aggTableColumnDef = [];
    this.aggTableJson['columns'] = [];
    this.metricColDef = [];
    this.loadMainLineChart(mainLineChartReq);
    this.tableReq = {
      dimensions: [this.filtersApplied['groupby'][0]['key'], 'accounting_key'],
      metrics: ['unfilled_impressions'],
      derived_metrics: [],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys(this.filtersApplied['groupby'][0]['key']), // grpBys,
      orderBy: [
        { key: 'accounting_key', opcode: 'desc' },
        { key: this.filtersApplied['groupby'][0]['key'], opcode: 'asc' }
      ],
      limit: '',
      offset: ''
    };
    this.loadTableData(this.tableReq);

  }

  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }

  sendEmail() {
    const imageConfig = {
      imgName: `${this.appConfig['name']}.png`,
      type: 'image/png'
    };

    this.htmltoimage
      .canvasImage(document.getElementById('capture'), imageConfig)
      .then(canvas => {
        this.dialogService.open(SendEmailComponent, {
          header: 'Email Screenshot',
          contentStyle: {
            'max-height': '80vh',
            width: '30vw',
            overflow: 'auto'
          },
          data: canvas.toDataURL('image/png')
        });
      });
  }

  onGlobalSearchChanged(searchValue) {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }

    this.timeout = setTimeout(() => {
      this.tableReq['filters']['globalSearch'] = {
        dimensions: [],
        value: searchValue
      };
      this.tableReq['filters']['globalSearch']['dimensions'].push(
        this.filtersApplied['groupby'][0]['key'], "unfilled_impressions"
      );
      // });
      this.loadTableData(this.tableReq);
    }, 3000);
  }

}
