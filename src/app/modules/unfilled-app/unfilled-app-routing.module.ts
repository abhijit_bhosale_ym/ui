import { Routes } from '@angular/router';
import { UnfilledAppComponent } from './unfilled-app.component';

export const UnfilledAppRoutes: Routes = [
  {
    path: '',
    component: UnfilledAppComponent
  }
];
