import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnfilledDataPopupComponent } from './unfilled-daily-data-popup.component';

describe('Rev360DailyDataPopupComponent', () => {
  let component: UnfilledDataPopupComponent;
  let fixture: ComponentFixture<UnfilledDataPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UnfilledDataPopupComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnfilledDataPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
