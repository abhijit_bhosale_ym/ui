import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectButtonModule } from 'primeng/selectbutton';
import { ButtonModule } from 'primeng/button';
import { TabViewModule } from 'primeng/tabview';
import { TreeTableModule } from 'primeng/treetable';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MultiSelectModule } from 'primeng/multiselect';

import { SharedModule } from 'src/app/_pipes/shared.module';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { FilterContainerModule } from '../common/filter-container/filter-container.module';
import { CardsModule } from '../common/cards/cards.module';
import { ChartsModule } from '../common/charts/charts.module';
import { RouterModule } from '@angular/router';
import { ThreePStatementsAppRoutesAppRoutes } from './threep-statements-routing.module';
import { ThreepStatementsComponent } from './threep-statements.component';
import { TableModule } from 'primeng/table';
import { ConfirmDialogModule, ConfirmationService } from 'primeng';
// import { CostPopupComponent } from './cost-popup/cost-popup.component';

@NgModule({
  declarations: [ThreepStatementsComponent],
  imports: [
    CommonModule,
    SelectButtonModule,
    ButtonModule,
    TabViewModule,
    TreeTableModule,
    FormsModule,
    ReactiveFormsModule,
    MultiSelectModule,
    SharedModule,
    FilterContainerModule,
    CardsModule,
    ChartsModule,
    TableModule,
    ConfirmDialogModule,
    RouterModule.forChild(ThreePStatementsAppRoutesAppRoutes)
  ],
  providers: [FormatNumPipe,ConfirmationService]
})
export class ThreePStatementsAppRoutesAppModule { }
