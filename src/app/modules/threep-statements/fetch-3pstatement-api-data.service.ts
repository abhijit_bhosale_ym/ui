import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class Fetch3PstatementApiDataService {
  private BASE_URL: string = environment.baseUrl;

  constructor(private http: HttpClient) {}

  getVastData(params: object) {
    const url = `${this.BASE_URL}api/vast/getData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
  }

  getLastUpdatedData(appId) {
    const url = `${this.BASE_URL}api/prebid-data/getLastUpdatedData/${appId}`;
    return this.http.get(url, { responseType: 'text' });
  }
  getExportReportData(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/common/exportData`;
    if (!('isTable' in params)) {
      params = Object.assign(params, { isTable: true });
    }
    return this.http.post(url, params);
    // return <TreeNode[]> json.data;
  }

  getTableData(req) {
    const url = `${this.BASE_URL}/frankly/v1/3p-statements/getData`;
    return this.http.post(url, req);
  }

  getFooterData(req) {
    const url = `${this.BASE_URL}/frankly/v1/3p-statements/getFooter`;
    return this.http.post(url, req);
  }

  getInvoiceData(req) {
    const url = `${this.BASE_URL}/frankly/v1/3p-statements/getInvoice`;
    return this.http.post(url, req);
  }

  DeleteInvoice(id) {
    const url = `${this.BASE_URL}/frankly/v1/3p-statements/DeleteInvoice/${id}`;
    return this.http.post(url, id);
  }

  DownloadInvoice(req) {
    const url = `${this.BASE_URL}/frankly/v1/3p-statements/downloadInvoice`;
    return this.http.post(url, req);
  }

  updateData(req) {
    const url = `${this.BASE_URL}/frankly/v1/3p-statements/UpdateData`;
    return this.http.post(url, req);
  }

  addInvoice(req, hi) {
    const url = `${this.BASE_URL}/frankly/v1/3p-statements/addInvoice`;
    return this.http.post(url, req);
  }

  getMTDLink(date) {
    return this.http.get(
      `${this.BASE_URL}/frankly/v1/3p-statements/getMTDLink/${date}`
    );
  }

  updateFileLink(req) {
    const url = `${this.BASE_URL}/frankly/v1/3p-statements/updateFileLink`;
    return this.http.post(url, req);
  }

  downloadFile(req) {
    const url = `${this.BASE_URL}/frankly/v1/3p-statements/downloadReportFile`;
    return this.http.post(url, req);
  }
}
