import { Component, OnInit } from '@angular/core';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { Fetch3PstatementApiDataService } from '../fetch-3pstatement-api-data.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { DynamicDialogConfig, DynamicDialogRef, DialogService } from 'primeng/dynamicdialog';

@Component({
  selector: 'ym-file-link-edit-popup',
  templateUrl: './file-link-edit-popup.component.html',
  styleUrls: ['./file-link-edit-popup.component.scss']
})
export class FileLinkEditPopupComponent implements OnInit {
  rowData: any = {};
  fileLink: any;
  invalid_File = false;

  constructor(
    private dataFetchServ: Fetch3PstatementApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private config: DynamicDialogConfig,
    public ref: DynamicDialogRef
  ) {
    this.rowData = this.config.data;
  }

  ngOnInit() {
    this.fileLink = this.rowData['value'];
  }

  CloseDialog() {
    this.dialogService.dialogComponentRef.destroy();
  }

  saveFileLink() {
    const req = {
      id: this.rowData['id'].toString(),
      value: this.fileLink,
      field: this.rowData['field']
    };

    this.dataFetchServ.updateFileLink(req).subscribe(data => {
      if (data['status'] === 'file not exist') {
        this.invalid_File = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Invalid',
          detail: 'Invalid File Link'
        });
        console.log(data['status_msg']);
        return;
      } else if (data['status'] === 'false') {
        this.invalid_File = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      } else {
        this.toastService.displayToast({
          severity: 'success',
          summary: 'Update',
          detail: 'Link Updated Successfuly !'
        });
        const data = {
          isTable: true
        }
        this.ref.close(data);

      }
    });
  }

  saveMTDLink() {
    const req = {
      accounting_key: this.rowData['id'],
      value: this.fileLink,
      field: this.rowData['field']
    };

    this.dataFetchServ.updateFileLink(req).subscribe(data => {
      if (data['status'] == 'file not exist') {
        this.invalid_File = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Invalid',
          detail: 'Invalid File Link'
        });
        console.log(data['status_msg']);
        return;
      } else if (data['status'] == 'false') {
        this.invalid_File = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      } else {
        this.toastService.displayToast({
          severity: 'success',
          summary: 'Update',
          detail: 'MTD Link Updated Successfuly !'
        });
        const data = {
          isTable: false
        }
        this.ref.close(data);
      }
    });
  }

}
