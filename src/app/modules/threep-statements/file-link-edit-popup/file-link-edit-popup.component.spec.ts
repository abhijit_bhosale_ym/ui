import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileLinkEditPopupComponent } from './file-link-edit-popup.component';

describe('FileLinkEditPopupComponent', () => {
  let component: FileLinkEditPopupComponent;
  let fixture: ComponentFixture<FileLinkEditPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FileLinkEditPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileLinkEditPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
