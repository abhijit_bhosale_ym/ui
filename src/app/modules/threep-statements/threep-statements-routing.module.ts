import { Routes } from '@angular/router';
import { ThreepStatementsComponent } from './threep-statements.component';

export const ThreePStatementsAppRoutesAppRoutes: Routes = [
  {
    path: '',
    component: ThreepStatementsComponent
  }
];
