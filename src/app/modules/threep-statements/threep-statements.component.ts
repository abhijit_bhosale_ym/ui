import { Component, OnInit, OnDestroy } from '@angular/core';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { CommonLibService, DataShareService } from 'src/app/_services';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';
import { Fetch3PstatementApiDataService } from './fetch-3pstatement-api-data.service';
// import { UnfilledDataPopupComponent } from "./unfilled-popup/unfilled-daily-data-popup.component";
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { groupBy, filter } from 'rxjs/operators';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { Subscription } from 'rxjs';
import { log } from 'util';
import { CostPopupComponent } from './cost-popup/cost-popup.component';

import { SummaryResolver } from '@angular/compiler';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { FileLinkEditPopupComponent } from './file-link-edit-popup/file-link-edit-popup.component'
import { DialogService } from 'primeng/dynamicdialog';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-threep-statements',
  templateUrl: './threep-statements.component.html',
  styleUrls: ['./threep-statements.component.scss']
})
export class ThreepStatementsComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;

  appConfig: object = {};
  aggTableData: TreeNode[];
  dimColDef: any[];
  metricColDef: any[];
  noTableData = false;
  aggTableColumnDef: any[];
  sources: any[];
  aggTableJson: Object;
  displayAggTable = false;
  appliedFilters: object = {};
  filtersApplied: object = {};
  start_date: any;
  startD: any;
  sources_data: any;
  updateFee = false;
  updateAmount = false;
  updateMTDLink = false;
  updateRawReport = false;
  updateRevCalculationDoc = false;
  mtdLink: any;
  exportRequest: ExportRequest = <ExportRequest>{};

  /* ---------------------------------- Table --------------------------------- */

  flatTableReq: object;
  flatTableReq1: object;
  isExportReport = false;
  flatTableData: TreeNode[];
  flatTableColumnDef: any[];
  flatTableJson: Object;
  Total_Fee: any;
  Total_invoice: any;
  editRevCalLink = false;

  selectedChartName;
  rev_calculation_doc: any;
  row_report: any;

  /* ---------------------------------- Table --------------------------------- */

  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: Fetch3PstatementApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private formatNumPipe: FormatNumPipe,
    private exportService: ExportdataService,
    private pptExport: ExportPptService,
    private htmltoimage: HtmltoimageService,
    private confirmationService: ConfirmationService// private exportService: ExportdataService
  ) { }

  ngOnInit() {
    this.metricColDef = [];

    this.flatTableColumnDef = [
      {
        field: 'accounting_key',
        displayName: 'Month',
        format: '',
        formatConfig: ['MMMM YYYY'],
        width: '150',
        exportConfig: {
          format: 'date<<MMMM-YY<<-',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },

        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: 'Total'
      },
      {
        field: 'source',
        displayName: '3P Source',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          },
          footerTotal: ''
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'Fee',
        displayName: '3P fee',
        format: '$',
        width: '150',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: ''
      },
      {
        field: 'adjusted_invoice',
        displayName: '3P Statement Amount',
        format: '$',
        width: '150',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: true,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: ''
      },
      {
        field: 'statement',
        displayName: 'Statements',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: ''
      },
      {
        field: 'raw_report_link',
        displayName: 'Raw Report',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: ''
      },
      {
        field: 'rev_calculation_doc_link',
        displayName: 'Rev Calculation Doc',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        },
        footerTotal: ''
      }
    ];

    this.flatTableJson = {
      page_size: 10,
      page: 1,
      lazy: false,
      loading: true,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '300px',
      totalRecords: 1000,
      // columns: this.flatTableColumnDef.slice(1),
      // selectedColumns: this.flatTableColumnDef.slice(1),
      columns: this.libServ.deepCopy(this.flatTableColumnDef),
      selectedColumns: this.libServ.deepCopy(this.flatTableColumnDef),
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: false,
      overallSearch: true,
      columnSearch: false
    };


    (this.sources_data = ''),
      (this.appConfigObs = this.appConfigService.appConfig.subscribe(
        appConfig => {
          if (!this.libServ.isEmptyObj(appConfig)) {
            this.appConfig = appConfig;
            console.log('appConfig', this.appConfig);
            const permission_arr = this.appConfig['permissions'];
            for (let i = 0; i < permission_arr.length; i++) {
              if (
                permission_arr[i].name == '3p-update-fees' &&
                permission_arr[i].permissionType == 'Update'
              ) {
                this.updateFee = true;
              }

              if (
                permission_arr[i].name == '3p-update-amount' &&
                permission_arr[i].permissionType == 'Update'
              ) {
                this.updateAmount = true;
              }

              if (
                permission_arr[i].name == '3p-update-mtd-link' &&
                permission_arr[i].permissionType == 'Update'
              ) {
                this.updateMTDLink = true;
              }

              if (
                permission_arr[i].name == '3p-update-raw-report' &&
                permission_arr[i].permissionType == 'Update'
              ) {
                this.updateRawReport = true;
              }

              if (
                permission_arr[i].name == '3p-update-rev-calculation-doc' &&
                permission_arr[i].permissionType == 'Update'
              ) {
                this.updateRevCalculationDoc = true;
              }
            }

            this.isExportReport = this.appConfig['permissions'].some(
              o => o.name === '3p-export-report'
            );
            this._titleService.setTitle(this.appConfig['displayName']);
            let startDate;
            if (
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['startOf']
            ) {
              startDate = moment()
                .subtract(
                  this.appConfig['filter']['filterConfig']['filters'][
                  'datePeriod'
                  ][0]['defaultDate'][0]['value'],
                  this.appConfig['filter']['filterConfig']['filters'][
                  'datePeriod'
                  ][0]['defaultDate'][0]['period']
                )
                .startOf(
                  this.appConfig['filter']['filterConfig']['filters'][
                  'datePeriod'
                  ][0]['defaultDate'][0]['period']
                );
            } else {
              startDate = moment().subtract(
                this.appConfig['filter']['filterConfig']['filters'][
                'datePeriod'
                ][0]['defaultDate'][0]['value'],
                this.appConfig['filter']['filterConfig']['filters'][
                'datePeriod'
                ][0]['defaultDate'][0]['period']
              );
              AppConfigService;
            }
            const endDate = moment().subtract(
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][1]['value'],
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][1]['period']
            );

            this.filtersApplied = {
              timeKeyFilter: {
                time_key1: startDate.format('YYYYMMDD')
                // time_key2: endDate.format("YYYYMMDD")
              },
              filters: { dimensions: [], metrics: [] },
              groupby: this.appConfig['filter']['filterConfig'][
                'groupBy'
              ].filter(v => v.selected)
            };
            this.exportRequest['appName'] = this.appConfig['name'].toString();
            this.exportRequest['sendEmail'] = this.appConfig['user'][
              'contactEmail'
            ].split(',');
            this.start_date = startDate.format('YYYYMMDD');
            this.initialLoading();
            this.getMTDLink();
          }
        }
      ));
  }

  initialLoading() {

    this.startD = moment(this.start_date, 'YYYYMMDD').format('MMMM-YYYY');
    this.flatTableReq = {
      startDate: this.filtersApplied['timeKeyFilter']['time_key1'],
      source: this.sources_data.toString()
    };

    this.flatTableJson['loading'] = true;
    this.dataFetchServ.getTableData(this.flatTableReq).subscribe(data => {
      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      } else {
        this.noTableData = false;
      }

      this.flatTableJson['loading'] = false;

      const data2 = data as [];
      data2.forEach((o: object) => {
        const tk = o['accounting_key'];
        delete o['accounting_key'];
        o['accounting_key'] = moment(tk, 'YYYYMMDD').format('MMMM-YYYY');
      });

      const arr = [];
      for (const r of data2) {
        const obj = {};
        obj['data'] = r;
        arr.push(obj);
      }

      this.flatTableData = <TreeNode[]>arr;
      this.flatTableJson['totalRecords'] = data2.length;
      this.flatTableJson['loading'] = false;
      this.getFooter();
    });
  }

  getMTDLink() {

    this.dataFetchServ.getMTDLink(this.filtersApplied['timeKeyFilter']['time_key1']).subscribe(data => {
      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }
      this.mtdLink = data[0]['MTDLink'];
    })
  }

  onEdit = (e, id, val, field) => {
    if (e.keyCode === 13) {
      if (val != undefined) {
        this.flatTableReq = {
          id: id,
          field: field,
          value: val
        };

        this.dataFetchServ.updateData(this.flatTableReq).subscribe(data => {
          this.initialLoading();
        });
      }
    }
  }

  getFooter() {
    if (this.sources_data == undefined) {
      this.sources_data = '';
    }
    this.flatTableReq1 = {
      startDate: this.filtersApplied['timeKeyFilter']['time_key1'],
      source: this.sources_data.toString()
    };
    this.dataFetchServ.getFooterData(this.flatTableReq1).subscribe(data1 => {
      if (data1['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data1['status_msg']);
        return;
      } else {
        this.noTableData = false;
      }
      if (data1 == '') {
        this.Total_Fee = '';
        this.Total_invoice = '';
      } else {
        this.Total_Fee = data1[0]['fee'];
        this.Total_invoice = data1[0]['invoice'];
      }
      const datas = data1 as [];

      datas.forEach(d => {
        this.flatTableColumnDef.forEach(c => {
          if (c['field'] === 'Fee') {
            c['footerTotal'] = this.Total_Fee;
          } else if (c['field'] === 'adjusted_invoice') {
            c['footerTotal'] = this.Total_invoice;
          }
        });
      });
    });
  }

  onFiltersApplied(filterData: object) {
    this.start_date = filterData['date'][0];
    this.sources_data = filterData['filter']['dimensions']['source'];
    this.startD = moment(this.start_date, 'YYYYMMDD').format('MMMM-YYYY');
    this.filtersApplied['timeKeyFilter']['time_key1'] = this.start_date;
    if (this.sources_data == undefined) {
      this.sources_data = '';
    }

    this.flatTableReq = {
      startDate: this.start_date,
      source: this.sources_data.toString()
    };
    this.flatTableJson['loading'] = true;
    this.dataFetchServ.getTableData(this.flatTableReq).subscribe(data => {
      const data2 = data as [];
      data2.forEach((o: object) => {
        const tk = o['accounting_key'];
        delete o['accounting_key'];
        o['accounting_key'] = moment(tk, 'YYYYMMDD').format('MMMM-YYYY');
      });

      const arr = [];
      for (const r of data2) {
        const obj = {};
        obj['data'] = r;
        arr.push(obj);
      }

      this.flatTableData = <TreeNode[]>arr;
      this.flatTableJson['totalRecords'] = data2.length;
      this.flatTableJson['loading'] = false;
      this.getFooter();
    });
    this.getMTDLink();
  }

  isHiddenColumn(col: Object) {
    return (
      this.flatTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.flatTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }

  exportTable(table, fileFormat) {
    console.log('col', this.flatTableColumnDef);
    let end_Date = moment(this.start_date).add(1, 'months').subtract(1, 'days').format('YYYYMMDD');
    if (this.flatTableData.length == 0) {
      this.confirmationService.confirm({
        message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
        header: 'Information',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
        },
        reject: () => {
        }
      });
    } else {
      if (this.exportRequest['sendEmail'].length > 0) {
        this.toastService.displayToast({
          severity: 'info',
          summary: 'Export Report',
          detail:
            'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
          life: 10000
        });
        if (table === 'flat') {
          const columnDefs = this.libServ.deepCopy(
            this.flatTableColumnDef.slice(0, 4)
          );
          const req = this.libServ.deepCopy(this.flatTableReq);
          req['limit'] = ''; // Max value of Int MySql
          req['offset'] = '';
          req['orderBy'] = '';
          req['isTable'] = true;
          this.dataFetchServ.getTableData(req).subscribe(response => {
            let data = [];
            data = response as [];
            // data = res;
            if (this.sources_data == undefined) {
              this.sources_data = '';
            }
            this.flatTableReq1 = {
              startDate: this.start_date,
              source: this.sources_data.toString()
            };
            this.dataFetchServ
              .getFooterData(this.flatTableReq1)
              .subscribe(data1 => {
                data.push({
                  Fee: data1[0]['fee'],
                  adjusted_invoice: data1[0]['invoice'],
                  accounting_key: 'Total'
                });

                const sheetDetails = {};
                sheetDetails['columnDef'] = columnDefs;
                sheetDetails['data'] = data;
                sheetDetails['sheetName'] = 'Data';
                sheetDetails['isRequest'] = false;
                sheetDetails['request'] = {
                  url: '',
                  method: '',
                  param: { timeKeyFilter: { time_key1: this.start_date, time_key2: end_Date } }
                };
                sheetDetails['disclaimer'] = [
                  {
                    position: 'bottom',
                    label: 'Note: Data present in the table may vary over a period of time.',
                    color: '#000000'
                  }
                ];
                sheetDetails['totalFooter'] = {
                  available: true,
                  custom: false
                };
                sheetDetails['tableTitle'] = {
                  available: false,
                  label: '3P Statements'
                };
                sheetDetails['image'] = [
                  {
                    available: true,
                    path: environment.exportConfig.exportLogo,
                    position: 'top'
                  }
                ];
                const sheetDetailsarray = [];
                sheetDetailsarray.push(sheetDetails);
                this.exportRequest['sheetDetails'] = <SheetDetails[]>(
                  sheetDetailsarray
                );
                this.exportRequest['fileName'] =
                  '3P Statements ' +
                  moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
                this.exportRequest['exportFormat'] = fileFormat;
                this.exportRequest['exportConfig'] = environment.exportConfig;
                // console.log('exportreport', this.exportRequest);
                // return false;
                this.dataFetchServ
                  .getExportReportData(this.exportRequest)
                  .subscribe(response => {
                    console.log(response);
                  });
              });
          });
        }
      } else {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Export Report',
          detail:
            'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
          life: 10000
        });
      }
    }
    return false;
  }

  // List<AppPermission> alp=principal.getUserRoleTeamApp().getAppPermissions();
  // //		Permission p=new Permission();
  // //		alp=principal.getUserRoleTeamApp().getAppPermissions().get(10).getPermissions();
  //       ListIterator itr=alp.listIterator();
  //       while(itr.hasNext())
  //       {
  //          //Object student=itr.next();
  //         AppPermission perm=(AppPermission) itr.next();
  //          System.out.println("Perm Name : "+perm.getPermissions().get(0).getName());

  //       }

  openPopup(row) {
    const data = {
      costid: row.id,
      userId: this.appConfig['user']['id']
    };
    const ref = this.dialogService.open(CostPopupComponent, {
      header: 'Statements',
      contentStyle: { 'max-height': '80vh', width: '80vw', overflow: 'auto' },
      data: data
    });
    ref.onClose.subscribe((data1: string) => { });
  }

  resetPagination(tt1) {
    if (typeof tt1 !== 'undefined') {
      tt1.reset();
    }
  }
  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }

  openRevCalculationDoc(rowData) {
    window.open(rowData, "_blank");
  }

  downloadMTDFile() {
    const req = {
      fileLink: this.mtdLink
    };
    this.dataFetchServ.downloadFile(req).subscribe(data => {
      if (data['status'] === false) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }
      console.log("download file data===", data['data']);
      const fileName = data['fileName'];
      const mimeType = fileName.split('.').pop();
      const file = new Blob([this.base64ToArrayBuffer(data['data'])], {
        type: mimeType
      });
      saveAs(file, fileName);
    })

  }

  downloadRawReport(rowData) {
    const req = {
      fileLink: rowData
    };
    this.dataFetchServ.downloadFile(req).subscribe(data => {
      if (data['status'] === false) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }
      console.log("download file data===", data['data']);
      const fileName = data['fileName'];
      const mimeType = fileName.split('.').pop();
      const file = new Blob([this.base64ToArrayBuffer(data['data'])], {
        type: mimeType
      });
      saveAs(file, fileName);
    })

  }

  base64ToArrayBuffer = data => {
    const binaryString = window.atob(data);
    const binaryLen = binaryString.length;
    const bytes = new Uint8Array(binaryLen);
    for (let i = 0; i < binaryLen; i++) {
      const ascii = binaryString.charCodeAt(i);
      bytes[i] = ascii;
    }
    return bytes;
  }

  editFileLink(id, value, field) {
    const data = {
      id: id,
      value: value,
      field: field
    };
    const ref = this.dialogService.open(FileLinkEditPopupComponent, {
      header: 'Update',
      contentStyle: { 'max-height': '80vh', width: '60vw', overflow: 'auto' },
      data: data
    });
    ref.onClose.subscribe((data: string) => {
      if (data['isTable'])
        this.initialLoading();
      else
        this.getMTDLink();
    });
  }
}
