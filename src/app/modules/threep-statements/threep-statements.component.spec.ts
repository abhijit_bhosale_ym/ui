import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThreepStatementsComponent } from './threep-statements.component';

describe('ThreepStatementsComponent', () => {
  let component: ThreepStatementsComponent;
  let fixture: ComponentFixture<ThreepStatementsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ThreepStatementsComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThreepStatementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
