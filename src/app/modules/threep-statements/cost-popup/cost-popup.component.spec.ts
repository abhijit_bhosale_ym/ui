import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CostPopupComponent } from './cost-popup.component';

describe('CostPopupComponent', () => {
  let component: CostPopupComponent;
  let fixture: ComponentFixture<CostPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CostPopupComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
