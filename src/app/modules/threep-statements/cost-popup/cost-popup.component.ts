import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef
} from '@angular/core';
import { TreeNode } from 'primeng/api';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { CommonLibService, DataShareService } from 'src/app/_services';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';
import { Fetch3PstatementApiDataService } from '../fetch-3pstatement-api-data.service';
// import { UnfilledDataPopupComponent } from "./unfilled-popup/unfilled-daily-data-popup.component";
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { groupBy, filter } from 'rxjs/operators';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { Subscription } from 'rxjs';
import { log } from 'util';
import { DynamicDialogConfig, DialogService } from 'primeng/dynamicdialog';

@Component({
  selector: 'ym-cost-popup',
  templateUrl: './cost-popup.component.html',
  styleUrls: ['./cost-popup.component.scss']
})
export class CostPopupComponent implements OnInit {
  private appConfigObs: Subscription;

  appConfig: object = {};
  aggTableData: TreeNode[];
  dimColDef: any[];
  metricColDef: any[];
  noTableData = false;
  aggTableColumnDef: any[];
  sources: any[];
  307;
  aggTableJson: Object;
  displayAggTable = false;
  appliedFilters: object = {};
  filtersApplied: object = {};
  start_date: any;
  sources_data: any;
  /* ---------------------------------- Table --------------------------------- */

  flatTableReq: object;
  flatTableReq1: object;

  flatTableData: TreeNode[];
  flatTableColumnDef: any[];
  flatTableJson: Object;
  Total_Fee: any;
  Total_invoice: any;
  costid: any;
  userid: any;
  namefile: any[];
  update: any;
  deleteFile = false;
  uploadFile = false;
  downloadFile = false;
  frameUploader: any;

  selectedChartName;

  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: Fetch3PstatementApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private formatNumPipe: FormatNumPipe,
    private config: DynamicDialogConfig,
    private exportService: ExportdataService
  ) {
    this.costid = this.config.data['costid'];
    this.userid = this.config.data['userId'];
  }

  ngOnInit() {
    this.metricColDef = [];

    this.flatTableColumnDef = [
      {
        field: 'accounting_key',
        displayName: 'Link To File Download',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'source',
        displayName: 'Uploaded On',
        format: '',
        width: '120',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'Fee',
        displayName: 'Delete',
        format: '',
        width: '50',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      }
    ];

    this.flatTableJson = {
      page_size: 10,
      page: 1,
      lazy: true,
      loading: false,
      // export: true,
      // sortMode: "multiple",
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '200px',
      totalRecords: 1000,
      // columns: this.flatTableColumnDef.slice(1),
      // selectedColumns: this.flatTableColumnDef.slice(1),
      columns: this.flatTableColumnDef.slice(0),
      selectedColumns: this.flatTableColumnDef.slice(0),
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: false,
      columnSearch: false
    };

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;

        const permission_arr = this.appConfig['permissions'];
        for (let i = 0; i < permission_arr.length; i++) {
          if (
            permission_arr[i].name == '3p-remove-statement' &&
            permission_arr[i].permissionType == 'Delete'
          ) {
            this.deleteFile = true;
          }

          if (
            permission_arr[i].name == '3p-upload-statement' &&
            permission_arr[i].permissionType == 'Create'
          ) {
            this.uploadFile = true;
          }
          if (
            permission_arr[i].name == '3p-download-statement' &&
            permission_arr[i].permissionType == 'Download'
          ) {
            this.downloadFile = true;
          }
        }
        this._titleService.setTitle(this.appConfig['displayName']);
        let startDate;
        if (
          this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
          'defaultDate'
          ][0]['startOf']
        ) {
          startDate = moment()
            .subtract(
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['value'],
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['period']
            )
            .startOf(
              this.appConfig['filter']['filterConfig']['filters'][
              'datePeriod'
              ][0]['defaultDate'][0]['period']
            );
        } else {
          startDate = moment().subtract(
            this.appConfig['filter']['filterConfig']['filters'][
            'datePeriod'
            ][0]['defaultDate'][0]['value'],
            this.appConfig['filter']['filterConfig']['filters'][
            'datePeriod'
            ][0]['defaultDate'][0]['period']
          );
          AppConfigService;
        }
        const endDate = moment().subtract(
          this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
          'defaultDate'
          ][1]['value'],
          this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0][
          'defaultDate'
          ][1]['period']
        );

        this.filtersApplied = {
          timeKeyFilter: {
            timekey1: moment()
              .subtract(1, 'months')
              .startOf('month')
              .format('YYYYMMDD')
            // this.selectedMonth = moment(timekey).format('MMMM YYYY'),
            // time_key1: startDate.format("YYYYMMDD"),
            // time_key2: endDate.format("YYYYMMDD")
          },
          filters: { dimensions: [], metrics: [] },
          groupby: this.appConfig['filter']['filterConfig']['groupBy'].filter(
            v => v.selected
          )
        };
        this.initialLoading();
      }
    });
  }

  initialLoading() {
    this.flatTableReq = {
      costId: this.costid.toString()
    };

    this.flatTableJson['loading'] = true;
    this.dataFetchServ.getInvoiceData(this.flatTableReq).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      }

      const data2 = data as [];

      const arr = [];
      for (const r of data2) {
        const obj = {};
        obj['data'] = r;
        arr.push(obj);
      }

      this.flatTableData = <TreeNode[]>arr;
      this.flatTableJson['totalRecords'] = 25;
      this.flatTableJson['loading'] = false;
    });
  }

  deleteInvoice(id) {
    this.flatTableJson['loading'] = true;
    this.dataFetchServ.DeleteInvoice(id).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }
      this.initialLoading();
    });
  }

  base64ToArrayBuffer = data => {
    const binaryString = window.atob(data);
    const binaryLen = binaryString.length;
    const bytes = new Uint8Array(binaryLen);
    for (let i = 0; i < binaryLen; i++) {
      const ascii = binaryString.charCodeAt(i);
      bytes[i] = ascii;
    }
    return bytes;
  }

  upload = event => {
    for (let i = 0; i < event.target.files.length; i++) {
      const fd = new FormData();
      fd.append('invoice', event.target.files[i], event.target.files[i].name);
      fd.append('costId', this.costid);
      fd.append('userId', this.userid);

      const invoice = {
        id: 0,
        invoiceUrl: '',
        filename: '',
        costId: this.costid,
        userId: this.userid
      };

      for (const key in invoice) {
        fd.append(key, invoice[key]);
      }

      this.flatTableJson['loading'] = true;
      this.dataFetchServ.addInvoice(fd, '142').subscribe(data => {
        this.initialLoading();
      });
    }
  }

  CloseDialog() {
    this.dialogService.dialogComponentRef.destroy();
  }

  downloadInvoice1(costid, url, filename) {
    this.flatTableReq = {
      costId: costid.toString(),
      Invoice_url: url,
      filename: filename
    };
    this.dataFetchServ.DownloadInvoice(this.flatTableReq).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }
      const invoice = data as [];

      const invoices = data;

      const mimeType = filename.split('.').pop();
      const file = new Blob([this.base64ToArrayBuffer(data[0].contents)], {
        type: mimeType
      });
      saveAs(file, data[0].file);
    });
  }
}
