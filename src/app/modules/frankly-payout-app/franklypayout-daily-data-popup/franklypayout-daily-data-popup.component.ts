import { Component, OnInit, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { FetchApiDataService } from '../../frankly-payout-app/fetch-api-data.service';
import * as moment from 'moment';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { Dimension } from '../../../modules/common/filter-container/models/dimension.model';
import { groupBy } from 'rxjs/operators';
import { FranklyPayoutExportPopupComponent } from '../franklypayout-export-popup/franklypayout-export-popup.component';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { DynamicDialogConfig, DialogService } from 'primeng';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-franklypayout-daily-data-popup',
  templateUrl: './franklypayout-daily-data-popup.component.html',
  styleUrls: ['./franklypayout-daily-data-popup.component.scss'],
  providers: [DialogService]
})
export class FranklyPayoutDailyDataPopupComponent implements OnInit {
  showRevSourcePieChart = false;
  showRevDeviceTypePieChart = false;
  showRevCreativeSizePieChart = false;
  popupPieChartSourceJson: object;
  popupPieChartDeviceTypeJson: object;
  popupPieChartCreativeSizeJson: object;
  isExportReport = false;
  filtersApplied: object;
  tabChangedStationFlag = true;
  tabChangedSourceFlag = false;
  tabChangedChartFlag = false;
  tabChangedGroupByFlag = false;
  groupByDims: any[];
  // selectedGrpBy: Dimension[];
  grpBySelected;
  isPopupGroupbyView = false;
  isPopupChartView = false;
  tableData: TreeNode[];
  tableColumnDef: any[];
  tableJson: Object;
  flatTableStationReq: object;
  flatTableStationData: TreeNode[];
  flatTableStationColumnDef: any[];
  flatTableStationJson: Object;

  flatTableSourceReq: object;
  flatTableSourceData: TreeNode[];
  flatTableSourceColumnDef: any[];
  flatTableSourceJson: Object;

  aggTableData: TreeNode[];
  noTableData = false;
  dimColDef: any[];
  metricColDef: any[];
  aggTableColumnDef: any[];
  aggTableJson: Object;
  displayAggTable = false;
  tableReq; object;
  timeout: any;
  groupBy: object;
  exportRequest: ExportRequest = <ExportRequest>{};

  constructor(
    private libServ: CommonLibService,
    private formatNumPipe: FormatNumPipe,
    private dataFetchServ: FetchApiDataService,
    public config: DynamicDialogConfig,
    private exportService: ExportdataService,
    private toastService: ToastService,
    private dialogService: DialogService,
    private confirmationService: ConfirmationService
  ) {
    this.filtersApplied = this.config.data['filters'];

  }

  ngOnInit() {
    console.log('new', this.config['data'])
    this.isExportReport = this.config.data['isExportReport'];
    this.isPopupGroupbyView = this.config['data']['isPopupGroupbyView'];
    this.isPopupChartView = this.config['data']['isPopupChartView'];
    this.exportRequest = this.config.data['exportRequest'];
    this.flatTableStationColumnDef = [
      {
        field: 'time_key',
        displayName: 'Date',
        format: 'date',
        width: '125',
        value: '',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'station_group',
        displayName: 'Media Group',
        format: '',
        width: '230',
        isExpanded: 'false',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'derived_station_rpt',
        displayName: 'Property',
        format: '',
        width: '150',
        isExpanded: 'false',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'dp_impressions',
        displayName: 'Billable Imps.',
        format: 'number',
        width: '130',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'gross_revenue',
        displayName: 'Gross Revenue',
        width: '140',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'gross_ecpm',
        displayName: 'Gross eCPM',
        width: '125',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'station_revenue_share_distribution',
        displayName: 'Client Net Revenue',
        format: '$',
        width: '160',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'client_net_ecpm',
        displayName: 'Client Net eCPM',
        format: '$',
        width: '140',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'total_expenses',
        displayName: 'Expenses',
        format: '$',
        width: '115',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },

      {
        field: 'client_net_share',
        displayName: 'Client Net Share',
        format: 'percentage',
        width: '140',
        value: '',
        condition: '',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      }
    ];
    this.flatTableStationJson = {
      page_size: 10,
      page: 0,
      lazy: false,
      loading: true,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '300px',
      totalRecords: 1000,
      columns: this.flatTableStationColumnDef.slice(1),
      selectedColumns: this.flatTableStationColumnDef.slice(1),
      frozenCols: [...this.flatTableStationColumnDef.slice(0, 1)],
      frozenWidth:
        this.flatTableStationColumnDef
          .slice(0, 1)
          .reduce((tot, cur) => tot + parseInt(cur.width), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };

    this.flatTableSourceColumnDef = [
      {
        field: 'time_key',
        displayName: 'Date',
        format: 'date',
        width: '125',
        value: '',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'station_group',
        displayName: 'Media Group',
        format: '',
        width: '215',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'derived_station_rpt',
        displayName: 'Property',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'source',
        displayName: 'Source',
        format: '',
        width: '250',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'dp_impressions',
        displayName: 'Billable Imps.',
        format: 'number',
        width: '130',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'gross_revenue',
        displayName: 'Gross Revenue',
        width: '140',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'gross_ecpm',
        displayName: 'Gross eCPM',
        width: '125',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'station_revenue_share_distribution',
        displayName: 'Client Net Revenue',
        format: '$',
        width: '160',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'client_net_ecpm',
        displayName: 'Client Net eCPM',
        format: '$',
        width: '150',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'total_expenses',
        displayName: 'Expenses',
        format: '$',
        width: '110',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },

      {
        field: 'client_net_share',
        displayName: 'Client Net Share',
        format: 'percentage',
        width: '150',
        value: '',
        condition: '',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      }
    ];
    this.flatTableSourceJson = {
      page_size: 10,
      page: 0,
      lazy: false,
      loading: true,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '300px',
      totalRecords: 1000,
      columns: this.flatTableSourceColumnDef.slice(1),
      selectedColumns: this.flatTableSourceColumnDef.slice(1),
      frozenCols: [...this.flatTableSourceColumnDef.slice(0, 1)],
      frozenWidth:
        this.flatTableSourceColumnDef
          .slice(0, 1)
          .reduce((tot, cur) => tot + parseInt(cur.width), 0) + 'px',
      // frozenWidth: '250px',
      // frozenWidth:
      //   this.flatTableStationColumnDef
      //     .slice(0, 1)
      //          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + "px",
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };

    this.dimColDef = [
      {
        field: 'derived_station_rpt',
        displayName: 'Property',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'source',
        displayName: 'Source',
        format: '',
        width: '250',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'derived_device_type1_name',
        displayName: 'Device Type',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'derived_ad_type_name',
        displayName: 'Ad Type',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'creative_size_rpt_name',
        displayName: 'Creative Size',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      }
    ];

    this.metricColDef = [
      {
        field: 'dp_impressions',
        displayName: 'Billable Imps.',
        format: 'number',
        width: '170',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'gross_revenue',
        displayName: 'Gross Revenue',
        width: '155',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'gross_ecpm',
        displayName: 'Gross eCPM',
        width: '155',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'station_revenue_share_distribution',
        displayName: 'Client Net Revenue',
        format: '$',
        width: '195',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'client_net_ecpm',
        displayName: 'Client Net eCPM',
        format: '$',
        width: '165',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'total_expenses',
        displayName: 'Expenses',
        format: '$',
        width: '135',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },

      {
        field: 'client_net_share',
        displayName: 'Client Net Share',
        format: 'percentage',
        width: '175',
        value: '',
        condition: '',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      }
    ];

    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '300px',
      totalRecords: 1000,

      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: false,
      overallSearch: true,
      columnSearch: false
    };

    this.groupByDims = [
      {
        key: 'derived_station_rpt',
        label: 'Property',
        // config_json: '{}',
        selected: true
      },
      {
        key: 'source',
        label: 'Source',
        // config_json: '{}',
        selected: true
      },
      {
        key: 'derived_device_type1_name',
        label: 'Device Type',
        // config_json: '{}'
      },
      {
        key: 'derived_ad_type_name',
        label: 'Ad Type',
        // config_json: '{}'
      },
      {
        key: 'creative_size_rpt_name',
        label: 'Creative Size',
        // config_json: '{}'
      }
    ];
    this.initialLoading();
  }

  selectedGrpBy(e) {
    console.log('eeeee', e)
    if (this.tabChangedGroupByFlag) {
      const grpBys = e.map(e => e.key);
      this.groupBy = e.map(e => e.key);
      this.tableReq = {
        dimensions: [grpBys[0]], // grpBys,0//[this.filtersApplied['groupby'][0]['key']],
        metrics: [
          'dp_impressions',
          'gross_revenue',
          'total_expenses',
          'station_revenue_share_distribution',
          'total_recoupable_expenses'
        ],
        derived_metrics: ['client_net_share', 'gross_ecpm', 'client_net_ecpm'],
        timeKeyFilter: this.libServ.deepCopy(
          this.filtersApplied['timeKeyFilter']
        ),
        filters: this.libServ.deepCopy(this.filtersApplied['filters']),
        groupByTimeKey: { key: [], interval: 'daily' },
        gidGroupBy: this.getGrpBys([grpBys[0]]),
        orderBy: [{ key: grpBys[0], opcode: 'desc' }],
        limit: '',
        offset: ''
      };
      this.loadAggTable(e.map(e => e.key), this.tableReq);
    }
  }

  initialLoading() {
    this.flatTableStationReq = {
      dimensions: ['station_group', 'derived_station_rpt', 'time_key'],
      metrics: [
        'dp_impressions',
        'gross_revenue',
        'total_expenses',
        'station_revenue_share_distribution',
        'total_recoupable_expenses'
      ],
      derived_metrics: ['client_net_share', 'gross_ecpm', 'client_net_ecpm'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['station_group', 'derived_station_rpt'], // grpBys.filter(v => v !== 'time_key'),
      orderBy: [{ key: 'time_key', opcode: 'desc' }],
      limit: '',
      offset: ''
    };
    // this.loadTable(this.flatTableStationReq);
    this.flatTableSourceReq = {
      dimensions: [
        'station_group',
        'derived_station_rpt',
        'source',
        'time_key'
      ],
      metrics: [
        'dp_impressions',
        'gross_revenue',
        'total_expenses',
        'station_revenue_share_distribution',
        'total_recoupable_expenses'
      ],
      derived_metrics: ['client_net_share', 'gross_ecpm', 'client_net_ecpm'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key', 'accounting_key'],
        interval: 'daily'
      },
      gidGroupBy: ['station_group', 'derived_station_rpt', 'source'],
      orderBy: [

        { key: 'source', opcode: 'asc' },
        { key: 'time_key', opcode: 'desc' },
      ],
      limit: '',
      offset: ''
    };
    this.loadTable(this.flatTableStationReq);
    // this.selectedGrpBy([this.groupByDims[0], this.groupByDims[1]]);
  }

  loadAggTable(grpByParams, tableReq) {
    const finalColDef = this.libServ.deepCopy(this.metricColDef);

    const grpBys = this.libServ.deepCopy(grpByParams); // this.getGrpBys(grpByParams);//grpByParams;
    this.grpBySelected = this.libServ.deepCopy(grpByParams);

    const grpBysFilter = this.libServ.deepCopy(grpBys); // this.filtersApplied['groupby'];

    grpBysFilter.reverse().forEach(grp => {
      finalColDef.unshift(this.dimColDef.find(e => e.field === grp));
    });

    this.aggTableColumnDef = finalColDef.slice();

    this.aggTableJson['columns'] = this.aggTableColumnDef.slice(
      grpBysFilter.length
    );
    this.aggTableJson['selectedColumns'] = this.aggTableColumnDef.slice(
      grpBysFilter.length
    );
    this.aggTableJson['frozenCols'] = [
      ...this.aggTableColumnDef.slice(0, grpBysFilter.length)
    ];

    this.aggTableJson['frozenWidth'] =
      this.aggTableColumnDef
        .slice(0, grpBysFilter.length)
        .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px';

    this.reloadAggTable();

    this.aggTableJson['loading'] = true;

    // const tableReq = {
    //   dimensions: [grpBys[0]], // grpBys,0//[this.filtersApplied['groupby'][0]['key']],
    //   metrics: [
    //     'dp_impressions',
    //     'gross_revenue',
    //     'total_expenses',
    //     'station_revenue_share_distribution',
    //     'total_recoupable_expenses'
    //   ],
    //   derived_metrics: ['client_net_share', 'gross_ecpm', 'client_net_ecpm'],
    //   timeKeyFilter: this.libServ.deepCopy(
    //     this.filtersApplied['timeKeyFilter']
    //   ),
    //   filters: this.libServ.deepCopy(this.filtersApplied['filters']),
    //   groupByTimeKey: { key: [], interval: 'daily' },
    //   gidGroupBy: this.getGrpBys([grpBys[0]]),
    //   orderBy: [{ key: grpBys[0], opcode: 'desc' }],
    //   limit: '',
    //   offset: ''
    // };

    // if (
    //   tableReq['filters']['dimensions'].findIndex(
    //     item => item.key == 'derived_station_rpt'
    //   ) > -1 &&
    //   !tableReq['gidGroupBy'].includes('derived_station_rpt')
    // ) {
    //   tableReq['gidGroupBy'] = this.filtersApplied['filters']['dimensions'].map(
    //     e => e.key
    //   );
    //   tableReq['dimensions'] = [grpBys[0]];
    // } else {
    //   tableReq['dimensions'] = [grpBys[0]];
    //   tableReq['gidGroupBy'].push('station_group');
    //   tableReq['gidGroupBy'].push(grpBys[0]);
    // }

    this.dataFetchServ.getPayoutData(tableReq).subscribe(data => {
      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      } else {
        this.noTableData = false;
      }
      const tableData = data['data'];
      const arr = [];
      tableData.forEach((row: object) => {
        for (let i = 0; i < grpBysFilter.length; i++) {
          if (i !== grpBysFilter.length - 1) {
            row[grpBysFilter[i]] = 'All';
          }
        }
        let obj = {};
        if (grpBysFilter.length === 1) {
          obj = {
            data: row
          };
        } else {
          obj = {
            data: row,
            children: [{ data: {} }]
          };
        }
        arr.push(obj);
      });

      this.aggTableData = <TreeNode[]>arr;
      this.aggTableJson['totalRecords'] = data['totalItems'];
      this.aggTableJson['loading'] = false;
    });
  }

  reloadAggTable() {
    this.displayAggTable = false;
    setTimeout(() => {
      this.displayAggTable = true;
    }, 0);
  }

  onTableDrill(e: Event) {
    console.log('Drilled', e);
    if (!e['node']['childLoaded']) {
      this.aggTableJson['loading'] = true;

      const tableReq = {
        dimensions: [],
        metrics: [
          'dp_impressions',
          'gross_revenue',
          'total_expenses',
          'station_revenue_share_distribution',
          'total_recoupable_expenses'
        ],
        derived_metrics: ['client_net_share', 'gross_ecpm', 'client_net_ecpm'],

        timeKeyFilter: this.libServ.deepCopy(
          this.filtersApplied['timeKeyFilter']
        ),
        filters: this.libServ.deepCopy(this.filtersApplied['filters']),
        groupByTimeKey: { key: [], interval: 'daily' },
        gidGroupBy: this.grpBySelected, // this.getGrpBys(),
        orderBy: [],
        limit: '',
        offset: ''
      };

      const grpBys = this.grpBySelected; // this.filtersApplied['groupby'].map(e => e.key);
      for (const g of grpBys) {
        if (e['node']['data'][g] !== 'All') {
          tableReq['dimensions'].push(g);
          if (!tableReq['gidGroupBy'].includes(g)) {
            tableReq['gidGroupBy'].push(g);
          }
          if (
            tableReq['filters']['dimensions'].findIndex(e => e.key === g) === -1
          ) {
            tableReq['filters']['dimensions'].push({
              key: g,
              values: [e['node']['data'][g]]
            });
          } else {
            tableReq['filters']['dimensions'].splice(
              tableReq['filters']['dimensions'].findIndex(e => e.key === g),
              1
            );
            tableReq['filters']['dimensions'].push({
              key: g,
              values: [e['node']['data'][g]]
            });
          }
        } else {
          tableReq['dimensions'].push(g);
          if (!tableReq['gidGroupBy'].includes(g)) {
            tableReq['gidGroupBy'].push(g);
          }
          break;
        }
      }

      if (!tableReq['gidGroupBy'].includes('station_group')) {
        tableReq['gidGroupBy'].push('station_group');
      }
      let keys = this.filtersApplied['filters']['dimensions'].map(m => m.key)
      for (const col of keys) {
        if (!tableReq['gidGroupBy'].includes(col)) {
          tableReq['gidGroupBy'].push(col);
        }
      }

      this.dataFetchServ.getPayoutData(tableReq).subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        const childData = data['data'];
        const arr = [];

        childData.forEach((row: object) => {
          this.aggTableJson['frozenCols'].forEach(r => {
            if (typeof row[r['field']] === 'undefined') {
              row[r['field']] = 'All';
            }
          });
          let obj = {};

          if (row[grpBys[grpBys.length - 2]] === 'All') {
            obj = {
              data: row,
              children: [{ data: {} }]
            };
          } else {
            obj = {
              data: row
            };
          }
          arr.push(obj);
        });

        this.aggTableJson['loading'] = false;
        e['node']['children'] = <TreeNode[]>arr;
        this.aggTableData = [...this.aggTableData];
        e['node']['childLoaded'] = true;
      });
    }
  }

  getGrpBys(tabGrpBy) {
    let grpBys = tabGrpBy; // [this.filtersApplied['groupby'][0]['key']];
    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          grpBys.concat(
            this.filtersApplied['filters']['dimensions']
              .filter(f => f.values.length)
              .map(m => m.key)
          )
        )
      );
    }
    return grpBys;
  }

  loadTable(params) {
    this.dataFetchServ.getPayoutData(params).subscribe(data => {
      this.flatTableStationJson['loading'] = true;

      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }
      const tableData = data['data'];

      const arr = [];
      tableData.forEach((row: object) => {
        const obj = {
          data: row
        };
        arr.push(obj);
      });
      this.flatTableStationData = <TreeNode[]>arr;
      this.flatTableStationJson['totalRecords'] = data['totalItems'];

      this.flatTableStationJson['lazy'] = true;
      setTimeout(() => {
        this.flatTableStationJson['loading'] = false;
      }, 0);

    });
  }

  loadSourceTable(params) {
    this.dataFetchServ.getPayoutData(params).subscribe(data => {
      this.flatTableSourceJson['loading'] = true;

      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }
      const tableData = data['data'];

      const arr = [];
      tableData.forEach((row: object) => {
        const obj = {
          data: row
        };
        arr.push(obj);
      });
      this.flatTableSourceData = <TreeNode[]>arr;
      this.flatTableSourceJson['totalRecords'] = data['totalItems'];
      this.flatTableSourceJson['loading'] = false;
      this.flatTableSourceJson['lazy'] = true;
    });
  }

  loadRevSourcePieChart(params) {
    this.popupPieChartSourceJson = {
      chartTypes: [{ key: 'pie', label: 'Pie Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Gross Revenue($) distribution across Source'
        },
        legend: {
          display: false
        },
        elements: {
          arc: {
            borderWidth: 0
          }
        },
        plugins: {
          datalabels: {
            formatter: (value, ctx) => {
              let sum = 0;
              const dataArr = ctx.chart.data.datasets[0].data;
              dataArr.map(data => {
                sum += data;
              });
              const percentage = (value * 100) / sum;
              if (percentage > 10) {
                return `${percentage.toFixed(2)} %`;
              } else {
                return '';
              }
            },
            color: 'black'
          }
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };
    this.popupPieChartSourceJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          let sum = 0;
          const dataArr = data.datasets[0].data;
          dataArr.map(data => {
            sum += data;
          });
          const percentage = (currentValue * 100) / sum;
          return `${
            data.labels[tooltipItem.index]
            } : ${this.formatNumPipe.transform(
              currentValue,
              '$',
              []
            )} - ${percentage.toFixed(2)} %`;
        }
      }
    };
    this.showRevSourcePieChart = false;
    params['orderBy'] = [{ key: 'gross_revenue', opcode: 'desc' }];
    params['gidGroupBy'].push('source');

    this.dataFetchServ.getPayoutData(params).subscribe(data => {
      const chartData = data['data'];

      const sources = Array.from(new Set(chartData.map(s => s['source'])));
      const colors = this.libServ.dynamicColors(sources.length);
      this.popupPieChartSourceJson['chartData']['labels'] = sources;
      this.popupPieChartSourceJson['chartData']['datasets'][0][
        'data'
      ] = Array.from(new Set(chartData.map(s => s['gross_revenue'])));

      this.popupPieChartSourceJson['chartData']['datasets'][0][
        'backgroundColor'
      ] = colors;
      this.showRevSourcePieChart = true;
    });
  }

  loadRevDeviceTypePieChart(params) {
    this.popupPieChartDeviceTypeJson = {
      chartTypes: [{ key: 'pie', label: 'Pie Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Gross Revenue($) distribution across Device Type'
        },
        legend: {
          display: false
        },
        elements: {
          arc: {
            borderWidth: 0
          }
        },
        plugins: {
          datalabels: {
            formatter: (value, ctx) => {
              let sum = 0;
              const dataArr = ctx.chart.data.datasets[0].data;
              dataArr.map(data => {
                sum += data;
              });
              const percentage = (value * 100) / sum;
              if (percentage > 10) {
                return `${percentage.toFixed(2)} %`;
              } else {
                return '';
              }
            },
            color: 'black'
          }
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };
    this.popupPieChartDeviceTypeJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          let sum = 0;
          const dataArr = data.datasets[0].data;
          dataArr.map(data => {
            sum += data;
          });
          const percentage = (currentValue * 100) / sum;
          return `${
            data.labels[tooltipItem.index]
            } : ${this.formatNumPipe.transform(
              currentValue,
              '$',
              []
            )} - ${percentage.toFixed(2)} %`;
        }
      }
    };
    this.showRevDeviceTypePieChart = false;
    params['orderBy'] = [{ key: 'gross_revenue', opcode: 'desc' }];
    params['gidGroupBy'].push('derived_device_type1_name');
    this.dataFetchServ.getPayoutData(params).subscribe(data => {
      const chartData = data['data'];

      const sources = Array.from(
        new Set(chartData.map(s => s['derived_device_type1_name']))
      );
      const colors = this.libServ.dynamicColors(sources.length);
      this.popupPieChartDeviceTypeJson['chartData']['labels'] = sources;
      this.popupPieChartDeviceTypeJson['chartData']['datasets'][0][
        'data'
      ] = Array.from(new Set(chartData.map(s => s['gross_revenue'])));

      this.popupPieChartDeviceTypeJson['chartData']['datasets'][0][
        'backgroundColor'
      ] = colors;
      this.showRevDeviceTypePieChart = true;
    });
  }

  loadRevCreativeSizePieChart(params) {
    this.popupPieChartCreativeSizeJson = {
      chartTypes: [{ key: 'pie', label: 'Pie Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Gross Revenue($) distribution across Creative Size(Top 10)'
        },
        legend: {
          display: false
        },
        elements: {
          arc: {
            borderWidth: 0
          }
        },
        plugins: {
          datalabels: {
            formatter: (value, ctx) => {
              let sum = 0;
              const dataArr = ctx.chart.data.datasets[0].data;
              dataArr.map(data => {
                sum += data;
              });
              const percentage = (value * 100) / sum;
              if (percentage > 10) {
                return `${percentage.toFixed(2)} %`;
                // ${this.formatNumPipe.transform(
                //   value,
                //   '$',
                //   []
                // )} -
              } else {
                return '';
              }
            },
            color: 'black'
          }
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };
    this.popupPieChartCreativeSizeJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          let sum = 0;
          const dataArr = data.datasets[0].data;
          dataArr.map(data => {
            sum += data;
          });
          const percentage = (currentValue * 100) / sum;
          return `${
            data.labels[tooltipItem.index]
            } : ${this.formatNumPipe.transform(
              currentValue,
              '$',
              []
            )} - ${percentage.toFixed(2)} %`;
        }
      }
    };
    this.showRevCreativeSizePieChart = false;
    params['orderBy'] = [{ key: 'gross_revenue', opcode: 'desc' }];
    params['gidGroupBy'].push('creative_size_rpt_name');

    this.dataFetchServ.getPayoutData(params).subscribe(data => {
      const chartData = data['data'];
      const sources = Array.from(
        new Set(chartData.map(s => s['creative_size_rpt_name']))
      );
      const colors = this.libServ.dynamicColors(sources.length);
      this.popupPieChartCreativeSizeJson['chartData']['labels'] = sources;
      this.popupPieChartCreativeSizeJson['chartData']['datasets'][0][
        'data'
      ] = Array.from(new Set(chartData.map(s => s['gross_revenue'])));

      this.popupPieChartCreativeSizeJson['chartData']['datasets'][0][
        'backgroundColor'
      ] = colors;
      this.showRevCreativeSizePieChart = true;
    });
  }

  exportTablePopup(tableName, fileFormat) {
    if (this.exportRequest['sendEmail'].length > 0) {

      if (tableName === 'tbleStationGrp') {
        if (this.flatTableStationData.length == 0) {
          this.confirmationService.confirm({
            message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
            header: 'Information',
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
            },
            reject: () => {
            }
          });
        } else {
          this.toastService.displayToast({
            severity: 'info',
            summary: 'Export Report',
            detail:
              'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
            life: 10000
          });
          // this.flatTableStationJson['loading'] = true;
          const columnDefs = this.flatTableStationColumnDef;
          const req = this.libServ.deepCopy(this.flatTableStationReq);
          req['limit'] = '';
          req['offset'] = '';
          req['isTable'] = false;

          const sheetDetails = {};
          sheetDetails['columnDef'] = columnDefs;
          sheetDetails['data'] = [];
          sheetDetails['sheetName'] = 'Daily Data Distribution';
          sheetDetails['isRequest'] = true;
          sheetDetails['request'] = {
            url: '/api/flask/payout/cassandradata',
            method: 'POST',
            param: req
          };
          sheetDetails['disclaimer'] = [
            {
              position: 'bottom',
              label: 'Note: Data present in the table may vary over a period of time.',
              color: '#000000'
            }
          ];
          sheetDetails['totalFooter'] = {
            available: true,
            custom: true
          };
          sheetDetails['tableTitle'] = {
            available: false,
            label: this.config.data['field'] + ' Daily Distribution '
          };
          sheetDetails['image'] = [
            {
              available: true,
              path: environment.exportConfig.exportLogo,
              position: 'top'
            }
          ];
          const sheetDetailsarray = [];
          sheetDetailsarray.push(sheetDetails);
          this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
          this.exportRequest['fileName'] =
            this.config.data['field'] +
            ' Daily Distribution ' +
            moment(new Date()).format('MM-DD-YYYY HH:mm:ss');

          this.exportRequest['exportFormat'] = fileFormat;
          this.exportRequest['exportConfig'] = environment.exportConfig;
          this.dataFetchServ
            .getExportReportData(this.exportRequest)
            .subscribe(response => {
              console.log(response);
            });

        }

      } else if (tableName == 'tblSource') {
        if (this.flatTableSourceData.length == 0) {
          this.confirmationService.confirm({
            message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
            header: 'Information',
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
            },
            reject: () => {
            }
          });
        } else {
          this.toastService.displayToast({
            severity: 'info',
            summary: 'Export Report',
            detail:
              'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
            life: 10000
          });
          const columnDefs = this.flatTableSourceColumnDef;
          const req = this.libServ.deepCopy(this.flatTableSourceReq);
          req['limit'] = '';
          req['offset'] = '';
          req['isTable'] = false;
          const sheetDetails = {};
          sheetDetails['columnDef'] = columnDefs;
          sheetDetails['data'] = [];
          sheetDetails['sheetName'] = 'Daily Data Distribution';
          sheetDetails['isRequest'] = true;
          sheetDetails['request'] = {
            url: '/api/flask/payout/cassandradata',
            method: 'POST',
            param: req
          };
          sheetDetails['disclaimer'] = [
            {
              position: 'bottom',
              label: 'Note: Data present in the table may vary over a period of time.',
              color: '#000000'
            }
          ];
          sheetDetails['totalFooter'] = {
            available: true,
            custom: true
          };
          sheetDetails['tableTitle'] = {
            available: false,
            label: this.config.data['field'] + ' Daily Distribution '
          };
          sheetDetails['image'] = [
            {
              available: true,
              path: environment.exportConfig.exportLogo,
              position: 'top'
            }
          ];
          const sheetDetailsarray = [];
          sheetDetailsarray.push(sheetDetails);
          this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
          this.exportRequest['fileName'] =
            this.config.data['field'] +
            ' Daily Distribution ' +
            moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
          this.exportRequest['exportFormat'] = fileFormat;
          this.exportRequest['exportConfig'] = environment.exportConfig;
          this.dataFetchServ
            .getExportReportData(this.exportRequest)
            .subscribe(response => {
              console.log(response);
            });
        }
      } else if (tableName == 'tblAgg') {
        if (this.aggTableData.length == 0) {
          this.confirmationService.confirm({
            message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
            header: 'Information',
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
            },
            reject: () => {
            }
          });
        } else {
          const data = {
            filters: this.filtersApplied,
            colDef: this.aggTableColumnDef,
            groupBys: this.grpBySelected,
            field: this.config.data['field'],
            fileFormat: fileFormat,
            exportRequest: this.exportRequest
          };

          const ref = this.dialogService.open(
            FranklyPayoutExportPopupComponent,
            {
              header: 'Download Report',
              contentStyle: { 'max-height': '80vh', overflow: 'auto' },
              data: data
            }
          );
          ref.onClose.subscribe((data: string) => { });
        }

      } else {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Export Report',
          detail:
            'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
          life: 10000
        });
      }
      return false;
    }
  }
  // }

  isHiddenColumn(col: Object) { }

  onLazyLoadFlatTable(e: Event, tableName) {
    console.log('Lazyeeed', e, tableName);
    if (tableName == 'tblStationGrp' && this.tabChangedStationFlag) {
      console.log('Lazy', e);
      if (typeof this.flatTableStationReq !== 'undefined') {
        let orderby = [];
        if (
          typeof e['multiSortMeta'] !== 'undefined' &&
          e['multiSortMeta'] !== null
        ) {
          e['multiSortMeta'].forEach(sort => {
            orderby.push({
              key: sort['field'],
              opcode: sort['order'] === -1 ? 'desc' : 'asc'
            });
          });
        } else {
          orderby = [{ key: 'time_key', opcode: 'desc' }];
        }
        this.flatTableStationReq['limit'] = e['rows'];
        this.flatTableStationReq['offset'] = e['first'];
        this.flatTableStationReq['orderBy'] = orderby;
        this.loadTable(this.flatTableStationReq);
      }
    } else if (tableName == 'tblSource' && this.tabChangedSourceFlag) {
      console.log('Lazy', e);
      if (typeof this.flatTableSourceReq !== 'undefined') {
        let orderby = [];
        if (
          typeof e['multiSortMeta'] !== 'undefined' &&
          e['multiSortMeta'] !== null
        ) {
          e['multiSortMeta'].forEach(sort => {
            orderby.push({
              key: sort['field'],
              opcode: sort['order'] === -1 ? 'desc' : 'asc'
            });
          });
        } else {
          orderby = [{ key: 'time_key', opcode: 'desc' }];
        }

        this.flatTableSourceReq['limit'] = e['rows'];
        this.flatTableSourceReq['offset'] = e['first'];
        this.flatTableSourceReq['orderBy'] = orderby;
        this.loadSourceTable(this.flatTableSourceReq);
      }
    }
  }

  tabChanged(e) {
    const index = e.index;
    console.log('index', index)
    if (!this.tabChangedSourceFlag && index == 1) {
      this.loadSourceTable(this.flatTableSourceReq);
      this.tabChangedSourceFlag = true;
    } else if (!this.tabChangedChartFlag && index == 2) {
      if ((!this.isPopupChartView) && this.isPopupGroupbyView) {
        this.tabChangedGroupByFlag = true;
        this.selectedGrpBy([this.groupByDims[0], this.groupByDims[1]]);
      } else {
        const grpByRevChart = this.filtersApplied['filters']['dimensions'].map(
          e => e.key
        );

        const revSourcePieChartReq = {
          dimensions: ['source'],
          metrics: ['gross_revenue'],
          derived_metrics: [],
          timeKeyFilter: this.libServ.deepCopy(
            this.filtersApplied['timeKeyFilter']
          ),
          filters: this.libServ.deepCopy(this.filtersApplied['filters']),
          groupByTimeKey: {
            key: ['time_key', 'accounting_key'],
            interval: 'daily'
          },
          gidGroupBy: this.libServ.deepCopy(grpByRevChart),
          orderBy: [{ key: 'gross_revenue', opcode: 'desc' }],
          limit: '',
          offset: ''
        };

        this.loadRevSourcePieChart(revSourcePieChartReq);

        const revDeviceTypePieChartReq = {
          dimensions: ['derived_device_type1_name'],
          metrics: ['gross_revenue'],
          derived_metrics: [],
          timeKeyFilter: this.libServ.deepCopy(
            this.filtersApplied['timeKeyFilter']
          ),
          filters: this.libServ.deepCopy(this.filtersApplied['filters']),
          groupByTimeKey: {
            key: ['time_key', 'accounting_key'],
            interval: 'daily'
          },
          gidGroupBy: this.libServ.deepCopy(grpByRevChart),
          orderBy: [{ key: 'gross_revenue', opcode: 'desc' }],
          limit: '',
          offset: ''
        };
        this.loadRevDeviceTypePieChart(revDeviceTypePieChartReq);

        const revCreativeSizePieChartReq = {
          dimensions: ['creative_size_rpt_name'],
          metrics: ['gross_revenue'],
          derived_metrics: [],
          timeKeyFilter: this.libServ.deepCopy(
            this.filtersApplied['timeKeyFilter']
          ),
          filters: this.libServ.deepCopy(this.filtersApplied['filters']),
          groupByTimeKey: {
            key: ['time_key', 'accounting_key'],
            interval: 'daily'
          },
          gidGroupBy: this.libServ.deepCopy(grpByRevChart),
          orderBy: [{ key: 'gross_revenue', opcode: 'desc' }],
          limit: 10,
          offset: 0
        };
        this.loadRevCreativeSizePieChart(revCreativeSizePieChartReq);
      }
      this.tabChangedChartFlag = true;
    } else if (!this.tabChangedGroupByFlag && index == 3) {
      this.tabChangedGroupByFlag = true;
      console.log('this.groupByDims[0]', this.groupByDims)
      this.selectedGrpBy([this.groupByDims[0], this.groupByDims[1]]);

    }
  }

  onGlobalSearchChanged(searchValue, tableColDef, tableName) {
    if (tableName == 'tblStationGrp' && this.tabChangedStationFlag) {
      if (this.timeout) {
        clearTimeout(this.timeout);
      }
      this.timeout = setTimeout(() => {
        this.flatTableStationReq['filters']['globalSearch'] = {
          dimensions: [],
          value: searchValue
        };
        tableColDef.forEach(element => {
          this.flatTableStationReq['filters']['globalSearch']['dimensions'].push(
            element.field
          );
        });
        this.loadTable(this.flatTableStationReq);
      }, 3000);

    } else if (tableName == 'tblSource' && this.tabChangedSourceFlag) {
      if (this.timeout) {
        clearTimeout(this.timeout);
      }
      this.timeout = setTimeout(() => {
        this.flatTableSourceReq['filters']['globalSearch'] = {
          dimensions: [],
          value: searchValue
        };
        tableColDef.forEach(element => {
          this.flatTableSourceReq['filters']['globalSearch']['dimensions'].push(
            element.field
          );
        });
        this.loadSourceTable(this.flatTableSourceReq);
      }, 3000);

    } else {
      if (this.timeout) {
        clearTimeout(this.timeout);
      }
      this.timeout = setTimeout(() => {
        this.tableReq['filters']['globalSearch'] = {
          dimensions: [],
          value: searchValue
        };

        this.tableReq['filters']['globalSearch']['dimensions'].push(
          this.aggTableJson['frozenCols'][0]['field']
        );
        tableColDef.forEach(element => {
          this.tableReq['filters']['globalSearch']['dimensions'].push(
            element.field
          );
        });
        this.loadAggTable(this.groupBy, this.tableReq);
      }, 3000);
    }
  }

  chartSelected(e) { }
}
