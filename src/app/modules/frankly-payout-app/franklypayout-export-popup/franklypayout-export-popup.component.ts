import { Component, OnInit } from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { FetchApiDataService } from '../../frankly-payout-app/fetch-api-data.service';

import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import * as moment from 'moment';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-franklypayout-export-popup',
  templateUrl: './franklypayout-export-popup.component.html',
  styleUrls: ['./franklypayout-export-popup.component.scss']
})
export class FranklyPayoutExportPopupComponent implements OnInit {
  configData: object;
  exportRequest: ExportRequest = <ExportRequest>{};
  constructor(
    private libServ: CommonLibService,
    private dataFetchServ: FetchApiDataService,
    private exportService: ExportdataService,
    public config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private toastService: ToastService
  ) {
    this.configData = this.config.data;
  }

  ngOnInit() {
    this.exportRequest = this.configData['exportRequest'];
  }

  async downloadData(daily) {
    this.toastService.displayToast({
      severity: 'info',
      summary: 'Export Report',
      detail:
        'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
      life: 10000
    });
    console.log('config', this.configData);

    const filtersApplied = this.configData['filters'];
    const colDef = this.configData['colDef'];
    let grpBys = this.configData['groupBys']; // filtersApplied['groupby'].map(e => e.key);

    if (
      this.libServ.isEmptyObj(filtersApplied['filters']['dimensions']) ||
      filtersApplied['filters']['dimensions'].length === 0
    ) {
      filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          filtersApplied['filters']['dimensions']
            .filter(f => f.values.length)
            .map(m => m.key)
            .concat(grpBys)
        )
      );
    }

    if (daily) {
      grpBys.push('time_key');
      const tableReq = {
        dimensions: grpBys,
        metrics: [
          'dp_impressions',
          'gross_revenue',
          'total_expenses',
          'station_revenue_share_distribution',
          'total_recoupable_expenses'
        ],
        derived_metrics: ['client_net_share', 'gross_ecpm', 'client_net_ecpm'],
        timeKeyFilter: filtersApplied['timeKeyFilter'],
        filters: filtersApplied['filters'],
        groupByTimeKey: {
          key: ['accounting_key', 'time_key'],
          interval: 'daily'
        },
        gidGroupBy: grpBys.filter(v => v !== 'time_key'),
        orderBy: [{ key: 'time_key', opcode: 'desc' }],
        limit: '',
        offset: ''
      };

      const obj = {
        field: 'time_key',
        displayName: 'Date',
        format: 'date',
        width: '150',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: false
        }
      };

      if (!colDef.some(o => o.field == 'time_key')) {
        colDef.splice(0, 0, obj);
      }
      const sheetDetails = {};
      sheetDetails['columnDef'] = colDef;
      sheetDetails['data'] = [];
      sheetDetails['sheetName'] = 'Daily Distribution';
      sheetDetails['isRequest'] = true;
      sheetDetails['request'] = {
        url: '/api/flask/payout/cassandradata',
        method: 'POST',
        param: tableReq
      };
      sheetDetails['disclaimer'] = [
        {
          position: 'bottom',
          label: 'Note: Data present in the table may vary over a period of time.',
          color: '#000000'
        }
      ];
      sheetDetails['totalFooter'] = {
        available: false,
        custom: false
      };
      sheetDetails['tableTitle'] = {
        available: false,
        label: this.config.data['field'] + ' Daily Distribution '
      };
      sheetDetails['image'] = [
        {
          available: true,
          path: environment.exportConfig.exportLogo,
          position: 'top'
        }
      ];
      const sheetDetailsarray = [];
      sheetDetailsarray.push(sheetDetails);
      this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
      this.exportRequest['fileName'] =
        this.config.data['field'] +
        ' Daily Distribution ' +
        moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
      this.exportRequest['exportFormat'] = this.configData['fileFormat'];
      this.exportRequest['exportConfig'] = environment.exportConfig;
      // return false;
      this.dataFetchServ
        .getExportReportData(this.exportRequest)
        .subscribe(response => {
          console.log(response);
        });
    } else {
      const tableReq = {
        dimensions: grpBys,
        metrics: [
          'dp_impressions',
          'gross_revenue',
          'total_expenses',
          'station_revenue_share_distribution',
          'total_recoupable_expenses'
        ],
        derived_metrics: ['client_net_share', 'gross_ecpm', 'client_net_ecpm'],
        timeKeyFilter: filtersApplied['timeKeyFilter'],
        filters: filtersApplied['filters'],
        groupByTimeKey: {
          key: ['accounting_key', 'time_key'],
          interval: 'daily'
        },
        gidGroupBy: grpBys, // .filter(v => v !== 'time_key'),
        orderBy: [],
        limit: '',
        offset: ''
      };

      let temp = 0;
      const data1 = [];
      const sheetDetailsarray = [];
      while (temp < grpBys.length) {
        const grpbyExport = [];
        for (let i = 0; i <= temp; i++) {
          if (grpBys[i] != 'station_group') {
            grpbyExport.push(grpBys[i]);
            tableReq['orderBy'] = [{ key: grpBys[i], opcode: 'asc' }];
          }
        }

        tableReq['gidGroupBy'] = this.getGrpBys(grpbyExport);
        tableReq['dimensions'] = grpbyExport;
        tableReq['isTable'] = false;

        tableReq['isTable'] = false;
        if (grpbyExport.length > 0) {
          const colDefOptions = [];
          let i = 0;
          let sheetName = '';
          while (i < grpbyExport.length) {
            let displayName = '';
            if (grpbyExport[i] === 'derived_station_rpt') {
              displayName = 'Property';
              if (i === 0) {
                sheetName = 'Property';
              } else {
                sheetName = '+Property';
              }
            } else if (grpbyExport[i] === 'source') {
              displayName = 'Source';
              if (i === 0) {
                sheetName = 'Source';
              } else {
                sheetName = '+Source';
              }
            } else if (grpbyExport[i] === 'derived_device_type1_name') {
              displayName = 'Device Type';
              if (i === 0) {
                sheetName = 'Device Type';
              } else {
                sheetName = '+Device Type';
              }
            } else if (grpbyExport[i] === 'creative_size_rpt_name') {
              displayName = 'Creative Size';
              if (i === 0) {
                sheetName = 'Creative Size';
              } else {
                sheetName = '+Creative Size';
              }
            } else if (grpbyExport[i] === 'derived_ad_type_name') {
              displayName = 'Ad Type';
              if (i === 0) {
                sheetName = 'Ad Type';
              } else {
                sheetName = '+Ad Type';
              }
            }

            colDefOptions.push({
              field: grpbyExport[i],
              displayName: displayName,
              visible: true,
              width: 150,
              format: 'string',
              exportConfig: {
                format: 'string',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              }
            });
            i++;
          }

          colDefOptions.push(
            {
              field: 'dp_impressions',
              displayName: 'Billable Imps.',
              format: 'number',
              width: '170',
              exportConfig: {
                format: 'number',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: true
              },
              footerTotal: '-'
            },
            {
              field: 'gross_revenue',
              displayName: 'Gross Revenue',
              width: '155',
              exportConfig: {
                format: 'currency',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              format: '$',
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: true
              },
              footerTotal: '-'
            },
            {
              field: 'gross_ecpm',
              displayName: 'Gross eCPM',
              width: '155',
              exportConfig: {
                format: 'currency',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              format: '$',
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: true
              },
              footerTotal: '-'
            },
            {
              field: 'station_revenue_share_distribution',
              displayName: 'Client Net Revenue',
              format: '$',
              width: '195',
              exportConfig: {
                format: 'currency',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: true
              },
              footerTotal: '-'
            },
            {
              field: 'client_net_ecpm',
              displayName: 'Client Net eCPM',
              format: '$',
              width: '135',
              exportConfig: {
                format: 'currency',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: false,
                resizable: true,
                movable: true
              },
              footerTotal: '-'
            },
            {
              field: 'total_expenses',
              displayName: 'Expenses',
              format: '$',
              width: '135',
              exportConfig: {
                format: 'currency',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              formatConfig: [],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: true
              },
              footerTotal: '-'
            },

            {
              field: 'client_net_share',
              displayName: 'Client Net Share',
              format: 'percentage',
              width: '175',
              value: '',
              condition: '',
              exportConfig: {
                format: 'percentage',
                styleinfo: {
                  thead: 'default',
                  tdata: 'white'
                }
              },
              formatConfig: [2],
              options: {
                editable: false,
                colSearch: false,
                colSort: true,
                resizable: true,
                movable: true
              }
            }
          );

          const sheetDetails = {};
          sheetDetails['columnDef'] = this.libServ.deepCopy(colDefOptions);
          sheetDetails['data'] = [];
          sheetDetails['sheetName'] = sheetName;
          sheetDetails['isRequest'] = true;
          sheetDetails['request'] = {
            url: '/api/flask/payout/cassandradata',
            method: 'POST',
            param: this.libServ.deepCopy(tableReq)
          };
          sheetDetails['disclaimer'] = [
            {
              position: 'bottom',
              label: 'Note: Data present in the table may vary over a period of time.',
              color: '#000000'
            }
          ];
          sheetDetails['totalFooter'] = {
            available: true,
            custom: true
          };
          sheetDetails['tableTitle'] = {
            available: false,
            label: 'Aggregated Data By ' + sheetName.replace('+', '')
          };
          sheetDetails['image'] = [
            {
              available: true,
              path: environment.exportConfig.exportLogo,
              position: 'top'
            }
          ];

          sheetDetailsarray.push(sheetDetails);
        }
        temp++;
      }
      this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
      this.exportRequest['fileName'] =
        this.config.data['field'] +
        ' Aggregated Data ' +
        moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
      this.exportRequest['exportFormat'] = this.configData['fileFormat'];
      this.exportRequest['exportConfig'] = environment.exportConfig;
      this.dataFetchServ
        .getExportReportData(this.exportRequest)
        .subscribe(response => {
          console.log(response);
        });
    }
  }
  getGrpBys(key) {
    // let grpBys = this.filtersApplied['groupby'].map(e => e.key);

    const filtersApplied = this.configData['filters'];
    let grpBys = key;
    if (
      this.libServ.isEmptyObj(filtersApplied['filters']['dimensions']) ||
      filtersApplied['filters']['dimensions'].length === 0
    ) {
      filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          grpBys.concat(
            filtersApplied['filters']['dimensions']
              .filter(f => f.values.length)
              .map(m => m.key)
          )
        )
      );
    }
    return grpBys;
  }

  cancel() {
    this.ref.close(null);
  }
}
