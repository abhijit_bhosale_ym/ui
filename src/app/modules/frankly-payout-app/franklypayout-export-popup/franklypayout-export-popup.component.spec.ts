import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FranklyPayoutExportPopupComponent } from './franklypayout-export-popup.component';

describe('FranklyPayoutExportPopupComponent', () => {
  let component: FranklyPayoutExportPopupComponent;
  let fixture: ComponentFixture<FranklyPayoutExportPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FranklyPayoutExportPopupComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FranklyPayoutExportPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
