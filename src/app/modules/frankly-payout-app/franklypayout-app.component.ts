import { Component, OnInit, OnDestroy } from '@angular/core';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { CommonLibService } from 'src/app/_services';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';
import { FetchApiDataService } from '../frankly-payout-app/fetch-api-data.service';
import { FranklyPayoutDailyDataPopupComponent } from './franklypayout-daily-data-popup/franklypayout-daily-data-popup.component';
import { FranklyPayoutExportPopupComponent } from './franklypayout-export-popup/franklypayout-export-popup.component';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ExportPptService } from 'src/app/_services/export/export-ppt.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { Subscription } from 'rxjs';
import { SendEmailComponent } from '../send-email/send-email.component';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { environment } from '../../../environments/environment';
import { DialogService } from 'primeng';

@Component({
  selector: 'ym-franklypayout-app',
  templateUrl: './franklypayout-app.component.html',
  styleUrls: ['./franklypayout-app.component.scss']
})
export class FranklyPayoutAppComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;
  timeZone = environment.timeZone;
  appConfig: object = {};
  lastUpdatedOn: Date;
  nextUpdated: Date;
  dataUpdatedThrough: Date;
  cardsJson = [];
  showCards = true;
  isPopupGroupbyView = false;
  isPopupChartView = false;
  aggTableData: TreeNode[];
  noTableData = false;
  dimColDef: any[];
  //  metricColDef: any[];
  aggTableColumnDef: any[];
  aggTableJson: Object;
  displayAggTable = true;
  isExportReport = false;
  timeout: any;
  tableReq: object;

  filtersApplied: object = {};
  exportRequest: ExportRequest = <ExportRequest>{};

  appFeaturesConfigs = {};
  mainCardsConfig = {};

  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private htmltoimage: HtmltoimageService,
    private exportService: ExportdataService,
    private pptExport: ExportPptService,
    private confirmationService: ConfirmationService
  ) { }

  ngOnInit() {
    this.dimColDef = [
      {
        field: 'accounting_key',
        displayName: 'Month-Year',
        format: 'date',
        width: '125',
        value: '',
        isExpanded: 'false',
        exportConfig: {
          format: 'date<<MMMM-YYYY<<-',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: ['MMMM YYYY'],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'station_group',
        displayName: 'Media Group',
        format: '',
        width: '250',
        isExpanded: 'false',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'derived_station_rpt',
        displayName: 'Property',
        format: '',
        width: '150',
        isExpanded: 'false',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'dp_impressions',
        displayName: 'Billable Imps.',
        format: 'number',
        width: '125',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'gross_revenue',
        displayName: 'Gross Revenue',
        width: '135',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'gross_ecpm',
        displayName: 'Gross eCPM',
        width: '120',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '$',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'station_revenue_share_distribution',
        displayName: 'Client Net Revenue',
        format: '$',
        width: '160',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'client_net_ecpm',
        displayName: 'Client Net eCPM',
        format: '$',
        width: '150',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'total_expenses',
        displayName: 'Expenses',
        format: '$',
        width: '110',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },

      {
        field: 'client_net_share',
        displayName: 'Client Net Share',
        format: 'percentage',
        width: '150',
        value: '',
        condition: '',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        }
      }
    ];

    // this.metricColDef = [

    // ];
    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      reload: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,

      columns: this.libServ.deepCopy(this.dimColDef.slice(3)),
      selectedColumns: this.libServ.deepCopy(this.dimColDef.slice(3)),
      frozenCols: this.libServ.deepCopy([...this.dimColDef.slice(0, 3)]),
      frozenWidth:
        this.dimColDef
          .slice(0, 3)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        console.log('appConfig', this.appConfig);
        this.isExportReport = this.appConfig['permissions'].some(
          o => o.name == 'frankly-payout-export-report'
        );
        this.isPopupGroupbyView = this.appConfig['permissions'].some(
          o => o.name === 'frank-payout-grpby-popup'
        );

        this.isPopupChartView = this.appConfig['permissions'].some(
          o => o.name === 'frank-payout-view-charts-popup'
        );
        this._titleService.setTitle(this.appConfig['displayName']);
        let startDate;
        let endDate;
        const date_config = this.appConfig['filter']['filterConfig']['filters'][
          'datePeriod'
        ][0];
        if (
          date_config['defaultDate'][0]['value'] === 0 &&
          date_config['defaultDate'][0]['period'] === 'month' &&
          (moment().format('DD') === '01' || moment().format('DD') === '02')
        ) {
          startDate = moment()
            .subtract(1, 'months')
            .startOf('month');
          endDate = moment()
            .subtract(1, 'months')
            .endOf('month');
        } else {
          if (date_config['defaultDate'][0]['startOf']) {
            startDate = moment()
              .subtract(
                date_config['defaultDate'][0]['value'],
                date_config['defaultDate'][0]['period']
              )
              .startOf(date_config['defaultDate'][0]['period']);
          } else {
            startDate = moment().subtract(
              date_config['defaultDate'][0]['value'],
              date_config['defaultDate'][0]['period']
            );
          }
          endDate = moment().subtract(
            date_config['defaultDate'][1]['value'],
            date_config['defaultDate'][1]['period']
          );
        }

        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: startDate.format('YYYYMMDD'),
            time_key2: endDate.format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: [{ key: 'station_group' }] // this.appConfig['filter']['filterConfig']['groupBy'].filter(
          // v => v.selected
          // )
        };
        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        this.initialLoading();
      }
    });
  }

  initialLoading() {
    this.dataFetchServ
      .getLastUpdatedData(this.appConfig['id'])
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        this.lastUpdatedOn = moment(data[0]['updated_at']).toDate();
        this.nextUpdated = moment(data[0]['next_run_at']).toDate();
        this.dataUpdatedThrough = moment(
          data[0]['source_updated_through'],
          'YYYYMMDD'
        ).toDate();
      });

    this.libServ.getAppFeaturesConfigs(this.appConfig['id']).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: data['message']
        });
        console.log(data['message']);
        return;
      }
      this.appFeaturesConfigs = data['data'];
      this.appFeaturesConfigs['cardsConfig']['cards'].forEach(element => {
        if (element['name'] === 'main') {
          this.mainCardsConfig = element['config'];
          this.cardsJson = this.mainCardsConfig['list'];
          if (this.mainCardsConfig['display']) {
            this.loadCards();
          }
        }
      });
    });

    this.tableReq = {
      dimensions: ['accounting_key', 'station_group'], // [this.filtersApplied['groupby'][0]['key']],
      metrics: [
        'dp_impressions',
        'gross_revenue',
        'total_expenses',
        'station_revenue_share_distribution',
        'total_recoupable_expenses'
      ],
      derived_metrics: ['client_net_share', 'gross_ecpm', 'client_net_ecpm'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: ['station_group'], // grpBys,
      orderBy: [{ key: 'accounting_key', opcode: 'desc' }],
      limit: '',
      offset: ''
    };
    this.loadTableData(this.tableReq);
  }

  resetPagination(tt1) {
    if (typeof tt1 !== 'undefined') {
      tt1.reset();
    }
  }

  loadTableData(params) {
    // this.reloadAggTable();
    this.aggTableJson['loading'] = true;
    this.dataFetchServ.getPayoutData(params).subscribe(data => {
      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      } else {
        this.noTableData = false;
      }
      const grpBysFilter = ['station_group', 'derived_station_rpt'];

      const tableData = data['data'];
      const arr = [];
      tableData.forEach((row: object) => {
        for (let i = 0; i < grpBysFilter.length; i++) {
          if (i !== grpBysFilter.length - 1) {
            row['derived_station_rpt'] = 'All';
          }
        }
        let obj = {};
        if (grpBysFilter.length === 1) {
          obj = {
            data: row
          };
        } else {
          obj = {
            data: row,
            children: [{ data: {} }]
          };
        }

        obj['data']['isExpanded'] = '0';
        arr.push(obj);
      });
      this.aggTableData = <TreeNode[]>arr;
      this.aggTableJson['totalRecords'] = data['totalItems'];
      this.aggTableJson['loading'] = false;
    });
  }

  onGlobalSearchChanged(searchValue, tableColDef) {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    this.timeout = setTimeout(() => {
      this.tableReq['filters']['globalSearch'] = {
        dimensions: [],
        value: searchValue
      };
      let colDef = tableColDef.filter(x => x.field !== "derived_station_rpt");
      colDef.forEach(element => {
        this.tableReq['filters']['globalSearch']['dimensions'].push(
          element.field
        );
      });
      this.loadTableData(this.tableReq);
    }, 3000);
  }

  reloadAggTable() {
    this.displayAggTable = false;
    setTimeout(() => {
      this.displayAggTable = true;
    }, 0);
  }

  reset(table) {
    table.reset();
    this.tableReq = {
      // dimensions: this.getGrpBys(),
      dimensions: ['station_group', 'accounting_key'],
      metrics: [
        'dp_impressions',
        'gross_revenue',
        'total_expenses',
        'station_revenue_share_distribution',
        'total_recoupable_expenses'
      ],
      derived_metrics: ['client_net_share', 'gross_ecpm', 'client_net_ecpm'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys(), // this.filtersApplied['filters']['dimensions'].map(e => e.key), // grpBys,
      orderBy: [{ key: 'accounting_key', opcode: 'desc' }],
      limit: '',
      offset: ''
    };
    (this.aggTableJson['columns'] = this.libServ.deepCopy(
      this.dimColDef.slice(3)
    )),
      (this.aggTableJson['selectedColumns'] = this.libServ.deepCopy(
        this.dimColDef.slice(3)
      )),
      (this.aggTableJson['frozenCols'] = this.libServ.deepCopy([
        ...this.dimColDef.slice(0, 3)
      ])),
      this.loadTableData(this.tableReq);
  }

  loadCards() {
    this.showCards = false;
    const apiUrl = this.mainCardsConfig['api']['url'];
    const cardsReq = this.libServ.deepCopy(
      this.mainCardsConfig['api']['request']
    );
    cardsReq['timeKeyFilter'] = this.libServ.deepCopy(
      this.filtersApplied['timeKeyFilter']
    );
    cardsReq['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);
    cardsReq['gidGroupBy'] = this.getGrpBys(),
      this.libServ.getDataFromDynamicAPI(apiUrl, cardsReq).subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error - Contact to administator',
            detail: data
          });
          console.log(data['status_msg']);
          return;
        }
        if (!this.libServ.isEmptyObj(data['data'])) {
          this.cardsJson.map(o => {
            o['value'] = data['data'][0][o['field']];
          });
        } else {
          this.cardsJson.map(o => {
            o['value'] = 0;
          });
        }
        this.showCards = this.mainCardsConfig['display'];
      });
  }

  onTableDrill(e: Event) {
    if (!e['node']['childLoaded']) {
      this.aggTableJson['loading'] = true;
      const tableReq = {
        dimensions: ['station_group', 'accounting_key', 'derived_station_rpt'],
        metrics: [
          'dp_impressions',
          'gross_revenue',
          'total_expenses',
          'station_revenue_share_distribution',
          'total_recoupable_expenses'
        ],
        derived_metrics: ['client_net_share', 'gross_ecpm', 'client_net_ecpm'],
        timeKeyFilter: {},
        filters: this.libServ.deepCopy(this.filtersApplied['filters']),
        groupByTimeKey: {
          key: ['accounting_key', 'time_key'],
          interval: 'daily'
        },
        gidGroupBy: ['derived_station_rpt', 'station_group'], // this.getGrpBys(),
        orderBy: [],
        limit: '',
        offset: ''
      };

      const timeKey1 = e['node']['data']['accounting_key'];
      const timeKey2 = moment(timeKey1, 'YYYYMMDD')
        .endOf('month')
        .format('YYYYMMDD');

      tableReq['timeKeyFilter'] = { time_key1: timeKey1, time_key2: timeKey2 };

      const grpBys = ['derived_station_rpt', 'station_group']; // this.filtersApplied['groupby'].map(e => e.key);
      for (const g of grpBys) {
        if (e['node']['data'][g] !== 'All') {
          // tableReq['dimensions'].push(g);
          if (!tableReq['gidGroupBy'].includes(g)) {
            tableReq['gidGroupBy'].push(g);
          }
          if (
            tableReq['filters']['dimensions'].findIndex(e => e.key === g) === -1
          ) {
            tableReq['filters']['dimensions'].push({
              key: g,
              values: [e['node']['data'][g]]
            });
          } else {
            tableReq['filters']['dimensions'].splice(
              tableReq['filters']['dimensions'].findIndex(e => e.key === g),
              1
            );
            tableReq['filters']['dimensions'].push({
              key: g,
              values: [e['node']['data'][g]]
            });
            if (
              tableReq['filters']['dimensions']
                .find(e => e.key === g)
              ['values'].findIndex(v => v === e['node']['data'][g]) === -1
            ) {
              tableReq['filters']['dimensions'].push(e['node']['data'][g]);
            }
          }
        }
        // else {
        //   tableReq['dimensions'].push(g);
        //   if (!tableReq['gidGroupBy'].includes(g)) {
        //     tableReq['gidGroupBy'].push(g);
        //   }
        //   break;
        // }
      }

      // tableReq['gidGroupBy'] = this.getGrpBys();
      // For Dev purpose
      // tableReq['gidGroupBy'] = [
      //   'ad_size',
      //   'ad_type',
      //   'ad_units',
      //   'bidding_type',
      //   'device_category',
      //   'geography',
      //   'sales_channel',
      //   'sales_type',
      //   'site',
      //   'source'
      // ];

      this.dataFetchServ.getPayoutData(tableReq).subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        const childData = data['data'];
        const arr = [];

        childData.forEach((row: object) => {
          this.aggTableJson['frozenCols'].forEach(r => {
            if (typeof row[r['field']] === 'undefined') {
              row[r['field']] = 'All';
            }
          });
          let obj = {};
          if (row[grpBys[grpBys.length - 1]] === 'All') {
            obj = {
              data: row,
              children: [{ data: {} }]
            };
          } else {
            obj = {
              data: row
            };
          }
          obj['data']['isExpanded'] = '1';
          arr.push(obj);
        });

        this.aggTableJson['loading'] = false;
        e['node']['children'] = <TreeNode[]>arr;
        this.aggTableData = [...this.aggTableData];
        e['node']['childLoaded'] = true;
      });
    }
  }

  onLazyLoadAggTable(e: Event) {
    // console.log('Lazy Agg', e);
  }

  openPopup(row, field, rowData) {
    const grpBys = ['station_group'];
    const filtersApplied = this.libServ.deepCopy(this.filtersApplied);

    const isStationGrpAvail = filtersApplied['filters']['dimensions'].findIndex(
      item => item.key === 'station_group'
    );

    filtersApplied['timeKeyFilter']['time_key1'] = rowData['accounting_key'];
    filtersApplied['timeKeyFilter']['time_key2'] = moment(
      rowData['accounting_key'],
      'YYYYMMDD'
    )
      .endOf('month')
      .format('YYYYMMDD');

    filtersApplied['filters']['dimensions'].splice(
      filtersApplied['filters']['dimensions'].findIndex(
        item => item.key === 'station_group'
      ),
      1
    );

    filtersApplied['filters']['dimensions'].push({
      key: 'station_group',
      values: [rowData['station_group']]
    });

    if (rowData['derived_station_rpt'] !== 'All') {
      filtersApplied['filters']['dimensions'].splice(
        filtersApplied['filters']['dimensions'].findIndex(
          item => item.key === 'derived_station_rpt'
        ),
        1
      );

      filtersApplied['filters']['dimensions'].push({
        key: 'derived_station_rpt',
        values: [rowData['derived_station_rpt']]
      });
    }

    filtersApplied['groupby'] = grpBys;
    const data = {
      filters: filtersApplied,
      field: row,
      isPopupChartView: this.isPopupChartView,
      isPopupGroupbyView: this.isPopupGroupbyView,
      exportRequest: this.exportRequest,
      isExportReport: this.isExportReport
    };
    const ref = this.dialogService.open(FranklyPayoutDailyDataPopupComponent, {
      header: row + ' Daily Data ',
      contentStyle: { width: '80vw', overflow: 'auto' },
      data: data
    });
    ref.onClose.subscribe((data: string) => { });
  }

  exportTable(table, fileFormat) {
    if (this.aggTableData.length == 0) {
      this.confirmationService.confirm({
        message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
        header: 'Information',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
        },
        reject: () => {
        }
      });
    } else {
      if (this.exportRequest['sendEmail'].length > 0) {
        this.toastService.displayToast({
          severity: 'info',
          summary: 'Export Report',
          detail:
            'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
          life: 10000
        });

        const colDef = this.libServ.deepCopy(this.dimColDef);

        const tableReq = {
          dimensions: ['accounting_key', 'station_group', 'derived_station_rpt'],
          metrics: [
            'dp_impressions',
            'gross_revenue',
            'total_expenses',
            'station_revenue_share_distribution',
            'total_recoupable_expenses'
          ],
          derived_metrics: ['client_net_share', 'gross_ecpm', 'client_net_ecpm'],
          timeKeyFilter: this.libServ.deepCopy(
            this.filtersApplied['timeKeyFilter']
          ),
          filters: this.libServ.deepCopy(this.filtersApplied['filters']),
          groupByTimeKey: {
            key: ['accounting_key', 'time_key'],
            interval: 'daily'
          },
          gidGroupBy: ['station_group', 'derived_station_rpt'], // grpBys.filter(v => v !== 'time_key'),
          orderBy: [{ key: 'accounting_key', opcode: 'asc' }],
          limit: '',
          offset: ''
        };

        tableReq['isTable'] = false;

        const sheetDetails = {};
        sheetDetails['columnDef'] = colDef;
        sheetDetails['data'] = [];
        sheetDetails['sheetName'] = 'Data Distribution';
        sheetDetails['isRequest'] = true;
        sheetDetails['request'] = {
          url: '/api/flask/payout/cassandradata',
          method: 'POST',
          param: tableReq
        };
        sheetDetails['disclaimer'] = [
          {
            position: 'bottom',
            label: 'Note: Data present in the table may vary over a period of time.',
            color: '#000000'
          }
        ];
        sheetDetails['totalFooter'] = {
          available: true,
          custom: true
        };
        sheetDetails['tableTitle'] = {
          available: false,
          label: 'Overall Distribution'
        };
        sheetDetails['image'] = [
          {
            available: true,
            path: environment.exportConfig.exportLogo,
            position: 'top'
          }
        ];
        const sheetDetailsarray = [];
        sheetDetailsarray.push(sheetDetails);
        this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
        this.exportRequest['fileName'] =
          'Overall Distribution ' +
          moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
        this.exportRequest['exportFormat'] = fileFormat;
        this.exportRequest['exportConfig'] = environment.exportConfig;
        // console.log('exportreport', this.exportRequest);
        // return false;
        this.dataFetchServ
          .getExportReportData(this.exportRequest)
          .subscribe(response => {
            console.log(response);
          });
      } else {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Export Report',
          detail:
            'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
          life: 10000
        });
      }
    }
    return false;
  }

  isHiddenColumn(col: Object) {
    return (
      this.aggTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }

  /* --------------------- On Filter Apply Button Clicked --------------------- */

  onFiltersApplied(filterData: object) {
    this.filtersApplied['filters']['dimensions'] = [];

    // tslint:disable-next-line: forin
    for (const k in filterData['filter']['dimensions']) {
      if (filterData['filter']['dimensions'][k].length !== 0) {
        this.filtersApplied['filters']['dimensions'].push({
          key: k,
          values: filterData['filter']['dimensions'][k]
        });
      }
    }

    // this.filtersApplied['groupby'] = filterData['groupby'];
    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];

    if (this.mainCardsConfig['display']) {
      this.loadCards();
    }

    this.tableReq = {
      dimensions: ['accounting_key', 'station_group'],
      metrics: [
        'dp_impressions',
        'gross_revenue',
        'total_expenses',
        'station_revenue_share_distribution',
        'total_recoupable_expenses'
      ],
      derived_metrics: ['client_net_share', 'gross_ecpm', 'client_net_ecpm'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['accounting_key', 'time_key'],
        interval: 'daily'
      },
      gidGroupBy: this.getGrpBys(), // this.filtersApplied['filters']['dimensions'].map(e => e.key), // grpBys,
      orderBy: [{ key: 'accounting_key', opcode: 'desc' }],
      limit: '',
      offset: ''
    };
    this.loadTableData(this.tableReq);
  }

  getGrpBys() {
    let grpBys = this.filtersApplied['groupby'].map(e => e.key);
    // let grpBys = [this.filtersApplied['groupby'][0]['key']];
    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          grpBys.concat(
            this.filtersApplied['filters']['dimensions']
              .filter(f => f.values.length)
              .map(m => m.key)
          )
        )
      );
    }
    return grpBys;
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['name']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    } else if (type === 'ppt') {
      const chartElements = document.getElementsByTagName('ym-charts');
      const slides = [];
      [].forEach.call(chartElements, (el, index) => {
        const c = el.getElementsByTagName('canvas')[0];

        slides.push({
          title: `${this.appConfig['name']}`,
          base64data: this.libServ.canvasToImage('white', c),
          width: 8,
          height: 3
        });

        if (index === chartElements.length - 1) {
          const pptConfig = {
            filename: 'Charts',
            logo: {
              visible: true,
              path: '/assets/images/ym.png',
              width: 0.53125,
              height: 0.458333
            },
            slides: slides
          };
          this.pptExport.exportPPT(pptConfig);
        }
      });
    }
    return false;
  }

  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }

  sendEmail() {
    const imageConfig = {
      imgName: `${this.appConfig['name']}.png`,
      type: 'image/png'
    };

    this.htmltoimage
      .canvasImage(document.getElementById('capture'), imageConfig)
      .then(canvas => {
        this.dialogService.open(SendEmailComponent, {
          header: 'Email Screenshot',
          contentStyle: {
            'max-height': '80vh',
            width: '30vw',
            overflow: 'auto'
          },
          data: canvas.toDataURL('image/png')
        });
      });
  }
}
