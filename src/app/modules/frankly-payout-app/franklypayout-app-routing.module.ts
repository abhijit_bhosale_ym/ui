import { Routes } from '@angular/router';
import { FranklyPayoutAppComponent } from './franklypayout-app.component';

export const FranklyPayoutAppRoutes: Routes = [
  {
    path: '',
    component: FranklyPayoutAppComponent
  }
];
