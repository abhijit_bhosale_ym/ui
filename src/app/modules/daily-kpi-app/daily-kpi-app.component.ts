import { Component, OnInit, OnDestroy } from '@angular/core';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { CommonLibService } from 'src/app/_services';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';
import { FetchApiDataService } from './fetch-api-data.service';
import { HtmltoimageService } from 'src/app/_services/screencapture/htmltoimage.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { Subscription } from 'rxjs';
import { SendEmailComponent } from '../send-email/send-email.component';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { environment } from 'src/environments/environment';
import { DialogService } from 'primeng';

@Component({
  selector: 'ym-daily-kpi-app',
  templateUrl: './daily-kpi-app.component.html',
  styleUrls: ['./daily-kpi-app.component.scss']
})
export class DailyKpiAppComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;
  timeZone = environment.timeZone;
  appConfig: object = {};
  lastUpdatedOn: Date;
  nextUpdated: Date;
  dataUpdatedThrough: Date;

  cardsJson = [];
  showCards = true;
  isVideo = false;
  aggTableData: TreeNode[];
  noTableData = true; //make it false after dev complete
  VideoMetricColDef: any[];
  DisplayMetricColDef: any[];
  aggTableColumnDef: any[];
  aggTableJson: Object;
  displayAggTable = true; //make it false after dev complete
  toggleView = true;
  filtersApplied: object = {};
  exportRequest: ExportRequest = <ExportRequest>{};
  showhide = true;
  tableReq: object;
  oppsMissedTotal: object = {};
  oppsCPMTotal: object = {};
  rpmTotal: object = {};



  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: FetchApiDataService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private htmltoimage: HtmltoimageService,
    private confirmationService: ConfirmationService
  ) { }

  ngOnInit() {

    this.VideoMetricColDef = [
      {
        field: 'time_key',
        displayName: 'Date',
        format: 'date',
        width: '120',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: ['MM-DD-YYYY'],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'start',
        displayName: 'Video Starts',
        format: 'number',
        visible: false,
        width: '150',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'total_code_serve_count',
        displayName: 'Code Serve Count',
        width: '155',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'default_impressions',
        displayName: 'Default  Imps',
        format: 'number',
        width: '135',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },

      {
        field: 'ad_requests',
        displayName: 'Ad Requests',
        format: 'number',
        width: '135',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'adrequest_per_start',
        displayName: 'Ad Requests per Start',
        format: 'floatNumber',
        width: '150',
        exportConfig: {
          format: 'floatNumber',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'total_impressions',
        displayName: 'Total Imps',
        format: 'number',
        width: '135',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'drop_offs',
        displayName: 'Drop Offs',
        format: 'number',
        width: '135',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },

      {
        field: 'drop_offs_rate',
        displayName: 'Drop Off Rate %',
        format: 'percentage',
        width: '135',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },

      {
        field: 'video_missed_opp',
        displayName: 'Missed  Opp %',
        format: 'percentage',
        width: '135',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'kpi_fill_rate',
        displayName: 'Fill Rate %',
        format: 'percentage',
        width: '135',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'dfp_revenue',
        displayName: 'Est. Rev',
        format: '$',
        width: '135',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'kpi_ecpm',
        displayName: 'eCPMs',
        format: '$',
        width: '135',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },

      {
        field: 'kpi_cpm',
        displayName: 'OPP CPM',
        format: '$',
        width: '135',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          movable: true,
          colSearch: false,
          colSort: true,
          resizable: true,
        },
        footerTotal: '-'
      },
      {
        field: 'video_rpm',
        displayName: 'RPM',
        format: '$',
        width: '135',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          movable: true,
          colSearch: false,
          colSort: true,
          resizable: true,
        },
        footerTotal: '-'
      },
      {
        field: 'total_error_count',
        displayName: 'Total Errors',
        format: 'number',
        width: '135',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },

      {
        field: 'error_rate',
        displayName: 'Error Rate %',
        format: 'percentage',
        width: '135',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },

      {
        field: 'complete',
        displayName: 'Completions',
        format: 'number',
        width: '135',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },

      {
        field: 'completion_rate',
        displayName: 'Completion Rate %',
        format: 'percentage',
        width: '135',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'skip_button',
        displayName: 'Skip button shown',
        format: 'number',
        width: '135',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'engage_view',
        displayName: 'Engaged view',
        format: 'number',
        width: '135',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'view_through_rate',
        displayName: 'View-through Rate %',
        format: 'percentage',
        width: '135',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      }
    ];

    this.DisplayMetricColDef = [
      {
        field: 'time_key',
        displayName: 'Date',
        format: 'date',
        width: '120',
        exportConfig: {
          format: 'date',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: ['MM-DD-YYYY'],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'pageviews',
        displayName: 'Page Views',
        format: 'number',
        visible: false,
        width: '150',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'total_code_serve_count',
        displayName: 'Code Serve Count',
        width: '155',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: 'number',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'default_impressions',
        displayName: 'Default  Imps',
        format: 'number',
        width: '135',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },

      {
        field: 'ad_requests',
        displayName: 'Ad Requests',
        format: 'number',
        width: '135',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },

      {
        field: 'total_impressions',
        displayName: 'Total Imps',
        format: 'number',
        width: '135',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },

      {
        field: 'houseads_impressions',
        displayName: 'House  Ads',
        format: 'number',
        width: '135',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'drop_offs',
        displayName: 'Drop Offs',
        format: 'number',
        width: '135',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },

      {
        field: 'drop_offs_rate',
        displayName: 'Drop Off Rate %',
        format: 'percentage',
        width: '135',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },

      {
        field: 'missed_opp',
        displayName: 'Missed  Opp %',
        format: 'percentage',
        width: '135',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'kpi_fill_rate',
        displayName: 'Fill Rate %',
        format: 'percentage',
        width: '135',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'viewability',
        displayName: 'Display Viewability %',
        format: 'percentage',
        width: '145',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },

      {
        field: 'dfp_revenue',
        displayName: 'Est. Rev',
        format: '$',
        width: '135',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },
      {
        field: 'kpi_ecpm',
        displayName: 'eCPMs',
        format: '$',
        width: '135',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: true,
          resizable: true,
          movable: true
        },
        footerTotal: '-'
      },

      {
        field: 'kpi_cpm',
        displayName: 'OPP CPM',
        format: '$',
        width: '135',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          movable: true,
          colSearch: false,
          colSort: true,
          resizable: true,
        },
        footerTotal: '-'
      },
      {
        field: 'rpm',
        displayName: 'RPM',
        format: '$',
        width: '135',
        exportConfig: {
          format: 'currency',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          movable: true,
          colSearch: false,
          colSort: true,
          resizable: true,
        },
        footerTotal: '-'
      },

    ];
    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      reload: false,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,

      columns: this.DisplayMetricColDef.slice(1),
      selectedColumns: this.DisplayMetricColDef.slice(1),
      frozenCols: [...this.DisplayMetricColDef.slice(0, 1)],
      frozenWidth:
        this.DisplayMetricColDef
          .slice(0, 1)
          .reduce((tot, cur) => tot + parseInt(cur.width), 0) + 'px',

      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: false,
      columnSearch: false
    };

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        this._titleService.setTitle(this.appConfig['displayName']);
        let startDate;
        let endDate;
        const date_config = this.appConfig['filter']['filterConfig']['filters'][
          'datePeriod'
        ][0];
        if ((date_config['defaultDate'][0]['value'] === 0 && date_config['defaultDate'][0]['period'] === 'month') && (moment().format('DD') === '01' || moment().format('DD') === '02')) {
          startDate = moment().subtract(1, 'months').startOf('month');
          endDate = moment().subtract(1, 'months').endOf('month');
        } else {
          if (
            date_config[
            'defaultDate'
            ][0]['startOf']
          ) {
            startDate = moment()
              .subtract(
                date_config['defaultDate'][0]['value'],
                date_config['defaultDate'][0]['period']
              )
              .startOf(
                date_config['defaultDate'][0]['period']
              );
          } else {
            startDate = moment().subtract(
              date_config['defaultDate'][0]['value'],
              date_config['defaultDate'][0]['period']
            );
          }
          endDate = moment().subtract(
            date_config[
            'defaultDate'
            ][1]['value'],
            date_config[
            'defaultDate'
            ][1]['period']
          );
        }

        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: startDate.format('YYYYMMDD'),
            time_key2: endDate.format('YYYYMMDD')
          },
          filters: {
            dimensions: [
              { key: "publisher", values: ["IBT", "Newsweek", "TitanTV", "UMG"] }
            ], metrics: []
          },
          groupby: this.appConfig['filter']['filterConfig']['groupBy'].filter(
            v => v.selected
          )
        };
        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        this.initialLoading();
      }
    });
  }

  initialLoading() {
    this.dataFetchServ
      .getLastUpdatedData(this.appConfig['id'])
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }

        this.lastUpdatedOn = moment(data[0]['updated_at']).toDate();
        this.nextUpdated = moment(data[0]['next_run_at']).toDate();
        this.dataUpdatedThrough = moment(data[0]['source_updated_through'], 'YYYYMMDD').toDate();
      });


    this.loadTableData();
  }

  ConvertToColor(value, field) {
    let color = '';

    if (field == 'missed_opp' || field == 'video_missed_opp') {
      if (value == this.oppsMissedTotal['avgValue']) {
        color = 'rgb(255,255, 0)';
      } else if (value > this.oppsMissedTotal['avgValue']) {
        color = 'rgb(255,' + (255 - Math.round(((value - this.oppsMissedTotal['avgValue']) / (this.oppsMissedTotal['maxValue'] - this.oppsMissedTotal['avgValue'])) * 255)).toString() + ', 0)';
      } else {
        color = 'rgb(' + Math.round(((value - this.oppsMissedTotal['minValue']) / (this.oppsMissedTotal['maxValue'] - this.oppsMissedTotal['minValue'])) * 255).toString() + ',255, 0)';
      }
    } else if (field == 'kpi_cpm') {
      if (value == this.oppsCPMTotal['avgValue']) {
        color = 'rgb(255,255, 0)';
      } else if (value > this.oppsCPMTotal['avgValue']) {
        color = 'rgb(' + (255 - Math.round(((value - this.oppsCPMTotal['avgValue']) / (this.oppsCPMTotal['maxValue'] - this.oppsCPMTotal['avgValue'])) * 255)).toString() + ',255, 0)';
      } else {
        color = 'rgb(255,' + Math.round(((value - this.oppsCPMTotal['minValue']) / (this.oppsCPMTotal['maxValue'] - this.oppsCPMTotal['minValue'])) * 255).toString() + ', 0)';
      }
    } else if (field == 'rpm' || field == 'video_rpm') {
      if (value == this.rpmTotal['avgValue']) {
        color = 'rgb(255,255, 0)';
      } else if (value > this.rpmTotal['avgValue']) {
        color = 'rgb(' + (255 - Math.round(((value - this.rpmTotal['avgValue']) / (this.rpmTotal['maxValue'] - this.rpmTotal['avgValue'])) * 255)).toString() + ',255, 0)';
      } else {
        color = 'rgb(255,' + Math.round(((value - this.rpmTotal['minValue']) / (this.rpmTotal['maxValue'] - this.rpmTotal['minValue'])) * 255).toString() + ', 0)';
      }
    }
    return color;
  }
  resetPagination(tt1) {
    if (typeof tt1 !== 'undefined') {
      tt1.reset();
    }
  }

  reset(table) {
    table.reset();
    this.loadTableData();
  }

  loadTableData() {
    let finalColDef = [];
    if (this.isVideo) {
      finalColDef = this.VideoMetricColDef;
    } else {
      finalColDef = this.DisplayMetricColDef;
    }

    this.aggTableJson['columns'] = finalColDef.slice(1);
    this.aggTableJson['selectedColumns'] = finalColDef.slice(1);
    this.aggTableJson['frozenCols'] = [
      ...finalColDef.slice(0, 1)
    ];
    this.reloadAggTable();

    this.aggTableJson['loading'] = true;

    this.tableReq = {
      dimensions: ['time_key'],
      metrics: ['complete', 'default_impressions', 'dfp_revenue',
        'houseads_impressions', 'measurable_impressions', 'pageviews',
        'start', 'total_code_serve_count', 'total_error_count', 'total_impressions', 'viewable_impressions', 'skip_button', 'engage_view'],
      derived_metrics: ['kpi_cpm', 'kpi_ecpm', 'ad_requests',
        'drop_offs', 'drop_offs_rate', 'missed_opp', 'rpm', 'completion_rate',
        'error_rate', 'adrequest_per_start', 'viewability', 'video_missed_opp', 'video_rpm', 'view_through_rate', 'kpi_fill_rate'],
      timeKeyFilter: this.libServ.deepCopy(
        this.filtersApplied['timeKeyFilter']
      ),
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: { key: ['time_key'], interval: 'daily' },
      gidGroupBy: this.getGrpBys(),
      orderBy: [{ key: 'time_key', opcode: 'desc' }],
      limit: '',
      offset: ''
    };

    this.dataFetchServ.getDailyKPIData(this.tableReq).subscribe(data => {
      if (data['status'] === 0) {
        this.noTableData = true;

        // this.noTableData = false;

        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      } else {
        this.noTableData = false;
      }
      const tableData = data['data'];
      const arr = [];
      const cpmRow = [];
      const missedRow = [];
      const rpmRow = [];
      tableData.forEach((row: object) => {
        let obj = {};
        if (this.isVideo) {
          cpmRow.push(row['kpi_cpm']);
          missedRow.push(row['video_missed_opp']);
          rpmRow.push(row['video_rpm']);
        } else {
          cpmRow.push(row['kpi_cpm']);
          missedRow.push(row['missed_opp']);
          rpmRow.push(row['rpm']);
        }
        obj = {
          data: row
        };
        arr.push(obj);
      });
      if (tableData.length > 0) {
        this.oppsCPMTotal['minValue'] = Math.min(...cpmRow);
        this.oppsCPMTotal['maxValue'] = Math.max(...cpmRow);
        this.oppsCPMTotal['avgValue'] = cpmRow.reduce((previous, current) => current += previous) / cpmRow.length;
        this.rpmTotal['minValue'] = Math.min(...rpmRow);
        this.rpmTotal['maxValue'] = Math.max(...rpmRow);
        this.rpmTotal['avgValue'] = rpmRow.reduce((previous, current) => current += previous) / rpmRow.length;
        this.oppsMissedTotal['minValue'] = Math.min(...missedRow);
        this.oppsMissedTotal['maxValue'] = Math.max(...missedRow);
        this.oppsMissedTotal['avgValue'] = missedRow.reduce((previous, current) => current += previous) / missedRow.length;
      }
      // console.log('rowdata', missedRow);
      this.aggTableData = <TreeNode[]>arr;
      this.aggTableJson['totalRecords'] = data['totalItems'];
      setTimeout(() => {
        this.aggTableJson['loading'] = false;
      }, 0);
    });
  }

  reloadAggTable() {
    this.displayAggTable = false;
    setTimeout(() => {
      this.displayAggTable = true;
    }, 0);
  }

  onLazyLoadAggTable(e: Event) {
    console.log('Lazy Agg', e);
  }

  exportTable(table, fileFormat) {
    if (this.aggTableData.length == 0) {
      this.confirmationService.confirm({
        message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
        header: 'Information',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
        },
        reject: () => {
        }
      });
    } else {
      if (this.exportRequest['sendEmail'].length > 0) {
        this.toastService.displayToast({
          severity: 'info',
          summary: 'Export Report',
          detail:
            'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
          life: 10000
        });
        const columnDefs = this.isVideo ? this.VideoMetricColDef : this.DisplayMetricColDef;
        const req = this.libServ.deepCopy(this.tableReq);
        req['limit'] = '';
        req['offset'] = '';
        req['isTable'] = false;

        const sheetDetails = {};
        sheetDetails['columnDef'] = columnDefs;
        sheetDetails['data'] = [];
        sheetDetails['sheetName'] = 'Programmatic Daily Data';
        sheetDetails['isRequest'] = true;
        sheetDetails['request'] = {
          url: '/api/flask/daily_kpi/cassandradata',
          method: 'POST',
          param: req
        };
        sheetDetails['disclaimer'] = [
          {
            position: 'bottom',
            label: 'Note: Data present in the table may vary over a period of time.',
            color: '#000000'
          }
        ];
        sheetDetails['totalFooter'] = {
          available: true,
          custom: true
        };
        sheetDetails['tableTitle'] = {
          available: false,
          label: 'Daily KPI Data'
        };
        sheetDetails['image'] = [
          {
            available: true,
            path: environment.exportConfig.exportLogo,
            position: 'top'
          }
        ];
        const sheetDetailsarray = [];
        sheetDetailsarray.push(sheetDetails);
        this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
        this.exportRequest['fileName'] =
          'Daily KPI ' +
          moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
        this.exportRequest['exportFormat'] = fileFormat;
        this.exportRequest['exportConfig'] = environment.exportConfig;
        console.log('exportreport', this.exportRequest);
        // return false;
        this.dataFetchServ
          .getExportReportData(this.exportRequest)
          .subscribe(response => {
            console.log(response);
          });

      } else {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Export Report',
          detail:
            'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
          life: 10000
        });
      }
    }
    return false;
  }

  isHiddenColumn(col: Object) {
    return (
      this.aggTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }

  /* --------------------- On Filter Apply Button Clicked --------------------- */

  onFiltersApplied(filterData: object) {
    console.log('filterdata', filterData, filterData['filter']['dimensions']['publisher']);
    if (filterData['filter']['dimensions']['publisher'] == undefined || filterData['filter']['dimensions']['publisher'].length == 0) {
      this.filtersApplied['filters']['dimensions'] = [
        { key: "publisher", values: ["IBT", "Newsweek", "TitanTV", "UMG"] }
      ];
    } else {
      this.filtersApplied['filters']['dimensions'] = [];
    }
    this.isVideo = false;
    console.log('filterdata', filterData);
    if (filterData['filter']['dimensions'])
      // tslint:disable-next-line: forin
      for (const k in filterData['filter']['dimensions']) {
        console.log('filterdata', filterData['filter']['dimensions'][k].some(x => x == 'Video'), k);
        if (k == 'ad_type' && filterData['filter']['dimensions'][k].some(x => x == 'Video') && !(filterData['filter']['dimensions'][k].some(x => x == 'Display'))) {
          this.isVideo = true;
        }
        if (filterData['filter']['dimensions'][k].length !== 0) {
          this.filtersApplied['filters']['dimensions'].push({
            key: k,
            values: filterData['filter']['dimensions'][k]
          });
        }
      }

    this.filtersApplied['groupby'] = filterData['groupby'];
    this.filtersApplied['timeKeyFilter']['time_key1'] = filterData['date'][0];
    this.filtersApplied['timeKeyFilter']['time_key2'] = filterData['date'][1];

    this.loadTableData();
  }

  getGrpBys() {
    // let grpBys = this.filtersApplied['groupby'].map(e => e.key);
    let grpBys = ['publisher'];
    if (
      this.libServ.isEmptyObj(this.filtersApplied['filters']['dimensions']) ||
      this.filtersApplied['filters']['dimensions'].length === 0
    ) {
      this.filtersApplied['filters']['dimensions'] = [];
    } else {
      grpBys = Array.from(
        new Set(
          grpBys.concat(
            this.filtersApplied['filters']['dimensions']
              .filter(f => f.values.length)
              .map(m => m.key)
          )
        )
      );
    }
    return grpBys;
  }

  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['name']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    }
    return false;
  }

  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }

  sendEmail() {
    const imageConfig = {
      imgName: `${this.appConfig['name']}.png`,
      type: 'image/png'
    };

    this.htmltoimage
      .canvasImage(document.getElementById('capture'), imageConfig)
      .then(canvas => {
        this.dialogService.open(SendEmailComponent, {
          header: 'Email Screenshot',
          contentStyle: {
            'max-height': '80vh',
            width: '30vw',
            overflow: 'auto'
          },
          data: canvas.toDataURL('image/png')
        });
      });
  }
}
