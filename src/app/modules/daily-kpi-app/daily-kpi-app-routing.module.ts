import { Routes } from '@angular/router';
import { DailyKpiAppComponent } from './daily-kpi-app.component';

export const DailyKpiAppRoutes: Routes = [
  {
    path: '',
    component: DailyKpiAppComponent
  }
];
