import { Routes } from '@angular/router';
import { DerivedMetricsComponent } from './derived-metrics.component';
import { AddDerivedMetricsComponent } from './add-derived-metrics/add-derived-metrics.component'

export const DerivedMetricsRoutes: Routes = [
  {
    path: '',
    component: DerivedMetricsComponent
  },
  {
    path: 'add-derived-metrics',
    component: AddDerivedMetricsComponent
  },
];
