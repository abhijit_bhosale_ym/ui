import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DerivedMetricsApiService {
  private BASE_URL: string = environment.baseUrl;

  constructor(private http: HttpClient) { }

  getAppList() {
    const url = `${this.BASE_URL}/unified/v1/common/app-list`;
    return this.http.get(url);
  }

  getDerivedMetricsTableData() {
    const url = `${this.BASE_URL}/unified/v1/derived-metrics/get-data`;
    return this.http.get(url);
  }

  createNewDerivedMetric(params: object) {
    const url = `${this.BASE_URL}/unified/v1/derived-metrics/create`;
    return this.http.post(url, params);
  }

  deleteDerivedMetrics(derivedMetricsId) {
    const url = `${this.BASE_URL}/unified/v1/derived-metrics/delete/${derivedMetricsId}`;
    return this.http.get(url);
  }

  runAlert(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/alert-mgmt/runAlert`;
    return this.http.post(url, params);
  }

  getLastUpdatedData(appId) {
    const url = `${this.BASE_URL}/frankly/v1/common/getLastUpdatedData/${appId}`;
    return this.http.get(url);
  }

  public updateAlertGroup(params: object) {
    const url = `${this.BASE_URL}/frankly/v1/alert-mgmt/getData`;
    return this.http.post(url, params);
  }


}
