import { Component, OnInit, OnDestroy, ViewContainerRef } from '@angular/core';
import { TreeNode } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { CommonLibService } from 'src/app/_services';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';
import {
  Router,
  ActivatedRoute,
  NavigationEnd,
  RouterEvent
} from '@angular/router';
import {
  filter,
  takeUntil,
  pairwise,
  startWith
} from 'rxjs/operators';
import { Subject } from 'rxjs';

import { DerivedMetricsApiService } from './fetch-api-data.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { Subscription } from 'rxjs';
import { AddDerivedMetricsComponent } from './add-derived-metrics/add-derived-metrics.component';
// import { EditDerivedMetricsComponent } from './edit-derived-metrics/edit-derived-metrics.component'

@Component({
  selector: 'ym-derived-metrics',
  templateUrl: './derived-metrics.component.html',
  styleUrls: ['./derived-metrics.component.scss']
})
export class DerivedMetricsComponent implements OnInit {
  private appConfigObs: Subscription;
  appConfig: object = {};
  tableDimColDef: any[];
  aggTableJson: Object;
  aggTableData: TreeNode[];
  noTableData = false;
  tableData = [];
  timeout: any;
  globalSearchValue = "";
  displayAggTable = true;
  value: any;
  colSearch: any = {};
  public destroyed = new Subject<any>();

  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private dataFetchServ: DerivedMetricsApiService,
    private dialogService: DialogService,
    private toastService: ToastService,
    private router: Router,
    private currentActivatedRoute: ActivatedRoute,
    private confirmationService: ConfirmationService,
  ) {
    this.router.events
      .pipe(
        filter((event: RouterEvent) => event instanceof NavigationEnd),
        pairwise(),
        filter((events: RouterEvent[]) => events[0].url === events[1].url),
        startWith('Initial call'),
        takeUntil(this.destroyed)
      )
      .subscribe(() => {
        // If it is a NavigationEnd event re-initalise the component
        // this.usersService.getUsersList().subscribe(res => {
        //   console.log('res-----', res);
        //   this.users = <IUser[]>res['data'];
        // });
      });
  }

  ngOnInit() {
    this.tableDimColDef = [
      {
        field: 'name',
        displayName: 'Derived Metric',
        format: '',
        width: '80',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        },
      },
      {
        field: 'app_name',
        displayName: 'App Name',
        format: '',
        width: '120',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'formula_display_name',
        displayName: 'Formula',
        format: '',
        width: '160',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      // {
      //   field: 'edit',
      //   displayName: 'Edit',
      //   width: '35',
      //   exportConfig: {
      //     format: 'String',
      //     styleinfo: {
      //       thead: 'default',
      //       tdata: 'white'
      //     }
      //   },
      //   format: '',
      //   formatConfig: [],
      //   options: {
      //     editable: false,
      //     colSearch: false,
      //     colSort: false,
      //     resizable: true,
      //     movable: true
      //   },
      // },
      {
        field: 'delete',
        displayName: 'Delete',
        width: '35',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: false,
          colSort: false,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'created_at',
        displayName: 'Created At',
        width: '100',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: 'updated_at',
        displayName: 'Updated At',
        width: '100',
        exportConfig: {
          format: 'String',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        format: '',
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      },
      {
        field: "created_by",
        displayName: 'Created By',
        format: '',
        width: '100',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      }
    ];
    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,
      footerColumns: this.tableDimColDef,
      columns: this.tableDimColDef,
      selectedColumns: this.tableDimColDef,
      globalFilterFields: this.tableDimColDef.concat({
        field: "created_at_date",
        displayName: 'Created By',
        format: '',
        width: '100',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: true
        },
      }),
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: true
    };
    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        this._titleService.setTitle(this.appConfig['displayName']);
        this.initialLoading();
      }
    });
  }

  initialLoading() {
    this.loadDerivedMetrics()
  }

  loadDerivedMetrics() {
    this.dataFetchServ.getDerivedMetricsTableData().subscribe(data => {
      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      } else {
        this.noTableData = false;
      }
      this.tableData = data['data'];
      const arr = [];
      this.tableData.forEach((row: object) => {
        row['created_at_date'] = this.convertToLocalDate(row['created_at']);
        row['updated_at_date'] = this.convertToLocalDate(row['updated_at']);
        let obj = {};
        obj = {
          data: row
        };
        arr.push(obj);
      });
      this.aggTableData = <TreeNode[]>arr;
      this.reloadAggTable();
      this.aggTableJson['totalRecords'] = data['data'].length;
      this.aggTableJson['loading'] = false;
      // console.log(this.tableData);
    });
  }

  convertToLocalDate = (datetimeStr) => {
    var date = new Date(datetimeStr);
    return moment(date).format('YYYY-MM-DD hh:mm A Z');
  }

  // editDerivedMetrics(rowData) {
  //   // console.log("rowData", rowData);
  //   const data = {
  //     derivedMetricsData: rowData,
  //     user_id: this.appConfig['user']['id'].toString()
  //   };
  //   const ref = this.dialogService.open(EditDerivedMetricsComponent, {
  //     header: 'Edit Derived Metric : ' + rowData['name'],
  //     contentStyle: { 'max-height': '90vh', width: '70vw', overflow: 'auto' },
  //     data: data,
  //   });
  //   ref.onClose.subscribe((data1: string) => {
  //     if (data1) {
  //       this.loadDerivedMetrics()
  //     }
  //   });
  // }

  addDerivedMetric() {
    const ref = this.dialogService.open(AddDerivedMetricsComponent, {
      header: 'Add New Derived Metric',
      contentStyle: { 'min-height': '42vh', width: '65vw', overflow: 'auto' },
      data: '',
    });
    ref.onClose.subscribe(() => {
      this.loadDerivedMetrics()
    });
  }

  deleteDerivedMetrics(rowData) {
    this.confirmationService.confirm({
      message: 'Do you want to delete this derived metrics',
      header: 'Delete Derived Metrics',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.dataFetchServ.deleteDerivedMetrics(rowData['id']).subscribe(data => {
          if (data['status'] === 0) {
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Server Error',
              detail: 'Please refresh the page'
            });
            console.log(data['message']);
            return;
          } else {
            this.toastService.displayToast({
              severity: 'success',
              summary: 'Delete Derived Metrics',
              detail: 'Derived Metrics deleted successfully'
            });
            this.loadDerivedMetrics();
          }
        });
      },
      reject: () => {
      }
    })
  }

  reloadAggTable() {
    this.displayAggTable = false;
    setTimeout(() => {
      this.displayAggTable = true;
    }, 0);
  }

  ngOnDestroy() {
    this.destroyed.next();
    this.destroyed.complete();
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }

}
