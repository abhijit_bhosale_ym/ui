import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { DerivedMetricsApiService } from './../fetch-api-data.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { DialogService } from 'primeng/dynamicdialog';

@Component({
  selector: 'ym-add-derived-metrics',
  templateUrl: './add-derived-metrics.component.html',
  styleUrls: ['./add-derived-metrics.component.scss']
})

export class AddDerivedMetricsComponent implements OnInit, OnDestroy {
  private appConfigObs: Subscription;
  derivedForm: FormGroup;
  operatorList = [{ alies: '+', name: 'plus', type: 'op' },
  { alies: '-', name: 'minus', type: 'op' },
  { alies: '*', name: 'mulit', type: 'op' },
  { alies: '/', name: 'divid', type: 'op' },
  { alies: '(', name: 'divid', type: 'sbrt' },
  { alies: ')', name: 'divid', type: 'ebrt' }
  ]
  formatList = [
    { alies: 'Currency', name: 'currency' },
    { alies: 'Number', name: 'number' },
    { alies: 'Percentage', name: 'percentage' }
  ]
  appList = []
  availableMetric: any = []
  selectedformat: any;
  selectedApp: any;
  selectedMerticArray: any = [];
  selectedMerticArrayObj: any = [];
  draggedMetric: any;
  derivedMetricName: any;
  derivedMetricDescription: any;
  isValidDerviedMetric: boolean = true;
  isAppSelected: boolean = true;
  isFormatSelected: boolean = true;
  isDerivedMetricNameEnter: boolean = true;


  constructor(
    private formBuilder: FormBuilder,
    private dataFetchServ: DerivedMetricsApiService,
    private toastService: ToastService,
    public router: Router, private dialogService: DialogService,
  ) { }

  ngOnInit() {
    this.derivedForm = this.formBuilder.group({
      draggedMetric: new FormControl(),
      selectedformat: [this.selectedformat, Validators.required],
      selectedApp: [this.selectedApp, Validators.required],
      derivedMetricName: ['', Validators.required],
      derivedMetricDescription: ['']
    });
    this.dataFetchServ.getAppList().subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      } else {
        this.appList = data['data'];
      }
    });
  }

  appSelectedShowMetrics() {
    this.availableMetric = [];
    this.selectedMerticArray = [];
    this.selectedMerticArrayObj = [];
    this.isAppSelected = true;
    for (let i = 0; i < this.appList.length; i++) {
      if (this.selectedApp.app_id == this.appList[i]['app_id']) {
        let alertConfigJsonStr = this.appList[i]['alert_config'];
        this.availableMetric = JSON.parse(alertConfigJsonStr)['metrics'];
        for (let i = 0; i < this.availableMetric.length; i++) {
          this.availableMetric[i]['type'] = 'metric';
          this.availableMetric[i]['alies'] = this.availableMetric[i]['display_name'];
        }
        break;
      }
    }
  }

  dragStart(event, metric: any) {
    this.draggedMetric = metric
  }

  drop(event) {
    this.selectedMerticArrayObj.push(this.draggedMetric);
    this.selectedMerticArray.push(this.draggedMetric['alies']);
  }

  dragEnd(event) {
    this.draggedMetric = null;
  }

  onResetDerviedMetric() {
    this.selectedMerticArray = [];
    this.selectedMerticArrayObj = [];
    this.derivedMetricName = '';
    this.derivedMetricDescription = '';
    this.selectedformat = {};
    this.selectedApp = {};
    this.availableMetric = [];
  }

  onAddDerivedMetrics() {
    this.isValidDerviedMetric = false;
    let sbkt = 0;
    let ebkt = 0;
    if (this.selectedMerticArrayObj.length == 0)
      this.isValidDerviedMetric = false;
    else {
      this.selectedMerticArrayObj.forEach((element, idx) => {
        if (element['type'] = 'sbrt')
          sbkt = sbkt + 1;
        if (element['type'] = 'ebkt')
          ebkt = ebkt + 1;
        if (this.selectedMerticArrayObj.length == 1 && idx == 0 && (element['type'] == 'op' || element['type'] == 'ebkt')) {
          this.isValidDerviedMetric = false;
        }
        else if (element['type'] == 'metric') {
          if (this.selectedMerticArrayObj[idx - 1] != undefined && this.selectedMerticArrayObj[idx - 1]['type'] != 'op' && this.selectedMerticArrayObj[idx - 1]['type'] != 'sbrt') {
            this.isValidDerviedMetric = false;
          }
          if (this.selectedMerticArrayObj[idx + 1] != undefined && this.selectedMerticArrayObj[idx + 1]['type'] != 'op' && this.selectedMerticArrayObj[idx + 1]['type'] != 'ebrt' && this.selectedMerticArrayObj[idx + 1]['type'] != 'sbrt') {
            this.isValidDerviedMetric = false;
          }
        }
        else if (element['type'] == 'op') {
          if (this.selectedMerticArrayObj[idx - 1] != undefined && (this.selectedMerticArrayObj[idx - 1]['type'] != 'metric' && this.selectedMerticArrayObj[idx - 1]['type'] != 'ebrt' && this.selectedMerticArrayObj[idx - 1]['type'] != 'number')) {
            this.isValidDerviedMetric = false;
          }
          if (this.selectedMerticArrayObj[idx + 1] != undefined && (this.selectedMerticArrayObj[idx + 1]['type'] != 'metric') && (this.selectedMerticArrayObj[idx + 1]['type'] != 'sbrt')) {
            this.isValidDerviedMetric = false;
          }
        }
        else if (element['type'] == 'number') {
          if (this.selectedMerticArrayObj[idx - 1] != undefined && (this.selectedMerticArrayObj[idx - 1]['type'] != 'op' && this.selectedMerticArrayObj[idx - 1]['type'] != 'sbrt')) {
            this.isValidDerviedMetric = false;
          }
          if (this.selectedMerticArrayObj[idx + 1] != undefined && (this.selectedMerticArrayObj[idx + 1]['type'] != 'op') && (this.selectedMerticArrayObj[idx + 1]['type'] != 'ebrt')) {
            this.isValidDerviedMetric = false;
          }
        }
        else if (element['type'] == 'sbrt') {
          if (this.selectedMerticArrayObj[idx - 1] != undefined && this.selectedMerticArrayObj[idx - 1]['type'] != 'op') {
            this.isValidDerviedMetric = false;
          }
          else if (this.selectedMerticArrayObj[idx + 1] != undefined && (this.selectedMerticArrayObj[idx + 1]['type'] != 'metric' && this.selectedMerticArrayObj[idx - 1]['type'] != 'number')) {
            this.isValidDerviedMetric = false
          }
        }
        else if (element['type'] == 'ebrt') {
          if (this.selectedMerticArrayObj[idx - 1] != undefined && (this.selectedMerticArrayObj[idx - 1]['type'] != 'metric')) {
            this.isValidDerviedMetric = false;
          }
          else if (this.selectedMerticArrayObj[idx + 1] != undefined && (this.selectedMerticArrayObj[idx + 1]['type'] != 'op')) {
            this.isValidDerviedMetric = false
          }
        }
      });
      if (this.selectedMerticArrayObj[this.selectedMerticArrayObj.length - 1]['type'] == 'op' || this.selectedMerticArrayObj[this.selectedMerticArrayObj.length - 1]['type'] == 'sbrt') {
        this.isValidDerviedMetric = false;
      }
      if (ebkt % 2 == 0) {
        this.isValidDerviedMetric = false;
      }
      if (ebkt % 2 == 0) {
        this.isValidDerviedMetric = false;
      }
      this.isValidDerviedMetric = true;
      if (typeof this.selectedApp == "undefined" || this.selectedApp == null) {
        this.isAppSelected = false;
      }
      else {
        this.isAppSelected = true;
      }
      if (typeof this.derivedMetricName == "undefined" || this.derivedMetricName == null) {
        this.isDerivedMetricNameEnter = false;
      } else {
        this.isDerivedMetricNameEnter = true;
      }
      if (typeof this.selectedformat == "undefined" || this.selectedformat == null) {
        this.isFormatSelected = false;
      } else {
        this.isFormatSelected = true;
      }
      if (this.isValidDerviedMetric && this.isAppSelected && this.isDerivedMetricNameEnter && this.isFormatSelected) {
        const selectedMerticNamesArray = Object.assign([], this.selectedMerticArray);
        this.availableMetric.forEach((row: object) => {
          for (let i = 0; i < selectedMerticNamesArray.length; i++) {
            if (row["alies"] == selectedMerticNamesArray[i]) {
              selectedMerticNamesArray[i] = row["name"]
            }
          }
        });
        const newDerivedMetric = {
          name: this.derivedMetricName,
          description: this.derivedMetricDescription,
          format: this.selectedformat.alies,
          app_id: this.selectedApp.app_id,
          formula: selectedMerticNamesArray.join(' '),
          formula_display_name: this.selectedMerticArray.join(' '),
          formulaArr: this.selectedMerticArray
        }
        console.log("New derived metric --  ", newDerivedMetric);
        this.dataFetchServ.createNewDerivedMetric(newDerivedMetric).subscribe(data => {
          if (data['status'] === 0) {
            var details = "Please refresh the page";
            if (data['message']['message'].includes("Duplicate entry")) {
              details = "You have already created derived metrics by using same name.";
            }
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Change the name',
              detail: details
            });
            console.log(data['status_msg']);
            return;
          } else {
            this.toastService.displayToast({
              severity: 'success',
              summary: 'Added',
              detail: 'Derived Metrics Added Successfully.'
            });
            this.router.navigate(['/derived-metrics']);
            this.dialogService.dialogComponentRef.destroy();
          }
        });
      }
    }
  }

  onAddChips(e) {
    const arr = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.'];
    if (!isNaN(parseFloat(e.value))) {
      const obj = { alies: e.value, type: 'number' };
      this.selectedMerticArrayObj.push(obj);
    }
    else
      this.selectedMerticArray.splice(this.selectedMerticArray.length - 1);
  }

  goBack() {
    // this.router.navigate(['/derived-metrics']);
    const ref = this.dialogService.dialogComponentRef.destroy();
  }

  ngOnDestroy() {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }
}

