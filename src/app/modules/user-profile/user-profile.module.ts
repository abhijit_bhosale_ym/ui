import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserProfileComponent } from './user-profile.component';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';
import { PasswordModule } from 'primeng/password';
import { UserProfileRoutes } from './user-profile.routing.module';
import { RouterModule } from '@angular/router';
@NgModule({
  imports: [
    CommonModule,
    ButtonModule,
    FormsModule,
    PasswordModule,
    RouterModule.forChild(UserProfileRoutes)
  ],
  declarations: [UserProfileComponent]
})
export class UserProfileModule {}
