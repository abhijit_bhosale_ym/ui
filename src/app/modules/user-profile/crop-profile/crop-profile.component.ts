import { Component, OnInit } from '@angular/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { UsersService } from '../../../_services';

@Component({
  selector: 'ym-crop-profile',
  templateUrl: './crop-profile.component.html',
  styleUrls: ['./crop-profile.component.scss']
})
export class CropProfileComponent implements OnInit {
  imageChangedEvent: any = '';
  croppedImage: any = '';
  fileSelected = false;

  constructor(
    private userService: UsersService,
    public toastService: ToastService,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig
  ) {
    this.croppedImage = this.config['data']['user']['avatar'];
  }

  ngOnInit() {}

  fileChangeEvent(event: any): void {
    this.fileSelected = true;
    this.imageChangedEvent = event;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }
  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }

  cancel() {
    this.fileSelected = false;
    this.ref.close(null);
  }

  saveImage() {
    if (typeof this.croppedImage.split(',')[1] !== 'undefined') {
      const userInfo = {
        avatar: this.croppedImage
      };
      this.userService.updateUserDetails(userInfo).subscribe(
        res => {
          const data = res as {};
          if (data['status']) {
            this.toastService.displayToast({
              severity: 'success',
              summary: 'Update successful',
              detail: 'User details updated successfully'
            });
            this.ref.close(this.croppedImage);
          } else {
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Something went wrong',
              detail: 'Please refresh page if problem persist contact us'
            });
            this.ref.close(this.croppedImage);
          }
        },
        error => {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Something went wrong',
            detail: 'Please refresh page if problem persist contact us'
          });
          this.ref.close(this.croppedImage);
        }
      );
    } else {
      this.toastService.displayToast({
        severity: 'info',
        summary: 'Something went wrong',
        detail: 'Please select new image first'
      });
    }
    this.fileSelected = false;
  }
}
