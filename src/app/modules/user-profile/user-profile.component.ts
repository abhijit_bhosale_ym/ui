import {
  Component,
  OnInit,
  ViewChild,
  ViewContainerRef,
  OnDestroy
} from '@angular/core';
import { PlatformConfigService, CommonLibService } from '../../_services';
import { CropProfileComponent } from './crop-profile/crop-profile.component';
import { DialogService } from 'primeng/dynamicdialog';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { Subscription } from 'rxjs';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'ym-app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit, OnDestroy {
  private platConfigObs: Subscription;

  constructor(
    public dialogService: DialogService,
    private platformConfigService: PlatformConfigService,
    private toastService: ToastService,
    public libServ: CommonLibService,
    private _titleService: Title
  ) { }
  config: object;

  ngOnInit() {
    this.platConfigObs = this.platformConfigService.obsConfig.subscribe(res => {
      if (!this.libServ.isEmptyObj(res)) {
        this.config = res;
        this._titleService.setTitle('User Profile');
      }
    });
  }

  updatePic() {
    const ref = this.dialogService.open(CropProfileComponent, {
      header: 'Choose a Profile Picture',
      contentStyle: { 'max-height': '80vh', overflow: 'auto' },
      data: this.config
    });

    ref.onClose.subscribe((img: string) => {
      if (img != null) {
        this.config['user']['avatar'] = img;
        // Call API to save profile pic
        const req = {
          email: this.config['user']['email'],
          avatar: this.config['user']['avatar']
        };

        this.platformConfigService
          .setPlatformConfigInfoData(req)
          .subscribe(res => {
            if (res['status']) {
              this.toastService.displayToast({
                severity: 'success',
                summary: 'Picture updated',
                detail: 'Profile Picture Updated'
              });
            } else {
              this.toastService.displayToast({
                severity: 'error',
                summary: 'Update Error',
                detail: 'Profile Picture Error'
              });
            }
          });
      }
    });
  }

  editProfile() {
    const ref = this.dialogService.open(EditProfileComponent, {
      header: 'Edit Profile',
      contentStyle: { 'max-height': '80vh', width: '36vw', overflow: 'auto' },
      data: this.config
    });

    ref.onClose.subscribe((data: string) => { });
  }

  editPassword() {
    const ref = this.dialogService.open(ChangePasswordComponent, {
      header: 'Change Password',
      contentStyle: { 'max-height': '80vh', overflow: 'auto' },
      data: this.config
    });

    ref.onClose.subscribe((data: string) => { });
  }

  ngOnDestroy(): void {
    if (this.platConfigObs && !this.platConfigObs.closed) {
      this.platConfigObs.unsubscribe();
    }
  }
}
