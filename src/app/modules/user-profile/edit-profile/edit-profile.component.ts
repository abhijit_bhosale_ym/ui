import { Component, OnInit } from '@angular/core';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng/dynamicdialog';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { UsersService } from '../../../_services';
import { PlatformConfigService } from 'src/app/_services/platform-config/platform-config.service';

@Component({
  selector: 'ym-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {
  displayName: string;
  contactEmail: string[];

  constructor(
    private userService: UsersService,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    public toastService: ToastService,
    private platformConfigService: PlatformConfigService,
  ) {
    this.displayName = this.config['data']['user']['userName'];
    const contactEmail1 = this.config['data']['user']['contactEmail'];
    this.contactEmail = contactEmail1.length ? contactEmail1.split(',') : '';
  }

  ngOnInit() {}

  saveProfileChanges() {
    const userInfo = {
      name: this.displayName,
      contactEmail: this.contactEmail.length ? this.contactEmail.join(',') : ''
    };
    this.userService.updateUserDetails(userInfo).subscribe(
      res => {
        const data = res as {};
        if (data['status']) {
          this.config['data']['user']['userName'] = userInfo.name;
          this.config['data']['user']['contactEmail'] = userInfo.contactEmail;
          // setTimeout(() => {
            this.platformConfigService.setPlatformConfig(this.config['data']);
          // }, 0);

          this.toastService.displayToast({
            severity: 'success',
            summary: 'Update successful',
            detail: 'User details updated successfully'
          });
        }
        this.ref.close(null);
      },
      error => {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Something went wrong',
          detail: 'Please refresh page if problem persist contact us'
        });
        this.ref.close(null);
      }
    );
  }

  cancel() {
    this.ref.close(null);
  }
}
