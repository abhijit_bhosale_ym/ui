import { Component, OnInit } from '@angular/core';
import {
  Validators,
  FormControl,
  FormGroup,
  FormBuilder,
  AbstractControl
} from '@angular/forms';
import { DynamicDialogRef, DialogService } from 'primeng/dynamicdialog';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { UsersService } from '../../../_services';
import { Router } from '@angular/router';
@Component({
  selector: 'ym-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  oldPassword: string;
  newPassword: string;
  confirmPassword: string;
  userform: FormGroup;
  showPassword: object = {
    old: false,
    new: false,
    confirm: false
  };

  constructor(
    private userService: UsersService,
    private toastService: ToastService,
    public ref: DynamicDialogRef,
    private fb: FormBuilder,
    private router: Router,
    private dialogService : DialogService
  ) { }

  ngOnInit() {
    this.userform = this.fb.group({
      oldpassword: new FormControl('', Validators.required),
      newpassword: new FormControl('', Validators.required),
      confirmpassword: new FormControl('', Validators.required)
    });
  }

  togglePassword(pass: string) {
    this.showPassword[pass] = !this.showPassword[pass];
  }

  savePassword(values) {
    const userInfo = {
      oldPassword: values['oldpassword'],
      newPassword: values['newpassword']
    };
    this.userService.updateUserDetails(userInfo).subscribe(
      res => {
        const data = res as {};
        if (data['status']) {
          this.toastService.displayToast({
            severity: 'success',
            summary: 'Update successful',
            detail: 'User details updated successfully'
          });
          this.ref.close(null);
          localStorage.clear();
          this.router.navigate(['/login']);
        } else {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Update unsuccessful',
            detail: 'User credentials does not match'
          });
        }
      },
      error => {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Something went wrong',
          detail: 'Please refresh page if problem persist contact us'
        });
        this.ref.close(null);
      }
    );
  }

  validateField(confirmControl: AbstractControl, newControl: AbstractControl) {
    if (confirmControl.value) {
      if (confirmControl.value != newControl.value) {
        confirmControl.setErrors({ 'missMatch': true });
      }
      if (confirmControl.value == newControl.value) {
        confirmControl.clearValidators();
        confirmControl.updateValueAndValidity();
      }
    }
  }

  cancel() {
    this.dialogService.dialogComponentRef.destroy();
  }
}
