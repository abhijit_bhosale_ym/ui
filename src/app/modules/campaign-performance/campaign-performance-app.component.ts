import { Component, OnInit, OnDestroy } from '@angular/core';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { AppConfigService } from '../../_services/app-config/app-config.service';
import { CommonLibService } from '../../_services';
import { Title } from '@angular/platform-browser';
import { FetchApiDataService } from './fetch-api-data.service';
import { ExportdataService } from '../../_services/export/exportdata.service';
import { ExportPptService } from '../../_services/export/export-ppt.service';
import { HtmltoimageService } from '../../_services/screencapture/htmltoimage.service';
import { ToastService } from '../../_services/toast-notification/toast.service';
import { Subscription } from 'rxjs';
import { SendEmailComponent } from '../send-email/send-email.component';
import { ExportRequest } from '../../_interfaces/exportRequest';
import { FormatNumPipe } from '../../_pipes/number-format.pipe';
import * as moment from 'moment';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { CampaignPerformanceDailyDataPopupComponent } from './campaign-performance-daily-data-popup/campaign-performance-daily-data-popup.component';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-campaign-performance-app',
  templateUrl: './campaign-performance-app.component.html',
  styleUrls: ['./campaign-performance-app.component.scss']
})
export class CampaignPerformanceAppComponent implements OnInit, OnDestroy {

  lastUpdatedOn: Date;
  dataUpdatedThrough: Date;
  nextUpdated: Date;

  exportRequest: ExportRequest = <ExportRequest>{};
  appConfigObs: Subscription;
  appConfig: object;
  tableRequestParam = {};
  filtersApplied: object = {};

  cardsJson = [];
  showCards = true;

  // filtersApplied: { timeKeyFilter: { time_key1: any; time_key2: any; }; filters: { dimensions: any[]; metrics: any[]; }; groupby: any[]; };

  showMainPieChart: boolean;
  mainPieChartJson: object;
  noDataMainPieChart: boolean;

  campaignsEndIn3DayTableData: TreeNode[];
  campaignsEndIn3DayTableJson: object;

  showChartsData: boolean = false;
  chartsMultiselectButton: object = {
    data: [
      {
        label: 'Amount Spent',
        value: 'amount_spent'
      },
      {
        label: 'Leads',
        value: 'leads'
      },
      {
        label: 'Results',
        value: 'results'
      },
      {
        label: 'Impressions',
        value: 'impressions'
      },
      {
        label: 'Cost Per Lead',
        value: 'cost_per_lead'
      }
    ],
    model: ['amount_spent']
  };
  chartsMultiselectButtonDimension: object = {
    data: [{
      label: 'Campaign',
      value: 'campaign_name'
    },
    {
      label: 'Source',
      value: 'source'
    }

    ],
    model: ['campaign_name']
  };
  showCharts: boolean = false;
  amountSpendChartJson: any;
  leadsChartJson: any;
  resultsChartJson: any;
  impressionsChartJson: any;
  cplChartJson: any;
  defaultChartsJson: any;

  flag: boolean = true;

  noTableData: boolean = true;
  displayAggTable: boolean;
  aggTableData: TreeNode[];
  aggTableJson: object;
  aggColDef: any[];
  dimensionsDropdown: any[];
  selectedDimension: any;
  metricsDropdown: any[];
  metricChartJson: any;
  selectedMetric: any;
  tableReq: object;
  isSourceRevenue = false;
  appFeaturesConfigs = {};
  mainCardsConfig = {};
  constructor(
    private appConfigService: AppConfigService,
    private libServ: CommonLibService,
    private _titleService: Title,
    private htmltoimage: HtmltoimageService,
    private pptExport: ExportPptService,
    private dialogService: DialogService,
    private dataFetchServ: FetchApiDataService,
    private toastService: ToastService,
    private formatNumPipe: FormatNumPipe,
    private confirmationService: ConfirmationService
  ) { }

  ngOnInit(): void {
    this.dimensionsDropdown = [
      {
        field: 'source',
        displayName: 'Source'
      },
      {
        field: 'campaign_name',
        displayName: 'Campaign'
      }
    ];
    this.selectedDimension = 'campaign_name';
    this.selectedMetric = "amount_spent";

    this.defaultChartsJson = {
      chartTypes: [
        { key: 'line', label: 'Linear Trend' },
        { key: 'bar', label: 'Stack Trend', stacked: true }
      ],
      chartData: {
        labels: [],
        datasets: []
      },
      chartOptions: {
        title: {
          display: true,
          text: ''
        },
        legend: {
          display: true
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              display: true,
              scaleLabel: {
                display: true
              }
            }
          ]
        },
        pan: {
          enabled: true,
          mode: 'x'
        },
        zoom: {
          enabled: true,
          mode: 'x'
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '243px'
    };


    this.aggColDef = [
      {
        field: 'campaign_name',
        displayName: 'Campaign Name',
        format: '',
        width: '200',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'ad_set_name',
        displayName: 'Ad Set Name',
        format: '',
        width: '180',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      }, {
        field: 'ad_name',
        displayName: 'Ad Name',
        format: '',
        width: '180',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'campaign_id',
        displayName: 'Campaign Id',
        format: '',
        width: '125',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'delivery_status',
        displayName: 'Delivery Status',
        format: '',
        width: '115',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      }, {
        field: 'result_type',
        displayName: 'Result Type',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'results',
        displayName: 'Results',
        format: 'number',
        width: '90',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'impressions',
        displayName: 'Impressions',
        format: 'number',
        width: '115',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'reach',
        displayName: 'Reach',
        format: 'number',
        width: '90',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'cost_per_result',
        displayName: 'Cost Per Result',
        format: '₹',
        width: '90',
        exportConfig: {
          format: 'floatNumber',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'amount_spent',
        displayName: 'Amount Spent (₹)',
        format: '₹',
        width: '110',
        exportConfig: {
          format: 'floatNumber',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'leads',
        displayName: 'Leads',
        format: 'number',
        width: '90',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'cost_per_lead',
        displayName: 'Cost Per Lead',
        format: '₹',
        width: '90',
        exportConfig: {
          format: 'floatNumber',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'link_clicks',
        displayName: 'Link Clicks',
        format: 'number',
        width: '90',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'cpm_social',
        displayName: 'CPM',
        format: '₹',
        width: '90',
        exportConfig: {
          format: 'floatNumber',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'ctr_social',
        displayName: 'CTR',
        format: 'percentage',
        width: '90',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'cpc_social',
        displayName: 'CPC',
        format: '₹',
        width: '90',
        exportConfig: {
          format: 'floatNumber',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'clicks',
        displayName: 'Clicks (All)',
        format: 'number',
        width: '110',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'page_engagement',
        displayName: 'Page Engagement',
        format: 'number',
        width: '140',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'page_likes',
        displayName: 'Page Likes',
        format: 'number',
        width: '100',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'post_comments',
        displayName: 'Post Comments',
        format: 'number',
        width: '110',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'post_engagement',
        displayName: 'Post Engagement',
        format: 'number',
        width: '120',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'post_reactions',
        displayName: 'Post Reactions',
        format: 'number',
        width: '120',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'post_saves',
        displayName: 'Post Saves',
        format: 'number',
        width: '90',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'post_shares',
        displayName: 'Post Shares',
        format: 'number',
        width: '110',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      }
    ];
    this.aggTableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      reload: false,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,

      columns: this.libServ.deepCopy(this.aggColDef.slice(3)),
      selectedColumns: this.libServ.deepCopy(this.aggColDef.slice(3)),
      globalFilterFields: this.libServ.deepCopy(this.aggColDef.slice(0)),
      frozenCols: this.libServ.deepCopy([...this.aggColDef.slice(0, 3)]),
      frozenWidth:
        this.aggColDef
          .slice(0, 3)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };

    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {
        this.appConfig = appConfig;
        console.log('appConfig', this.appConfig);

        this._titleService.setTitle(this.appConfig['displayName']);
        // let startDate;
        // let endDate;
        // const date_config = this.appConfig['filter']['filterConfig']['filters'][
        //   'datePeriod'
        // ][0];
        // if ((date_config['defaultDate'][0]['value'] === 0 && date_config['defaultDate'][0]['period'] === 'month') && (moment().format('DD') === '01' || moment().format('DD') === '02')) {
        //   startDate = moment().subtract(1, 'months').startOf('month');
        //   endDate = moment().subtract(1, 'months').endOf('month');
        // } else {
        //   if (
        //     date_config[
        //     'defaultDate'
        //     ][0]['startOf']
        //   ) {
        //     startDate = moment()
        //       .subtract(
        //         date_config['defaultDate'][0]['value'],
        //         date_config['defaultDate'][0]['period']
        //       )
        //       .startOf(
        //         date_config['defaultDate'][0]['period']
        //       );
        //   } else {
        //     startDate = moment().subtract(
        //       date_config['defaultDate'][0]['value'],
        //       date_config['defaultDate'][0]['period']
        //     );
        //   }
        //   endDate = moment().subtract(
        //     date_config[
        //     'defaultDate'
        //     ][1]['value'],
        //     date_config[
        //     'defaultDate'
        //     ][1]['period']
        //   );
        // }

        this.filtersApplied = {
          timeKeyFilter: {
            time_key1: '20201101',
            time_key2: moment().format('YYYYMMDD')
          },
          filters: { dimensions: [], metrics: [] },
          groupby: ['campaign_name', 'creative_name']
        };
        const dateArr = [];
        // let startDateCp = moment(this.filtersApplied.timeKeyFilter.time_key1, 'YYYYMMDD');
        // const endDateCp = moment(this.filtersApplied.timeKeyFilter.time_key2, 'YYYYMMDD');
        // while (startDateCp.isSameOrBefore(endDateCp)) {
        //   dateArr.push(startDateCp.format('DD/MM/YYYY'));
        //   startDateCp.add(1, 'day');
        // }
        // this.filtersApplied.filters.dimensions.push({
        //   key: 'start_date',
        //   values: dateArr
        // });

        this.exportRequest['appName'] = this.appConfig['name'].toString();
        this.exportRequest['sendEmail'] = this.appConfig['user'][
          'contactEmail'
        ].split(',');
        this.tableRequestParam = {
          dimensions: [],
          metrics: [],
          derived_metrics: [],
          timeKeyFilter: this.filtersApplied['timeKeyFilter'],
          filters: { dimensions: [], metrics: [] },
          groupByTimeKey: { key: [], interval: 'daily' },
          gidGroupBy: ['source', 'campaign_id', 'campaign_name', 'ad_set_name', 'ad_name', 'delivery_status', 'result_type'],
          orderBy: [
            { key: '', opcode: '' }
          ],
          limit: '',
          offset: ''
        };
        this.initialLoading();
      }
    });

  }

  initialLoading() {

    this.dataFetchServ
      .getLastUpdatedData(this.appConfig['id'])
      .subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          return;
        }
        this.lastUpdatedOn = moment(data[0]['updated_at']).toDate();
        this.nextUpdated = moment(data[0]['next_run_at']).toDate();
        this.dataUpdatedThrough = moment(
          data[0]['source_updated_through'],
          'YYYYMMDD'
        ).toDate();
      });

      this.libServ.getAppFeaturesConfigs(this.appConfig['id']).subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: data['message']
          });
          console.log(data['message']);
          return;
        }
        this.appFeaturesConfigs = data['data'];
        this.appFeaturesConfigs['cardsConfig']['cards'].forEach(element => {
          if (element['name'] === 'main') {
            this.mainCardsConfig = element['config'];
            this.cardsJson = this.mainCardsConfig['list'];
            if (this.mainCardsConfig['display']) {
              this.loadCards();
            }
          }
        });
      });
    this.loadCampaignStatusChart();
    this.loadCharts();

    this.tableReq = this.libServ.deepCopy(this.tableRequestParam);

    this.tableReq['dimensions'] = ['campaign_id', 'campaign_name', 'delivery_status', 'result_type'];
    this.tableReq['metrics'] = ['results', 'reach', 'impressions', 'amount_spent', 'leads', 'link_clicks',
      'clicks', 'page_engagement', 'page_likes', 'post_comments', 'post_engagement', 'post_reactions', 'post_saves', 'post_shares'];
    this.tableReq['derived_metrics'] = ['cost_per_result', 'cost_per_lead', 'cpm_social', 'ctr_social', 'cpc_social']
    this.tableReq['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);
    this.tableReq['gidGroupBy'] = ['source', 'campaign_id', 'campaign_name', 'ad_set_name', 'ad_name', 'delivery_status', 'result_type'],
      this.tableReq['orderBy'] = [{ key: 'campaign_id', opcode: 'asc' }],
      this.tableReq['groupByTimeKey'] = { key: ['time_key'], interval: 'daily' },
      this.tableReq['limit'] = '',
      this.tableReq['offset'] = ''
    this.loadTableMainData(this.tableReq);
  }

  loadCampaignStatusChart() {
    const request = {
      dimensions: ['delivery_status', 'campaign_name'],
      metrics: [],
      derived_metrics: [],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.filtersApplied['filters'],
      groupByTimeKey: {
        key: ['time_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source', 'campaign_id', 'campaign_name', 'ad_set_name', 'ad_name', 'delivery_status', 'result_type'],
      orderBy: [{ key: 'campaign_name', opcode: 'asc' }],
      limit: '',
      offset: ''
    };
    this.mainPieChartJson = {
      chartTypes: [{ key: 'doughnut', label: 'Pie Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Campaigns Running'
        },
        legend: {
          display: false
        },
        elements: {
          arc: {
            borderWidth: 0
          }
        },
        plugins: {
          datalabels: {
            formatter: (value, ctx) => {
              let sum = 0;
              const dataArr = ctx.chart.data.datasets[0].data;
              dataArr.map(data => {
                sum += data;
              });
              return (value * 100) / sum;
            },
            color: 'black'
          }
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '350px'
    };
    this.mainPieChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          let sum = 0;
          const dataArr = data.datasets[0].data;
          dataArr.map(data => {
            sum += data;
          });
          const percentage = (currentValue * 100) / sum;
          return `${
            data.labels[tooltipItem.index]
            } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
        }
      }
    };
    this.showMainPieChart = false;
    this.dataFetchServ.getCPRData(request).subscribe(data => {
      const chartData = data['data'];
      this.noDataMainPieChart = false;
      if (!chartData.length) {
        this.noDataMainPieChart = true;
        return;
      }
      let active = 0;
      let inactive = 0;
      chartData.forEach(element => {
        if (element.delivery_status == 'Active') {
          active++;
        } else {
          inactive++;
        }
      });
      const data1 = [active, inactive];
      const sourceArr = ['Active', 'Inactive'];
      const colors = ['#00b000', '#fa9937'];
      this.mainPieChartJson['chartData']['labels'] = sourceArr;
      this.mainPieChartJson['chartData']['datasets'][0]['data'] = data1;
      this.mainPieChartJson['chartData']['datasets'][0]['backgroundColor'] = colors;
      setTimeout(() => {
        this.showMainPieChart = true
      }, 0);
    });
  }

  loadTableMainData(tableReq) {
    this.dataFetchServ.getCPRData(tableReq).subscribe(data => {
      if (data['status'] === 0) {
        this.noTableData = true;
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        return;
      } else {
        this.noTableData = false;
      }
      const tableData = data['data'] as [];
      const arr = [];
      tableData.forEach((row: object) => {
        row['ad_set_name'] = 'All';
        row['ad_name'] = 'All';
        arr.push({
          data: row,
          children: [{ data: {} }]
        });
      });
      this.aggTableData = <TreeNode[]>arr;
      this.aggTableJson['totalRecords'] = arr.length;
      setTimeout(() => {
        this.aggTableJson['loading'] = false;
        this.displayAggTable = true;
      });
    });
  }

  loadCharts() {
    let selectedDimension = this.selectedDimension;
    const chartReq = {
      dimensions: [selectedDimension, 'time_key'],
      metrics: [
        'amount_spent',
        'leads',
        'results',
        'impressions'
      ],
      derived_metrics: ['cost_per_lead'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source', 'campaign_id', 'campaign_name', 'ad_set_name', 'ad_name', 'delivery_status', 'result_type'],
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };
    // Reset Data
    this.amountSpendChartJson = this.libServ.deepCopy(this.defaultChartsJson);
    this.amountSpendChartJson['chartOptions']['title'] = {
      display: true,
      text: `Amount Spent (₹) by ${
        this.dimensionsDropdown.find(x => x.field == selectedDimension)
          .displayName
        } Trend`
    };
    this.amountSpendChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
      // Include a dollar sign in the ticks
      callback: (value, index, values) => {
        return this.formatNumPipe.transform(value, '₹', [0]);
      }
    };
    this.amountSpendChartJson['chartOptions']['scales']['yAxes'][0]['scaleLabel'][
      'labelString'
    ] = 'Amount Spent (₹)';
    this.amountSpendChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return `${
            data.datasets[tooltipItem.datasetIndex].label
            } : ${this.formatNumPipe.transform(currentValue, '₹', [2])}`;
        }
      }
    };
    this.leadsChartJson = this.libServ.deepCopy(this.defaultChartsJson);
    // this.leadsChartJson['chartTypes'] = [{ key: 'line', label: 'Linear Trend' }];
    this.leadsChartJson['chartOptions']['title'] = {
      display: true,
      text: `Leads by ${
        this.dimensionsDropdown.find(x => x.field == this.selectedDimension)
          .displayName
        } Trend`
    };
    this.leadsChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
      // Include a dollar sign in the ticks
      callback: (value, index, values) => {
        return this.formatNumPipe.transform(value, 'number', []);
      }
    };
    this.leadsChartJson['chartOptions']['scales']['yAxes'][0]['scaleLabel'][
      'labelString'
    ] = 'Leads';
    this.leadsChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return `${
            data.datasets[tooltipItem.datasetIndex].label
            } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
        }
      }
    };
    this.resultsChartJson = this.libServ.deepCopy(this.defaultChartsJson);
    // this.resultsChartJson['chartOptions']['title']['text'] = 'Sell Through Rate Trend';
    this.resultsChartJson['chartOptions']['title'] = {
      display: true,
      text: `Results by ${
        this.dimensionsDropdown.find(x => x.field == this.selectedDimension)
          .displayName
        } Trend`
    };
    this.resultsChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
      callback: (value, index, values) => {
        return this.formatNumPipe.transform(value, 'number', []);
      }
    };
    this.resultsChartJson['chartOptions']['scales']['yAxes'][0]['scaleLabel'][
      'labelString'
    ] = 'Results';
    this.resultsChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return `${
            data.datasets[tooltipItem.datasetIndex].label
            } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
        }
      }
    };
    this.impressionsChartJson = this.libServ.deepCopy(this.defaultChartsJson);
    this.impressionsChartJson['chartOptions']['title']['text'] =
      'Impressions Trend';
    this.impressionsChartJson['chartOptions']['title'] = {
      display: true,
      text: `Impressions by ${
        this.dimensionsDropdown.find(x => x.field == this.selectedDimension)
          .displayName
        } Trend`
    };
    this.impressionsChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
      // Include a dollar sign in the ticks
      callback: (value, index, values) => {
        return this.formatNumPipe.transform(value, 'number', []);
      }
    };
    this.impressionsChartJson['chartOptions']['scales']['yAxes'][0][
      'scaleLabel'
    ]['labelString'] = 'Impressions';
    this.impressionsChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return `${
            data.datasets[tooltipItem.datasetIndex].label
            } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
        }
      }
    };

    this.cplChartJson = this.libServ.deepCopy(this.defaultChartsJson);
    this.cplChartJson['chartOptions']['title'] = {
      display: true,
      text: `Cost Per Lead by ${
        this.dimensionsDropdown.find(x => x.field == this.selectedDimension)
          .displayName
        } Trend`
    };
    this.cplChartJson['chartOptions']['scales']['yAxes'][0]['ticks'] = {
      // Include a dollar sign in the ticks
      callback: (value, index, values) => {
        return this.formatNumPipe.transform(value, '₹', [0]);
      }
    };
    this.cplChartJson['chartOptions']['scales']['yAxes'][0][
      'scaleLabel'
    ]['labelString'] = 'Cost Per Lead';
    this.cplChartJson['chartOptions']['tooltips'] = {
      callbacks: {
        label: (tooltipItem, data) => {
          const currentValue =
            data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
          return `${
            data.datasets[tooltipItem.datasetIndex].label
            } : ${this.formatNumPipe.transform(currentValue, '₹', [2])}`;
        }
      }
    };


    this.showCharts = false;
    this.showChartsData = false;
    this.dataFetchServ.getCPRData(chartReq).subscribe(data => {
      const chartData = data['data'];
      // console.log('dataa', chartData);
      if (typeof chartData === 'undefined' || !chartData.length) {
        this.showCharts = true;
      } else {

        const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));

        const sources = Array.from(
          new Set(chartData.map(s => s[this.selectedDimension]))
        );
        const colors = this.libServ.dynamicColors(sources.length);
        this.amountSpendChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );
        this.leadsChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );
        this.resultsChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );
        this.impressionsChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );

        this.cplChartJson['chartData']['labels'] = datesArr.map(d =>
          moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
        );

        sources.forEach((src, i) => {
          const amountArr = [];
          const leadArr = [];
          const resultArr = [];
          const impArr = [];
          const cplArr = [];

          datesArr.forEach(time_key => {
            let isNotavailable = true;
            chartData.forEach(r => {
              if (r[this.selectedDimension] == src && r['time_key'] == time_key) {
                amountArr.push(r['amount_spent']);
                leadArr.push(r['leads']);
                resultArr.push(r['results']);
                impArr.push(r['impressions']);
                cplArr.push(r['cost_per_lead']);
                isNotavailable = false;
              }
            });
            if (isNotavailable) {
              amountArr.push(0);
              leadArr.push(0);
              resultArr.push(0);
              impArr.push(0);
              cplArr.push(0);

            }
          });

          this.amountSpendChartJson['chartData']['datasets'].push({
            label: src,
            data: amountArr,
            borderColor: colors[i],
            fill: false,
            backgroundColor: colors[i]
          });
          this.leadsChartJson['chartData']['datasets'].push({
            label: src,
            data: leadArr,
            borderColor: colors[i],
            fill: false,
            backgroundColor: colors[i]
          });
          this.resultsChartJson['chartData']['datasets'].push({
            label: src,
            data: resultArr,
            borderColor: colors[i],
            fill: false,
            backgroundColor: colors[i]
          });
          this.impressionsChartJson['chartData']['datasets'].push({
            label: src,
            data: impArr,
            borderColor: colors[i],
            fill: false,
            backgroundColor: colors[i]
          });
          this.cplChartJson['chartData']['datasets'].push({
            label: src,
            data: cplArr,
            borderColor: colors[i],
            fill: false,
            backgroundColor: colors[i]
          });
        });
        this.getChartJson();
      }
    });
  }

  loadCards() {
    this.showCards = false;
    const apiUrl = this.mainCardsConfig['api']['url'];
    const cardsReq = this.libServ.deepCopy(
      this.mainCardsConfig['api']['request']
    );
    cardsReq['timeKeyFilter'] = this.libServ.deepCopy(
      this.filtersApplied['timeKeyFilter']
    );
    cardsReq['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);
    cardsReq['gidGroupBy'] = ['source', 'campaign_id', 'campaign_name', 'ad_set_name', 'ad_name', 'delivery_status', 'result_type'],
      this.libServ.getDataFromDynamicAPI(apiUrl, cardsReq).subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error - Contact to administator',
            detail: data
          });
          console.log(data['status_msg']);
          return;
        }
        if (!this.libServ.isEmptyObj(data['data'])) {
          this.cardsJson.map(o => {
            o['value'] = data['data'][0][o['field']];
          });
        } else {
          this.cardsJson.map(o => {
            o['value'] = 0;
          });
        }
        this.showCards = this.mainCardsConfig['display'];
      });
  }


  onFiltersApplied(filterData: object) {
    console.log("filterData", filterData);

    const dim = {};
    this.filtersApplied['filters']['dimensions'] = [];
    for (const k in filterData['filter']['dimensions']) {
      if (filterData['filter']['dimensions'][k].length !== 0) {
        this.filtersApplied['filters']['dimensions'].push({
          key: k,
          values: filterData['filter']['dimensions'][k]
        });
      }
    }
    console.log(this.filtersApplied);
    this.initialLoading();
  }


  onTableDrill(e: Event) {
    if (!e['node']['childLoaded']) {
      this.aggTableJson['loading'] = true;
      this.tableReq = this.libServ.deepCopy(this.tableRequestParam);

      this.tableReq['dimensions'] = ['campaign_id', 'delivery_status', 'result_type'];
      this.tableReq['metrics'] = ['results', 'reach', 'impressions', 'amount_spent', 'leads', 'link_clicks',
        'clicks', 'page_engagement', 'page_likes', 'post_comments', 'post_engagement', 'post_reactions', 'post_saves', 'post_shares'];
      this.tableReq['derived_metrics'] = ['cost_per_result', 'cost_per_lead', 'cpm_social', 'ctr_social', 'cpc_social']
      this.tableReq['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);
      this.tableReq['gidGroupBy'] = ['source', 'campaign_id', 'campaign_name', 'ad_set_name', 'ad_name', 'delivery_status', 'result_type'],
        this.tableReq['orderBy'] = [{ key: 'campaign_id', opcode: 'asc' }],
        this.tableReq['groupByTimeKey'] = { key: ['time_key'], interval: 'daily' },
        this.tableReq['limit'] = '',
        this.tableReq['offset'] = ''


      const grpBys = ['campaign_name', 'ad_set_name', 'ad_name']; // this.filtersApplied['groupby'].map(e => e.key);
      for (const g of grpBys) {
        if (e['node']['data'][g] !== 'All') {
          this.tableReq['dimensions'].push(g);
          // if (!this.tableReq['gidGroupBy'].includes(g)) {
          //   this.tableReq['gidGroupBy'].push(g);
          // }
          if (
            this.tableReq['filters']['dimensions'].findIndex(e => e.key === g) === -1
          ) {
            this.tableReq['filters']['dimensions'].push({
              key: g,
              values: [e['node']['data'][g]]
            });
          } else {
            this.tableReq['filters']['dimensions'].splice(
              this.tableReq['filters']['dimensions'].findIndex(e => e.key === g),
              1
            );
            this.tableReq['filters']['dimensions'].push({
              key: g,
              values: [e['node']['data'][g]]
            });
            if (
              this.tableReq['filters']['dimensions']
                .find(e => e.key === g)
              ['values'].findIndex(v => v === e['node']['data'][g]) === -1
            ) {
              this.tableReq['filters']['dimensions'].push(e['node']['data'][g]);
            }
          }
        }
        else {
          this.tableReq['dimensions'].push(g);
          break;
        }
      }

      // this.tableReq['gidGroupBy'] = this.getGrpBys();
      // For Dev purpose
      // this.tableReq['gidGroupBy'] = [
      //   'ad_size',
      //   'ad_type',
      //   'ad_units',
      //   'bidding_type',
      //   'device_category',
      //   'geography',
      //   'sales_channel',
      //   'sales_type',
      //   'site',
      //   'source'
      // ];

      this.dataFetchServ.getCPRData(this.tableReq).subscribe(data => {
        if (data['status'] === 0) {
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Server Error',
            detail: 'Please refresh the page'
          });
          console.log(data['status_msg']);
          return;
        }
        const childData = data['data'];
        const arr = [];

        childData.forEach((row: object) => {
          this.aggTableJson['frozenCols'].forEach(r => {
            if (typeof row[r['field']] === 'undefined') {
              row[r['field']] = 'All';
            }
          });
          let obj = {};
          if (row[grpBys[grpBys.length - 1]] === 'All') {
            obj = {
              data: row,
              children: [{ data: {} }]
            };
          } else {
            obj = {
              data: row
            };
          }
          obj['data']['isExpanded'] = '1';
          arr.push(obj);
        });

        this.aggTableJson['loading'] = false;
        e['node']['children'] = <TreeNode[]>arr;
        this.aggTableData = [...this.aggTableData];
        e['node']['childLoaded'] = true;
      });

    }
  }

  openPopup(row, field) {
    const grpBys = ['campaign_name', 'ad_set_name', 'ad_name'];
    // this.filtersApplied['groupby'].map(e => e.key);
    console.log(row, field)
    let filtersApplied = this.libServ.deepCopy(this.filtersApplied);
    for (const g of grpBys) {
      if (row[g] !== 'All') {
        if (
          filtersApplied['filters']['dimensions'].findIndex(e => e.key === g) === -1
        ) {
          filtersApplied['filters']['dimensions'].push({
            key: g,
            values: [row[g]]
          });
        } else {
          filtersApplied['filters']['dimensions'].splice(
            filtersApplied['filters']['dimensions'].findIndex(e => e.key === g),
            1
          );
          filtersApplied['filters']['dimensions'].push({
            key: g,
            values: [row[g]]
          });
          if (
            filtersApplied['filters']['dimensions']
              .find(e => e.key === g)
            ['values'].findIndex(v => v === row[g]) === -1
          ) {
            filtersApplied['filters']['dimensions'].push(row[g]);
          }
        }

        if (g == field) {
          break;
        }
      }
    }

    const data = {
      row: row,
      filtersApplied: filtersApplied,
      exportRequest: this.exportRequest,
      appName: this.exportRequest['appName'],
      email: this.exportRequest['sendEmail'],
      field: field
    };
    console.log('filterdata', data)
    const ref = this.dialogService.open(CampaignPerformanceDailyDataPopupComponent, {
      header: "Daily Distribution :" + row[field],
      contentStyle: { width: '90vw', height: 'auto', overflow: 'auto' },
      data: data
    });
  }

  isHiddenColumn(col: Object) {
    return (
      this.aggTableJson['selectedColumns'].some(
        (c: Object) => c['field'] === col['field']
      ) ||
      this.aggTableJson['frozenCols'].some(
        (c: Object) => c['field'] === col['field']
      )
    );
  }


  captureFn(type) {
    if (type === 'image') {
      const imageConfig = {
        imgName: `${this.appConfig['displayName']}.png`,
        type: 'image/png',
        download: true
      };
      this.htmltoimage.canvasImage(
        document.getElementById('capture'),
        imageConfig
      );
    } else if (type === 'ppt') {
      const chartElements = document.getElementsByTagName('ym-charts');
      const slides = [];
      [].forEach.call(chartElements, (el, index) => {
        const c = el.getElementsByTagName('canvas')[0];

        slides.push({
          title: `${this.appConfig['displayName']}`,
          base64data: this.libServ.canvasToImage('white', c),
          width: 8,
          height: 3
        });

        if (index === chartElements.length - 1) {
          const pptConfig = {
            filename: 'Charts',
            logo: {
              visible: true,
              path: '/assets/images/ym.png',
              width: 0.53125,
              height: 0.458333
            },
            slides: slides
          };
          this.pptExport.exportPPT(pptConfig);
        }
      });
    }
    return false;
  }

  sendEmail() {
    const imageConfig = {
      imgName: `${this.appConfig['name']}.png`,
      type: 'image/png'
    };

    this.htmltoimage
      .canvasImage(document.getElementById('capture'), imageConfig)
      .then(canvas => {
        this.dialogService.open(SendEmailComponent, {
          header: 'Email Screenshot',
          contentStyle: {
            'max-height': '80vh',
            width: '30vw',
            overflow: 'auto'
          },
          data: canvas.toDataURL('image/png')
        });
      });
  }

  getChartJson() {
    this.showCharts = false;
    this.showChartsData = false;
    if (this.selectedMetric == 'amount_spent') {
      this.metricChartJson = this.libServ.deepCopy(this.amountSpendChartJson);
    } else if (this.selectedMetric == 'leads') {
      this.metricChartJson = this.libServ.deepCopy(this.leadsChartJson);
    } else if (this.selectedMetric == 'results') {
      this.metricChartJson = this.libServ.deepCopy(this.resultsChartJson);
    } else if (this.selectedMetric == 'impressions') {
      this.metricChartJson = this.libServ.deepCopy(this.impressionsChartJson);
    } else {
      this.metricChartJson = this.libServ.deepCopy(this.cplChartJson);
    }
    // console.log('this.metricChartJson', this.metricChartJson);
    setTimeout(() => {
      this.showCharts = false;
      this.showChartsData = true;
    }, 0);

  }

  redirectTolink(row) {
    let src = '';
    let win = null;
    win = window.open('https://business.facebook.com/adsmanager/manage/adsets?act=112871736019021&business_id=743166196157801&global_scope_id=743166196157801&selected_campaign_ids=' + row['campaign_id'], '_blank');
    win.focus();
    win = null;
  };

  exportTable(fileFormat) {
    const data = [];

    if (this.aggTableData.length == 0) {
      this.confirmationService.confirm({
        message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
        header: 'Information',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
        },
        reject: () => {
        }
      });
    } else {


      if (this.exportRequest['sendEmail'].length > 0) {
        this.toastService.displayToast({
          severity: 'info',
          summary: 'Export Report',
          detail:
            'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
          life: 10000
        });


        const tableReq = this.libServ.deepCopy(this.tableRequestParam);
        let campaignColDef = this.libServ.deepCopy(this.aggColDef);
        campaignColDef.splice(1, 2);
        tableReq['dimensions'] = ['campaign_id', 'campaign_name', 'delivery_status', 'result_type'];
        tableReq['metrics'] = ['results', 'reach', 'impressions', 'amount_spent', 'leads', 'link_clicks',
          'clicks', 'page_engagement', 'page_likes', 'post_comments', 'post_engagement', 'post_reactions', 'post_saves', 'post_shares'];
        tableReq['derived_metrics'] = ['cost_per_result', 'cost_per_lead', 'cpm_social', 'ctr_social', 'cpc_social']
        tableReq['filters'] = this.libServ.deepCopy(this.filtersApplied['filters']);
        tableReq['gidGroupBy'] = ['source', 'campaign_id', 'campaign_name', 'ad_set_name', 'ad_name', 'delivery_status', 'result_type'];
        tableReq['orderBy'] = [{ key: 'campaign_id', opcode: 'asc' }];
        tableReq['groupByTimeKey'] = { key: ['time_key'], interval: 'daily' };
        tableReq['limit'] = '';

        const sheetDetailsarray = [];
        const sheetDetails = {};
        sheetDetails['columnDef'] = campaignColDef;
        sheetDetails['data'] = [];
        sheetDetails['sheetName'] = 'Campaign';
        sheetDetails['isRequest'] = true;
        sheetDetails['request'] = {
          url: '/api/flask/campaign_performance/cassandradata',
          method: 'Post',
          param: tableReq,
        };
        sheetDetails['disclaimer'] = [
          {
            position: 'top',
            label:
              'Data persent in below table is not final and may varies over period of time',
            color: '#3A37CF'
          },
          {
            position: 'bottom',
            label: 'Thank You',
            color: '#EC6A15'
          }
        ];
        sheetDetails['totalFooter'] = {
          available: false,
          custom: false
        };
        sheetDetails['tableTitle'] = {
          available: true,
          label: 'Campaign Distribution'
        };
        sheetDetails['image'] = [
          {
            available: true,
            path: environment.exportConfig.exportLogo,
            position: 'top'
          }
        ];


        sheetDetailsarray.push(sheetDetails);
        let adsetsheetDetails = this.libServ.deepCopy(sheetDetails);
        const tableadsetReq = this.libServ.deepCopy(this.tableReq);
        let adsetColDef = this.libServ.deepCopy(this.aggColDef);
        adsetColDef.splice(2, 1);
        tableadsetReq['dimensions'] = ['campaign_id', 'campaign_name', 'ad_set_name', 'delivery_status', 'result_type'];
        adsetsheetDetails['columnDef'] = adsetColDef;
        adsetsheetDetails['sheetName'] = '+Ad Set';
        adsetsheetDetails['isRequest'] = true;
        adsetsheetDetails['request'] = {
          url: '/api/flask/campaign_performance/cassandradata',
          method: 'Post',
          param: tableadsetReq,
        };

        adsetsheetDetails['tableTitle'] = {
          available: true,
          label: 'Campaign and Ad Set Distribution'
        };

        sheetDetailsarray.push(adsetsheetDetails);
        let adnamesheetDetails = this.libServ.deepCopy(sheetDetails);
        const tableadnameReq = this.libServ.deepCopy(this.tableReq);
        let adNameColDef = this.libServ.deepCopy(this.aggColDef);
        tableadnameReq['dimensions'] = ['campaign_id', 'campaign_name', 'ad_set_name', 'ad_name', 'delivery_status', 'result_type'];
        adnamesheetDetails['columnDef'] = adNameColDef;
        adnamesheetDetails['sheetName'] = '+Ad Name';
        adnamesheetDetails['isRequest'] = true;
        adnamesheetDetails['request'] = {
          url: '/api/flask/campaign_performance/cassandradata',
          method: 'Post',
          param: tableadnameReq,
        };

        adnamesheetDetails['tableTitle'] = {
          available: true,
          label: 'Campaign ,Ad Set And Ad Name Distribution'
        };

        sheetDetailsarray.push(adnamesheetDetails);
        this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
        this.exportRequest['fileName'] =
          'Campaign Distribution ' + moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
        this.exportRequest['exportFormat'] = fileFormat;
        this.exportRequest['exportConfig'] = environment.exportConfig;
        this.dataFetchServ
          .getExportReportData(this.exportRequest)
          .subscribe(response => {
            console.log(response);

          })
      } else {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Export Report',
          detail:
            'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email.Thanks!',
          life: 10000
        });
      }
    }
  }
  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
  }
}
