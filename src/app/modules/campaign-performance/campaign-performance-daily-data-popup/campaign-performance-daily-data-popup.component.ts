import { Component, OnInit } from '@angular/core';
import { CommonLibService } from 'src/app/_services';
import { FetchApiDataService } from '../fetch-api-data.service';
import * as moment from 'moment';
import { TreeNode, ConfirmationService } from 'primeng/api';
import { ExportdataService } from 'src/app/_services/export/exportdata.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { ExportRequest } from 'src/app/_interfaces/exportRequest';
import { SheetDetails } from 'src/app/_interfaces/sheetDetails';
import { DynamicDialogConfig } from 'primeng';
import { FormatNumPipe } from '../../../_pipes/number-format.pipe';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-campaign-performance-daily-data-popup',
  templateUrl: './campaign-performance-daily-data-popup.component.html',
  styleUrls: ['./campaign-performance-daily-data-popup.component.scss']
})
export class CampaignPerformanceDailyDataPopupComponent implements OnInit {
  request: object;
  appName: string;
  email: [];
  tabChangedFlag = false;
  displayAggTable: boolean;
  tableDataReq: object;
  tableData: TreeNode[];
  tableColumnDef: any[];
  tableJson: Object;
  timeout: any;
  exportRequest: ExportRequest = <ExportRequest>{};
  showMainLineChart = false;
  mainLineChartJson: object;
  noDataMainLineChart = false;
  filtersApplied: object;
  constructor(
    private libServ: CommonLibService,
    private dataFetchServ: FetchApiDataService,
    private config: DynamicDialogConfig,
    private exportService: ExportdataService,
    private toastService: ToastService,
    private confirmationService: ConfirmationService,
    private formatNumPipe: FormatNumPipe
  ) {
    this.request = this.config.data['request'];
    this.appName = this.config.data['appName'];
    this.email = this.config.data['email'];
    this.filtersApplied = this.config.data['filtersApplied'];

  }

  ngOnInit() {



    this.tableColumnDef = [
      {
      field: 'time_key',
      displayName: 'Date',
      format: 'date',
      width: '90',
      exportConfig: {
        format: 'date',
        styleinfo: {
          thead: 'default',
          tdata: 'white'
        }
      },
      formatConfig: [],
      options: {
        editable: false,
        colSearch: true,
        colSort: true,
        resizable: true,
        movable: false
      }
    },
      {
        field: 'campaign_name',
        displayName: 'Campaign Name',
        format: '',
        width: '200',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'ad_set_name',
        displayName: 'Ad Set Name',
        format: '',
        width: '180',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      }, {
        field: 'ad_name',
        displayName: 'Ad Name',
        format: '',
        width: '180',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'campaign_id',
        displayName: 'Campaign Id',
        format: '',
        width: '125',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'delivery_status',
        displayName: 'Delivery Status',
        format: '',
        width: '115',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      }, {
        field: 'result_type',
        displayName: 'Result Type',
        format: '',
        width: '150',
        exportConfig: {
          format: 'string',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'results',
        displayName: 'Results',
        format: 'number',
        width: '90',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },

      {
        field: 'impressions',
        displayName: 'Impressions',
        format: 'number',
        width: '115',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'reach',
        displayName: 'Reach',
        format: 'number',
        width: '90',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'cost_per_result',
        displayName: 'Cost Per Result',
        format: '₹',
        width: '90',
        exportConfig: {
          format: 'floatNumber',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'amount_spent',
        displayName: 'Amount Spent (₹)',
        format: '₹',
        width: '110',
        exportConfig: {
          format: 'floatNumber',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'leads',
        displayName: 'Leads',
        format: 'number',
        width: '90',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'cost_per_lead',
        displayName: 'Cost Per Lead',
        format: '₹',
        width: '90',
        exportConfig: {
          format: 'floatNumber',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'link_clicks',
        displayName: 'Link Clicks',
        format: 'number',
        width: '90',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'cpm_social',
        displayName: 'CPM',
        format: '₹',
        width: '90',
        exportConfig: {
          format: 'floatNumber',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'ctr_social',
        displayName: 'CTR',
        format: 'percentage',
        width: '90',
        exportConfig: {
          format: 'percentage',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'cpc_social',
        displayName: 'CPC',
        format: '₹',
        width: '90',
        exportConfig: {
          format: 'floatNumber',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'clicks',
        displayName: 'Clicks (All)',
        format: 'number',
        width: '110',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'page_engagement',
        displayName: 'Page Engagement',
        format: 'number',
        width: '140',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [2],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'page_likes',
        displayName: 'Page Likes',
        format: 'number',
        width: '100',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'post_comments',
        displayName: 'Post Comments',
        format: 'number',
        width: '110',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'post_engagement',
        displayName: 'Post Engagement',
        format: 'number',
        width: '120',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'post_reactions',
        displayName: 'Post Reactions',
        format: 'number',
        width: '120',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'post_saves',
        displayName: 'Post Saves',
        format: 'number',
        width: '90',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      },
      {
        field: 'post_shares',
        displayName: 'Post Shares',
        format: 'number',
        width: '110',
        exportConfig: {
          format: 'number',
          styleinfo: {
            thead: 'default',
            tdata: 'white'
          }
        },
        formatConfig: [],
        options: {
          editable: false,
          colSearch: true,
          colSort: true,
          resizable: true,
          movable: false
        }
      }
    ];
    this.tableJson = {
      page_size: 10,
      lazy: false,
      loading: false,
      export: true,
      reload: false,
      sortMode: 'multiple',
      resizableColumns: true,
      columnResizeMode: 'fit',
      reorderableColumns: true,
      scrollHeight: '500px',
      totalRecords: 1000,

      columns: this.libServ.deepCopy(this.tableColumnDef.slice(1)),
      selectedColumns: this.libServ.deepCopy(this.tableColumnDef.slice(1)),
      globalFilterFields: this.libServ.deepCopy(this.tableColumnDef.slice(0)),
      frozenCols: this.libServ.deepCopy([...this.tableColumnDef.slice(0, 1)]),
      frozenWidth:
        this.tableColumnDef
          .slice(0, 1)
          .reduce((tot, cur) => tot + parseInt(cur.width, 10), 0) + 'px',
      scrollable: true,
      selectionMode: 'multiple',
      selectedColsModal: [],
      selectionDataKey: 'name',
      metaKeySelection: true,
      showHideCols: true,
      overallSearch: true,
      columnSearch: false
    };




    const tableRequestParam = {
      dimensions: ['campaign_id','ad_set_name', 'ad_name', 'campaign_name', 'delivery_status', 'result_type','time_key'],
      metrics:  ['results', 'reach', 'impressions', 'amount_spent', 'leads', 'link_clicks',
      'clicks', 'page_engagement', 'page_likes', 'post_comments', 'post_engagement', 'post_reactions', 'post_saves', 'post_shares'],
      derived_metrics: ['cost_per_result', 'cost_per_lead', 'cpm_social', 'ctr_social', 'cpc_social'],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters:  this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: { key: ['time_key'], interval: 'daily' },
      gidGroupBy: ['source', 'campaign_id', 'campaign_name', 'ad_set_name', 'ad_name', 'delivery_status', 'result_type'],
      orderBy:  [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };

    this.loadTable(tableRequestParam);
  }

  tabChanged(event) {
    const mainLineChartReq = {
      dimensions: ['time_key'],
      metrics: ['reach', 'amount_spent'],
      derived_metrics:[],
      timeKeyFilter: this.filtersApplied['timeKeyFilter'],
      filters: this.libServ.deepCopy(this.filtersApplied['filters']),
      groupByTimeKey: {
        key: ['time_key'],
        interval: 'daily'
      },
      gidGroupBy: ['source', 'campaign_id', 'campaign_name', 'ad_set_name', 'ad_name', 'delivery_status', 'result_type'],
      orderBy: [{ key: 'time_key', opcode: 'asc' }],
      limit: '',
      offset: ''
    };
    this.loadMainLineChart(mainLineChartReq);
  }

  loadTable(tableDataReq) {
    this.tableJson['loading'] = true;
    this.dataFetchServ.getCPRData(tableDataReq).subscribe(data => {
      if (data['status'] === 0) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(data['status_msg']);
        return;
      }
      this.tableData = data['data'];

      const arr = [];
      this.tableData.forEach((row: object, index: number) => {
        const obj = {
          data: row
        };
        arr.push(obj);
      });
      this.tableData = <TreeNode[]>arr;
      this.tableJson['totalRecords'] = data['totalItems'];
      this.tableJson['loading'] = false;
    });
  }

  loadMainLineChart(params) {
    const colors = this.libServ.dynamicColors(2);
    this.mainLineChartJson = {
      chartTypes: [{ key: 'bar', label: 'Line Chart' }],
      chartData: {
        labels: [],
        datasets: [
          {
            label: 'Reach',
            type: 'line',
            yAxisID: 'y-axis-0',

            borderColor: colors[0],
            fill: false,
            backgroundColor: colors[0],
            data: []
          },
          {
            label: 'Amount Spent(₹)',
            type: 'bar',
            yAxisID: 'y-axis-1',

            borderColor: colors[1],
            fill: false,
            backgroundColor: colors[1],
            data: []
          }
        ]
      },
      chartOptions: {
        title: {
          display: true,
          text: 'Amount Spent vs Reach'
        },
        legend: {
          display: true,
          // onClick: e => e.stopPropagation()
        },
        scales: {
          xAxes: [
            {
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Date'
              }
            }
          ],
          yAxes: [
            {
              id: 'y-axis-0',
              scaleLabel: {
                display: true,
                labelString: 'Reach'
              },
              position: 'right',
              name: '1',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, 'number', []);
                }
              }
              // scaleFontColor: 'rgba(151,137,200,0.8)'
            },
            {
              id: 'y-axis-1',
              scaleLabel: {
                display: true,
                labelString: 'Amount Spent(₹)'
              },
              position: 'left',
              name: '2',
              ticks: {
                callback: (value, index, values) => {
                  return this.formatNumPipe.transform(value, '₹', [0]);
                }
              }
              // scaleFontColor: 'rgba(151,187,205,0.8)'
            }
          ]
        },
        tooltips: {
          mode: 'index',
          intersect: false,
          callbacks: {
            label: (tooltipItem, data) => {
              const currentValue =
                data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index];
                if (tooltipItem.datasetIndex !== 0) {
                  return `${
                    data.datasets[tooltipItem.datasetIndex].label
                    } : ${this.formatNumPipe.transform(
                      currentValue,
                      '₹',
                      [2]
                    )}`;
                }
                return `${
                  data.datasets[tooltipItem.datasetIndex].label
                  } : ${this.formatNumPipe.transform(currentValue, 'number', [])}`;
            }
          }
        },
        plugins: {
          datalabels: false
        }
      },
      zoomLabel: false,
      chartWidth: '',
      chartHeight: '300px'
    };

    this.showMainLineChart = false;
    this.dataFetchServ.getCPRData(params).subscribe(data => {
      const chartData = data['data'];
      this.noDataMainLineChart = false;
      if (!chartData.length) {
        this.noDataMainLineChart = true;
        return;
      }
      const datesArr = Array.from(new Set(chartData.map(r => r['time_key'])));

      this.mainLineChartJson['chartData']['labels'] = datesArr.map(d =>
        moment(d, 'YYYYMMDD').format('MM-DD-YYYY')
      );

      const amountArr = [];
      const reachArr = [];
      datesArr.forEach(time_key => {
        chartData.forEach(r => {
          if (r['time_key'] === time_key) {
            amountArr.push(r['amount_spent']);
            reachArr.push(r['reach']);
          }
        });
      });
      this.mainLineChartJson['chartData']['datasets'][0]['data'] = reachArr;
      this.mainLineChartJson['chartData']['datasets'][1]['data'] = amountArr;
      this.showMainLineChart = true;
    });
  }

  exportTablePopup(fileFormat) {
    const req = this.libServ.deepCopy(this.request);
    this.exportRequest['sendEmail'] = this.email;
    this.exportRequest['appName'] = this.appName;
    if (this.tableData.length == 0) {
      this.confirmationService.confirm({
        message: 'Your report did not generated because data is not available for the selected dates. Please select the date range from August 2018 onwards.',
        header: 'Information',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
        },
        reject: () => {
        }
      });
    } else {
      if (this.exportRequest['sendEmail'].length > 0) {
        this.toastService.displayToast({
          severity: 'info',
          summary: 'Export Report',
          detail:
            'We have received your report request, we will deliver it to your contact email inbox shortly.Thanks!',
          life: 10000
        });
        const columnDefs = this.libServ.deepCopy(this.tableColumnDef);
        const tableRequestParam = {
          dimensions: ['campaign_id','ad_set_name', 'ad_name', 'campaign_name', 'delivery_status', 'result_type','time_key'],
          metrics:  ['results', 'reach', 'impressions', 'amount_spent', 'leads', 'link_clicks',
          'clicks', 'page_engagement', 'page_likes', 'post_comments', 'post_engagement', 'post_reactions', 'post_saves', 'post_shares'],
          derived_metrics: ['cost_per_result', 'cost_per_lead', 'cpm_social', 'ctr_social', 'cpc_social'],
          timeKeyFilter: this.filtersApplied['timeKeyFilter'],
          filters:  this.libServ.deepCopy(this.filtersApplied['filters']),
          groupByTimeKey: { key: ['time_key'], interval: 'daily' },
          gidGroupBy: ['source', 'campaign_id', 'campaign_name', 'ad_set_name', 'ad_name', 'delivery_status', 'result_type'],
          orderBy:  [{ key: 'time_key', opcode: 'asc' }],
          limit: '',
          offset: ''
        };

        const sheetDetails = {};
        sheetDetails['columnDef'] = columnDefs;
        sheetDetails['data'] = [];
        sheetDetails['sheetName'] = 'Daily Data Distribution';
        sheetDetails['isRequest'] = true;
        sheetDetails['request'] = {
          url: '/api/flask/campaign_performance/cassandradata',
          method: 'Post',
          param: tableRequestParam,
        };

        sheetDetails['disclaimer'] = [
          {
            position: 'bottom',
            label: 'Note: Data present in the table may vary over a period of time.',
            color: '#000000'
          }
        ];
        sheetDetails['totalFooter'] = {
          available: false,
          custom: false
        };
        sheetDetails['tableTitle'] = {
          available: true,
          label: 'Daily Data Distribution :' +  this.config.data['field']
        };
        sheetDetails['image'] = [
          {
            available: true,
            path: environment.exportConfig.exportLogo,
            position: 'top'
          }
        ];
        const sheetDetailsarray = [];
        sheetDetailsarray.push(sheetDetails);
        this.exportRequest['sheetDetails'] = <SheetDetails[]>sheetDetailsarray;
        this.exportRequest['fileName'] =
          this.config.data['field'] +
          ' Daily Distribution ' +
          moment(new Date()).format('MM-DD-YYYY HH:mm:ss');
        this.exportRequest['exportFormat'] = fileFormat;
        this.exportRequest['exportConfig'] = environment.exportConfig;
        this.dataFetchServ
          .getExportReportData(this.exportRequest)
          .subscribe(response => {
            console.log('response of table', response);
          });
      } else {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Export Report',
          detail:
            'Email id not found. Please add your contact email to proceed. Click profile icon > User Profile > Edit Profile > Contact Email. \n Thanks!',
          life: 10000
        });
      }
    }
    return false;
  }

  isHiddenColumn(col: Object) { }

  onGlobalSearchChanged(searchValue, tableColDef) {
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    this.timeout = setTimeout(() => {
      this.tableDataReq['filters']['globalSearch'] = {
        dimensions: [],
        value: searchValue
      };
      tableColDef.forEach(element => {
        this.tableDataReq['filters']['globalSearch']['dimensions'].push(
          element.field
        );
      });
      this.loadTable(this.tableDataReq);
    }, 3000);
  }


}
