import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignPerformanceDailyDataPopupComponent } from './campaign-performance-daily-data-popup.component';

describe('Rev360DailyDataPopupComponent', () => {
  let component: CampaignPerformanceDailyDataPopupComponent;
  let fixture: ComponentFixture<CampaignPerformanceDailyDataPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CampaignPerformanceDailyDataPopupComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignPerformanceDailyDataPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
