import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignPerformanceAppComponent } from './campaign-performance-app.component';

describe('CompaignDashboardComponent', () => {
  let component: CampaignPerformanceAppComponent;
  let fixture: ComponentFixture<CampaignPerformanceAppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignPerformanceAppComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignPerformanceAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
