import { Routes } from '@angular/router';

import { CampaignPerformanceAppComponent } from './campaign-performance-app.component';
export const SampleAppRoutes: Routes = [
  {
    path: '',
    component: CampaignPerformanceAppComponent
    // data: {
    //   breadcrumb: 'Yield Management'
    // }
  }
];
