import {
  Component,
  OnInit,
  Input,
  ViewChild,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import {
  FilterDataService,
  CommonLibService,
  PlatformConfigService
} from './../../_services';
import { Dimension } from './../common/filter-container/models/dimension.model';
import { Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'ym-team-resources',
  templateUrl: './team-resources.component.html',
  styleUrls: ['./team-resources.component.scss']
})
export class TeamResourcesComponent implements OnInit, OnDestroy {
  @ViewChild('dimensionsDropdown') dimensionsDropdown: any;
  @Input() defaultDimensions: Dimension[];
  @Input() availableDimensions: Dimension[];
  @Input() filterUrl: string;
  @Input() enabled: boolean;
  @Output() dimFilterChanged = new EventEmitter<object>();

  selectedDimensionToAdd: Dimension = { key: '', label: '', config_json: '' };
  availableFilterData: object = {};
  selectedFilterData: object = {};
  private platConfigObs: Subscription;
  config: object;
  adminRoles = environment.adminRoles;

  constructor(
    private filterDataService: FilterDataService,
    private libServ: CommonLibService,
    private platformConfigService: PlatformConfigService
  ) { }

  ngOnInit() {
    this.platConfigObs = this.platformConfigService.obsConfig.subscribe(res => {
      if (!this.libServ.isEmptyObj(res)) {
        this.config = res;
        if (
          this.adminRoles.includes(
            this.config['user']['role']['name'].toUpperCase()
          )
        ) {
          this.defaultDimensions.forEach((dim, i) => {
            const prevFilters = {};
            this.defaultDimensions.forEach((d, idx) => {
              if (idx < i) {
                prevFilters[d['key']] = d['value'];
              }
            });
            setTimeout(() => {
              this.getFilterValues(dim['filter_key'], dim['key'], prevFilters, dim['value']);
            }, 0);
          });
        } else {
          const resources = JSON.parse(
            this.config['user']['team']['resources']
          );

          resources.forEach(resource => {
            console.log('resource', resource);
            this.availableFilterData[resource['key']] = [];
            this.selectedFilterData[resource['key']] = [];

            this.availableFilterData[resource['key']] = resource['value'].map(
              d => {
                const obj = {};
                obj['key'] = d;
                obj['label'] = d;
                obj['value'] = d;
                return obj;
              }
            );

            this.defaultDimensions.forEach(dim => {
              if (resource['key'] == dim['filter_key']) {
                this.selectedFilterData[resource['key']] = dim['value'];
              }
            });

            // this.selectedFilterData[resource['key']].push(
            //   ...this.availableFilterData[resource['key']].map(a => {
            //     return a.key;
            //   })
            // );

            this.dimFilterChanged.emit(this.selectedFilterData);
          });
        }
      }
    });

    console.log('availableDimensions', this.availableDimensions);
  }

  onChangeFilterCom(dim: Dimension, dimIdx: number) {
    console.log('onclick', dim, dimIdx);
    for (let i = dimIdx; i < this.defaultDimensions.length; i++) {
      for (const k of Object.keys(this.selectedFilterData)) {
        if (this.selectedFilterData[k].length === 0) {
          this.selectedFilterData[k] = [];
          this.selectedFilterData[k].push(
            ...this.availableFilterData[k].map(a => {
              return a.key;
            })
          );
        }
      }
      const nextDim = this.defaultDimensions[i + 1];

      const prevFilters = {};
      this.defaultDimensions.forEach((d, idx) => {
        if (idx <= i) {
          prevFilters[d['key']] = this.selectedFilterData[d['filter_key']];
        }
      });

      if (typeof nextDim !== 'undefined') {
        setTimeout(() => {
          this.getFilterValues(nextDim['filter_key'], nextDim['key'], prevFilters, this.selectedFilterData[nextDim['filter_key']]);
        }, 0);
      }

      this.dimFilterChanged.emit(this.selectedFilterData);
    }
  }

  getFilterValues(dim: string, dimValue: string, params: object, value: any) {
    let keys = Object.keys(params)
    keys.forEach(element => {
      if(params[element] != undefined && params[element].length > 0)
      {
          const str = "'" + params[element].join("'$$'") + "'";
          params[element] = [str];
      }
    });
    this.filterDataService
      .getFilterDataList(`${this.filterUrl}/${dimValue}`, params)
      .subscribe(res => {
        this.availableFilterData[dim] = [];
        this.selectedFilterData[dim] = [];
        const arr = this.libServ.deepCopy(res['data']);
        const arrExclude =
          this.defaultDimensions[0]['config'] &&
            this.defaultDimensions[0]['config'].hasOwnProperty('exclude')
            ? this.defaultDimensions[0]['config'].exclude.split(',')
            : [];
        for (let i = 0; i < arr.length; i++) {
          arr[i]['value'] = arr[i]['label'];
        }

        arrExclude.forEach(element => {
          if (arr.some(o => o.label === element)) {
            arr.splice(
              arr.findIndex(x => x.label === element),
              1
            );
          }
        });
        this.availableFilterData[dim].push(...arr);
        if (value.length > 0) {
          const dimValues = [];
          value.forEach(element => {
            if (this.availableFilterData[dim].some(x => x.key == element)) {
              dimValues.push(element);
            }
          });
          this.selectedFilterData[dim] = dimValues;

        } else {
          this.selectedFilterData[dim].push(
            ...this.availableFilterData[dim].map(a => {
              return a.key;
            })
          );
        }
        this.dimFilterChanged.emit(this.selectedFilterData);
        console.log('defaultDimensions', this.defaultDimensions, this.availableFilterData);
      });
  }

  onClickFilterCom(dim: Dimension, dimIdx: number) {
    console.log('onclick', dim, dimIdx);
    for (let i = dimIdx; i < this.defaultDimensions.length; i++) {
      if (this.availableFilterData[dim['filter_key']].length > 0) {
        return;
      }
      const prevFilters = {};
      this.defaultDimensions.forEach((d, idx) => {
        if (idx < i) {
          prevFilters[d['key']] = this.selectedFilterData[d['filter_key']];
        }
      });
      setTimeout(() => {
        this.getFilterValues(dim['filter_key'], dim['key'], prevFilters, this.selectedFilterData[dim['filter_key']]);
      }, 0);

    }
  }

  pluralize(label) {
    if (label.endsWith('y')) {
      return label.slice(0, -1) + 'ies';
    } else {
      return label + 's';
    }
  }

  ngOnDestroy(): void {
    if (this.platConfigObs && !this.platConfigObs.closed) {
      this.platConfigObs.unsubscribe();
    }
  }
}
