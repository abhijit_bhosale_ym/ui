import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectButtonModule } from 'primeng/selectbutton';
import { ButtonModule } from 'primeng/button';
import { TabViewModule } from 'primeng/tabview';
import { TreeTableModule } from 'primeng/treetable';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MultiSelectModule } from 'primeng/multiselect';
import { DropdownModule } from 'primeng/dropdown';

import { SharedModule } from 'src/app/_pipes/shared.module';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { RevMgmtAppComponent } from './revmgmt-app.component';
import { FilterContainerModule } from '../common/filter-container/filter-container.module';
import { CardsModule } from '../common/cards/cards.module';
import { ChartsModule } from '../common/charts/charts.module';
import { RouterModule } from '@angular/router';
import { SampleAppRoutes } from './revmgmt-app.routing.module';
import { DummyCompComponent } from './dummy-comp/dummy-comp.component';
import { ConfirmDialogModule, ConfirmationService } from 'primeng';

@NgModule({
  declarations: [RevMgmtAppComponent],
  // exports: [SampleAppComponent],
  imports: [
    CommonModule,
    SelectButtonModule,
    ButtonModule,
    TabViewModule,
    TreeTableModule,
    FormsModule,
    ReactiveFormsModule,
    MultiSelectModule,
    DropdownModule,

    SharedModule,

    FilterContainerModule,
    CardsModule,
    ChartsModule, ConfirmDialogModule,
    RouterModule.forChild(SampleAppRoutes)
  ],
  providers: [FormatNumPipe, ConfirmationService]
})
export class RevMgmtAppModule { }
