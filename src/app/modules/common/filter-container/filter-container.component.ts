import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  OnDestroy,
  Input
} from '@angular/core';
import { Dimension } from './models/dimension.model';
import * as moment from 'moment';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { SaveQueryService } from 'src/app/_services/save-query/save-query.service';
import { CommonLibService } from 'src/app/_services/common-lib/common-lib.service';
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';
import { LoaderObsService } from '../../../_services/api-loader/loader-obs.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { DialogService } from 'primeng/dynamicdialog';
import { EditSaveViewComponent } from './edit-save-view/edit-save-view.component';

import { AutoUnsubscribe } from 'ngx-auto-unsubscribe';
import { IfStmt } from '@angular/compiler';
@AutoUnsubscribe()

@Component({
  selector: 'ym-filter-container',
  templateUrl: './filter-container.component.html',
  styleUrls: ['./filter-container.component.scss']
})
export class FilterContainerComponent implements OnInit, OnDestroy {
  // @Input() private appFilters: object;
  @Input() dateDimensionRelation = false;
  @Output() filtersApplied = new EventEmitter<object>();
  @Input() name: any;
  @Output() jsonOfList = new EventEmitter<object>();
  @Output() saveViewToggle = new EventEmitter<object>();
  @Input() selectedColumn = {};

  appConfigObs: Subscription;
  appConfigSubscription: Subscription = null;
  appconfigRestSubscription: Subscription = null;

  appConfigObs1: Subscription;
  appConfigObs2: Subscription;
  saveQueryObs: Subscription;

  public collapsed = false;
  public appConfig = {};
  public appFilters = {};
  public date_filter_config = [];
  public dimFilterFlag = true;
  public grpByFilterFlag = true;
  public dateFilterFlag = true;
  public defaultDimensions = [];
  public availableDimensions = [];
  public defaultMetrics = [];
  public availableMetrics = [];
  public filterUrl = '';
  public selectedDimFilter: object = {};
  public selectedMetricFilter: object = {};
  public selectedGrpBy: Dimension[];
  public selectedDates: any = [];
  public groupByDims = [];
  public savedViews = [];
  public maxSelectable: number;
  public enableDatePicker = false;
  public selectedSavedView: string;
  public saveViewName: String = '';
  public displaySaveViewConfirmDialog = false;
  public setDefaultCheckbox = false;
  private BASE_URL: string = environment.baseUrl;
  displayLoad = false;
  dateFilters: any;
  showToggle = false;
  viewJson = {};
  is_app_default = false;
  is_default = false;
  isSaveView: boolean;
  isToggled: boolean;


  constructor(
    private appConfigService: AppConfigService,
    private saveQueryService: SaveQueryService,
    public libServ: CommonLibService,
    private loaderService: LoaderObsService,
    private dialogService: DialogService,
    private toastService: ToastService,
  ) { }

  ngOnInit() {
    this.loaderService.displayLoad.subscribe(visibility => {
      setTimeout(() => {
        this.displayLoad = visibility;
      });
    });
    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      console.log('appconfig....', appConfig)
      if (!this.libServ.isEmptyObj(appConfig)) {

        this.isSaveView = appConfig['filter']['filterConfig']['saveView'];
        this.isToggled = false;
        this.appConfig = appConfig;

        if (!this.libServ.isEmptyObj(appConfig['filter'])) {
          this.appFilters = this.libServ.deepCopy(
            appConfig['filter']['filterConfig']['filters']
          );
          this.selectedGrpBy = this.libServ.deepCopy(
            appConfig['filter']['filterConfig']['groupBy']
          );

          this.dateFilters = this.appConfig['filter']['filterConfig']['filters']['datePeriod'];
          this.changeConfig();

          // if (this.appConfig['filter']['filterConfig']['saveView']) {
          //   this.saveQueryService.changeSelectedQuery({
          //     date_filter_config: this.appFilters['datePeriod'][0],
          //     filter: {
          //       dimensions: this.selectedDimFilter,
          //       metrics: this.selectedMetricFilter
          //     },
          //     groupby: this.selectedGrpBy
          //   });
          //   this.saveQueryService.allQueries.subscribe(res => {
          //     if (!this.libServ.isEmptyObj(res)) {
          //       this.savedViews = res as [];
          //       this.selectedSavedView = this.savedViews.find(
          //         v => v['isDefault'] === 'true'
          //       );
          //       this.viewChanged();
          //     }
          //   });
          // }
        }
      }
    });
  }

  changeConfig() {    
    this.filterUrl = `${this.BASE_URL}${this.appFilters['url']}`;
    this.defaultDimensions = this.appFilters['dimension']['default'];
    this.availableDimensions = this.appFilters['dimension']['available'];
    this.maxSelectable = typeof this.appFilters['dimension']['maxSelectable'] !== 'undefined' ? this.appFilters['dimension']['maxSelectable'] : -1;
    this.defaultMetrics = this.appFilters['metric']['default'];
    this.availableMetrics = this.appFilters['metric']['available'];
    this.groupByDims = this.selectedGrpBy;
    this.date_filter_config = this.appFilters['datePeriod'];
 
    if (!(this.appConfig['filter']['filterConfig']['saveView'] && this.showToggle)) {        
      this.dateFilters.forEach((element, idx) => {
        if (
          typeof this.date_filter_config !== 'undefined' &&
          !this.libServ.isEmptyObj(this.date_filter_config)
        ) {
          this.enableDatePicker = true;
          let startDate;
          let endDate;

          if ((this.date_filter_config[0]['defaultDate'][0]['value'] === 0 && this.date_filter_config[0]['defaultDate'][0]['period'] === 'month') && (moment().format('DD') === '01' || moment().format('DD') === '02')) {
            startDate = moment().subtract(1, 'months').startOf('month');
            endDate = moment().subtract(1, 'months').endOf('month');
          } else {
            if (element['defaultDate'][0]['startOf']) {
              startDate = moment()
                .subtract(
                  element['defaultDate'][0]['value'],
                  element['defaultDate'][0]['period']
                )
                .startOf(element['defaultDate'][0]['period']);
            } else {
              startDate = moment().subtract(
                element['defaultDate'][0]['value'],
                element['defaultDate'][0]['period']
              );
            }

            endDate = moment().subtract(
              element['defaultDate'][1]['value'],
              element['defaultDate'][1]['period']
            );
          }

          if (element['filterType'] === 'single_date') {
            this.selectedDates.push(startDate.toDate());
          } else if (element['filterType'] === 'date_range') {
            this.selectedDates = [startDate.toDate(), endDate.toDate()];
          } else if (element['filterType'] === 'single_month') {
            this.selectedDates = [startDate.toDate()];
          } else if (element['filterType'] === 'month_range') {
            this.selectedDates = [startDate.toDate(), endDate.toDate()];
          }

        }
      });
    }
  }

  onApplyFilterClicked() {
    document.body.click();
    const selectedDimFilterlocal = this.libServ.deepCopy(this.selectedDimFilter);
    if (
      selectedDimFilterlocal['dimension'] &&
      !Array.isArray(selectedDimFilterlocal['dimension'])
    ) {
      selectedDimFilterlocal['dimension'] = selectedDimFilterlocal[
        'dimension'
      ].split(',');
    }

    if (
      selectedDimFilterlocal['metric'] &&
      !Array.isArray(selectedDimFilterlocal['metric'])
    ) {
      selectedDimFilterlocal['metric'] = selectedDimFilterlocal['metric'].split(
        ','
      );
    }
    if (this.selectedDates['dateBucketCurrent'] == undefined && !this.appConfig['filter']['filterConfig']['filters']['isMultiDate']) {      
      
      for (const key in this.selectedDates) {
        this.selectedDates[key] = moment(
          this.selectedDates[key],
          'YYYYMMDD'
        ).format('YYYYMMDD');
      }
    }

    setTimeout(() => {
      this.filtersApplied.emit({
        date: this.enableDatePicker 
          ? this.selectedDates
          : '',
        filter: {
          dimensions: selectedDimFilterlocal,
          metrics: this.selectedMetricFilter
        },
        groupby: this.selectedGrpBy
      });
    }, 0);

  }


  onResetFilterClicked() {
    this.appConfig = {};
    this.selectedDimFilter = {};
    this.selectedMetricFilter = {};
    this.selectedGrpBy = [];
    this.selectedDates = [];
    this.groupByDims = [];
    this.savedViews = [];

    setTimeout(() => {
      this.appConfigObs1 = this.appConfigService.appConfig.subscribe(appConfig => {
        if (!this.libServ.isEmptyObj(appConfig)) {
          this.appConfig = appConfig;
          if (!this.libServ.isEmptyObj(appConfig['filter'])) {
            this.appFilters = this.libServ.deepCopy(
              appConfig['filter']['filterConfig']['filters']
            );
            this.selectedGrpBy = this.libServ.deepCopy(
              appConfig['filter']['filterConfig']['groupBy']
            );
            this.changeConfig();

            // if (this.appConfig['filter']['filterConfig']['saveView']) {
            //   this.saveQueryService.changeSelectedQuery({
            //     date_filter_config: this.appFilters['datePeriod'][0],
            //     filter: {
            //       dimensions: this.selectedDimFilter,
            //       metrics: this.selectedMetricFilter
            //     },
            //     groupby: this.selectedGrpBy
            //   });
            //   this.saveQueryService.allQueries.subscribe(res => {
            //     if (!this.libServ.isEmptyObj(res)) {
            //       this.savedViews = res as [];
            //       this.selectedSavedView = this.savedViews.find(
            //         v => v['isDefault'] === 'true'
            //       );
            //       this.viewChanged();
            //     }
            //   });
            // }
          }
        }
      });
    }, 0);
  }

  datePickerChanged() {
    // Refresh dimension filter if date picker changed
    if (this.dateDimensionRelation) {
      this.dimFilterFlag = false;
      setTimeout(() => {
        this.dimFilterFlag = true;
      });
      if (this.appConfig['filter']['filterConfig']['saveView'] && this.showToggle) {
        this.grpByFilterFlag = false;
        this.dateFilterFlag = false;
        setTimeout(() => {
          this.grpByFilterFlag = true;
          this.dateFilterFlag = true;
        });
      }
    }
  }

  viewChanged() {
    this.is_app_default = (this.selectedSavedView['is_app_default'] == 'true');
    this.is_default = (this.selectedSavedView['is_default'] == 'true');
    this.dimFilterFlag = false;
    this.grpByFilterFlag = false;
    this.dateFilterFlag = false;
    setTimeout(() => {
      this.dimFilterFlag = true;
      this.grpByFilterFlag = true;
      this.dateFilterFlag = true;
    });

    if (!this.libServ.isEmptyObj(this.appFilters)) {
      this.appFilters['dimension']['default'] = JSON.parse(
        this.selectedSavedView['state_json']
      )['defaultDimensions'];

      this.appFilters['dimension']['available'] = JSON.parse(
        this.selectedSavedView['state_json']
      )['availableDimensions'];

      this.appFilters['dimension']['maxSelectable'] = JSON.parse(
        this.selectedSavedView['state_json']
      )['maxSelectable'];

      this.appFilters['metric']['default'] = JSON.parse(
        this.selectedSavedView['state_json']
      )['defaultMetrics'];

      this.appFilters['metric']['available'] = JSON.parse(
        this.selectedSavedView['state_json']
      )['availableMetrics'];

      this.selectedGrpBy = JSON.parse(this.selectedSavedView['state_json'])['groupby'];
      let dates= JSON.parse(this.selectedSavedView['state_json'])['date'];
      if (typeof this.appFilters['datePeriod'] !== 'undefined' && this.appFilters['datePeriod'].length > 0) {
        const arr = dates['date'] as [];
        let key = Object.keys(JSON.parse(this.selectedSavedView['state_json'])['date'])
        key.forEach((element,i) => {
          this.appFilters['datePeriod'][i]['isEnable'] = dates[element]['isEnable'];
          this.appFilters['datePeriod'][i]['defaultDate'][0] = dates[element]['time_key1'];
          this.appFilters['datePeriod'][i]['defaultDate'][1] = dates[element]['time_key2'];
          this.appFilters['datePeriod'][i]['date_type'] = dates[element]['date_type']
        });
      }
    }

    this.changeConfig();
    this.date_filter_config = this.appFilters['datePeriod'];
    this.selectedDimFilter = JSON.parse(this.selectedSavedView['state_json'])[
      'filter'
    ]['dimensions'];
    this.selectedMetricFilter = JSON.parse(this.selectedSavedView['state_json'])[
      'filter'
    ]['metrics'];

    this.saveQueryService.changeSelectedQuery({
      date_filter_config: this.date_filter_config,
      filter: {
        dimensions: this.selectedDimFilter,
        metrics: this.selectedMetricFilter
      },
      groupby: this.selectedGrpBy
    });

    document.body.click();
    setTimeout(() => {
      this.jsonOfList.emit({
        saveViewsJson: this.selectedSavedView,
        groupBy: JSON.parse(this.selectedSavedView['state_json'])['groupby'].filter(e => e['selected'])
      });
    }, 0);
      setTimeout(() => {
        this.onApplyFilterClicked();
      }, 0);
  }

  deleteView() {
    const param = {
      appId: this.selectedSavedView['app_id'],
      userId: this.selectedSavedView['user_id'],
      viewId: this.selectedSavedView['id']
    };
    this.saveQueryService.deleteSavedQueries(param).subscribe(res => {
      if (res['status'] === false) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Server Error',
          detail: 'Please refresh the page'
        });
        console.log(res['status_msg']);
        return;
      } else {
        this.toastService.displayToast({
          severity: 'success',
          summary: 'Delete View',
          detail: 'View has been deleted succesfully'
        });
      }
      this.saveQueryService.getSavedQueries(param);
    });
  }

  activateDeactivate(toggle) {
    setTimeout(() => {
      this.saveViewToggle.emit({
        toggleValue: this.showToggle
      });
    }, 0);
    toggle = !toggle;
    if (!toggle) {
      this.isToggled = true;
      if (this.appConfig['filter']['filterConfig']['saveView'] && this.showToggle) {

        this.saveQueryObs = this.saveQueryService.allQueries.subscribe(res => {
          if (!this.libServ.isEmptyObj(res)) {
            this.savedViews = res as [];
            this.selectedSavedView = this.savedViews.find(
              v => v['is_default'] === 'true'
            );
            if (typeof this.selectedSavedView !== 'undefined') {
              this.viewChanged();
            }
          }
        });
      }
    } else {
      // this.onResetFilterClicked();
      this.appConfig = {};
      this.selectedDimFilter = {};
      this.selectedMetricFilter = {};
      this.selectedGrpBy = [];
      this.selectedDates = [];
      this.groupByDims = [];
      this.savedViews = [];

      setTimeout(() => {
        this.appConfigObs2 = this.appConfigService.appConfig.subscribe(appConfig => {
          if (!this.libServ.isEmptyObj(appConfig)) {
            this.appConfig = appConfig;
            if (!this.libServ.isEmptyObj(appConfig['filter'])) {
              this.appFilters = this.libServ.deepCopy(
                appConfig['filter']['filterConfig']['filters']
              );
              this.selectedGrpBy = this.libServ.deepCopy(
                appConfig['filter']['filterConfig']['groupBy']
              );
              this.changeConfig();
              
              this.saveQueryService.changeSelectedQuery({
                date_filter_config: this.date_filter_config,
                filter: {
                  dimensions: this.selectedDimFilter,
                  metrics: this.selectedMetricFilter
                },
                groupby: this.selectedGrpBy
              });
            }
          }
        });
      }, 0);

      setTimeout(() => {
        this.onApplyFilterClicked();
      }, 0);
    }
  }

  openPopup(row, value) {
    const data = {
      column: this.selectedColumn,
      data: this.selectedSavedView,
      buttonValue: value,
    };

    const ref = this.dialogService.open(EditSaveViewComponent, {
      header: value + ' View',
      contentStyle: { width: '60vw', height: '470px', overflow: 'auto' },
      data: data
    });
    ref.onClose.subscribe((data: string) => {
    });
  }

  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
    if (this.saveQueryObs && !this.saveQueryObs.closed) {
      this.saveQueryObs.unsubscribe();
    }
    if (this.appConfigObs1 && !this.appConfigObs1.closed) {
      this.appConfigObs1.unsubscribe();
    }
    if (this.appConfigObs2 && !this.appConfigObs2.closed) {
      this.appConfigObs2.unsubscribe();
    }
  }
}
