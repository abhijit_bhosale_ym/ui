import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Dimension } from '../models/dimension.model';
import { SaveQueryService } from 'src/app/_services/save-query/save-query.service';
import { CommonLibService } from 'src/app/_services/common-lib/common-lib.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'ym-group-by',
  templateUrl: './group-by.component.html',
  styleUrls: ['./group-by.component.scss']
})
export class GroupByComponent implements OnInit, OnDestroy {
  @Input() dimensions: Dimension[];
  @Output() grpByChanged = new EventEmitter<Dimension[]>();
  @Input() isSaveView: boolean;
  @Input() showToggle: boolean;
  limit: any;

  selectedGrpBy: Dimension[];
  savedQueryServiceSelectedQuery1: Subscription;

  constructor(
    private saveQueryService: SaveQueryService,
    public libServ: CommonLibService
  ) { }

  ngOnInit() {

    console.log("dimensions", this.dimensions);
   this.limit = this.dimensions.length;
    let temp = [];
    if (this.dimensions.filter(item => item['config_json'] != null).length > 0) {
      temp = this.dimensions.filter(item => item['config_json']);
      console.log("temp0==", temp[0]);

      if (temp[0].config_json['selectionLimit']) {
        this.limit = temp[0]['config_json']['selectionLimit'];
      }
      const idx = this.dimensions.findIndex(item => item['config_json'] != null);
      delete this.dimensions[idx];
    }

    this.savedQueryServiceSelectedQuery1 = this.saveQueryService.selectedQuery.subscribe(query => {
      if (this.isSaveView && this.showToggle) {
        setTimeout(() => {
          this.selectedGrpBy = query['groupby'].filter(d => d['selected']);
          this.onChange();
        }, 0);
      } else {
        this.selectedGrpBy = this.dimensions.filter(d => d['selected']);
        this.onChange();
      }
    });
  }

  onChange() {
    if (this.selectedGrpBy.length == 0) {
      this.selectedGrpBy.push(this.dimensions[0]);
      return;
    }
    if (this.selectedGrpBy.length > this.limit) {
      this.selectedGrpBy.pop();
    } else {
      const arr = [];
      arr.push(...this.selectedGrpBy);
      arr.push(
        ...this.dimensions.filter(
          o => !this.selectedGrpBy.map(a => a['key']).includes(o['key'])
        )
      );
      this.dimensions = arr;
      this.grpByChanged.emit(this.selectedGrpBy);
    }
  }

  ngOnDestroy() {
    if (this.savedQueryServiceSelectedQuery1 && !this.savedQueryServiceSelectedQuery1.closed) {
      this.savedQueryServiceSelectedQuery1.unsubscribe();
    }
  }
}
