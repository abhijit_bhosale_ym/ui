import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SaveQueryService } from 'src/app/_services/save-query/save-query.service';
import * as moment from 'moment';
import { CommonLibService } from 'src/app/_services/common-lib/common-lib.service';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';

@Component({
  selector: 'ym-daterange-picker',
  templateUrl: './daterange-picker.component.html',
  styleUrls: ['./daterange-picker.component.scss']
})
export class DaterangePickerComponent implements OnInit {
  @Input('appConfig') public appConfig;
  @Output() datePickerChanged = new EventEmitter<Object>();
  @Output() periodSeledted = new EventEmitter<object>();
  @Input() name: any;
  @Input() isSaveView: boolean;
  @Input() showToggle: boolean;
  config: any[] = [];
  rangeDates: any[] = [];
  today = new Date();
  minDate = new Date();

  rangeDates1: Date[];
  dateRangeOptions: any[];
  dateRangeCompareOptions: any[];
  selectedRange: {};
  selectedCompareRange: {};
  selectedCompareValues: boolean;
  isCustom: boolean;
  isCustomPrev = false;
  dateFilters: any;
  filterType: any;
  saveDate: any[];
  savePeriod = [];
  finalDateRange: any[] = [];
  isMultiple: boolean;
  multipleDateRangeConfig: [];
  multiDateRange: any[] = [];
  bucketName: any;
  isDefaultVal = false;
  saveViewDate = [];

  saveDateCon = {
    isEnable: true,
    time_key1: "",
    time_key2: "",
    date_type: ""
  }
  constructor(
    private saveQueryService: SaveQueryService,
    public libServ: CommonLibService,
    private toastService: ToastService,
  ) { }

  ngOnInit() {
    
    this.isMultiple = this.appConfig['filter']['filterConfig']['filters']['isMultiDate'];
    if (this.isSaveView && this.showToggle) {
      this.saveQueryService.selectedQuery.subscribe(query => {
        setTimeout(() => {
          this.dateFilters = query['date_filter_config'];
          this.isMultiple ? this.config = this.appConfig['filter']['filterConfig']['filters']['datePeriod'] : this.config = this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0];
          this.saveDate = query['date_filter_config'];
          this.updateSaveDate();
        }, 0);
      });

    } else {

      this.dateFilters = this.appConfig['filter']['filterConfig']['filters']['datePeriod'];
      this.isMultiple ? this.config = this.appConfig['filter']['filterConfig']['filters']['datePeriod'] : this.config = this.appConfig['filter']['filterConfig']['filters']['datePeriod'][0];

      if(this.config['isCompare'])  
       this.selectedCompareValues = this.config['isCompare'];

      this.dateRangeOptions = [
        { key: 'custom', label: 'Custom' },
        { key: 'today', label: 'Today' },
        { key: 'yesterday', label: 'Yesterday' },
        { key: 'last_7_days', label: 'Last 7 days' },
        { key: 'last_week', label: 'Last Week' },
        { key: 'last_month', label: 'Last Month' },
        { key: 'last_30_days', label: 'Last 30 days' },
        { key: 'this_month', label: 'This Month' },
        { key: 'mtd', label: 'MTD' },
        { key: 'qtd', label: 'QTD' },
        { key: 'ytd', label: 'YTD' }
      ];

      this.selectedRange = this.dateRangeOptions[
        this.dateRangeOptions.findIndex(
          item => item['key'] == this.config['date_range']
        )
      ];

      this.rangeDates = [];
      this.dateRangeCompareOptions = [
        { key: 'custom', label: 'Custom' },
        { key: 'previous_period', label: 'Previous Period' }
      ];


      if (this.config['preCompare'] === 'none') {
        this.isCustomPrev = false;
        this.selectedCompareValues = false;
      } else {
        this.selectedCompareRange = this.dateRangeCompareOptions[
          this.dateRangeCompareOptions.findIndex(
            item => item['key'] == this.config['preCompare']
          )
        ];
        if (this.selectedCompareRange && this.selectedCompareRange['key'] == 'custom') {
          this.isCustomPrev = true;
        }
      }

      setTimeout(() => {
        this.updateDate();
      }, 0);
    }
  }

  updateSaveDate() {
    this.multiDateRange = []
    this.dateFilters.forEach((element, idx) => {
      this.minDate = moment(element['minDate'], 'YYYYMMDD').toDate();

      if (element['date_type'] == "custom") {
        this.rangeDates = [];

        let startDate = moment(element['defaultDate'][0]);
        let endDate = moment(element['defaultDate'][1]);

        this.filterType = element['filterType'];

        if (element['filterType'] === 'single_date') {
          this.rangeDates.push(startDate.toDate());
        } else if (element['filterType'] === 'date_range') {
          this.rangeDates.push([startDate.toDate(), endDate.toDate()]);
          if (this.isMultiple) {
            this.multiDateRange.push([startDate.toDate(), endDate.toDate()])
          }
        } else if (element['filterType'] === 'single_month') {
          this.rangeDates.push(startDate.toDate());
        } else if (element['filterType'] === 'month_range') {
          if (this.isMultiple) {

          } else {
            this.rangeDates.push([startDate.toDate(), endDate.toDate()]);
          }
        } else if (element['filterType'] === 'date_range_compare') {
          this.rangeDates = [startDate.toDate(), endDate.toDate()];
          this.changeDate(element['date_range'], '');
        }
        setTimeout(() => {
          this.onDateSelected();
        }, 0);

        this.saveDateCon = {
          isEnable: element['isEnable'],
          time_key1: element['defaultDate'][0],
          time_key2: element['defaultDate'][1],
          date_type: element['date_type']
        }
        this.saveViewDate[idx] = this.saveDateCon;
      } else {
        this.isDefaultVal = true;
        let period = element['date_type'];
        this.saveDateCon = {
          isEnable: element['isEnable'],
          time_key1: element['defaultDate'][0],
          time_key2: element['defaultDate'][1],
          date_type: element['date_type']
        }
        this.saveViewDate[idx] = this.saveDateCon;
        this.changeDate(period, idx);
      }
    });
  }

  updateDate() {
    this.dateFilters.forEach((element, idx) => {
      let startDate;
      let endDate;

      // this.minDate = moment(this.config['minDate'], 'YYYYMMDD').toDate();
      this.minDate = moment(element['minDate'], 'YYYYMMDD').toDate();

      if ((element['defaultDate'][0]['value'] === 0 && element['defaultDate'][0]['period'] === 'month') && (moment().format('DD') === '01' || moment().format('DD') === '02')) {
        startDate = moment().subtract(1, 'months').startOf('month');
        endDate = moment().subtract(1, 'months').endOf('month');
      } else {

        if (element['defaultDate'][0]['startOf']) {
          startDate = moment()
            .subtract(
              element['defaultDate'][0]['value'],
              element['defaultDate'][0]['period']
            )
            .startOf(element['defaultDate'][0]['period']);
        } else {
          startDate = moment().subtract(
            element['defaultDate'][0]['value'],
            element['defaultDate'][0]['period']
          );
        }

        endDate = moment().subtract(
          element['defaultDate'][1]['value'],
          element['defaultDate'][1]['period']
        );
      }

      this.filterType = element['filterType'];
      if (element['filterType'] === 'single_date') {
        this.rangeDates.push(startDate.toDate());
      } else if (element['filterType'] === 'date_range') {
        startDate = new Date(startDate);
        endDate = new Date(endDate);
        if (typeof startDate !== 'undefined' && typeof endDate !== 'undefined') {
          this.rangeDates.push([startDate, endDate]);
          this.finalDateRange.push(this.rangeDates)
        }
        if (this.isMultiple) {
          this.multiDateRange.push([startDate, endDate])

        }


      } else if (element['filterType'] === 'single_month') {
        this.rangeDates.push(startDate.toDate());
      } else if (element['filterType'] === 'month_range') {
        this.rangeDates.push([startDate.toDate(), endDate.toDate()]);
      } else if (element['filterType'] === 'date_range_compare') {
        this.rangeDates = [startDate.toDate(), endDate.toDate()];
        this.changeDate(element['date_range'], '');
      }
    });
  }

  changeDate(period: string, bucketName) {
    if (this.isSaveView && this.showToggle) {
      this.saveViewDate[bucketName]['date_type'] = period;
    }
    this.bucketName = bucketName;
    this.savePeriod[bucketName] = period;
    const today = new Date();
    let firstDate = new Date();
    let lastDate = new Date();

    let prevFirstDate = new Date();
    let prevLastDate = new Date();

    switch (period) {
      case 'today':
        {
          firstDate = today;
          lastDate = today;
          this.rangeDates = [firstDate, lastDate];

          prevFirstDate = moment(firstDate)
            .subtract(1, 'days')
            .toDate();
          prevLastDate = moment(lastDate)
            .subtract(1, 'days')
            .toDate();
          this.rangeDates1 = [prevFirstDate, prevLastDate];

          this.isCustom = false;
        }
        break;

      case 'yesterday':
        {
          firstDate.setDate(today.getDate() - 1);
          if (this.config['isCompare']) {
            this.rangeDates = [firstDate, firstDate];
          }
          else if (this.isMultiple) {
            //bucketName = [];
            this.multiDateRange[bucketName] = [];
            this.multiDateRange[bucketName].push(firstDate);
            this.multiDateRange[bucketName].push(lastDate);
          }
          else {
            this.rangeDates = [];
            this.rangeDates.push([new Date(firstDate), new Date(firstDate)]);
          }
          prevFirstDate = moment(firstDate)
            .subtract(1, 'days')
            .toDate();
          prevLastDate = moment(lastDate)
            .subtract(1, 'days')
            .toDate();
          this.rangeDates1 = [prevFirstDate, prevLastDate];
          this.isCustom = false;

        }
        break;
      case 'last_7_days':
        {
          firstDate.setDate(today.getDate() - 7);
          lastDate.setDate(today.getDate() - 1);
          if (this.config['isCompare']) {
            this.rangeDates = [firstDate, lastDate];
          }
          else if (this.isMultiple) {
            // bucketName = [];
            // bucketName.push(firstDate);
            // bucketName.push(lastDate);
            // console.log('in bucket',bucketName);
            this.multiDateRange[bucketName] = [];
            this.multiDateRange[bucketName].push(firstDate);
            this.multiDateRange[bucketName].push(lastDate);
          }
          else {
            this.rangeDates = [];
            this.rangeDates.push([new Date(firstDate), new Date(lastDate)]);
          }
          //        this.rangeDates.push(new Date(lastDate))
          prevFirstDate = moment(firstDate, 'YYYYMMDD')
            .subtract(7, 'days')
            .toDate();
          prevLastDate = moment(lastDate, 'YYYYMMDD')
            .subtract(7, 'days')
            .toDate();
          this.rangeDates1 = [prevFirstDate, prevLastDate];

          this.isCustom = false;


        }
        break;
      case 'last_30_days':
        {
          firstDate.setDate(today.getDate() - 30);
          lastDate.setDate(today.getDate() - 1);
          // this.rangeDates = [firstDate, lastDate];

          if (this.config['isCompare']) {
            this.rangeDates = [firstDate, lastDate];
          }
          else if (this.isMultiple) {
            // bucketName = [];
            // bucketName.push(firstDate);
            // bucketName.push(lastDate);
            // console.log('in bucket',bucketName);   
            this.multiDateRange[bucketName] = [];
            this.multiDateRange[bucketName].push(firstDate);
            this.multiDateRange[bucketName].push(lastDate);
          }
          else {
            this.rangeDates = [];
            this.rangeDates.push([new Date(firstDate), new Date(lastDate)]);
          }

          prevFirstDate = moment(firstDate)
            .subtract(30, 'days')
            .toDate();
          prevLastDate = moment(lastDate)
            .subtract(30, 'days')
            .toDate();
          this.rangeDates1 = [prevFirstDate, prevLastDate];

          this.isCustom = false;
          if (this.isMultiple) {
            bucketName = [];
            bucketName.push(firstDate);
            bucketName.push(lastDate);
          }
        }
        break;
      case 'this_month':
        {
          firstDate = new Date(today.getFullYear(), today.getMonth(), 1);
          lastDate = new Date(today.getFullYear(), today.getMonth() + 1, 0);
          // this.rangeDates = [firstDate, lastDate];

          if (this.config['isCompare']) {
            this.rangeDates = [firstDate, lastDate];
          }
          else if (this.isMultiple) {
            this.multiDateRange[bucketName] = [];
            this.multiDateRange[bucketName].push(firstDate);
            this.multiDateRange[bucketName].push(lastDate);
          }
          else {
            this.rangeDates = [];
            this.rangeDates.push([new Date(firstDate), new Date(lastDate)]);
          }

          prevFirstDate = new Date(
            firstDate.getFullYear(),
            firstDate.getMonth() - 1,
            1
          );
          prevLastDate = new Date(
            lastDate.getFullYear(),
            lastDate.getMonth(),
            0
          );
          this.rangeDates1 = [prevFirstDate, prevLastDate];
          this.isCustom = false;
          if (this.isMultiple) {
            bucketName = [];
            bucketName.push(firstDate);
            bucketName.push(lastDate);
          }
        }
        break;
      case 'last_month':
        {
          firstDate = new Date(
            today.getFullYear() - (today.getMonth() > 0 ? 0 : 1),
            (today.getMonth() - 1 + 12) % 12,
            1
          );
          lastDate = new Date(today.getFullYear(), today.getMonth(), 0);
          // this.rangeDates = [firstDate, lastDate];

          if (this.config['isCompare']) {
            this.rangeDates = [firstDate, lastDate];
          }

          else if (this.isMultiple) {
            this.multiDateRange[bucketName] = [];
            this.multiDateRange[bucketName].push(firstDate);
            this.multiDateRange[bucketName].push(lastDate);
          }
          else {
            this.rangeDates = [];
            this.rangeDates.push([new Date(firstDate), new Date(lastDate)]);
          }

          prevFirstDate = moment(firstDate)
            .subtract(30, 'days')
            .toDate();
          prevLastDate = moment(lastDate)
            .subtract(30, 'days')
            .toDate();
          this.rangeDates1 = [prevFirstDate, prevLastDate];

          this.isCustom = false;

          if (this.isMultiple) {
            this.multiDateRange[bucketName] = [];
            this.multiDateRange[bucketName].push(firstDate);
            this.multiDateRange[bucketName].push(lastDate);
          }
        }
        break;
      case 'mtd':
        {
          firstDate = new Date(today.getFullYear(), today.getMonth(), 1);
          lastDate.setDate(today.getDate());
          this.rangeDates = [firstDate, lastDate];

          prevFirstDate = moment(firstDate, 'YYYYMMDD')
            .subtract(1, 'month')
            .toDate();

          prevLastDate = moment(prevFirstDate, 'YYYYMMDD')
            .endOf('month')
            .toDate();

          this.rangeDates1 = [prevFirstDate, prevLastDate];
          this.isCustom = false;
        }
        break;
      case 'qtd':
        {
          firstDate = new Date(
            moment()
              .quarter(moment().quarter())
              .startOf('quarter')
              .toDate()
          );
          lastDate.setDate(today.getDate());
          this.rangeDates = [firstDate, lastDate];

          prevFirstDate = moment(firstDate)
            .subtract(1, 'quarter')
            .toDate();

          const days = moment(firstDate, 'YYYYMMDD').diff(
            moment(lastDate, 'YYYYMMDD'),
            'days'
          );
          prevLastDate = moment(prevFirstDate)
            .add(3, 'months')
            .toDate();

          prevLastDate = moment(prevLastDate)
            .subtract(1, 'days')
            .toDate();

          this.rangeDates1 = [prevFirstDate, prevLastDate];
          this.isCustom = false;
        }
        break;
      case 'ytd':
        {
          firstDate = new Date(
            moment()
              .startOf('year')
              .toDate()
          );
          lastDate.setDate(today.getDate());
          this.rangeDates = [firstDate, lastDate];

          prevFirstDate = moment(firstDate)
            .subtract(1, 'year')
            .toDate();

          prevLastDate = moment(prevFirstDate)
            .endOf('year')
            .toDate();

          this.rangeDates1 = [prevFirstDate, prevLastDate];

          this.isCustom = false;
        }
        break;
      case 'last_week':
        {
          firstDate = new Date(
            moment()
              .subtract(1, 'weeks')
              .startOf('isoWeek')
              .toDate()
          );
          lastDate = new Date(
            moment()
              .subtract(1, 'weeks')
              .endOf('isoWeek')
              .toDate()
          );
          this.rangeDates = [firstDate, lastDate];

          prevFirstDate = new Date(
            moment()
              .subtract(2, 'weeks')
              .startOf('isoWeek')
              .toDate()
          );
          prevLastDate = new Date(
            moment()
              .subtract(2, 'weeks')
              .endOf('isoWeek')
              .toDate()
          );
          this.rangeDates1 = [prevFirstDate, prevLastDate];

          this.isCustom = false;
        }
        break;

      case 'custom':
        {
          if (this.config['defaultDate'][0]['startOf']) {
            firstDate = moment()
              .subtract(
                this.config['defaultDate'][0]['value'],
                this.config['defaultDate'][0]['period']
              )
              .startOf(this.config['defaultDate'][0]['period'])
              .toDate();
          } else {
            firstDate = moment()
              .subtract(
                this.config['defaultDate'][0]['value'],
                this.config['defaultDate'][0]['period']
              )
              .toDate();
          }

          lastDate = moment()
            .subtract(
              this.config['defaultDate'][1]['value'],
              this.config['defaultDate'][1]['period']
            )
            .toDate();

          this.rangeDates = [firstDate, lastDate];
          this.isCustom = true;

          if (this.selectedCompareValues) {
            this.rangeDates1 = [new Date(), new Date()];
            const days = moment(firstDate, 'YYYYMMDD').diff(
              moment(lastDate, 'YYYYMMDD'),
              'days'
            );
            this.rangeDates1[1] = moment(this.rangeDates[0], 'YYYYMMDD')
              .subtract(1, 'days')
              .toDate();
            this.rangeDates1[0] = moment(this.rangeDates1[1], 'YYYYMMDD')
              .subtract(Math.abs(days), 'days')
              .toDate();
          }
        }
        break;
    }

    if (this.config['isCompare'] && this.selectedCompareValues) {
      this.datePickerChanged.emit({
        dateBucketCurrent: [
          moment(this.rangeDates[0]).format('YYYYMMDD'),
          moment(this.rangeDates[1]).format('YYYYMMDD')
        ],
        dateBucketPrevious: [
          moment(this.rangeDates1[0]).format('YYYYMMDD'),
          moment(this.rangeDates1[1]).format('YYYYMMDD')
        ]
      });
    }
    else if (this.isMultiple) {
      let tempArr = [];
      this.dateFilters.forEach((element, i) => {
        let obj = {};
        let key = element.key;
        obj = {
          [key]: this.multiDateRange[i]
        }
        tempArr.push(obj);
      });
      this.datePickerChanged.emit(tempArr);
    }
    else {
      let dateBucketCurrent = [];
      this.datePickerChanged.emit({
        dateBucketCurrent: [
          moment(this.rangeDates[0][0]).format('YYYYMMDD'),
          moment(this.rangeDates[0][1]).format('YYYYMMDD')
        ]
      });
    }

    if (this.isSaveView && this.showToggle) {
      this.onDateSelected();
    }
  }

  changePrevDate(selectedValue) {
    if (selectedValue['key'] === 'custom') { this.isCustomPrev = true; } else { this.isCustomPrev = false; }
  }
  checkBoxChanged() {
    if (this.config['isCompare'] && this.selectedCompareValues) {
      this.datePickerChanged.emit({
        dateBucketCurrent: [
          moment(this.rangeDates[0]).format('YYYYMMDD'),
          moment(this.rangeDates[1]).format('YYYYMMDD')
        ],
        dateBucketPrevious: [
          moment(this.rangeDates1[0]).format('YYYYMMDD'),
          moment(this.rangeDates1[1]).format('YYYYMMDD')
        ]
      });
    } else {
      this.datePickerChanged.emit({
        dateBucketCurrent: [
          moment(this.rangeDates[0]).format('YYYYMMDD'),
          moment(this.rangeDates[1]).format('YYYYMMDD')
        ]
      });
    }
  }

  onDateSelected() {
    if (this.config['isCompare']) {
      if (
        (this.selectedCompareValues &&
          this.selectedCompareRange != undefined &&
          this.selectedCompareRange['key'] != 'custom')
      ) {
        const days = moment(this.rangeDates[0], 'YYYYMMDD').diff(
          moment(this.rangeDates[1], 'YYYYMMDD'),
          'days'
        );

        this.rangeDates1[1] = moment(this.rangeDates[0], 'YYYYMMDD')
          .subtract(1, 'days')
          .toDate();
        this.rangeDates1[0] = moment(this.rangeDates1[1], 'YYYYMMDD')
          .subtract(Math.abs(days), 'days')
          .toDate();

        this.datePickerChanged.emit({
          dateBucketCurrent: [
            moment(this.rangeDates[0]).format('YYYYMMDD'),
            moment(this.rangeDates[1]).format('YYYYMMDD')
          ],
          dateBucketPrevious: [
            moment(this.rangeDates1[0]).format('YYYYMMDD'),
            moment(this.rangeDates1[1]).format('YYYYMMDD')
          ]
        });

      } else if
        (this.selectedCompareValues && this.selectedCompareRange != undefined &&
        this.selectedCompareRange['key'] == 'custom') {
        this.datePickerChanged.emit({
          dateBucketCurrent: [
            moment(this.rangeDates[0]).format('YYYYMMDD'),
            moment(this.rangeDates[1]).format('YYYYMMDD')
          ],
          dateBucketPrevious: [
            moment(this.rangeDates1[0]).format('YYYYMMDD'),
            moment(this.rangeDates1[1]).format('YYYYMMDD')
          ]
        });

      } else {
        this.datePickerChanged.emit({
          dateBucketCurrent: [
            moment(this.rangeDates[0]).format('YYYYMMDD'),
            moment(this.rangeDates[1]).format('YYYYMMDD')
          ]
        });
      }
    }
    else if (this.isMultiple) {
      const tempArr1 = [];
      this.multiDateRange.forEach((element, i) => {
        if (this.dateFilters[i]['isEnable']) {
          let startDate = moment(element[0]).format('YYYYMMDD');
          let endDate = moment(element[1]).format('YYYYMMDD');
          let obj = {};
          let key = this.dateFilters[i].key;

          obj = {
            [key]: [startDate, endDate]
          }
          tempArr1.push(obj);
        }
      });
      this.datePickerChanged.emit(tempArr1);
      document.body.click();
    }
    else {
      if (
        (this.config['filterType'] === 'month_range' ||
          this.config['filterType'] === 'date_range') &&
        this.rangeDates[0][1] === null
      ) {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Invalid Date',
          detail: 'Please select valid date range'
        });
        return;
      }
      document.body.click();
      const tempArr = [];

      if (this.filterType == 'single_month') {
        for (let i = 0; i < this.dateFilters.length; i++) {
          tempArr.push(moment(this.rangeDates[i]).format('YYYYMMDD'));
        }
      } else if (this.filterType == 'single_date') {
        for (let i = 0; i < this.dateFilters.length; i++) {
          tempArr.push(moment(this.rangeDates[i]).format('YYYYMMDD'));
        }
      } else if (this.filterType == 'date_range') {
        for (let i = 0; i < this.rangeDates.length; i++) {
          const t1 = moment(this.rangeDates[i][0]).format('YYYYMMDD');
          const t2 = moment(this.rangeDates[i][1]).format('YYYYMMDD');

          tempArr.push(t1);
          tempArr.push(t2);
        }
      }
      else if (this.filterType == 'month_range') {
        const t1 = moment(this.rangeDates[0][0]).format('YYYYMMDD');
        const t2 = moment(this.rangeDates[0][1], 'YYYYMMDD')
          .endOf('month')
          .toDate();

        tempArr.push(t1);
        tempArr.push(t2);
      }
      this.datePickerChanged.emit(
        tempArr
      );
    }
  }

  onChangeFilterStatus(idx, isEnable) {
    this.saveViewDate.forEach((x, i) => {
      if (idx === i) {
        this.saveViewDate[idx]['isEnable'] = isEnable;
      }
    })
  }

  isEnableDate(dimIdx) {
    if (this.isSaveView && this.showToggle) {
      this.saveViewDate[dimIdx]['date_type'] = "custom";
    }
  }

  saveViewSelectedDates() {
    if (this.isSaveView && this.showToggle) {
      this.multiDateRange.forEach((element, i) => {
        this.saveViewDate[i]['time_key1'] = moment(element[0]).format('YYYYMMDD')
        this.saveViewDate[i]['time_key2'] = moment(element[1]).format('YYYYMMDD')
      })
      setTimeout(() => {
        this.periodSeledted.emit({
          savePeriod: this.saveViewDate,
        });
        setTimeout(() => {
          this.savePeriod = [];
          this.bucketName = "";
        }, 0);
      }, 0);
    }
  }
}
