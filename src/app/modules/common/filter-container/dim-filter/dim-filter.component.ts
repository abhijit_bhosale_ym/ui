import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
  OnDestroy
} from '@angular/core';
import { FilterDataService, CommonLibService } from '../../../../_services';
import { Dimension } from '../models/dimension.model';
import { SaveQueryService } from 'src/app/_services/save-query/save-query.service';
import * as moment from 'moment';
import { Subscription } from 'rxjs';


@Component({
  selector: 'ym-dim-filter',
  templateUrl: './dim-filter.component.html',
  styleUrls: ['./dim-filter.component.scss']
})
export class DimFilterComponent implements OnInit, OnDestroy {
  @ViewChild('dimensionsDropdown') dimensionsDropdown: any;
  @ViewChild('dimensionsFilters') dimensionsFilters: any;
  @Input() defaultDimensions: Dimension[];
  @Input() availableDimensions: Dimension[];
  @Input() selectedDates: Date[];
  @Input() dateDimensionRelation = false;
  @Input() filterUrl: string;
  @Output() dimFilterChanged = new EventEmitter<object>();
  @Input() excludeValues = [];
  @Input() isSaveView: boolean;
  @Input() showToggle: boolean;

  selectedDimensionToAdd: Dimension = { key: '', label: '', config_json: '' };
  availableFilterData: object = {};
  selectedFilterData: object = {};
  selectedFilterDataLocal: object = {};
  selectedDateFilter: object = {};
  selectionLimit: 'multi';
  selectedLabel = 1;

  savedQueryServiceSelectedQuery1: Subscription;
  filterService: Subscription;

  constructor(
    private filterDataService: FilterDataService,
    private saveQueryService: SaveQueryService,
    private libServ: CommonLibService
  ) { }

  ngOnInit() {
    this.savedQueryServiceSelectedQuery1 = this.saveQueryService.selectedQuery.subscribe(query => {
      this.selectedLabel = 1;
      this.defaultDimensions.forEach(obj => {
        this.availableFilterData[obj['key']] = [];
        this.selectedFilterDataLocal[obj['key']] = [];
      });
      this.availableDimensions.forEach(obj => {
        this.availableFilterData[obj['key']] = [];
        this.selectedFilterDataLocal[obj['key']] = [];
      });

      if (this.isSaveView && this.showToggle) {
        this.selectedLabel = 0;
        setTimeout(() => {
          if (
            !this.libServ.isEmptyObj(query) &&
            Object.keys(query['filter']['dimensions']).length
          ) {
            this.selectedFilterData = this.libServ.deepCopy(
              query['filter']['dimensions']
            );
            this.selectedFilterDataLocal = this.libServ.deepCopy(
              query['filter']['dimensions']
            );
          } else {
            this.defaultDimensions.forEach(obj => {
              this.availableFilterData[obj['key']] = [];
              this.selectedFilterDataLocal[obj['key']] = [];
            });
            this.availableDimensions.forEach(obj => {
              this.availableFilterData[obj['key']] = [];
              this.selectedFilterDataLocal[obj['key']] = [];
            });
          }
        }, 0);
      }
    });

    if (this.dateDimensionRelation) {
      this.selectedDateFilter = {
        time_key1: moment(this.selectedDates[0]).format('YYYYMMDD'),
        time_key2: moment(this.selectedDates[1]).format('YYYYMMDD')
      };
    }

    const isSelected = this.defaultDimensions[0]['isSelected'];
    if (isSelected) {
      this.getFilterValues(this.defaultDimensions[0].key, {});
    }
  }

  onClickFilterCom(dim: Dimension, dimIdx: number) {
    if (this.availableFilterData[dim['key']].length > 0) {
      return;
    }
    const prevFilters = this.selectedDateFilter;
    this.defaultDimensions.forEach((d, idx) => {
      if (idx < dimIdx) {
        prevFilters[d['key']] = this.selectedFilterData[d['key']];
      }
    });
    this.getFilterValues(dim['key'], prevFilters);
  }

  onChangeFilterCom(dim: Dimension, dimIdx: number) {
    if (
      this.selectedFilterDataLocal[dim['key']] ===
      this.selectedFilterData[dim['key']]
    ) {
      return;
    }
    this.selectedFilterDataLocal[dim['key']] = this.selectedFilterData[
      dim['key']
    ];

    for (let i = 0; i < this.defaultDimensions.length; i++) {
      if (i > dimIdx) {
        this.availableFilterData[this.defaultDimensions[i]['key']] = [];
        this.selectedFilterData[this.defaultDimensions[i]['key']] = [];
        this.selectedFilterDataLocal[this.defaultDimensions[i]['key']] = [];
      }
    }
    const nextDim = this.defaultDimensions[dimIdx + 1];

    if (typeof nextDim !== 'undefined') {
      this.getFilterValues(
        nextDim['key'],
        Object.assign({}, this.selectedDateFilter, this.selectedFilterData)
      );
    }
    if (
      !this.libServ.isEqual(
        this.availableFilterData[dim['key']].map(e => e.key),
        this.selectedFilterData[dim['key']]
      )
    ) {
      this.dimFilterChanged.emit(this.selectedFilterData);
    }
  }

  getFilterValues(dim: string, params: object) {
    console.log('param', params)
    let keys = Object.keys(params)

    keys.forEach(element => {
      console.log('typeof',typeof  params[element] )
      // if (element != 'time_key1' && element != 'time_key2') {
        if (params[element] != undefined && (typeof  params[element]) != 'string' && params[element].length > 0) {
          const str = '\'' + params[element].join('\'$$\'') + '\'';
          params[element] = [str];
        }
      // }
    });

    this.filterDataService
      .getFilterDataList(`${this.filterUrl}/${dim}`, params)
      .subscribe(res => {
        this.availableFilterData[dim] = [];
        const arr = this.libServ.deepCopy(res['data']);
        const arrExclude =
          this.defaultDimensions[0]['config'] &&
            this.defaultDimensions[0]['config'].hasOwnProperty('exclude')
            ? this.defaultDimensions[0]['config'].exclude.split(',')
            : [];
        for (let i = 0; i < arr.length; i++) {
          arr[i]['value'] = arr[i]['label'];
        }

        arrExclude.forEach(element => {
          if (arr.some(o => o.label === element)) {
            arr.splice(
              arr.findIndex(x => x.label === element),
              1
            );
          }
        });
        if (typeof this.excludeValues[dim] != 'undefined') {
          this.excludeValues[dim].forEach(element => {
            if (arr.some(o => o.label === element)) {
              arr.splice(
                arr.findIndex(x => x.label === element),
                1
              );
            }
          });
        }
        this.availableFilterData[dim].push(...arr);

        if (
          dim == this.defaultDimensions[0].key &&
          this.defaultDimensions[0]['isSelected']
        ) {
          // this.selectedFilterData[dim]=[];
          const selItems = [];
          setTimeout(() => {
            for (
              let i = 0;
              i < this.defaultDimensions[0]['selectionLimit'];
              i++
            ) {
              selItems.push(arr[i]['label']);
            }
          }, 0);
          setTimeout(() => {
            this.selectedFilterData[dim] = selItems;
            this.dimFilterChanged.emit(this.selectedFilterData);
            document
              .querySelector('ym-filter-container')
              .querySelector(
                'p-footer > button.ui-button-rounded.ui-button-success'
              )
              .dispatchEvent(new Event('click'));
          }, 0);
        }
      });
  }

  remove(d: Dimension) {
    const idx = this.defaultDimensions.indexOf(d);

    for (let i = idx; i < this.defaultDimensions.length; i++) {
      this.selectedFilterData[this.defaultDimensions[i]['key']] = [];
      this.availableFilterData[this.defaultDimensions[i]['key']] = [];
      this.selectedFilterDataLocal[this.defaultDimensions[i]['key']] = [];
    }
    if (idx + 1 < this.defaultDimensions.length) {
      this.getFilterValues(
        this.defaultDimensions[idx + 1]['key'],
        Object.assign({}, this.selectedDateFilter, this.selectedFilterData)
      );
    }

    this.availableDimensions.push(...this.defaultDimensions.splice(idx, 1));
    setTimeout(() => {
      this.dimensionsDropdown.options = this.availableDimensions;
    }, 0);
    this.dimFilterChanged.emit(this.selectedFilterData);
  }

  dimensionAdded() {
    this.defaultDimensions.push(this.selectedDimensionToAdd);
    this.availableDimensions.splice(
      this.availableDimensions.indexOf(this.selectedDimensionToAdd),
      1
    );
    this.getFilterValues(
      this.selectedDimensionToAdd['key'],
      Object.assign({}, this.selectedDateFilter, this.selectedFilterData)
    );
    this.selectedDimensionToAdd = { key: '', label: '', config_json: '' };
    this.dimensionsDropdown.options = this.availableDimensions;
  }

  hidePanel(event) {
    event.hide();
  }


  pluralize(label) {
    if (label.endsWith('y')) {
      return label.slice(0, -1) + 'ies';
    } if (label.endsWith('s')) {
      return label;
    } else {
      return label + 's';
    }
  }

  ngOnDestroy() {
    if (this.savedQueryServiceSelectedQuery1 && !this.savedQueryServiceSelectedQuery1.closed) {
      this.savedQueryServiceSelectedQuery1.unsubscribe();
    }
  }
}
