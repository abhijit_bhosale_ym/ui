import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import * as moment from 'moment';
import { Dimension } from '../models/dimension.model';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { SaveQueryService } from 'src/app/_services/save-query/save-query.service';
import { CommonLibService } from 'src/app/_services/common-lib/common-lib.service';
import { environment } from 'src/environments/environment';
import { Subscription } from 'rxjs';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';

@Component({
  selector: 'ym-edit-save-view',
  templateUrl: './edit-save-view.component.html',
  styleUrls: ['./edit-save-view.component.scss']
})
export class EditSaveViewComponent implements OnInit {

  @Input() dateDimensionRelation = false;

  appConfigObs: Subscription;
  appConfigObs1: Subscription;
  public appConfig = {};
  public saveViewName: String = '';
  public setDefaultCheckbox = false;
  public appFilters = {};
  public data = [];
  public date_filter_config = [];
  public defaultDimensions = [];
  public availableDimensions = [];
  public defaultMetrics = [];
  public availableMetrics = [];
  public filterUrl = '';
  public selectedDimFilter: object = {};
  public selectedMetricFilter: object = {};
  public selectedGrpBy: Dimension[];
  public selectedDates: Date[];
  public groupByDims = [];
  public savedViews = [];
  public enableDatePicker = false;
  public selectedSavedView: string;
  private BASE_URL: string = environment.baseUrl;
  jsonData = {};
  dimensionData = {};
  defaultSaveValue = false;
  savePeriod=[];
  saveDate={};
  bucketName:any;

  constructor(
    private appConfigService: AppConfigService,
    private saveQueryService: SaveQueryService,
    public libServ: CommonLibService,
    private config: DynamicDialogConfig,
    public ref: DynamicDialogRef,
    private toastService: ToastService,
  ) {
    this.jsonData = this.config.data;
  }

  ngOnInit() {
    this.appConfigObs = this.appConfigService.appConfig.subscribe(appConfig => {
      if (!this.libServ.isEmptyObj(appConfig)) {        
        this.appConfig = appConfig;        
        if (!this.libServ.isEmptyObj(appConfig['filter'])) {
          this.appFilters = this.libServ.deepCopy(
            appConfig['filter']['filterConfig']['filters']
          );
          this.selectedGrpBy = this.libServ.deepCopy(
            appConfig['filter']['filterConfig']['groupBy']
          );
        }
        this.changeConfig();
      }
    });    
    this.initialLoading();
  }

  initialLoading() {
    this.data = this.appFilters['dimension']['default'];
    this.dimensionData = JSON.parse(this.jsonData['data']['state_json'])['filter']['dimensions'];
    if (this.jsonData['buttonValue'] == 'Save') {
      // this.onResetFilterClicked();
    } else {
      // this.onResetFilterClicked();
      this.defaultSaveValue = (this.jsonData['data']['is_default'] == 'true');
      this.saveViewName = this.jsonData['data']['state_name'];
      this.setDefaultCheckbox = (this.jsonData['data']['is_default'] == 'true');      
    }
  }

  saveQuery() {
    let date = [];
    let dateObj ={}
    if(this.savePeriod.length){
      this.savePeriod.forEach((element,i) =>{
        date.push({
          ['date_range'+(i+1)]: {
            time_key1 : element['time_key1'],
            time_key2 : element['time_key2'],
            isEnable : element['isEnable'],
            date_type : element['date_type']
          }
        })
      })
      date.forEach(x => {
        Object.assign(dateObj,x)
      })  
    } else {
      dateObj = this.selectedSavedView['date'];
    }

    this.selectedGrpBy.forEach(element => {
      element['selected'] = true;
    });

    const config_json = {
      date: dateObj,
      availableDimensions: this.availableDimensions,
      availableMetrics: this.availableMetrics,

      defaultDimensions: this.defaultDimensions,
      defaultMetrics: this.defaultMetrics,

      groupby: this.selectedGrpBy
    };
    if (Object.keys(this.selectedDimFilter).length > 0 && Object.keys(this.dimensionData).length == 0) {
      Object.assign(config_json, { filter: { dimensions: this.selectedDimFilter, metrics: this.selectedMetricFilter } });

    } else if (Object.keys(this.selectedDimFilter).length == 0 && Object.keys(this.dimensionData).length > 0) {
      Object.assign(config_json, {
        filter: {
          dimensions: this.dimensionData,
          metrics: this.selectedMetricFilter
        }
      });
    } else {
      for (const key in this.dimensionData) {
        if (typeof this.selectedDimFilter[key] != 'undefined') {
          const t = this.selectedDimFilter[key].concat(this.dimensionData[key]);
          this.dimensionData[key] = t;
        }
      }

      Object.assign(config_json, {
        filter: {
          dimensions: this.dimensionData,
          metrics: this.selectedMetricFilter
        }
      });
    }

    Object.assign(config_json, { tabPanel: this.selectedSavedView['tabPanel'] });
    Object.assign(config_json, { toggleView: this.selectedSavedView['toggleView'] });
    Object.assign(config_json, { selectedColDefs: this.jsonData['column'] });
    const param = {
      userId: this.appConfig['user']['id'],
      appId: this.appConfig['id'],
      saveViewJson: config_json,
      stateName: this.saveViewName,
      isDefault: this.setDefaultCheckbox,
      id: this.jsonData['data']['id']
    };
    if (param.stateName == '') {
      this.toastService.displayToast({
        severity: 'error',
        summary: 'Error',
        detail: 'Please Enter State Name'
      });
    } else {
      if (this.jsonData['buttonValue'] == 'Save') {
        this.saveQueryService.addSavedQueries(param).subscribe(res => {
          if (res['status'] === false) {
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Server Error',
              detail: 'Please refresh the page'
            });
            console.log(res['status_msg']);
            return;
          } else {
            this.toastService.displayToast({
              severity: 'success',
              summary: 'Create View',
              detail: 'View Created Successfully'
            });
          }
          this.saveQueryService.getSavedQueries(param);
        });
        this.ref.close(null);
      } else {
        this.saveQueryService.updateSavedQueries(param).subscribe(res => {
          if (res['status'] === false) {
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Server Error',
              detail: 'Please refresh the page'
            });
            console.log(res['status_msg']);
            return;
          } else {
            this.toastService.displayToast({
              severity: 'success',
              summary: 'Update View',
              detail: 'View Updated Successfully'
            });
          }
          this.saveQueryService.getSavedQueries(param);
        });
        this.ref.close(null);
      }
    }
  }

  periodSeledted(event)
  {
    this.savePeriod =  event['savePeriod']; 
  }

  removeDimension() {
    this.onResetFilterClicked();
  }

  onResetFilterClicked() {
    this.appConfig = {};
    this.selectedDimFilter = {};
    this.selectedMetricFilter = {};
    this.selectedGrpBy = [];
    this.selectedDates = [];
    this.groupByDims = [];
    this.savedViews = [];

    setTimeout(() => {
      this.appConfigObs1 = this.appConfigService.appConfig.subscribe(appConfig => {
        if (!this.libServ.isEmptyObj(appConfig)) {
          this.appConfig = appConfig;
          if (!this.libServ.isEmptyObj(appConfig['filter'])) {

            this.appFilters = this.libServ.deepCopy(
              appConfig['filter']['filterConfig']['filters']
            );
            this.selectedGrpBy = this.libServ.deepCopy(
              appConfig['filter']['filterConfig']['groupBy']
            );
            this.changeConfig();

            if (this.appConfig['filter']['filterConfig']['saveView']) {
              this.selectedDimFilter = {};
              this.selectedMetricFilter = {};
              this.saveQueryService.changeSelectedQuery({
                date_filter_config: this.date_filter_config,
                filter: {
                  dimensions: this.selectedDimFilter,
                  metrics: this.selectedDimFilter
                }
              });
            }
          }
        }
      });
    }, 0);
  }


  changeConfig() { 
    this.filterUrl = `${this.BASE_URL}${this.appFilters['url']}`;
    this.defaultDimensions = this.appFilters['dimension']['default'];
    this.availableDimensions = this.appFilters['dimension']['available'];
    this.defaultMetrics = this.appFilters['metric']['default'];
    this.availableMetrics = this.appFilters['metric']['available'];
    this.groupByDims = this.selectedGrpBy;
    this.date_filter_config = this.appFilters['datePeriod'];

    this.selectedSavedView = JSON.parse(this.jsonData['data']['state_json']);
    let dates= this.selectedSavedView['date'];
      if (typeof this.appFilters['datePeriod'] !== 'undefined' && this.appFilters['datePeriod'].length > 0) {
        let key = Object.keys(this.selectedSavedView['date'])
        key.forEach((element,i) => {
          this.appFilters['datePeriod'][i]['isEnable'] = dates[element]['isEnable'];
          this.appFilters['datePeriod'][i]['defaultDate'][0] = dates[element]['time_key1'];
          this.appFilters['datePeriod'][i]['defaultDate'][1] = dates[element]['time_key2'];
          this.appFilters['datePeriod'][i]['date_type'] = dates[element]['date_type']
        });
      }
  
    this.saveQueryService.changeSelectedQuery({
      date_filter_config: this.date_filter_config,
      filter: {
        dimensions: this.selectedDimFilter,
        metrics: this.selectedMetricFilter
      },
      groupby: this.selectedGrpBy
    });
  }

  cancel() {
    this.ref.close(null);
  }

  ngOnDestroy(): void {
    if (this.appConfigObs && !this.appConfigObs.closed) {
      this.appConfigObs.unsubscribe();
    }
    if (this.appConfigObs1 && !this.appConfigObs1.closed) {
      this.appConfigObs1.unsubscribe();
    }
  }
}
