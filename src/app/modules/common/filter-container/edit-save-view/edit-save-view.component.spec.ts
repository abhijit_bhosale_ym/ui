import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSaveViewComponent } from './edit-save-view.component';

describe('EditSaveViewComponent', () => {
  let component: EditSaveViewComponent;
  let fixture: ComponentFixture<EditSaveViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSaveViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSaveViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
