import { Component, OnInit, Input, Renderer2, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  AuthService,
  UsersService,
  PlatformConfigService
} from '../../_services';
import { ToastService } from 'src/app/_services/toast-notification/toast.service';
import { LoaderObsService } from 'src/app/_services/api-loader/loader-obs.service';
import { DialogService } from 'primeng/dynamicdialog';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';
import { ImpersonateService } from 'src/app/_services/impersonate/impersonate.service';
import { Title } from '@angular/platform-browser';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  model: any = {};
  returnUrl: string;
  showPassword: object = {
    new: false,
    confirm: false
  };
  newUser = false;

  result: object;
  userEmail: string;
  token: String;
  loginConfig = {};
  message = "Welcome";
  img_url = "../../../assets/images/yuktaone logo.png";
  is_line = true;
  bottom_href = "http://yuktamedia.com/";
  bottom_img = "assets/images/ym-logo.png";
  bottom_alt_name = "YuktaMedia";

  login_config = {
    "data": {
      "header": {
        "message": "Welcome"
      },
      "card-body": {
        "color": "#96a8b9",
        "is_line": true,
        "logo_url": "../../../assets/images/yuktaone logo.png",
        "line_color": "22b6fd",
        "background_color": "rgba(0,0,0,.3)",
        "forgot_password_color": "#cbd8e2"
      },
      "bottom-img": {
        "href": "http://yuktamedia.com/",
        "img_url": "assets/images/ym-logo.png",
        "alternativ_name": "YuktaMedia"
      },
      "login-page": {
        "background_img": "linear-gradient(180deg, rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)),url('../../../assets/images/background_img.jpg') no-repeat center center fixed"
      }
    }
  }

  constructor(
    private authService: AuthService,
    private platformConfigService: PlatformConfigService,
    private route: ActivatedRoute,
    private router: Router,
    private impersonateServ: ImpersonateService,
    private _loadingBar: LoaderObsService,
    private userService: UsersService,
    private toastService: ToastService,
    private dialogService: DialogService,
    private _titleService: Title,
    private renderer: Renderer2,
    private _window: Window,
    @Inject(DOCUMENT) private _document: HTMLDocument
  ) {
    if (this.dialogService.dialogComponentRef) {
      this.dialogService.dialogComponentRef.destroy();
    }
    this.getLoginConfig();
    this.route.params.subscribe(res => (this.userEmail = res.email));
    this.route.params.subscribe(res => (this.token = res.token));
    if (this.router.url.indexOf('/auth/set-password') > -1) {
      this.newUser = true;
      this.model.password = '';
      localStorage.clear();
    }
    if (this.userEmail !== undefined && this.token !== undefined) {
      if (!this.newUser) {
        const req = {
          email: this.userEmail,
          token: this.token
        };
        this.userService.verify(req).subscribe(res => {
          this.result = res;
          if (this.result['status']) {
            const ref = this.dialogService.open(ForgotPasswordComponent, {
              header: 'Forgot Password',
              contentStyle: { 'max-height': '80vh', overflow: 'auto' },
              data: req
            });
          } else {
            localStorage.clear();
            this.router.navigate(['/login']);
          }
        });
      }
    }
  }

  onLogin(): void {
    // this.impersonateServ.stopImpersonate(); // Useful If impersonated and session timed-out
    this._loadingBar.start();
    if (this.newUser) {
      if (
        this.model['password'] === this.model['confirmPassword'] &&
        this.model['password'] !== '' &&
        typeof this.model['password'] !== 'undefined'
      ) {
        const req = {
          email: this.userEmail,
          password: this.model['password'],
          rememberToken: this.token
        };

        this.authService.setNewUserPassword(req).subscribe(res => {
          this.result = res;
          if (this.result['status']) {
            this.toastService.displayToast({
              severity: 'success',
              summary: 'Update successful',
              detail: 'User details updated successfully'
            });
            localStorage.clear();
            this.router.navigate(['/login']);
          } else {
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Something went wrong',
              detail: res['message']
            });
            localStorage.clear();
            this.router.navigate(['/login']);
          }
        });
      } else {
        this.toastService.displayToast({
          severity: 'error',
          summary: 'Password Mismatch',
          detail: 'Please make sure your passwords match'
        });
        this._loadingBar.complete();
      }
    } else {
      this.authService
        .login(this.model)
        .then(
          response => {
            // if (response['UserStatus']) {
            //   this.toastService.displayToast({
            //     severity: 'error',
            //     summary: 'Something went wrong',
            //     detail: response['message']
            //   });
            //   // this._loadingBar.complete();

            // }
            if (response['token']) {
              this._loadingBar.complete();
              this.platformConfigService.getPlatformConfigInfoData().then(data => {
                this.router.navigate(['/']);
              });
            } else {
              this.toastService.displayToast({
                severity: 'error',
                summary: 'Incorrect',
                detail: 'Email or Password incorrect'
              });
              localStorage.removeItem('token');
              this._loadingBar.complete();
            }
          },
          error => {
            console.log('error', error);
            this.toastService.displayToast({
              severity: 'error',
              summary: 'Incorrect',
              detail: error.error['message']
            });
            localStorage.removeItem('token');
            this._loadingBar.complete();

          }
        )
        .catch(err => {
          console.log('err', err);
          this.toastService.displayToast({
            severity: 'error',
            summary: 'Incorrect',
            detail: 'Email or Password incorrect'
          });
          localStorage.removeItem('token');
          this._loadingBar.complete();
        });
    }
  }

  togglePassword(pass: string) {
    this.showPassword[pass] = !this.showPassword[pass];
  }

  forgotPassword() {
    const ref = this.dialogService.open(ForgotPasswordComponent, {
      header: 'Forgot Password',
      contentStyle: { 'max-height': '80vh', overflow: 'auto' },
      data: this.model
    });
    ref.onClose.subscribe((data1: string) => { });
  }

  termsPopup() {
    const ref = this.dialogService.open(TermsConditionsComponent, {
      header: 'Terms and Conditions',
      contentStyle: { 'max-height': '80vh', overflow: 'auto' },
      data: this.model
    });
    ref.onClose.subscribe((data1: string) => { });
  }

  getLoginConfig() {
    // console.log("Location", this._window['location']);
    this.platformConfigService.getLoginConfig(this._window['location']).subscribe(data => {
      if (data['status'] === 1 && data['data'].length > 0) {
        let documentConfig = JSON.parse(data['data'][0]['document_config']);
        this._document.getElementById('appFavicon').setAttribute('href', documentConfig['document']['favicon']);
        this._titleService.setTitle(documentConfig['document']['title']);
        this.loginConfig = JSON.parse(data['data'][0]['login_config']);
        this.message = this.loginConfig['data']['header']['message'];
        this.img_url = this.loginConfig['data']['card-body']['logo_url'];
        this.is_line = this.loginConfig['data']['card-body']['is_line']
        this.bottom_href = this.loginConfig['data']['bottom-img']['href'];
        this.bottom_img = this.loginConfig['data']['bottom-img']['img_url'];
        this.bottom_alt_name = this.loginConfig['data']['bottom-img']['alternativ_name'];
        const parent: HTMLElement = document.getElementById('myDIV');
        const child = parent.children[0];
        this.renderer.setStyle(child, 'background-color', this.loginConfig['data']['card-body']['background_color']);
        this.renderer.setStyle(child, 'color', this.loginConfig['data']['card-body']['color']);
      }
      else {
        this._titleService.setTitle('YuktaOne');
        this._document.getElementById('appFavicon').setAttribute('href', '../../../../assets/images/ym-favicon.ico');
        this.loginConfig = this.login_config;
        const parent: HTMLElement = document.getElementById('myDIV');
        const child = parent.children[0];
        this.renderer.setStyle(child, 'background-color', this.loginConfig['data']['card-body']['background_color']);
        this.renderer.setStyle(child, 'color', this.loginConfig['data']['card-body']['color']);
      }
    });
  }
}
