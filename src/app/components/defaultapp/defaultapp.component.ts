import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { PlatformConfigService, CommonLibService } from 'src/app/_services';
import { Router } from '@angular/router';

@Component({
  selector: 'ym-defaultapp',
  template: '',
  styles: []
})
export class DefaultappComponent implements OnDestroy {
  private platConfigObs: Subscription;
  config: object;
  constructor(
    private platformConfigService: PlatformConfigService,
    private libServ: CommonLibService,
    private router: Router
  ) {
    this.platConfigObs = this.platformConfigService.obsConfig.subscribe(res => {
      if (!this.libServ.isEmptyObj(res)) {
        this.config = res;
        let defaultAppIdAvailable = false;
        console.log('aaaa...', this.config['user']['role']['appGroups']);
        const appId = this.config['user']['defaultAppId'];
        this.config['user']['role']['appGroups'].forEach(appGrp => {
          appGrp['apps'].forEach(app => {
            if (app['id'] === appId) {
              defaultAppIdAvailable = true;
              this.router.navigate([app.route]);
            }
          });
        });
        if (!defaultAppIdAvailable) {
          this.router.navigate([this.config['user']['role']['appGroups'][0]['apps'][0].route]);
        }
      }
    });
  }

  ngOnDestroy(): void {
    if (this.platConfigObs && !this.platConfigObs.closed) {
      this.platConfigObs.unsubscribe();
    }
  }
}
