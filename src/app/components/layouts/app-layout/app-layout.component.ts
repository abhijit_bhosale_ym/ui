import { Component, Input, OnInit, OnDestroy } from '@angular/core';

import { Router, NavigationEnd } from '@angular/router';
import { PlatformConfigService, CommonLibService } from 'src/app/_services';
import { AppConfigService } from 'src/app/_services/app-config/app-config.service';
import { filter } from 'rxjs/operators';
import { SaveQueryService } from 'src/app/_services/save-query/save-query.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'ym-app-layout',
  templateUrl: './app-layout.component.html',
  styleUrls: ['./app-layout.component.scss']
})
export class AppLayoutComponent implements OnDestroy {
  private platConfigObs: Subscription = null;
  platformConfig: object = null;

  // crumbs$: Observable<MenuItem[]>;
  sidenavWidth: number;

  constructor(
    private router: Router,
    private platformConfigService: PlatformConfigService,
    private libServ: CommonLibService,
    private appConfigService: AppConfigService,
    private saveQueryService: SaveQueryService
  ) {
    /* ----------------------------- Load AppConfig ----------------------------- */
    router.events
      .pipe(filter(e => e instanceof NavigationEnd))
      .subscribe(val => {
        this.getPlatformConfigData().then(response => {
          if (typeof this.platformConfig !== 'undefined' && (this.platformConfig !== null || this.platformConfig !== {})) {
            const appGroups = this.platformConfig['user']['role']['appGroups'];
            for (const appGrpIdx of Object.keys(appGroups)) {
              for (const appIdx of Object.keys(
                appGroups[appGrpIdx]['apps']
              )) {
                if (
                  appGroups[appGrpIdx]['apps'][appIdx]['route'] ===
                  this.router.url
                ) {
                  if (
                    appGroups[appGrpIdx]['apps'][appIdx]['filter']['filterConfig']['saveView']
                  ) {
                    this.saveQueryService.getSavedQueries({
                      userId: this.platformConfig['user']['id'],
                      appId: appGroups[appGrpIdx]['apps'][appIdx]['id']
                    });
                  }
                  this.appConfigService.changeAppConfig(
                    Object.assign({}, appGroups[appGrpIdx]['apps'][appIdx], {
                      user: this.platformConfig['user']
                    })
                  );
                }
              }
            }
          }
        });
      });
  }

  getPlatformConfigData() {
    const platformConfigPromise = new Promise((resolve, reject) => {
      if (this.platConfigObs === null || this.platConfigObs.closed) {
        this.platConfigObs = this.platformConfigService.obsConfig.subscribe(
          res => {
            if (!this.libServ.isEmptyObj(res)) {
              this.platformConfig = this.libServ.deepCopy(res);
              resolve(this.platformConfig);
            }
          }
        );
      } else {
        if (!this.libServ.isEmptyObj(this.platformConfig)) {
          resolve(this.platformConfig);
        }
      }
    });
    return platformConfigPromise;
  }

  ngOnDestroy(): void {
    this.platConfigObs.unsubscribe();
  }
}
