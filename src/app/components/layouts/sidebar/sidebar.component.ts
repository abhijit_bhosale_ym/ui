import {
  Component,
  Input,
  Output,
  OnInit,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import { Router } from '@angular/router';
import {
  AuthService,
  PlatformConfigService,
  CommonLibService
} from '../../../_services';
import { Subscription } from 'rxjs';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit, OnDestroy {
  @Output() public childEvent = new EventEmitter();
  @Input() public expanded: boolean;

  config: object = {};

  isActive = false;
  showMenu = 0;
  show = false;
  showSidenavTitle = true;
  sidenavWidth = this.expanded ? 16 : 4.3;
  val = 0;

  sideBarConfig = {};
  logo_url = "../../../assets/images/yuktaone logo.png";
  logo_href = "http://yuktamedia.com/";
  logo_alt_name = "YuktaMedia";

  side_BarConfig = {
    "data": {
      "app_name": {
        "color": "#43b6fa"
      },
      "app_group": {
        "color": "#e2dbdb",
        "background_color": "#1d3954"
      },
      "main_menu": {
        "background_color": "#1d3954"
      },
      "brand_logo": {
        "href": "https://yuktamedia.com",
        "img_url": "../../../../assets/images/yuktaone logo.png",
        "alternativ_name": "YuktaMedia"
      },
      "designation": {
        "color": "#fff"
      },
      "pi_bars_icon": {
        "color": "#fff"
      },
      "active_app_name": {
        "color": "#b9d15b"
      }
    }
  }

  private platConfigObs: Subscription;

  constructor(
    private auth: AuthService,
    private router: Router,
    private platformConfigService: PlatformConfigService,
    public libServ: CommonLibService,
    private _window: Window
  ) { }

  ngOnInit() {
    this.platConfigObs = this.platformConfigService.obsConfig.subscribe(res => {
      if (!this.libServ.isEmptyObj(res)) {
        console.log('url', this.router.url);
        this.config = res;
        this.config['user']['role']['appGroups'].forEach(element => {
          element['apps'].forEach(apps => {
            if (this.router.url != '/') {
              if (apps.route === this.router.url) {
                this.val = element.id;
              }
            } else {
              if (apps.id === this.config['user']['defaultAppId']) {

                this.val = element.id;
              }
            }

          });
        });
        this.getLoginConfig();
      }
    });
    this.childEvent.emit(this.sidenavWidth);
  }

  toggleCollapse() {
    this.show = !this.show;
  }

  toggleExpand(expanded: boolean): void {
    this.show = expanded;

    if (this.show) {
      this.sidenavWidth = 16;
      this.showSidenavTitle = true;
    } else {
      this.sidenavWidth = 4.3;
      this.showSidenavTitle = false;
    }
    this.childEvent.emit(this.sidenavWidth);
  }

  logout(evt): void {
    this.auth.logout().then(
      res => {
        this.platformConfigService.resetPlatformConfig();
        this.router.navigate(['/login']);
      },
      err => {
        console.log(err);
      }
    );
  }

  eventCalled() {
    this.isActive = !this.isActive;
  }

  addExpandClass(element: any) {
    if (element === this.val) {
      this.val = -1;
    } else {
      this.val = element;
    }
  }

  ngOnDestroy(): void {
    if (this.platConfigObs && !this.platConfigObs.closed) {
      this.platConfigObs.unsubscribe();
    }
  }

  preventPropogation(event) {
    let prevent = false;
    event.path.some(ele => {
      if (typeof ele.classList !== 'undefined' && ele.classList.length) {
        if (ele.classList.contains('router-link-active')) {
          prevent = true;
          return true;
        } else {
          return false;
        }
      }
    });
    if (prevent) {
      event.preventDefault();
      event.stopPropagation();
    }
  }

  getLoginConfig() {
    this.platformConfigService.getLoginConfig(this._window['location']).subscribe(data => {
      if (data['status'] === 1 && data['data'].length > 0) {
        this.sideBarConfig = JSON.parse(data['data'][0]['sidebar_config']);
        this.logo_url = this.sideBarConfig['data']['brand_logo']['img_url'];
        this.logo_href = this.sideBarConfig['data']['brand_logo']['href'];
        this.logo_alt_name = this.sideBarConfig['data']['brand_logo']['alternativ_name'];
      } else {
        this.sideBarConfig = this.side_BarConfig;
      }
    })
  }
}
