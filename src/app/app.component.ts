import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { LoaderObsService } from './_services/api-loader/loader-obs.service';
import { ToastService } from './_services/toast-notification/toast.service';
import { MessageService } from 'primeng/api';
import { CommonLibService } from 'src/app/_services/common-lib/common-lib.service';
import { ImpersonateService } from './_services/impersonate/impersonate.service';
import { Subscription } from 'rxjs';
import { PlatformConfigService } from './_services';
import { Router, NavigationEnd } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { Title, Meta } from '@angular/platform-browser';
import { environment } from '../environments/environment';

@Component({
  selector: 'ym-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'ng-framwork-ym';
  displayLoader = false;
  impersonated = false;
  imperSubscription: Subscription;
  enableRouter = false;

  constructor(
    private libServ: CommonLibService,
    private loaderService: LoaderObsService,
    private impersonateServ: ImpersonateService,
    private platformConfigService: PlatformConfigService,
    private toastService: ToastService,
    private messageService: MessageService,
    private router: Router,
    private _window: Window,
    @Inject(DOCUMENT) private _document: HTMLDocument,
    private _titleService: Title,
    private metaService: Meta
  ) {
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        if (!localStorage.getItem('token') && (event.url.includes('auth/set-password/') || event.url.includes('login/forgot-password'))) {
          console.log('url includes forgot pwd or set pwd');
          localStorage.clear();
        } else if (!localStorage.getItem('token') && !event.url.includes('login')) {
          localStorage.clear();
          router.navigate(['/login']);
        } else if (event.url.includes('login')) {
          this.impersonated = false;
        }
      }
    });
    this.getLoginConfig();
  }

  ngOnInit() {
    if (localStorage.getItem('token') && localStorage.getItem('token') != 'null') {
      // console.log('daaaaaaaaaa', localStorage.getItem('token') != 'null');
      this.platformConfigService.getPlatformConfigInfoData();
    }

    this.loaderService.displayLoader.subscribe(visibility => {
      setTimeout(() => {
        this.displayLoader = visibility;
      }, 0);
    });

    this.imperSubscription = this.impersonateServ.impersonated.subscribe(impFlg => {
      setTimeout(() => {
        if (impFlg) {
          this.impersonated = impFlg;
          localStorage.setItem('impersonated', 'true');
        }
      });
    });
    if (localStorage.getItem('impersonated')) {
      this.impersonated = true;
    } else {
      this.impersonated = false;
    }
    this.toastService.showToastToggle$.subscribe(obj => {
      if (!this.libServ.isEmptyObj(obj)) {
        this.messageService.add(obj);
      }
    });
  }

  stopImpersonate() {
    this.impersonateServ.stopImpersonate();
    this.impersonated = false;
    if (localStorage.getItem('impersonated')) {
      localStorage.removeItem('impersonated');
    }
  }

  ngOnDestroy() {
    if (this.imperSubscription && this.imperSubscription.closed) {
      this.imperSubscription.unsubscribe();
      this.toastService.showToastToggle$.unsubscribe();
    }
  }

  getLoginConfig() {
    // console.log("Location", this._window['location']);
    this.platformConfigService.getLoginConfig(this._window['location']).subscribe(data => {
      if (data['status'] === 1 && data['data'].length > 0) {
        const config = JSON.parse(data['data'][0]['document_config']);
        this._document.getElementById('appFavicon').setAttribute('href', config['document']['favicon']);
        this._titleService.setTitle(config['document']['title']);
        this.metaService.addTags(config['meta-tags']);
        environment.exportConfig.exportLogo = config['export-report']['logo'];
        environment.exportConfig.exportTeamName = config['export-report']['team-name'];
        environment.exportConfig.exportMailId = config['export-report']['mail'];
        // console.log("environment", environment);
      }
      else {
        this._titleService.setTitle('YuktaOne');
        this._document.getElementById('appFavicon').setAttribute('href', '../../../../assets/images/ym-favicon.ico');
        this.metaService.addTags([{
          "content": "Digital Advertising",
          "property": "og:type"
        }, {
          "content": "YuktaOne Media ERP",
          "property": "og:title"
        }, {
          "content": "YuktaOne is the industry's first ERP for Publishers to scale their Digital business by enabling automated unified reporting, Billing and Revenue reconciliation, campaign pacing and performance tracking, alerts and notifications, and much more...",
          "property": "og:description"
        }, {
          "content": "https://media.licdn.com/dms/image/C4E22AQEUO3n7RlUTpw/feedshare-shrink_8192/0?e=1566432000&v=beta&t=VFLW5rSh8PSaw5ThHlKo0A8l6dDVwAGgnqQlpxpi-iY",
          "property": "og:image"
        }, {
          "name": "twitter:card",
          "content": "summary_large_image"
        }, {
          "name": "twitter:site",
          "content": "YuktaMedia"
        }, {
          "name": "twitter:creator",
          "content": "YuktaMedia"
        }, {
          "name": "twitter:title",
          "content": "YuktaOne Media ERP"
        }, {
          "name": "twitter:description",
          "content": "YuktaOne is the industry's first ERP for Publishers to scale their Digital business by enabling automated unified reporting, Billing and Revenue reconciliation, campaign pacing and performance tracking, alerts and notifications, and much more..."
        }, {
          "name": "twitter:image",
          "content": "https://media.licdn.com/dms/image/C4E22AQEUO3n7RlUTpw/feedshare-shrink_8192/0?e=1566432000&v=beta&t=VFLW5rSh8PSaw5ThHlKo0A8l6dDVwAGgnqQlpxpi-iY"
        }]);
      }
    });
  }

}
