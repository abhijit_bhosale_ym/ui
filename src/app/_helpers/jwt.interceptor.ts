import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpErrorResponse,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, NEVER } from 'rxjs';
import { tap } from 'rxjs/operators';
import { LoaderObsService } from '../_services/api-loader/loader-obs.service';

// import KJUR = require('jsrsasign');

import { ToastService } from 'src/app/_services/toast-notification/toast.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  count = 0;
  constructor(
    private router: Router,
    private _loadingBar: LoaderObsService,
    private toastService: ToastService
  ) { }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    // add authorization header with jwt token if available
    const token = localStorage.getItem('token');

    // Don't show loading for Tables
    if (
      request.body == null ||
      !request.body['isTable'] ||
      typeof request.body['isTable'] === 'undefined'
    ) {
      this._loadingBar.start();
      this.count = this.count + 1;
    }
    //  else {
    //   delete request.body.isTable;
    // }
    const defaultHeaders = {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Origin, Authorization, Content-Type'
    };
    // debugger
    if (token && typeof token !== 'undefined' && token !== 'null') {
      defaultHeaders['Authorization'] = `Bearer ${token}`;
    }

    request = request.clone({
      setHeaders: defaultHeaders
    });

    return next.handle(request).pipe(
      tap(
        event => {
          if (event instanceof HttpResponse) {
            // do stuff with response if you want
            const url: string = event.url;
            if (
              (url.includes('/login') || url.includes('/sociallogin')) &&
              !url.includes('logout')
            ) {
              localStorage.setItem('token', event.body.token);
            } else if (url.includes('logout')) {
              if (localStorage.getItem('token')) {
                localStorage.removeItem('token');
              }
            }else if (url.includes('getLoginConfig')) {  
            }
            else if (url.includes('countries-110m.json')) {
              this.count = this.count - 1;
            }
            else {
              localStorage.removeItem('token');
              localStorage.setItem('token', event.headers.get('token'));
              // console.log(event.headers.get('token'));
            }
            if (
              request.body == null ||
              !request.body['isTable'] ||
              typeof request.body['isTable'] === 'undefined'
            ) {
              this.count = this.count - 1;
            }
            // console.log("count  :", this.count, "t/f   :", this.count <= 0)
            if (this.count <= 0) {
              this._loadingBar.complete();
            }
          }
        },
        (err: any) => {
          if (err instanceof HttpErrorResponse) {
            if (
              request.body == null ||
              !request.body['isTable'] ||
              typeof request.body['isTable'] === 'undefined'
            ) {
              this.count = this.count - 1;
            }
            if (this.count === 0) {
              this._loadingBar.complete();
            }
            if (err.status === 401) {
              // redirect to the login route
              this.router.navigate(['/login']);
              if (localStorage.getItem('token')) {
                // Token is there but expired
                this.toastService.displayToast({
                  severity: 'error',
                  summary: 'Session Expired',
                  detail: 'Please Login Again'
                });
              }
            } else if (err.status === 403) {
              this.toastService.displayToast({
                severity: 'error',
                summary: 'Unauthorized',
                detail: 'Sorry!!! You are not allowed to access this resource'
              });
            } else {
              this.toastService.displayToast({
                severity: 'error',
                summary: 'Server Error',
                detail: 'Please refresh the page'
              });
            }
          }
        }
      )
    );
  }

  private toJSON(proto): any {
    const jsoned = {};
    const toConvert = proto || this;
    Object.getOwnPropertyNames(toConvert).forEach(prop => {
      const val = toConvert[prop];
      // don't include those
      if (prop === 'toJSON' || prop === 'constructor' || prop === 'blobfile') {
        return;
      }
      if (typeof val === 'function') {
        jsoned[prop] = val.bind(jsoned);
        return;
      }
      jsoned[prop] = val;
    });

    const inherited = Object.getPrototypeOf(toConvert);
    if (inherited !== null) {
      Object.keys(this.toJSON(inherited)).forEach(key => {
        if (!!jsoned[key] || key === 'constructor' || key === 'toJSON') {
          return;
        }
        if (typeof inherited[key] === 'function') {
          jsoned[key] = inherited[key].bind(jsoned);
          return;
        }
        jsoned[key] = inherited[key];
      });
    }
    return jsoned;
  }
}
