import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../../environments/environment';

@Injectable()
export class AuthService {
  private BASE_URL = environment.baseUrl;

  constructor(private http: HttpClient) {}
  login(user) {
    const url = `${this.BASE_URL}/umf/v1/auth/login`;
    return this.http.post(url, user).toPromise();
  }

  initialLogin(user) {
    const id = window.location.pathname.split('/').pop();
    const url = `${this.BASE_URL}/umf/v1/auth/create/${id}`;
    user['re-password'] = user['repassword'];
    delete user['repassword'];
    return this.http.post(url, user);
  }

  register(user) {
    const url = `${this.BASE_URL}/umf/v1/auth/register`;
    return this.http.post(url, user);
  }

  public setForgotPassword(details) {
    const url = `${this.BASE_URL}/umf/v1/auth/forgot-password`;
    return this.http.post(url, details);
  }

  public setResetPassword(details) {
    const url = `${this.BASE_URL}/umf/v1/auth/reset-password`;
    return this.http.post(url, details);
  }

  public setNewUserPassword(details) {
    const url = `${this.BASE_URL}/umf/v1/auth/set-password`;
    return this.http.put(url, details);
  }

  logout() {
    const url = `${this.BASE_URL}/umf/v1/auth/logout`;
    return this.http.get(url).toPromise();
  }
}
