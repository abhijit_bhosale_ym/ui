import { Injectable } from '@angular/core';
import { ReplaySubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppConfigService {
  private appConfigSubject$ = new ReplaySubject<any>(1);
  appConfig = this.appConfigSubject$.asObservable();

  changeAppConfig(appConfig: object) {
    this.appConfigSubject$.next(Object.assign(this.appConfig, appConfig));
  }

}
