import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchObj'
})
export class SearchObjPipe implements PipeTransform {
  transform(items: any[], field: string, value: string, type): any[] {
    if (!items) {
      return [];
    }
    if (type == 'ticket') {
      if (value !== '') {

        return items.filter(item => {
          let severity = item['severity'] === null ? "" : item['severity'];
          let ticketComment = item['ticketComment'] === null ? "" : item['ticketComment'];
          let display_name = item['display_name'] === null ? "" : item['display_name'];
          let ticketTitle = item['ticketTitle'] === null ? "" : item['ticketTitle'];
          let created_at = item['created_at'] === null ? "" : item['created_at'];
        
          if (
            severity.toLowerCase().indexOf(value) !== -1 ||
            ticketComment.toLowerCase().indexOf(value) !== -1 ||
            display_name.toLowerCase().indexOf(value) !== -1 ||
            ticketTitle.toLowerCase().indexOf(value) !== -1
          ) {
            return true;
          }

          return false;
        });
      } else {
        return items;
      }
    }
    else {
      if (value !== '') {
        return items.filter(it =>
          it[field].toLowerCase().includes(value.toLowerCase())
        );
      } else {
        return items;
      }
    }
  }
}

// Example
// <input #txtSearch placeholder="first name" />
// <tr *ngFor="let item of mylst | searchObj: 'fname' : txtSearch.value">
//   <td>
//       {{item.fname}}
//   </td>
//   <td>
//       {{item.lname}}
//   </td>
// </tr>
