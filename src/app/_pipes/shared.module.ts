import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormatNumPipe } from './number-format.pipe';
import { YmInputDirective } from '../_directives/input-ym.directive';
import { SearchStringPipe } from './search-string.pipe';
import { SearchObjPipe } from './search-obj.pipe';

@NgModule({
  declarations: [
    FormatNumPipe,
    YmInputDirective,
    SearchStringPipe,
    SearchObjPipe
  ],
  imports: [CommonModule],
  exports: [FormatNumPipe, SearchStringPipe, SearchObjPipe, YmInputDirective]
})
export class SharedModule {}
