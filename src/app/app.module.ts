import { BrowserModule, Title } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// prime ng module, services and apis
import { CardModule } from 'primeng/card';
import { ButtonModule } from 'primeng/button';
import { PasswordModule } from 'primeng/password';
import { MessageModule } from 'primeng/message';
import { CheckboxModule } from 'primeng/checkbox';
import { DynamicDialogModule } from 'primeng/dynamicdialog';
import { ToastModule } from 'primeng/toast';
import { TableModule } from 'primeng/table';
import { TreeTableModule } from 'primeng/treetable';
import { AccordionModule } from 'primeng/accordion';
import { MultiSelectModule } from 'primeng/multiselect';
import { EditorModule } from 'primeng/editor';
import { ChipsModule } from 'primeng/chips';
import { TabViewModule } from 'primeng/tabview';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { DropdownModule } from 'primeng/dropdown';
import { RadioButtonModule } from 'primeng/radiobutton';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { MessageService, ConfirmationService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { SelectButtonModule } from 'primeng/selectbutton';

import { AuthGuard, JwtInterceptor } from './_helpers';
import { AuthService, PlatformConfigService, DataShareService, CommonLibService } from './_services';
import { FormatNumPipe } from 'src/app/_pipes/number-format.pipe';
import { SearchStringPipe } from 'src/app/_pipes/search-string.pipe';
import { SearchObjPipe } from 'src/app/_pipes/search-obj.pipe';

import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from 'src/app/_pipes/shared.module';
import { ChatModule } from './modules/common/chat/chat.module';
import { ChartsModule } from './modules/common/charts/charts.module';
import { FilterContainerModule } from 'src/app/modules/common/filter-container/filter-container.module';

import { AppComponent } from './app.component';
import { LoginComponent } from './components';
import { ForgotPasswordComponent } from './components/login/forgot-password/forgot-password.component';
import { AppLayoutComponent } from './components/layouts/app-layout/app-layout.component';
import { FooterComponent } from './components/layouts/footer/footer.component';
import { SidebarComponent } from './components/layouts//sidebar/sidebar.component';
import { AutoCompleteModule } from 'primeng/autocomplete';

// Entry Components
import { DummyCompComponent } from './modules/revmgmt-app/dummy-comp/dummy-comp.component';
import { CropProfileComponent } from './modules/user-profile/crop-profile/crop-profile.component';
import { EditProfileComponent } from './modules/user-profile/edit-profile/edit-profile.component';
import { ChangePasswordComponent } from './modules/user-profile/change-password/change-password.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { Rev360DailyDataPopupComponent } from './modules/rev360-app/rev360-daily-data-popup/rev360-daily-data-popup.component';
import { Rev360ExportPopupComponent } from './modules/rev360-app/rev360-export-popup/rev360-export-popup.component';
import { UPRDailyDataPopupComponent } from './modules/upr/upr-daily-data-popup/upr-daily-data-popup.component';
import { UPRExportPopupComponent } from './modules/upr/upr-export-popup/upr-export-popup.component';
import { UnfilledDataPopupComponent } from './modules/unfilled-app/unfilled-popup/unfilled-daily-data-popup.component';
import { VastDataPopupComponent } from './modules/vast-app/vast-popup/vast-daily-data-popup.component';
import { CostPopupComponent } from './modules/threep-statements/cost-popup/cost-popup.component';
import {
  FranklyPayoutExportPopupComponent
} from './modules/frankly-payout-app/franklypayout-export-popup/franklypayout-export-popup.component';
import {
  FranklyPayoutDailyDataPopupComponent
} from './modules/frankly-payout-app/franklypayout-daily-data-popup/franklypayout-daily-data-popup.component';
import {
  PayoutPublisherDailyDataPopupComponent
} from './modules/payout-publisher/payout-publisher-daily-data-popup/payout-publisher-daily-data-popup.component';
import {
  OTTAnalyticsDailyDataPopupComponent
} from './modules/ott-analytics/ott-analytics-daily-data-popup/ott-analytics-daily-data-popup.component';
import {
  CampaignProgressDailyDataPopupComponent
} from './modules/compaign-progress-dashboard/campaign-progress-daily-data-popup/campaign-progress-daily-data-popup.component';
import { RoleDetailsComponent } from './modules/users/role-details/role-details.component';
import { TeamDetailsComponent } from './modules/users/team-details/team-details.component';
import { TeamDetailsComponent as TeamsTeamDetailsComponent } from './modules/team/team-details/team-details.component';
import { ChangeRoleTeamComponent } from './modules/users/change-role-team/change-role-team.component';
import { ResetPasswordComponent } from './modules/users/reset-password/reset-password.component';
import { TermsConditionsComponent } from './components/login/terms-conditions/terms-conditions.component';
import { DefaultappComponent } from './components/defaultapp/defaultapp.component';
import { DailyDataComponent } from './modules/cpr/daily-data/daily-data.component';
import { CommentComponent } from './modules/cpr/comment/comment.component';
import { SendEmailComponent } from './modules/send-email/send-email.component';
import { EditRoleComponent } from './modules/roles/edit-role/edit-role.component';
import { ImpactedUserComponent } from './modules/users/impacted-user/impacted-user.component';
import { RoleDetailComponent } from './modules/roles/role-details/role-details.component';
import { EditTeamComponent } from './modules/team/edit-team/edit-team.component';
import { TeamModule } from './modules/team/team.module';
import { TeamResourcesComponent } from './modules/team-resources/team-resources.component';
// import { UsersModule } from './modules/users/users.module';
// import { AddUserComponent } from './modules/users/add-user/add-user.component';
import { DSPConnectionDailyDataPopupComponent } from './modules/dsp-connection-app/dsp-connection-daily-data-popup/dsp-connection-daily-data-popup.component';
import { DSPConnectionDataPopupComponent } from './modules/dsp-connection-app/dsp-connection-data-popup/dsp-connection-daily-data-popup.component';
import { DSPConnectionExportPopupComponent } from './modules/dsp-connection-app/dsp-connection-export-popup/dsp-connection-export-popup.component';
import { StepsModule } from 'primeng/steps';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { InputSwitchModule } from 'primeng/inputswitch';

import { PublisherExportPopupComponent } from './modules/payout-publisher/publisher-export-popup/publisher-export-popup.component';
import { OTTAnalyticsExportPopupComponent } from './modules/ott-analytics/ott-analytics-export-popup/ott-analytics-export-popup.component';
import { ChatComponent } from './modules/common/chat/chat/chat.component';
import { PlatformPublisherDailyDataPopupComponent } from './modules/platform-publisher/platform-publisher-daily-data-popup/platform-publisher-daily-data-popup.component';
// import { AudienceSegement360Component } from './modules/audience-segement360/audience-segement360.component';
import { AttributeDailyDataPopupComponent } from './modules/audience-segement360/attribute-daily-data-popup/attribute-daily-data-popup.component';
import { DetailsDataPopupComponent } from './modules/builder-app/details-data-popup/details-data-popup.component';
import { DSPDetailsDataPopupComponent } from './modules/audience-segement360/dsp-details-data-popup/dsp-details-data-popup.component';
import { Audience360DailyDataPopupComponent } from './modules/audience-segement360/audience360-daily-data-popup/audience360-daily-data-popup.component';
import { Audience360ExportPopupComponent } from './modules/audience-segement360/audience360-export-popup/audience360-export-popup.component';
import {
  BuilderDailyDataPopupComponent
} from './modules/builder-daily-data-popup/builder-daily-data-popup.component';
import {
  ExpressionDetailsDailyDataPopupComponent
} from './modules/builder-app/expression-details-data-popup/expression-details-data-popup.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { NewRioComponent } from './modules/rio-app/new-io/new-rio.component';
import { CalendarModule } from 'primeng/calendar';
import { DailyDataComponent360 } from './modules/supply360-app/daily-data/daily-data.component';
import { NewPitIssuesComponent } from './modules/pit-issues/new-pit-issues/new-pit-issues.component';
import { EditSaveViewComponent } from './modules/common/filter-container/edit-save-view/edit-save-view.component';
import { CreateTicketComponent } from './modules/support-app/create-ticket/create-ticket.component'
import { TaxFormComponent } from './modules/payment-contact-info-app/tax-form/tax-form.component';
import { UploadTaxFormComponent } from './modules/payment-contact-info-app/tax-form/upload-tax-form/upload-tax-form.component';
import { FileUploadModule } from 'primeng/fileupload';
import { BankingFormComponent } from './modules/payment-contact-info-app/banking-form/banking-form.component';
import { MainFormComponent } from './modules/payment-contact-info-app/banking-form/main-form/main-form.component';
import { ContactFormComponent } from './modules/payment-contact-info-app/contact-form/contact-form.component';
import { ContactMainFormComponent } from './modules/payment-contact-info-app/contact-form/contact-main-form/contact-main-form.component';
import { PaymentMethodFormComponent } from './modules/payment-contact-info-app/payment-method/payment-method-form.component';
import { PaymentSummaryExportPopupComponent } from './modules/payment-summary/payment-summary-export-popup/payment-summary-export-popup.component';
import { TagCodePopupComponent } from './modules/compaign-management/tag-code-popup/tag-code-popup.component'
import { VisualizeChartPopComponent } from './modules/visualize/visualize-chart-pop/visualize-chart-pop.component';
import { ScrollPanelModule, DataViewModule } from 'primeng';
import { PanelModule } from 'primeng/panel';
import { OrderListModule } from 'primeng/orderlist';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { ChartDetailsComponent } from './modules/visualize/visualize-create/chart-details/chart-details.component';
import { AdHocDashboardPopComponent } from './modules/ad-hoc-dashboard/ad-hoc-dashboard-pop/ad-hoc-dashboard-pop.component';
import { DashboardDetailsComponent } from './modules/ad-hoc-dashboard/dashboard-create/dashboard-details/dashboard-details.component';
import { AngularDraggableModule } from 'angular2-draggable';
import { ListboxModule } from 'primeng/listbox';
import { TabularDataPopupComponent } from './modules/ad-hoc-dashboard/dashboard-create/tabular-data/tabular-data-popup.component';
import { CustomDateRangeComponent } from './modules/ad-hoc-dashboard/dashboard-create/custom-daterange/custom-daterange.component';
import { CustomPanelComponent } from './modules/ad-hoc-dashboard/dashboard-create/custom-panel/custom-panel.component';
import { ColorPickerModule } from 'primeng/colorpicker';
import { CardsModule } from './modules/common/cards/cards.module';
import { AlertInfoByNamePopupComponent } from './modules/alert-dashboard-app/alert-info-by-name-popup/alert-info-by-name-popup.component';
import { AlertResultPopupComponent } from './modules/alert-dashboard-app/alert-result-popup/alert-result-popup.component';
import { EditAlertGroupComponent } from './modules/alert-create-app/edit-alert-group/edit-alert-group.component';
import { AlertByAlertGroupPopupComponent } from './modules/alert-create-app/alert-by-alertgroup-popup/alert-by-alertgroup-popup.component';
import { EditAlertComponent } from './modules/alert-create-app/edit-alert/edit-alert.component'
import { AlertGroupInfoComponent } from './modules/alert-create-app/alert-group-info/alert-group-info.component'
import { AlertInfoPopupComponent } from './modules/alert-create-app/alert-by-alertgroup-popup/alert-info-popup/alert-info-popup.component';
import { DataTableModule } from './modules/common/data-table/data-table.module';
import { TreeModule } from 'primeng/tree';
import { FileLinkEditPopupComponent } from './modules/threep-statements/file-link-edit-popup/file-link-edit-popup.component'
import { ListDashboardGraphComponent } from './modules/ad-hoc-dashboard/dashboard-create/list-dashboard-graph/list-dashboard-graph.component';
import { CampaignDailyDataPopupComponent } from './modules/active-campaigns/campaign-daily-data-popup/campaign-daily-data-popup.component'
import { NewSitesDataPopupComponent } from './modules/new-sites-app/new-sites-popup/new-sites-daily-data-popup.component';
import { DailyDataPopupComponent } from './modules/prebid-app/daily-data-popup/daily-data-popup.component';
import { AnalyticsDailyDataPopupComponent } from './modules/analytics/analytics-daily-data-popup/analytics-daily-data-popup.component';
import { ShareDashboardPopComponent } from './modules/ad-hoc-dashboard/share-dashboard-pop/share-dashboard-pop.component';
import { CampaignEditPopupComponent } from './modules/active-campaigns/campaign-edit-popup/campaign-edit-popup.component'
import { DummyRevmgmtCompComponent } from './modules/revmgmt-freecycle/dummy-revmgmt-comp/dummy-revmgmt-comp.component';
import { UnfilledPopupComponent } from './modules/unfilled-freecycle/unfilled-popup/unfilled-daily-data-popup.component';

import { CreateSegmentComponent } from './modules/audience-segement360/create-segment/create-segment.component';
// import { YqlAutocomplateComponent } from '../../projects/yql-autocomplate/src/lib/yql-autocomplate.component'
import { YqlAutocomplateModule } from 'projects/yql-autocomplate/src/public-api';

import { SupplyPerformanceDailyDataPopupComponent } from './modules/supply-performance-app/supply-performance-daily-data-popup/supply-performance-daily-data-popup.component'
import { GoalSettingDailyDataComponent } from './modules/goal-setting-app/daily-data/daily-data.component'
import { AllCampaignsDailyDataPopupComponent } from './modules/all-campaigns/all-campaigns-daily-data-popup/all-campaigns-daily-data-popup.component'
import { AllCampaignsEditPopupComponent } from './modules/all-campaigns/all-campaigns-edit-popup/all-campaigns-edit-popup.component'

import { CampaignPerformanceDailyDataPopupComponent } from './modules/campaign-performance/campaign-performance-daily-data-popup/campaign-performance-daily-data-popup.component';
import { RevmgmtAppAsbnDailyComponent } from './modules/revmgmt-app-asbn/revmgmt-app-asbn-daily/revmgmt-app-asbn-daily.component';
import { Rev360AsbnDailyDataPopupComponent } from './modules/rev360-app-asbn/rev360-asbn-daily-data-popup/rev360-asbn-daily-data-popup.component';
import { Rev360AsbnExportPopupComponent } from './modules/rev360-app-asbn/rev360-asbn-export-popup/rev360-asbn-export-popup.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ForgotPasswordComponent,
    AppLayoutComponent,
    FooterComponent,
    SidebarComponent,
    CropProfileComponent,
    EditProfileComponent,
    ChangePasswordComponent,
    Rev360DailyDataPopupComponent,
    Rev360ExportPopupComponent,
    PayoutPublisherDailyDataPopupComponent,
    OTTAnalyticsDailyDataPopupComponent,
    CampaignProgressDailyDataPopupComponent,
    UPRExportPopupComponent,
    UPRDailyDataPopupComponent,
    RoleDetailsComponent,
    TeamDetailsComponent,
    TeamsTeamDetailsComponent,
    ChangeRoleTeamComponent,
    ResetPasswordComponent,
    DummyCompComponent,
    UnfilledDataPopupComponent,
    CostPopupComponent,
    VastDataPopupComponent,
    TermsConditionsComponent,
    FranklyPayoutDailyDataPopupComponent,
    FranklyPayoutExportPopupComponent,
    DefaultappComponent,
    DailyDataComponent,
    CommentComponent,
    SendEmailComponent,
    EditRoleComponent,
    ImpactedUserComponent,
    RoleDetailComponent,
    EditTeamComponent,
    TeamResourcesComponent,
    // AddUserComponent
    PublisherExportPopupComponent,
    OTTAnalyticsExportPopupComponent,
    PlatformPublisherDailyDataPopupComponent,
    DSPConnectionDailyDataPopupComponent,
    DSPConnectionDataPopupComponent,
    DSPConnectionExportPopupComponent,
    AttributeDailyDataPopupComponent,
    DSPDetailsDataPopupComponent,
    Audience360DailyDataPopupComponent,
    Audience360ExportPopupComponent,
    BuilderDailyDataPopupComponent,
    ExpressionDetailsDailyDataPopupComponent,
    DetailsDataPopupComponent,
    NewRioComponent,
    DailyDataComponent360,
    EditSaveViewComponent,
    CreateTicketComponent,
    NewPitIssuesComponent,
    TaxFormComponent,
    UploadTaxFormComponent,
    BankingFormComponent,
    MainFormComponent,
    ContactFormComponent,
    ContactMainFormComponent,
    PaymentMethodFormComponent,
    PaymentSummaryExportPopupComponent,
    TagCodePopupComponent,
    VisualizeChartPopComponent,
    ChartDetailsComponent,
    AdHocDashboardPopComponent,
    DashboardDetailsComponent,
    TabularDataPopupComponent,
    CustomDateRangeComponent,
    CustomPanelComponent,
    AlertInfoByNamePopupComponent,
    AlertResultPopupComponent,
    AlertByAlertGroupPopupComponent,
    EditAlertComponent,
    EditAlertGroupComponent,
    AlertGroupInfoComponent,
    AlertInfoPopupComponent,
    CampaignDailyDataPopupComponent,
    NewSitesDataPopupComponent,
    FileLinkEditPopupComponent,
    ListDashboardGraphComponent,
    DailyDataPopupComponent,
    AnalyticsDailyDataPopupComponent,
    ShareDashboardPopComponent,
    CampaignEditPopupComponent,
    DummyRevmgmtCompComponent,
    UnfilledPopupComponent,
    CreateSegmentComponent,
    SupplyPerformanceDailyDataPopupComponent,
    GoalSettingDailyDataComponent,
    AllCampaignsDailyDataPopupComponent,
    AllCampaignsEditPopupComponent,
    CampaignPerformanceDailyDataPopupComponent,
    RevmgmtAppAsbnDailyComponent,
    Rev360AsbnDailyDataPopupComponent,
    Rev360AsbnExportPopupComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    FilterContainerModule,
    AppRoutingModule,
    HttpClientModule,
    CardModule,
    MessageModule,
    ButtonModule,
    CheckboxModule,
    PasswordModule,
    ProgressSpinnerModule,
    DynamicDialogModule,
    ToastModule,
    ImageCropperModule,
    ChipsModule,
    TabViewModule,
    ChartsModule,
    TableModule,
    TreeTableModule,
    AccordionModule,
    MultiSelectModule,
    SharedModule,
    ChatModule,
    DropdownModule,
    ConfirmDialogModule,
    RadioButtonModule,
    TeamModule, EditorModule,
    // UsersModule,
    InputTextareaModule,
    InputSwitchModule,
    StepsModule,
    ChatModule,
    MatSelectModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,
    CalendarModule,
    SelectButtonModule,
    FileUploadModule,
    ScrollPanelModule,
    DataViewModule,
    PanelModule,
    OrderListModule,
    ToggleButtonModule,
    ListboxModule,
    AngularDraggableModule,
    ColorPickerModule,
    CardsModule,
    AutoCompleteModule,
    TreeModule,
    DataTableModule,
    YqlAutocomplateModule
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    MatInputModule,
    SelectButtonModule
  ],
  providers: [
    AuthGuard,
    AuthService,
    DataShareService,
    PlatformConfigService,
    CommonLibService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    MessageService,
    DialogService,
    FormatNumPipe,
    SearchStringPipe,
    SearchObjPipe,
    ConfirmationService,
    { provide: Window, useValue: window },
    Title
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    LoginComponent,
    ForgotPasswordComponent,
    CropProfileComponent,
    EditProfileComponent,
    RoleDetailsComponent,
    TeamDetailsComponent,
    TeamsTeamDetailsComponent,
    ChangeRoleTeamComponent,
    ChangePasswordComponent,
    Rev360DailyDataPopupComponent,
    Rev360ExportPopupComponent,
    PayoutPublisherDailyDataPopupComponent,
    OTTAnalyticsDailyDataPopupComponent,
    CampaignProgressDailyDataPopupComponent,
    UPRExportPopupComponent,
    UPRDailyDataPopupComponent,
    TermsConditionsComponent,
    DummyCompComponent,
    UnfilledDataPopupComponent,
    CostPopupComponent,
    SendEmailComponent,
    VastDataPopupComponent,
    FranklyPayoutDailyDataPopupComponent,
    FranklyPayoutExportPopupComponent,
    DailyDataComponent,
    CommentComponent,
    EditRoleComponent,
    ImpactedUserComponent,
    RoleDetailComponent,
    ResetPasswordComponent,
    EditTeamComponent,
    PublisherExportPopupComponent,
    OTTAnalyticsExportPopupComponent,
    ChatComponent,
    PlatformPublisherDailyDataPopupComponent,
    DSPConnectionDailyDataPopupComponent,
    DSPConnectionDataPopupComponent,
    DSPConnectionExportPopupComponent,
    AttributeDailyDataPopupComponent,
    DSPDetailsDataPopupComponent,
    Audience360DailyDataPopupComponent,
    Audience360ExportPopupComponent,
    BuilderDailyDataPopupComponent,
    ExpressionDetailsDailyDataPopupComponent,
    DetailsDataPopupComponent,
    NewRioComponent,
    DailyDataComponent360,
    EditSaveViewComponent,
    CreateTicketComponent,
    NewPitIssuesComponent,
    TaxFormComponent,
    UploadTaxFormComponent,
    BankingFormComponent,
    MainFormComponent,
    ContactFormComponent,
    ContactMainFormComponent,
    PaymentMethodFormComponent,
    PaymentSummaryExportPopupComponent,
    TagCodePopupComponent,
    VisualizeChartPopComponent,
    ChartDetailsComponent,
    AdHocDashboardPopComponent,
    DashboardDetailsComponent,
    TabularDataPopupComponent,
    CustomDateRangeComponent,
    CustomPanelComponent,
    AlertInfoByNamePopupComponent,
    AlertResultPopupComponent,
    AlertByAlertGroupPopupComponent,
    EditAlertComponent,
    EditAlertGroupComponent,
    AlertGroupInfoComponent,
    AlertInfoPopupComponent,
    CampaignDailyDataPopupComponent,
    NewSitesDataPopupComponent,
    FileLinkEditPopupComponent,
    ListDashboardGraphComponent,
    DailyDataPopupComponent,
    AnalyticsDailyDataPopupComponent,
    ShareDashboardPopComponent,
    CampaignEditPopupComponent,
    SupplyPerformanceDailyDataPopupComponent,
    GoalSettingDailyDataComponent,
    AllCampaignsDailyDataPopupComponent,
    AllCampaignsEditPopupComponent,
    UnfilledPopupComponent,
    CreateSegmentComponent,
    SupplyPerformanceDailyDataPopupComponent,
    GoalSettingDailyDataComponent,
    DummyRevmgmtCompComponent,
    CampaignPerformanceDailyDataPopupComponent,
    RevmgmtAppAsbnDailyComponent,
    Rev360AsbnDailyDataPopupComponent,
    Rev360AsbnExportPopupComponent
  ]
})
export class AppModule { }
