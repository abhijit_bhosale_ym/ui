import * as moment from 'moment-timezone';
export const environment = {
  baseUrl: '/api',
  socketBaseUrl: '',
  secret: 'TXlEYWFTc3ByaW5nQm9vdA==',
  adminRoles: ['YM MASTER ADMIN', 'ANALYTICS MASTER ADMIN'],
  production: true,
  timeZone: moment().tz('America/New_York').format('z'),
  exportConfig: {
    exportLogo: 'ExportImages/yuktaone-logo.png',
    exportTeamName: 'YuktaMedia',
    exportMailId: "info@yuktamedia.com"
  }
};
